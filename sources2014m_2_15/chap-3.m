#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
                                                                         #####
  ####   #    #    ##    #####      #     #####  #####   ######         #     #
 #    #  #    #   #  #   #    #     #       #    #    #  #                    #
 #       ######  #    #  #    #     #       #    #    #  #####           #####
 #       #    #  ######  #####      #       #    #####   #                    #
 #    #  #    #  #    #  #          #       #    #   #   #              #     #
  ####   #    #  #    #  #          #       #    #    #  ###### #######  #####
 #
 #
 #
 #
 #
 #
 #
 #                       CALCUL DE L'IMPOT NET
 #
 #
 #
 #
 #
 #
regle 301:
application : bareme , iliad , batch  ;

IRN = min( 0, IAN + AVFISCOPTER - IRE ) + max( 0, IAN + AVFISCOPTER - IRE ) * positif( IAM[DGFIP][2017] + 1 - SEUIL_61) ;


regle 3010:
application : bareme , iliad , batch  ;

IAR = min( 0, IAN + AVFISCOPTER - IRE ) + max( 0, IAN + AVFISCOPTER - IRE ) ;

regle 302:
application : iliad , batch  ;
CREREVET =  min(arr((BPTP3 + BPTPD + BPTPG) * TX16/100),arr(CIIMPPRO * TX_CREREVET/100 ))
	  + min(arr(BPT[DGFIP][2017]9 * TX19/100),arr(CIIMPPRO2 * TX19/100 ));

CIIMPPROTOT = CIIMPPRO + CIIMPPRO2 ;
regle 30202:
application : iliad , batch  ;
ICI8XFH = min(arr(BPT[DGFIP][2017]8 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));
ICI8XV  = min(arr(RCM2FA * TX24/100),arr(COD8XV * TX24/100 )) * (1 - positif(null(2 - V_REGCO)+null(4-V_REGCO)));
ICIGLO = min(arr(BPT[DGFIP][2017]8 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(RCM2FA * TX24/100),arr(COD8XV * TX24/100 )) * (1 - positif(null(2 - V_REGCO)+null(4-V_REGCO)))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));

CIGLOTOT = COD8XF + COD8XG + COD8XH; 
CI8XV = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET,ICI8XV));
CI8XFH = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET-CI8XV,ICI8XFH));
CIGLO = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET,ICIGLO));
regle 3025:
application : iliad , batch  ;

ICREREVET = max(0,min(IA[DGFIP][2017]1 + ITP - CIRCMAVFT - IRETS - min(IA[DGFIP][2017]1 , CRCFA), min(ITP,CREREVET)));

regle 3026:
application : iliad , batch , bareme ;

INE = (CIRCMAVFT + IRETS + min(max(0,IA[DGFIP][2017]1-CIRCMAVFT-IRETS) , CRCFA) + ICREREVET + CIGLO + CICULTUR + CIGPA + CIDONENTR + CICORSE + CIRECH + CICOMPEMPL)
            * (1-positif(RE168+TAX1649));

IAN = max( 0, (IRB - AVFISCOPTER + ((- CIRCMAVFT
				     - IRETS
                                     - min(max(0,IA[DGFIP][2017]1-CIRCMAVFT-IRETS) , CRCFA) 
                                     - ICREREVET
                                     - CIGLO
                                     - CICULTUR
                                     - CIGPA
                                     - CIDONENTR
                                     - CICORSE
				     - CIRECH 
                                     - CICOMPEMPL)
                                   * (1 - positif(RE168 + TAX1649)))
                  + min(TAXASSUR+0 , max(0,INE-IRB+AVFISCOPTER)) 
                  + min(IPCAPTAXTOT+0 , max(0,INE-IRB+AVFISCOPTER - min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))))
                  + min(TAXLOY+0 ,max(0,INE-IRB+AVFISCOPTER - min(IPCAPTAXTOT+0 , max(0,INE-IRB+AVFISCOPTER - min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))))
										  - min(TAXASSUR+0 , max(0,INE-IRB+AVFISCOPTER))))
	      )
         )
 ;

regle 3021:
application : iliad , batch  ;
IRE = (1- positif(RE168+TAX1649+0)) * (
                      CIDIREPARGNE + EPAV + CRICH + CICORSENOW 
                    + CIGE + CIDEVDUR + CITEC
                    +  IPELUS + CICA + CIGARD + CISYND 
                    + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM 
                    + CREAPP +CREAGRIBIO + CREPROSP + CRESINTER 
                    + CREFORMCHENT + CREINTERESSE + CREARTS + CICONGAGRI 
                    + CRERESTAU + CILOYIMP + AUTOVERSLIB
                    + PPETOTX - PPERSA
                    + C[DGFIP][2017]CK + CIFORET + CIEXCEDENT
                    + COD8TL * (1 - positif(RE168 + TAX1649))
	                              );
IRE2 = IRE + (BCIGA * (1 - positif(RE168+TAX1649))); 
regle 3022:
application : iliad , batch  ;

CRICH =  IPRECH * (1 - positif(RE168+TAX1649));
regle 30221:
application : iliad , batch  ;
CIRCMAVFT = max(0,min(IRB + TAXASSUR + IPCAPTAXTOT +TAXLOY - AVFISCOPTER , RCMAVFT * (1 - positif(null(2 - V_REGCO)+null(4-V_REGCO)))));
regle 302229:
application : iliad , batch  ;
CIEXCEDENT =  arr((COD3VE * TX45/100) + (COD3UV * TX30/100))* (1 - positif(RE168 + TAX1649));
regle 30222:
application : iliad , batch  ;
CIDIREPARGNE = DIREPARGNE * (1 - positif(RE168 + TAX1649)) * (1 - positif(null(2 - V_REGCO)+null(4-V_REGCO)));
C[DGFIP][2017]CK = CO[DGFIP][2017]CK * (1 - positif(RE168 + TAX1649)) * (1 - positif(null(2 - V_REGCO)+null(4-V_REGCO)));
regle 30226:
application : batch, iliad;
CICA =  arr(BAILOC98 * TX_BAIL / 100) * (1 - positif(RE168 + TAX1649)) ;
regle 3023:
application : iliad , batch  ;
CRCFA = (arr(IPQ1 * REGCI / (RB018XR + TONEQUO )) * (1 - positif(RE168+TAX1649)));
regle 30231:
application : iliad , batch  ;
IRETS = max(0,min(min(COD8PA,IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT)*present(COD8PA)+(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT)*(1-present(COD8PA)) , (IPSOUR * (1 - positif(RE168+TAX1649))))) ;
regle 3023101:
application : iliad , batch  ;
CRDIE = max(0,min(IRB-REI-AVFISCOPTER-CIRCMAVFT-IRETS,(min(IA[DGFIP][2017]1-CIRCMAVFT-IRETS,CRCFA) * (1 - positif(RE168+TAX1649)))));
CRDIE2 = -CRDIE+0;
regle 3023102:
application : iliad , batch  ;
BCIAQCUL = arr(CIAQCUL * TX_CIAQCUL / 100);
CICULTUR = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-REI-IRETS-CRDIE-ICREREVET-CIGLO,min(IA[DGFIP][2017]1+ITP+TAXASSUR+TAXLOY +IPCAPTAXTOT+CHRAPRES,BCIAQCUL)));
regle 3023103:
application : iliad , batch  ;
BCIGA = CRIGA;
CIGPA = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR,BCIGA));
regle 3023104:
application : iliad , batch  ;
BCIDONENTR = RDMECENAT * (1-V_CNR) ;
CIDONENTR = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-REI-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR-CIGPA,BCIDONENTR));
regle 3023105:
application : iliad , batch  ;
CICORSE = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IPPRICORSE-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR-CIGPA-CIDONENTR,CIINVCORSE+IPREPCORSE));
CICORSEAVIS = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY-AVFISCOPTER-CIRCMAVFT-IPPRICORSE-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR-CIGPA-CIDONENTR,CIINVCORSE+IPREPCORSE))+CICORSENOW;
TOTCORSE = CIINVCORSE + IPREPCORSE + CICORSENOW;
regle 3023106:
application : iliad , batch  ;
CIRECH = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR-CIGPA-CIDONENTR-CICORSE,IPCHER));
regle 30231061:
application : iliad , batch  ;
CICOMPEMPL = max(0,min(IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR-CIGPA-CIDONENTR-CICORSE-CIRECH,COD8UW));

DIEMPLOI = (COD8UW + COD8TL) * (1 - positif(RE168+TAX1649)) ;

CIEMPLOI = (CICOMPEMPL + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECR = abs(min(0 ,IRB+TAXASSUR+IPCAPTAXTOT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-CRDIE-ICREREVET-CIGLO-CICULTUR-CIGPA-CIDONENTR-CICORSE-CIRECH-CICOMPEMPL));
regle 30231051:
application : iliad , batch  ;
REPCORSE = abs(CIINVCORSE+IPREPCORSE-CICORSE) ;
REPRECH = abs(IPCHER - CIRECH) ;
REPCICE = abs(COD8UW - CICOMPEMPL) ;
regle 3023107:
application : iliad , batch  ;
CICONGAGRI = CRECONGAGRI * (1-V_CNR) ;
regle 30231071:
application : iliad , batch  ;
BCICAP = min(IPCAPTAXTOT,arr((PRELIBXT - arr(PRELIBXT * TX10/100))*T_PCAPTAX/100));
BCICAPAVIS = max(0,(PRELIBXT - arr(PRELIBXT * TX10/100)));
CICAP = max(0,min(IRB + TAXASSUR + IPCAPTAXTOT +TAXLOY +CHRAPRES - AVFISCOPTER ,min(IPCAPTAXTOT,BCICAP)));
regle 30231072:
application : iliad , batch  ;
BCICHR = arr(CHRAPRES * (REGCI*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR - TEFFHRC+COD8YJ));
CICHR = max(0,min(IRB + TAXASSUR + IPCAPTAXTOT +TAXLOY +CHRAPRES - AVFISCOPTER-CICAP ,min(CHRAPRES,BCICHR)));
regle 407015:
application : iliad , batch  ;
BCOS = max(0 , min(RDSYVO+0,arr(TX_BASECOTSYN/100*
                                   (TSBV*IND_10V
                                   - BPCOSAV + EXPRV)))) 
       + max(0 , min(RDSYCJ+0,arr(TX_BASECOTSYN/100*
                                   (TSBC*IND_10C
                                   - BPCOSAC + EXPRC)))) 
       + min(RDSYPP+0,arr(TX_BASECOTSYN/100* (somme(i=1..4:TSBi *IND_10i + EXPRi))))  ;

CISYND = arr(TX_REDCOTSYN/100 * BCOS) * (1 - V_CNR) ;

DSYND = RDSYVO + RDSYCJ + RDSYPP ;

ASYND = BCOS * (1-V_CNR) ;

regle 3023108:
application : iliad , batch ;

IAVF = IRE - EPAV + CICORSE + CICULTUR + CIGPA + CIRCMAVFT ;


DIAV[DGFIP][2017] = (BCIGA + IPRECH + IPCHER + IPELUS + RCMAVFT + DIREPARGNE + COD3VE + COD3UV) * (1 - positif(RE168+TAX1649)) + CIRCMAVFT * positif(RE168+TAX1649);


IAV[DGFIP][2017] = (CIDIREPARGNE + IPRECH + CIRECH + IPELUS + CIRCMAVFT + CIGPA + CIEXCEDENT + 0) * (1 - positif(RE168 + TAX1649))
        + CIRCMAVFT * positif(RE168 + TAX1649) ;

IAVFGP = IAV[DGFIP][2017] + CREFAM + CREAPP ;

regle 3023109:
application : iliad , batch ;

[DGFIP][2017]DH = EPAV ;

regle 30231011:
application : iliad , batch  ;
BTANTGECUM   = (V_BTGECUM * (1 - present(DEPMOBIL)) + DEPMOBIL);
BTANTGECUMWL = (V_BTGECUMWL * (1 - present(COD7WD)) + COD7WD);
[DGFIP][2017]GE = max( (   PLAF_GE2 * (1 + BOOL_0AM)
             + PLAF_GE2_PACQAR * (V_0CH + V_0DP)
             + PLAF_GE2_PAC * (V_0CR + V_0CF + V_0DJ + V_0DN)  
              ) - BTANTGECUM
             , 0
             ) ;
BGEDECL = RDTECH + RDEQPAHA ;
BGEPAHA = min(RDEQPAHA , [DGFIP][2017]GE) * (1 - V_CNR);
[DGFIP][2017]GEWL = max(0,[DGFIP][2017]GE + PLAF_GE2 * (1 + BOOL_0AM) -  BGEPAHA - BTANTGECUMWL);

BGTECH = min(RDTECH , [DGFIP][2017]GEWL) * (1 - V_CNR) ;
TOTBGE = BGTECH + BGEPAHA ;
RGEPAHA =  (BGEPAHA * TX25 / 100 ) * (1 - V_CNR) ;
RGTECH = (BGTECH * TX40 / 100 ) * (1 - V_CNR) ;
CIGE = arr (RGTECH + RGEPAHA ) * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;
GECUM = min([DGFIP][2017]GE,BGEPAHA + BGTECH)+BTANTGECUM ;
GECUMWL = max(0,BGTECH + BGEPAHA - min([DGFIP][2017]GE,BGEPAHA + BGTECH)+BTANTGECUMWL) ;
DAIDC = CREAIDE ;
AAIDC = BADCRE * (1-V_CNR) ;
CIADCRE = arr (BADCRE * TX_AIDOMI /100) * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;
regle 30231012:
application : iliad , batch  ;
DLOYIMP = LOYIMP ;
ALOYIMP = DLOYIMP;
CILOYIMP = arr(ALOYIMP*TX_LOYIMP/100) * (1 - positif(RE168 + TAX1649)) ;
regle 30231014:
application : iliad , batch  ;

DDEVDUR = 
  CIBOIBAIL  + COD7SA  + CINRJBAIL  + COD7SB  + CRENRJ  + COD7SC  + TRAMURWC  
 + COD7WB  + CINRJ  + COD7RG  + TRATOIVG  + COD7VH  + CIDE[DGFIP][2017]5  + COD7RH  
 + MATISOSI  + COD7RI  + TRAVITWT  + COD7WU  + MATISOSJ  + COD7RJ  + VOLISO  
 + COD7RK  + PORENT  + COD7RL  + CHAUBOISN  + COD7RN  + POMPESP  + COD7RP  
 + POMPESR  + COD7RR  + CHAUFSOL  + COD7RS  + POMPESQ  + COD7RQ  + ENERGIEST  
 + COD7RT  + DIAGPERF  + COD7TV  + RESCHAL  + COD7TW  + COD7RV  + COD7RW  + COD7RZ  ;

PDEVDUR = max( (   PLAF_DEVDUR * (1 + BOOL_0AM)
                  + PLAF_GE2_PACQAR * (V_0CH+V_0DP)
	          + PLAF_GE2_PAC * (V_0CR+V_0CF+V_0DJ+V_0DN) 
		 ) - (V_BTDEVDUR*(1-present(DEPCHOBAS))+DEPCHOBAS) , 0 );
BQRESS = positif(CIBOIBAIL + CINRJBAIL + CRENRJ + CINRJ + CIDE[DGFIP][2017]5 + MATISOSI + MATISOSJ + VOLISO + PORENT
                                          + CHAUBOISN + POMPESP + POMPESR + CHAUFSOL + POMPESQ + ENERGIEST + DIAGPERF + RESCHAL
                                          + TRAMURWC + TRATOIVG + TRAVITWT) * 1
        + 0;
BQTRAV = positif((present(TRAVITWT)+present(COD7WU)) * (present(TRAMURWC)+present(COD7WB))
                +(present(TRAVITWT)+present(COD7WU)) * (present(TRATOIVG)+present(COD7VH))
                +(present(TRAVITWT)+present(COD7WU)) * (present(CHAUBOISN)+present(COD7RN))
                +(present(TRAVITWT)+present(COD7WU)) * (present(POMPESR)+present(COD7RR)+present(CHAUFSOL)+present(COD7RS))
                +(present(TRAVITWT)+present(COD7WU)) * (present(CIBOIBAIL)+present(COD7SA)+present(CINRJBAIL)+present(COD7SB)
                                                       +present(POMPESP)+present(COD7RP)+present(POMPESQ)+present(COD7RQ)
                                                       +present(ENERGIEST)+present(COD7RT))
                +(present(TRAMURWC)+present(COD7WB)) * (present(TRATOIVG)+present(COD7VH))
                +(present(TRAMURWC)+present(COD7WB)) * (present(CHAUBOISN)+present(COD7RN))
                +(present(TRAMURWC)+present(COD7WB)) * (present(POMPESR)+present(COD7RR)+present(CHAUFSOL)+present(COD7RS))
                +(present(TRAMURWC)+present(COD7WB)) * (present(CIBOIBAIL)+present(COD7SA)+present(CINRJBAIL)+present(COD7SB)
                                                       +present(POMPESP)+present(COD7RP)+present(POMPESQ)+present(COD7RQ)
                                                       +present(ENERGIEST)+present(COD7RT))
                +(present(TRATOIVG)+present(COD7VH)) * (present(CHAUBOISN)+present(COD7RN))
                +(present(TRATOIVG)+present(COD7VH)) * (present(CHAUBOISN)+present(COD7RN))
                +(present(TRATOIVG)+present(COD7VH)) * (present(POMPESR)+present(COD7RR)+present(CHAUFSOL)+present(COD7RS))
                +(present(TRATOIVG)+present(COD7VH)) * (present(CIBOIBAIL)+present(COD7SA)+present(CINRJBAIL)+present(COD7SB)
                                                       +present(POMPESP)+present(COD7RP)+present(POMPESQ)+present(COD7RQ)
                                                       +present(ENERGIEST)+present(COD7RT))
                +(present(CHAUBOISN)+present(COD7RN)) * (present(POMPESR)+present(COD7RR)+present(CHAUFSOL)+present(COD7RS))
                +(present(CHAUBOISN)+present(COD7RN)) * (present(CIBOIBAIL)+present(COD7SA)+present(CINRJBAIL)+present(COD7SB)
                                                       +present(POMPESP)+present(COD7RP)+present(POMPESQ)+present(COD7RQ)
                                                       +present(ENERGIEST)+present(COD7RT))
                +(present(POMPESR)+present(COD7RR)+present(CHAUFSOL)+present(COD7RS)) * (present(CIBOIBAIL)+present(COD7SA)+present(CINRJBAIL)+present(COD7SB)
                                                       +present(POMPESP)+present(COD7RP)+present(POMPESQ)+present(COD7RQ)
                                                       +present(ENERGIEST)+present(COD7RT))
                ) + 0;


PLA[DGFIP][2017]M = null(1-V_REGCO) * arr(  24043 
                                + 2808.5 * min( (NBPT - 1) * 4 , 2 ) * positif( NBPT -1 )
                                + 2210.5 * ( (NBPT - 1.5) * 4 ) * positif( NBPT - 1.5 )
                              );

PLA[DGFIP][2017]MGR = null(5-V_REGCO) * arr( 29058  
                                + 3082 * min( (NBPT - 1) * 4 , 2 ) * positif( NBPT -1 )
                                + 2938.5  * min( (NBPT - 1.5) * 4 , 2 ) * positif( NBPT - 1.5 )
                                + 2210.5  *  ( (NBPT - 2) * 4 ) * positif( NBPT - 2 )
                                );

PLA[DGFIP][2017]GM = positif(null(6-V_REGCO)+null(7-V_REGCO)) * arr( 31843  
                                                        + 3082 * min( (NBPT - 1) * 4 , 4 ) * positif( NBPT -1 )
                                                        + 2624.5  * min( (NBPT - 2) * 4 , 2 ) * positif( NBPT - 2 )
                                                        + 2210.5  *  ( (NBPT - 2.5) * 4 ) * positif( NBPT - 2.5 )
                                                        );


PLA[DGFIP][2017]M = null(1-V_REGCO) * arr(  25005 
                               + 2921 * min( (NBPT - 1) * 4 , 2 ) * positif( NBPT -1 )
                               + 2299 * ( (NBPT - 1.5) * 4 ) * positif( NBPT - 1.5 )
                              );


PLA[DGFIP][2017]MGR = null(5-V_REGCO) * arr( 30220  
                                + 3205.5 * min( (NBPT - 1) * 4 , 2 ) * positif( NBPT -1 )
                                + 3056  * min( (NBPT - 1.5) * 4 , 2 ) * positif( NBPT - 1.5 )
                                + 2299  *  ( (NBPT - 2) * 4 ) * positif( NBPT - 2 )
                                );


PLA[DGFIP][2017]GM = positif(null(6-V_REGCO)+null(7-V_REGCO)) * arr( 33117  
                                                        + 3205.5 * min( (NBPT - 1) * 4 , 4 ) * positif( NBPT -1 )
                                                        + 2729.5  * min( (NBPT - 2) * 4 , 2 ) * positif( NBPT - 2 )
                                                        + 2299  *  ( (NBPT - 2.5) * 4 ) * positif( NBPT - 2.5 )
                                                        );


PLA[DGFIP][2017] = PLA[DGFIP][2017]M + PLA[DGFIP][2017]MGR + PLA[DGFIP][2017]GM;
PLA[DGFIP][2017] = PLA[DGFIP][2017]M + PLA[DGFIP][2017]MGR + PLA[DGFIP][2017]GM;

RESS = positif(positif_ou_nul(PLA[DGFIP][2017] - ((V_BTRFRN2+0)*(1-present(RFRN2))+RFRN2)) 
                + (1-positif_ou_nul(PLA[DGFIP][2017] - ((V_BTRFRN2+0)*(1-present(RFRN2))+RFRN2))) * 
                                      positif_ou_nul(PLA[DGFIP][2017] - ((V_BTRFRN1+0)*(1-present(RFRN1))+RFRN1))+present(COD7WX));

BDEV30 =  min(PDEVDUR,COD7SA  + COD7SB  + COD7SC  + COD7WB  + COD7RG  + COD7VH  + COD7RH  + COD7RI
                    + COD7WU  + COD7RJ  + COD7RK  + COD7RL  + COD7RN  + COD7RP  + COD7RR  + COD7RS
                    + COD7RQ  + COD7RT  + COD7TV  + COD7TW  + COD7RV  + COD7RW  + COD7RZ)
              * positif(positif(BQRESS)*positif(BQTRAV)
                       +positif(BQRESS)*null(BQTRAV)*null(RESS)
                       +positif(BQRESS)*null(BQTRAV)*positif(RESS)
                       +positif(BQRESS)*null(BQTRAV)*positif(RESS)*present(CRECHOBOI)
                       +null(BQRESS))
                  ;

BDEV25 =  min(max(0,PDEVDUR-BDEV30),CIBOIBAIL  + CINRJBAIL  + TRAMURWC  + TRATOIVG  + TRAVITWT
                                  + CHAUBOISN  + POMPESP  + POMPESR  + CHAUFSOL  + POMPESQ  + ENERGIEST)
              * positif(positif(BQRESS)*positif(BQTRAV));

BDEV15 =  min(max(0,PDEVDUR-BDEV30-BDEV25),CRENRJ  + CINRJ  + CIDE[DGFIP][2017]5  + MATISOSI  + MATISOSJ
                                         + VOLISO  + PORENT  + DIAGPERF  + RESCHAL)
              * positif(positif(BQRESS)*positif(BQTRAV))

        + min(max(0,PDEVDUR-BDEV30),( CIBOIBAIL  + CINRJBAIL  + CRENRJ  + TRAMURWC  + CINRJ  + TRATOIVG  + CIDE[DGFIP][2017]5  + MATISOSI  + TRAVITWT
                                    + MATISOSJ  + VOLISO  + PORENT  + CHAUBOISN  + POMPESP  + POMPESR  + CHAUFSOL  + POMPESQ  + ENERGIEST
                                    + DIAGPERF  + RESCHAL) * (1-present(COD7WX)) + present(COD7WX) * COD7WX) 
              *positif(positif(BQRESS)*null(BQTRAV)*positif(RESS)*(1-present(CRECHOBOI)))

        + min(max(0,PDEVDUR-BDEV30),(CIBOIBAIL  + CINRJBAIL  + CRENRJ  + TRAMURWC  + CINRJ  + TRATOIVG  + CIDE[DGFIP][2017]5
                                   + MATISOSI  + CHAUBOISN  + POMPESP  + POMPESR  + CHAUFSOL  + POMPESQ  + ENERGIEST  
                                   + DIAGPERF  + RESCHAL)* (1-present(COD7WX)) + present(COD7WX) * COD7WX)
              * positif(positif(BQRESS)*null(BQTRAV)*positif(RESS)*present(CRECHOBOI))
          ;

ADEVDUR = BDEV30 + BDEV25 + BDEV15; 

CIDEVDUR = arr( BDEV30 * TX30/100
               +BDEV25 * TX25/100
               +BDEV15 * TX15/100
      	      )  * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;

DEVDURCUM = ADEVDUR + (V_BTDEVDUR*(1-present(DEPCHOBAS))+DEPCHOBAS);
regle 30231015:
application : iliad , batch  ;
DTEC = RISKTEC;
ATEC = positif(DTEC) * DTEC;
CITEC = arr (ATEC * TX40/100);
regle 30231016:
application : iliad , batch  ;
DPRETUD = PRETUD + PRETUDANT ;
APRETUD = max(min(PRETUD,LIM_PRETUD) + min(PRETUDANT,LIM_PRETUD*CASEPRETUD),0) * (1-V_CNR) ;

CIPRETUD = arr(APRETUD*TX_PRETUD/100) * (1 - positif(RE168 + TAX1649)) * (1-V_CNR) ;
regle 30231017:
application : iliad , batch  ;

EM7 = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - V_ANREV)))
      + (1 - positif(somme(i=0..7:V_0Fi) + 0)) * V_0CF ;

EM7QAR = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - V_ANREV)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - V_ANREV)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

BRFG = min(RDGAR[DGFIP][2017],PLAF_REDGARD) + min(RDGAR[DGFIP][2017],PLAF_REDGARD)
       + min(RDGARD3,PLAF_REDGARD) + min(RDGARD4,PLAF_REDGARD)
       + min(RDGAR[DGFIP][2017]QAR,PLAF_REDGARDQAR) + min(RDGAR[DGFIP][2017]QAR,PLAF_REDGARDQAR)
       + min(RDGARD3QAR,PLAF_REDGARDQAR) + min(RDGARD4QAR,PLAF_REDGARDQAR)
       ;
RFG = arr ( (BRFG) * TX_REDGARD /100 ) * (1 -V_CNR);
DGARD = somme(i=1..4:RDGARDi)+somme(i=1..4:RDGARDiQAR);
AGARD = (BRFG) * (1-V_CNR) ;
CIGARD = RFG * (1 - positif(RE168 + TAX1649)) ;
regle 30231018:
application : iliad , batch  ;

PREHAB = PREHABT + PREHABTN + PREHABTN1 + PREHABT1 + PREHABT2 + PREHABTN2 + PREHABTVT ;

BCIHP = max(( PLAFHABPRIN * (1 + BOOL_0AM) * (1+positif(V_0AP+V_0AF+V_0CG+V_0CI+V_0CR))
                 + (PLAFHABPRINENF/2) * (V_0CH + V_0DP)
                 + PLAFHABPRINENF * (V_0CR + V_0CF + V_0DJ + V_0DN)
                  )
             ,0);

BCIHABPRIN1 = min(BCIHP , PREHABT) * (1 - V_CNR) ;
BCIHABPRIN2 = min(max(0,BCIHP-BCIHABPRIN1),PREHABT1) * (1 - V_CNR);
BCIHABPRIN3 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN2),PREHABTN) * (1 - V_CNR);
BCIHABPRIN4 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN2-BCIHABPRIN3),PREHABTN1) * (1 - V_CNR);
BCIHABPRIN5 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN2-BCIHABPRIN3-BCIHABPRIN4),PREHABT2) * (1 - V_CNR);
BCIHABPRIN6 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN2-BCIHABPRIN3-BCIHABPRIN4-BCIHABPRIN5),PREHABTN2) * (1 - V_CNR);
BCIHABPRIN7 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN2-BCIHABPRIN3-BCIHABPRIN4-BCIHABPRIN5-BCIHABPRIN6),PREHABTVT) * (1 - V_CNR);

BCIHABPRIN = BCIHABPRIN1 + BCIHABPRIN2 + BCIHABPRIN3 + BCIHABPRIN4 + BCIHABPRIN5 + BCIHABPRIN6 + BCIHABPRIN7 ;

CIHABPRIN = arr((BCIHABPRIN1 * TX40/100)
                + (BCIHABPRIN2 * TX40/100)
                + (BCIHABPRIN3 * TX30/100)
                + (BCIHABPRIN4 * TX25/100)
                + (BCIHABPRIN5 * TX20/100)
                + (BCIHABPRIN6 * TX15/100)
                + (BCIHABPRIN7 * TX10/100))
                * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);
regle 30231020:
application : iliad , batch  ;

BDCIFORET  = RDFORESTRA + SINISFORET + COD7UA + COD7UB + RDFORESTGES + COD7UI;
BCIFORETUA = max(0,min(COD7UA,max(0,PLAF_FOREST1 * (1 + BOOL_0AM)))) * (1-V_CNR);
BCIFORETUB = max(0,min(COD7UB,max(0,PLAF_FOREST1 * (1 + BOOL_0AM)-BCIFORETUA))) * (1-V_CNR);
BCIFORETUP = max(0,min(RDFORESTRA,PLAF_FOREST1 * (1 + BOOL_0AM)-BCIFORETUA-BCIFORETUB)) * (1-V_CNR);
BCIFORETUT = max(0,min(SINISFORET,max(0,PLAF_FOREST1 * (1 + BOOL_0AM)-BCIFORETUA-BCIFORETUB-BCIFORETUP))) * (1-V_CNR);
BCIFORETUI = max(0,min(COD7UI,max(0,PLAF_FOREST2 * (1 + BOOL_0AM))))  * (1-V_CNR);
BCIFORETUQ = max(0,min(RDFORESTGES, PLAF_FOREST2 * (1 + BOOL_0AM)-BCIFORETUI)) * (1-V_CNR);
BCIFORET  = BCIFORETUP+BCIFORETUT+BCIFORETUA+BCIFORETUB+BCIFORETUQ+BCIFORETUI;
CIFORET = arr(BCIFORETUP * TX18/100 + BCIFORETUT * TX18/100 + BCIFORETUA * TX25/100 + BCIFORETUB * TX25/100
             + BCIFORETUQ * TX18/100 + BCIFORETUI * TX25/100); 
regle 30231025:
application : iliad , batch  ;

REPCIFAD = max(0,COD7UA - PLAF_FOREST1 * (1+ BOOL_0AM)) * (1-V_CNR);
REPCIFADSIN = (positif_ou_nul(COD7UA - PLAF_FOREST1 * (1+ BOOL_0AM)) * COD7UB
            + (1-positif_ou_nul(COD7UA - PLAF_FOREST1 * (1+ BOOL_0AM))) * max(0,COD7UB - (PLAF_FOREST1 * (1+ BOOL_0AM) - COD7UA))) * (1-V_CNR);
REPCIF = (positif_ou_nul(COD7UA + COD7UB - PLAF_FOREST1 * (1+ BOOL_0AM)) * RDFORESTRA
            + (1-positif_ou_nul(COD7UA + COD7UB- PLAF_FOREST1 * (1+ BOOL_0AM))) * max(0,RDFORESTRA - (PLAF_FOREST1 * (1+ BOOL_0AM) - COD7UA-COD7UB))) * (1-V_CNR);

REPCIFSIN = (positif_ou_nul(COD7UA + COD7UB + RDFORESTRA - PLAF_FOREST1 * (1+ BOOL_0AM)) * SINISFORET
            + (1-positif_ou_nul(COD7UA + COD7UB+ RDFORESTRA- PLAF_FOREST1 * (1+ BOOL_0AM))) * max(0,SINISFORET - (PLAF_FOREST1 * (1+ BOOL_0AM) - COD7UA-COD7UB-RDFORESTRA))) * (1-V_CNR);
 
regle 302311:
application : iliad , batch ;
CICSG = min( CSGC , arr( IPPNCS * T_CSG/100 ));
CICSGAPS = min( CSGCAPS , arr( IPPNCS * T_CSG/100 ));
CIRDS = min( RDSC , arr( (IPPNCS+REVCSXA+REVCSXB+REVCSXC+REVCSXD+REVCSXE+COD8XI+COD8XJ) * T_RDS/100 ));
CIRDSAPS = min( RDSCAPS , arr( (IPPNCS+REVCSXA+REVCSXB+REVCSXC+REVCSXD+REVCSXE) * T_RDS/100 ));
CIPRS = min( PRSC , arr( IPPNCS * T_PREL_SOC/100 ));
CIPRSAPS = min( PRSCAPS , arr( IPPNCS * T_PREL_SOC/100 ));

CIRSE1 = min( RSE1 , arr( REVCSXA * TX075/100 ));

RSE8TV = arr(BRSE8TV * TXTV/100) * (1 - positif(ANNUL2042));
RSE8SA = arr(BRSE8SA * TXTV/100) * (1 - positif(ANNUL2042));
CIRSE8TV = min( RSE8TV , arr( REVCSXC * TX066/100 )) ;
CIRSE8SA = min( RSE8SA , arr(COD8XI * TX066/100 )) ;
CIRSE2 = CIRSE8TV + CIRSE8SA ; 

CIRSE3 = min( RSE3 , arr( REVCSXD * TX062/100 ));

RSE8TX = arr(BRSE8TX * TXTX/100) * (1 - positif(ANNUL2042));
RSE8SB = arr(BRSE8SB * TXTX/100) * (1 - positif(ANNUL2042));
CIRSE8TX = min( RSE8TX , arr( REVCSXE * TX038/100 )) ;
CIRSE8SB = min( RSE8SB , arr( COD8XJ * TX038/100 ));
CIRSE4 = CIRSE8TX + CIRSE8SB ;

CIRSE5 = min( RSE5 , arr( REVCSXB * TX075/100 ));

CIRSETOT = CIRSE1 + CIRSE2 + CIRSE3 + CIRSE4 + CIRSE5;



regle 30400:
application : iliad , batch  ;
PPE_DATE_DEB = positif(V_0AV+0) * positif(V_0AZ+0) * (V_0AZ+0)
              + positif(DATRETETR+0) * (DATRETETR+0) * null(V_0AZ+0) ;

PPE_DATE_FIN = positif(BOOL_0AM) * positif(V_0AZ+0) * (V_0AZ+0)
              + positif(DATDEPETR+0) * (DATDEPETR+0) * null(V_0AZ+0) ;
regle 30500:
application : iliad , batch  ;
PPE_DEBJJMMMM =  PPE_DATE_DEB + (01010000+V_ANREV) * null(PPE_DATE_DEB+0);
PPE_DEBJJMM = arr( (PPE_DEBJJMMMM - V_ANREV)/10000);
PPE_DEBJJ =  inf(PPE_DEBJJMM/100);
PPE_DEBMM =  PPE_DEBJJMM -  (PPE_DEBJJ*100);
PPE_DEBRANG= PPE_DEBJJ 
             + (PPE_DEBMM - 1 ) * 30;
regle 30501:
application : iliad , batch  ;
PPE_FINJJMMMM =  PPE_DATE_FIN + (30120000+V_ANREV) * null(PPE_DATE_FIN+0);
PPE_FINJJMM = arr( (PPE_FINJJMMMM - V_ANREV)/10000);
PPE_FINJJ =  inf(PPE_FINJJMM/100);
PPE_FINMM =  PPE_FINJJMM -  (PPE_FINJJ*100);
PPE_FINRANG= PPE_FINJJ 
             + (PPE_FINMM - 1 ) * 30
             - 1 * positif (PPE_DATE_FIN);
regle 30503:
application : iliad , batch  ;
PPE_DEBUT = PPE_DEBRANG ;
PPE_FIN   = PPE_FINRANG ;
PPENBJ = max(1, arr(min(PPENBJAN , PPE_FIN - PPE_DEBUT + 1))) ;
regle 30508:
application : iliad , batch  ;
PPETX1 = PPE_TX1 ;
PPETX2 = PPE_TX2 ;
PPETX3 = PPE_TX3 ;
regle 30510:
application : iliad , batch  ;
PPE_BOOL_ACT_COND = positif(


   positif ( TSHALLOV ) 
 + positif ( TSHALLOC ) 
 + positif ( TSHALLO1 ) 
 + positif ( TSHALLO2 ) 
 + positif ( TSHALLO3 ) 
 + positif ( TSHALLO4 ) 
 + positif ( GL[DGFIP][2017]V ) 
 + positif ( GL[DGFIP][2017]V ) 
 + positif ( GLD3V ) 
 + positif ( GL[DGFIP][2017]C ) 
 + positif ( GL[DGFIP][2017]C ) 
 + positif ( GLD3C ) 
 + positif ( BPCOSAV ) 
 + positif ( BPCOSAC ) 
 + positif ( TSASSUV ) 
 + positif ( TSASSUC ) 
 + positif( CARTSV ) * positif( CARTSNBAV )
 + positif( CARTSC ) * positif( CARTSNBAC )
 + positif( CARTS[DGFIP][2017] ) * positif( CARTSNBA[DGFIP][2017] )
 + positif( CARTS[DGFIP][2017] ) * positif( CARTSNBA[DGFIP][2017] )
 + positif( CARTSP3 ) * positif( CARTSNBAP3 )
 + positif( CARTSP4 ) * positif( CARTSNBAP4 )
 + positif ( FEXV ) 
 + positif ( BAFV ) 
 + positif ( BAFPVV ) 
 + positif ( BAEXV ) 
 + positif ( BACREV ) + positif ( 4BACREV )
 + positif  (BACDEV * (1 - positif(ART1731BIS)) ) 
 + positif ( BAHEXV ) 
 + positif ( BAHREV ) + positif ( 4BAHREV )
 + positif ( BAHDEV * (1 - positif(ART1731BIS) )) 
 + positif ( MIBEXV ) 
 + positif ( MIBVENV ) 
 + positif ( MIBPRESV ) 
 + positif ( MIBPVV ) 
 + positif ( BICEXV ) 
 + positif ( BICNOV ) 
 + positif ( BICDNV * (1 - positif(ART1731BIS) ) ) 
 + positif ( BIHEXV ) 
 + positif ( BIHNOV ) 
 + positif ( BIHDNV * (1 - positif(ART1731BIS) )) 
 + positif ( FEXC ) 
 + positif ( BAFC ) 
 + positif ( BAFPVC ) 
 + positif ( BAEXC ) 
 + positif ( BACREC ) + positif ( 4BACREC )
 + positif (BACDEC * (1 - positif(ART1731BIS) ) ) 
 + positif ( BAHEXC ) 
 + positif ( BAHREC ) + positif ( 4BAHREC )
 + positif ( BAHDEC * (1 - positif(ART1731BIS) ) )  
 + positif ( MIBEXC ) 
 + positif ( MIBVENC ) 
 + positif ( MIBPRESC ) 
 + positif ( MIBPVC ) 
 + positif ( BICEXC ) 
 + positif ( BICNOC ) 
 + positif ( BICDNC * (1 - positif(ART1731BIS) ) ) 
 + positif ( BIHEXC ) 
 + positif ( BIHNOC ) 
 + positif ( BIHDNC * (1 - positif(ART1731BIS) ))  
 + positif ( FEXP ) 
 + positif ( BAFP ) 
 + positif ( BAFPVP ) 
 + positif ( BAEXP ) 
 + positif ( BACREP ) + positif ( 4BACREP )
 + positif  (BACDEP * (1 - positif(ART1731BIS))) 
 + positif ( BAHEXP ) 
 + positif ( BAHREP ) + positif ( 4BAHREP )
 + positif ( BAHDEP * (1 - positif(ART1731BIS) )) 
 + positif ( MIBEXP ) 
 + positif ( MIBVENP ) 
 + positif ( MIBPRESP ) 
 + positif ( BICEXP ) 
 + positif ( MIBPVP ) 
 + positif ( BICNOP ) 
 + positif ( BICDNP * (1 - positif(ART1731BIS) )) 
 + positif ( BIHEXP ) 
 + positif ( BIHNOP ) 
 + positif ( BIHDNP * (1 - positif(ART1731BIS) ) )
 + positif ( BNCPROEXV ) 
 + positif ( BNCPROV ) 
 + positif ( BNCPROPVV ) 
 + positif ( BNCEXV ) 
 + positif ( BNCREV ) 
 + positif ( BNCDEV * (1 - positif(ART1731BIS) )) 
 + positif ( BNHEXV ) 
 + positif ( BNHREV ) 
 + positif ( BNHDEV * (1 - positif(ART1731BIS) ) ) 
 + positif ( BNCCRV ) 
 + positif ( BNCPROEXC ) 
 + positif ( BNCPROC ) 
 + positif ( BNCPROPVC ) 
 + positif ( BNCEXC ) 
 + positif ( BNCREC ) 
 + positif ( BNCDEC * (1 - positif(ART1731BIS) )) 
 + positif ( BNHEXC ) 
 + positif ( BNHREC ) 
 + positif ( BNHDEC * (1 - positif(ART1731BIS))) 
 + positif ( BNCCRC ) 
 + positif ( BNCPROEXP ) 
 + positif ( BNCPROP ) 
 + positif ( BNCPROPVP ) 
 + positif ( BNCEXP ) 
 + positif ( BNCREP ) 
 + positif ( BNCDEP * (1 - positif(ART1731BIS) )) 
 + positif ( BNHEXP ) 
 + positif ( BNHREP ) 
 + positif ( BNHDEP * (1 - posi
