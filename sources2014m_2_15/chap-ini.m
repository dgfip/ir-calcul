#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle irisf 1:
application : bareme,batch,iliad;
BIDON=1;

regle irisf 1000140:
application :  iliad;
APPLI_GP      = 0 ;
APPLI_ILIAD   = 1 ;
APPLI_BATCH   = 0 ;
regle irisf 1000150:
application :  batch;
APPLI_GP      = 0 ;
APPLI_ILIAD   = 0 ;
APPLI_BATCH   = 1 ;
regle 1000717:
application : batch, iliad ;
SOMMEA71701 = positif(CELLIERJA) + positif(CELLIERJB) + positif(CELLIERJD) + positif(CELLIERJE)
	     + positif(CELLIERJF) + positif(CELLIERJG) + positif(CELLIERJH) + positif(CELLIERJJ)
	     + positif(CELLIERJK) + positif(CELLIERJL) + positif(CELLIERJM) + positif(CELLIERJN)
	     + positif(CELLIERJO) + positif(CELLIERJP) + positif(CELLIERJQ) + positif(CELLIERJR) 
	     + 0 ;

SOMMEA71702 = positif(CELLIERNA) + positif(CELLIERNB) + positif(CELLIERNC) + positif(CELLIERND)
             + positif(CELLIERNE) + positif(CELLIERNF) + positif(CELLIERNG) + positif(CELLIERNH) 
	     + positif(CELLIERNI) + positif(CELLIERNJ) + positif(CELLIERNK) + positif(CELLIERNL)
	     + positif(CELLIERNM) + positif(CELLIERNN) + positif(CELLIERNO) + positif(CELLIERNP) 
	     + positif(CELLIERNQ) + positif(CELLIERNR) + positif(CELLIERNS) + positif(CELLIERNT) 
	     + 0 ;

regle 1000718:
application : batch, iliad ;

SOMMEA718 = (

   present( BAFV ) + (1 - null( V_FORVA+0 ))
 + present( BAFORESTV ) + present( BAFPVV ) + present( BA[DGFIP][2017]AV ) 
 + present( BAFC ) + (1 - null( V_FORCA+0 ))
 + present( BAFORESTC ) + present( BAFPVC ) + present( BA[DGFIP][2017]AC ) 
 + present( BAFP ) + (1 - null( V_FORPA+0 ))
 + present( BAFORESTP ) + present( BAFPVP ) + present( BA[DGFIP][2017]AP ) 
 + present( BACREV ) + present( 4BACREV ) + present( BA1AV ) + present( BACDEV ) 
 + present( BACREC ) + present( 4BACREC ) + present( BA1AC ) + present( BACDEC ) 
 + present( BACREP ) + present( 4BACREP ) + present( BA1AP ) + present( BACDEP ) 
 + present( BAHREV ) + present( 4BAHREV ) + present( BAHDEV ) 
 + present( BAHREC ) + present( 4BAHREC ) + present( BAHDEC ) 
 + present( BAHREP ) + present( 4BAHREP ) + present( BAHDEP ) 

 + present( MIBVENV ) + present( MIBPRESV ) + present( MIBPVV ) + present( MIB1AV ) + present( MIBDEV ) + present( BICPMVCTV )
 + present( MIBVENC ) + present( MIBPRESC ) + present( MIBPVC ) + present( MIB1AC ) + present( MIBDEC ) + present( BICPMVCTC )
 + present( MIBVENP ) + present( MIBPRESP ) + present( MIBPVP ) + present( MIB1AP ) + present( MIBDEP ) + present( BICPMVCTP )
 + present( BICNOV ) + present( LOCPROCGAV ) + present( B[DGFIP][2017]AV ) + present( BICDNV ) + present( LOCDEFPROCGAV )
 + present( BICNOC ) + present( LOCPROCGAC ) + present( B[DGFIP][2017]AC ) + present( BICDNC ) + present( LOCDEFPROCGAC )
 + present( BICNOP ) + present( LOCPROCGAP ) + present( B[DGFIP][2017]AP ) + present( BICDNP ) + present( LOCDEFPROCGAP )
 + present( BIHNOV ) + present( LOCPROV ) + present( BIHDNV ) + present( LOCDEFPROV )
 + present( BIHNOC ) + present( LOCPROC ) + present( BIHDNC ) + present( LOCDEFPROC )
 + present( BIHNOP ) + present( LOCPROP ) + present( BIHDNP ) + present( LOCDEFPROP )

 + present( MIBMEUV ) + present( MIBGITEV ) + present( LOCGITV ) + present( MIBNPVENV ) + present( MIBNPPRESV ) 
 + present( MIBNPPVV ) + present( MIBN[DGFIP][2017]AV ) + present( MIBNPDEV ) 
 + present( MIBMEUC ) + present( MIBGITEC ) + present( LOCGITC ) + present( MIBNPVENC ) + present( MIBNPPRESC ) 
 + present( MIBNPPVC ) + present( MIBN[DGFIP][2017]AC ) + present( MIBNPDEC ) 
 + present( MIBMEUP ) + present( MIBGITEP ) + present( LOCGITP ) + present( MIBNPVENP ) + present( MIBNPPRESP ) 
 + present( MIBNPPVP ) + present( MIBN[DGFIP][2017]AP ) + present( MIBNPDEP ) 
 + present( MIBNPDCT ) 
 + present( BICREV ) + present( LOCNPCGAV ) + present( LOCGITCV ) + present( B[DGFIP][2017]AV ) + present( BICDEV ) + present( LOCDEFNPCGAV )
 + present( BICREC ) + present( LOCNPCGAC ) + present( LOCGITCC ) + present( B[DGFIP][2017]AC ) + present( BICDEC ) + present( LOCDEFNPCGAC )
 + present( BICREP ) + present( LOCNPCGAPAC ) + present( LOCGITCP ) + present( B[DGFIP][2017]AP ) + present( BICDEP ) + present( LOCDEFNPCGAPAC )
 + present( BICHREV ) + present( LOCNPV ) + present( LOCGITHCV ) + present( BICHDEV ) + present( LOCDEFNPV )
 + present( BICHREC ) + present( LOCNPC ) + present( LOCGITHCC ) + present( BICHDEC ) + present( LOCDEFNPC )
 + present( BICHREP ) + present( LOCNPPAC ) + present( LOCGITHCP ) + present( BICHDEP ) + present( LOCDEFNPPAC )

 + present( BNCPROV ) + present( BNCPROPVV ) + present( BNCPRO1AV ) + present( BNCPRODEV ) + present( BNCPMVCTV )
 + present( BNCPROC ) + present( BNCPROPVC ) + present( BNCPRO1AC ) + present( BNCPRODEC ) + present( BNCPMVCTC )
 + present( BNCPROP ) + present( BNCPROPVP ) + present( BNCPRO1AP ) + present( BNCPRODEP ) + present( BNCPMVCTP )
 + present( BNCREV ) + present( BN1AV ) + present( BNCDEV ) 
 + present( BNCREC ) + present( BN1AC ) + present( BNCDEC ) 
 + present( BNCREP ) + present( BN1AP ) + present( BNCDEP ) 
 + present( BNHREV ) + present( BNHDEV ) 
 + present( BNHREC ) + present( BNHDEC ) 
 + present( BNHREP ) + present( BNHDEP ) 

 + present( BNCNPV ) + present( BNCNPPVV ) + present( BNCN[DGFIP][2017]AV ) + present( BNCNPDEV ) 
 + present( BNCNPC ) + present( BNCNPPVC ) + present( BNCN[DGFIP][2017]AC ) + present( BNCNPDEC ) 
 + present( BNCNPP ) + present( BNCNPPVP ) + present( BNCN[DGFIP][2017]AP ) + present( BNCNPDEP ) 
 + present( BNCNPDCT ) 
 + present ( BNCAABV ) + present( ANOCEP ) + present( PVINVE ) 
 + present( INVENTV ) + present ( BNCAADV ) + present( DNOCEP ) 
 + present ( BNCAABC ) + present( ANOVEP ) + present( PVINCE ) 
 + present( INVENTC ) + present ( BNCAADC ) + present( DNOCEPC )
 + present ( BNCAABP ) + present( ANOPEP ) + present( PVINPE ) 
 + present ( INVENTP ) + present ( BNCAADP ) + present( DNOCEPP )
 + 0
            ) ;

regle 1000719:
application : batch, iliad ;

SOMMEA719 = (

   present( BAEXV ) + present ( BACREV ) + present( 4BACREV ) + present ( BA1AV ) + present ( BACDEV ) 
 + present( BAEXC ) + present ( BACREC ) + present( 4BACREC ) + present ( BA1AC ) + present ( BACDEC ) 
 + present( BAEXP ) + present ( BACREP ) + present( 4BACREP ) + present ( BA1AP ) + present ( BACDEP ) 
 + present( BAHEXV ) + present ( BAHREV ) + present( 4BAHREV ) + present ( BAHDEV ) 
 + present( BAHEXC ) + present ( BAHREC ) + present( 4BAHREC ) + present ( BAHDEC ) 
 + present( BAHEXP ) + present ( BAHREP ) + present( 4BAHREP ) + present ( BAHDEP ) 

 + present( BICEXV ) + present ( BICNOV ) + present ( LOCPROCGAV )
 + present ( B[DGFIP][2017]AV ) + present ( BICDNV ) + present ( LOCDEFPROCGAV )
 + present( BICEXC ) + present ( BICNOC ) + present ( LOCPROCGAC )
 + present ( B[DGFIP][2017]AC ) + present ( BICDNC ) + present ( LOCDEFPROCGAC )
 + present( BICEXP ) + present ( BICNOP ) + present ( LOCPROCGAP )
 + present ( B[DGFIP][2017]AP ) + present ( BICDNP ) + present ( LOCDEFPROCGAP )
 + present( BIHEXV ) + present ( BIHNOV ) + present ( LOCPROV )
 + present ( BIHDNV ) + present ( LOCDEFPROV )
 + present( BIHEXC ) + present ( BIHNOC ) + present ( LOCPROC )
 + present ( BIHDNC ) + present ( LOCDEFPROC )
 + present( BIHEXP ) + present ( BIHNOP ) + present ( LOCPROP )
 + present ( BIHDNP ) + present ( LOCDEFPROP )

 + present( BICNPEXV ) + present ( BICREV ) + present( LOCNPCGAV )
 + present ( B[DGFIP][2017]AV ) + present ( BICDEV ) + present( LOCDEFNPCGAV )
 + present( BICNPEXC ) + present ( BICREC ) + present( LOCNPCGAC )
 + present ( B[DGFIP][2017]AC ) + present ( BICDEC ) + present( LOCDEFNPCGAC )
 + present( BICNPEXP ) + present ( BICREP ) + present( LOCNPCGAPAC )
 + present ( B[DGFIP][2017]AP ) + present ( BICDEP ) + present( LOCDEFNPCGAPAC )
 + present( BICNPHEXV ) + present ( BICHREV ) + present ( LOCNPV )
 + present ( BICHDEV ) + present ( LOCDEFNPV )
 + present( BICNPHEXC ) + present ( BICHREC ) + present ( LOCNPC )
 + present ( BICHDEC ) + present ( LOCDEFNPC )
 + present( BICNPHEXP ) + present ( BICHREP ) + present ( LOCNPPAC )
 + present ( BICHDEP ) + present ( LOCDEFNPPAC )

 + present( BNCEXV ) + present ( BNCREV ) + present ( BN1AV ) + present ( BNCDEV ) 
 + present( BNCEXC ) + present ( BNCREC ) + present ( BN1AC ) + present ( BNCDEC ) 
 + present( BNCEXP ) + present ( BNCREP ) + present ( BN1AP ) + present ( BNCDEP ) 
 + present( BNHEXV ) + present ( BNHREV ) + present ( BNHDEV ) 
 + present( BNHEXC ) + present ( BNHREC ) + present ( BNHDEC ) 
 + present( BNHEXP ) + present ( BNHREP ) + present ( BNHDEP )
 + present( XHONOAAV ) + present( XHONOV ) 
 + present( XHONOAAC ) + present( XHONOC ) 
 + present( XHONOAAP ) + present( XHONOP )

 + present ( BNCNPREXAAV ) + present ( BNCAABV )   + present ( BNCAADV )  + present ( BNCNPREXV ) 
 + present( ANOCEP ) + present( DNOCEP ) + present( PVINVE ) + present( INVENTV )
 + present ( BNCNPREXAAC ) + present ( BNCAABC ) + present ( BNCAADC ) + present ( BNCNPREXC )
 + present( ANOVEP ) + present( DNOCEPC ) + present( PVINCE ) + present( INVENTC )
 + present ( BNCNPREXAAP ) + present ( BNCAABP ) + present ( BNCAADP ) + present ( BNCNPREXP )
 + present( ANOPEP ) + present( DNOCEPP ) + present( PVINPE ) + present( INVENTP )

 + 0
        ) ;

regle 1000530:
application : batch, iliad;

SOMMEA030 =     
                somme(i=1..4: positif(TSHALLOi) + positif(ALLOi)
		+ positif(CARTSPi) + positif(REMPLAPi)
		+ positif(CARTSNBAPi) + positif(REMPLANBPi)
                + positif(PRBRi)
		+ positif(CARPEPi) + positif(CARPENBAPi)
                + positif(PALIi) + positif(FRNi) + positif(PPETPPi) + positif(PPENHPi)
		+ positif(PENSALPi) + positif(PENSALNBPi)
		)
 + positif(RSAPAC1) + positif(RSAPAC2)
 + positif(FEXP)  + positif(BAFP)  + positif(BAFORESTP) + positif(BAFPVP)  + positif(BA[DGFIP][2017]AP)
 + positif(BAEXP)  + positif(BACREP) + positif(4BACREP)  
 + positif(BA1AP)  + positif(BACDEP * (1 - positif(ART1731BIS) ))
 + positif(BAHEXP)  + positif(BAHREP) + positif(4BAHREP) 
 + positif(BAHDEP * (1 - positif(ART1731BIS) )) 
 + positif(MIBEXP) + positif(MIBVENP) + positif(MIBPRESP)  + positif(MIBPVP)  + positif(MIB1AP)  + positif(MIBDEP)
 + positif(BICPMVCTP) + positif(BICEXP) + positif(BICNOP) + positif(B[DGFIP][2017]AP)  
 + positif(BICDNP * (1 - positif(ART1731BIS) )) 
 + positif(BIHEXP) + positif(BIHNOP) + positif(BIHDNP * (1 - positif(ART1731BIS) ))  
 + positif(MIBNPEXP)  + positif(MIBNPVENP)  + positif(MIBNPPRESP)  + positif(MIBNPPVP)  + positif(MIBN[DGFIP][2017]AP)  + positif(MIBNPDEP)
 + positif(BICNPEXP)  + positif(BICREP) + positif(B[DGFIP][2017]AP)  + positif(min(BICDEP,BICDE[DGFIP][2017]731+0) * positif(ART1731BIS) + BICDEP * (1 - ART1731BIS))  
 + positif(BICNPHEXP) + positif(BICHREP) + positif(min(BICHDEP,BICHDE[DGFIP][2017]731+0) * positif(ART1731BIS) + BICHDEP * (1 - ART1731BIS)) 
 + positif(BNCPROEXP)  + positif(BNCPROP)  + positif(BNCPROPVP)  + positif(BNCPRO1AP)  + positif(BNCPRODEP) + positif(BNCPMVCTP)
 + positif(BNCEXP)  + positif(BNCREP) + positif(BN1AP) 
 + positif(BNCDEP * (1 - positif(ART1731BIS) ))
 + positif(BNHEXP)  + positif(BNHREP)  + positif(BNHDEP * (1 - positif(ART1731BIS) )) + positif(BNCCRP)
 + positif(BNCNPP)  + positif(BNCNPPVP)  + positif(BNCN[DGFIP][2017]AP)  + positif(BNCNPDEP)
 + positif(ANOPEP) + positif(PVINPE) + positif(INVENTP) + positif(min(DNOCEPP,DNOCEP[DGFIP][2017]731+0) * positif(ART1731BIS) + DNOCEPP * (1 - ART1731BIS)) + positif(BNCCRFP)
 + positif(BNCAABP) + positif(min(BNCAADP,BNCAAD[DGFIP][2017]731+0) * positif(ART1731BIS) + BNCAADP * (1 - ART1731BIS))
 + positif(RCSP) + positif(PPEACP) + positif(PPENJP)
 + positif(BAPERPP) + positif(BIPERPP) 
 + positif(PERPP) + positif(PERP_COTP) + positif(RACCOTP) + positif(PLAF_PERPP)
 + somme(i=1..4: positif(PEBFi))
 + positif( COT[DGFIP][2017] ) + positif( COT[DGFIP][2017] ) + positif( COTF3 ) + positif( COTF4 )
 + positif (BNCNPREXAAP) + positif (BNCNPREXP)
 + positif(AUTOBICVP) + positif(AUTOBICPP) 
 + positif(AUTOBNCP) + positif(LOCPROCGAP) 
 + positif(LOCDEFPROCGAP * (1 - positif(ART1731BIS) ))
 + positif(LOCPROP) + positif(LOCDEFPROP * (1 - positif(ART1731BIS) )) 
 + positif(LOCNPCGAPAC) + positif(LOCGITCP) + positif(LOCGITHCP) 
 + positif(min(LOCDEFNPCGAPAC,LOCDEFNPCGAPAC1731+0) * positif(ART1731BIS) + LOCDEFNPCGAPAC * (1 - ART1731BIS))
 + positif(LOCNPPAC) + positif(min(LOCDEFNPPAC,LOCDEFNPPAC1731+0) * positif(ART1731BIS) + LOCDEFNPPAC * (1 - ART1731BIS)) 
 + positif(XHONOAAP) + positif(XHONOP) + positif(XSPENPP)
 + positif(BANOCGAP) + positif(MIBMEUP) + positif(MIBGITEP) + positif(LOCGITP) 
 + positif(SALEXT1) + positif(CO[DGFIP][2017]CD) + positif(CO[DGFIP][2017]CE) + positif(PPEXT1) + positif(CO[DGFIP][2017]CH)
 + positif(SALEXT2) + positif(CO[DGFIP][2017]DD) + positif(CO[DGFIP][2017]DE) + positif(PPEXT2) + positif(CO[DGFIP][2017]DH)
 + positif(SALEXT3) + positif(CO[DGFIP][2017]ED) + positif(CO[DGFIP][2017]EE) + positif(PPEXT3) + positif(CO[DGFIP][2017]EH)
 + positif(SALEXT4) + positif(CO[DGFIP][2017]FD) + positif(CO[DGFIP][2017]FE) + positif(PPEXT4) + positif(CO[DGFIP][2017]FH)
 + positif(RDSYPP)
 + positif(PENIN1) + positif(PENIN2) + positif(PENIN3) + positif(PENIN4)
 + positif(CODRCZ) + positif(CODRDZ) + positif(CODREZ) + positif(CODRFZ)
 + 0 ;

regle 1000531:
application : batch, iliad;

SOMMEA031 = (

   positif( TSHALLOC ) + positif( ALLOC ) + positif( PRBRC ) 
 + positif( PALIC ) + positif( GSALC ) + positif( TSASSUC ) + positif( XETRANC ) 
 + positif( EXOCETC ) + positif( FRNC ) 
 + positif( PPETPC ) + positif( PPENHC )  + positif( PCAPTAXC )
 + positif( CARTSC ) + positif( PENSALC ) + positif( REMPLAC ) + positif( CARPEC ) 
 + positif( GLDGRATC ) 
 + positif( GL[DGFIP][2017]C ) + positif( GL[DGFIP][2017]C ) + positif( GLD3C ) 

 + positif( BPV18C ) + positif( BPCOPTC ) + positif( BPV40C ) 
 + positif( BPCOSAC ) + positif( CVNSALAC )

 + positif( FEXC ) + positif( BAFC ) + positif( BAFORESTC ) + positif( BAFPVC ) + positif( BA[DGFIP][2017]AC ) 
 + positif( BAEXC ) + positif( BACREC ) + positif( 4BACREC ) + positif( BA1AC ) 
 + positif(BACDEC * (1 - positif(ART1731BIS) )) 
 + positif( BAHEXC ) + positif( BAHREC ) + positif( 4BAHREC ) 
 + positif (BAHDEC * (1 - positif(ART1731BIS) ))   + positif( BAPERPC ) + positif( BANOCGAC ) 
 + positif( AUTOBICVC ) + positif( AUTOBICPC ) + positif( AUTOBNCC ) 
 + positif( MIBEXC ) + positif( MIBVENC ) + positif( MIBPRESC ) + positif( MIBPVC ) 
 + positif( MIB1AC ) + positif( MIBDEC ) + positif( BICPMVCTC )
 + positif( BICEXC ) + positif( BICNOC ) + positif( LOCPROCGAC ) + positif( B[DGFIP][2017]AC ) 
 + positif (BICDNC * (1 - positif(ART1731BIS) ))  
 + positif (LOCDEFPROCGAC * (1 - positif(ART1731BIS) ))
 + positif( BIHEXC ) + positif( BIHNOC ) + positif( LOCPROC ) + positif(BIHDNC * (1 - positif(ART1731BIS) ))  
 + positif (LOCDEFPROC * (1 - positif(ART1731BIS) )) 
 + positif( BIPERPC ) 
 + positif( MIBNPEXC ) + positif( MIBNPVENC ) + positif( MIBNPPRESC ) + positif( MIBNPPVC ) + positif( MIBN[DGFIP][2017]AC ) + positif( MIBNPDEC ) 
 + positif( BICNPEXC ) + positif( BICREC ) + positif( LOCNPCGAC ) + positif( B[DGFIP][2017]AC ) 
 + positif (min(BICDEC,BICDEC1731+0) * positif(ART1731BIS) + BICDEC * (1 - ART1731BIS))
 + positif (min(LOCDEFNPCGAC,LOCDEFNPCGAC1731+0) * positif(ART1731BIS) + LOCDEFNPCGAC * (1 - ART1731BIS))
 + positif( MIBMEUC ) + positif( MIBGITEC ) + positif( LOCGITC ) + positif( LOCGITCC ) + positif( LOCGITHCC )
 + positif( BICNPHEXC ) + positif( BICHREC ) + positif( LOCNPC ) 
 + positif (min(BICHDEC,BICHDEC1731+0) * positif(ART1731BIS) + BICHDEC * (1 - ART1731BIS)) 
 + positif (min(LOCDEFNPC,LOCDEFNPC1731+0) * positif(ART1731BIS) + LOCDEFNPC * (1 - ART1731BIS)) 
 + positif( BNCPROEXC ) + positif( BNCPROC ) + positif( BNCPROPVC ) + positif( BNCPRO1AC ) + positif( BNCPRODEC ) + positif( BNCPMVCTC )
 + positif( BNCEXC ) + positif( BNCREC ) + positif( BN1AC ) 
 + positif (BNCDEC * (1 - positif(ART1731BIS) ))  
 + positif( BNHEXC ) + positif( BNHREC ) + positif (BNHDEC * (1 - positif(ART1731BIS) )) + positif( BNCCRC ) + positif( CESSASSC )
 + positif( XHONOAAC ) + positif( XHONOC ) + positif( XSPENPC )
 + positif( BNCNPC ) + positif( BNCNPPVC ) + positif( BNCN[DGFIP][2017]AC ) + positif( BNCNPDEC ) 
 + positif( BNCNPREXAAC ) + positif( BNCAABC ) + positif(min(BNCAADC,BNCAADC1731+0) * positif(ART1731BIS) + BNCAADC * (1 - ART1731BIS)) + positif( BNCNPREXC ) + positif( ANOVEP )
 + positif( PVINCE ) + positif( INVENTC ) + positif (min(DNOCEPC,DNOCEPC1731+0) * positif(ART1731BIS) + DNOCEPC * (1 - ART1731BIS)) + positif( BNCCRFC )
 + positif( RCSC ) + positif( PVSOCC ) + positif( PPEACC ) + positif( PPENJC ) 
 + positif( PEBFC ) 
 + positif( PERPC ) + positif( PERP_COTC ) + positif( RACCOTC ) + positif( PLAF_PERPC )
 + positif( PERPPLAFCC ) + positif( PERPPLAFNUC1 ) + positif( PERPPLAFNUC2 ) + positif( PERPPLAFNUC3 )
 + positif( ELURASC )
 + positif(CODDBJ) + positif(CODEBJ)  
 + positif(SALEXTC) + positif(CO[DGFIP][2017]BD) + positif(CO[DGFIP][2017]BE) + positif(PPEXTC) + positif(CO[DGFIP][2017]BH)
 + positif(RDSYCJ)
 + positif(PENINC) + positif(CODRBZ)

 + 0 ) ;
regle 1000804:
application : iliad , batch;  


SOMMEA804 = SOMMEANOEXP 
	    + positif ( GL[DGFIP][2017]V ) + positif ( GL[DGFIP][2017]V ) + positif ( GLD3V ) 
            + positif ( GL[DGFIP][2017]C ) + positif ( GL[DGFIP][2017]C ) + positif ( GLD3C ) 
           ;

SOMMEA805 = SOMMEANOEXP + positif(CODDAJ) + positif(CODEAJ) + positif(CODDBJ) + positif(CODEBJ) 
            + positif ( CARTSV ) + positif ( CARTSNBAV ) + positif ( CARTSC ) + positif ( CARTSNBAC ) ;

regle 10009931:
application : iliad , batch;  

SOMMEA899 = present( BICEXV ) + present( BICNOV ) + present( B[DGFIP][2017]AV ) + present( BICDNV )
            + present( BICEXC ) + present( BICNOC ) + present( B[DGFIP][2017]AC ) + present( BICDNC )
	    + present( BICEXP ) + present( BICNOP ) + present( B[DGFIP][2017]AP ) + present( BICDNP )
	    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
	    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
	    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
	    ;

SOMMEA859 = present( BICEXV ) + present( BICNOV ) + present( B[DGFIP][2017]AV ) + present( BICDNV )
            + present( BICEXC ) + present( BICNOC ) + present( B[DGFIP][2017]AC ) + present( BICDNC )
	    + present( BICEXP ) + present( BICNOP ) + present( B[DGFIP][2017]AP ) + present( BICDNP )
	    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
	    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
	    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
	    ;

SOMMEA860 = present( BICEXV ) + present( BICNOV ) + present( B[DGFIP][2017]AV ) + present( BICDNV )
            + present( BICEXC ) + present( BICNOC ) + present( B[DGFIP][2017]AC ) + present( BICDNC )
	    + present( BICEXP ) + present( BICNOP ) + present( B[DGFIP][2017]AP ) + present( BICDNP )
	    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
	    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
	    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
            + present( BNCEXV ) + present( BNCREV ) + present( BN1AV ) + present( BNCDEV )
	    + present( BNCEXC ) + present( BNCREC ) + present( BN1AC ) + present( BNCDEC )
	    + present( BNCEXP ) + present( BNCREP ) + present( BN1AP ) + present( BNCDEP )
	    + present( BNHEXV ) + present( BNHREV ) + present( BNHDEV )
	    + present( BNHEXC ) + present( BNHREC ) + present( BNHDEC )
	    + present( BNHEXP ) + present( BNHREP ) + present( BNHDEP )
            ;

SOMMEA895 = present( BAEXV ) + present( BACREV ) + present( 4BACREV )
            + present( BA1AV ) + present( BACDEV )
            + present( BAEXC ) + present( BACREC ) + present( 4BACREC )
            + present( BA1AC ) + present( BACDEC )
	    + present( BAEXP ) + present( BACREP ) + present( 4BACREP )
	    + present( BA1AP ) + present( BACDEP )
	    + present( BAHEXV ) + present( BAHREV ) + present( 4BAHREV )
	    + present( BAHDEV )
	    + present( BAHEXC ) + present( BAHREC ) + present( 4BAHREC )
	    + present( BAHDEC )
	    + present( BAHEXP ) + present( BAHREP ) + present( 4BAHREP )
	    + present( BAHDEP )
	    + present( FEXV ) + present( BAFV ) + (1 - null( V_FORVA+0 )) + present( BAFPVV ) + present( BA[DGFIP][2017]AV )
            + present( FEXC ) + present( BAFC ) + (1 - null( V_FORCA+0 )) + present( BAFPVC ) + present( BA[DGFIP][2017]AC )
            + present( FEXP ) + present( BAFP ) + (1 - null( V_FORPA+0 )) + present( BAFPVP ) + present( BA[DGFIP][2017]AP ) 
	    ;

SOMMEA898 = SOMMEA895 ;

SOMMEA881 =  
	     present( BAEXV ) + present( BACREV ) + present( 4BACREV ) + present( BA1AV ) + present( BACDEV )
           + present( BAEXC ) + present( BACREC ) + present( 4BACREC ) + present( BA1AC ) + present( BACDEC )
	   + present( BAEXP ) + present( BACREP ) + present( 4BACREP ) + present( BA1AP ) + present( BACDEP )
	   + present( BAHEXV ) + present( BAHREV ) + present( 4BAHREV ) + present( BAHDEV )
	   + present( BAHEXC ) + present( BAHREC ) + present( 4BAHREC ) + present( BAHDEC )
	   + present( BAHEXP ) + present( BAHREP ) + present( 4BAHREP ) + present( BAHDEP )

	   + present( BICEXV ) + present( BICNOV ) + present( B[DGFIP][2017]AV ) + present( BICDNV )
	   + present( BICEXC ) + present( BICNOC ) + present( B[DGFIP][2017]AC )
	   + present( BICDNC ) + present( BICEXP ) + present( BICNOP ) 
	   + present( B[DGFIP][2017]AP ) + present( BICDNP ) + present( BIHEXV ) + present( BIHNOV )
	   + present( BIHDNV ) + present( BIHEXC )
	   + present( BIHNOC ) + present( BIHDNC ) 
	   + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
	   + present( BICNPEXV ) + present( BICREV ) + present( B[DGFIP][2017]AV )
	   + present( BICDEV ) + present( BICNPEXC ) + present( BICREC ) 
	   + present( B[DGFIP][2017]AC ) + present( BICDEC ) + present( BICNPEXP ) + present( BICREP )
	   + present( B[DGFIP][2017]AP ) + present( BICDEP ) + present( BICNPHEXV )
	   + present( BICHREV ) + present( BICHDEV ) 
	   + present( BICNPHEXC ) + present( BICHREC ) + present( BICHDEC )
	   + present( BICNPHEXP ) + present( BICHREP ) 
	   + present( BICHDEP ) 
	   + present( LOCPROCGAV ) + present( LOCDEFPROCGAV ) 
	   + present( LOCPROCGAC ) + present( LOCDEFPROCGAC ) 
	   + present( LOCPROCGAP ) + present( LOCDEFPROCGAP )
	   + present( LOCPROV ) + present( LOCDEFPROV ) + present( LOCPROC )
	   + present( LOCDEFPROC ) + present( LOCPROP ) + present( LOCDEFPROP )
	   + present( LOCNPCGAV ) + present( LOCGITCV ) + present( LOCDEFNPCGAV ) 
	   + present( LOCNPCGAC ) + present( LOCGITCC ) + present( LOCDEFNPCGAC ) 
	   + present( LOCNPCGAPAC ) + present( LOCGITCP ) + present( LOCDEFNPCGAPAC )
	   + present( LOCNPV ) + present( LOCGITHCV ) + present( LOCDEFNPV ) 
	   + present( LOCNPC ) + present( LOCGITHCC ) + present( LOCDEFNPC ) 
	   + present( LOCNPPAC ) + present( LOCGITHCP ) + present( LOCDEFNPPAC )
           + present( BAPERPV ) + present( BAPERPC ) + present( BAPERPP)
           + present( BANOCGAV ) + present( BANOCGAC ) + present( BANOCGAP )

	   + present( BNCEXV ) + present( BNCREV ) + present( BN1AV ) + present( BNCDEV ) 
	   + present( BNCEXC ) + present( BNCREC ) + present( BN1AC ) + present( BNCDEC )
	   + present( BNCEXP ) + present( BNCREP ) + present( BN1AP ) + present( BNCDEP ) 
	   + present( BNHEXV ) + present( BNHREV ) + present( BNHDEV ) 
	   + present( BNHEXC ) + present( BNHREC ) + present( BNHDEC ) 
	   + present( BNHEXP ) + present( BNHREP ) + present( BNHDEP ) 
           + present( XHONOAAV ) + present( XHONOV ) 
	   + present( XHONOAAC ) + present( XHONOC ) 
	   + present( XHONOAAP ) + present( XHONOP )

	   + present ( BNCAABV ) + present ( ANOCEP ) + present ( INVENTV ) 
	   + present ( PVINVE ) + present ( BNCAADV ) + present ( DNOCEP ) 
	   + present ( BNCAABC ) + present ( ANOVEP ) + present ( INVENTC ) 
	   + present ( PVINCE ) + present ( BNCAADC ) + present ( DNOCEPC )
	   + present ( BNCAABP ) + present ( ANOPEP ) + present ( INVENTP )
	   + present ( PVINPE ) + present ( BNCAADP ) + present ( DNOCEPP )
           + present( BNCNPREXAAV ) + present( BNCNPREXAAC ) + present( BNCNPREXAAP )
           + present( BNCNPREXV ) + present( BNCNPREXC ) + present( BNCNPREXP )
	   ;

SOMMEA858 = SOMMEA881 + present(TSHALLOV) + present(TSHALLOC) + present(TSASSUV) + present(TSASSUC)
                      + present(RFMIC) + present(RFORDI) + present(RFDORD) + present(RFDHIS) ;

SOMMEA861 = SOMMEA881 ;

SOMMEA890 = SOMMEA881  + present( TSHALLOV ) + present( TSHALLOC ) 
		       + present( CARTSV ) + present( CARTSC )
		       + present( CARTSNBAV ) + present( CARTSNBAC ) ;

SOMMEA891 = SOMMEA881 ;

SOMMEA892 = SOMMEA881 ;

SOMMEA893 = SOMMEA881 ;

SOMMEA894 = SOMMEA881 ;

SOMMEA896 = SOMMEA881 ;

SOMMEA897 = SOMMEA881 ;

SOMMEA885 =  present( BA1AV ) + present( BA1AC ) + present( BA1AP )
           + present( B[DGFIP][2017]AV ) + present( B[DGFIP][2017]AC ) + present( B[DGFIP][2017]AP ) 
	   + present( BN1AV ) + present( BN1AC ) + present( BN1AP ) ;

SOMMEA880 =  present ( BICEXV ) + present ( BICNOV ) + present ( B[DGFIP][2017]AV )
           + present ( BICDNV ) + present ( BICEXC ) + present ( BICNOC )
	   + present ( B[DGFIP][2017]AC ) + present ( BICDNC ) 
	   + present ( BICEXP ) + present ( BICNOP ) + present ( B[DGFIP][2017]AP )
	   + present ( BICDNP ) + present ( BIHEXV ) + present ( BIHNOV )
           + present ( BIHDNV ) + present ( BIHEXC )
	   + present ( BIHNOC ) + present ( BIHDNC ) 
	   + present ( BIHEXP ) + present ( BIHNOP ) + present ( BIHDNP )
	   + present ( LOCPROCGAV ) + present ( LOCDEFPROCGAV ) + present ( LOCPROCGAC )
	   + present ( LOCDEFPROCGAC ) + present ( LOCPROCGAP ) + present ( LOCDEFPROCGAP )
	   + present ( LOCPROV ) + present ( LOCDEFPROV ) + present ( LOCPROC )
	   + present ( LOCDEFPROC ) + present ( LOCPROP ) + present ( LOCDEFPROP )
	   ;

SOMMEA874 =  somme(i=V,C,1,2,3,4:present(TSHALLOi) + present(ALLOi) + present(PRBRi) + present(PALIi) + present(PENINi))
            + present ( CARTSV ) + present ( CARTSC ) + present ( CARTS[DGFIP][2017] )
            + present ( CARTS[DGFIP][2017] ) + present ( CARTSP3) + present ( CARTSP4 )
            + present ( CARTSNBAV ) + present ( CARTSNBAC ) + present ( CARTSNBA[DGFIP][2017] )
            + present ( CARTSNBA[DGFIP][2017] ) + present ( CARTSNBAP3) + present ( CARTSNBAP4 )
            + present ( REMPLAV ) + present ( REMPLAC ) + present ( REMPLA[DGFIP][2017] )
            + present ( REMPLA[DGFIP][2017] ) + present ( REMPLAP3 ) + present ( REMPLAP4 )
            + present ( REMPLANBV ) + present ( REMPLANBC ) + present ( REMPLANB[DGFIP][2017] )
            + present ( REMPLANB[DGFIP][2017] ) + present ( REMPLANBP3 ) + present ( REMPLANBP4 )
            + present ( CARPEV ) + present ( CARPEC ) + present ( CARPE[DGFIP][2017] )
            + present ( CARPE[DGFIP][2017] ) + present ( CARPEP3 ) + present ( CARPEP4 )
            + present ( CARPENBAV ) + present ( CARPENBAC ) + present ( CARPENBA[DGFIP][2017] )
            + present ( CARPENBA[DGFIP][2017] ) + present ( CARPENBAP3 ) + present ( CARPENBAP4 )
            + present ( PENSALV ) + present ( PENSALC ) + present ( PENSAL[DGFIP][2017] )
            + present ( PENSAL[DGFIP][2017] ) + present ( PENSALP3 ) + present ( PENSALP4 )
            + present ( PENSALNBV ) + present ( PENSALNBC ) + present ( PENSALNB[DGFIP][2017] )
            + present ( PENSALNB[DGFIP][2017] ) + present ( PENSALNBP3 ) + present ( PENSALNBP4 )
	    + somme(k=V,C,P: somme (i=C,H: present(BIiNOk)  
		           + somme(j = N: present(BIiDjk))) 
	                   + somme (i = I: present(B[DGFIP][2017]Ak)) 
		  )
            + present(CODRAZ) + present(CODRBZ) + present(CODRCZ) 
            + present(CODRDZ) + present(CODREZ) + present(CODRFZ)
	    + present ( RVB1 ) + present ( RVB2 ) + present ( RVB3 ) + present ( RVB4 )
	    + present ( RENTAX ) + present ( RENTAX5 ) + present ( RENTAX6 ) + present ( RENTAX7 )
	    + present ( RENTAXNB ) + present ( RENTAXNB5 ) + present ( RENTAXNB6 ) + present ( RENTAXNB7 )
	    + present( RCMABD ) + present( RCMHAD ) + present( REGPRIV ) + present( RCMIMPAT )
	    + present( REVACT ) + present( DISQUO ) + present( RESTUC )
	    + present( REVACTNB ) + present( DISQUONB ) + present ( RESTUCNB )
            + present( CO[DGFIP][2017]FA ) + present( RCMHAB ) + present( INTERE )
	    + present ( MIBVENV ) + present ( MIBPRESV ) + present ( MIB1AV ) + present ( MIBDEV ) 
	    + present ( MIBVENC ) + present ( MIBPRESC ) + present ( MIB1AC ) + present ( MIBDEC ) 
	    + present ( MIBVENP ) + present ( MIBPRESP ) + present ( MIB1AP ) + present ( MIBDEP ) 
	    + present( LOCPROCGAV ) + present( LOCDEFPROCGAV ) + present( LOCPROCGAC )
	    + present( LOCDEFPROCGAC ) + present( LOCPROCGAP ) + present( LOCDEFPROCGAP )
	    + present( LOCPROV ) + present( LOCDEFPROV ) + present( LOCPROC )
	    + present( LOCDEFPROC ) + present( LOCPROP ) + present( LOCDEFPROP )
	    + present( BICREV ) + present( LOCNPCGAV ) + present( LOCGITCV ) + present( B[DGFIP][2017]AV ) + present( BICDEV ) + present( LOCDEFNPCGAV ) 
	    + present( BICREC ) + present( LOCNPCGAC ) + present( LOCGITCC ) + present( B[DGFIP][2017]AC ) + present( BICDEC ) + present( LOCDEFNPCGAC ) 
	    + present( BICREP ) + present( LOCNPCGAPAC ) + present( LOCGITCP ) + present( B[DGFIP][2017]AP  ) + present( BICDEP ) + present( LOCDEFNPCGAPAC )
	    + present( BICHREV ) + present( LOCNPV ) + present( LOCGITHCV ) + present( BICHDEV ) + present( LOCDEFNPV ) 
	    + present( BICHREC ) + present( LOCNPC ) + present( LOCGITHCC ) + present( BICHDEC ) + present( LOCDEFNPC ) 
	    + present( BICHREP ) + present( LOCNPPAC ) + present( LOCGITHCP ) + present( BICHDEP ) + present( LOCDEFNPPAC )
	    + present( MIBMEUV ) + present( MIBGITEV ) + present( LOCGITV ) + present( MIBNPVENV ) 
	    + present( MIBNPPRESV ) + present( MIBN[DGFIP][2017]AV ) + present( MIBNPDEV ) 
	    + present( MIBMEUC ) + present( MIBGITEC ) + present( LOCGITC ) + present( MIBNPVENC ) 
	    + present( MIBNPPRESC ) + present( MIBN[DGFIP][2017]AC ) + present( MIBNPDEC ) 
	    + present( MIBMEUP ) + present( MIBGITEP ) + present( LOCGITP ) + present( MIBNPVENP ) 
	    + present( MIBNPPRESP ) + present( MIBN[DGFIP][2017]AP ) + present( MIBNPDEP ) 
	    + present ( BNCREV ) + present ( BN1AV ) + present ( BNCDEV )
	    + present ( BNCREC ) + present ( BN1AC ) + present ( BNCDEC )
	    + present ( BNCREP ) + present ( BN1AP ) + present ( BNCDEP )
	
