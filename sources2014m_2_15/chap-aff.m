#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle 111011:
application : iliad ;

CONST0 = 0;
CONST1 = 1;
CONST2 = 2;
CONST3 = 3;
CONST4 = 4;
CONST10 = 10;
CONST20 = 20;
CONST40 = 40;

regle 1110:
application : batch, iliad;

LI[DGFIP][2017] = (1 - positif(IPVLOC)) * (1 - positif(RE168 + TAX1649)) * IND_REV ;

LI[DGFIP][2017]1 = (1 - positif(RE168 + TAX1649)) * (1 - positif(ANNUL2042)) * IND_REV ;

LI[DGFIP][2017] = (1 - positif(RE168 + TAX1649)) ;

LI[DGFIP][2017] = (1 - positif(ANNUL2042)) ;

LIG3 = positif(positif(CMAJ + 0) 
	+ positif_ou_nul(MAJTX1 - 40) + positif_ou_nul(MAJTX4 - 40)
        + positif_ou_nul(MAJTXPCA[DGFIP][2017] - 40) + positif_ou_nul(MAJTXPCAP4 - 40)
        + positif_ou_nul(MAJTXLOY1 - 40) + positif_ou_nul(MAJTXLOY4 - 40)
        + positif_ou_nul(MAJTXCHR1 - 40) + positif_ou_nul(MAJTXCHR4 - 40)
	+ positif_ou_nul(MAJTXC1 - 40) + positif_ou_nul(MAJTXC4 - 40) 
        + positif_ou_nul(MAJTXCVN1 - 40) + positif_ou_nul(MAJTXCVN4 - 40)
	+ positif_ou_nul(MAJTXCDIS1 - 40) + positif_ou_nul(MAJTXCDIS4 - 40)
        + positif_ou_nul(MAJTXGLO1 - 40) + positif_ou_nul(MAJTXGLO4 - 40)
        + positif_ou_nul(MAJTXRSE11 - 40) + positif_ou_nul(MAJTXRSE14 - 40)
        + positif_ou_nul(MAJTXRSE51 - 40) + positif_ou_nul(MAJTXRSE54 - 40)
	+ positif_ou_nul(MAJTXRSE21 - 40) + positif_ou_nul(MAJTXRSE24 - 40)
        + positif_ou_nul(MAJTXRSE31 - 40) + positif_ou_nul(MAJTXRSE34 - 40)
        + positif_ou_nul(MAJTXRSE41 - 40) + positif_ou_nul(MAJTXRSE44 - 40)
        + positif_ou_nul(MAJTXTAXA4 - 40)) ;

regle 1110010:
application : batch , iliad ;


LI[DGFIP][2017]010 = (INDV * INDC * INDP) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]020 = (INDV * (1 - INDC) * (1 - INDP)) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]030 = (INDC * (1 - INDV) * (1 - INDP)) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]040 = (INDP * (1 - INDV) * (1 - INDC)) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]050 = (INDV * INDC * (1 - INDP)) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]060 = (INDV * INDP * (1 - INDC)) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]070 = (INDC * INDP * (1 - INDV)) * (1 - ART1731BIS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]0YT = (INDV * INDC * INDP) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]0YT = (INDV * (1 - INDC) * (1 - INDP)) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIG30YT = (INDC * (1 - INDV) * (1 - INDP)) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIG40YT = (INDP * (1 - INDV) * (1 - INDC)) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]0YT = (INDV * INDC * (1 - INDP)) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIG60YT = (INDV * INDP * (1 - INDC)) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIG70YT = (INDC * INDP * (1 - INDV)) * ART1731BIS * LI[DGFIP][2017] * LI[DGFIP][2017] ;

regle 11110:
application : batch , iliad ;
LI[DGFIP][2017]0V = positif_ou_nul(TSBNV + PRBV + BPCOSAV + GLDGRATV + positif([DGFIP][2017]0AV * null(TSBNV + PRBV + BPCOSAV + GLDGRATV))) ;
LI[DGFIP][2017]0C = positif_ou_nul(TSBNC + PRBC + BPCOSAC + GLDGRATC + positif([DGFIP][2017]0AC * null(TSBNC + PRBC + BPCOSAC + GLDGRATC))) ;
LI[DGFIP][2017]0P = positif_ou_nul(somme(i=1..4:TSBNi + PRBi) + positif([DGFIP][2017]0AP * null(somme(i=1..4:TSBNi + PRBi)))) ;
LI[DGFIP][2017]0 = positif(LI[DGFIP][2017]0V + LI[DGFIP][2017]0C + LI[DGFIP][2017]0P) ;
regle 11000:
application : batch , iliad ;

LI[DGFIP][2017]100 = positif(T2RV) * (1 - positif(IPVLOC)) ;

LIG899 = positif(RVTOT + LI[DGFIP][2017]100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LI[DGFIP][2017]RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		  + RCMLIB + LI[DGFIP][2017]9 + LIG30 + RFQ + 2REVF + 3REVF + LI[DGFIP][2017]130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LI[DGFIP][2017]010 + LI[DGFIP][2017]020 + LI[DGFIP][2017]030 + LI[DGFIP][2017]040 + LI[DGFIP][2017]050 + LI[DGFIP][2017]060 + LI[DGFIP][2017]070)) 
		 * (1 - ART1731BIS) ; 

LIG900 = positif(RVTOT + LI[DGFIP][2017]100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LI[DGFIP][2017]RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		  + RCMLIB + LI[DGFIP][2017]9 + LIG30 + RFQ + 2REVF + 3REVF + LI[DGFIP][2017]130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LI[DGFIP][2017]010 + LI[DGFIP][2017]020 + LI[DGFIP][2017]030 + LI[DGFIP][2017]040 + LI[DGFIP][2017]050 + LI[DGFIP][2017]060 + LI[DGFIP][2017]070) 
		 * (1 - ART1731BIS) ; 

LIG899YT = positif(RVTOT + LI[DGFIP][2017]100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LI[DGFIP][2017]RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		   + RCMLIB + LI[DGFIP][2017]9 + LIG30 + RFQ + 2REVF + 3REVF + LI[DGFIP][2017]130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LI[DGFIP][2017]0YT + LI[DGFIP][2017]0YT + LIG30YT + LIG40YT + LI[DGFIP][2017]0YT + LIG60YT + LIG70YT)) 
		 * ART1731BIS ; 

LIG900YT = positif(RVTOT + LI[DGFIP][2017]100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LI[DGFIP][2017]RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		   + RCMLIB + LI[DGFIP][2017]9 + LIG30 + RFQ + 2REVF + 3REVF + LI[DGFIP][2017]130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LI[DGFIP][2017]0YT + LI[DGFIP][2017]0YT + LIG30YT + LIG40YT + LI[DGFIP][2017]0YT + LIG60YT + LIG70YT) 
		 * ART1731BIS ; 

regle 111440:
application : batch , iliad ;

LIG4401 =  positif(V_FORVA) * (1 - positif_ou_nul(BAFV)) * LI[DGFIP][2017] ;

LIG4402 =  positif(V_FORCA) * (1 - positif_ou_nul(BAFC)) * LI[DGFIP][2017] ;

LIG4403 =  positif(V_FORPA) * (1 - positif_ou_nul(BAFP)) * LI[DGFIP][2017] ;

regle 11113:
application : iliad , batch ;

LI[DGFIP][2017]3 =  positif(present(BACDEV)+ present(BACREV)
               + present(BAHDEV) +present(BAHREV)
               + present(BACDEC) +present(BACREC)
               + present(BAHDEC)+ present(BAHREC)
               + present(BACDEP)+ present(BACREP)
               + present(BAHDEP)+ present(BAHREP)
               + present(4BAHREV) + present(4BAHREC) + present(4BAHREP)
               + present(4BACREV) + present(4BACREC) + present(4BACREP)
               + present(BAFV) + present(BAFC) + present(BAFP)
	       + present(BAFORESTV) + present(BAFORESTC) 
	       + present(BAFORESTP)
               + present(BAFPVV) + present(BAFPVC) + present(BAFPVP))
	* (1 - positif(IPVLOC)) * (1 - positif(ANNUL2042)) * LI[DGFIP][2017] ;

regle 111135:
application : batch , iliad ;

4BAQLV = positif(4BACREV + 4BAHREV) ;
4BAQLC = positif(4BACREC + 4BAHREC) ;
4BAQLP = positif(4BACREP + 4BAHREP) ;

regle 111134:
application : iliad , batch ;

LI[DGFIP][2017]34V = positif(present(BAFV) + present(BAHREV) + present(BAHDEV) + present(BACREV) + present(BACDEV)+ present(BAFPVV)+present(BAFORESTV)) ;
LI[DGFIP][2017]34C = positif(present(BAFC) + present(BAHREC) + present(BAHDEC) + present(BACREC) + present(BACDEC)+ present(BAFPVC)+present(BAFORESTC)) ;
LI[DGFIP][2017]34P = positif(present(BAFP) + present(BAHREP) + present(BAHDEP) + present(BACREP) + present(BACDEP)+ present(BAFPVP)+present(BAFORESTP)) ;
LI[DGFIP][2017]34 = positif(LI[DGFIP][2017]34V + LI[DGFIP][2017]34C + LI[DGFIP][2017]34P+present(DAGRI6)+present(DAGR[DGFIP][2017])+present(DAGRI4)+present(DAGRI3)+present(DAGR[DGFIP][2017])+present(DAGR[DGFIP][2017])) 
		* (1 - positif(IPVLOC)) * (1 - positif(abs(DEFIBA))) * (1 - positif(ANNUL2042)) * LI[DGFIP][2017] ;

LIGDBAIP = positif_ou_nul(DBAIP) * positif(DAGR[DGFIP][2017] + DAGR[DGFIP][2017] + DAGRI3 + DAGRI4 + DAGR[DGFIP][2017] + DAGRI6) * (1 - positif(IPVLOC))
			  * positif(abs(abs(BAHQTOT)+abs(BAQTOT)-(DAGRI6+DAGR[DGFIP][2017]+DAGRI4+DAGRI3+DAGR[DGFIP][2017]+DAGR[DGFIP][2017]))) * LI[DGFIP][2017] ;
regle 111136:
application : iliad ,batch;
LI[DGFIP][2017]36 = positif(4BAQV + 4BAQC + 4BAQP) * (1 - positif(IPVLOC)) * (1 - positif(ANNUL2042)) * LI[DGFIP][2017] ;

regle 111590:
application : iliad, batch ;
pour i = V,C,P:
LIG_BICPi =        (
  present ( BICNOi )                          
 + present (BICDNi )                          
 + present (BIHNOi )                          
 + present (BIHDNi )                          
                  ) * (1 - positif(ANNUL2042)) * LI[DGFIP][2017] ;

LIG_BICP = LIG_BICPV + LIG_BICPC + LIG_BICPP ;

LIG_DEFNPI = positif(
   present ( DEFBIC6 ) 
 + present ( DEFBIC5 ) 
 + present ( DEFBIC4 ) 
 + present ( DEFBIC3 ) 
 + present ( DEFBIC2 )
 + present ( DEFBIC1 )
            )
  * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGMLOC = positif(present(MIBMEUV) + present(MIBMEUC) + present(MIBMEUP)
		+ present(MIBGITEV) + present(MIBGITEC) + present(MIBGITEP)
		+ present(LOCGITV) + present(LOCGITC) + present(LOCGITP))
	  * LI[DGFIP][2017] * LI[DGFIP][2017] ;
 
LIGMLOCAB = positif(MLOCABV + MLOCABC + MLOCABP) * LI[DGFIP][2017]  * LI[DGFIP][2017] ; 

LIGMIBMV = positif(BICPMVCTV + BICPMVCTC + BICPMVCTP) * (1 - null(4 - V_REGCO)) * LI[DGFIP][2017] ;

LIGBNCMV = positif(BNCPMVCTV + BNCPMVCTC + BNCPMVCTP) * (1 - null(4 - V_REGCO)) * LI[DGFIP][2017] ;

LIGPLOC = positif(LOCPROCGAV + LOCPROCGAC + LOCPROCGAP + LOCDEFPROCGAV + LOCDEFPROCGAC + LOCDEFPROCGAP 
		+ LOCPROV + LOCPROC + LOCPROP + LOCDEFPROV +LOCDEFPROC + LOCDEFPROP)
		   * (1 - null(4 - V_REGCO)) * LI[DGFIP][2017] ;

LIGNPLOC = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP)
		   *  (1-null(4 - V_REGCO)) * LI[DGFIP][2017] ;

LIGNPLOCF = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC
                   + LNPRODE[DGFIP][2017]0 + LNPRODEF9 + LNPRODEF8 + LNPRODEF7 + LNPRODEF6 + LNPRODE[DGFIP][2017]
                   + LNPRODEF4 + LNPRODEF3 + LNPRODE[DGFIP][2017] + LNPRODE[DGFIP][2017]
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP)
		   *  (1-null(4 - V_REGCO)) * LI[DGFIP][2017] ;

LIGDEFNPLOC = positif(TOTDEFLOCNP) *  (1-null(4 - V_REGCO)) ;

LIGDFLOCNPF = positif(DEFLOCNPF) * positif(DEFRILOC) * (1 - PREM8_11) ;

LIGLOCNSEUL = positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) ;

LIGLOCSEUL = 1 - positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) ;

regle 1115901:
application : iliad,batch;

LIG_BICNPF = 
       positif(
   present (BICDEC)
 + present (BICDEP)
 + present (BICDEV)
 + present (BICHDEC)
 + present (BICHDEP)
 + present (BICHDEV)
 + present (BICHREC)
 + present (BICHREP)
 + present (BICHREV)
 + present (BICREC)
 + present (BICREP)
 + present (BICREV)
 + present ( DEFBIC6 ) 
 + present ( DEFBIC5 ) 
 + present ( DEFBIC4 ) 
 + present ( DEFBIC3 ) 
 + present ( DEFBIC2 )
 + present ( DEFBIC1 )
)
                   * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 11117:
application : iliad,batch;
LIG_BNCNF = positif (present(BNCV) + present(BNCC) + present(BNCP)) ;

LIGNOCEP = (present(NOCEPV) + present(NOCEPC) + present(NOCEPP)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGNOCEPIMP = (present(NOCEPIMPV) + present(NOCEPIMPC) + present(NOCEPIMPP)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGDAB = positif(present(DABNCNP6) + present(DABNCN[DGFIP][2017]) + present(DABNCNP4)
		 + present(DABNCNP3) + present(DABNCN[DGFIP][2017]) + present(DABNCN[DGFIP][2017])) 
		* LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGDIDAB = positif_ou_nul(DIDABNCNP) * positif(LIGDAB) * LI[DGFIP][2017] * LI[DGFIP][2017] ;


LIGDEFBNCNPF = positif(DEFBNCNPF) * positif(DEFRIBNC) * null(PREM8_11) ;
LIGDEFBANIF  = positif (DEFBANIF) * positif(DEFRIBASUP + DEFRIGLOBSUP) * (1-PREM8_11);
LIGDEFBICNPF = positif (DEFBICNPF) * DEFRIBIC * (1-PREM8_11);
LIGDEFRFNONI = positif (DEFRFNONI) * DEFRIRF * (1-PREM8_11);

LIGBNCIF = ( positif (LIGNOCEP) * (1 - positif(LIG3250) + null(BNCIF)) 
             + (null(BNCIF) * positif(LIGBNCDF)) 
	     + null(BNCIF) * (1 - positif_ou_nul(NOCEPIMP+SPENETNPF-DABNCNP6 -DABNCN[DGFIP][2017] -DABNCNP4 -DABNCNP3 -DABNCN[DGFIP][2017] -DABNCN[DGFIP][2017]))
             + positif (LIGDEFBNCNPF)
             + positif(
   present (DABNCNP6)
 + present (DABNCN[DGFIP][2017])
 + present (DABNCNP4)
 + present (DABNCNP3)
 + present (DABNCN[DGFIP][2017])
 + present (DABNCN[DGFIP][2017])
 + present (BNCAADV)
 + present (DNOCEPC)
 + present (DNOCEPP)
 + present (BNCAADC)
 + present (BNCAADP)
 + present (DNOCEP)
 + present (BNCNPV)
 + present (BNCNPC)
 + present (BNCNPP)
 + present (BNCNPPVV)
 + present (BNCNPPVC)
 + present (BNCNPPVP)
 + present (BNCAABV)
 + present (ANOCEP)
 + present (BNCAABC)
 + present (ANOVEP)
 + present (BNCAABP)
 + present (ANOPEP)
                        )
           )
	     * (1 - positif(LIGSPENPNEG + LIGSPENPPOS)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 125:
application : batch, iliad;
LIG910 = positif(present(RCMABD) + present(RCMTNC) + present(RCMAV) + present(RCMHAD) 
	         + present(RCMHAB) + present(REGPRIV) + (1-present(BRCMQ)) *(present(RCMFR))
                ) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1111130: 
application : iliad , batch ;
LI[DGFIP][2017]130 = positif(present(REPSOF)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1111950:
application : iliad, batch;
LI[DGFIP][2017]950 = INDREV1A8 *  positif_ou_nul(REVKIRE) 
                    * (1 - positif_ou_nul(IND_TDR)) 
                    * (1 - positif(ANNUL2042 + 0)) ;

regle 11129:
application : batch, iliad;
LI[DGFIP][2017]9 = positif(present(RFORDI) + present(RFDHIS) + present(RFDANT) +
                present(RFDORD)) * (1 - positif(IPVLOC))
                * (1 - positif(LIG30)) * LI[DGFIP][2017] * LI[DGFIP][2017] * IND_REV ;
regle 11130:
application : iliad, batch ;
LIG30 = positif(RFMIC) * (1 - positif(IPVLOC)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGREVRF = positif(present(FONCI) + present(REAMOR)) * (1 - positif(IPVLOC)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 11149:
application : batch, iliad;
LIG49 =  INDREV1A8 * positif_ou_nul(DRBG) * LI[DGFIP][2017] ;
regle 11152:
application :  iliad, batch;
LI[DGFIP][2017]2 = positif(present(CHEN[DGFIP][2017]) + present(CHEN[DGFIP][2017]) + present(CHENF3) + present(CHENF4) 
                 + present(NCHEN[DGFIP][2017]) + present(NCHEN[DGFIP][2017]) + present(NCHENF3) + present(NCHENF4)) 
	     * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 11158:
application : iliad, batch;
LI[DGFIP][2017]8 = (present(PAAV) + present(PAAP)) * positif(LI[DGFIP][2017]2) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 111585:
application : iliad, batch;
LI[DGFIP][2017]85 = (present(PAAP) + present(PAAV)) * (1 - positif(LI[DGFIP][2017]8)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIG65 = positif(LI[DGFIP][2017]2 + LI[DGFIP][2017]8 + LI[DGFIP][2017]85 
                + present(CHRFAC) + present(CHNFAC) + present(CHRDED)
		+ present(DPERPV) + present(DPERPC) + present(DPERPP)
                + LIGREPAR)  
       * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 111555:
application : iliad, batch;
LIGDPREC = present(CHRFAC) * (1 - positif(ANNUL2042)) * LI[DGFIP][2017];

LIGDFACC = (positif(20-V_NOTRAIT+0) * positif(DFACC)
           + (1 - positif(20-V_NOTRAIT+0)) * present(DFACC)) * (1 - positif(ANNUL2042)) * LI[DGFIP][2017] ;
regle 1111390:
application : batch, iliad;
LI[DGFIP][2017]390 = positif(positif(ABMAR) + (1 - positif(R[DGFIP][2017])) * positif(V_0DN)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 11168:
application : batch, iliad;
LIG68 = INDREV1A8 * (1 - positif(abs(RNIDF))) * LI[DGFIP][2017] ;
regle 1111420:
application : iliad,batch;
LIGTTPVQ = positif(
                   positif(CARTSV) + positif(CARTSC) + positif(CARTS[DGFIP][2017]) + positif(CARTS[DGFIP][2017])+ positif(CARTSP3)+ positif(CARTSP4)
                   + positif(REMPLAV) + positif(REMPLAC) + positif(REMPLA[DGFIP][2017]) + positif(REMPLA[DGFIP][2017])+ positif(REMPLAP3)+ positif(REMPLAP4)
                   + positif(PEBFV) + positif(PEBFC) + positif(PEB[DGFIP][2017]) + positif(PEB[DGFIP][2017])+ positif(PEBF3)+ positif(PEBF4)
                   + positif(CARPEV) + positif(CARPEC) + positif(CARPE[DGFIP][2017]) + positif(CARPE[DGFIP][2017])+ positif(CARPEP3)+ positif(CARPEP4)
                   + positif(CODRAZ) + positif(CODRBZ) + positif(CODRCZ) + positif(CODRDZ) + positif(CODREZ) + positif(CODRFZ) 
                   + positif(PENSALV) + positif(PENSALC) + positif(PENSAL[DGFIP][2017]) + positif(PENSAL[DGFIP][2017])+ positif(PENSALP3)+ positif(PENSALP4)
                   + positif(RENTAX) + positif(RENTAX5) + positif(RENTAX6) + positif(RENTAX7)
                   + positif(REVACT) + positif(REVPEA) + positif(PROVIE) + positif(DISQUO) + positif(RESTUC) + positif(INTERE)
                   + positif(FONCI) + positif(REAMOR)
                   + positif(4BACREV) + positif(4BACREC)+positif(4BACREP)+positif(4BAHREV)+positif(4BAHREC)+positif(4BAHREP)
                   + positif(GL[DGFIP][2017]V) + positif(GL[DGFIP][2017]C) + positif(GL[DGFIP][2017]V) + positif(GL[DGFIP][2017]C) + positif(GLD3V) + positif(GLD3C)
                   + positif(CODDAJ) + positif(CODEAJ) + positif(CODDBJ)+ positif(CODEBJ)   
                   + positif(CODRVG)
                  ) * LI[DGFIP][2017] * LI[DGFIP][2017] * (1 - null(4-V_REGCO)) ;

regle 111721:
application : batch, iliad;

LI[DGFIP][2017]430 = positif(BPTP3) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]431 = positif(BPT[DGFIP][2017]8) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]432 = positif(BPT[DGFIP][2017]9) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 111722:
application : batch, iliad;
LIG815 = V_EAD * positif(BPTPD) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIG816 = V_EAG * positif(BPTPG) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGTX[DGFIP][2017]25 = positif(PEA+0) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGTX[DGFIP][2017]4 = positif(BPT[DGFIP][2017]4) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGTXF30 = positif_ou_nul(BPCOPTV + BPCOPTC + BPVSK) * LI[DGFIP][2017]  * LI[DGFIP][2017] ;
LIGTXF40 = positif(BPV40V + BPV40C + 0) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

regle 111723:
application : batch, iliad ;

LIGCESDOM = positif(BPTPDIV) * positif(PVTAXSB) * positif(V_EAD + 0) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGCESDOMG = positif(BPTPDIV) * positif(PVTAXSB) * positif(V_EAG + 0) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

regle 11181:
application : batch , iliad ;
 
LIG81 = positif(present(RDDOUP) + present(DONAUTRE) + present(REPDON03) + present(REPDON04) 
                + present(REPDON05) + present(REPDON06) + present(REPDON07) + present(COD7UH)
                + positif(EXCEDANTA))
        * LI[DGFIP][2017] * LI[DGFIP][2017] ;

regle 1111500:
application : iliad, batch ;

LI[DGFIP][2017]500 = positif((positif(IPMOND) * positif(present(IPTEFP)+positif(VARIPTEFP)*present(DEFZU))) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) * (1 - positif(DEFRIMOND)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]510 = positif((positif(IPMOND) * present(IPTEFN)) + positif(INDTEFF) * (1 - positif(TEFFREVTOT))) 
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) * (1 - positif(DEFRIMOND)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LI[DGFIP][2017]500YT = positif((positif(IPMOND) * positif(present(IPTEFP)+positif(VARIPTEFP)*present(DEFZU))) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	     * positif(positif(max(0,IPTEFP+DEFZU-IPTEFN))+positif(max(0,RMOND+DEFZU-DMOND)))* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) * positif(DEFRIMOND) * LI[DGFIP][2017] * LI[DGFIP][2017];

LI[DGFIP][2017]510YT =  positif(null(max(0,RMOND+DEFZU-DMOND))+null(max(0,IPTEFP+DEFZU-IPTEFN))) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) * positif(DEFRIMOND) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

regle 1111522:
application : iliad, batch ;
LI[DGFIP][2017]522 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * (1 - INDTXMOY) * V_CR2 * LI[DGFIP][2017] ;
regle 1111523:
application : batch, iliad;
LI[DGFIP][2017]523 = (1 - present(IND_TDR)) * null(V_REGCO - 4) * LI[DGFIP][2017] ;
regle 11175:
application : iliad, batch ;
LIG75 = (1 - INDTXMIN) * (1 - INDTXMOY) * (1 - (LI[DGFIP][2017]500+ LI[DGFIP][2017]500YT)) * (1 - (LI[DGFIP][2017]510+ LI[DGFIP][2017]510YT)) * INDREV1A8 * LI[DGFIP][2017] ;

LI[DGFIP][2017]545 = (1 - present(IND_TDR)) * INDTXMIN * positif(IND_REV) * LI[DGFIP][2017] ;

LI[DGFIP][2017]760 = (1 - present(IND_TDR)) * INDTXMOY * LI[DGFIP][2017] ;

LI[DGFIP][2017]546 = positif(PRODOM + PROGUY) * (1 - positif(V_EAD + V_EAG)) * LI[DGFIP][2017] ;

LI[DGFIP][2017]550 = (1 - present(IND_TDR)) * INDTXMOY * LI[DGFIP][2017] ;

LIG74 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * positif(LI[DGFIP][2017]500 + LI[DGFIP][2017]510 + LI[DGFIP][2017]500YT + LI[DGFIP][2017]510YT) * LI[DGFIP][2017] ;

regle 11180:
application : batch, iliad ;
LIG80 = positif(present(RDREP) + present(DONETRAN)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 11188:
application : iliad , batch ;
LIGRSOCREPR = positif(present(RSOCREPRISE)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1111740:
application : batch , iliad ;
LI[DGFIP][2017]740 = positif(RECOMP) * LI[DGFIP][2017] ;
regle 1111780:
application : batch , iliad ;
LI[DGFIP][2017]780 = positif(RDCOM + NBACT) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 111981:
application : batch, iliad;
LIG98B = positif(LIG80 + LIGFIPC + LIGFIPDOM + present(DAIDE)
                 + LIGDUFLOT + LIGPINEL + LIG7CY
                 + LIGREDAGRI + LIGFORET + LIGRESTIMO  
	         + LIGCINE + LIGRSOCREPR + LIGCOTFOR 
	         + present(PRESCOM[DGFIP][2017]000) + present(RDPRESREPORT) + present(FCPI) 
		 + present(DSOUFIP) + LIGRIRENOV + present(DFOREST) 
		 + present(DHEBE) + present(DSURV)
	         + LIGLOGDOM + LIGREPTOUR + LIGLOCHOTR
	         + LIGREPHA + LIGCREAT + LI[DGFIP][2017]780 + LI[DGFIP][2017]040 + LIG81 + LIGLOGSOC
	         + LIGDOMSOC1 
		 + somme (i=A,B,E,M,C,D,S,F,Z,N,T,X : LIGCELLi) + LIGCELMG + LIGCELMH
		 + somme (i=A,B,D,E,F,H,G,L,M,S,R,U,T,Z,X,W,V : LIGCELHi) 
                 + somme (i=U,X,T,S,W,P,L,V,K,J : LIGCELGi)
                 + somme (i=A,B,C,D,E,F,G,H,I,J,K,L : LIGCELYi)
		 + LIGCELHNO + LIGCELHJK + LIGCELNQ + LIGCELCOM + LIGCELNBGL
		 + LIGCEL + LIGCELJP + LIGCELJBGL + LIGCELJOQR + LIGCEL2012
                 + LIGCELFD + LIGCELFABC
                 + LIGILMPA + LIGILMPB + LIGILMPC + LIGILMPD + LIGILMPE
                 + LIGILMOA + LIGILMOB + LIGILMOC + LIGILMOD + LIGILMOE
		 + LIGREDMEUB + LIGREDREP + LIGILMIX + LIGILMIY + LIGINVRED + LIGILMIH  + LIGILMJC + LIGILMIZ 
                 + LIGILMJI + LIGILMJS + LIGMEUBLE + LIGPROREP + LIGREPNPRO + LIGMEUREP + LIGILMIC + LIGILMIB 
                 + LIGILMIA + LIGILMJY + LIGILMJX + LIGILMJW + LIGILMJV 
		 + LIGRESIMEUB + LIGRESINEUV + LIGRESIVIEU + LIGLOCIDEFG + LIGCODJTJU
                 + LIGCODOU
		 + present(DNOUV) + LIGLOCENT + LIGCOLENT + LIGRIDOMPRO
		 + LIGPATNAT1 + LIGPATNAT2 + LIGPATNAT3+LIGPATNAT4) 
           * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGRED = LIG98B * (1 - positif(RIDEFRI)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGREDYT = LIG98B * positif(RIDEFRI) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

regle 1111820:
application : batch , iliad ;

LI[DGFIP][2017]820 = positif(ABADO + ABAGU + RECOMP) * LI[DGFIP][2017] ;

regle 111106:
application : iliad , batch ;

LI[DGFIP][2017]06 = positif(RETIR) ;
LIGINRTAX = positif(RETTAXA) ;
LI[DGFIP][2017]0622 = positif(RETIR22) ;
LIGINRTAX22 = positif(RETTAXA22) ;
ZIG_INT22 = positif(RETCS22 + RETPS22 + RETR[DGFIP][2017]2 + RETCVN22) ;

LIGINRPCAP = positif(RETPCAP) ;
LIGINRPCA[DGFIP][2017] = positif(RETPCA[DGFIP][2017]2) ;
LIGINRLOY = positif(RETLOY) ;
LIGINRLOY2 = positif(RETLOY22) ;

LIGINRHAUT = positif(RETHAUTREV) ;
LIGINRHAUT2 = positif(RETCHR22) ;
regle 111107:
application : iliad, batch;

LIG_172810 = TYPE2 * positif(NMAJ1) ;

LIGTAXA17281 = TYPE2 * positif(NMAJTAXA1) ;

LIGPCA[DGFIP][2017]7281 = TYPE2 * positif(NMAJPCA[DGFIP][2017]) ;

LIGCHR17281 = TYPE2 * positif(NMAJCHR1) ;

LIG_NMAJ1 = TYPE2 * positif(NMAJ1) ;
LIG_NMAJ3 = TYPE2 * positif(NMAJ3) ;
LIG_NMAJ4 = TYPE2 * positif(NMAJ4) ;

LIGNMAJTAXA1 = TYPE2 * positif(NMAJTAXA1) ;
LIGNMAJTAXA3 = TYPE2 * positif(NMAJTAXA3) ;
LIGNMAJTAXA4 = TYPE2 * positif(NMAJTAXA4) ;

LIGNMAJPCA[DGFIP][2017] = TYPE2 * positif(NMAJPCA[DGFIP][2017]) ;
LIGNMAJPCAP3 = TYPE2 * positif(NMAJPCAP3) ;
LIGNMAJPCAP4 = TYPE2 * positif(NMAJPCAP4) ;
LIGNMAJLOY1 = TYPE2 * positif(NMAJLOY1) ;
LIGNMAJLOY3 = TYPE2 * positif(NMAJLOY3) ;
LIGNMAJLOY4 = TYPE2 * positif(NMAJLOY4) ;

LIGNMAJCHR1 = TYPE2 * positif(NMAJCHR1) ;
LIGNMAJCHR3 = TYPE2 * positif(NMAJCHR3) ;
LIGNMAJCHR4 = TYPE2 * positif(NMAJCHR4) ;

regle 11119:
application : batch, iliad;
LI[DGFIP][2017]09 = positif(IPSOUR + REGCI + LIGPVETR + LIGCULTURE + LIGMECENAT 
		  + LIGCORSE + LI[DGFIP][2017]305 + LIGEMPLOI + LIGC[DGFIP][2017]CK + LIGCICAP + LIGCI8XV + LIGCIGLO + LIGREGCI
		  + LIGBPLIB + LIGCIGE + LIGDEVDUR 
                  + LIGCICA + LIGCIGARD + LIG82
		  + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN
		  + LIGCREFAM + LIGCREAPP + LIGCREBIO  + LIGPRESINT + LIGCREPROSP + LIGINTER
		  + LIGRESTAU + LIGCONGA + LIGMETART 
		  + LIGCREFORM + LIGLOYIMP 
		  + LIGVERSLIB + LIGCITEC + INDLIGPPE
		   ) 
               * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGCRE[DGFIP][2017] = positif(REGCI + LIGPVETR + LIGCICAP + LIGREGCI + LIGCI8XV + LIGCIGLO + 0) 
	    * (1 - positif(IPSOUR + LIGCULTURE + LIGMECENAT + LIGCORSE + LI[DGFIP][2017]305 + LIGEMPLOI + LIGC[DGFIP][2017]CK + LIGBPLIB + LIGCIGE + LIGDEVDUR 
		           + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		           + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGRESTAU + LIGCONGA + LIGMETART
		           + LIGCREFORM + LIGLOYIMP + LIGVERSLIB + LIGCITEC + 0))
	    ;

LIGCRE[DGFIP][2017] = (1 - positif(REGCI + LIGPVETR + LIGCICAP + LIGREGCI + LIGCI8XV + LIGCIGLO + 0)) 
	    * positif(IPSOUR + LIGCULTURE + LIGMECENAT + LIGCORSE + LI[DGFIP][2017]305 + LIGEMPLOI + LIGC[DGFIP][2017]CK + LIGBPLIB + LIGCIGE + LIGDEVDUR 
		      + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		      + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGRESTAU + LIGCONGA + LIGMETART
		      + LIGCREFORM + LIGLOYIMP + LIGVERSLIB + LIGCITEC + 0)
	    ;

LIGCRED3 = positif(REGCI + LIGPVETR + LIGCICAP + LIGREGCI + LIGCI8XV + LIGCIGLO + 0) 
	    * positif(IPSOUR + LIGCULTURE + LIGMECENAT + LIGCORSE + LI[DGFIP][2017]305 + LIGEMPLOI + LIGC[DGFIP][2017]CK + LIGBPLIB + LIGCIGE + LIGDEVDUR
		      + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		      + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGRESTAU + LIGCONGA + LIGMETART
		      + LIGCREFORM + LIGLOYIMP + LIGVERSLIB + LIGCITEC + 0)
           ;
regle 11120:
application : batch, iliad ;

LIGPVETR = positif(present(CIIMPPRO) + present(CIIMPPRO2)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCICAP = present(PRELIBXT) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGREGCI = positif(present(REGCI) + present(COD8XY)) * positif(CICHR) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCI8XV = present(COD8XV) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCIGLO = positif(present(COD8XF) + present(COD8XG) + present(COD8XH)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGCULTURE = present(CIAQCUL) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGMECENAT = present(RDMECENAT) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCORSE = positif(present(CIINVCORSE) + present(IPREPCORSE) + present(CICORSENOW)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LI[DGFIP][2017]305 = positif(DIAV[DGFIP][2017]) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGEMPLOI = positif(COD8UW + COD8TL) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGC[DGFIP][2017]CK = positif(CO[DGFIP][2017]CK) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGBPLIB = present(RCMLIB) * LI[DGFIP][2017] * (1 - null(4-V_REGCO)) * LI[DGFIP][2017] ;
LIGCIGE = positif(RDTECH + RDEQPAHA) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGDEVDUR = positif(DDEVDUR) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCICA = positif(BAILOC98) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCIGARD = positif(DGARD) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIG82 = positif(present(RDSYVO) + present(RDSYCJ) + present(RDSYPP) ) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGPRETUD = positif(PRETUD+PRETUDANT) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGSALDOM = present(CREAIDE) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCIFORET = positif(BDCIFORET) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGHABPRIN = positif(present(PREHABT) + present(PREHABT1) + present(PREHABT2) + present(PREHABTN) 
                     + present(PREHABTN1) + present(PREHABTN2) + present(PREHABTVT)
                    ) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCREFAM = positif(CREFAM) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCREAPP = positif(CREAPP) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCREBIO = positif(CREAGRIBIO) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGPRESINT = positif(PRESINTER) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCREPROSP = positif(CREPROSP) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGINTER = positif(CREINTERESSE) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGRESTAU = positif(CRERESTAU) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCONGA = positif(CRECONGAGRI) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGMETART = positif(CREARTS) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCREFORM = positif(CREFORMCHENT) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGLOYIMP = positif(LOYIMP) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGVERSLIB = positif(AUTOVERSLIB) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGCITEC = positif(DTEC) * LI[DGFIP][2017] * LI[DGFIP][2017] ;

LIGCREAT = positif(DCREAT + DCREATHANDI) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1112030:
application : batch, iliad ;

LIGNRBASE = positif(present(NRINET) + present(NRBASE)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
LIGBASRET = positif(present(IMPRET) + present(BASRET)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1112332:
application :  iliad, batch ;
LIGAVFISC = positif(AVFISCOPTER) * LI[DGFIP][2017] * LI[DGFIP][2017] ; 
regle 1112040:
application : batch, iliad;
LI[DGFIP][2017]040 = positif(DNBE + RNBE + RRETU) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1112041:
application : iliad, batch ;
LIGRDCSG = positif(positif(V_BTCSGDED) + present(DCSG) + present(RCMSOC)) * LI[DGFIP][2017] * LI[DGFIP][2017] ;
regle 1111
