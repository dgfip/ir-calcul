#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
 
  ####   #    #    ##    #####      #     #####  #####   ######      #    
 #    #  #    #   #  #   #    #     #       #    #    #  #           #    #
 #       ######  #    #  #    #     #       #    #    #  #####       #    #
 #       #    #  ######  #####      #       #    #####   #           ######
 #    #  #    #  #    #  #          #       #    #   #   #                #
  ####   #    #  #    #  #          #       #    #    #  ######           #
regle 401:
application : bareme, iliad , batch  ;
IRB = IAM[DGFIP][2017]; 
IRB2 = IAM[DGFIP][2017] + TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES;
regle 40101:
application : iliad , batch  ;
KIR =   IAMD3 ;
regle 4011:
application : bareme , iliad , batch  ;
IAM[DGFIP][2017] = IBM13 ;
IAM[DGFIP][2017] = IBM23 ;
IAM[DGFIP][2017]TH = positif_ou_nul(IBM23 - SEUIL_61)*IBM23;
regle 40110:
application : bareme , iliad , batch  ;
IAMD3 = IBM33 - min(ACP3, IMPIM3);
regle 402112:
application : iliad , batch  ;
ANG3 = IAD32 - IAD31;
regle 40220:
application : iliad , batch  ;
ACP3 = max (0 ,
 somme (a=1..4: min(arr(CHENFa * TX_DPAEAV/100) , SEUIL_AVMAXETU)) - ANG3)
        * (1 - positif(V_CR2 + IPVLOC)) * positif(ANG3) * positif(IMPIM3);
regle 403:
application : bareme ,iliad , batch  ;

IBM13 = IA[DGFIP][2017]1 + ITP + REI + AUTOVERSSUP + TAXASSUR + IPCAPTAXTOT  + TAXLOY + CHRAPRES + AVFISCOPTER ;

IBM23 = IA[DGFIP][2017]1 + ITP + REI + AUTOVERSSUP + AVFISCOPTER ;

regle 404:
application : bareme , iliad , batch  ;
IBM33 = IAD31 + ITP + REI;
regle 4041:
application : iliad , batch  ;
DOMITPD = arr(BN1 + SPEPV + B[DGFIP][2017]2F + BA1) * (TX11/100) * positif(V_EAD);
DOMITPG = arr(BN1 + SPEPV + B[DGFIP][2017]2F + BA1) * (TX09/100) * positif(V_EAG);
DOMAVTD = arr((BN1 + SPEPV + B[DGFIP][2017]2F + BA1) * TX05/100) * positif(V_EAD);
DOMAVTG = arr((BN1 + SPEPV + B[DGFIP][2017]2F + BA1) * TX07/100) * positif(V_EAG);
DOMAVTO = DOMAVTD + DOMAVTG;
DOMABDB = max(PLAF_RABDOM - ABADO , 0) * positif(V_EAD)
          + max(PLAF_RABGUY - ABAGU , 0) * positif(V_EAG);
DOMDOM = max(DOMAVTO - DOMABDB , 0) * positif(V_EAD + V_EAG);
ITP = arr((BPT[DGFIP][2017] * TX225/100) 
       + (BPTPVT * TX19/100) 
       + (BPTP4 * TX30/100) 
       +  DOMITPD + DOMITPG
       + (BPTP3 * TX16/100) 
       + (BPTP40 * TX41/100)
       + DOMDOM * positif(V_EAD + V_EAG)
       + (BPT[DGFIP][2017]8 * TX18/100)
       + (BPTPSJ * TX19/100)
       + (BPT[DGFIP][2017]4 * TX24/100)
	  )
       * (1-positif(IPVLOC)) * (1 - positif(present(TAX1649)+present(RE168))); 
regle 40412:
application : iliad , batch  ;
REVTP = BPT[DGFIP][2017] +BPTPVT+BPTP4+BTP3A+BPTP40+ BPT[DGFIP][2017]4+BPT[DGFIP][2017]8 +BPTPSJ;
regle 40413:
application : iliad , batch  ;
BTP3A = (BN1 + SPEPV + B[DGFIP][2017]2F + BA1) * (1 - positif( IPVLOC ));
BPTPD = BTP3A * positif(V_EAD)*(1-positif(present(TAX1649)+present(RE168)));
BPTPG = BTP3A * positif(V_EAG)*(1-positif(present(TAX1649)+present(RE168)));
BPTP3 = BTP3A * (1 - positif(V_EAD + V_EAG))*(1-positif(present(TAX1649)+present(RE168)));
BTP3G = (BPVRCM) * (1 - positif( IPVLOC ));
BT[DGFIP][2017] = PEA * (1 - positif( IPVLOC ));
BPT[DGFIP][2017] = BT[DGFIP][2017]*(1-positif(present(TAX1649)+present(RE168)));
BTPVT = GAINPEA * (1 - positif( IPVLOC ));
BPTPVT = BTPVT*(1-positif(present(TAX1649)+present(RE168)));

BT[DGFIP][2017]8 = (BPV18V + BPV18C) * (1 - positif( IPVLOC ));
BPT[DGFIP][2017]8 = BT[DGFIP][2017]8 * (1-positif(present(TAX1649)+present(RE168))) ;

BPTP4 = (BPCOPTV + BPCOPTC + BPVSK) * (1 - positif(IPVLOC)) * (1 - positif(present(TAX1649) + present(RE168))) ;
BPTP4I = (BPCOPTV + BPCOPTC) * (1 - positif(IPVLOC)) * (1 - positif(present(TAX1649) + present(RE168))) ;
BTPSK = BPVSK * (1 - positif( IPVLOC ));
BPTPSK = BTPSK * (1-positif(present(TAX1649)+present(RE168))) ;

BTP40 = (BPV40V + BPV40C) * (1 - positif( IPVLOC )) ;
BPTP40 = BTP40 * (1-positif(present(TAX1649)+present(RE168))) ;

BT[DGFIP][2017] = PVIMPOS * (1 - positif( IPVLOC ));
BPT[DGFIP][2017] = BT[DGFIP][2017] * (1-positif(present(TAX1649)+present(RE168))) ;
BTPSJ = BPVSJ * (1 - positif( IPVLOC ));
BPTPSJ = BTPSJ * (1-positif(present(TAX1649)+present(RE168))) ;
BTPSB = PVTAXSB * (1 - positif( IPVLOC ));
BPTPSB = BTPSB * (1-positif(present(TAX1649)+present(RE168))) ;
BPT[DGFIP][2017]9 = (BPVSJ + GAINPEA) * (1 - positif( IPVLOC )) * (1 - positif(present(TAX1649) + present(RE168))) ;
BPT[DGFIP][2017]4 = RCM2FA *(1-positif(present(TAX1649)+present(RE168))) * (1 - positif(null(2-V_REGCO) + null(4-V_REGCO)));
ITPRCM = arr(BPT[DGFIP][2017]4 * TX24/100);

BPTPDIV = BT[DGFIP][2017] * (1-positif(present(TAX1649)+present(RE168))) ;

regle 4042:
application : iliad , batch  ;


REI = IPREP+IPPRICORSE;

regle 40421:
application : iliad , batch  ;


PPERSATOT = RSAFOYER + RSAPAC1 + RSAPAC2 ;

PPERSA = min(PPETOTX , PPERSATOT) * (1 - V_CNR) ;

PPEFINAL = PPETOTX - PPERSA ;

regle 405:
application : bareme , iliad , batch  ;


IA[DGFIP][2017]1 = ( max(0,IDOM11-DEC11-RED) *(1-positif(V_CR2+IPVLOC))
        + positif(V_CR2+IPVLOC) *max(0 , IDOM11 - RED) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * IDOM16;
regle 40510:
application : bareme , iliad , batch  ;
IREXITI = (present(FLAG_EXIT) * ((1-positif(FLAG_3WBNEG)) * abs(NAPTIR - V_NAPTIR3WB) 
           + positif(FLAG_3WBNEG) * abs(NAPTIR + V_NAPTIR3WB)) * positif(present(PVIMPOS)+present(CODRWB)))
          * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


IREXITS = (
           ((1-positif(FLAG_3WANEG)) * abs(V_NAPTIR3WA+V_NAPTIR3WB*positif(FLAG_3WBNEG)-V_NAPTIR3WB*(1-positif(FLAG_3WBNEG))) 
           + positif(FLAG_3WANEG) * abs(-V_NAPTIR3WA + V_NAPTIR3WB*positif(FLAG_3WBNEG)-V_NAPTIR3WB*(1-positif(FLAG_3WBNEG)))) * positif(present(PVIMPOS)+present(CODRWB))
           + ((1-positif(FLAG_3WANEG)) * abs(V_NAPTIR3WA-NAPTIR)
           + positif(FLAG_3WANEG) * abs(-V_NAPTIR3WA -NAPTIR)) * (1-positif(present(PVIMPOS)+present(CODRWB)))
          ) 
          * present(FLAG_EXIT) * positif(present(PVSURSI)+present(CODRWA))
          * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


IREXIT = IREXITI + IREXITS ;

EXITTAX3 = ((positif(FLAG_3WBNEG) * (-1) * ( V_NAPTIR3WB) + (1-positif(FLAG_3WBNEG)) * (V_NAPTIR3WB)) * positif(present(PVIMPOS)+present(CODRWB))
            + NAPTIR * positif(present(PVSURSI)+present(CODRWA)) * (1-positif(present(PVIMPOS)+present(CODRWB))))
           * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


PVCREA = PVSURSI + CODRWA ;

PVCREB = PVIMPOS + CODRWB ;

regle 406:
application : bareme , iliad , batch  ;
IAD31 = ((IDOM31-DEC31)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM31);
IAD32 = ((IDOM32-DEC32)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM32);

regle 4052:
application : bareme , iliad , batch  ;

IMPIM3 =  IAD31 ;

regle 4061:
application : bareme , iliad , batch  ;
pour z = 1,2:
DEC1z = ( min (max( arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - IDOM1z),0),IDOM1z) * (1 - V_ANC_BAR)
        + min (max( arr(SEUIL_DECOTEA/2 - (IDOM1z/2)),0),IDOM1z) * V_ANC_BAR)
        * (1 - V_CNR) ;

pour z = 1,2:
DEC3z = ( min (max( arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - IDOM3z),0),IDOM3z) * (1 - V_ANC_BAR)
        + min (max( arr(SEUIL_DECOTEA/2 - (IDOM3z/2)),0),IDOM3z) * V_ANC_BAR)
        * (1 - V_CNR) ;

DEC6 = ( min (max( arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - IDOM16),0),IDOM16) * (1 - V_ANC_BAR)
       + min (max( arr(SEUIL_DECOTEA/2 - (IDOM16/2)),0),IDOM16) * V_ANC_BAR)
       * (1 - V_CNR) ;

regle 407:
application : iliad   , batch ;
      
RED =  RCOTFOR + RSURV + RCOMP + RHEBE + RREPA + RDIFAGRI + RDONS
       + RDUFLOTOT + RPINELTOT
       + RCELTOT
       + RRESTIMO * (1-V_INDTEO)  + V_RRESTIMOXY * V_INDTEO
       + RFIPC + RFIPDOM + RAIDE + RNOUV + RPLAFREPME4
       + RTOURREP 
       + RTOUREPA + RTOUHOTR  
       + RLOGDOM + RLOGSOC + RDOMSOC1 + RLOCENT + RCOLENT
       + RRETU + RINNO + RRPRESCOMP + RFOR 
       + RSOUFIP + RRIRENOV + RSOCREPR + RRESIMEUB + RRESINEUV + RRESIVIEU 
       + RCODOU
       + RLOCIDEFG + RCODJT + RCODJU
       + RREDMEUB + RREDREP + RILMIX + RILMIY + RINVRED + RILMIH + RILMJC
       + RILMIZ + RILMJI + RILMJS + RMEUBLE + RPROREP + RREPNPRO + RREPMEU 
       + RILMIC + RILMIB + RILMIA + RILMJY + RILMJX + RILMJW + RILMJV
       + RILMOA + RILMOB + RILMOC + RILMOD + RILMOE
       + RILMPA + RILMPB + RILMPC + RILMPD + RILMPE
       + RIDOMPROE3   
       + RPATNAT1 + RPATNAT2 + RPATNAT3 + RPATNAT
       + RFORET + RCREAT + RCINE ;

REDTL = ASURV + ACOMP ;

CIMPTL = ATEC + ADEVDUR + TOTBGE ;

regle 4070:
application : bareme ;
RED = V_9UY;
regle 4025:
application : iliad , batch  ;

PLAFDOMPRO1 = max(0 , RR[DGFIP][2017]_1-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS-RDUFLOTOT-RPINELTOT-RNOUV
                          -RPLAFREPME4-RFOR-RTOURREP-RTOUHOTR-RTOUREPA-RCELTOT-RLOCNPRO
                          -RPATNATOT-RDOMSOC1-RLOGSOC ) ;
                          

RIDOMPROE3 = (RIDOMPROE3_1 * (1-ART1731BIS)
              + min(RIDOMPROE3_1 , max(RIDOMPROE3_P+RIDOMPROE3[DGFIP][2017], RIDOMPROE31731+0) * (1-PREM8_11)) * ART1731BIS) * (1-V_CNR);
                  


RIDOMPROTOT_1 = RIDOMPROE3_1 ;
RIDOMPROTOT = RIDOMPROE3 ;


RINVEST = RIDOMPROE3 ;


DIDOMPRO = ( RIDOMPRO * (1-ART1731BIS) 
             + min( RIDOMPRO, max(DIDOMPRO_P+DIDOMPRO[DGFIP][2017] , DIDOMPRO1731+0 )*(1-PREM8_11)) * ART1731BIS ) * (1 - V_CNR) ;

regle 40749:
application : iliad , batch  ;

DFORET = FORET ;

AFORET_1 = max(min(DFORET,LIM_FORET),0) * (1-V_CNR) ;

AFORET = max( 0 , AFORET_1  * (1-ART1731BIS) 
                  + min( AFORET_1 , max(AFORET_P + AFORET[DGFIP][2017] , AFORET1731+0) * (1-PREM8_11)) * ART1731BIS
            ) * (1-V_CNR) ;

RAFORET = arr(AFORET_1*TX_FORET/100) ;

RFORET_1 =  max( min( RAFORET , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1) , 0 ) ;

RFORET =  max( 0 , RFORET_1 * (1-ART1731BIS) 
                   + min( RFORET_1 , max( RFORET_P+RFORET[DGFIP][2017] , RFORET1731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

regle 4075:
application : iliad , batch ;

DFIPC = FIPCORSE ;

AFIPC_1 = max( min(DFIPC , LIM_FIPCORSE * (1 + BOOL_0AM)) , 0) * (1 - V_CNR) ;

AFIPC = max( 0, AFIPC_1 * (1-ART1731BIS)
                + min( AFIPC_1 , max( AFIPC_P + AFIPC[DGFIP][2017] , AFIPC1731+0 ) * (1-PREM8_11)) * ART1731BIS
           ) * (1 - V_CNR) ;

RFIPCORSE = arr(AFIPC_1 * TX_FIPCORSE/100) * (1 - V_CNR) ;

RFIPC_1 = max( min( RFIPCORSE , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1-RFORET_1-RFIPDOM_1) , 0) ;

RFIPC = max( 0, RFIPC_1 * (1 - ART1731BIS) 
                + min( RFIPC_1 , max(RFIPC_P+RFIPC[DGFIP][2017] , RFIPC1731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

regle 40751:
application : iliad , batch ;

DFIPDOM = FIPDOMCOM ;

AFIPDOM_1 = max( min(DFIPDOM , LIMFIPDOM * (1 + BOOL_0AM)) , 0) * (1 - V_CNR) ;

AFIPDOM = max( 0 , AFIPDOM_1 * (1 - ART1731BIS)
               + min( AFIPDOM_1 , max(AFIPDOM_P+ AFIPDOM[DGFIP][2017] , AFIPDOM1731+0) * (1-PREM8_11)) * ART1731BIS
	     ) * (1 - V_CNR) ;

RFIPDOMCOM = arr(AFIPDOM_1 * TXFIPDOM/100);

RFIPDOM_1 = max( min( RFIPDOMCOM , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1-RFORET_1),0);

RFIPDOM = max( 0 , RFIPDOM_1 * (1 - ART1731BIS) 
                   + min( RFIPDOM_1, max(RFIPDOM_P+RFIPDOM[DGFIP][2017] ,  RFIPDOM1731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

regle 4076:
application : iliad , batch  ;
BSURV = min( RDRESU , PLAF_RSURV + PLAF_COMPSURV * (EAC + V_0DN) + PLAF_COMPSURVQAR * (V_0CH + V_0DP) );

RRS = arr( BSURV * TX_REDSURV / 100 ) * (1 - V_CNR);

DSURV = RDRESU;

ASURV = (BSURV * (1-ART1731BIS)
         + min( BSURV , max( ASURV_P + ASURV[DGFIP][2017] , ASURV1731+0 ) * (1-PREM8_11)) * ART1731BIS
        )  * (1-V_CNR);

RSURV_1 = max( min( RRS , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1-RFORET_1-RFIPDOM_1-RFIPC_1
			              -RCINE_1-RRESTIMO_1-RSOCREPR_1-RRPRESCOMP_1-RHEBE_1 ) , 0 ) ;

RSURV = max( 0 , RSURV_1 * (1-ART1731BIS) 
                 + min( RSURV_1, max(RSURV_P+RSURV[DGFIP][2017] , RSURV1731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

regle 4100:
application : iliad , batch ;

RRCN = arr(  min( CINE1 , min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE )) * TX_CINE1/100
        + min( CINE2 , max( min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE ) - CINE1 , 0)) * TX_CINE2/100 
       ) * (1 - V_CNR) ;

DCINE = CINE1 + CINE2 ;

ACINE_1 = max(0,min( CINE1 + CINE2 , min( arr(SOFIRNG * TX_CINE3/100) , PLAF_CINE ))) * (1 - V_CNR) ;

ACINE = max( 0, ACINE_1 * (1-ART1731BIS) 
                + min( ACINE_1 , max(ACINE_P+ACINE[DGFIP][2017] , ACINE1731+0 ) * (1-PREM8_11)) * ART1731BIS
           ) * (1-V_CNR) ; 

RCINE_1 = max( min( RRCN , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1-RFORET_1-RFIPDOM_1-RFIPC_1) , 0 ) ;

RCINE = max( 0, RCINE_1 * (1-ART1731BIS) 
                + min( RCINE_1 , max(RCINE_P+RCINE[DGFIP][2017] , RCINE1731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

regle 4176:
application : iliad , batch  ;
BSOUFIP = min( FFIP , LIM_SOUFIP * (1 + BOOL_0AM));

RFIP = arr( BSOUFIP * TX_REDFIP / 100 ) * (1 - V_CNR);

DSOUFIP = FFIP;

ASOUFIP = (BSOUFIP * (1-ART1731BIS) 
           + min( BSOUFIP , max(ASOUFIP_P + ASOUFIP[DGFIP][2017] , ASOUFI[DGFIP][2017]731+0) * (1-PREM8_11)) * ART1731BIS
          ) * (1-V_CNR) ;

RSOUFIP_1 = max( min( RFIP , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1-RFORET_1-RFIPDOM_1-RFIPC_1
			   -RCINE_1-RRESTIMO_1-RSOCREPR_1-RRPRESCOMP_1-RHEBE_1-RSURV_1-RINNO_1) , 0 ) ;

RSOUFIP = max( 0 , RSOUFIP_1 * (1-ART1731BIS) 
                   + min( RSOUFIP_1 , max(RSOUFIP_P+RSOUFIP[DGFIP][2017] , RSOUFI[DGFIP][2017]731+0) * (1-PREM8_11)) * ART1731BIS ) ;

regle 4200:
application : iliad , batch  ;

BRENOV = min(RIRENOV,PLAF_RENOV) ;

RENOV = arr( BRENOV * TX_RENOV / 100 ) * (1 - V_CNR) ;

DRIRENOV = RIRENOV ;

ARIRENOV = (BRENOV * (1-ART1731BIS) 
            + min( BRENOV, max(ARIRENOV_P + ARIRENOV[DGFIP][2017] , ARIRENOV1731+0) * (1-PREM8_11)) * ART1731BIS 
           ) * (1 - V_CNR) ;

RRIRENOV_1 = max(min(RENOV , IDOM11-DEC11-RCOTFOR_1-RREPA_1-RAIDE_1-RDIFAGRI_1-RFORET_1-RFIPDOM_1-RFIPC_1-RCINE_1
			     -RRESTIMO_1-RSOCREPR_1-RRPRESCOMP_1-RHEBE_1-RSURV_1-RINNO_1-RSOUFIP_1) , 0 ) ;

RRIRENOV = max( 0 , RRIRENOV_1 * (1-ART1731BIS) 
                    + min(RRIRENOV_1 , max(RRIRENOV_P+RRIRENOV[DGFIP][2017] , RRIRENOV1731+0) * (1-PREM8_11)) * ART1731BIS ) ;

regle 40771:
application : iliad , batch  ;

RFC = min(RDCOM,PLAF_FRCOMPTA * max(1,NBACT)) * present(RDCOM)*(1-V_CNR);

NCOMP = ( max(1,NBACT)* present(RDCOM) * (1-ART1731BIS) + min( max(1,NBACT)* present(RDCOM) , NCOM[DGFIP][2017]731+0) * ART1731BIS ) * (1-V_CNR);

DCOMP = RDCOM;

ACOMP =  RFC * (1-ART1731BIS) 
         + min( RFC , max(ACOMP_P + ACOMP[DGFIP][2017] , ACOM[DGFIP][2017]731+0 ) * (1-PREM8_11)) * ART1731BIS ;

regle 10040771:
application :  iliad , batch  ;
RCOMP_1 = max(min( RFC , RR[DGFIP][2017]-RLOGDOM-RCREAT) , 0) ;

RCOMP = max( 0 , RCOMP_1 * (1-ART1731BIS) 
                 + min( RCOMP_1 ,max(RCOMP_P+RCOMP[DGFIP][2017] , RCOM[DGFIP][2017]731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

regle 4077:
application : iliad , batch  ;




DUFREPFI = DUFLOFI ;

DDUFLOGIH = DUFLOGI + DUFLOGH ;

DDUFLOEKL = DUFLOEK + DUFLOEL ;

DPIQABCD = PINELQA + PINELQB + PINELQC + PINELQD ;



ADUFREPFI = DUFLOFI * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ADUFLOEKL_1 = ( arr( min( DUFLOEL + 0, LIMDUFLO) / 9 ) 
              + arr( min( DUFLOEK + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + 0, LIMDUFLO)) / 9 )
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

ADUFLOEKL = ADUFLOEKL_1 * (1-ART1731BIS) ;


APIQABCD_1 = ( arr( min(PINELQD + 0, LIMDUFLO - min( DUFLOEL + 0, LIMDUFLO)) /9 ) 
                + arr( min(PINELQB + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + 0, LIMDUFLO)) / 9 ) 
                + arr(min( PINELQC + 0, LIMDUFLO - min( DUFLOEL + PINELQD + 0, LIMDUFLO)) /6 )
                + arr(min( PINELQA + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + PINELQB + 0, LIMDUFLO)) / 6)
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

APIQABCD = APIQABCD_1 * (1-ART1731BIS) ;

ADUFLOGIH_1 = ( arr( min( DUFLOGI + 0, LIMDUFLO) / 9 ) +
              arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) / 9 )
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

ADUFLOGIH =  ADUFLOGIH_1 * (1-ART1731BIS) 
             + min( ADUFLOGIH_1, max(ADUFLOGIH_P + ADUFLOGIH[DGFIP][2017] , ADUFLOGIH1731 +0 ) * (1-PREM8_11)) * ART1731BIS ;



RDUFLO_EKL = ( arr(arr( min( DUFLOEL + 0, LIMDUFLO) / 9 ) * (TX29/100))
              + arr(arr( min( DUFLOEK + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + 0, LIMDUFLO)) / 9 ) * (TX18/100))
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

RPI_QABCD = ( arr(arr( min(PINELQD + 0, LIMDUFLO - min( DUFLOEL + 0, LIMDUFLO)) /9 ) * (TX29/100)) 
                + arr(arr( min(PINELQB + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + 0, LIMDUFLO)) / 9 ) * (TX18/100)) 
                + arr(arr(min( PINELQC + 0, LIMDUFLO - min( DUFLOEL + PINELQD + 0, LIMDUFLO)) /6 ) * (TX23/100))
                + arr(arr(min( PINELQA + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + PINELQB + 0, LIMDUFLO)) / 6) * (TX12/100))
               ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;



RDUFLO_GIH = ( arr( arr( min( DUFLOGI + 0, LIMDUFLO) / 9 ) * (TX29/100)) +
              arr( arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) / 9 ) * (TX18/100))
             ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;


regle 40772:
application : iliad , batch  ;


RDUFREPFI = max( 0, min( ADUFREPFI , RR[DGFIP][2017]-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS)) ;

RDUFLOGIH_1 = max( 0, min( RDUFLO_GIH , RR[DGFIP][2017]-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS
                                            -RDUFREPFI)) ;

RDUFLOGIH = max( 0, RDUFLOGIH_1 * (1 - ART1731BIS) 
                    + min ( RDUFLOGIH_1 , max(RDUFLOGIH_P+RDUFLOGIH[DGFIP][2017] , RDUFLOGIH1731+0 ) * (1-PREM8_11)) * ART1731BIS ) ;

RDUFLOEKL_1 = max( 0, min( RDUFLO_EKL , RR[DGFIP][2017]-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS
                                            -RDUFREPFI-RDUFLOGIH)) ;

RDUFLOEKL = max( 0, RDUFLOEKL_1 * (1 - ART1731BIS))  ;

RPIQABCD_1 = max( 0, min( RPI_QABCD , RR[DGFIP][2017]-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS
                                            -RDUFREPFI-RDUFLOGIH-RDUFLOEKL)) ;

RPIQABCD = max( 0, RPIQABCD_1 * (1 - ART1731BIS)) ;

RDUFLOTOT = RDUFREPFI + RDUFLOGIH + RDUFLOEKL ;
RPINELTOT = RPIQABCD ;

regle 40773:
application : iliad , batch  ;

RIVDUEKL = ( arr( arr( min( DUFLOEL + 0, LIMDUFLO) / 9 ) * (TX29/100)) 
              + arr(arr( min( DUFLOEK + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + 0, LIMDUFLO)) / 9 ) * (TX18/100))
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

RIVDUEKL8 = max (0 , ( arr( min( DUFLOEL + 0, LIMDUFLO) * (TX29/100)) 
                         + arr( min( DUFLOEK + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + 0, LIMDUFLO)) * (TX18/100))
                        ) 
                          - 8 * RIVDUEKL  
                   ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ; 


RIVPIQBD =  ( arr(arr( min(PINELQD + 0, LIMDUFLO - min( DUFLOEL + 0, LIMDUFLO)) /9 ) * (TX29/100))
            + arr(arr( min(PINELQB + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + 0, LIMDUFLO)) / 9 ) * (TX18/100))
            ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ; 

RIVPIQBD8 = max (0 , ( arr( min(PINELQD + 0, LIMDUFLO - min( DUFLOEL + 0, LIMDUFLO)) * (TX29/100)) + 
                        arr( min(PINELQB + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + 0, LIMDUFLO)) * (TX18/100))
                    ) 
                          - 8 * RIVPIQBD  
                ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ; 

RIVPIQAC = ( arr(arr(min( PINELQC + 0, LIMDUFLO - min( DUFLOEL + PINELQD + 0, LIMDUFLO)) /6 ) * (TX23/100))
           + arr(arr(min( PINELQA + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + PINELQB + 0, LIMDUFLO)) / 6) * (TX12/100))
           ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

RIVPIQAC5 = max (0 , ( arr( min(PINELQC + 0, LIMDUFLO - min( DUFLOEL + PINELQD + 0, LIMDUFLO)) * (TX23/100)) + 
                        arr( min(PINELQA + 0, LIMDUFLO - min( DUFLOEL + PINELQD + PINELQC + DUFLOEK + PINELQB + 0, LIMDUFLO)) * (TX12/100))
                     ) 
                          - 5 * RIVPIQAC  
                ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ; 


RIVDUGIH = ( arr( arr( min( DUFLOGI + 0, LIMDUFLO) / 9 ) * (TX29/100)) +
                arr( arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) / 9 ) * (TX18/100))
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

RIVDUGIH8 = max (0 , ( arr( min( DUFLOGI + 0, LIMDUFLO) * (TX29/100)) +
                          arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) * (TX18/100))
                     ) 
                          - 8 * RIVDUGIH  
                   ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ; 

REPDUEKL = RIVDUEKL * 7 + RIVDUEKL8 ;
REPIQBD = RIVPIQBD * 7 + RIVPIQBD8 ;
REPIQAC = RIVPIQAC * 4 + RIVPIQAC5 ;
REPDUGIH = RIVDUGIH * 7 + RIVDUGIH8 ;

regle 4078:
application : iliad , batch  ;
BCEL_FABC = arr ( min( CELLIERFA + CELLIERFB + CELLIERFC , LIMCELLIER ) /9 );

BCEL_FD = arr ( min( CELLIERFD , LIMCELLIER ) /5 );

BCEL_2012 = arr( min(( CELLIERJA + CELLIERJD + CELLIERJE + CELLIERJF + CELLIERJH + CELLIERJJ 
		     + CELLIERJK + CELLIERJM + CELLIERJN + 0 ), LIMCELLIER ) /9 );

BCEL_JOQR = arr( min(( CELLIERJO + CELLIERJQ + CELLIERJR + 0 ), LIMCELLIER ) /5 );

BCEL_2011 = arr( min(( CELLIERNA + CELLIERNC + CELLIERND + CELLIERNE + CELLIERNF + CELLIERNH
		     + CELLIERNI + CELLIERNJ + CELLIERNK + CELLIERNM + CELLIERNN + CELLIERNO  + 0 ), LIMCELLIER ) /9 );

BCELCOM2011 = arr( min(( CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT + 0 ), LIMCELLIER ) /5 );

BCEL_NBGL = arr( min(( CELLIERNB + CELLIERNG + CELLIERNL + 0), LIMCELLIER ) /9 );

BCEL_NQ = arr( min(( CELLIERNQ + 0), LIMCELLIER ) /5 );

BCEL_JBGL = arr( min(( CELLIERJB + CELLIERJG + CELLIERJL + 0), LIMCELLIER ) /9 );

BCEL_JP = arr( min(( CELLIERJP + 0), LIMCELLIER ) /5 );


BCEL_HNO = arr ( min ((CELLIERHN + CELLIERHO + 0 ), LIMCELLIER ) /9 );
BCEL_HJK = arr ( min ((CELLIERHJ + CELLIERHK + 0 ), LIMCELLIER ) /9 );

BCEL_HL = arr ( min ((CELLIERHL + 0 ), LIMCELLIER ) /9 );
BCEL_HM = arr ( min ((CELLIERHM + 0 ), LIMCELLIER ) /9 );


DCELRREDLA = CELRREDLA;

ACELRREDLA = (CELRREDLA * (1-ART1731BIS) 
              + min (CELRREDLA, max(ACELRREDLA_P+ACELRREDLA[DGFIP][2017] , ACELRREDLA1731 +0)*(1-PREM8_11)) * ART1731BIS 
             ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLB = CELRREDLB;

ACELRREDLB = (CELRREDLB * (1-ART1731BIS) 
              + min (CELRREDLB, max(ACELRREDLB_P+ACELRREDLB[DGFIP][2017] , ACELRREDLB1731 +0)*(1-PREM8_11)) * ART1731BIS 
             ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLE = CELRREDLE;

ACELRREDLE = (CELRREDLE * (1-ART1731BIS)
              + min (CELRREDLE , max(ACELRREDLE_P+ACELRREDLE[DGFIP][2017] , ACELRREDLE1731 +0)*(1-PREM8_11)) * ART1731BIS 
             ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLN = CELRREDLN;

ACELRREDLN = (CELRREDLN * (1-ART1731BIS) 
              + min (CELRREDLN, max(ACELRREDLN_P+ACELRREDLN[DGFIP][2017] , ACELRREDLN1731 +0)*(1-PREM8_11)) * ART1731BIS 
             ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLM = CELRREDLM;

ACELRREDLM = (CELRREDLM * (1-ART1731BIS) 
              + min (CELRREDLM, max(ACELRREDLM_P+ACELRREDLM[DGFIP][2017] , ACELRREDLM1731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLC = CELRREDLC;

ACELRREDLC = (CELRREDLC * (1-ART1731BIS) 
              + min (CELRREDLC, max(ACELRREDLC_P+ACELRREDLC[DGFIP][2017] , ACELRREDLC1731 +0)*(1-PREM8_11)) * ART1731BIS 
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLD = CELRREDLD;

ACELRREDLD = (CELRREDLD * (1-ART1731BIS) 
              + min (CELRREDLD, max(ACELRREDLD_P+ACELRREDLD[DGFIP][2017] , ACELRREDL[DGFIP][2017]731 +0)*(1-PREM8_11)) * ART1731BIS 
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLS = CELRREDLS;

ACELRREDLS = (CELRREDLS * (1-ART1731BIS) 
              + min (CELRREDLS, max(ACELRREDLS_P+ACELRREDLS[DGFIP][2017] , ACELRREDLS1731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLT = CELRREDLT;

ACELRREDLT = (CELRREDLT * (1-ART1731BIS) 
              + min (CELRREDLT, max(ACELRREDLT_P+ACELRREDLT[DGFIP][2017] , ACELRREDLT1731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLF = CELRREDLF;

ACELRREDLF = (CELRREDLF * (1-ART1731BIS) 
              + min (CELRREDLF, max(ACELRREDLF_P+ACELRREDLF[DGFIP][2017] , ACELRREDL[DGFIP][2017]731 +0)*(1-PREM8_11)) * ART1731BIS 
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLZ = CELRREDLZ;

ACELRREDLZ = (CELRREDLZ * (1-ART1731BIS) 
              + min (CELRREDLZ, max(ACELRREDLZ_P+ACELRREDLZ[DGFIP][2017] , ACELRREDLZ1731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLX = CELRREDLX;

ACELRREDLX = (CELRREDLX * (1-ART1731BIS) 
              + min (CELRREDLX, max(ACELRREDLX_P+ACELRREDLX[DGFIP][2017] , ACELRREDLX1731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


DCELRREDMG = CELRREDMG;

ACELRREDMG = (CELRREDMG * (1-ART1731BIS) 
              + min (CELRREDMG, max(ACELRREDMG_P+ACELRREDMG[DGFIP][2017] , ACELRREDM[DGFIP][2017]731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDMH = CELRREDMH;

ACELRREDMH = (CELRREDMH * (1-ART1731BIS) 
              + min (CELRREDMH, max(ACELRREDMH_P+ACELRREDMH[DGFIP][2017] , ACELRREDMH1731 +0)*(1-PREM8_11)) * ART1731BIS
	     ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHS = CELREPHS; 


ACELREPHS = ( CELREPHS * (1 - ART1731BIS) 
             + min( CELREPHS , max(ACELREPHS_P+ACELREPHS[DGFIP][2017] , ACELREPHS1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 

DCELREPHR = CELREPHR ;    


ACELREPHR = ( CELREPHR * (1 - ART1731BIS) 
             + min( CELREPHR , max(ACELREPHR_P+ACELREPHR[DGFIP][2017] , ACELREPHR1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 


DCELREPHU = CELREPHU ; 


ACELREPHU = ( CELREPHU * (1 - ART1731BIS) 
             + min( CELREPHU , max(ACELREPHU_P+ACELREPHU[DGFIP][2017] , ACELREPHU1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 

DCELREPHT = CELREPHT; 


ACELREPHT = ( CELREPHT * (1 - ART1731BIS) 
             + min( CELREPHT , max(ACELREPHT_P+ACELREPHT[DGFIP][2017] , ACELREPHT1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 

DCELREPHZ = CELREPHZ; 


ACELREPHZ = ( CELREPHZ * (1 - ART1731BIS) 
             + min( CELREPHZ , max(ACELREPHZ_P+ACELREPHZ[DGFIP][2017] , ACELREPHZ1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 

DCELREPHX = CELREPHX; 


ACELREPHX = ( CELREPHX * (1 - ART1731BIS) 
             + min( CELREPHX , max(ACELREPHX_P+ACELREPHX[DGFIP][2017] , ACELREPHX1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 


DCELREPHW = CELREPHW; 


ACELREPHW = ( CELREPHW * (1 - ART1731BIS) 
             + min( CELREPHW , max(ACELREPHW_P+ACELREPHW[DGFIP][2017] , ACELREPHW1731 + 0 )*(1-PREM8_11)) * ART1731BIS 
            ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ; 

DCELREPHV = CELREPHV; 

ACELREPHV = ( CELREPHV * (1 - ART1731BIS) 
             + min( CELREPHV , max(ACELREPHV_P+ACELREPHV[DGFIP][2017] , ACELREPHV1731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHF = CELREPHF; 

ACELREPHF = ( CELREPHF * (1 - ART1731BIS) 
             + min( CELREPHF , max(ACELREPHF_P+ACELREPHF[DGFIP][2017] , ACELREPH[DGFIP][2017]731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHE = CELREPHE ;    

ACELREPHE = ( CELREPHE * (1 - ART1731BIS) 
             + min( CELREPHE , max(ACELREPHE_P+ACELREPHE[DGFIP][2017] , ACELREPHE1731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHD = CELREPHD; 

ACELREPHD = ( CELREPHD * (1 - ART1731BIS) 
             + min( CELREPHD , max(ACELREPHD_P+ACELREPHD[DGFIP][2017] , ACELREPH[DGFIP][2017]731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHH = CELREPHH; 

ACELREPHH = ( CELREPHH * (1 - ART1731BIS) 
             + min( CELREPHH , max(ACELREPHH_P+ACELREPHH[DGFIP][2017] , ACELREPHH1731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHG = CELREPHG; 

ACELREPHG = ( CELREPHG * (1 - ART1731BIS) 
             + min( CELREPHG , max(ACELREPHG_P+ACELREPHG[DGFIP][2017] , ACELREPH[DGFIP][2017]731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHB = CELREPHB; 

ACELREPHB = ( CELREPHB * (1 - ART1731BIS) 
             + min( CELREPHB , max(ACELREPHB_P+ACELREPHB[DGFIP][2017] , ACELREPHB1731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHA = CELREPHA; 

ACELREPHA = ( CELREPHA * (1 - ART1731BIS) 
             + min( CELREPHA , max(ACELREPHA_P+ACELREPHA[DGFIP][2017] , ACELREPHA1731 + 0 )*(1-PREM8_11)) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGU = CELREPGU; 

ACELREPGU = (CELREPGU * (1 - ART1731BIS) 
             + min( CELREPGU , max (ACELREPGU_P+ACELREPGU[DGFIP][2017] , ACELREPGU1731 + 0 )*(1-PREM8_11)) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGX = CE
