#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2011 
#au titre des revenus perçus en 2010. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
 #
 #     CHAPITRE 2. CALCUL DU NET A PAYER
 #
 #
 #
regle corrective 10801:
application : oceans, iliad;

TXINR = max(0,(NBMOIS * TXMOISRETARD2) + max(0,(NBMOIS2 * TXMOISRETARD2)));

TXINRRED = max(0,(NBMOIS * TXMOISRETARD2*TXMOISRED*2) + max(0,(NBMOIS2 * TXMOISRETARD2 * TXMOISRED * 2)));

regle corrective 1081:
application : oceans, iliad ;
IND_PASSAGE = positif(FLAG_DEFAUT + FLAG_RETARD) + IND_PASSAGE_A;
IND_PASSR9901 = 1 + IND_PASSR9901_A;
IRNIN_PA = IRNIN_INR * null(1 - IND_PASSAGE) + IRNIN_PA_A;
TXINR_PA = TXINR * null(1 - IND_PASSAGE) + TXINR_PA_A;
INRIR_RETDEF = (1 - IND_RJLJ) * FLAG_DEFAUT * ( 
             arr(IRNIN_INR * TXINR / 100) * positif(IRNIN_INR) * null(1 - IND_PASSAGE) 
             + INRIR_RETDEF_A* (1-positif(ACODELAISINR))
		  + arr(max(0,IRNIN_PA - ACODELAISINR) * TXINR_PA/100) * positif(IND_PASSAGE -1)* positif(ACODELAISINR)
                                );
INR_IR_TARDIF = ((arr(IRNIN_INR * TXINR/100) * positif(IRNIN_INR) * null(1-IND_PASSAGE)+ INR_IR_TARDIF_A*(1-positif(ACODELAISINR)))
		  + arr(max(0,IRNIN_PA - ACODELAISINR) * TXINR_PA/100) * positif(IND_PASSAGE -1)* positif(ACODELAISINR)) * FLAG_RETARD * (1-IND_RJLJ);
CSG_PA = CSG * null(1 - IND_PASSAGE) + CSG_PA_A;
INRCSG_RETDEF = (1 - IND_RJLJ) * (
                arr((CSG-CSGIM) * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCSG_RETDEF_A;
INR_CSG_TARDIF = (arr((CSG-CSGIM) * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CSG_TARDIF_A) * (1-IND_RJLJ);
PRS_PA = PRS * null(1 - IND_PASSAGE) + PRS_PA_A;
INRPRS_RETDEF = (1 - IND_RJLJ) * (
             arr((PRS-PRSPROV) * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRPRS_RETDEF_A;
INR_PS_TARDIF = (arr((PRS-PRSPROV) * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_PS_TARDIF_A) * (1-IND_RJLJ);
CRDS_PA = RDSN * null(1 - IND_PASSAGE) + CRDS_PA_A;
INRCRDS_RETDEF = (1 - IND_RJLJ) * (
             arr((RDSN-CRDSIM) * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCRDS_RETDEF_A;
INR_CRDS_TARDIF = (arr((RDSN-CRDSIM) * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CRDS_TARDIF_A) * (1-IND_RJLJ);
TAXA_PA = TAXABASE * null(1 - IND_PASSAGE) + TAXA_PA_A;
INRTAXA_RETDEF = (1 - IND_RJLJ) * (
               arr(TAXABASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRTAXA_RETDEF_A;
INR_TAXAGA_TARDIF = (arr(TAXABASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_TAXA_TARDIF_A) * (1-IND_RJLJ);
CSAL_PA = CSALBASE * null(1 - IND_PASSAGE) + CSAL_PA_A;
INRCSAL_RETDEF = (1 - IND_RJLJ) * (
               arr(CSALBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCSAL_RETDEF_A;
INR_CSAL_TARDIF = (arr(CSALBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CSAL_TARDIF_A) * (1-IND_RJLJ);
CDIS_PA = CDISBASE * null(1 - IND_PASSAGE) + CDIS_PA_A;
INRCDIS_RETDEF = (1 - IND_RJLJ) * (
               arr(CDISBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCDIS_RETDEF_A;
INR_CDIS_TARDIF = (arr(CDISBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CDIS_TARDIF_A) * (1-IND_RJLJ);
regle corrective 10811:
application : oceans, iliad ;
IRNIN_TLDEC_1=IRNIN_INR;
CSG_TLDEC_1=CSG;
PRS_TLDEC_1=PRS;
RDS_TLDEC_1=RDSN;
TAXA_TLDEC_1=TAXASSUR;
CSAL_TLDEC_1=CSAL;
CDIS_TLDEC_1=CDIS;
INRIR_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2-FLAG_INR) * (positif(IRNIN_INR - IRNIN_R99R)
                       * (
            (positif(IRNIN_INR - IRNIN_REF+0)
            * arr((IRNIN_INR - IRNIN_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - IRNIN_REF+0)
            * positif(IRNIN_INR-IRNIN_RECT)
            * arr((IRNIN_INR - IRNIN_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRIR_RETDEF * null(IND_PASSAGE - 1)
                            )
            + null(3 - FLAG_INR) * (positif(IRNIN_INR - IRNIN_REF)
                       * (
            (arr((min(IRNIN_INR,IRNIN_TLDEC_1) - IRNIN_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR-IRNIN_RECT)
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - IRNIN_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRIR_RETDEF * null(IND_PASSAGE - 1)
                         )   )
             ;
INRCSG_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		      null(2-FLAG_INR) * (positif(CSG-CSG_R99R) * (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - CSG_REF+0) 
            * arr((CSG - CSG_REF-CSGIM) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - CSG_REF+0)
            * arr((CSG - CSG_REF-CSGIM) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
            + INRCSG_RETDEF * null(IND_PASSAGE - 1)
                            )
            + null(3 - FLAG_INR) * (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - CSG_REF)
                       * (
            (arr((min(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2),CSG_TLDEC_1) - CSG_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)-CSG_RECT)
            * arr((min(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2),CSG_TLDEC_1) - CSG_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRCSG_RETDEF * null(IND_PASSAGE - 1)
                         )   )
             ;
INRPRS_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		      null(2-FLAG_INR) * (positif(PRS-PRS_R99R) * (
            (positif(PRS * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - PRS_REF+0)
            * arr((PRS - PRS_REF-PRSPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - PRS_REF+0) * null(2 - FLAG_INR)
            * arr((PRS - PRS_REF-PRSPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
            + INRPRS_RETDEF * null(IND_PASSAGE - 1)
                            )
            + null(3 - FLAG_INR) * (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - PRS_REF)
                       * (
            (arr((min(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2),PRS_TLDEC_1) - PRS_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)-PS_RECT)
            * arr((min(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2),PRS_TLDEC_1) - PRS_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRPRS_RETDEF * null(IND_PASSAGE - 1)
                         )   )
             ;

INRCRDS_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		      null(2-FLAG_INR) * (positif(RDSN-RDS_R99R) * (
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - RDS_REF+0)
            * arr((RDSN - RDS_REF-CRDSIM) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RDSN* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - RDS_REF+0)
            * arr((RDSN - RDS_REF-CRDSIM) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
            + INRCRDS_RETDEF * null(IND_PASSAGE - 1)
                            )
            + null(3 - FLAG_INR) * (positif(RDSN* positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - RDS_REF)
                       * (
            (arr((min(RDSN* positif(CSG+PRS+RDSN-SEUIL_REC_CP2),RDS_TLDEC_1) - RDS_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RDSN* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)-CRDS_RECT)
            * arr((min(RDSN* positif(CSG+PRS+RDSN-SEUIL_REC_CP2),RDS_TLDEC_1) - RDS_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRCRDS_RETDEF * null(IND_PASSAGE - 1)
                         )   )
             ;
INRTAXA_NTL = (1 - IND_RJLJ)  * (1-FLAG_NINR)* 
	           ( 
		   null(2-FLAG_INR) * (positif(TAXABASE - TAXA_R99R) 
		 * (
             (positif(TAXABASE - TAXA_REF+0) * null(2 - FLAG_INR)
            * arr((TAXABASE - TAXA_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(TAXABASE - TAXA_REF+0) * null(2 - FLAG_INR)
            * arr((TAXABASE - TAXA_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRTAXA_RETDEF * null(IND_PASSAGE - 1)
                            )
	    +        null(3-FLAG_INR) * (positif(TAXABASE - TAXA_R99R) 
		 * (
             (positif(TAXABASE - TAXA_REF+0) * null(2 - FLAG_INR)
            * arr((min(TAXABASE,TAXA_TLDEC) - TAXA_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(TAXABASE - TAXA_REF+0) * null(2 - FLAG_INR)
            * arr((min(TAXABASE,TAXA_TLDEC) - TAXA_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRTAXA_RETDEF * null(IND_PASSAGE - 1)
                            )
			    )
             ;
INRCSAL_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		      null(2-FLAG_INR) * (positif(CSAL-CSAL_R99R) * (
            (positif(CSAL - CSAL_REF-CSALPROV+0)
            * arr((CSAL - CSAL_REF-CSALPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSAL - CSAL_REF-CSALPROV+0)
            * arr((CSAL - CSAL_REF-CSALPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
            + INRCSAL_RETDEF * null(IND_PASSAGE - 1)
                            )
            + null(3 - FLAG_INR) * (positif(CSAL - CSAL_REF)
                       * (
            (arr((min(CSAL,CSAL_TLDEC_1) - CSAL_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSAL-CSAL_RECT)
            * arr((min(CSAL,CSAL_TLDEC_1) - CSAL_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRCSAL_RETDEF * null(IND_PASSAGE - 1)
                         )   )
             ;
INRCDIS_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		      null(2-FLAG_INR) * (positif(CDIS-CDIS_R99R) * (
            (positif(CDIS - CDIS_REF+0)
            * arr((CDIS - CDIS_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CDIS - CDIS_REF-CSALPROV+0)
            * arr((CDIS - CDIS_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
            + INRCDIS_RETDEF * null(IND_PASSAGE - 1)
                            )
            + null(3 - FLAG_INR) * (positif(CDIS - CDIS_REF)
                       * (
            (arr((min(CDIS,CDIS_TLDEC_1) - CDIS_REF) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CDIS-CDIS_RECT)
            * arr((min(CDIS,CDIS_TLDEC_1) - CDIS_REF) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRCDIS_RETDEF * null(IND_PASSAGE - 1)
                         )   )
             ;
regle corrective 108111:
application : oceans, iliad ;
INRIR_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2-FLAG_INR) * positif(IRNIN_INR - IRNIN_R99R)
                       * (
            (positif(IRNIN_INR - max(IRNIN_REF+0,IRNIN_NTLDEC)) 
            * arr((IRNIN_INR - max(IRNIN_REF,IRNIN_NTLDEC)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - max(IRNIN_NTLDEC,IRNIN_REF+0))
            * positif(IRNIN_INR-max(IRNIN_NTLDEC,IRNIN_RECT))
            * arr((IRNIN_INR - max(IRNIN_NTLDEC,IRNIN_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
                   +  null(3-FLAG_INR) * positif(IRNIN_INR - IRNIN_REF)
                       * (
            (positif(IRNIN_INR - max(IRNIN_REF+0,IRNIN_NTLDEC)) 
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - max(IRNIN_REF,IRNIN_NTLDEC)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - max(IRNIN_NTLDEC,IRNIN_REF+0))
            * positif(IRNIN_INR-max(IRNIN_NTLDEC,IRNIN_RECT))
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - max(IRNIN_NTLDEC,IRNIN_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
                                                )
                                               ;
INRCSG_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2 - FLAG_INR) * positif(CSG-CSG_R99R) 
			* (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(CSG_NTLDEC,CSG_REF+0))
            * arr((CSG - max(CSG_NTLDEC,CSG_REF-CSGIM)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - max(CSG_NTLDEC,CSG_REF+0))
            * arr((CSG - max(CSG_NTLDEC,CSG_REF)-CSGIM) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
                      + null(3 - FLAG_INR) * positif(CSG-CSG_REF) 
			* (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(CSG_NTLDEC,CSG_REF+0))
            * arr((min(CSG,CSG_TLDEC_1) - max(CSG_NTLDEC,CSG_REF-CSGIM)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - max(CSG_NTLDEC,CSG_REF+0))
            * arr((min(CSG,CSG_TLDEC_1) - max(CSG_NTLDEC,CSG_REF)-CSGIM) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
                            )
             ;
INRPRS_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			   null(2 - FLAG_INR) * positif(PRS-PRS_R99R) 
			   * (
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - max(PRS_NTLDEC,PRS_REF+0)) 
            * arr((PRS  - max(PRS_NTLDEC,PRS_REF-PRSPROV)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(PRS_NTLDEC,PRS_REF+0))
            * arr((PRS - max(PRS_NTLDEC,PRS_REF)-PRSPROV) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                        )
                        + null(3 - FLAG_INR) * positif(PRS-PRS_REF) 
			   * (
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_REC_CP2)  - max(PRS_NTLDEC,PRS_REF+0)) 
            * arr((min(PRS,PRS_TLDEC_1)  - max(PRS_NTLDEC,PRS_REF-PRSPROV)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(PRS_NTLDEC,PRS_REF+0))
            * arr((min(PRS,PRS_TLDEC_1) - max(PRS_NTLDEC,PRS_REF)-PRSPROV) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                        )
                            )
             ;
INRCRDS_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(RDSN - RDS_R99R) 
		      * (
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(CRDS_NTLDEC,RDS_REF+0))
            * arr((RDSN - max(CRDS_NTLDEC,RDS_REF)-CRDSIM) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(CRDS_NTLDEC,RDS_REF+0))
            * arr((RDSN -max(CRDS_NTLDEC,RDS_REF)-CRDSIM) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                         )
                    +  null(3 - FLAG_INR) * positif(RDSN - RDS_REF) 
		      * (
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(CRDS_NTLDEC,RDS_REF+0))
            * arr((min(RDSN,RDS_TLDEC_1) - max(CRDS_NTLDEC,RDS_REF)-CRDSIM) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_REC_CP2) - max(CRDS_NTLDEC,RDS_REF+0))
            * arr((min(RDSN,RDS_TLDEC_1) -max(CRDS_NTLDEC,RDS_REF)-CRDSIM) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                         )
                            )
             ;
INRTAXA_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		     null(2 - FLAG_INR) * positif(TAXABASE - TAXA_R99R) 
		     * (
             (positif(TAXABASE - max(TAXA_NTLDEC,TAXA_REF+0))
            * arr((TAXABASE - max(TAXA_NTLDEC,TAXA_REF)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(TAXABASE - max(TAXA_NTLDEC,TAXA_REF+0))
            * arr((TAXABASE - max(TAXA_NTLDEC,TAXA_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		     + null(3 - FLAG_INR) * positif(TAXABASE - TAXA_REF) 
		     * (
             (positif(TAXABASE - max(TAXA_NTLDEC,TAXA_REF+0))
            * arr((min(TAXABASE,TAXA_TLDEC_1) - max(TAXA_NTLDEC,TAXA_REF)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(TAXABASE - max(TAXA_NTLDEC,TAXA_REF+0))
            * arr((min(TAXABASE,TAXA_TLDEC_1) - max(TAXA_NTLDEC,TAXA_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
                            )
	     ; 
INRCSAL_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(CSALBASE - CSAL_R99R) 
		       * (
             (positif(CSALBASE - max(CSAL_NTLDEC,CSAL_REF+0))
            * arr((CSALBASE - max(CSAL_NTLDEC,CSAL_REF)-CSALPROV) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSALBASE - max(CSAL_NTLDEC,CSAL_REF+0)) 
            * arr((CSALBASE - max(CSAL_NTLDEC,CSAL_REF)-CSALPROV) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(CSALBASE - CSAL_REF) 
		       * (
             (positif(CSALBASE - max(CSAL_NTLDEC,CSAL_REF+0)-CSALPROV)
            * arr((min(CSALBASE,CSAL_TLDEC_1) - max(CSAL_NTLDEC,CSAL_REF)-CSALPROV) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSALBASE - max(CSAL_NTLDEC,CSAL_REF-CSALPROV+0))
            * arr((min(CSALBASE,CSAL_TLDEC_1) - max(CSAL_NTLDEC,CSAL_REF)-CSALPROV) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
                            )
	     ; 
INRCDIS_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(CDISBASE - CDIS_R99R) 
		       * (
             (positif(CDISBASE - max(CDIS_NTLDEC,CDIS_REF+0))
            * arr((CDISBASE - max(CDIS_NTLDEC,CDIS_REF)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CDISBASE - max(CDIS_NTLDEC,CDIS_REF+0)) 
            * arr((CDISBASE - max(CDIS_NTLDEC,CDIS_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(CDISBASE - CDIS_REF) 
		       * (
             (positif(CDISBASE - max(CDIS_NTLDEC,CDIS_REF+0))
            * arr((min(CDISBASE,CDIS_TLDEC_1) - max(CDIS_NTLDEC,CDIS_REF)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CDISBASE - max(CDIS_NTLDEC,CDIS_REF+0))
            * arr((min(CDISBASE,CDIS_TLDEC_1) - max(CDIS_NTLDEC,CDIS_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
                            )
	     ; 
regle corrective 1082:
application : oceans, iliad ;
INRIR_TLACQ = positif(IRNIN_INR - max(max(IRNIN_REF,IRNIN_RECT),IRNIN_NTLDEC_1+0)) * null(3-FLAG_INR)
            * arr((IRNIN_INR - max(max(IRNIN_REF,IRNIN_RECT),IRNIN_NTLDEC_1)) * (TXINR / 100));
INRIR_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRIR_TLACQ;
INRCSG_TLACQ = positif(CSG - max(CSG_REF,CSG_NTLDEC_1+0)) * null(3 - FLAG_INR)
            * arr((CSG - max(CSG_REF,CSG_NTLDEC_1)-CSGIM*(1-positif(CSG_A))) * (TXINR / 100));
INRCSG_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCSG_TLACQ;
INRPRS_TLACQ = positif(PRS - max(PRS_REF,PRS_NTLDEC_1+0)) * null(3 - FLAG_INR)
            * arr((PRS - max(PRS_REF,PRS_NTLDEC_1)-PRSPROV*(1-positif(PRS_A))) * (TXINR / 100))  ;
INRPRS_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRPRS_TLACQ;
INRCRDS_TLACQ = positif(RDSN - max(RDS_REF,CRDS_NTLDEC_1+0)) * null(3 - FLAG_INR)
            * arr((RDSN - max(RDS_REF,CRDS_NTLDEC_1)-CRDSIM*(1-positif(RDS_A))) * (TXINR / 100))  ;
INRCRDS_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCRDS_TLACQ;
INRTAXA_TLACQ = positif(TAXABASE - max(TAXA_REF,TAXA_NTLDEC_1+0))*null(3- FLAG_INR)
            * arr((TAXABASE - max(TAXA_REF,TAXA_NTLDEC_1)) * (TXINR / 100))  ;
INRTAXA_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRTAXA_TLACQ;
INRCSAL_TLACQ = positif(CSALBASE - max(CSAL_REF,CSAL_NTLDEC_1+0))*null(3- FLAG_INR)
            * arr((CSALBASE - max(CSAL_REF,CSAL_NTLDEC_1)-CSALPROV*(1-positif(CSAL_A))) * (TXINR / 100))  ;
INRCSAL_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCSAL_TLACQ;
INRCDIS_TLACQ = positif(CDISBASE - max(CDIS_REF,CDIS_NTLDEC_1+0))*null(3- FLAG_INR)
            * arr((CDISBASE - max(CDIS_REF,CDIS_NTLDEC_1)) * (TXINR / 100))  ;
INRCDIS_TLA = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCDIS_TLACQ;
regle corrective 10821:
application : oceans, iliad ;
INRIR_TLACQ_1 = positif(IRNIN_INR - max(max(IRNIN_REF,IRNIN_RECT),IRNIN_TLDEC+0)) * null(3-FLAG_INR)
            * arr((IRNIN_INR - max(max(IRNIN_REF,IRNIN_RECT),IRNIN_TLDEC)) * (TXINRRED / 200)) * (1-positif(FLAG_C22+FLAG_C02))
	    +
               positif(IRNIN_INR - IRNIN_TLDEC) * null(3-FLAG_INR)
            * arr((IRNIN_INR - IRNIN_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRIR_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRIR_TLACQ_1;
INRCSG_TLACQ_1 = positif(CSG - max(CSG_REF,CSG_TLDEC+0)) * null(3 - FLAG_INR)
            * arr((CSG - max(CSG_REF,CSG_TLDEC)-CSGIM*(1-positif(CSG_A))) * (TXINRRED / 200)) * (1 - positif(FLAG_C22+FLAG_C02))
	    +
               positif(CSG - CSG_TLDEC) * null(3-FLAG_INR)
            * arr((CSG - CSG_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRCSG_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCSG_TLACQ_1;
INRPRS_TLACQ_1 = positif(PRS - max(PRS_REF,PRS_TLDEC+0)) * null(3 - FLAG_INR)
            * arr((PRS - max(PRS_REF,PRS_TLDEC)-PRSPROV*(1-positif(PRS_A))) * (TXINRRED / 200))*(1-positif(FLAG_C22+FLAG_C02))
	    +
               positif(PRS - PRS_TLDEC) * null(3-FLAG_INR)
            * arr((PRS - PRS_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRPRS_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRPRS_TLACQ_1;
INRCRDS_TLACQ_1 = positif(RDSN - max(RDS_REF,RDS_TLDEC+0)) * null(3 - FLAG_INR)
            * arr((RDSN - max(RDS_REF,RDS_TLDEC)-CRDSIM*(1-positif(RDS_A))) * (TXINRRED / 200))* (1-positif(FLAG_C22+FLAG_C02))
	    +
               positif(RDSN - RDS_TLDEC) * null(3-FLAG_INR)
            * arr((RDSN - RDS_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRCRDS_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCRDS_TLACQ_1;
INRTAXA_TLACQ_1 = positif(TAXABASE - max(TAXA_REF*null(TAXABASE-TAXABASE_A),TAXA_TLDEC+0))*null(3- FLAG_INR)
            * arr((TAXABASE - max(TAXA_REF*null(TAXABASE-TAXABASE_A),TAXA_TLDEC)) * (TXINRRED / 200))
	    +
               positif(TAXABASE - TAXA_TLDEC) * null(3-FLAG_INR)
            * arr((TAXABASE - TAXA_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRTAXA_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRTAXA_TLACQ_1;
INRCSAL_TLACQ_1 = positif(CSALBASE - max(CSAL_REF*null(CSALBASE-CSALBASE_A),CSAL_TLDEC+0))*null(3- FLAG_INR)
            * arr((CSALBASE - max(CSAL_REF*null(CSALBASE-CSALBASE_A),CSAL_TLDEC)-CSALPROV*(1-positif(CSAL_A))) * (TXINRRED / 200))
	    +
               positif(CSALBASE - CSAL_TLDEC) * null(3-FLAG_INR)
            * arr((CSALBASE - CSAL_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRCSAL_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCSAL_TLACQ_1;
INRCDIS_TLACQ_1 = positif(CDISBASE - max(CDIS_REF*null(CDISBASE-CDISBASE_A),CDIS_TLDEC+0))*null(3- FLAG_INR)
            * arr((CDISBASE - max(CDIS_REF*null(CDISBASE-CDISBASE_A),CDIS_TLDEC)) * (TXINRRED / 200))
	    +
               positif(CDISBASE - CDIS_TLDEC) * null(3-FLAG_INR)
            * arr((CDISBASE - CDIS_TLDEC) * (TXINRRED / 200)) * positif(FLAG_C22+FLAG_C02);
INRCDIS_TLA_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * INRCDIS_TLACQ_1;
regle corrective 1083:
application : oceans, iliad ;
INRIR_TLADEC_1 = INRIR_TLACQ_1;
INRIR_TL_1_AD=INRIR_TL_1_A;
INRCSG_TLADEC_1 = INRCSG_TLACQ_1;
INRCSG_TL_1_AD = INRCSG_TL_1_A;
INRPRS_TLADEC_1 = INRPRS_TLACQ_1;
INRPRS_TL_1_AD = INRPRS_TL_1_A;
INRCRDS_TLADEC_1 = INRCRDS_TLACQ_1;
INRCRDS_TL_1_AD = INRCRDS_TL_1_A;
INRTAXA_TLADEC_1 = INRTAXA_TLACQ_1;
INRTAXA_TL_1_AD = INRTAXA_TL_1_A;
INRCSAL_TLADEC_1 = INRCSAL_TLACQ_1;
INRCSAL_TL_1_AD = INRCSAL_TL_1_A;
INRCDIS_TLADEC_1 = INRCDIS_TLACQ_1;
INRCDIS_TL_1_AD = INRCDIS_TL_1_A;
INRIR_TLDEC_1 = INRIR_TLA_1+INRIR_RETDEF*null(INRIR_RETDEF_A);
INRCSG_TLDEC_1 = INRCSG_TLA_1 + INRCSG_RETDEF * null(INRCSG_RETDEF_A);
INRPRS_TLDEC_1 = INRPRS_TLA_1 + INRPRS_RETDEF * null(INRPRS_RETDEF_A);
INRCRDS_TLDEC_1 = INRCRDS_TLA_1 + INRCRDS_RETDEF * null(INRCRDS_RETDEF_A);
INRTAXA_TLDEC_1 = INRTAXA_TLA_1 + INRTAXA_RETDEF * null(INRTAXA_RETDEF_A);
INRCSAL_TLDEC_1 = INRCSAL_TLA_1 + INRCSAL_RETDEF * null(INRCSAL_RETDEF_A);
INRCDIS_TLDEC_1 = INRCDIS_TLA_1 + INRCDIS_RETDEF * null(INRCDIS_RETDEF_A);
INRIR_NTLDECD = INRIR_NTLDEC * positif_ou_nul(IRNIN_TLDEC_1 - IRNIN_NTLDEC) + INRIR_NTL *positif(IRNIN_NTLDEC-IRNIN_TLDEC_1);
INRCSG_NTLDECD = INRCSG_NTLDEC * positif_ou_nul(CSG_TLDEC_1 - CSG_NTLDEC) + INRCSG_NTL *positif(CSG_NTLDEC-CSG_TLDEC_1);
INRCRDS_NTLDECD = INRCRDS_NTLDEC * positif_ou_nul(RDS_TLDEC_1 - CRDS_NTLDEC) + INRCRDS_NTL *positif(CRDS_NTLDEC-RDS_TLDEC_1);
INRPRS_NTLDECD = INRPRS_NTLDEC * positif_ou_nul(PRS_TLDEC_1 - PRS_NTLDEC) + INRPRS_NTL *positif(PRS_NTLDEC-PRS_TLDEC_1);
INRCSAL_NTLDECD = INRCSAL_NTLDEC * positif_ou_nul(CSAL_TLDEC_1 - CSAL_NTLDEC) + INRCSAL_NTL *positif(CSAL_NTLDEC-CSAL_TLDEC_1);
INRCDIS_NTLDECD = INRCDIS_NTLDEC * positif_ou_nul(CDIS_TLDEC_1 - CDIS_NTLDEC) + INRCDIS_NTL *positif(CDIS_NTLDEC-CDIS_TLDEC_1);
INRTAXA_NTLDECD = INRTAXA_NTLDEC * positif_ou_nul(TAXA_TLDEC_1 - TAXA_NTLDEC) + INRTAXA_NTL *positif(TAXA_NTLDEC-TAXA_TLDEC_1);
INRIR_NTLDECD_1 = INRIR_NTLDEC_1 * positif_ou_nul(IRNIN_TLDEC_1 - IRNIN_NTLDEC_1) + INRIR_NTL_1 *positif(IRNIN_NTLDEC_1-IRNIN_TLDEC_1);
INRCSG_NTLDECD_1 = INRCSG_NTLDEC_1 * positif_ou_nul(CSG_TLDEC_1 - CSG_NTLDEC_1) + INRCSG_NTL_1 *positif(CSG_NTLDEC_1-CSG_TLDEC_1);
INRCRDS_NTLDECD_1 = INRCRDS_NTLDEC_1 * positif_ou_nul(RDS_TLDEC_1 - CRDS_NTLDEC_1) + INRCRDS_NTL_1 *positif(CRDS_NTLDEC_1-RDS_TLDEC_1);
INRPRS_NTLDECD_1 = INRPRS_NTLDEC_1 * positif_ou_nul(PRS_TLDEC_1 - PRS_NTLDEC_1) + INRPRS_NTL_1 *positif(PRS_NTLDEC_1-PRS_TLDEC_1);
INRCSAL_NTLDECD_1 = INRCSAL_NTLDEC_1 * positif_ou_nul(CSAL_TLDEC_1 - CSAL_NTLDEC_1) + INRCSAL_NTL_1 *positif(CSAL_NTLDEC_1-CSAL_TLDEC_1);
INRCDIS_NTLDECD_1 = INRCDIS_NTLDEC_1 * positif_ou_nul(CDIS_TLDEC_1 - CDIS_NTLDEC_1) + INRCDIS_NTL_1 *positif(CDIS_NTLDEC_1-CDIS_TLDEC_1);
INRTAXA_NTLDECD_1 = INRTAXA_NTLDEC_1 * positif_ou_nul(TAXA_TLDEC_1 - TAXA_NTLDEC_1) + INRTAXA_NTL_1 *positif(TAXA_NTLDEC_1-TAXA_TLDEC_1);
INRIR_TLDECD = INRIR_TLDEC * positif_ou_nul(IRNIN_TLDEC_1 - IRNIN_TLDEC) + INRIR_TLA *positif(IRNIN_TLDEC-IRNIN_TLDEC_1);
INRCSG_TLDECD = INRCSG_TLDEC * positif_ou_nul(CSG_TLDEC_1 - CSG_TLDEC) + INRCSG_TLA *positif(CSG_TLDEC-CSG_TLDEC_1);
INRCRDS_TLDECD = INRCRDS_TLDEC * positif_ou_nul(RDS_TLDEC_1 - RDS_TLDEC) + INRCRDS_TLA *positif(RDS_TLDEC-RDS_TLDEC_1);
INRPRS_TLDECD = INRPRS_TLDEC * positif_ou_nul(PRS_TLDEC_1 - PRS_TLDEC) + INRPRS_TLA *positif(PRS_TLDEC-PRS_TLDEC_1);
INRCSAL_TLDECD = INRCSAL_TLDEC * positif_ou_nul(CSAL_TLDEC_1 - CSAL_TLDEC) + INRCSAL_TLA *positif(CSAL_TLDEC-CSAL_TLDEC_1);
INRCDIS_TLDECD = INRCDIS_TLDEC * positif_ou_nul(CDIS_TLDEC_1 - CDIS_TLDEC) + INRCDIS_TLA *positif(CDIS_TLDEC-CDIS_TLDEC_1);
INRTAXA_TLDECD = INRTAXA_TLDEC * positif_ou_nul(TAXA_TLDEC_1 - TAXA_TLDEC) + INRTAXA_TLA *positif(TAXA_TLDEC-TAXA_TLDEC_1);

INRIR_R99RA = INRIR_R99R_A;
INRIR_R99R = arr(IRNIN_R99R * (TXINR_PA/100)-INCIR_NET_A) * positif(IRNIN_R99R- IRNIN_R99R_A)
             * positif(IND_PASSAGE-1) * positif(IRNIN_TLDEC-IRNIN_PA-ACODELAISINR);
INRIR_R9901A = INRIR_R9901_A;
INRIR_R9901 = arr(IRNIN_R9901 * (TXINR_PA/100)-INCIR_NET_A) * positif(IRNIN_R9901- IRNIN_R9901_A)
              * positif(IND_PASSAGE-1) * positif(IRNIN_TLDEC-IRNIN_R9901) * positif(IRNIN_R9901_A)
             + (arr(IRNIN_R9901 * (TXINR_PA/100))-INCIR_NET_A) * positif(IRNIN_R9901- IRNIN_INR_A)
              * positif(IND_PASSAGE-1) * positif(IRNIN_TLDEC-IRNIN_R9901) * (1-positif(IRNIN_R9901_A))
             + (INCIR_NET_A - arr(IRNIN_R9901 * (TXINR_PA/100))) * positif(IRNIN_INR_A- IRNIN_R9901)
              * positif(IND_PASSAGE-1) * positif(IRNIN_TLDEC-IRNIN_R9901) * (1-positif(IRNIN_R9901_A)) * positif(IRNIN_R9901)
	     ;
DO_INR_IRC=DO_INR_IR_A;
INR_NTL_GLOB_IR2 = INRIR_NTLDECD+ INRIR_NTL_A+ INRIR_NTLDECD_1 + INRIR_NTL_1_A ;
INR_TL_GLOB_IR2 = INRIR_TLDECD + INRIR_TL_A + INRIR_TLDEC_1 + INRIR_TL_1_A;
INR_TOT_GLOB_IR2 = (INR_NTL_GLOB_IR2 + INR_TL_GLOB_IR2*TL_IR+INRIR_R99R+INRIR_R99R_A) * (1-IND_RJLJ) ;
INR_TOT_GLOB_IRC = (INRIR_NTLDECD+ INRIR_NTL_A+ (INRIR_TLDECD + INRIR_TL_A)*TL_IR +INRIR_R99R+INRIR_R99R_A) * (1-IND_RJLJ) ;
DO_INR_IR2 = max(0,
          arr(((INRIR_TL_A+INRIR_TL_1_A)*TL_IR_A *TL_IR+ INRIR_NTL_A+INRIR_NTL_1_A) 
            * min(1,((IRNIN_REF - IRNIN_TLDEC_1)/(IRNIN_REF-max(0,IRNIN_R9901))))) * (1-positif(FLAG_RETARD + FLAG_DEFAUT))
            *positif(IRNIN_REF - IRNIN_TLDEC_1) * (1 - positif(FLAG_C02+FLAG_C22))
            *(1-positif_ou_nul(IRNIN_TLDEC_1 - IRNIN_INR_A))
        + arr(((INRIR_TL_A+INRIR_TL_1_A)*TL_IR_A *TL_IR) 
            * min(1,((IRNIN_REF - IRNIN_TLDEC_1)/(IRNIN_REF-max(0,IRNIN_R9901))))) * (1-positif(FLAG_RE