#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2011 
#au titre des revenus perçus en 2010. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle 111011:
application :  iliad;
CONST0 = 0;
CONST1 = 1;
CONST2 = 2;
CONST3 = 3;
CONST4 = 4;
CONST10 = 10;
CONST20 = 20;
CONST40 = 40;
regle 1110:
application : batch, pro  , oceans, iliad;
LIG0 = (1 - positif(IPVLOC)) * (1 - positif(RE168+TAX1649)) * IND_REV;
LIG1 = (1-positif(RE168+TAX1649)) ;
regle 1110010:
application : pro , oceans, iliad;
LIG0010 = ( INDV * INDC * INDP ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1110020:
application : pro , oceans, iliad;
LIG0020 = ( INDV * (1 - INDC) * (1 - INDP) ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1110030:
application : pro , oceans, iliad;
LIG0030 = ( INDC * (1 - INDV) * (1 - INDP) ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1110040:
application : pro , oceans, iliad;
LIG0040 = ( INDP * (1 - INDV) * (1 - INDC) ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1110050:
application : pro , oceans, iliad;
LIG0050 = ( INDV * INDC * (1 - INDP) ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1110060:
application : pro , oceans, iliad;
LIG0060 = ( INDV * INDP * (1 - INDC) ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1110070:
application : pro , oceans, iliad;
LIG0070 = ( INDC * INDP * (1 - INDV) ) * LIG0
           * (1-positif(ANNUL2042)) * TYPE4 ;
regle 11110:
application : pro  , oceans, iliad;
LIG10V = positif_ou_nul(TSBNV + PRBV + BPCOSAV + positif(F10AV*null(TSBNV+PRBV+BPCOSAV)));
LIG10C = positif_ou_nul(TSBNC + PRBC + BPCOSAC + positif(F10AC*null(TSBNC+PRBC+BPCOSAC)));
LIG10P = positif_ou_nul(somme(i=1..4:TSBNi + PRBi) + positif(F10AP*null(somme(i=1..4:TSBNi+PRBi))));
LIG10 = positif(LIG10V + LIG10C + LIG10P)*TYPE1 ;
regle 11000:
application : pro, oceans, iliad ;

LIG1100 = positif(positif(T2RV) * (1 - positif(IPVLOC)) * TYPE4) ;

LIG900 = positif((RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIG3RCMABT + LIG4RCMABT
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + VLHAB + DFANT + ESFP + RE168 +TAX1649+ R1649+PREREV)
                 * TYPE4 ) ;

regle 111440:
application : pro , oceans, iliad;
LIG4401 =  positif(V_FORVA) * (1 - positif_ou_nul(BAFV))
    	  * LIG0 ;
LIG4402 =  positif(V_FORCA) * (1 - positif_ou_nul(BAFC))
    	  * LIG0 ;
LIG4403 =  positif(V_FORPA) * (1 - positif_ou_nul(BAFP)) 
    	  * LIG0 ;
regle 11113:
application : pro  , oceans, iliad,batch;
LIG13 =  positif(present(BACDEV)+ present(BACREV)
               + present(BAHDEV) +present(BAHREV)
               + present(BACDEC) +present(BACREC)
               + present(BAHDEC)+ present(BAHREC)
               + present(BACDEP)+ present(BACREP)
               + present(BAHDEP)+ present(BAHREP)
               + present(4BAHREV) + present(4BAHREC) + present(4BAHREP)
               + present(4BACREV) + present(4BACREC) + present(4BACREP)
               + present(BAFV) + present(BAFC) + present(BAFP)
	       + present(BAFORESTV) + present(BAFORESTC) 
	       + present(BAFORESTP)
               + present(BAFPVV) + present(BAFPVC) + present(BAFPVP))
	* (1 - positif(IPVLOC)) * LIG1 *TYPE1 ;

regle 111135:
application : pro  , oceans, iliad;
4BAQLV = positif(4BACREV + 4BAHREV);
4BAQLC = positif(4BACREC + 4BAHREC);
4BAQLP = positif(4BACREP + 4BAHREP);
regle 111134:
application : pro  , oceans, iliad,batch;
LIG134V = positif(present(BAFV) + present(BAHREV) + present(BAHDEV) + present(BACREV) + present(BACDEV)+ present(BAFPVV)+present(BAFORESTV));
LIG134C = positif(present(BAFC) + present(BAHREC) + present(BAHDEC) + present(BACREC) + present(BACDEC)+ present(BAFPVC)+present(BAFORESTC));
LIG134P = positif(present(BAFP) + present(BAHREP) + present(BAHDEP) + present(BACREP) + present(BACDEP)+ present(BAFPVP)+present(BAFORESTP));
LIG134 = positif(LIG134V + LIG134C + LIG134P+present(DAGRI6)+present(DAGRI5)+present(DAGRI4)+present(DAGRI3)+present(DAGRI2)+present(DAGRI1)) 
		* (1 - positif(IPVLOC)) * (1-positif(abs(DEFIBA)))*TYPE1 ;
LIGDBAIP = positif_ou_nul(DBAIP) * positif(DAGRI1+DAGRI2+DAGRI3+DAGRI4+DAGRI5+DAGRI6) * (1-positif(IPVLOC))*TYPE1
                          * positif(abs(abs(BAHQTOT)+abs(BAQTOT)-(DAGRI6+DAGRI5+DAGRI4+DAGRI3+DAGRI2+DAGRI1)));
regle 111136:
application : pro  , oceans, iliad ,batch;
LIG136 = positif(4BAQV + 4BAQC + 4BAQP)
		* (1 - positif(IPVLOC)) * LIG1 *TYPE1 ;

regle 111590:
application : pro, oceans, iliad, batch ;
pour i = V,C,P:
LIG_BICPi =        (
  present ( BICNOi )                          
 + present (BICDNi )                          
 + present (BIHNOi )                          
 + present (BIHDNi )                          
                  ) * LIG0 ;
LIG_BICP = LIG_BICPV + LIG_BICPC + LIG_BICPP ;
LIG_DEFNPI = positif(
   present ( DEFBIC6 ) 
 + present ( DEFBIC5 ) 
 + present ( DEFBIC4 ) 
 + present ( DEFBIC3 ) 
 + present ( DEFBIC2 )
 + present ( DEFBIC1 )
            )
  * LIG0  * (1-positif(ANNUL2042)) * TYPE1 ;

LIGPLOC = positif(LOCPROCGAV + LOCPROCGAC + LOCPROCGAP + LOCDEFPROCGAV + LOCDEFPROCGAC + LOCDEFPROCGAP
		   + LOCPROV + LOCPROC + LOCPROP + LOCDEFPROV + LOCDEFPROC + LOCDEFPROP) 
		   * (1 - null(4 - V_REGCO)) ;

LIGNPLOC = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC 
		   )
		   *  (1-null(4 - V_REGCO)) ;

LIGNPLOCF = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC 
                   + LNPRODEF10 + LNPRODEF9 + LNPRODEF8 + LNPRODEF6 + LNPRODEF5
                   + LNPRODEF4 + LNPRODEF3 + LNPRODEF2 + LNPRODEF1
		   )
		   *  (1-null(4 - V_REGCO)) ;

LIGDEFNPLOC = positif(TOTDEFLOCNP) *  (1-null(4 - V_REGCO)) ;

regle 1115901:
application : pro  , oceans, iliad,batch;

LIG_BICNPF = 
       positif(
   present (BICDEC)
 + present (BICDEP)
 + present (BICDEV)
 + present (BICHDEC)
 + present (BICHDEP)
 + present (BICHDEV)
 + present (BICHREC)
 + present (BICHREP)
 + present (BICHREV)
 + present (BICREC)
 + present (BICREP)
 + present (BICREV)
 + present ( DEFBIC6 ) 
 + present ( DEFBIC5 ) 
 + present ( DEFBIC4 ) 
 + present ( DEFBIC3 ) 
 + present ( DEFBIC2 )
 + present ( DEFBIC1 )
)
                   * LIG0
                     * (1-positif(ANNUL2042)) * TYPE1 ;
regle 11117:
application : pro  , oceans, iliad,batch;
LIG_BNCNF = positif (present(BNCV) + present(BNCC) + present(BNCP))*TYPE1 ;

LIGNOCEP = ( present ( NOCEPV ) + present ( NOCEPC ) + present( NOCEPP ))     
            * LIG0  * (1-positif(ANNUL2042)) * TYPE1 ;
LIGNOCEPIMP = ( present ( NOCEPIMPV ) + present ( NOCEPIMPC ) + present( NOCEPIMPP ))     
            * LIG0  * (1-positif(ANNUL2042)) * TYPE1 ;
LIGDAB = positif(present(DABNCNP6)+present(DABNCNP5)+present(DABNCNP4)
		+present(DABNCNP3)+present(DABNCNP2)+present(DABNCNP1)) 
		* LIG0  * (1-positif(ANNUL2042)) * TYPE1 ;
LIGDIDAB = present(DIDABNCNP) * LIG0  * (1-positif(ANNUL2042)) * TYPE1 ;

LIGBNCIF = ( positif (LIGNOCEP) * (1 - positif(LIG3250) + null(BNCIF)) 
             + (null(BNCIF) * positif(LIGBNCDF)) 
	     + null(BNCIF)*(1-positif_ou_nul(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3-DABNCNP2-DABNCNP1)))
	    * (1-positif(ANNUL2042))  * (1-positif(LIGSPENPNEG+LIGSPENPPOS))* LIG0 * TYPE1;
regle 125:
application : pro  , oceans, iliad;
LIG910 = positif(
           present(RCMABD) + present(RCMTNC) +
           present(RCMAV) + present(RCMHAD) + present(RCMHAB) + 
           present(REGPRIV) + 
      (1-present(BRCMQ)) *(present(RCMFR))
                ) * LIG0  * (1-positif(ANNUL2042)) * TYPE1;
regle 111266:
application : pro  , oceans, iliad, batch;
LIGBPLIB= present(RCMLIB) * LIG0  * (1-null(4-V_REGCO))* (1-positif(ANNUL2042)) * TYPE1;
regle 1111130: 
application : pro  , oceans, iliad;
LIG1130 = positif(present(REPSOF)) * LIG0  * (1-positif(ANNUL2042)) * TYPE1;
regle 1111950:
application : pro  , oceans, iliad, batch;
LIG1950 =  ( INDREV1A8 *  positif_ou_nul(REVKIRE) 
                   * (1 - positif(positif_ou_nul(IND_TDR) * (1-(positif_ou_nul(TSELUPPEV + TSELUPPEC))))) 
                   * (1 - positif(null(2-V_REGCO)+null(4-V_REGCO)+positif(present(NRINET)+present(NRBASE))))*  TYPE1 ) ;
regle 11129:
application : pro  , oceans, iliad;
LIG29 = positif(present(RFORDI) + present(RFDHIS) + present(RFDANT) +
                present(RFDORD)) * (1 - positif(IPVLOC))
                *(1-positif(LIG30)) * LIG1  * (1-positif(ANNUL2042)) * TYPE1 * IND_REV ;
regle 11130:
application : pro, oceans, iliad, batch ;
LIG30 = present(RFMIC) * (1 - positif(IPVLOC)) * (1-positif(ANNUL2042))*TYPE1 ;
LIGREVRF = positif(present(FONCI) + present(REAMOR)) *(1 - positif(IPVLOC)) * (1-positif(ANNUL2042))*TYPE1 ;
regle 11149:
application : pro  , oceans, iliad;
LIG49 =  INDREV1A8 * positif_ou_nul(DRBG)  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 11152:
application : pro  , oceans, iliad, batch;
LIG52 = positif(present(CHENF1) + present(CHENF2) + present(CHENF3) + present(CHENF4) 
                 + present(NCHENF1) + present(NCHENF2) + present(NCHENF3) + present(NCHENF4)) 
	     * (1-positif(ANNUL2042)) * TYPE1 ;
regle 11158:
application : pro  , oceans, iliad, batch;
LIG58 = (present(PAAV) + present(PAAP)) 
	* positif(LIG52)  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 111585:
application : pro  , oceans, iliad, batch;
LIG585 = (present(PAAP) + present(PAAV)) 
	* (1-positif(LIG58))  * (1-positif(ANNUL2042)) * TYPE1 ;
LIG65 = positif(LIG52 + LIG58 + LIG585 
                + present(CHRFAC) + present(CHNFAC) + present(CHRDED)
		+ present(DPERPV) + present(DPERPC) + present(DPERPP)
                + LIGREPAR)  
       * (1-positif(ANNUL2042)) * TYPE1 ;
regle 111555:
application : pro  , oceans, iliad, batch;
LIGDPREC = present(CHRFAC) * LIG1;

LIGDFACC = (positif(20-V_NOTRAIT+0) * positif(DFACC)
           + (1-positif(20-V_NOTRAIT+0)) * present(DFACC)) * LIG1;
regle 1111390:
application : pro  , oceans, iliad;
LIG1390 = positif(positif(ABMAR) + (1-positif(RI1)) * positif(V_0DN))  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 11168:
application : pro  , oceans, iliad;
LIG68 = INDREV1A8 * (1-positif(abs(RNIDF)))  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 111681:
application : pro  , oceans, iliad, batch;
LIGRNIDF = positif(abs(RNIDF))  * (1-positif(ANNUL2042)) * (1-null(4-V_REGCO)) * TYPE1 ;
LIGRNIDF0 = positif(abs(RNIDF0)) * positif(abs(RNIDF))  * (1-null(4-V_REGCO)) * (1-positif(ANNUL2042)) * TYPE1 ;
LIGRNIDF1 = positif(abs(RNIDF1)) * positif(abs(RNIDF))  * (1-null(4-V_REGCO)) * (1-positif(ANNUL2042)) * TYPE1 ;
LIGRNIDF2 = positif(abs(RNIDF2)) * positif(abs(RNIDF))  * (1-null(4-V_REGCO)) * (1-positif(ANNUL2042)) * TYPE1 ;
LIGRNIDF3 = positif(abs(RNIDF3)) * positif(abs(RNIDF))  * (1-null(4-V_REGCO)) * (1-positif(ANNUL2042)) * TYPE1 ;
LIGRNIDF4 = positif(abs(RNIDF4)) * positif(abs(RNIDF))  * (1-null(4-V_REGCO)) * (1-positif(ANNUL2042)) * TYPE1 ;
LIGRNIDF5 = positif(abs(RNIDF5)) * positif(abs(RNIDF))  * (1-null(4-V_REGCO)) * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1111420:
application : pro  , oceans, iliad,batch;
LIGTTPVQ = positif(
              positif(CARTSV) + positif(CARTSC) + positif(CARTSP1) + positif(CARTSP2)+ positif(CARTSP3)+ positif(CARTSP4)
           +  positif(REMPLAV) + positif(REMPLAC) + positif(REMPLAP1) + positif(REMPLAP2)+ positif(REMPLAP3)+ positif(REMPLAP4)
           +  positif(PEBFV) + positif(PEBFC) + positif(PEBF1) + positif(PEBF2)+ positif(PEBF3)+ positif(PEBF4)
           +  positif(CARPEV) + positif(CARPEC) + positif(CARPEP1) + positif(CARPEP2)+ positif(CARPEP3)+ positif(CARPEP4)
           +  positif(PENSALV) + positif(PENSALC) + positif(PENSALP1) + positif(PENSALP2)+ positif(PENSALP3)+ positif(PENSALP4)
           +  positif(RENTAX) + positif(RENTAX5) + positif(RENTAX6) + positif(RENTAX7)
           +  positif(REVACT) + positif(REVPEA) + positif(PROVIE) + positif(DISQUO) + positif(RESTUC) + positif(INTERE)
           +  positif(FONCI) + positif(REAMOR)
           +  positif(4BACREV) + positif(4BACREC)+positif(4BACREP)+positif(4BAHREV)+positif(4BAHREC)+positif(4BAHREP)
           +  positif(GLD1V) + positif(GLD1C)+positif(GLD2V)+positif(GLD2V)+positif(GLD3V)+positif(GLD3V)
                  ) * LIG1  * (1-positif(ANNUL2042)) *(1-null(4-V_REGCO))* TYPE1 ;

regle 111721:
application : pro  , oceans, iliad;
LIG1430 =  positif(BPTP3) * LIG0  * (1-positif(ANNUL2042)) * TYPE2 ;

LIG1431 =  positif(BPTP18) * LIG0  * (1-positif(ANNUL2042)) * TYPE2 ;
regle 111722:
application : pro  , oceans, iliad;
LIG815 = V_EAD * positif(BPTPD) * LIG0  * (1-positif(ANNUL2042)) * TYPE2;
LIG816 = V_EAG * positif(BPTPG) * LIG0  * (1-positif(ANNUL2042)) * TYPE2;
LIGTXF225 = positif(PEA+0) * LIG0  * (1-positif(ANNUL2042)) * TYPE2;
LIGTXF30 = positif_ou_nul(BPCOPT) * LIG0  * (1-positif(ANNUL2042)) * TYPE2;
LIGTXF40 = positif(BPV40+0) * LIG0  * (1-positif(ANNUL2042)) * TYPE2;

regle 111723:
application : pro, batch, oceans, iliad ;

LIGCESDOM = positif( positif(BPVCESDOM) * positif(V_EAD + 0) * LIG0  * (1-positif(ANNUL2042)) * TYPE2 ) ;
LIGCESDOMG = positif( positif(BPVCESDOM) * positif(V_EAG + 0) * LIG0  * (1-positif(ANNUL2042)) * TYPE2 ) ;

regle 11181:
application : pro  , oceans, iliad;
LIG81 = positif(present(RDDOUP) + present(RDFDOU) + present(REPDON03) + present(REPDON04) 
		+ present(REPDON05) + present(REPDON06) + present(REPDON07)) 
               * LIG1 * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1111500:
application : pro  , oceans, iliad, batch ;

LIG1500 = positif((positif(IPMOND) * present(IPTEFP)) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	      * ((V_REGCO+0) dans (1,3,5,6)) * (1-positif(ANNUL2042)) * TYPE1;

LIG1510 = positif((positif(IPMOND) * present(IPTEFN)) + positif(INDTEFF) * (1-positif(TEFFREVTOT))) 
	      * ((V_REGCO+0) dans (1,3,5,6)) * (1-positif(ANNUL2042)) * TYPE1;

regle 1111522:
application : pro, oceans, iliad, batch ;
LIG1522 = (1-present(IND_TDR))* (1 - INDTXMIN)*(1 - INDTXMOY) * V_CR2  * (1-positif(ANNUL2042)) * TYPE4 ;
regle 1111523:
application : pro  , oceans, iliad;
LIG1523 = (1-present(IND_TDR)) * null(V_REGCO - 4) * (1-positif(ANNUL2042)) * TYPE4 ;
regle 11175:
application : pro, oceans, iliad, batch ;
LIG75 = (1 - INDTXMIN) * (1 - INDTXMOY) * (1-LIG1500)*(1-LIG1510) 
        * INDREV1A8  * (1-positif(ANNUL2042)) * TYPE3;
LIG1545 = (1-present(IND_TDR))*INDTXMIN  * positif(IND_REV) * (1-positif(ANNUL2042)) * TYPE3;
LIG1760 = (1-present(IND_TDR))*INDTXMOY  * (1-positif(ANNUL2042)) * TYPE3;
LIG1546 = positif(PRODOM + PROGUY) * (1-positif(V_EAD + V_EAG)) * (1-positif(ANNUL2042)) * TYPE4;
LIG1550 = (1-present(IND_TDR))*INDTXMOY  * (1-positif(ANNUL2042)) * TYPE3;
LIG74 = (1-present(IND_TDR))*(1 - INDTXMIN) * positif(LIG1500+LIG1510) 
         * (1-positif(ANNUL2042)) * TYPE3;
regle 11180:
application : pro, oceans, iliad ;
LIG80 = positif(present(RDREP) + present(RDFREP)) 
        * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 11182:
application : pro  , oceans, iliad;
LIG82 = positif(present(RDSYVO) + present(RDSYCJ) + present(RDSYPP) +
	    present(COSBV) + present(COSBC) + present(COSBP))
	* LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 11188:
application : pro  , oceans, iliad, batch;
LIGRSOCREPR = positif( present(RSOCREPRISE))
        * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1111740:
application : pro  , oceans, iliad;
LIG1740 = positif(RECOMP)
         * (1-positif(ANNUL2042)) * TYPE2 ;
regle 1111780:
application : pro  , oceans, iliad;
LIG1780 = positif(RDCOM + NBACT) * LIG1  * (1-positif(ANNUL2042)) * TYPE1;
regle 111981:
application : pro  , oceans, iliad;
LIG98B = positif(LIG80 + LIG82 + LIGFIPC + present(DAIDE)
                 + LIGREDAGRI + LIGFORET + LIGRESTIMO + LIGPECHE 
	         + LIGCINE + LIGTITPRISE + LIGRSOCREPR 
	         + present(PRESCOMP2000) + present(RDPRESREPORT) + present(FCPI) 
		 + present(DSOUFIP) + LIGRIRENOV + present(DFOREST) 
		 + present(DHEBE) + present(DPATNAT) + present(DSURV)
	         + LIGLOGDOM + LIGACHTOUR + LIGTOURHOT 
	         + LIGTRATOUR + LIGLOGRES + LIGREPTOUR + LIGLOCHOTR
	         + LIGREPHA + LIGCREAT + LIG1780 + LIG2040 + LIG81 + LIGLOGSOC
	         + LIGDOMSOC1 
		 + LIGCELDO + LIGCELDOP + LIGCELREDOP + LIGCELMET + LIGCELNP 
		 + LIGCELRENP + LIGCELRRED 
	         + LIGRESINEUV + LIGRESIVIEU + LIGMEUBLE + LIGREDMEUB 
		 + present(DNOUV) + LIGLOCENT + LIGCOLENT + LIGRIDOMPRO) 
           * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1111820:
application : pro  , oceans, iliad;
LIG1820 = positif(ABADO + ABAGU + RECOMP)  * (1-positif(ANNUL2042)) * TYPE2 ;
regle 111106:
application : oceans , iliad , pro , batch ;
LIG106 = TYPE2 * positif(RETIR);
LIGINRTAX = TYPE2 * positif(RETTAXA);
LIG10622 = TYPE2 * positif(RETIR22);
LIGINRTAX22 = TYPE2 * positif(RETTAXA22);
ZIG_INT22 = TYPE2 * positif(RETCS22+RETPS22+RETRD22+RETCSAL22);

ZIGINT22 = positif(RETCDIS22) * TYPE2 ;

regle 111107:
application : oceans, iliad, pro,batch;
LIG_172810    =TYPE2 * null(MAJTX1 - 10)
                     * positif(NMAJ1);
LIG_1728      =TYPE2 * (1-null(MAJTX1 - 10)) 
                     * positif(NMAJ1);
LIGTAXA17281    =TYPE2 * null(MAJTX1 - 10)
                       * positif(NMAJTAXA1);
LIGTAXA1728      =TYPE2 * (1-null(MAJTX1 - 10))
                       * positif(NMAJTAXA1);
LIG_NMAJ1 = TYPE2 * positif(NMAJ1);
LIG_NMAJ3 = TYPE2 * positif(NMAJ3);
LIG_NMAJ4 = TYPE2 * positif(NMAJ4);
LIGNMAJTAXA1 = TYPE2 * positif(NMAJTAXA1);
LIGNMAJTAXA3 = TYPE2 * positif(NMAJTAXA3);
LIGNMAJTAXA4 = TYPE2 * positif(NMAJTAXA4);
regle 111109:
application : pro  , oceans, iliad;
LIG109 = positif(IPSOUR + REGCI + LIGPVETR + LIGCULTURE + LIGMECENAT 
		  + LIGCORSE + LIG2305 
		  + LIGBPLIB + LIGCIGE + LIGDEVDUR + LIGDDUBAIL
                  + LIGVEHICULE + LIGVEACQ + LIGVEDESTR + LIGCICA + LIGCIGARD
		  + LIGPRETUD + LIGSALDOM + LIGHABPRIN
		  + LIGCREFAM + LIGCREAPP + LIGCREBIO + LIGCREPROSP + LIGINTER
		  + LIGCRETECH + LIGRESTAU + LIGRESERV + LIGCONGA + LIGMETART 
		  + LIGCREFORM + LIGLOYIMP + LIGMOBIL + LIGJEUNE
		  + LIGPPEVCP + LIGPPEV + LIGPPEC + LIGPPEP
		  + LIGPPEVC + LIGPPEVP + LIGPPECP + LIGPERT
		  + LIGRSA + LIGVERSLIB + LIGCITEC 
		   ) 
               * LIG1
               * (1-positif(ANNUL2042)) * TYPE3;
regle 1112030:
application : pro, oceans, iliad ;
LIGNRBASE = positif(present(NRINET) + present(NRBASE)) * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
LIGBASRET = positif(present(IMPRET) + present(BASRET)) * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1112332:
application : pro, oceans, iliad, batch ;
LIGAVFISC = positif(AVFISCOPTER) * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1112040:
application : pro  , oceans, iliad;
LIG2040 = positif(DNBE + RNBE + RRETU) * LIG1  * (1-positif(ANNUL2042)) * TYPE1 ;
regle 1112041:
application : pro, oceans, iliad, batch ;
LIGRDCSG = positif(positif(V_BTCSGDED)+present(DCSG)+present(RCMSOC)) * LIG1 *(1-positif(ANNUL2042)) * TYPE1  ;
regle 111973:
application : pro  , oceans, iliad, batch;
LIG2305 = positif(DIAVF2)  * (1-positif(ANNUL2042)) * TYPE1 ;
LIGCIGE = positif(RDTECH + RDGEQ + RDEQPAHA) * LIG1  * (1-positif(ANNUL2042)) * TYPE1;
regle 111117:
application : pro  , oceans, iliad;
LIG_IRNET = TYPE1*(1-positif(V_NOTRAIT-20)) + positif(V_NOTRAIT-20)*positif(present(CESSASSV) + present(CESSASSC));
regle 1112135:
application : pro  , oceans, iliad;
LIGANNUL = positif(ANNUL2042)  * TYPE1;
regle 1112050:
application : iliad , oceans;
LIG2053 = positif(V_NOTRAIT-20) * positif(IDEGR)*
          positif(IREST-SEUIL_REMBCP) *TYPE2 ;
LIG2051 = (1 - positif(20 - V_NOTRAIT))
          * positif (RECUMBIS)
          *TYPE1 ;
LIG2052 = (APPLI_ILIAD 
                 * (1 - positif(20 - V_NOTRAIT)) * positif (V_ANTIR)
          +
           APPLI_OCEANS * positif (ANTIRAFF)
          )
       	  * (1 - positif(LIG2080))   *  TYPE2 ;
LIGTAXANT =(APPLI_ILIAD
               * (1 - positif(20 - V_NOTRAIT)) * positif (V_TAXANT)
            +
             APPLI_OCEANS * positif (TAXANTAFF)
            )
       	      * (1 - positif(LIGTAXADEG))   * TYPE2 ;
regle 1112080:
application : pro  , oceans, iliad;
LIG2080 = positif(NATIMP - 71)  * (1-positif(ANNUL2042)) * TYPE1;
regle 1112081:
application : pro  , oceans, iliad;
LIGTAXADEG = positif(NATIMP - 71) * positif(TAXADEG)  * (1-positif(ANNUL2042)) * TYPE1;
regle 1112140:
application : pro , oceans, iliad,batch;
INDCTX = si (  (V_NOTRAIT+0 = 23) ou (V_NOTRAIT+0 = 25) 
            ou (V_NOTRAIT+0 = 33) ou (V_NOTRAIT+0 = 35)  
            ou (V_NOTRAIT+0 = 43) ou (V_NOTRAIT+0 = 45)  
            ou (V_NOTRAIT+0 = 53) ou (V_NOTRAIT+0 = 55)  
            ou (V_NOTRAIT+0 = 63) ou (V_NOTRAIT+0 = 65)  
            )
         alors (1)
         sinon (0)
         finsi;
LIG2140 = si (( ((V_CR2+0=0) et NATIMP=1 et ( IRNET+TAXANET + NRINET -NAPTOTA  = 0
                ou  IRNET + TAXANET + NRINET -NAPTOTA  >=SEUIL_REC_CP)) 
		ou ((V_CR2+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
          alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * (1-positif(ANNUL2042)) * TYPE3)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV))
          finsi;
regle 112141:
application : batch, pro , oceans, iliad;

LIG2141 = positif(null(IAN + RPEN - IAVT + RPPEACO + TAXASSUR - IRANT) 
                  * positif(IRANT)
                  * (1 - positif(LIG2501))
		  * null(V_IND_TRAIT - 4)
		  * (1 - positif(NRINET + 0)) * TYPE4) ;

regle 112145:
application : batch, pro , oceans, iliad;
LIGNETAREC = positif (IINET)  * positif(ANNUL2042) * TYPE2 ;
regle 1112150:
application : pro , oceans , iliad , batch ;

LIG2150 = (1 - INDCTX) 
	 * positif(IREST)
         * (1 - positif(LIG2140))
         * (1 - positif(IND_REST50))
         * (1-positif(ANNUL2042)) * TYPE2 ;

regle 1112160:
application : pro , oceans , iliad ;

LIG2161 =  INDCTX 
	  * positif(IREST) 
          * positif_ou_nul(IREST - SEUIL_REMBCP) 
	  * (1 - positif(IND_REST50)) ;


LIG2368 = INDCTX 
	 * positif(IREST)
         * positif ( positif(IND_REST50)
                     + positif(IDEGR) )
           ;

regle 1112171:
application : batch , pro , oceans , iliad ;

LIG2171 = (1 - INDCTX) 
	 * positif(IREST)
	 * (1 - positif(LIG2140))
         * positif(IND_REST50)  
	 * (1-positif(ANNUL2042)) * TYPE4 ;

regle 11121710:
application : pro , iliad ;

LIGTROP = positif(V_ANTRE) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_REC_CP))
               * null(IDRS2 - IDEC + IREP)
               * (1 - INDCTX);

LIGTROPREST =  positif(V_ANTRE) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
               - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
               - IRANT - SEUIL_REC_CP))
		 * (1 - positif(LIGTROP))
                 * (1 - INDCTX);

regle 1113210:
application : pro , oceans , iliad ;

LIGRESINF50 = positif (
              positif(IND_REST50) * positif(IREST) 
              + ( positif(IDEGR) 
                 * (1 - positif_ou_nul(IDEGR - SEUIL_REMBCP)) )
                      )  *  TYPE4 ;
regle 1112200:
application : pro , oceans , iliad ;
LIG2200 = (positif(IDEGR) 
         * positif_ou_nul(IDEGR - SEUIL_REMBCP)  * TYPE2);

regle 1112205:
application : pro , oceans, iliad;
LIG2205 = positif(IDEGR) 
         * (1 - positif_ou_nul(IDEGR - SEUIL_REMBCP))  * (1-positif(ANNUL2042)) * TYPE2 ;

regle 1112301:
application : batch, pro , oceans, iliad;
IND_NIRED = si ((CODINI=3 ou CODINI=5 ou CODINI=13)
               et (IAVIM-TAXASSUR) = 0 
                   et  V_CR2 = 0)
          alors (1 - INDCTX) 
          finsi;
regle 1112905:
application : batch, pro , oceans, iliad;
IND_IRNMR = si (CODINI=8 et NATIMP=0 et V_CR2 = 0)
          alors (1 - INDCTX)  
          finsi;
regle 1112310:
application : batch, pro , oceans, iliad;

 
IND_IRINF80 = si ( ((CODINI+0=9 et NATIMP+0=0) ou (CODINI +0= 99))
                  et V_CR2=0 
                  et  (IRNET+TAXASSUR < SEUIL_REC_CP)
                  et  (IAVIM >= SEUIL_PERCEP))
              alors ((1 - positif(INDCTX)) * (1 - positif(IREST))) 
              finsi;


regle 11123101:
application : batch, pro , oceans,iliad;

LIGNIIR = positif(
	   positif ( positif(NIAMENDE) 
                      * positif(IINET - LIM_AMENDE + 1)
                      * INDREV1A8* (1 - V_CR2)

                     + null(IRNETBIS)
                      * null(NRINET + 0)
                    )
           *  null(NATIMP +0)
           * null(TAXANET + 0)
           * (1 - positif(IREP))
           * (1 - positif(IPROP))
           * (1 - positif(IRESTIT))
           * (1 - positif(RPPEACO))
           * (1 - positif(IDEGR))
           * (1 - positif(LIGIDB))
           * (1 - positif(LIGNIRI))
           * (1 - positif(LIG80F))
           * (1 - positif(LIG400RI))
           * (1 - positif(LIG400F))
           * (1 - positif(LIGAUCUN))
           * (1 - positif(LIG2141))
           * (1 - positif(LIG2501))
           * (1 - positif(LIG8FV))
           * (1 - positif(LIGAME))
           * (1 - positif(LIGAMEND))
           * (1 - positif(LIGNIDB))
           * (1 - null(V_REGCO-2 +0))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * positif(20 - V_NOTRAIT)
           * (1-positif(ANNUL2042)) * TYPE4 );

LIGNIIRA = positif(
	    positif ( positif(NIAMENDE) 
                       * positif(IINET - LIM_AMENDE + 1)
                       * INDREV1A8 * (1 - V_CR2)

                     + null(IRNETBIS)
                      * null(NRINET + 0)
	   + positif (RPPEACO)
                   )
           *  null(NATIMP +0)
           * null(TAXANET + 0)
           * (1 - positif(IREP))
           * (1 - positif(IPROP))
           * (1 - positif(IRESTIT))
           * (1- positif_ou_nul(IRB-IRE-INE+RPPEACO-SEUIL_REC_CP))
	   * positif (RPPEACO)
           * (1 - positif(IDEGR))
           * (1 - positif(NRINET)*positif(RPPEACO))
           * (1 - positif(LIGIDB))
           * (1 - positif(LIGNIRI))
           * (1 - positif(LIG80F))
           * (1 - positif(LIG400RI))
           * (1 - positif(LIG400F))
           * (1 - positif(LIGAUCUN))
           * (1 - positif(LIG2141))
           * (1 - positif(LIG2501))
           * (1 - positif(LIG8FV))
           * (1 - positif(LIGAME))
           * (1 - positif(LIGAMEND))
           * (1 - positif(LIGNIDB))
           * (1 - null(V_REGCO-2 +0))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * positif(20 - V_NOTRAIT)
           * (1-positif(ANNUL2042)) * TYPE4 );

LIGNIIRDEG = positif( 
           null(IDRS3 - IDEC)
	   * null(IBM23)
           * (1 - V_CR2)
           * null(TAXASSUR)
           * (1 - null(V_REGCO-2))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET - SEUIL_REC_CP))
	   * positif(V_NOTRAIT - 20)
	   * (1 - positif(INDCTX))
           * (1-positif(ANNUL2042)) * TYPE4 ) ;


regle 11123102:
application : batch, pro , oceans,iliad;


LIGCBAIL = positif(
	     positif(TAXANET)
           * positif(NAPT)
           * null(INDNIRI) * (1 - positif(IBM23))
	   * ((1-positif (RPPEACO)) + null(IRNET)*positif(RPPEACO))
           * positif_ou_nul(IINET-SEUIL_REC_CP)
	   * (1 - positif(LIGNIDB))
           * positif(1 - null(2-V_REGCO)) * INDREV1A8
           * (1 - null(V_REGCO-2))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * positif(20 - V_NOTRAIT)
           * (1-positif(ANNUL2042)) * TYPE4 );
                       
LIGNITSUP = positif(
	     positif_ou_nul(TAXASSUR - SEUIL_PERCEP)
           * null(IDRS2-IDEC+IREP)
           * positif_ou_nul(TAXANET - SEUIL_REC_CP)
	   * (1 - positif (RPPEACO))
	   * (1 - positif(LIG0TSUP))
           * (1 - null(V_REGCO-2))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * positif(V_NOTRAIT - 20)
	   * (1 - positif(INDCTX))
           * (1-positif(ANNUL2042)) * TYPE4 );
                       
LIGNITDEG = positif(
	     positif(TAXANET)
           * positif(IRB2 - SEUIL_PERCEP)
           * positif_ou_nul(TAXANET - SEUIL_REC_CP)
           * null(INDNIRI) * (1 - positif(IBM23))
	   * ((1-positif (RPPEACO)) + null(IRNET)*positif(RPPEACO))
           * positif(1 - null(2-V_REGCO)) * INDREV1A8
           * (1 - null(V_REGCO-2))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * positif(INDCTX)
           * (1-positif(ANNUL2042)) * TYPE4 );
                       
regle 11123103:
application : batch, pro , oceans,iliad;




LIGNIDB = positif(
	 (null( IDOM11 - DEC11)
        * null((NAPT)*(1-positif(RPPEACO)))
        * positif (TAXASSUR)
        * positif(SEUIL_PERCEP - TAXASSUR)
        * null((IRNETBIS)*(1-positif(RPPEACO)))
	* ((1 - positif(RPPEACO))+positif(SEUIL_REC_CP-IRNET)
	      *positif(IRNET)*positif(RPPEACO))
	* null(IRB)
        * (1 - null(V_REGCO-2))
        * (1 - positif(LIG80F))
        * (1 - positif(LIG400RI))
	* (1 - positif(LIGTROP))
	* (1 - positif(LIGTROPREST))
	* (1 - positif(IMPRET))
	* positif(20 - V_NOTRAIT)
        + 0)
 * (1-positif(ANNUL2042)) * TYPE4) ;  

LIGNI61SUP = positif(
	 null( IDOM11 - DEC11)
        * positif (TAXASSUR)
        * positif(SEUIL_PERCEP - TAXASSUR)
        * null((IRNETBIS)*(1-positif(RPPEACO)))
	* ((1 - positif(RPPEACO))+positif(SEUIL_REC_CP-IRNET)
	      *positif(IRNET)*positif(RPPEACO))
	* null(IRB)
        * (1 - null(V_REGCO-2))
	* (1 - positif(LIGTROP))
	* (1 - positif(LIGTROPREST))
	* (1 - positif(IMPRET))
	* positif(V_NOTRAIT - 20)
	* (1 - positif(INDCTX))
 * (1-positif(ANNUL2042)) * TYPE4) ;  

LIGNI61DEG = positif(
	 null(IDRS2 - IDEC + IREP)
        * positif(SEUIL_PERCEP - TAXASSUR)
        * positif(TAXANET)
	* (1 - positif(RPPEACO))
        * (1 - null(V_REGCO-2))
	* (1 - positif(LIGTROP))
	* (1 - positif(LIGTROPREST))
	* (1 - positif(IMPRET))
	* positif(INDCTX)
        * (1-positif(ANNUL2042)) * TYPE4) ;  

regle 11123104:
application : batch, pro , oceans,iliad;
	


LIGNIRI = positif(
	   INDNIRI
           * null(IRNETBIS)
           * null(NATIMP)
           * (1 - positif (LIGIDB))
	   * (1 - positif (RPPEACO))
           * (1 - positif(IREP))
           * (1 - positif(IPROP))
           * (1 - null(V_REGCO-2))
           * (1 - positif(TAXANET))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * positif(20 - V_NOTRAIT)
           * (1-positif(ANNUL2042)) * TYPE4 ) ;

regle 11123105:
application : batch, pro , oceans,iliad;
	


LIGIDB = positif( INDNIRI
                 * null(NAPT*(1-positif(RPPEACO)))
                 * null(IRNETBIS*(1-positif(RPPEACO)))
                 * positif (TAXASSUR)
	         * positif(TAXANET+IRNET)
                 * positif(SEUIL_PERCEP - TAXASSUR)
                 * (1 - positif(REI))
	         * (1 - positif( BPTP2 + BPTP3 + BPTP4 + BPTP40 + BPTP18+ BPTPD + BPTPG))
                 * (1 - positif(IPROP))
	         * ((1 - positif(RPPEACO))+positif(SEUIL_REC_CP-IRNET)*positif(RPPEACO))
                 * (1 - null(V_REGCO-2))
		 * (1 - positif(LIGTROP))
		 * (1 - positif(LIGTROPREST))
	         * (1 - positif(IMPRET))
	         * positif(20 - V_NOTRAIT) )
                 * (1-positif(ANNUL2042)) * TYPE4;

regle 11123106:
application : batch, pro, oceans, iliad ;



LIGRIDB = positif(
	   INDNIRI
           * null(IRNETBIS)
           * positif(NAPT)
           * positif (TAXASSUR)
           * (1 - positif(SEUIL_PERCEP - TAXASSUR))
           * (1 - positif(IREP))
           * ((1 - positif(RPPEACO))+null(IRNET)*positif(RPPEACO)) 
           * (1 - positif(IPROP))
           * (1 - null(V_REGCO-2))
	   * (1 - positif(LIGTROP))
	 