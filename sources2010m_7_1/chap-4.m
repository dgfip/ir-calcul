#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2011 
#au titre des revenus perçus en 2010. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

 #
                                                                
  ####   #    #    ##    #####      #     #####  #####   ######      #    
 #    #  #    #   #  #   #    #     #       #    #    #  #           #    #
 #       ######  #    #  #    #     #       #    #    #  #####       #    #
 #       #    #  ######  #####      #       #    #####   #           ######
 #    #  #    #  #    #  #          #       #    #   #   #                #
  ####   #    #  #    #  #          #       #    #    #  ######           #
 #
 #
 #
 #
 #
 #
 #                      CALCUL DE L'IMPOT BRUT
 #
 #
 #
 #
 #
 #
 #
regle 401:
application : pro , oceans , bareme, iliad , batch  ;
IRB = IAMD2; 
IRB2 = IAMD2 + TAXASSUR;
regle 40101:
application : pro , oceans , iliad , batch  ;
KIR =   IAMD3 ;
regle 4011:
application : pro , oceans , bareme , iliad , batch  ;
IAMD1 = IBM13 ;
IAMD2 = IBM23 ;
IAMD2TH = positif_ou_nul(IBM23 - SEUIL_PERCEP)*IBM23;
regle 40110:
application : pro , oceans , bareme , iliad , batch  ;
IAMD3 = IBM33 - min(ACP3, IMPIM3);
regle 402112:
application : pro , oceans , iliad , batch  ;
ANG3 = IAD32 - IAD31;
regle 40220:
application : pro , oceans , iliad , batch  ;
ACP3 = max (0 ,
 somme (a=1..4: min(arr(CHENFa * TX_DPAEAV/100) , SEUIL_AVMAXETU)) - ANG3)
        * (1 - positif(V_CR2 + IPVLOC)) * positif(ANG3) * positif(IMPIM3);
regle 403:
application : pro , oceans , bareme ,iliad , batch  ;
IBM13 = IAD11 + ITP + REI + TAXASSUR + AVFISCOPTER ;
IBM23 = IAD11 + ITP + REI + AVFISCOPTER ;
regle 404:
application : pro , oceans , bareme , iliad , batch  ;
IBM33 = IAD31 + ITP + REI;
regle 4041:
application : pro , oceans , iliad , batch  ;
TX_RED0 =  inf ( (100 - ((TX_RABGUY * V_EAG) + (TX_RABDOM * V_EAD))
	     ) * TXGAIN0  / 100 * positif(V_EAD + V_EAG)
	   );
TX_RED1 =  inf ( (100 - ((TX_RABGUY * V_EAG) + (TX_RABDOM * V_EAD))
	     ) * TXGAIN1 / 100 * positif(V_EAD + V_EAG)
	   );
DOMAVTO = arr(BN1 + SPEPV + BI12F + BA1) * ((TXGAIN0 - TX_RED0)/100) * positif(V_EAD)
          + arr(BN1 + SPEPV + BI12F + BA1) * ((TXGAIN0 - TX_RED0)/100) * positif(V_EAG);
DOMABDB = max(PLAF_RABDOM - ABADO , 0) * positif(V_EAD)
          + max(PLAF_RABGUY - ABAGU , 0) * positif(V_EAG);
DOMDOM = max(DOMAVTO - DOMABDB , 0) * positif(V_EAD + V_EAG);

ITP = arr((BTP2 * TXGAIN2/100) + (BPTP4 * TXGAIN3/100) 
       + ((BN1 + SPEPV + BI12F + BA1) * positif(V_EAG+V_EAD) * TX_RED0/100) 
       + (BPVCESDOM * positif(V_EAG+V_EAD) * TX_RED1/100) 
       + (BTP40 * TXGAIN4/100)
       + DOMDOM * positif(V_EAD + V_EAG)
       + (BPV18 * TXGAIN1/100)
       + (BTP3G * TXGAIN1/100)
       + (BTP3N * TXGAIN1/100)
       + ((BN1 + SPEPV + BI12F + BA1) * (1-positif(V_EAG + V_EAD)) * TXGAIN0/100) 
       + (BPVCESDOM * (1-positif(V_EAG + V_EAD)) * TXGAIN1/100))
       * (1-positif(IPVLOC)); 

regle 40413:
application : pro , oceans , iliad , batch  ;
BTP3A = (BN1 + SPEPV + BI12F + BA1) * (1 - positif( IPVLOC )); 
BTP3N = (BPVKRI) * (1 - positif( IPVLOC )); 
BTP3G = (BPVRCM) * (1 - positif( IPVLOC )); 
BTP2 = PEA * (1 - positif( IPVLOC )); 
BTP40 = BPV40 * (1 - positif( IPVLOC )); 
BTP18 = BPV18 * (1 - positif( IPVLOC )); 

REVTP1 = max(0,BPVRCM + ABDETPLUS + ABIMPPV - DPVRCM - ABDETMOINS - ABIMPMV) + BPVCESDOM + BPV18 + BPV40 + BPCOPT + BPVKRI + PEA + PVSOCG ;

regle 4042:
application : pro , oceans , iliad , batch  ;


REI = IPREP+IPPRICORSE;

regle 40421:
application : pro , oceans , iliad , batch  ;


RPPEACOMENS = positif(ACPTMENSPPE) * ACPTMENSPPE ;

PPERSATOT = RSAFOYER + RSAPAC1 + RSAPAC2 ;

PPERSA = min(PPETOT,PPERSATOT) * (1 - V_CNR) ;

PPEFINAL = PPETOT - PPERSA ;

RPPEACO = RPPEACOMENS * (1-V_ANC_BAR)
		* (1 - V_CNR);

regle 405:
application : pro , oceans , bareme , iliad , batch  ;


IAD11 = ( max(0,IDOM11-DEC11-RED) *(1-positif(V_CR2+IPVLOC))
        + positif(V_CR2+IPVLOC) *max(0 , IDOM11 - RED) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * IDOM16;

regle 406:
application : pro , oceans , bareme , iliad , batch  ;
IAD31 = ((IDOM31-DEC31)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM31);
IAD32 = ((IDOM32-DEC32)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM32);

regle 4052:
application : pro , oceans , bareme , iliad , batch  ;

IMPIM3 =  IAD31 ;

regle 4061:
application : pro , oceans , bareme , iliad , batch  ;
pour z = 1,2:
DEC1z = min (max( arr(SEUIL_DECOTE/2 - (IDOM1z/2)),0),IDOM1z) * (1 - V_CNR);

pour z = 1,2:
DEC3z = min (max( arr(SEUIL_DECOTE/2 - (IDOM3z/2)),0),IDOM3z) * (1 - V_CNR);

DEC6 = min (max( arr(SEUIL_DECOTE/2 - (IDOM16/2)),0),IDOM16) * (1 - V_CNR);
regle 407:
application : pro ,   oceans , iliad   , batch ;

RED =  RSURV + RCOMP + RHEBE + RREPA + RDIFAGRI + RDONS
       + RCELDO + RCELDOP + RCELREPDOP9
       + RCELMET + RCELNP + RCELREPNP9 + RCELRRED09
       + RSYND + RRESTIMO + RPECHE 
       + RFIPC + RAIDE + RNOUV 
       + RTOURNEUF + RTOURTRA + RTOURREP + RTOURHOT
       + RTOUREPA + RTOURES + RTOUHOTR  
       + RLOGDOM + RMEUBLE + RREDMEUB 
       + RLOGSOC + RDOMSOC1 + RLOCENT + RCOLENT
       + RRETU + RINNO + RRPRESCOMP + RFOR 
       + RSOUFIP + RRIRENOV + RSOCREPR + RRESINEUV + RRESIVIEU
       + RIDOMPROE1 + RIDOMPROE2 + RIDOMPROE3 + RIDOMPROE4 + RIDOMPROE5
       + RPATNAT
       + RFORET + RTITPRISE + RCREAT + RCINE 
       - (V_INDTEO) * (RLOG4 + RLOG5 + RLOG6);	
regle 4070:
application : bareme ;
RED = V_9UY;
regle 4025:
application : pro ,   oceans , iliad , batch  ;

 

PLAFDOMPRO1 = max(0,IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR
			  -RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
			  -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RDONS-RLOGSOC-RDOMSOC1-RCELDO-RCELDOP-RCELREPDOP9
			  -RCELMET-RCELNP-RCELREPNP9-RCELRRED09-RRESINEUV-RRESIVIEU-RMEUBLE-RREDMEUB-RNOUV-RPATNAT
			  );
RIDOMPROE5 =  min(REPINVDOMPRO1,PLAFDOMPRO1) * (1 - V_CNR);

REPDOMENTR5 = positif (REPINVDOMPRO1- PLAFDOMPRO1)* (REPINVDOMPRO1 - PLAFDOMPRO1)* (1 -V_CNR);


PLAFDOMPRO2 = positif(PLAFDOMPRO1 - REPINVDOMPRO1)* (PLAFDOMPRO1 - REPINVDOMPRO1)* (1 -V_CNR); 
RIDOMPROE4 = min(REPINVDOMPRO2,PLAFDOMPRO2) * (1 - V_CNR);
REPDOMENTR4 = positif (REPINVDOMPRO2 - PLAFDOMPRO2)* (REPINVDOMPRO2 - PLAFDOMPRO2)* (1 -V_CNR);


PLAFDOMPRO3 = positif(PLAFDOMPRO2 - REPINVDOMPRO2)* (PLAFDOMPRO2 - REPINVDOMPRO2)* (1 -V_CNR); 
RIDOMPROE3 = min(REPINVDOMPRO3,PLAFDOMPRO3) * (1 - V_CNR);
REPDOMENTR3 = positif (REPINVDOMPRO3 - PLAFDOMPRO3)* (REPINVDOMPRO3 - PLAFDOMPRO3)* (1 -V_CNR);


PLAFDOMPRO4 = positif(PLAFDOMPRO3 - REPINVDOMPRO3)* (PLAFDOMPRO3 - REPINVDOMPRO3)* (1 -V_CNR); 
RIDOMPROE2 = min(REPINVDOMPRO4,PLAFDOMPRO4) * (1 - V_CNR);
REPDOMENTR2 = positif (REPINVDOMPRO4 - PLAFDOMPRO4)* (REPINVDOMPRO4 - PLAFDOMPRO4)* (1 -V_CNR);

PLAFDOMPRO5 = positif(PLAFDOMPRO4 - REPINVDOMPRO4)* (PLAFDOMPRO4 - REPINVDOMPRO4)* (1 -V_CNR); 

RIDOMPROTOT = RIDOMPROE1 + RIDOMPROE2 + RIDOMPROE3 + RIDOMPROE4 + RIDOMPROE5;

RINVEST = RIDOMPROE1 + RIDOMPROE2 + RIDOMPROE3  + RIDOMPROE4 + RIDOMPROE5 ;
RIDOMPRO = REPINVDOMPRO1 + REPINVDOMPRO2 + REPINVDOMPRO3 + REPINVDOMPRO4 ;

DIDOMPRO = RIDOMPRO * (1 - V_CNR) ;
regle 40742:
application : pro ,   oceans , iliad , batch  ;
BTANTGECUM = (V_BTGECUM * (1 - present(DEPMOBIL)) + DEPMOBIL);




P2GE = max( (   PLAF_GE2 * (1 + BOOL_0AM)

             + PLAF_GE2_PACQAR * (V_0CH + V_0DP)

             + PLAF_GE2_PAC * (V_0CR + V_0CF + V_0DJ + V_0DN)  

            ) - BTANTGECUM

             , 0

          ) ;

BGEDECL = RDTECH + RDEQPAHA + RDGEQ ;
BGERET = min(RDTECH + RDEQPAHA + RDGEQ , P2GE) * (1 - V_CNR);

BGTECH = min(RDTECH , P2GE) * (1 - V_CNR) ;
BGEPAHA = min(RDEQPAHA , max(P2GE - BGTECH,0)) * (1 - V_CNR) ;
BGEAUTRE = min(RDGEQ , max(P2GE - BGTECH - BGEPAHA,0)) * (1 - V_CNR) ;

TOTBGE = BGTECH + BGEPAHA + BGEAUTRE ;

RGTECH = (BGTECH * TX_RGTECH / 100 ) * (1 - V_CNR) ;
RGEPAHA =  (BGEPAHA * TX_RGEPAHA / 100 ) * (1 - V_CNR) ;
RGEAUTRE = (BGEAUTRE * TX_RGEAUTRE / 100 ) * (1 - V_CNR) ;

CIGE = arr (RGTECH + RGEPAHA + RGEAUTRE) * (1 - V_CNR) ;


GECUM = BGTECH + BGEPAHA + BGEAUTRE + BTANTGECUM ;
regle 40743:
application : pro, oceans, iliad, batch ; 
DLOYIMP = LOYIMP ;

ALOYIMP = DLOYIMP;

CILOYIMP = arr(ALOYIMP*TX_LOYIMP/100);

regle 40744:
application : pro, oceans, iliad, batch ; 
DDEVDUR = CRENRJRNOUV + CRECHOBOI + CRECHOCON2 + CRECHOBAS ;
PDEVDUR = max( (   PLAF_DEVDUR * (1 + BOOL_0AM)


                  + PLAF_GE2_PACQAR * (V_0CH+V_0DP)

	          + PLAF_GE2_PAC * (V_0CR+V_0CF+V_0DJ+V_0DN) 

		 ) - (V_BTDEVDUR*(1-present(DEPCHOBAS))+DEPCHOBAS) , 0 );
		 

ADEVDUR = max (0 , min (DDEVDUR,PDEVDUR)) * (1 - V_CNR);
R1DEVDUR = min (ADEVDUR,CRENRJRNOUV) ;
R2DEVDUR = min (max(0,ADEVDUR-R1DEVDUR),CRECHOBOI) ;
R4DEVDUR = min (max(0,ADEVDUR-R1DEVDUR-R2DEVDUR),CRECHOCON2) ;
R5DEVDUR = min (max(0,ADEVDUR-R1DEVDUR-R2DEVDUR-R4DEVDUR),CRECHOBAS) ;
CIDEVDUR = arr (R1DEVDUR * TX50/100 + R2DEVDUR * TX40/100  
		 + R4DEVDUR * TX25/100 + R5DEVDUR * TX15/100) * (1 - V_CNR);  

DEVDURCUM = ADEVDUR + (V_BTDEVDUR*(1-present(DEPCHOBAS))+DEPCHOBAS);

regle 407441:

application : pro, oceans, iliad, batch ; 

DEVDURBAIL = CINRJBAIL + CIBOIBAIL + CICHO2BAIL + CIDEP15;


ADEVDUBAIL = DEVDURBAIL * ((V_REGCO+0) dans (1,3,5,6));


R1DEDUBAIL = CINRJBAIL ;
R2DEDUBAIL = CIBOIBAIL ;
R4DEDUBAIL = CICHO2BAIL ;
R5DEDUBAIL = CIDEP15 ;

CIDEDUBAIL = arr (R1DEDUBAIL * TX50/100 + R2DEDUBAIL * TX40/100
		 + R4DEDUBAIL * TX25/100 + R5DEDUBAIL * TX15/100)
		  * ((V_REGCO+0) dans (1,3,5,6));
DEVCUMBAIL = ADEVDUBAIL;
regle 452:
application : pro, oceans, iliad, batch ; 

DTEC = RISKTEC;

ATEC = positif(DTEC) * DTEC;

CITEC = arr (ATEC * TX30/100);
regle 40745:
application : pro ,   oceans , iliad , batch  ;

DPRETUD = PRETUD + PRETUDANT ;

APRETUD = max(min(PRETUD,LIM_PRETUD) + min(PRETUDANT,LIM_PRETUD*CASEPRETUD),0) * (1-V_CNR) ;

CIPRETUD = arr(APRETUD*TX_PRETUD/100) * (1-V_CNR) ;

regle 40747:
application : pro ,   oceans , iliad , batch  ;

DPERT = PERCRE ;

CIPERT = arr(DPERT * TXPERT / 100) * (1 - V_CNR) ;

regle 40749:
application : pro ,   oceans , iliad , batch  ;

DFORET = FORET ;

AFORET = max(min(DFORET,LIM_FORET),0) * (1-V_CNR) ;

RAFORET = arr(AFORET*TX_FORET/100) * (1-V_CNR) ;

RFORET =  max( min( RAFORET , IDOM11-DEC11-RREPA-RSYND-RFIPC-RAIDE-RDIFAGRI) , 0 ) ;

regle 4075:
application : pro , oceans , iliad , batch ;

DFIPC = FIPCORSE ;

AFIPC = max(min(DFIPC,LIM_FIPCORSE*(1+BOOL_0AM)),0) * (1-V_CNR);

RFIPCORSE = arr(AFIPC*TX_FIPCORSE/100) * (1-V_CNR);

RFIPC = max( min( RFIPCORSE , IDOM11-DEC11-RREPA-RSYND) , 0);

regle 4076:
application : pro ,   oceans , iliad , batch  ;
BSURV = min( RDRESU , PLAF_RSURV + PLAF_COMPSURV * (EAC + V_0DN) + PLAF_COMPSURVQAR * (V_0CH + V_0DP) );
RRS = arr( BSURV * TX_REDSURV / 100 ) * (1 - V_CNR);
DSURV = RDRESU;
ASURV = BSURV * (1-V_CNR) ;
RSURV = max( min( RRS , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET-RTITPRISE
			      -RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE ) , 0 );

regle 4100:
application : pro , oceans , iliad , batch ;

RRCN = arr(  min( CINE1 , min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE )) * TX_CINE1/100
        + min( CINE2 , max( min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE ) - CINE1 , 0)) * TX_CINE2/100 
       ) * (1 - V_CNR) ;

DCINE = CINE1 + CINE2 ;

ACINE = max(0,min( CINE1 + CINE2 , min( arr(SOFIRNG * TX_CINE3/100) , PLAF_CINE ))) * (1 - V_CNR);

RCINE = max( min( RRCN , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET ) , 0 ) ;
regle 4176:
application : pro ,   oceans , iliad , batch  ;
BSOUFIP = min( FFIP , LIM_SOUFIP * (1 + BOOL_0AM));
RFIP = arr( BSOUFIP * TX_REDFIP / 100 ) * (1 - V_CNR);
DSOUFIP = FFIP;
ASOUFIP = BSOUFIP * (1-V_CNR) ;
RSOUFIP = max( min( RFIP , IDOM11-DEC11
           -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO) , 0 );

regle 4200:
application : pro ,   oceans , iliad , batch  ;

BRENOV = min(RIRENOV,PLAF_RENOV) ;
RENOV = arr( BRENOV * TX_RENOV / 100 ) * (1 - V_CNR) ;

DRIRENOV = RIRENOV ;
ARIRENOV = BRENOV * (1 - V_CNR) ;
RRIRENOV = max( min( RENOV , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC
			       -RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP) , 0 );

regle 40771:
application : pro ,   oceans , iliad , batch  ;
RFC = min(RDCOM,PLAF_FRCOMPTA * max(1,NBACT)) * present(RDCOM)*(1-V_CNR);
NCOMP = max(1,NBACT)* present(RDCOM) * (1-V_CNR);
DCOMP = RDCOM;
ACOMP = RFC;
regle 10040771:
application : pro ,   oceans , iliad , batch  ;
RCOMP = max( min( RFC , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET-RTITPRISE
		       -RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT
		       -RTOURTRA-RTOURES-RTOURREP-RTOUHOTR-RTOUREPA-RCREAT ) , 0 );

regle 4078:
application : pro ,   oceans , iliad , batch  ;
BCELLIERM = arr ( min ((CELLIERM + CELLIERM1 + 0 ), LIMCELLIERM ) /9 );
BCELLIERM2 = arr ( min ((CELLIERM2 + 0 ), LIMCELLIERM ) /9 );
	   

BCELLIERD = arr ( min ((CELLIERD + CELLIERD1 + 0 ), LIMCELLIERD ) /9 );
BCELLIERD2 = arr ( min ((CELLIERD2 + 0 ), LIMCELLIERD ) /9 );



DCELDO = CELLIERD + CELLIERD1 ;
ACELDO = (positif_ou_nul( CELLIERD + CELLIERD1 ) * BCELLIERD) * (1-V_CNR);


DCELDOP = CELLIERD2 ; 
ACELDOP = (positif_ou_nul( CELLIERD2) * BCELLIERD2) * (1-V_CNR); 


DCELREPDOP9 = CELREPD09; 
ACELREPDOP9 = DCELREPDOP9 * (1 - V_CNR); 


DCELMET = CELLIERM + CELLIERM1 ;
ACELMET = (positif_ou_nul( CELLIERM + CELLIERM1 ) * BCELLIERM) * (1-V_CNR) ;


DCELNP = CELLIERM2 ;    
ACELNP = (positif_ou_nul( CELLIERM2) * BCELLIERM2) * (1-V_CNR); 


DCELREPNP9 = CELREPM09 ;    
ACELREPNP9 = DCELREPNP9 * (1 - V_CNR);


DCELRRED09 = CELRRED09;
ACELRRED09 = CELRRED09 * (1 - V_CNR);


RCELDOM = (positif(CELLIERD + CELLIERD1) * arr (ACELDO * (TX40/100))) 
             * (1 - V_CNR);                           

RCEL = (positif( CELLIERM + CELLIERM1 ) * arr (ACELMET * (TX25/100)))
             * (1 - V_CNR);                           


RCELD2 = positif(CELLIERD2) * arr (ACELDOP * (TX40/100)) 
             * (1 - V_CNR);                           

RCELM2 = positif( CELLIERM2 ) * arr (ACELNP * (TX25/100))
             * (1 - V_CNR);                           

REPCELD09 = positif(CELREPD09) * arr (CELREPD09 * (TX40/100))
	     * (1 - V_CNR);

REPCELM09 = positif( CELREPM09 ) * arr (CELREPM09 * (TX25/100))
	     * (1 - V_CNR);

regle 2004078:
application : pro ,   oceans , iliad , batch  ;

RCELDO = (max( min( RCELDOM, IDOM11-DEC11
	  -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
          -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
          -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
	  -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RDONS-RLOGSOC-RDOMSOC1) , 0 ))
	   * (1 - V_CNR);

RCELDOP = (max( min( RCELD2, IDOM11-DEC11
	  -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAD-RDIFAGRI-RFORET
          -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
          -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
	  -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RON-RLOGSOC-RDOMSOC1
	  -RCELDO) , 0 ))
	   * (1 - V_CNR);

RCELREPDOP9 = (max( min( REPCELD09 , IDOM11-DEC11
	    -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAD-RDIFAGRI-RFORET
	    -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
            -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
            -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RON-RLOGSOC-RDOMSOC1
	    -RCELDO-RCELDOP) , 0))
	    * (1 - V_CNR);

RCELMET = (max( min( RCEL , IDOM11-DEC11
	  -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
          -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
          -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
	  -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RON-RLOGSOC-RDOMSOC1
	  -RCELDO-RCELDOP-RCELREPDOP9) , 0 ))
	   * (1 - V_CNR);

RCELNP = (max( min( RCELM2 , IDOM11-DEC11
	  -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAD-RDIFAGRI-RFORET
          -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
          -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
	  -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RON-RLOGSOC-RDOMSOC1
	  -RCELDO-RCELDOP-RCELREPDOP9-RCELMET) , 0 ))
	   * (1 - V_CNR);

RCELREPNP9 = (max( min( REPCELM09 , IDOM11-DEC11
	    -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
	    -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
            -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
            -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RON-RLOGSOC-RDOMSOC1
	    -RCELDO-RCELDOP-RCELREPDOP9-RCELMET-RCELNP ) , 0 ))
	    * (1 - V_CNR);

RCELRRED09 = (max( min( CELRRED09 , IDOM11-DEC11
           -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
	   -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE
	   -RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP
	   -RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RON-RLOGSOC-RDOMSOC1
	   -RCELDO-RCELDOP-RCELREPDOP9-RCELMET-RCELNP-RCELREPNP9 ) , 0 ))
	    * (1 - V_CNR);

RIVCELDO1 = ACELDO * positif(CELLIERD) ; 

RIVCELDO2 = RIVCELDO1;

RIVCELDO3 = RIVCELDO1;

RIVCELDO4 = RIVCELDO1;

RIVCELDO5 = RIVCELDO1;

RIVCELDO6 = RIVCELDO1;

RIVCELDO7 = RIVCELDO1;

RIVCELDO8 = (min ((CELLIERD + 0 ), LIMCELLIERD ) - ( 8 * RIVCELDO1))
	     * (1 - V_CNR);


RIVCELMET1 = ACELMET * positif(CELLIERM); 
RIVCELMET2 = RIVCELMET1; 
RIVCELMET3 = RIVCELMET1; 
RIVCELMET4 = RIVCELMET1; 
RIVCELMET5 = RIVCELMET1; 
RIVCELMET6 = RIVCELMET1; 
RIVCELMET7 = RIVCELMET1; 

RIVCELMET8 = (min((CELLIERM + 0) , LIMCELLIERM) - (8 * RIVCELMET1))
	      * (1 - V_CNR);

RIVCELDPA1 = ACELDO * positif(CELLIERD1) ; 
RIVCELDPA2 = RIVCELDPA1; 
RIVCELDPA3 = RIVCELDPA1; 
RIVCELDPA4 = RIVCELDPA1; 
RIVCELDPA5 = RIVCELDPA1; 
RIVCELDPA6 = RIVCELDPA1; 
RIVCELDPA7 = RIVCELDPA1; 

RIVCELDPA8 = (min ((CELLIERD1 + 0 ), LIMCELLIERD ) - (8 * RIVCELDPA1))
	      * (1 - V_CNR);


RIVCELMPA1 = ACELMET * positif(CELLIERM1) ; 
RIVCELMPA2 = RIVCELMPA1; 
RIVCELMPA3 = RIVCELMPA1; 
RIVCELMPA4 = RIVCELMPA1; 
RIVCELMPA5 = RIVCELMPA1; 
RIVCELMPA6 = RIVCELMPA1; 
RIVCELMPA7 = RIVCELMPA1; 

RIVCELMPA8 = (min ((CELLIERM1 + 0 ), LIMCELLIERM ) - (8 * RIVCELMPA1))
	  * positif(CELLIERM1) * (1 - V_CNR);


RIVCELDOP1 = ACELDOP * positif(CELLIERD2) ; 
RIVCELDOP2 = RIVCELDOP1;
RIVCELDOP3 = RIVCELDOP1;
RIVCELDOP4 = RIVCELDOP1;
RIVCELDOP5 = RIVCELDOP1;
RIVCELDOP6 = RIVCELDOP1;
RIVCELDOP7 = RIVCELDOP1;

RIVCELDOP8 = (min ((CELLIERD2 + 0 ), LIMCELLIERD) - (8 * RIVCELDOP1))
	       * (1 - V_CNR);


RIVCELNP1 = ACELNP * positif(CELLIERM2) ; 
RIVCELNP2 = RIVCELNP1;
RIVCELNP3 = RIVCELNP1;
RIVCELNP4 = RIVCELNP1;
RIVCELNP5 = RIVCELNP1;
RIVCELNP6 = RIVCELNP1;
RIVCELNP7 = RIVCELNP1;

RIVCELNP8 = (min ((CELLIERM2 + 0 ), LIMCELLIERD )
	     - ( 8 * RIVCELNP1))
	       * (1 - V_CNR);



RCELRED09 = (max(0, CELRRED09 - RCELRRED09)) 
	    * positif(CELRRED09) * (1 - V_CNR);


RCELRED = (max( 0, RCEL + RCELDOM - RCELMET - RCELDO)) * positif(CELLIERM + CELLIERD) 
				     * (1-positif(CELLIERM1 + CELLIERD1))
				     * (1 - V_CNR);


RCELREP = RCELRED09 + RCELRED;


RCELREPA = (max( 0, (RCEL - RCELMET + RCELDOM - RCELDO) * positif(CELLIERM1 + CELLIERD1) * (1-positif(CELLIERM+ CELLIERD))  
		   + RCELD2 - RCELDOP
		   + RCELM2 - RCELNP 	
		   + REPCELD09 + REPCELM09 - RCELREPNP9 - RCELREPDOP9)) 
	  * positif(CELLIERM1 + CELLIERD1 + CELLIERM2 + CELLIERD2 + CELREPM09 + CELREPD09)
	  * (1-V_CNR) ;
	 



REPCELDO = somme(i=1,2,3,4,5,6,7 : RIVCELDOi) + RIVCELDO8;  
REPCELMET = somme(i=1,2,3,4,5,6,7 : RIVCELMETi) + RIVCELMET8;  
REPCELDPA = somme(i=1,2,3,4,5,6,7 : RIVCELDPAi) + RIVCELDPA8;  
REPCELMPA = somme(i=1,2,3,4,5,6,7 : RIVCELMPAi) + RIVCELMPA8;  
REPCELDOP = somme(i=1,2,3,4,5,6,7 : RIVCELDOPi) + RIVCELDOP8;  
REPCELNP = somme(i=1,2,3,4,5,6,7 : RIVCELNPi) + RIVCELNP8;  


regle 4079:
application : pro ,   oceans , iliad , batch  ;

BILNEUF= min (INVLOCNEUF , arr( LIMLOC * (1+BOOL_0AM) /6) ) ;
BILHOT= min (INVLOCHOT , arr( LIMLOC * (1+BOOL_0AM) /6) ) * (1-positif(null(2-V_REGCO)+null(4-V_REGCO)));
BILLOCT1= min (INVLOCT1 , LIMLOC * (1+BOOL_0AM)  ) * (1-positif(null(2-V_REGCO)+null(4-V_REGCO)));
BILLOCT2= min (INVLOCT2 , max(( LIMLOC * (1+BOOL_0AM) - BILLOCT1 ),0)) * (1-positif(null(2-V_REGCO)+null(4-V_REGCO))) ;
BILRES= min (INVLOCRES , arr( LIMLOC * (1+BOOL_0AM) /6) ) ;

regle 40791 :
application : pro ,   oceans , iliad , batch  ;
RILNEUF = arr(BILNEUF * TX_REDIL25 / 100);
RILHOT  = arr(BILHOT * TX_REDIL25 / 100);
RILLOCT1 = (BILLOCT1 * TX_REDIL40 / 100);
RILLOCT2 = (BILLOCT2 * TX_REDIL20 / 100);
RILTRA = arr(RILLOCT1 + RILLOCT2);
RILRES = arr(BILRES * TX_REDIL20 / 100);
RITOUR = RILNEUF 
        + RILHOT
        + RILTRA
	+ RILRES
        + arr((REPINVLOCINV + RINVLOCINV) * TX_REDIL25 / 100)
        + arr((REPINVLOCREA + RINVLOCREA)* TX_REDIL20 / 100) ;
RIHOTR = arr((INVLOCHOTR1 + INVLOCHOTR) * TX_REDIL25 / 100) * (1-positif(null(2-V_REGCO)+null(4-V_REGCO)));


RIVL1 = (min( arr( max( 0 , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6) 
            , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)))
        + min(arr ( max ( 0 , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6) 
            , min(INVLOCRES,LIMLOC*(1+BOOL_0AM))))
     * positif(INDLOCNEUF + INDLOCRES) ;

RIVL1RES = (min ( max ( 0 , INVLOCNEUF - arr(LIMLOC*(1+BOOL_0AM)/6)),
                        arr(LIMLOC * (1+BOOL_0AM) / 6 )) 
          + min ( max ( 0, INVLOCRES - arr(LIMLOC*(1+BOOL_0AM)/6)),
       	                arr(LIMLOC * (1+BOOL_0AM) / 6 )))
                * (1 - positif(INDLOCNEUF + INDLOCRES));

RIVL2 = (max(min( arr( max( 0 , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6)
              , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - RIVL1), 0)
        + max(min(arr ( max ( 0 , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6) 
            , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - RIVL1), 0))
     * positif(INDLOCNEUF + INDLOCRES);

RIVL2RES = (min ( max ( 0, INVLOCNEUF - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES),
		       arr(LIMLOC * (1+BOOL_0AM) / 6 ))
           + min ( max ( 0, INVLOCRES - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES),
		       arr(LIMLOC * (1+BOOL_0AM) / 6 )))
                * (1 - positif(INDLOCNEUF + INDLOCRES));

RIVL3 = (max(min(arr ( max ( 0 , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6)
              , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - RIVL1 - RIVL2), 0)
        + max(min(arr ( max ( 0 , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6) 
            , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - RIVL1 - RIVL2), 0))
     * positif(INDLOCNEUF + INDLOCRES);

RIVL3RES = (min ( max ( 0, INVLOCNEUF - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES - RIVL2RES),
		       arr(LIMLOC * (1+BOOL_0AM) / 6 ))
           + min ( max ( 0, INVLOCRES - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES - RIVL2RES),
		       arr(LIMLOC * (1+BOOL_0AM) / 6 )))
                * (1 - positif(INDLOCNEUF + INDLOCRES));

RIVL4 = (max(min(arr ( max ( 0 , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6)
              , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - RIVL1 - RIVL2 - RIVL3), 0)
        + max(min(arr ( max ( 0 , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6) 
            , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - RIVL1 - RIVL2 - RIVL3), 0))
     * positif(INDLOCNEUF + INDLOCRES);

RIVL4RES = (min ( max ( 0, INVLOCNEUF - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES - RIVL2RES - RIVL3RES),
		       arr(LIMLOC * (1+BOOL_0AM) / 6 ))
           + min ( max ( 0, INVLOCRES - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES - RIVL2RES - RIVL3RES),
		       arr(LIMLOC * (1+BOOL_0AM) / 6 )))
                * (1 - positif(INDLOCNEUF + INDLOCRES));

RIVL5 = (max(min(arr ( max ( 0 , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6)
              , min(INVLOCNEUF,LIMLOC*(1+BOOL_0AM)) - RIVL1 - RIVL2 - RIVL3 - RIVL4), 0)
        + max(min(arr ( max ( 0 , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - arr(LIMLOC*(1+BOOL_0AM)/6))/6) 
            , min(INVLOCRES,LIMLOC*(1+BOOL_0AM)) - RIVL1 - RIVL2 - RIVL3 - RIVL4), 0))
     * positif(INDLOCNEUF + INDLOCRES);

RIVL5RES = (min ( max ( 0, INVLOCNEUF - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES - RIVL2RES  - RIVL3RES - RIVL4RES),
		       LIMLOC*(1+BOOL_0AM)-arr(LIMLOC * (1+BOOL_0AM) / 6 ) - RIVL1RES - RIVL2RES - RIVL3RES - RIVL4RES)
           + min ( max ( 0, INVLOCRES - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1RES - RIVL2RES  - RIVL3RES - RIVL4RES),
		       LIMLOC*(1+BOOL_0AM)-arr(LIMLOC * (1+BOOL_0AM) / 6 ) - RIVL1RES - RIVL2RES - RIVL3RES - RIVL4RES))
                * (1 - positif(INDLOCNEUF + INDLOCRES));

RIVL6 = (max(min ( max ( 0, INVLOCNEUF - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1 - RIVL2 - RIVL3 - RIVL4 - RIVL5 ),
		     LIMLOC*(1+BOOL_0AM)-arr(LIMLOC * (1+BOOL_0AM) / 6 )-RIVL1-RIVL2-RIVL3-RIVL4 - RIVL5 ), 0)
        + max( min ( max ( 0, INVLOCRES - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVL1 - RIVL2 - RIVL3 - RIVL4 - RIVL5),
		       LIMLOC*(1+BOOL_0AM)-arr(LIMLOC * (1+BOOL_0AM) / 6 ) - RIVL1 - RIVL2 - RIVL3 - RIVL4 - RIVL5), 0))
     * positif(INDLOCNEUF + INDLOCRES);

BREPNEUF = min (INVLOCNEUF , arr( LIMLOC * (1+BOOL_0AM)) ) * positif(INDLOCRES + INDLOCNEUF);
BRNEUF = max(0,BREPNEUF - BILNEUF)* positif(INDLOCRES + INDLOCNEUF);
REPNEUF = arr(BRNEUF /6) * positif(INDLOCNEUF + INDLOCRES);
BREPRES = min (INVLOCRES , arr( LIMLOC * (1+BOOL_0AM) ) ) * positif(INDLOCRES + INDLOCNEUF);
BRRES = max(0,BREPRES - BILRES)* positif(INDLOCRES + INDLOCNEUF);
REPRES = arr(BRRES /6) * positif(INDLOCNEUF + INDLOCRES);

RIVLHOT1 = min ( max ( 0 , INVLOCHOT 
				 - arr(LIMLOC*(1+BOOL_0AM)/6)),
              arr(LIMLOC * (1+BOOL_0AM) / 6 )); 
RIVLHOT2 = min ( max ( 0 , INVLOCHOT 
				 - arr(LIMLOC*(1+BOOL_0AM)/6) - RIVLHOT1),
              arr(LIMLOC * (1+BOOL_0AM) / 6 )); 
RIVLHOT3 = min ( max ( 0 , INVLOCHOT 
				 -arr( LIMLOC*(1+BOOL_0AM)/6) - RIVLHOT1 - RIVLHOT2),
              arr(LIMLOC * (1+BOOL_0AM) / 6 ));
RIVLHOT4 =  min ( max ( 0, INVLOCHOT 
				- arr(LIMLOC*(1+BOOL_0AM)/6) - RIVLHOT1 - RIVLHOT2 - RIVLHOT3 ),
		arr(LIMLOC * (1+BOOL_0AM) / 6) );
RIVLHOT5 =  min ( max ( 0, INVLOCHOT 
				- arr(LIMLOC*(1+BOOL_0AM)/6) - RIVLHOT1 - RIVLHOT2 - RIVLHOT3 - RIVLHOT4),
		LIMLOC*(1+BOOL_0AM)-arr(LIMLOC * (1+BOOL_0AM) / 6)- RIVLHOT1 - RIVLHOT2 - RIVLHOT3 - RIVLHOT4 );


DTOURNEUF = INVLOCNEUF ;
ATOURNEUF = BILNEUF  ;

DTOURHOT = INVLOCHOT;
ATOURHOT = BILHOT ;

DTOURTRA = INVLOCT1 + INVLOCT2;
ATOURTRA = (BILLOCT1 + BILLOCT2) ;

DTOURES = INVLOCRES;
ATOURES = BILRES ;

DTOURREP = REPINVLOCINV + RINVLOCINV ;
ATOURREP = REPINVLOCINV + RINVLOCINV ;

DTOUHOTR = INVLOCHOTR1 + INVLOCHOTR ;
ATOUHOTR = (INVLOCHOTR1 + INVLOCHOTR) * (1-positif(null(2-V_REGCO)+null(4-V_REGCO)));

DTOUREPA = REPINVLOCREA + RINVLOCREA ;
ATOUREPA = REPINVLOCREA + RINVLOCREA ;

regle 10040791 :
application : pro ,   oceans , iliad , batch  ;

RTOURNEUF = max( min( RILNEUF , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC
		-RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO
		-RSOUFIP-RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM ) , 0 );

RTOURHOT = max( min( RILHOT , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC
	 -RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP
	 -RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF ) , 0 );

RTOURTRA = max( min( RILTRA, IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC
	 -RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP
	 -RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT ) , 0 );

RTOURES = max( min( RILRES , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC
	 -RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP
	 -RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA ) , 0 );

RTOURREP = max( min( arr(ATOURREP * TX_REDIL25 / 100) , IDOM11-DEC11
         -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
	 -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR
	 -RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES) , 0 );

RTOUHOTR = max( min( RIHOTR , IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC
	 -RAIDE-RDIFAGRI-RFORET-RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP
	 -RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP ) , 0 )
	 * (1-positif(null(2-V_REGCO)+null(4-V_REGCO)));

RTOUREPA = max( min( arr(ATOUREPA * TX_REDIL20 / 100) , IDOM11-DEC11
         -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
	 -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR
	 -RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT-RTOURTRA-RTOURES-RTOURREP-RTOUHOTR) , 0 );

RTOUR = RTOURNEUF + RTOURREP ;

regle 407011 :
application : pro ,   oceans , iliad , batch  ;
BADCRE = min (CREAIDE, min((LIM_AIDOMI * (1 - positif(PREMAIDE)) + LIM_PREMAIDE * positif(PREMAIDE) 
		           + MAJSALDOM * ( positif_ou_nul(V_ANREV-V_0DA-65)
				         + positif_ou_nul(V_ANREV-V_0DB-65)
						     * BOOL_0AM
				         + V_0CF + V_0DJ + V_0DN 
				         + (V_0CH + V_0DP)/2
				         ) 
		           ),LIM_AIDOMI3 * (1 - positif(PREMAIDE)) + LIM_PREMAIDE2 * positif(PREMAIDE) ) * (1-positif(INAIDE + 0))
                    +  LIM_AIDOMI2 * positif(INAIDE + 0));
BADPLAF = min((LIM_AIDOMI * (1 - positif(PREMAIDE)) + LIM_PREMAIDE * positif(PREMAIDE) 
		           + MAJSALDOM * ( positif_ou_nul(V_ANREV-V_0DA-65)
				         + positif_ou_nul(V_ANREV-V_0DB-65)
						     * BOOL_0AM
				         + ASCAPA 
				         + V_0CF + V_0DJ + V_0DN 
				         + (V_0CH + V_0DP)/2
				         ) 
		          ),LIM_AIDOMI3 * (1 - positif(PREMAIDE)) + LIM_PREMAIDE2 * positif(PREMAIDE) ) * (1-positif(INAIDE + 0))
                    +  LIM_AIDOMI2 * positif(INAIDE + 0);
BADPLAFIMPUT =  max(0,BADPLAF-BADCRE);
BAD =  min (RVAIDE, BADPLAFIMPUT);

RAD = arr (BAD * TX_AIDOMI /100) * (1 - V_CNR) ;

DAIDE = RVAIDE;
AAIDE = BAD * (1-V_CNR) ;
RAIDE = max( min( RAD , IDOM11-DEC11-RREPA-RSYND-RFIPC) , 0 );
DAIDC = CREAIDE;
AAIDC = BADCRE * (1-V_CNR) ;
CIADCRE = arr (BADCRE * TX_AIDOMI /100) * (1 - V_CNR) ;

regle 4071 :
application : pro ,   oceans , iliad , batch  ;
BPATNAT = (min(PATNAT,LIM_PATNAT)) * (1 - V_CNR);
RAPATNAT = arr (BPATNAT * TX_PATNAT/100) ;
DPATNAT = PATNAT;
APATNAT = BPATNAT;
regle 20004071 :
application : pro ,   oceans , iliad , batch  ;
RPATNAT = max( min( RAPATNAT , IDOM11-DEC11
           -RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET-RTITPRISE
	   -RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE-RSURV-RLOGDOM-RTOURNEUF-RTOURHOT
	   -RTOURTRA-RTOURES-RTOURREP-RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU-RDONS-RLOGSOC-RDOMSOC1
	   -RCELDO-RCELDOP-RCELREPDOP9-RCELMET-RCELNP-RCELREPNP9-RCELRRED09-RRESINEUV-RRESIVIEU
	   -RMEUBLE-RREDMEUB-RNOUV) , 0 );

REPNATR6 = max(RAPATNAT - RPATNAT,0)*(1-V_CNR); 

PROTPAT = RPATNAT ;
REPPROTPAT = REPNATR6 ; 

regle 40704 :
application : pro ,   oceans , iliad , batch  ;

RRI1 = IDOM11-DEC11-RREPA-RSYND-RRESTIMO-RPECHE-RFIPC-RAIDE-RDIFAGRI-RFORET
       -RTITPRISE-RCINE-RSOCREPR-RRPRESCOMP-RINNO-RSOUFIP-RRIRENOV-RFOR-RHEBE-RSURV ;

regle 40705 :
application : pro ,   oceans , iliad , batch  ;

NRTOURNEUF = max( min( RILNEUF , RRI1-NRLOGDOM ) , 0 );

NRTOURHOT = max( min( RILHOT , RRI1-NRLOGDOM-NRTOURNEUF ) , 0 );

NRTOURTRA = max( min( RILTRA, RRI1-NRLOGDOM-NRTOURNEUF-NRTOURHOT ) , 0 );

NRTOURES = max( min( RILRES , RRI1-NRLOGDOM-NRTOURNEUF-NRTOURHOT-NRTOURTRA ) , 0 );

NRTOURREP = max( min( arr(ATOURREP * TX_REDIL25 / 100) , RRI1-NRLOGDOM-NRTOURNEUF-NRTOURHOT-NRTOURTRA-NRTOURES) , 0 );

NRTOUHOTR = max( min( RIHOTR , RRI1-NRLOGDOM-NRTOURNEUF-NRTOURHOT-NRTOURTR