#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2013 
#au titre des revenus percus en 2012. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

 #
 #
 #
 # #####   ######   ####    #####     #     #####  
 # #    #  #       #          #       #       #   
 # #    #  #####    ####      #       #       #  
 # #####   #            #     #       #       # 
 # #   #   #       #    #     #       #       # 
 # #    #  ######   ####      #       #       # 
 #
 #      #####   #####   #####   #
 #          #   #   #   #   #   #
 #      #####   #   #   #   #   #
 #      #       #   #   #   #   #
 #      #####   #####   #####   #
 #
 #
 #
 #
 #                     RES-SER2.m
 #                    =============
 #
 #
 #                      zones restituees par l'application
 #
 #
 #
 #
 #
regle 9071 :
application : iliad , batch ;
IDRS = INDTXMIN*IMI + 
       INDTXMOY*IMO + 
       (1-INDTXMIN) * (1-INDTXMOY) * max(0,IPHQ2 - ADO1) ;
regle 907100 :
application : iliad , batch, bareme ;
RECOMP = max(0 ,( IPHQANT2 - IPHQ2 )*(1-INDTXMIN) * (1-INDTXMOY)) 
         * (1 - positif(IPMOND+INDTEFF));
regle 907101 :
application : iliad , batch ;
IDRSANT = INDTXMIN*IMI + INDTXMOY*IMO 
         + (1-INDTXMIN) * (1-INDTXMOY) * max(0,IPHQANT2 - ADO1) ;
IDRS2 = (1 - positif(IPMOND+INDTEFF))  * 
        ( 
         IDRSANT + ( positif(ABADO)*ABADO + positif(ABAGU)*ABAGU )
                  * positif(IDRSANT)
         + IPHQANT2 * (1 - positif(IDRSANT))
         + positif(RE168+TAX1649) * IAMD2
        )
   + positif(IPMOND+INDTEFF) 
         * ( IDRS*(1-positif(IPHQ2)) + IPHQ2 * positif(IPHQ2) );

IDRS3 = IDRT ;
regle 90710 :
application : iliad , batch ;
PLAFQF = positif(IS521 - PLANT - IS511) * (1-positif(V_CR2+IPVLOC))
           * ( positif(abs(TEFF)) * positif(IDRS) + (1 - positif(abs(TEFF))) );
regle 907105 :
application : iliad , batch ;
ABADO = arr(min(ID11 * (TX_RABDOM / 100)
             * ((PRODOM * max(0,1 - V_EAD - V_EAG) / RG ) + V_EAD),PLAF_RABDOM)
	    );
ABAGU = arr(min(ID11 * (TX_RABGUY / 100)
	     * ((PROGUY * max(0,1 - V_EAD - V_EAG) / RG ) + V_EAG),PLAF_RABGUY)
	    );
regle 90711 :
application : iliad , batch ;

RGPAR =   positif(PRODOM) * 1 
       +  positif(PROGUY) * 2
       +  positif(PROGUY)*positif(PRODOM) 
       ;

regle 9074 :
application : iliad , batch ;
IBAEX = (IPQT2) * (1 - INDTXMIN) * (1 - INDTXMOY);
regle 9080 :
application : iliad , batch ;

PRELIB = PPLIB + RCMLIB ;

regle 9091 :
application : iliad , batch ;
IDEC = DEC11 * (1 - positif(V_CR2 + V_CNR + IPVLOC));
regle 9092 :
application : iliad , batch ;
IPROP = ITP ;
regle 9093 :
application : iliad , batch ;

IREP = REI ;

regle 90981 :
application : batch, iliad ;
RETIR = RETIR2 + arr(BTOINR * TXINT/100) ;
RETCS = RETCS2 + arr((CSG-CSGIM) * TXINT/100) ;
RETRD = RETRD2 + arr((RDSN-CRDSIM) * TXINT/100) ;
RETPS = RETPS2 + arr((PRS-PRSPROV) * TXINT/100) ;
RETGAIN = RETGAIN2 + arr((CGAINSAL - GAINPROV) * TXINT/100) ;
RETCSAL = RETCSAL2 + arr((CSAL - CSALPROV) * TXINT/100) ;
RETCVN = RETCVN2 + arr((CVNSALC - PROVCVNSAL) * TXINT/100) ;
RETCDIS = RETCDIS2 + arr((CDIS - CDISPROV) * TXINT/100) ;
RETGLOA = RETGLOA2 + arr(CGLOA * TXINT/100) ;
RETRSE1 = RETRSE12 + arr(RSE1 * TXINT/100) ;
RETRSE2 = RETRSE22 + arr(RSE2 * TXINT/100) ;
RETRSE3 = RETRSE32 + arr(RSE3 * TXINT/100) ;
RETRSE4 = RETRSE42 + arr(RSE4 * TXINT/100) ;
RETRSE5 = RETRSE52 + arr(RSE5 * TXINT/100) ;
RETTAXA = RETTAXA2 + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT)) * TXINT/100) ;
RETPCAP = RETPCAP2+arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR)) * TXINT/100) ;
RETLOY = RETLOY2+arr(max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT)) * TXINT/100) ;
RETHAUTREV = RETCHR2 + arr(max(0,IHAUTREVT+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * TXINT/100) ;

regle 90984 :
application : batch, iliad ;
MAJOIRTARDIF_A1 = MAJOIRTARDIF_A - MAJOIR17_2TARDIF_A;
MAJOTAXATARDIF_A1 = MAJOTAXATARDIF_A - MAJOTA17_2TARDIF_A;
MAJOCAPTARDIF_A1 = MAJOCAPTARDIF_A - MAJOCP17_2TARDIF_A;
MAJOLOYTARDIF_A1 = MAJOLOYTARDIF_A - MAJOLO17_2TARDIF_A;
MAJOHRTARDIF_A1 = MAJOHRTARDIF_A - MAJOHR17_2TARDIF_A;
MAJOTHTARDIF_A1 = MAJOTHTARDIF_A - MAJOTH17_2TARDIF_A;
MAJOIRTARDIF_D1 = MAJOIRTARDIF_D - MAJOIR17_2TARDIF_D;
MAJOTAXATARDIF_D1 = MAJOTAXATARDIF_D - MAJOTA17_2TARDIF_D;
MAJOCAPTARDIF_D1 = MAJOCAPTARDIF_D - MAJOCP17_2TARDIF_D;
MAJOLOYTARDIF_D1 = MAJOLOYTARDIF_D - MAJOLO17_2TARDIF_D;
MAJOHRTARDIF_D1 = MAJOHRTARDIF_D - MAJOHR17_2TARDIF_D;
MAJOTHTARDIF_D1 = MAJOTHTARDIF_D - MAJOTH17_2TARDIF_D;
MAJOIRTARDIF_P1 = MAJOIRTARDIF_P - MAJOIR17_2TARDIF_P;
MAJOIRTARDIF_R1 = MAJOIRTARDIF_R - MAJOIR17_2TARDIF_R;
MAJOTAXATARDIF_R1 = MAJOTAXATARDIF_R - MAJOTA17_2TARDIF_R;
MAJOCAPTARDIF_R1 = MAJOCAPTARDIF_R - MAJOCP17_2TARDIF_R;
MAJOLOYTARDIF_R1 = MAJOLOYTARDIF_R - MAJOLO17_2TARDIF_R;
MAJOHRTARDIF_R1 = MAJOHRTARDIF_R - MAJOHR17_2TARDIF_R;
MAJOTHTARDIF_R1 = MAJOTHTARDIF_R - MAJOTH17_2TARDIF_R;
NMAJ1 = max(0,MAJO1728IR + arr(BTO * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOIRTARDIF_D1
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOIRTARDIF_P1
		  + (1 - positif(PROPIR_A) ) * MAJOIRTARDIF_D1)
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOIRTARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOIRTARDIF_A1)
		);
NMAJTAXA1 = max(0,MAJO1728TAXA + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN-IRANT)) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOTAXATARDIF_D1
		+ FLAG_TRTARDIF_F * MAJOTAXATARDIF_D1
	- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOTAXATARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOTAXATARDIF_A1)
		);
NMAJPCAP1 = max(0,MAJO1728PCAP + arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN-IRANT+TAXASSUR)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOCAPTARDIF_D1
                + FLAG_TRTARDIF_F * MAJOCAPTARDIF_D1
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCAPTARDIF_R1
                + (1 - positif(FLAG_RECTIF)) * MAJOCAPTARDIF_A1)
                );
NMAJLOY1 = max(0,MAJO1728LOY + arr(max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOLOYTARDIF_D1
                + FLAG_TRTARDIF_F * MAJOLOYTARDIF_D1
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOLOYTARDIF_R1
                + (1 - positif(FLAG_RECTIF)) * MAJOLOYTARDIF_A1)
                );
NMAJCHR1 = max(0,MAJO1728CHR + arr(max(0,IHAUTREVT+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOHRTARDIF_D1
                + FLAG_TRTARDIF_F * MAJOHRTARDIF_D1
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOHRTARDIF_R1
                + (1 - positif(FLAG_RECTIF)) * MAJOHRTARDIF_A1)
                );
NMAJC1 = max(0,MAJO1728CS + arr((CSG - CSGIM) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOCSTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPCS_A) * MAJOCSTARDIF_P 
		  + (1 - positif(PROPCS_A) ) * MAJOCSTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPCS_A))
				    * ( positif(FLAG_RECTIF) * MAJOCSTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCSTARDIF_A)
		);
NMAJR1 = max(0,MAJO1728RD + arr((RDSN - CRDSIM) * COPETO/100) 
		+ FLAG_TRTARDIF * MAJORDTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPRD_A) * MAJORDTARDIF_P 
		  + (1 - positif(PROPRD_A) ) * MAJORDTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPCS_A))
				    * ( positif(FLAG_RECTIF) * MAJORDTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORDTARDIF_A)
		);
NMAJP1 = max(0,MAJO1728PS + arr((PRS - PRSPROV) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOPSTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPPS_A) * MAJOPSTARDIF_P 
		  + (1 - positif(PROPPS_A) ) * MAJOPSTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPPS_A))
				    * ( positif(FLAG_RECTIF) * MAJOPSTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOPSTARDIF_A)
		);
NMAJGAIN1 = max(0,MAJO1728GAIN + arr((CGAINSAL - GAINPROV) * COPETO/100)
		+ FLAG_TRTARDIF * MAJOGAINTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOGAINTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOGAINTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOGAINTARDIF_A)
		);
NMAJCSAL1 = max(0,MAJO1728CSAL + arr((CSAL - CSALPROV) * COPETO/100)
		+ FLAG_TRTARDIF * MAJOCSALTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCSALTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCSALTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCSALTARDIF_A)
		);
NMAJCVN1 = max(0,MAJO1728CVN + arr((CVNSALC - PROVCVNSAL) * COPETO/100)
		+ FLAG_TRTARDIF * MAJOCVNTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCVNTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCVNTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCVNTARDIF_A)
		);
NMAJCDIS1 = max(0,MAJO1728CDIS + arr((CDIS - CDISPROV) * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJOCDISTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCDISTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCDISTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCDISTARDIF_A)
		);
NMAJGLO1 = max(0,MAJO1728GLO + arr(CGLOA * COPETO/100)
                + FLAG_TRTARDIF * MAJOGLOTARDIF_D
                + FLAG_TRTARDIF_F  * MAJOGLOTARDIF_D
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOGLOTARDIF_R
                                     + (1 - positif(FLAG_RECTIF)) * MAJOGLOTARDIF_A)
);
NMAJRSE11 = max(0,MAJO1728RSE1 + arr((RSE1N - CSPROVYD) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJORSE1TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE1TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE1TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE1TARDIF_A)
		);
NMAJRSE21 = max(0,MAJO1728RSE2 + arr((RSE2N- CSPROVYF) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJORSE2TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE2TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE2TARDIF_A)
		);
NMAJRSE31 = max(0,MAJO1728RSE3 + arr((RSE3N - CSPROVYG)* COPETO/100) 
		+ FLAG_TRTARDIF * MAJORSE3TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE3TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE3TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE3TARDIF_A)
		);
NMAJRSE41 = max(0,MAJO1728RSE4 + arr((RSE4N - CSPROVYH) * COPETO/100) 
		+ FLAG_TRTARDIF * MAJORSE4TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE4TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE4TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE4TARDIF_A)
		);
NMAJRSE51 = max(0,MAJO1728RSE5 + arr((RSE5N - CSPROVYE) * COPETO/100) 
		+ FLAG_TRTARDIF * MAJORSE5TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE5TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE5TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE5TARDIF_A)
		);
NMAJ3 = max(0,MAJO1758AIR + arr(BTO * COPETO/100) * positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOIR17_2TARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOIR17_2TARDIF_P 
		  + (1 - positif(PROPIR_A) ) * MAJOIR17_2TARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOIR17_2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOIR17_2TARDIF_A)
		);
NMAJTAXA3 = max(0,MAJO1758ATAXA + arr(max(0,TAXASSUR+min(0,IRN-IRANT)) * COPETO/100)
					* positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOTA17_2TARDIF_D
		);
NMAJPCAP3 = max(0,MAJO1758APCAP + arr(max(0,IPCAPTAXT+min(0,IRN-IRANT+TAXASSUR)) * COPETO/100)
                * positif(null(CMAJ-10)+null(CMAJ-17))
                + FLAG_TRTARDIF * MAJOCP17_2TARDIF_D
		);
NMAJLOY3 = max(0,MAJO1758ALOY + arr(max(0,TAXLOY+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT)) * COPETO/100)
                * positif(null(CMAJ-10)+null(CMAJ-17))
                + FLAG_TRTARDIF * MAJOLO17_2TARDIF_D
		);
NMAJCHR3 = max(0,MAJO1758ACHR + arr(max(0,IHAUTREVT+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * COPETO/100)
                * positif(null(CMAJ-10)+null(CMAJ-17))
                + FLAG_TRTARDIF * MAJOHR17_2TARDIF_D
		);
NMAJ4    =      somme (i=03..06,30,32,55: MAJOIRi);
NMAJTAXA4  =    somme (i=03..06,30,55: MAJOTAXAi);
NMAJC4 =  somme(i=03..06,30,32,55:MAJOCSi);
NMAJR4 =  somme(i=03..06,30,32,55:MAJORDi);
NMAJP4 =  somme(i=03..06,30,55:MAJOPSi);
NMAJCSAL4 =  somme(i=03..06,30,55:MAJOCSALi);
NMAJCDIS4 =  somme(i=03..06,30,55:MAJOCDISi);
NMAJPCAP4 =  somme(i=03..06,30,55:MAJOCAPi);
NMAJCHR4 =  somme(i=03..06,30,32,55:MAJOHRi);
NMAJRSE14 =  somme(i=03..06,55:MAJORSE1i);
NMAJRSE24 =  somme(i=03..06,55:MAJORSE2i);
NMAJRSE34 =  somme(i=03..06,55:MAJORSE3i);
NMAJRSE44 =  somme(i=03..06,55:MAJORSE4i);
NMAJGAIN4 =  somme(i=03..06,55:MAJOGAINi);
regle isf 9094 :
application : batch, iliad ;
MAJOISFTARDIF_A1 = MAJOISFTARDIF_A - MAJOISF17TARDIF_A;
MAJOISFTARDIF_D1 = MAJOISFTARDIF_D - MAJOISF17TARDIF_D;
MAJOISFTARDIF_R1 = MAJOISFTARDIF_R - MAJOISF17TARDIF_R;
NMAJISF1BIS = max(0,MAJO1728ISF + arr(ISF4BASE * COPETO/100)
                   + FLAG_TRTARDIF * MAJOISFTARDIF_D
                   + FLAG_TRTARDIF_F * MAJOISFTARDIF_D
                   - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOISFTARDIF_R
					 + (1 - positif(FLAG_RECTIF)) * MAJOISFTARDIF_A)
                 );
regle 90101 :
application : iliad , batch ;

IAVIM = IRB + PTOT + TAXASSUR + PTAXA + IPCAPTAXTOT + PPCAP + TAXLOY + PTAXLOY + CHRAPRES + PHAUTREV ;

IAVIM2 = IRB + PTOT ;

regle 90113 :
application : iliad , batch ;
CDBA = positif_ou_nul(SEUIL_IMPDEFBA-SHBA-(REVTP-BA1)
      -GLN1-REV2-REV3-REV4-REVRF);
AGRBG = SHBA + (REVTP-BA1) + GLN1 + REV2 + REV3 + REV4 + REVRF ;

regle 901130 :
application : iliad , batch ;

DBAIP =  abs(min(BAHQT + BAQT , DAGRI6 + DAGRI5 + DAGRI4 + DAGRI3 + DAGRI2 + DAGRI1)
	     * positif(DAGRI6 + DAGRI5 + DAGRI4 + DAGRI3 + DAGRI2 + DAGRI1) * positif(BAHQT + BAQT)) ;

regle 901131 :
application : iliad , batch ;

RBAT = max (0 , BANOR) ;

regle 901132 :
application : iliad , batch ;
DEFIBA = (min(max(1+SEUIL_IMPDEFBA-SHBA-(REVTP-BA1)
      -GLN1-REV2-REV3-REV4-REVRF,0),1)) * min( 0 , BANOR ) ;
regle 901133 :
application :  iliad, batch ;
NAPALEG = abs(NAPT) ;

INDNAP = 1 - positif_ou_nul(NAPT) ;

GAINDBLELIQ = max(0,V_ANC_NAP*(1-2*V_IND_NAP) - NAPT) * (1-positif(V_0AN)) * (1 - V_CNR2) 
	       * (1 - null(V_REGCO - 2)) * (1 - null(V_REGCO - 4)) * (1 - positif(IPTEFP+IPTEFN+IRANT));

GAINPOURCLIQ = (1 - null(V_ANC_NAP*(1-2*V_IND_NAP))) * (V_ANC_NAP*(1-2*V_IND_NAP) - NAPT)/ V_ANC_NAP*(1-2*V_IND_NAP)  * (1 - V_CNR2);

ANCNAP = V_ANC_NAP * (1-2*V_IND_NAP) ;


INDPPEMENS = positif( ( positif(IRESTIT - 180) 
		       + positif((-1)*ANCNAP - 180) 
                       + positif(IRESTIT - IRNET - 180) * null(V_IND_TRAIT-5)
		      ) * positif(PPETOT - PPERSA - 180) )
	           * (1 - V_CNR) ;

BASPPEMENS = INDPPEMENS * min(max(IREST,(-1)*ANCNAP*positif((-1)*ANCNAP)),PPETOT-PPERSA) * null(V_IND_TRAIT-4) 
            + INDPPEMENS * max(0,min(IRESTIT-IRNET,PPETOT-PPERSA)) * null(V_IND_TRAIT-5) ;

regle 90114 :
application : iliad , batch ;
IINET = max(0 , null(4 - V_IND_TRAIT) * (( NAPTOT - IRANT + NAPCR)* positif_ou_nul (NAPTOT - IRANT + NAPCR - SEUIL_12))
	      + null (5 - V_IND_TRAIT) *  (TOTIRPS - TOTIRPSANT));
IINETIR = max(0 , NAPTIR) ;

regle 901140 :
application : bareme  ;

IINET = IRNET * positif ( IRNET - SEUIL_61 ) ;

regle 9011410 :
application : bareme , iliad , batch ;
IRNET2 =  (IAR + PIR - IRANT) ;

regle 901141 :
application : iliad , batch ;

IRNETTER = max ( 0 ,   IRNET2
                       + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER))
                        - max(0,TAXASSUR + PTAXA  - min(TAXASSUR + PTAXA + 0,max(0,INE-IRB+AVFISCOPTER))+ min(0,IRNET2)))
                       + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA))
                        - max(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)))
                       + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP))
                        - max(0,TAXLOY+PTAXLOY -min(TAXLOY+PTAXLOY,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA-IPCAPTAXT-PPCAP ))+ min(0,PCAPNEG)))
                       + (IHAUTREVT + PHAUTREV - max(0,IHAUTREVT+PHAUTREV + min(0,LOYELEVNEG)))
                )
                       ;
IRNETBIS = max(0 , IRNETTER - PIR * positif(SEUIL_12 - IRNETTER + PIR) 
				  * positif(SEUIL_12 - PIR) 
				  * positif_ou_nul(IRNETTER - SEUIL_12)) ;

regle 901143 :
application : iliad , batch ;
IRNET =  null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * IRNETBIS * positif_ou_nul(IRB - min(max(0,IRB-AVFISCOPTER),INE))
          + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
                    *
                    (
                    ((positif(IRE) + positif_ou_nul(IAVIM + NAPCRPAVIM - SEUIL_61) * (1 - positif(IRE)))
                    *
                    max(0, CHRNEG + NRINET + IMPRET + (RASAR * V_CR2) + (IRNETBIS * positif(positif_ou_nul(IAVIM + NAPCRPAVIM- SEUIL_61)) 
		                                                                  * positif_ou_nul(IRB - min(max(0,IRB-AVFISCOPTER),INE)))     
                      ) * (1 - positif(IRESTIT)))
                    + ((1 - positif_ou_nul(IAVIM + NAPCRPAVIM - SEUIL_61)) * (1 - positif(IRE)) * max(0, CHRNEG + NRINET + IMPRET + (RASAR * V_CR2)))
                    ) ;
regle 901144 :
application : iliad , batch ;
TOTNET = max (0,NAPTIR + NAPCRP);
regle 9011411 :
application : iliad , batch ;
TAXANEG = min(0 , TAXASSUR + PTAXA - min(TAXASSUR + PTAXA + 0 , max(0,INE-IRB+AVFISCOPTER)) + min(0 , IRNET2)) ;
TAXNET = positif(TAXASSUR)
	  * max(0 , TAXASSUR + PTAXA  - min(TAXASSUR + PTAXA + 0,max(0,INE-IRB+AVFISCOPTER)) + min(0 , IRNET2)) ;
TAXANET = null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * TAXNET
	   + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
             * (positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61) * TAXNET + (1 - positif_ou_nul(IAMD1 +NAPCRPIAMD1 - SEUIL_61)) * 0) ;

regle 90114111 :
application : iliad , batch ;
PCAPNEG =  min(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)) ;
PCAPTAXNET = positif(IPCAPTAXT)
                * max(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)) ;
PCAPNET = null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * PCAPTAXNET
	   + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
			* ( positif_ou_nul(IAMD1+NAPCRPIAMD1  - SEUIL_61) * PCAPTAXNET + (1 - positif_ou_nul(IAMD1 +NAPCRPIAMD1 - SEUIL_61)) * 0 ) ;
regle 90114112 :
application : iliad , batch ;
LOYELEVNEG =  min(0,TAXLOY + PTAXLOY -min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA-IPCAPTAXT-PPCAP ))+ min(0,PCAPNEG)) ;
LOYELEVNET = positif(LOYELEV)
                * max(0,TAXLOY+PTAXLOY -min(TAXLOY+PTAXLOY,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA-IPCAPTAXT-PPCAP ))+ min(0,PCAPNEG)) ;
TAXLOYNET = null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * LOYELEVNET
                + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
                * ( positif_ou_nul(IAMD1 +NAPCRPIAMD1 - SEUIL_61) * LOYELEVNET + (1 - positif_ou_nul(IAMD1 +NAPCRPIAMD1 - SEUIL_61)) * 0 ) ;
regle 901141111 :
application : iliad , batch ;
CHRNEG = min(0 , IHAUTREVT + PHAUTREV + min(0 , LOYELEVNEG)) ;
CHRNET = positif(IHAUTREVT)
                * max(0,IHAUTREVT+PHAUTREV + min(0,LOYELEVNEG))
               ;
HAUTREVNET = (null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * CHRNET
              +
              positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
              * ( positif_ou_nul(IAMD1 +NAPCRPIAMD1 - SEUIL_61) * CHRNET
              + (1 - positif_ou_nul(IAMD1 +NAPCRPIAMD1 - SEUIL_61)) * 0 )
              ) * (1-null(1-FLAG_ACO))
              ;
regle 9011412 :
application : bareme ;

IRNET = max(0 , IRNET2 + RECOMP) ;

regle 9011413 :
application : iliad , batch ;

IRPROV = min (IRANT , IAR + PIR) * positif(IRANT) ;

regle 9012401 :
application : batch , iliad ;
NAPPSAVIM = (PRS + PPRS ) ;
NAPCSAVIM = (CSG + PCSG ) ;
NAPRDAVIM = (RDSN + PRDS) ;
NAPGAINAVIM = (CGAINSAL + PGAIN) ;
NAPCSALAVIM = (CSAL + PCSAL) ;
NAPCVNAVIM = (CVNSALC + PCVN) ;
NAPCDISAVIM = (CDIS + PCDIS) ;
NAPGLOAVIM = (CGLOA + PGLOA) ;
NAPRSE1AVIM = (RSE1N + PRSE1) ;
NAPRSE2AVIM = (RSE2N + PRSE2) ;
NAPRSE3AVIM = (RSE3N + PRSE3) ;
NAPRSE4AVIM = (RSE4N + PRSE4) ;
NAPRSE5AVIM = (RSE5N + PRSE5) ;
NAPCRPAVIM = max(0 , NAPPSAVIM + NAPCSAVIM + NAPRDAVIM + NAPGAINAVIM + NAPCSALAVIM + NAPCVNAVIM + NAPCDISAVIM + NAPGLOAVIM
                    + NAPRSE1AVIM + NAPRSE2AVIM + NAPRSE3AVIM + NAPRSE4AVIM + NAPRSE5AVIM);
regle 90114010 :
application : batch , iliad ;
NAPCRPIAMD1 = PRS+CSG+RDSN +CGAINSAL + CSAL +CVNSALC + CDIS + CGLOA + RSE1N + RSE2N + RSE3N + RSE4N + RSE5N ;
regle 9011402 :
application : batch , iliad ;
NAPPS   = PRSNET   * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPCS   = CSNET    * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPRD   = RDNET    * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPGAIN = GAINNET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPCSAL = CSALNET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPCVN  = CVNNET   * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPCDIS = CDISNET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPGLOA = CGLOANET * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPRSE1 = RSE1NET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPRSE2 = RSE2NET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPRSE3 = RSE3NET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPRSE4 = RSE4NET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPRSE5 = RSE5NET  * positif_ou_nul(IAMD1 +NAPCRPIAMD1- SEUIL_61);
NAPCRP2 = max(0 , NAPPS + NAPCS + NAPRD + NAPGAIN + NAPCSAL + NAPCVN + NAPCDIS + NAPGLOA + NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4 + NAPRSE5);
regle 9011407 :
application : iliad , batch ;
IKIRN = KIR ;

IMPTHNET = max(0 , (IRB * positif_ou_nul(IRB-SEUIL_61)-INE-IRE)
		       * positif_ou_nul((IRB*positif_ou_nul(IRB-SEUIL_61)-INE-IRE)-SEUIL_12)) 
	     * (1 - V_CNR) ;

regle 90115 :
application : iliad , batch ;
IRESTIT = abs(min(0 , IRN + PIR + NRINET + IMPRET + RASAR
                    + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER)))
                    + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA)))
                    + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)))
                    + (IHAUTREVT + PHAUTREV) * (1-null(1-FLAG_ACO))                                                   
                 + NAPCRPAVIM-(CSGIM+CRDSIM+PRSPROV+CSALPROV+GAINPROV+PROVCVNSAL+CDISPROV+CSPROVYD+CSPROVYF+CSPROVYG+CSPROVYH+CSPROVYE))
                 ) * positif_ou_nul(IAVIM+NAPCRPAVIM - SEUIL_61)
         + abs(min(0 , IRN + PIR + NRINET + IMPRET + RASAR
                  + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER)))
                  + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA)))
                  + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)))
                  + (IHAUTREVT + PHAUTREV) * (1-null(1-FLAG_ACO))))
		  * (1 - positif_ou_nul(IAVIM+NAPCRPAVIM - SEUIL_61))
		 ;
regle 90115001 :
application : iliad , batch ;
IRESTITIR = abs(min(0 , IRN + PIR + NRINET + IMPRET + RASAR
                    + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER)))
                    + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA)))
                    + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)))
                    + (IHAUTREVT + PHAUTREV) * (1-null(1-FLAG_ACO))
                 )
	     ) ;
regle 901151 :
application : iliad , batch ;
IREST = null(4 - V_IND_TRAIT) * max(0 , IRESTIT - RECUMBIS)
       + null(5- V_IND_TRAIT) * max(0,V_NONRESTANT - V_ANTRE - min(0,NAPTEMP));
regle 9011511 :
application : iliad , batch ;
IRESTIR = max(0 , IRESTITIR - RECUMBISIR);
IINETCALC = max(0,NAPTEMP - TOTIRPSANT);
NONREST  =  (null(V_IND_TRAIT -4) * positif(SEUIL_8 - IRESTIT) * IRESTIT)
   + (null(5-V_IND_TRAIT) * max(NONRESTEMP,min(7,(IDEGR+IREST)*positif(IRESTIT))));

regle 901160 :
application : batch , iliad ;
TOTREC = positif_ou_nul(IRN + TAXANET + PIR + PCAPNET + TAXLOYNET + HAUTREVNET + NAPCRP - SEUIL_12) ;
regle 90116011 :
application :  batch , iliad ;

CSREC = positif(NAPCRP) * positif_ou_nul((IAVIM +NAPCRPAVIM) - SEUIL_61);

CSRECINR = positif(NAPCRINR) ;

regle 90116 :
application : batch , iliad ;

RSEREC = positif(max(0 , NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4 + NAPRSE5)
                 * positif_ou_nul(NAPTOT+NAPCRP- SEUIL_12)) ;

regle 9011603 :
application :  batch , iliad ;

CSRECA = positif_ou_nul(PRS_A + PPRS_A + CSG_A + RDS_A + PCSG_A + PRDS_A
                       + CSAL_A + CVN_A+ CDIS_A + CGLOA_A + GAINBASE_A + RSE1BASE_A + RSE2BASE_A + RSE3BASE_A + RSE4BASE_A + RSE5BASE_A + IRNIN_A
                       + TAXABASE_A + CHRBASE_A + PCAPBASE_A + LOYBASE_A
                       - SEUIL_12) ;
regle isf 90110 :
application : iliad ;
ISFDEGR = max(0,(ANTISFAFF  - ISF4BIS * positif_ou_nul (ISF4BIS - SEUIL_12)) 
	   * (1-positif_ou_nul (ISF4BIS - SEUIL_12))
          + (ANTISFAFF  - ISFNET * positif_ou_nul (ISFNET - SEUIL_12))
	   * positif_ou_nul(ISF4BIS - SEUIL_12)) ;


ISFDEG = ISFDEGR * positif_ou_nul(ISFDEGR - SEUIL_8) ;

regle corrective 9011602 :
application : iliad ;
IDEGR = max(0,V_IRPSANT + V_NONRESTANT - max(0,NAPTEMP));

IRDEG = positif(NAPTOTAIR - IRNET) * positif(NAPTOTAIR) * max(0 , V_ANTIR - max(0,IRNET))
	* positif_ou_nul(IDEGR - SEUIL_8) ;                   

TAXDEG = positif(NAPTOTAIR - TAXANET) * positif(NAPTOTAIR) * max(0 , V_TAXANT - max(0,TAXANET)) ;                    

TAXADEG = positif(TAXDEG) * positif(V_TAXANT) * max(0 , V_TAXANT - max(0,TOTAXAGA))
          * positif_ou_nul(IDEGR - SEUIL_8) ;

PCAPTAXDEG = positif(NAPTOTAIR - PCAPNET) * positif(NAPTOTAIR) * max(0 , V_PCAPANT- max(0,PCAPNET)) ;

PCAPDEG = positif(PCAPTAXDEG) * positif (V_PCAPANT) * max(0 , V_PCAPANT - max(0,PCAPTOT)) 
          * positif_ou_nul(IDEGR - SEUIL_8) ;

TAXLOYERDEG = positif(NAPTOTAIR - TAXLOYNET) * positif(NAPTOTAIR) * max(0 , V_TAXLOYANT- max(0,TAXLOYNET)) ;

TAXLOYDEG = positif(TAXLOYERDEG) * positif (V_TAXLOYANT) * max(0 , V_TAXLOYANT - max(0,TAXLOYTOT)) 
          * positif_ou_nul(IDEGR - SEUIL_8) ;

HAUTREVTAXDEG =  positif(NAPTOTAIR - HAUTREVNET) * positif(NAPTOTAIR) * max(0 , V_CHRANT - max(0,HAUTREVNET)) ;

HAUTREVDEG = positif(HAUTREVTAXDEG) * positif(V_CHRANT) * max(0 , V_CHRANT - max(0,HAUTREVTOT)) 
             * positif_ou_nul(IDEGR - SEUIL_8) ;
regle 90504:
application : batch , iliad ;
ABSRE = ABMAR + ABVIE;
regle 90509:
application : iliad , batch ;

REVTP = PTP ;

regle 90522:
application : batch , iliad ;
RPEN = PTOTD * positif(APPLI_ILIAD + APPLI_COLBERT);
regle isf 905270:
application : iliad  ;
ANTISFAFF = V_ANTISF * (1-positif(APPLI_OCEANS));    

regle 90527:
application :  iliad  ;
ANTIRAFF = V_ANTIR  * APPLI_ILIAD   
	    + 0 ;

TAXANTAFF = V_TAXANT * APPLI_ILIAD * (1- APPLI_OCEANS)
            + TAXANET_A * APPLI_OCEANS
	    + 0 ;

PCAPANTAFF = V_PCAPANT * APPLI_ILIAD * (1- APPLI_OCEANS)
            + PCAPNET_A * APPLI_OCEANS
	    + 0 ;
TAXLOYANTAFF = V_TAXLOYANT * APPLI_ILIAD * (1- APPLI_OCEANS)
            + TAXLOYNET_A * APPLI_OCEANS
	    + 0 ;

HAUTREVANTAF = V_CHRANT * APPLI_ILIAD * (1- APPLI_OCEANS)
            + CHRNET_A * APPLI_OCEANS
	    + 0 ;
regle 90514:
application : iliad , batch ;
IDRT = IDOM11;
regle 90525:
application : iliad , batch ;
IAVT = IRE - EPAV - CICA + 
          min( IRB , IPSOUR + CRCFA ) +
          min( max(0,IAN - IRE) , (BCIGA * (1 - positif(RE168+TAX1649))));
IAVT2 = IAVT + CICA;
regle 907001  :
application : iliad, batch ;
INDTXMOY = positif(TX_MIN_MET - TMOY) * positif( (present(RMOND) 
                             + present(DMOND)) ) * V_CR2 ;
INDTXMIN = positif_ou_nul( IMI - IPQ1 ) 
           * positif(1 - INDTXMOY) * V_CR2;
regle 907002  :
application : batch,  iliad ;
IND_REST = positif(IREST) ;
regle 907003  :
application :  iliad, batch ;
IND_NI =  null(NAPT) * (null (IRNET));
regle 9070030  :
application :  iliad, batch ;
IND_IMP = positif(NAPT);

INDNMR =  null(NAPT) * null(NAT1BIS) * (positif (IRNET + TAXANET + PCAPNET+ TAXLOYNET+ HAUTREVNET+NAPCRP));
IND61 =  (positif_ou_nul(IAMD1+NAPCRPIAMD1 - SEUIL_61) * 2)
	+ ((1-positif_ou_nul(IAMD1+NAPCRPIAMD1 - SEUIL_61))*positif(IAMD1+NAPCRPIAMD1) * 1)
	+ (null(IAMD1+NAPCRPIAMD1) * 3);
regle 9070031  :
application :  iliad, batch ;
INDCEX =  null(1-NATIMP) *1
+ positif(null(11-NATIMP)+null(21-NATIMP)+null(81-NATIMP)+null(91-NATIMP)) * 2
+ null(00 - NATIMP) * 3;

INDNMRI = INDNMR * positif ( RED ) ;

INDNIRI =   positif(IDOM11-DEC11) * null(IAD11);
regle 907004  :
application : batch , iliad ;

IND_REST50 = positif(SEUIL_8 - IREST) * positif(IREST) * (1-positif(APPLI_OCEANS));
IND08 = positif(NAPT*(-1)) * (positif(SEUIL_8 - abs(NAPT)) * 1 
                          + (1-positif(SEUIL_8 - abs(NAPT))) * 2 );

regle 9070041  :
application : iliad, batch;
INDMAJREV = positif(
 positif(BIHNOV)
+ positif(BIHNOC)
+ positif(BIHNOP)
+ positif(BICHREV)
+ positif(BICHREC)
+ positif(BICHREP)
+ positif(BNHREV)
+ positif(BNHREC)
+ positif(BNHREP)
+ positif(ANOCEP)
+ positif(ANOVEP)
+ positif(ANOPEP)
+ positif(BAFV)
+ positif(BAFC)
+ positif(BAFP)
+ positif(BAHREV)
+ positif(BAHREC)
+ positif(BAHREP)
+ positif(4BAHREV)
+ positif(4BAHREC)
+ positif(4BAHREP)
+ positif(REGPRIV)
);
regle 907005  :
application : iliad , batch ;
INDNMR1 = (1 - positif(IAMD1 + NAPCRPIAMD1 + 1 - SEUIL_61)) 
	   * null(NAPT) * positif(IAMD1 + NAPCRPIAMD1) ;

INDNMR2 = positif(INDNMR) * (1 - positif(INDNMR1)) ;
IND12 = (positif(SEUIL_12 - (CSTOT +IRNET+TAXANET+TAXLOYNET+PCAPNET+HAUTREVNET-IRESTITIR))*
			   positif(CSTOT +IRNET+TAXANET+TAXLOYNET+PCAPNET+HAUTREVNET-IRESTITIR)* 1 )
	+ ((1 - positif(SEUIL_12 - (CSTOT +IRNET+TAXANET+TAXLOYNET+PCAPNET+HAUTREVNET-IRESTITIR))) * 2 )
	+ (null(CSTOT +IRNET+TAXANET+TAXLOYNET+PCAPNET+HAUTREVNET-IRESTITIR) * 3);
regle 907006  :
application : batch,iliad ;


INDV = positif_ou_nul ( 
  positif( ALLOV ) 
 + positif( REMPLAV ) + positif( REMPLANBV )
 + positif( BACDEV ) + positif( BACREV )
 + positif( 4BACREV ) + positif( 4BAHREV )
 + positif( BAFPVV )
 + positif( BAFV ) + positif( BAHDEV ) + positif( BAHREV )
 + positif( BICDEV ) + positif( BICDNV )
 + positif( BICHDEV )
 + positif( BICHREV ) + positif( BICNOV )
 + positif( BICREV ) 
 + positif( BIHDNV ) + positif( BIHNOV )
 + positif( BNCAADV ) + positif( BNCAABV ) + positif( BNCDEV ) + positif( BNCNPPVV )
 + positif( BNCNPV ) + positif( BNCPROPVV ) + positif( BNCPROV )
 + positif( BNCREV ) + positif( BNHDEV ) + positif( BNHREV )
 + positif( BPCOSAV ) + positif( CARPENBAV ) + positif( CARPEV )
 + positif( CARTSNBAV ) + positif( CARTSV ) + positif( COTFV )
 + positif( DETSV ) + positif( FRNV ) + positif( GLD1V )
 + positif( GLDGRATV )
 + positif( GLD2V ) + positif( GLD3V ) + positif( ANOCEP )
 + positif( MIBNPPRESV ) + positif( MIBNPPVV ) + positif( MIBNPVENV )
 + positif( MIBPRESV ) + positif( MIBPVV ) + positif( MIBVENV )
 + positif( PALIV ) + positif( PENSALV ) + positif( PENSALNBV ) 
 + positif( PEBFV ) + positif( PRBRV )
 + positif( TSHALLOV ) + positif( DNOCEP ) + positif(BAFORESTV)
 + positif( LOCPROCGAV ) + positif( LOCPROV ) + positif( LOCNPCGAV )
 + positif( LOCNPV ) + positif( LOCDEFNPCGAV ) + positif( LOCDEFNPV )
 + positif( MIBMEUV ) + positif( MIBGITEV ) + positif( BICPMVCTV )
 + positif( BNCPMVCTV ) + positif( LOCGITV )
);
INDC = positif_ou_nul ( 
  positif( ALLOC ) 
 + positif( REM