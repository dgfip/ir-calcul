#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
verif 1700:
application : iliad  ;

si
   RDCOM > 0
   et
   SOMMEA700 = 0

alors erreur A700 ;
verif 1702:
application :  iliad ;

si
   (V_REGCO+0) dans (1,3,5,6)
   et
   INTDIFAGRI * positif(INTDIFAGRI) + 0 > RCMHAB * positif(RCMHAB) + COD2FA * positif(COD2FA) + 0

alors erreur A702 ;
verif 1703:
application :  iliad ;

si
 (
  ( (positif(PRETUD+0) = 1 ou positif(PRETUDANT+0) = 1)
   et
    V_0DA < 1979
   et
    positif(BOOL_0AM+0) = 0 )
  ou
  ( (positif(PRETUD+0) = 1 ou positif(PRETUDANT+0) = 1)
   et
   positif(BOOL_0AM+0) = 1
   et
   V_0DA < 1979
   et
   V_0DB < 1979 )
  )
alors erreur A703 ;
verif 1704:
application :  iliad ;


si
   (positif(CASEPRETUD + 0) = 1 et positif(PRETUDANT + 0) = 0)
   ou
   (positif(CASEPRETUD + 0) = 0 et positif(PRETUDANT + 0) = 1)

alors erreur A704 ;
verif 17071:
application :  iliad ;


si
   RDENS + RDENL + RDENU > V_0CF + V_0DJ + V_0DN + 0

alors erreur A70701 ;
verif 17072:
application :  iliad ;


si
   RDENSQAR + RDENLQAR + RDENUQAR > V_0CH + V_0DP + 0

alors erreur A70702 ;
verif 1708:
application : iliad ;


si
   V_IND_TRAIT > 0
   et
   (
    INVLOCXN + 0 > LIMLOC2
    ou
    INVLOCXV + 0 > LIMLOC2
    ou
    COD7UY + 0 > LIMLOC2
    ou
    COD7UZ + 0 > LIMLOC2
   )

alors erreur A708 ;
verif 1709:
application :  iliad ;


si
   SOMMEA709 > 1

alors erreur A709 ;
verif 17111:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   INAIDE > 0
   et
   positif( CREAIDE + 0) = 0

alors erreur A71101 ;
verif 17112:
application :  iliad ;
si
   V_IND_TRAIT > 0
   et
   ASCAPA >0
   et 
   positif (CREAIDE + 0) = 0

alors erreur A71102 ;
verif 17113:
application :  iliad ;

si
   V_IND_TRAIT > 0
  et
   PREMAIDE > 0
   et
   positif(CREAIDE + 0) = 0

alors erreur A71103 ;
verif 17121:
application :  iliad ;


si
   PRESCOMP2000 + 0 > PRESCOMPJUGE
   et
   positif(PRESCOMPJUGE) = 1

alors erreur A712 ;
verif non_auto_cc 1713:
application :  iliad ;


si
   (PRESCOMPJUGE + 0 > 0 et PRESCOMP2000 + 0 = 0)
   ou
   (PRESCOMPJUGE + 0 = 0 et PRESCOMP2000 + 0 > 0)

alors erreur A713 ;
verif 1714:
application :  iliad ;


si
   RDPRESREPORT + 0 > 0
   et
   PRESCOMPJUGE + PRESCOMP2000 + 0 > 0

alors erreur A714 ;
verif 1715:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   RDPRESREPORT + 0 > LIM_REPCOMPENS

alors erreur A715 ;
verif 1716:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   ((SUBSTITRENTE < PRESCOMP2000 + 0)
    ou
    (SUBSTITRENTE > 0 et present(PRESCOMP2000) = 0))

alors erreur A716 ;
verif 17171:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   positif(CELLIERFA) + positif(CELLIERFB) + positif(CELLIERFC) + positif(CELLIERFD) > 1

alors erreur A71701 ;
verif 17172:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   SOMMEA71701 > 1

alors erreur A71702 ;
verif 17173:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   SOMMEA71702 > 1

alors erreur A71703 ;
verif 17174:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   positif(CELLIERHJ) + positif(CELLIERHK) > 1

alors erreur A71704 ;
verif 17175:
application :  iliad ;


si
   V_IND_TRAIT > 0
   et
   positif(COD7ZA) + positif(COD7ZB) + positif(COD7ZC) + positif(COD7ZD) > 1

alors erreur A71705 ;
verif 17176:
application :  iliad ;


si
   V_IND_TRAIT > 0
   et
   positif(COD7ZI) + positif(COD7ZJ) + positif(COD7ZK) + positif(COD7ZL) > 1

alors erreur A71706 ;
verif 17177:
application :  iliad ;


si
   V_IND_TRAIT > 0
   et
   positif(COD7ZE) + positif(COD7ZF) + positif(COD7ZG) + positif(COD7ZH) > 1

alors erreur A71707 ;
verif 1718:
application :  iliad ;


si
   CIAQCUL > 0
   et
   SOMMEA718 = 0

alors erreur A718 ;
verif 1719:
application :  iliad ;


si
   RDMECENAT > 0
   et
   SOMMEA719 = 0

alors erreur A719 ;
verif 17302:
application :  iliad ;


si
   V_IND_TRAIT > 0
   et
   REPFOR + 0 > 0
   et
   REPSINFOR4 + 0 > 0

alors erreur A730 ;
verif 1731:
application :  iliad ;


si
   V_IND_TRAIT > 0
   et
   CASEPRETUD + 0 > 5

alors erreur A731 ;
verif 17361:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   positif(DUFLOEK) + positif(DUFLOEL) + positif(PINELQA) + positif(PINELQB) + positif(PINELQC) + positif(PINELQD) + 0 > 2

alors erreur A73601 ;
verif 17362:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   positif(PINELQE) + positif(PINELQF) + positif(PINELQG) + positif(PINELQH) + 0 > 2

alors erreur A73602 ;
verif 17363:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   positif(PINELQI) + positif(PINELQJ) + positif(PINELQK) + positif(PINELQL) + 0 > 2

alors erreur A73603 ;
verif 17364:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   positif(PINELQM) + positif(PINELQN) + positif(PINELQO) + positif(PINELQP) + 0 > 2

alors erreur A73604 ;
verif 17401:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   (CODHCM + CODHCR + CODHCW + INVOMENTRO + INVOMENTRT 
    + INVOMENTRY + CODHSO + CODHST + CODHSY + CODHTD 
    + CODHAO + CODHAT + CODHAY + CODHBG + CODHBR 
    + CODHBM + CODHBW + CODHCB + CODHCG + INVOMENTNY + CODHDM + CODHDR + CODHDW + 0) > PLAF_INVDOM6 

alors erreur A74001 ;
verif 17402:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   (CODHCM + CODHCR + CODHCW + INVOMENTRO + INVOMENTRT 
    + INVOMENTRY + CODHSO + CODHST + CODHSY + CODHTD 
    + CODHAO + CODHAT + CODHAY + CODHBG + CODHBR 
    + CODHBM + CODHBW + CODHCB + CODHCG + INVOMENTRI 
    + CODHSE + CODHSJ + CODHAE + CODHAJ + INVOMENTNY + CODHDM + CODHDR + CODHDW + 0) > PLAF_INVDOM5

alors erreur A74002 ;
verif 17403:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   (CODHCM + CODHCR + CODHCW + INVOMENTRO + INVOMENTRT 
    + INVOMENTRY + CODHSO + CODHST + CODHSY + CODHTD 
    + CODHAO + CODHAT + CODHAY + CODHBG + CODHBR 
    + CODHBM + CODHBW + CODHCB + CODHCG  
    + INVOMENTRI + CODHSE + CODHSJ + CODHAE + CODHAJ 
    + INVOMRETPR + INVOMRETPW + INVOMENTNY + CODHDM + CODHDR + CODHDW + 0) > PLAF_INVDOM2

alors erreur A74003 ;
verif 1741:
application :  iliad ;


si
   V_IND_TRAIT > 0
   et
   ((CELREPHR + 0 > PLAF_99999)
    ou
    (CELREPHS + 0 > PLAF_99999)
    ou
    (CELREPHT + 0 > PLAF_99999)
    ou
    (CELREPHU + 0 > PLAF_99999)
    ou
    (CELREPHV + 0 > PLAF_99999)
    ou
    (CELREPHW + 0 > PLAF_99999)
    ou
    (CELREPHX + 0 > PLAF_99999)
    ou
    (CELREPHZ + 0 > PLAF_99999))

alors erreur A741 ;
verif 1743:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   (REPMEUBLE + 0 > PLAF_99999
    ou
    INVREPMEU + 0 > PLAF_99999
    ou
    INVREPNPRO + 0 > PLAF_99999
    ou
    INVNPROREP + 0 > PLAF_99999)

alors erreur A743 ;
verif 1744:
application : iliad  ;

si
 present(COD7WK)+ present (COD7WQ) =1

alors erreur A744;
verif 1745:
application : iliad  ;

si
   V_IND_TRAIT > 0
   et
   positif_ou_nul(COD7ZW + COD7ZX + COD7ZY + COD7ZZ) = 1
   et
   positif_ou_nul(COD7ZW) + positif_ou_nul(COD7ZX) + positif_ou_nul(COD7ZY) + positif_ou_nul(COD7ZZ) < 4

alors erreur A745 ;
verif 17461:
application :  iliad ;

si
   V_IND_TRAIT > 0
      et
         CODHDM * positif(CODHDM + 0) > CODHDL * positif(CODHDL + 0) + 0

	 alors erreur A74601 ;
verif 17462:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   CODHDR * positif(CODHDR + 0) > CODHDQ * positif(CODHDQ + 0) + 0

alors erreur A74602 ;
verif 17463:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   CODHDW * positif(CODHDW + 0) > CODHDV * positif(CODHDV + 0) + 0

alors erreur A74603 ;
verif 1747:
application : iliad  ;

si
   FIPDOMCOM + 0 > 0
   et
    V_REGCO  = 2 

alors erreur A747 ;
verif 1752:
application : iliad  ;

si
   V_IND_TRAIT > 0
   et
   positif_ou_nul(COD7XD + COD7XE + COD7XF + COD7XG) = 1
   et
   positif_ou_nul(COD7XD) + positif_ou_nul(COD7XE) + positif_ou_nul(COD7XF) + positif_ou_nul(COD7XG) < 4

alors erreur A752 ;
verif 1761: 
application : iliad  ;

si
   APPLI_OCEANS = 0
   et
  (
  (CIGARD > 0
  et
  1 - V_CNR > 0
  et
  positif(RDGARD1) + positif(RDGARD2) + positif(RDGARD3) + positif(RDGARD4) > EM7 + 0)
  ou
 (CIGARD > 0
  et
  1 - V_CNR > 0
  et
  positif(RDGARD1QAR) + positif(RDGARD2QAR) + positif(RDGARD3QAR) + positif(RDGARD4QAR) > EM7QAR + 0)
  )

alors erreur A761 ;
