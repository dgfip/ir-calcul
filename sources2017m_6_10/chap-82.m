#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle 821000:
application : iliad ;

RCMRABDNOR =(1-V_MODUL)* (arr(RCMABD * 40/100));


2RCMRABDNOR =(1-V_MODUL)*( arr(REVACT * 40/100));
TRCMRABDNOR = RCMRABDNOR+2RCMRABDNOR;
RCMRTNCNOR =(1-V_MODUL)*( arr(RCMTNC * 40/100));

2RCMRTNCNOR =(1-V_MODUL)*( arr(REVPEA * 40/100));

TRCMRTNCNOR =(1-V_MODUL)* (RCMRTNCNOR + 2RCMRTNCNOR);
RCMRNABDNOR =(1-V_MODUL)*( max(0,RCMABD - RCMRABDNOR));

2RCMRNABDNOR =(1-V_MODUL)*( max(0,REVACT - 2RCMRABDNOR));
RCMRNTNCNOR =(1-V_MODUL)*( max(0,RCMTNC - RCMRTNCNOR) );

2RCMRNTNCNOR =(1-V_MODUL)*( max(0,REVPEA - 2RCMRTNCNOR)) ;
REGPRIVMNOR =(1-V_MODUL)*( arr(REGPRIV * MAJREV));

2REGPRIVMNOR =(1-V_MODUL)*( arr(RESTUC * MAJREV));
REPRCM2TUNOR =(1-V_MODUL)*( COD2TU + COD2TV);
TRCMABDNOR =(1-V_MODUL)*( RCMABD + REVACT);
RCMABNOR =(1-V_MODUL)*( RCMRNABDNOR + 2RCMRNABDNOR) ;
DRTNCNOR =(1-V_MODUL)*( RCMTNC + REVPEA);
RTNCNOR =(1-V_MODUL)*( RCMRNTNCNOR + 2RCMRNTNCNOR) ;
RAVCNOR =(1-V_MODUL)*( RCMAV + PROVIE);
ABRCM2 =(1-V_MODUL)*( min( ABTAV , RAVCNOR));
ABACH  =(1-V_MODUL)*( positif(RCMAV) * arr( ABRCM2 * RCMAV / RAVCNOR ));
RCMRNCH =(1-V_MODUL)*( max(0,RCMAV - ABACH));
2ABACH =(1-V_MODUL)*( positif(PROVIE) * min(arr( ABRCM2 * PROVIE / RAVCNOR ) , max(0,ABRCM2 - ABACH)));
2RCMRNCH =(1-V_MODUL)*( max(0,PROVIE - 2ABACH));
TRCMRNCH =RCMRNCH + 2RCMRNCH;
RCMNAB =RCMHAD + DISQUO ;
RTCAR =RCMHAB + INTERE;
RCMPRIV = REGPRIV + RESTUC;
RCMPRIVM = REGPRIVMNOR + 2REGPRIVMNOR ;

regle 821010:
application : iliad ;

RCMORDTOTNOR =(1-V_MODUL)*( RCMABD + RCMTNC + RCMAV + RCMHAD + RCMHAB + REGPRIV+COD2TT);
RCMQUOTOTNOR =(1-V_MODUL)*( REVACT + REVPEA + PROVIE + DISQUO + INTERE + RESTUC);
RCMTOTNOR =(1-V_MODUL)*( RCMORDTOTNOR + RCMQUOTOTNOR);

RCMORDTONORM =(1-V_MODUL)*( RCMRNABDNOR + RCMRNTNCNOR + RCMRNCH + RCMHAD + RCMHAB + REGPRIVMNOR + COD2TT);
RCMQUTONOR =(1-V_MODUL)*( 2RCMRNABDNOR + 2RCMRNTNCNOR + 2RCMRNCH + DISQUO + INTERE + 2REGPRIVMNOR);
INDRCMDEF1 =(1-V_MODUL)*( positif(RCMFR - RCMORDTONORM-RCMQUTONOR));
regle 888821010:
application : iliad ;

RCMORDTOTMODUL = ((V_MODUL)*(positif(COD2OP)*( RCMABD + RCMTNC + RCMAV + RCMHAD + RCMHAB + REGPRIV+COD2TT+COD2VV+COD2WW+COD2YY+COD2ZZ)));
RCMQUOTOTMOD =((V_MODUL)*(positif(COD2OP)* (REVACT + REVPEA + PROVIE + DISQUO + INTERE + RESTUC)));
RCMTOTMOD = ((V_MODUL)*(positif(COD2OP)*(RCMORDTOTMODUL + RCMQUOTOTMOD)));


RAVCMODUL =((V_MODUL)*(positif(COD2OP)))* (RCMAV + PROVIE+ RCMLIB + COD2VV + COD2WW);


RCMRABDMODUL =(V_MODUL*(positif(COD2OP)))*( arr(RCMABD * 40/100));

2RCMRABDMOD =(V_MODUL*(positif(COD2OP)))*(arr(REVACT * 40/100));

TRCMRABDMODUL =(V_MODUL*(positif(COD2OP)))*( RCMRABDMODUL + 2RCMRABDMOD);

 # FU
 RCMRTNCMODUL =(V_MODUL*(positif(COD2OP)))*( arr(RCMTNC * 40/100));

 2RCMRTNCMODUL =(V_MODUL*(positif(COD2OP)))*( arr(REVPEA * 40/100));

 TRCMRTNCMODUL =(V_MODUL*(positif(COD2OP)))*(RCMRTNCMODUL + 2RCMRTNCMODUL);

 # DC
 RCMRNABDMODUL =(V_MODUL*(positif(COD2OP)))*(max(0,RCMABD - RCMRABDMODUL));

 2RCMRNABDMODUL =(V_MODUL*(positif(COD2OP)))*( max(0,REVACT - 2RCMRABDMOD));
 #
 # FU
 RCMRNTNCMODUL =(V_MODUL*(positif(COD2OP)))* (max(0,RCMTNC - RCMRTNCMODUL ));

 2RCMRNTNCMODUL =(V_MODUL*(positif(COD2OP)))* (max(0,REVPEA - 2RCMRTNCMODUL));

 # Majorations 1.25 de GO modulation
 REGPRIVMMODUL = (V_MODUL*(positif(COD2OP)))*(arr(REGPRIV * MAJREV));

 2REGPRIVMMODUL = (V_MODUL*(positif(COD2OP)))*(arr(RESTUC * MAJREV));

REGPRIVMMODUL = (V_MODUL*(positif(COD2OP)))*(arr(REGPRIV * MAJREV));

2REGPRIVMMODUL = (V_MODUL*(positif(COD2OP)))*(arr(RESTUC * MAJREV));



RAVCMODUL1 = (V_MODUL*(positif(COD2OP)))*(positif(RAVCMODUL-ABTAV));

RND2CHRCH = (V_MODUL*(positif(COD2OP)))*(positif (RAVCMODUL1))*(RCMAV +PROVIE);

ABT2CHRCH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1))*( min(RND2CHRCH,ABTAV));

ABT2CH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1))*(ABT2CHRCH*(RCMAV/RND2CHRCH));

ABTRCH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1))*(ABT2CHRCH-ABT2CH);

RNET2CH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1))*(RCMAV-ABT2CH);

RNETRCH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1)) *(PROVIE-ABTRCH);

RNET2DH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1)) *(max(0,(RCMLIB-(ABTAV-ABT2CHRCH))));

ABT2DH = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1)) *(RCMLIB-RNET2DH);

RNET2VV2WW = (V_MODUL *(positif(COD2OP)))*(positif(RAVCMODUL1)) *
(positif((COD2VV)+(COD2WW)))*
 (max(0,((COD2VV+COD2WW)-(ABTAV-ABT2CHRCH-ABT2DH))));


REPRCM2TUMODUL =(V_MODUL*(positif(COD2OP)))*(COD2TU + COD2TV + COD2TW);

TRCMABDMODUL =(V_MODUL) *(RCMABD + REVACT);
RCMABMODUL =(V_MODUL *(positif(COD2OP)))*(RCMRNABDMODUL+ 2RCMRNABDMODUL) ;
DRTNCMODUL =(V_MODUL) *(RCMTNC + REVPEA);
RTNCMODUL =(V_MODUL *(positif(COD2OP)))*(RCMRNTNCMODUL + 2RCMRNTNCMODUL) ;

RCMORDTOMODUL =(V_MODUL* (positif(COD2OP)))*(RCMRNABDMODUL+RCMRNTNCMODUL+RNET2CH+REGPRIVMMODUL+RCMHAB+RCMHAD+COD2TT+RNET2VV2WW+COD2YY+COD2ZZ);
RCMQUTOMODUL =(V_MODUL*(positif(COD2OP)))*(2RCMRNABDMODUL+2RCMRNTNCMODUL+RNETRCH+2REGPRIVMMODUL+INTERE+DISQUO);


INDRCMDEFMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMFR - RCMORDTOMODUL-RCMQUTOMODUL));

regle 821011:
application : iliad ;


TRCMRABD = TRCMRABDNOR + TRCMRABDMODUL;
TRCMRTNC = TRCMRTNCNOR + TRCMRTNCMODUL;

RCMORDTOT = RCMORDTOTNOR + RCMORDTOTMODUL;
RCMQUOTOT = RCMQUOTOTNOR + RCMQUOTOTMOD;
RCMTOT = RCMTOTNOR + RCMTOTMOD ;

TRCMABD = TRCMABDNOR +TRCMABDMODUL;
RCMAB =RCMABNOR + RCMABMODUL ;
DRTNC = DRTNCNOR + DRTNCMODUL;
RTNC = RTNCNOR + RTNCMODUL;
RAVC =RAVCNOR +  RAVCMODUL;

REPRCM2TU = REPRCM2TUNOR +  REPRCM2TUMODUL;


RCMORDTOTNET=RCMORDTONORM + RCMORDTOMODUL;
RCMQUOTOTNET =RCMQUTONOR+RCMQUTOMODUL;

RCMRABD = RCMRABDNOR + RCMRABDMODUL;
2RCMRABD =2RCMRABDNOR + 2RCMRABDMOD;
RCMRTNC =RCMRTNCNOR + RCMRTNCMODUL;
2RCMRTNC =2RCMRTNCNOR + 2RCMRTNCMODUL;

RCMRNABD = RCMRNABDNOR + RCMRNABDMODUL ;

2RCMRNABD = 2RCMRNABDNOR + 2RCMRNABDMODUL;

RCMRNTNC = RCMRNTNCNOR + RCMRNTNCMODUL ;

2RCMRNTNC = 2RCMRNTNCNOR + 2RCMRNTNCMODUL ;

REGPRIVM =REGPRIVMNOR + REGPRIVMMODUL;

2REGPRIVM =2REGPRIVMNOR + 2REGPRIVMMODUL;


INDRCMDEF =INDRCMDEF1 +INDRCMDEFMODUL;
regle 821020:
application : iliad ;

RCMFRORDI1 =(1-V_MODUL)*( arr(RCMORDTOT*RCMFR / RCMTOT));
RCMFRQUOT1 =(1-V_MODUL)*( max(0,RCMFR - RCMFRORDI1));
INDRCMDEFQ1 = ((1-V_MODUL)*(positif(RCMQUOTOTNET - RCMFRQUOT1)));
RCMFRQUOTI1 =(1-V_MODUL)*( (1- INDRCMDEFQ1) * max(0,RCMFRQUOT1 - RCMQUOTOTNET));
regle 888821021:
application : iliad ;

DEFRCMMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMFR-(RCMORDTOTNET+RCMQUOTOTNET)))*((RCMORDTOTNET+RCMQUOTOTNET)-RCMFR);

RCMFRORDIMODUL =(V_MODUL*(positif(COD2OP)))*(1-positif(DEFRCMMODUL))*(arr(RCMORDTOT*RCMFR / RCMTOT));
RCMFRQUOTMODUL =(V_MODUL*(positif(COD2OP)))*(1-positif(DEFRCMMODUL))*( max(0,RCMFR - RCMFRORDIMODUL));


INDRCMDEFQMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET - RCMFRQUOTMODUL));

RCMFRQUOTIMODUL = (V_MODUL*(positif(COD2OP)))*(1- INDRCMDEFQMODUL) *(max(0,RCMFRQUOTMODUL - RCMQUOTOTNET));
regle 821022:
application : iliad ;

RCMFRORDI =RCMFRORDI1 + RCMFRORDIMODUL;
RCMFRQUOT =RCMFRQUOT1 + RCMFRQUOTMODUL;

INDRCMDEFQ = INDRCMDEFQ1 + INDRCMDEFQMODUL;

RCMFRQUOTI = RCMFRQUOTI1 + RCMFRQUOTIMODUL;

regle 888821022:
application : iliad ;


RCMORDNETBMOD = (V_MODUL*(positif(COD2OP)))*(positif_ou_nul(RCMFRORDI-RCMQUOTOTNET))*(RCMORDNET-(RCMQUOTOTNET-RCMFRQUOT));


regle 821030:
application : iliad ;


RCMORDNET1 =(1-V_MODUL)*(  max(0,RCMORDTOTNET - RCMFRORDI - RCMFRQUOTI) * (1-INDRCMDEF));
2RCMFRDC1 =(1-V_MODUL)*( positif(REVPEA + PROVIE + DISQUO + INTERE + RESTUC) * arr(RCMFRQUOT * REVACT / RCMQUOTOT)
          + (1-positif(REVPEA + PROVIE + DISQUO + INTERE + RESTUC)) * RCMFRQUOT);
2RCMFRFU1 =(1-V_MODUL)*( positif(PROVIE + DISQUO + INTERE + RESTUC) * arr(RCMFRQUOT * REVPEA / RCMQUOTOT)
          + (1-positif(PROVIE + DISQUO + INTERE + RESTUC)) * max(0,RCMFRQUOT- 2RCMFRDC1));
2RCMFRCH1 =(1-V_MODUL)*( positif(INTERE + DISQUO + RESTUC) * arr(RCMFRQUOT * PROVIE / RCMQUOTOT)
          + (1-positif(INTERE + DISQUO + RESTUC)) * max(0,RCMFRQUOT- 2RCMFRDC1-2RCMFRFU1));
2RCMFRTR1 =(1-V_MODUL)*( positif(DISQUO + RESTUC) * arr(RCMFRQUOT * INTERE / RCMQUOTOT)
          + (1-positif(DISQUO + RESTUC)) * max(0,RCMFRQUOT- 2RCMFRDC1-2RCMFRFU1-2RCMFRCH1));
2RCMFRTS1 =(1-V_MODUL)*( positif(RESTUC) * arr(RCMFRQUOT * DISQUO / RCMQUOTOT)
          + (1-positif(RESTUC)) * max(0,RCMFRQUOT- 2RCMFRDC1-2RCMFRFU1-2RCMFRCH1-2RCMFRTR1));
2RCMFRGO1 =(1-V_MODUL)*( max(0,RCMFRQUOT - 2RCMFRDC1-2RCMFRFU1-2RCMFRCH1-2RCMFRTR1-2RCMFRTS1));

regle 888821031:
application : iliad ;


RCMORDNETMODUL =(V_MODUL*(positif(COD2OP)))*(RCMORDTOTNET-RCMFRORDI);
2RCMFRDCMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET+RCMFRQUOT))*(arr(RCMFRQUOT*(REVACT/RCMQUOTOT)));
2RCMFRFUMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET+RCMFRQUOT))*(arr(RCMFRQUOT*(REVPEA/RCMQUOTOT)));
2RCMFRCHMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET+RCMFRQUOT))*(arr(RCMFRQUOT*(PROVIE/RCMQUOTOT)));
2RCMFRTRMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET+RCMFRQUOT))*(arr(RCMFRQUOT*(INTERE/RCMQUOTOT)));
2RCMFRTSMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET+RCMFRQUOT))*(arr(RCMFRQUOT*(DISQUO/RCMQUOTOT)));
2RCMFRGOMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMQUOTOTNET+RCMFRQUOT))*(arr(RCMFRQUOT*(RESTUC/RCMQUOTOT)));
regle 821032:
application : iliad ;


RCMORDNET = RCMORDNET1 + RCMORDNETMODUL;

2RCMFRDC = 2RCMFRDC1 +2RCMFRDCMODUL;
2RCMFRFU = 2RCMFRFU1 + 2RCMFRFUMODUL;
2RCMFRCH = 2RCMFRCH1 + 2RCMFRCHMODUL;
2RCMFRTR = 2RCMFRTR1 + 2RCMFRTRMODUL;
2RCMFRTS = 2RCMFRTS1 + 2RCMFRTSMODUL;
2RCMFRGO = 2RCMFRGO1 + 2RCMFRGOMODUL;

regle 821040:
application : iliad ;

RCMQNET1 =(1-V_MODUL)*( max(0,RCMQUOTOTNET - 2RCMFRDC-2RCMFRFU-2RCMFRCH-2RCMFRTR-2RCMFRTS-2RCMFRGO) * INDRCMDEFQ * (1-INDRCMDEF));
2RCMDCNET1 =(1-V_MODUL)*( max(0,2RCMRNABD - 2RCMFRDC) * INDRCMDEFQ);
2RCMFUNET1 =(1-V_MODUL)*( max(0,2RCMRNTNC - 2RCMFRFU) * INDRCMDEFQ);
2RCMCHNET1 =(1-V_MODUL)*( max(0,2RCMRNCH - 2RCMFRCH) * INDRCMDEFQ);
2RCMTRNET1 =(1-V_MODUL)*( max(0,INTERE - 2RCMFRTR) * INDRCMDEFQ);
2RCMTSNET1 =(1-V_MODUL)*( max(0,DISQUO - 2RCMFRTS) * INDRCMDEFQ); 
2RCMGONET1 =(1-V_MODUL)*( max(0,2REGPRIVM - 2RCMFRGO) * INDRCMDEFQ);
RCMTOTNET1 =(1-V_MODUL)*( RCMQNET1 + RCMORDNET) ;

regle 888821041:
application : iliad ;

2RCMDCNETMODUL =(V_MODUL*(positif(COD2OP)))*( max(0,2RCMRNABD - 2RCMFRDC)) ;
2RCMFUNETMODUL =(V_MODUL*(positif(COD2OP)))*(max(0,2RCMRNTNC - 2RCMFRFU));
2RCMCHNETMODUL =(V_MODUL*(positif(COD2OP)))*( max(0,RNETRCH - 2RCMFRCH)) ;
2RCMTRNETMODUL =(V_MODUL*(positif(COD2OP)))*( max(0,INTERE - 2RCMFRTR));
2RCMTSNETMODUL =(V_MODUL*(positif(COD2OP)))*( max(0,DISQUO - 2RCMFRTS));
2RCMGONETMODUL =(V_MODUL*(positif(COD2OP)))*( max(0,2REGPRIVM- 2RCMFRGO));
RCMQNETMODUL =(V_MODUL*(positif(COD2OP)))*(2RCMDCNETMODUL+2RCMFUNETMODUL+2RCMCHNETMODUL+2RCMGONETMODUL+2RCMTRNETMODUL+2RCMTSNETMODUL);
RCMTOTNETMODUL =((V_MODUL*(positif(COD2OP)))*( RCMQNETMODUL + RCMORDNET))+((V_MODUL*(1-positif(COD2OP)))*( RCMQNETMODUL + RCMORDNET)) ;

regle 821042:
application : iliad ;


RCMQNET = RCMQNET1 + RCMQNETMODUL;
2RCMDCNET = 2RCMDCNET1 + 2RCMDCNETMODUL;
2RCMFUNET = 2RCMFUNET1 + 2RCMFUNETMODUL;
2RCMCHNET = 2RCMCHNET1 + 2RCMCHNETMODUL;
2RCMTRNET = 2RCMTRNET1 + 2RCMTRNETMODUL;
2RCMTSNET = 2RCMTSNET1 + 2RCMTSNETMODUL;
2RCMGONET = 2RCMGONET1 + 2RCMGONETMODUL;
RCMTOTNET = RCMTOTNET1 + RCMTOTNETMODUL;


regle 821050:
application : iliad ;

RCMFRTEMP =(1-V_MODUL)*( min(RCMAB + RTNC + TRCMRNCH + RCMNAB + RTCAR + RCMPRIVM+COD2TT,RCMFR)) ;

regle 821060:
application : iliad ;

BRCMBIS =(1-V_MODUL)*( RCMAB + RTNC + TRCMRNCH + RCMNAB + RTCAR + RCMPRIVM) ;
BRCMBISB =(1-V_MODUL)*( RCMRNABD + RCMRNTNC + RCMHAD + RCMHAB + REGPRIVM + RCMRNCH +COD2TT);
BRCMBISQ =(1-V_MODUL)*( 2RCMRNABD + 2RCMRNTNC + DISQUO + INTERE + 2REGPRIVM + 2RCMRNCH) ;
DEFRCMI =(1-V_MODUL)*( BRCMBISB1 + BRCMBISQ1) ;

regle 821070:
application : iliad ;

DEFRCMIMPU =(1-V_MODUL)*( positif(null(PREM8_11)*positif(SOMMERCM_2)* positif(BRCMBISB + BRCMBISQ-RCMFR))
                  * (max(0,REPRCM - max(REPRCMB1731,max(REPRCMB_P,REPRCMBP2))
                                        - max(0,REPRCMB-REPRCMBP3)))
           + PREM8_11 * positif(BRCMBISB + BRCMBISQ-RCMFR) * REPRCM 
            + 0);

regle 821080:
application : iliad ;

RCMFRART1731 =(1-V_MODUL)*( RCMFRTEMP);

regle 821090 :
application : iliad ;


R2FA =(1-V_MODUL)*( max(0,COD2FA)) ;

regle 821100:
application : iliad ;


DFRCMNBIS =(1-V_MODUL)*( min(0,RCMORDTOTNET - RCMFRORDI + RCMQUOTOTNET - 2RCMFRDC-2RCMFRFU-2RCMFRCH-2RCMFRTR-2RCMFRTS-2RCMFRGO) * (-1));
DFRCMN =(1-V_MODUL)*( DFRCMNBIS * null(V_IND_TRAIT-4) + (RCMFR - RCMFRART1731) *  null(V_IND_TRAIT-5));

regle 821110:
application : iliad ;

1RCM_I =(1-V_MODUL)*( si( (V_REGCO + 0) dans (1,3,5,6) )
              alors  ((1-positif(DFRCMNBIS)) * RCMORDNET)
              sinon (0)
          finsi);
2RCM_I = (1-V_MODUL)*( si( (V_REGCO + 0)  dans (1,3,5,6))
              alors ((1- positif(DFRCMNBIS)) * 2RCMDCNET)
              sinon (0)
          finsi);
3RCM_I =(1-V_MODUL)*( si( (V_REGCO + 0)  dans (1,3,5,6))
             alors  ((1- positif(DFRCMNBIS)) * 2RCMFUNET)
             sinon (0)
         finsi);
4RCM_I =(1-V_MODUL)*( si( (V_REGCO + 0)  dans (1,3,5,6))
             alors  ((1- positif(DFRCMNBIS)) * 2RCMCHNET)
             sinon (0)
         finsi);
5RCM_I =(1-V_MODUL)*( si( (V_REGCO + 0)  dans (1,3,5,6))
             alors ((1- positif(DFRCMNBIS)) * 2RCMTSNET)
             sinon (0)
         finsi);
6RCM_I =(1-V_MODUL)*( si( (V_REGCO + 0)  dans (1,3,5,6))
             alors  ((1- positif(DFRCMNBIS)) * 2RCMGONET)
             sinon (0)
         finsi);
7RCM_I =(1-V_MODUL)*( si( (V_REGCO + 0)  dans (1,3,5,6))
             alors  ((1- positif(DFRCMNBIS)) * 2RCMTRNET) 
             sinon (0)
         finsi);

RCM_I =(1-V_MODUL)*( 1RCM_I + 2RCM_I + 3RCM_I + 4RCM_I + 5RCM_I + 6RCM_I + 7RCM_I) ;

regle 82014:
application : iliad ;
1REPRCM =(1-V_MODUL)*( (DEFRCM + DEFRCM2 + DEFRCM3 + DEFRCM4 + DEFRCM5 + DEFRCM6));
regle 888820150:
application : iliad ;


REPRCMODUL =((V_MODUL*(positif(COD2OP)))*(DEFRCM + DEFRCM2 + DEFRCM3 + DEFRCM4 + DEFRCM5 + DEFRCM6))+((V_MODUL*(1-positif(COD2OP)))*(DEFRCM + DEFRCM2 + DEFRCM3 + DEFRCM4 + DEFRCM5 + DEFRCM6));

regle 82016:
application : iliad ;

REPRCM=1REPRCM + REPRCMODUL;
regle 8201402:
application : iliad ;
REPRCMB =(1-V_MODUL)*(  max(0,BRCMBISB + BRCMBISQ - RCMFRTEMP));
regle 8201404:
application : iliad ;
REPRCMBIS1 =(1-V_MODUL)*( positif(positif(SOMMERCM_2)*null(PREM8_11) * positif(BRCMBISB + BRCMBISQ-RCMFR) + PREM8_11 * positif(BRCMBISB + BRCMBISQ-RCMFR))
           * max(0,REPRCM - DEFRCMIMPU)
            + (1-positif(positif(SOMMERCM_2)*null(PREM8_11) * positif(BRCMBISB + BRCMBISQ-RCMFR) + PREM8_11 * positif(BRCMBISB + BRCMBISQ-RCMFR)))
             * min(REPRCM,REPRCMB) + 0);
REPRCM11 =(1-V_MODUL)*( positif(REPRCMBIS1) * arr( (REPRCMBIS1 * 1RCM_I)/ RCM_I)
        + (1 - positif(REPRCMBIS1)) * 0) ;
REPRCM21 =(1-V_MODUL)*( positif(REPRCMBIS1) * min(arr((REPRCMBIS1 * 2RCM_I)/ RCM_I), REPRCMBIS1 - REPRCM11)
        + (1 - positif(REPRCMBIS1)) * 0) ;
REPRCM31 =(1-V_MODUL)*( positif(REPRCMBIS1) * min(arr((REPRCMBIS1 * 3RCM_I)/ RCM_I), REPRCMBIS1 - REPRCM11 - REPRCM21)
        + (1 - positif(REPRCMBIS1)) * 0) ;
REPRCM41 =(1-V_MODUL)*( positif(REPRCMBIS1) * min(arr((REPRCMBIS1 * 4RCM_I)/ RCM_I), REPRCMBIS1 - REPRCM11 - REPRCM21 - REPRCM31)
        + (1 - positif(REPRCMBIS1)) * 0) ;
REPRCM51 =(1-V_MODUL)*( positif(REPRCMBIS1) * min(arr((REPRCMBIS1 * 5RCM_I)/ RCM_I), REPRCMBIS1 - REPRCM11 - REPRCM21 - REPRCM31 - REPRCM41)
        + (1 - positif(REPRCMBIS1)) * 0) ;
REPRCM61 =(1-V_MODUL)*( positif(REPRCMBIS1) * min(arr((REPRCMBIS1 * 6RCM_I)/ RCM_I), REPRCMBIS1 - REPRCM11 - REPRCM21 - REPRCM31 - REPRCM41 - REPRCM51)
        + (1 - positif(REPRCMBIS1)) * 0) ;
REPRCM71 =(1-V_MODUL)*( positif(REPRCMBIS1) * max(0,REPRCMBIS1 - REPRCM11 -REPRCM21 - REPRCM31 - REPRCM41 - REPRCM51  - REPRCM61 )
        + (1 - positif(REPRCMBIS1)) * 0) ;
regle 201404:
application : iliad ;


REPRCM1MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*RCMORDNET/RCMTOTNET));
REPRCM2MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*2RCMDCNET/RCMTOTNET));
REPRCM3MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*2RCMFUNET/RCMTOTNET));
REPRCM4MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*2RCMCHNET/RCMTOTNET));
REPRCM5MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*2RCMTSNET/RCMTOTNET));
REPRCM6MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*2RCMGONET/RCMTOTNET));
REPRCM7MODUL=(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*(arr(REPRCM*2RCMTRNET/RCMTOTNET));


regle 8201406:
application : iliad ;

REPRCMBIS=REPRCMBIS1+REPRCMBISMODUL;
REPRCM1=REPRCM11+REPRCM1MODUL;
REPRCM2=REPRCM21+REPRCM2MODUL;
REPRCM3=REPRCM31+REPRCM3MODUL;
REPRCM4=REPRCM41+REPRCM4MODUL;
REPRCM5=REPRCM51+REPRCM5MODUL;
REPRCM6=REPRCM61+REPRCM6MODUL;
REPRCM7=REPRCM71+REPRCM7MODUL;
regle 82015:
application : iliad ;
DFRCM51 =(1-V_MODUL)*(   null(4-V_IND_TRAIT) * min(DEFRCM6,REPRCM - REPRCMBIS)
           + null(5-V_IND_TRAIT) * min(DEFRCM6,REPRCM - REPRCMBIS));
regle 88882015:
application : iliad ;

DFRCM5MODUL =(V_MODUL)*(positif(COD2OP))* min(DEFRCM6,(max(0,DEFRCM6-((RCMTOTNET)-(DEFRCM5+DEFRCM4+DEFRCM3+DEFRCM2+DEFRCM)))));

regle 820151:
application : iliad ;

DFRCM5 = DFRCM51+ DFRCM5MODUL;

regle 821140:
application : iliad ;



DFRCM41 =(1-V_MODUL)*(  null(4-V_IND_TRAIT) * min(DEFRCM5,REPRCM - REPRCMBIS - DFRCM5 )
           + null(5-V_IND_TRAIT) * min(DEFRCM5,REPRCM - REPRCMBIS - DFRCM5 ));
regle 888821140:
application : iliad ;

DFRCM4MODUL =(V_MODUL)*(positif(COD2OP))*min(DEFRCM5,(max(0,DEFRCM5-((RCMTOTNET)-(DEFRCM4+DEFRCM3+DEFRCM2+DEFRCM)))));


regle 821141:
application : iliad ;

DFRCM4 = DFRCM41 + DFRCM4MODUL;

regle 821150:
application : iliad ;

DFRCM31 =(1-V_MODUL)*(  null(4-V_IND_TRAIT) * min(DEFRCM4,REPRCM - REPRCMBIS - DFRCM5 - DFRCM4 )
           + null(5-V_IND_TRAIT) * min(DEFRCM4,REPRCM - REPRCMBIS - DFRCM5 - DFRCM4 ));
regle 888821150:
application : iliad ;

DFRCM3MODUL =(V_MODUL)*(positif(COD2OP))* min(DEFRCM4,(max(0,DEFRCM4-((RCMTOTNET)-(DEFRCM3+DEFRCM2+DEFRCM)))));

regle 821151:
application : iliad ;

DFRCM3 = DFRCM31 + DFRCM3MODUL;
regle 821160:
application : iliad ;

DFRCM21 =(1-V_MODUL)*(  null(4-V_IND_TRAIT) * min(DEFRCM3,REPRCM - REPRCMBIS - DFRCM5 - DFRCM4-DFRCM3)
           + null(5-V_IND_TRAIT) * min(DEFRCM3,REPRCM - REPRCMBIS - DFRCM5 - DFRCM4-DFRCM3));
regle 888821160:
application : iliad ;

DFRCM2MODUL =(V_MODUL)*(positif(COD2OP))* min(DEFRCM3,(max(0,DEFRCM3-((RCMTOTNET)-(DEFRCM2+DEFRCM)))));
           
regle 821161:
application : iliad ;

DFRCM2 = DFRCM21 + DFRCM2MODUL;
regle 821170:
application : iliad ;

DFRCM11 =(1-V_MODUL)*(  null(4-V_IND_TRAIT) * min(DEFRCM2,REPRCM-REPRCMBIS-DFRCM5-DFRCM4-DFRCM3-DFRCM2)
           + null(5-V_IND_TRAIT) * min(DEFRCM2,REPRCM-REPRCMBIS-DFRCM5-DFRCM4-DFRCM3-DFRCM2));
regle 888821170:
application : iliad ;

DFRCM1MODUL =(V_MODUL)*(positif(COD2OP))* min(DEFRCM2,(max(0,DEFRCM2-((RCMTOTNET)-DEFRCM))));
regle 821171:
application : iliad ;

DFRCM1 = DFRCM11 + DFRCM1MODUL;
regle 821180:
application : iliad ;


RCM11 =(1-V_MODUL)*( (1-V_CNR) * (
              max(0,(1RCM_I-REPRCM1)) +0));
RCM2FA =(1-V_MODUL)*( COD2FA * (1 - V_CNR)); 
2RCM1 =(1-V_MODUL)*( (1-V_CNR) * (
                max(0,(2RCM_I-REPRCM2)) +0));
3RCM1 =(1-V_MODUL)*( (1-V_CNR) * (
                max(0,(3RCM_I-REPRCM3)) +0)); 
4RCM1 =(1-V_MODUL)*( (1-V_CNR) * (
                max(0,(4RCM_I-REPRCM4)) +0));
5RCM1 =(1-V_MODUL)*( (1-V_CNR) * (
                max(0,(5RCM_I-REPRCM5)) +0));
6RCM1 =(1-V_MODUL)*( (1-V_CNR) * (
                max(0,(6RCM_I-REPRCM6)) +0));
7RCM1 =(1-V_MODUL)*( (1-V_CNR) * (
                max(0,(7RCM_I-REPRCM7)) +0));

DFRCM0 =(1-V_MODUL)*( (DFRCMN + DFRCM1+DFRCM2+DFRCM3+DFRCM4+DFRCM5) * (1-V_CNR)) ;
RCMEXCREF1 =(1-V_MODUL)*( max(0,TRCMRABD + TRCMRTNC) * (1 - V_CNR)) ;

regle 888821181:
application : iliad ;


RCM1MODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) *(arr(
              max(0,(RCMORDNET-REPRCM1)))));

2RCMMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) *(arr (2RCMDCNET-REPRCM2)));
              
	      
3RCMMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) *(arr (2RCMFUNET-REPRCM3)));


4RCMMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) * (arr(2RCMCHNET-REPRCM4)));


5RCMMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) * (arr(2RCMTSNET-REPRCM5)));


6RCMMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) * (arr(2RCMGONET-REPRCM6)));

	      
7RCMMODUL =(V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) * (arr(2RCMTRNET-REPRCM7)));


RI2QUOTMODUL = (V_MODUL*(positif(COD2OP)))*(positif(RCMTOTNET-REPRCM))*( (1-V_CNR) *(arr(2RCMMODUL+3RCMMODUL+4RCMMODUL+5RCMMODUL+6RCMMODUL+7RCMMODUL))) ;



DFRCMODUL =((V_MODUL*(positif(COD2OP)))*(max(0,DEFRCM6-(RCMTOTNET-(DEFRCM+DEFRCM2+DEFRCM3+DEFRCM4+DEFRCM5) * (1-V_CNR)))));
RCMEXCREFMODUL =(V_MODUL)*(positif(COD2OP)*( max(0,TRCMRABD + TRCMRTNC) * (1 - V_CNR))) ;

regle 8211812:
application : iliad ;

RCM1 = RCM11 + RCM1MODUL + RCM1MODUL2;
2RCM = 2RCM1 + 2RCMMODUL;
3RCM = 3RCM1 + 3RCMMODUL;
4RCM = 4RCM1 + 4RCMMODUL;
5RCM = 5RCM1 + 5RCMMODUL;
6RCM = 6RCM1 + 6RCMMODUL;
7RCM = 7RCM1 + 7RCMMODUL;

DFRCM = DFRCM0 + DFRCMODUL+DFRCMODUL2;
RCMEXCREF = RCMEXCREF1 + RCMEXCREFMODUL;

regle 821190:
application : iliad ;


ABTAV = PLAF_RCMAV1 * (1 + BOOL_0AM) ;

regle 821200:
application : iliad ;


BPLIB1 =(1-V_MODUL)*( (min( RCMLIB, max(0 , ABTAV - RAVC) ) * (1 - V_CNR))) ;

regle 888821200:
application : iliad ;


BPLIBMODUL =(V_MODUL*(positif(COD2OP)))*((ABT2DH)* (1 - V_CNR));

regle 821201:
application : iliad ;


BPLIB =BPLIB1 + BPLIBMODUL + BPLIBMODUL2;

regle 821210:
application : iliad ;


EPAV1 =(1-V_MODUL)*( arr(BPLIB * TX_PREVLIB/100)) ;

regle 888821210:
application : iliad ;


EPAVMODUL =(V_MODUL*(positif(COD2OP)))*(arr(BPLIB* TX_PREVLIB/100)) ;
regle 821211:
application : iliad ;

EPAV =EPAV1 + EPAVMODUL +EPAVMODUL2;

regle 888821212:
application : iliad ;



REVBRUTASSU =(V_MODUL * (1-positif(COD2OP)) * (RCMAV+ RCMLIB + COD2VV + COD2WW));


REVNET2CH = (V_MODUL*(1-positif(COD2OP)))*(si (REVBRUTASSU <= ABTAV)
            alors (0)
	    finsi);
          

REVNET2VV = (V_MODUL*(1-positif(COD2OP)))*(si(REVNET2CH = 0)
           alors(0)
         finsi);

REVNET2WW = (V_MODUL*(1-positif(COD2OP)))*(si(REVNET2CH = 0)
          alors(0)
        finsi);

REVBRUTASSU1= ((V_MODUL)*(1-positif(COD2OP)))*(positif(REVBRUTASSU-ABTAV));

RNABT2CH = (V_MODUL * (1-positif(COD2OP)))*(max(0,(RCMAV-ABTAV)));

ABT2CH2  = (V_MODUL *(1-positif(COD2OP)))*(RCMAV-RNABT2CH);

RNABT2DH = (V_MODUL*(1-positif(COD2OP)))*(max(0,(RCMLIB)-(ABTAV-ABT2CH2)));

ABT2DH2 = (V_MODUL *(1-positif(COD2OP)))*(RCMLIB-RNABT2DH);

RNABT2VV = (V_MODUL *(1-positif(COD2OP)))*(max(0,(COD2VV-(ABTAV-(ABT2CH2+ABT2DH2)))));

ABT2VV = (V_MODUL *(1-positif(COD2OP)))*(COD2VV-RNABT2VV);

RNABT2WW = (V_MODUL *(1-positif(COD2OP)))*(max(0,(COD2WW-(ABTAV-(ABT2CH2+ABT2DH2+ABT2VV)))));

ABT2WW = (V_MODUL *(1-positif(COD2OP)))*(COD2WW-RNABT2WW);



RCMIMPTN = (V_MODUL*(1-positif(COD2OP)))*(arr(RCMABD + RCMTNC + RCMHAD + RCMHAB + COD2TT + (REGPRIV*MAJREV)+RNABT2WW + COD2ZZ));


RCMIMPTR = (V_MODUL*(1-positif(COD2OP)))*(RNABT2VV);


RCMBAR =(V_MODUL*(1-positif(COD2OP)))*( RNABT2CH + COD2YY);

RCM1MODUL2=(V_MODUL*(1-positif(COD2OP)))* RCMBAR;


IMPOT128 =(V_MODUL*(1-positif(COD2OP)))*( RCMIMPTN*TX128/100);

IMPOT75 = (V_MODUL*(1-positif(COD2OP)))* ( RCMIMPTR*TX075/100);


BPLIBMODUL2 =(V_MODUL*(1-positif(COD2OP)))*(ABT2DH2)* (1 - V_CNR);


EPAVMODUL2 =(V_MODUL*(1-positif(COD2OP)))*( arr(BPLIBMODUL2* TX_PREVLIB/100)) ;

DFRCMODUL2 = (V_MODUL*(1-positif(COD2OP)))*(max(0,(DEFRCM2+DEFRCM3+DEFRCM4+DEFRCM5+DEFRCM6)))*(1-V_CNR);
