#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle 901000:
application : iliad ;

CONST0 = 0;
CONST1 = 1;
CONST2 = 2;
CONST3 = 3;
CONST4 = 4;
CONST10 = 10;
CONST20 = 20;
CONST40 = 40;

regle 901010:
application :  iliad ;


LIG0 = (1 - positif(RE168 + TAX1649)) * IND_REV ;

LIG01 = (1 - (positif(REVFONC) * (1 - IND_REV8FV))) * (1 - positif(RE168 + TAX1649)) * (1 - positif(ANNUL2042)) * IND_REV ;

LIG1 = (1 - positif(RE168 + TAX1649)) ;

LIG2 = (1 - positif(ANNUL2042)) ;

LIG3 = positif(positif(CMAJ + 0) 
	+ positif_ou_nul(MAJTX1 - 40) + positif_ou_nul(MAJTX4 - 40)
        + positif_ou_nul(MAJTXPCAP1 - 40) + positif_ou_nul(MAJTXPCAP4 - 40)
        + positif_ou_nul(MAJTXLOY1 - 40) + positif_ou_nul(MAJTXLOY4 - 40)
        + positif_ou_nul(MAJTXCHR1 - 40) + positif_ou_nul(MAJTXCHR4 - 40)
	+ positif_ou_nul(MAJTXC1 - 40) + positif_ou_nul(MAJTXC4 - 40) 
        + positif_ou_nul(MAJTXCVN1 - 40) + positif_ou_nul(MAJTXCVN4 - 40)
	+ positif_ou_nul(MAJTXCDIS1 - 40) + positif_ou_nul(MAJTXCDIS4 - 40)
        + positif_ou_nul(MAJTXGLO1 - 40) + positif_ou_nul(MAJTXGLO4 - 40)
        + positif_ou_nul(MAJTXRSE11 - 40) + positif_ou_nul(MAJTXRSE14 - 40)
        + positif_ou_nul(MAJTXRSE51 - 40) + positif_ou_nul(MAJTXRSE54 - 40)
	+ positif_ou_nul(MAJTXRSE21 - 40) + positif_ou_nul(MAJTXRSE24 - 40)
        + positif_ou_nul(MAJTXRSE31 - 40) + positif_ou_nul(MAJTXRSE34 - 40)
        + positif_ou_nul(MAJTXRSE41 - 40) + positif_ou_nul(MAJTXRSE44 - 40)
        + positif_ou_nul(MAJTXRSE61 - 40) + positif_ou_nul(MAJTXRSE64 - 40)
        + positif_ou_nul(MAJTXRSE71 - 40) + positif_ou_nul(MAJTXRSE74 - 40)
        + positif_ou_nul(MAJTXTAXA4 - 40)) ;

CNRLIG12 = (1 - V_CNR) * LIG1 * LIG2 ;

CNRLIG1 = (1 - V_CNR) * LIG1 ;

regle 901020:
application :  iliad ;


LIG0010 = (INDV * INDC * INDP) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0020 = (INDV * (1 - INDC) * (1 - INDP)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0030 = (INDC * (1 - INDV) * (1 - INDP)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0040 = (INDP * (1 - INDV) * (1 - INDC)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0050 = (INDV * INDC * (1 - INDP)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0060 = (INDV * INDP * (1 - INDC)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0070 = (INDC * INDP * (1 - INDV)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG10YT = (INDV * INDC * INDP) * ART1731BIS * LIG0 * LIG2 ;

LIG20YT = (INDV * (1 - INDC) * (1 - INDP)) * ART1731BIS * LIG0 * LIG2 ;

LIG30YT = (INDC * (1 - INDV) * (1 - INDP)) * ART1731BIS * LIG0 * LIG2 ;

LIG40YT = (INDP * (1 - INDV) * (1 - INDC)) * ART1731BIS * LIG0 * LIG2 ;

LIG50YT = (INDV * INDC * (1 - INDP)) * ART1731BIS * LIG0 * LIG2 ;

LIG60YT = (INDV * INDP * (1 - INDC)) * ART1731BIS * LIG0 * LIG2 ;

LIG70YT = (INDC * INDP * (1 - INDV)) * ART1731BIS * LIG0 * LIG2 ;

regle 901030:
application :  iliad ;


LIG10V = positif_ou_nul(TSBNV + PRBV + BPCOSAV + GLDGRATV + positif(F10AV * null(TSBNV + PRBV + BPCOSAV + GLDGRATV))) ;
LIG10C = positif_ou_nul(TSBNC + PRBC + BPCOSAC + GLDGRATC + positif(F10AC * null(TSBNC + PRBC + BPCOSAC + GLDGRATC))) ;
LIG10P = positif_ou_nul(somme(i=1..4:TSBNi + PRBi) + positif(F10AP * null(somme(i=1..4:TSBNi + PRBi)))) ;
LIG10 = positif(LIG10V + LIG10C + LIG10P) ;

regle 901040:
application :  iliad ;

LIG1100 = positif(T2RV) ;

LIG899 = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LIG0010 + LIG0020 + LIG0030 + LIG0040 + LIG0050 + LIG0060 + LIG0070)) 
		 * (1 - ART1731BIS) ; 

LIG900 = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LIG0010 + LIG0020 + LIG0030 + LIG0040 + LIG0050 + LIG0060 + LIG0070) 
		 * (1 - ART1731BIS) ; 

LIG899YT = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		   + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LIG10YT + LIG20YT + LIG30YT + LIG40YT + LIG50YT + LIG60YT + LIG70YT)) 
		 * ART1731BIS ; 

LIG900YT = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB + LIGPV3VE 
		   + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LIG10YT + LIG20YT + LIG30YT + LIG40YT + LIG50YT + LIG60YT + LIG70YT) 
		 * ART1731BIS ; 

regle 901060:
application : iliad  ;

LIGBAM = positif(COD5XB + COD5YB + COD5ZB) * LIG1 ;

LIGBAMPV = positif(BAFPVV + BAFPVC + BAFPVP) * LIG1 ;

LIGBAMMV = positif(COD5XO + COD5YO + COD5ZO) * LIG1 ;

LIGBAMTOT = positif(LIGBAM + LIGBAMPV + LIGBAMMV) * LIG1 ;

LIGCBOIS = positif(BAFORESTV + BAFORESTC + BAFORESTP) * LIG1 ;

LIG13 =  positif(present(BACDEV)+ present(BACREV) + present(COD5AK) + present(BAHDEV) +present(BAHREV) + present(COD5AL)
                 + present(BACDEC) +present(BACREC) + present(COD5BK) + present(BAHDEC)+ present(BAHREC) + present(COD5BL)
                 + present(BACDEP)+ present(BACREP) + present(COD5CK) + present(BAHDEP)+ present(BAHREP) + present(COD5CL)
                 + present(4BAHREV) + present(4BAHREC) + present(4BAHREP) + present(4BACREV) + present(4BACREC) + present(4BACREP))
	* (1 - positif(ANNUL2042)) * LIG1 ;

LIGBAMICF1 = (1 - positif(BAFORESTV + BAFORESTC + BAFORESTP + LIG13 + DEFANTBAF + 0)) * LIGBAMTOT ;

LIGBAMICF2 = 1 - LIGBAMICF1 ;

BAFORESTOT = (BAFORESTV + BAFORESTC + BAFORESTP) * (1 - positif(IBAMICF + LIG13 + DEFANTBAF)) ;

regle 901070:
application :  iliad ;

4BAQLV = positif(4BACREV + 4BAHREV) ;
4BAQLC = positif(4BACREC + 4BAHREC) ;
4BAQLP = positif(4BACREP + 4BAHREP) ;

regle 901080:
application : iliad  ;

LIG134V = positif(present(BAHREV) + present(BAHDEV) + present(BACREV) + present(BACDEV) + present(4BAHREV) + present(4BACREV)
                  + present(BAFPVV) + present(COD5AK) + present(COD5AL)
		  + present(COD5XB) + present(COD5XD) + present(COD5XE) + present(COD5XF) + present(COD5XN) + present(COD5XO)) ;

LIG134C = positif(present(BAHREC) + present(BAHDEC) + present(BACREC) + present(BACDEC) + present(4BAHREC) + present(4BACREC)
                  + present(BAFPVC) + present(COD5BK) + present(COD5BL)
		  + present(COD5YB) + present(COD5YD) + present(COD5YE) + present(COD5YF) + present(COD5YN) + present(COD5YO)) ;

LIG134P = positif(present(BAHREP) + present(BAHDEP) + present(BACREP) + present(BACDEP) + present(4BAHREP) + present(4BACREP)
                  + present(BAFPVP) + present(COD5CK) + present(COD5CL)
		  + present(COD5ZB) + present(COD5ZD) + present(COD5ZE) + present(COD5ZF) + present(COD5ZN) + present(COD5ZO)) ;

LIG134 = positif(LIG134V + LIG134C + LIG134P + present(DAGRI6) + present(DAGRI5) + present(DAGRI4) + present(DAGRI3) + present(DAGRI2) + present(DAGRI1)) 
	 * (1 - LIGBAMICF1) * (1 - BAFORESTOT) * (1 - positif(ANNUL2042)) * LIG1 ;

LIGDBAIP = positif(DEFANTBAF + 0) * LIG1 ;

BAHQTAVIS = (1 - positif_ou_nul(BAHQT)) * positif(SHBA + (REVTP - BA1) + REVQTOTQHT - SEUIL_IMPDEFBA) ;

LIGBAHQ = positif(LIG13 + present(DAGRI6) + present(DAGRI5) + present(DAGRI4) + present(DAGRI3) + present(DAGRI2) + present(DAGRI1) + (LIGCBOIS * LIGBAMTOT)) * LIG1 ;

LIGBAQ = positif(present(4BACREV) + present(4BAHREV) + present(4BACREC) + present(4BAHREC) + present(4BACREP) + present(4BAHREP)) ;

regle 901090:
application : iliad  ;

LIG136 = positif(BAQNODEFV + DEFANTBAQV + BAQNODEFC + DEFANTBAQC + BAQNODEFP + DEFANTBAQP) * (1 - positif(ANNUL2042)) * LIG1 ;

LIG138 = positif(BATMARGTOT) * (1 - positif(ANNUL2042)) * LIG1 ;

regle 901100:
application : iliad ;

LIGBICPRO = positif(BICNOV + COD5DF + CODCKC + BICDNV + BIHNOV + COD5DG + CODCKI + BIHDNV
                    + BICNOC + COD5EF + CODCLC + BICDNC + BIHNOC + COD5EG + CODCLI + BIHDNC
		    + BICNOP + COD5FF + CODCMC + BICDNP + BIHNOP + COD5FG + CODCMI + BIHDNP) * (1 - positif(ANNUL2042)) * LIG0 ;

LIGBICPROQ = positif(CODCKC + CODCKI + CODCLC + CODCLI + CODCMC + CODCMI) * (1 - positif(ANNUL2042)) * LIG0 ;

LIGMICPV = positif(MIBNPPVV + MIBNPPVC + MIBNPPVP) ;

LIGMICMV = positif(MIBNPDCT + COD5RZ + COD5SZ) ;

LIGBICNP = positif(BICREV + COD5UR + CODCNC + BICDEV + BICHREV + COD5US + CODCNI + BICHDEV
                   + BICREC + COD5VR + CODCOC + BICDEC + BICHREC + COD5VS + CODCOI + BICHDEC
                   + BICREP + COD5WR + CODCPC + BICDEP + BICHREP + COD5WS + CODCPI + BICHDEP) * (1 - positif(ANNUL2042)) * LIG0 ;

LIGBICNPQ = positif(CODCNC + CODCNI + CODCOC + CODCOI + CODCPC + CODCPI) * (1 - positif(ANNUL2042)) * LIG0 ;

LIG_DEFNPI = positif(present ( DEFBIC6 ) + present ( DEFBIC5 ) + present ( DEFBIC4 ) 
                     + present ( DEFBIC3 ) + present ( DEFBIC2 ) + present ( DEFBIC1 )) * LIG0 * LIG2 ;

LIGMLOC = positif(present(MIBMEUV) + present(MIBMEUC) + present(MIBMEUP)
		  + present(MIBGITEV) + present(MIBGITEC) + present(MIBGITEP)
		  + present(LOCGITV) + present(LOCGITC) + present(LOCGITP)
		  + present(COD5NW) + present(COD5OW) + present(COD5PW))
	  * LIG0 * LIG2 ;
 
LIGMLOCAB = positif(MLOCABV + MLOCABC + MLOCABP) * LIG0  * LIG2 ; 

LIGMIBMV = positif(BICPMVCTV + BICPMVCTC + BICPMVCTP) * LIG0 ;

LIGBNCPV = positif(BNCPROPVV + BNCPROPVC + BNCPROPVP) * LIG0 ;

LIGBNCMV = positif(BNCPMVCTV + BNCPMVCTC + BNCPMVCTP) * LIG0 ;

LIGBNCNPPV = positif(BNCNPPVV + BNCNPPVC + BNCNPPVP) * LIG0 ;

LIGBNCNPMV = positif(BNCNPDCT + COD5LD + COD5MD) * LIG0 ;

LIGSPENETPF = positif(BNCPROV + BNCPROC + BNCPROP + BNCPROPVV + BNCPROPVC + BNCPROPVP + BNCPMVCTV + BNCPMVCTC + BNCPMVCTP) * LIG0 ;

LIGBNCPHQ = positif(present(BNHREV) + present(COD5XK) + present(BNCREV) + present(COD5XJ) + present(BNHDEV) + present(BNCDEV)
                    + present(BNHREC) + present(COD5YK) + present(BNCREC) + present(COD5YJ) + present(BNHDEC) + present(BNCDEC)
                    + present(BNHREP) + present(COD5ZK) + present(BNCREP) + present(COD5ZJ) + present(BNHDEP) + present(BNCDEP)) ;

LIGBNCAFFQ = positif(present(CODCQC) + present(CODCQI) + present(CODCRC) + present(CODCRI) + present(CODCSC) + present(CODCSI)) * LIG0 ;

LIGBNCAFF = positif(LIGBNCPHQ + (LIGSPENETPF * LIGBNCAFFQ)) * LIG0 ;

LIGNPLOC = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP
		   + COD5EY + COD5EZ + COD5FY + COD5FZ + COD5GY + COD5GZ)
		   * LIG0 ;

LIGNPLOCF = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC
                   + LNPRODEF10 + LNPRODEF9 + LNPRODEF8 + LNPRODEF7 + LNPRODEF6 + LNPRODEF5
                   + LNPRODEF4 + LNPRODEF3 + LNPRODEF2 + LNPRODEF1
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP
		   + COD5EY + COD5EZ + COD5FY + COD5FZ + COD5GY + COD5GZ)
		   * LIG0 ;

LIGDEFNPLOC = positif(TOTDEFLOCNP) ;

LIGDFLOCNPF = positif(DEFLOCNPF) ;

LIGLOCNSEUL = positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) ;

LIGLOCSEUL = 1 - positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) ;

regle 901110:
application : iliad  ;

ABICHQF = max(0 , BICHQF) ;

LIG_BICNPF = LIGBICNP * (1 - LIGMIBNPPOS) * LIG0 * LIG2 ;

LIGBICNPFQ = positif(present(CODCNC) + present(CODCNI) + present(CODCOC) + present(CODCOI) + present(CODCPC) + present(CODCPI)) * LIG0 * LIG2 ;

regle 901120:
application : iliad  ;

BNCNF = positif(present(BNHREV) + present(COD5XK) + present(BNCREV) + present(COD5XJ) + present(CODCQC) + present(CODCQI) + present(BNHDEV) + present(BNCDEV)
                    + present(BNHREC) + present(COD5YK) + present(BNCREC) + present(COD5YJ) + present(CODCRC) + present(CODCRI) + present(BNHDEC) + present(BNCDEC)
		    + present(BNHREP) + present(COD5ZK) + present(BNCREP) + present(COD5ZJ) + present(CODCSC) + present(CODCSI) + present(BNHDEP) + present(BNCDEP)) ;

LIGBNCNF = 1 - BNCNF ;

LIGNOCEP = (present(NOCEPV) + present(NOCEPC) + present(NOCEPP)) * LIG0 * LIG2 ;

NOCEPIMPN = max(0 , NOCEPIMP) ;

regle 901121:
application : iliad  ;

LIGNOCEPIMP = positif(present(BNCAABV) + present(COD5XS) + present(BNCAADV) + present(ANOCEP) + present(COD5XX) + present(DNOCEP)
                      + present(BNCAABC) + present(COD5YS) + present(BNCAADC) + present(ANOVEP) + present(COD5YX) + present(DNOCEPC)
		      + present(BNCAABP) + present(COD5ZS) + present(BNCAADP) + present(ANOPEP) + present(COD5ZX) + present(DNOCEPP)
		      + present(BNCNPPVV) + present(BNCNPPVC) + present(BNCNPPVP) + (present(BNCNPDCT) + present(COD5LD) + present(COD5MD)) * LIGNOCEP)
              * (1 - (null(BNCNPHQCV) * null(BNCNPHQCC) * null(BNCNPHQCP))) * (1 - LIGSPENPPOS) * LIG0 * LIG2 ;

LIGNOCEPIMPQ = positif(present(CODCJG) + present(CODCSN) + present(CODCRF) + present(CODCNS) + present(CODCSF) + present(CODCOS)) * LIG0 * LIG2 ;

regle 901122:
application : iliad  ;

LIGDAB = positif(present(DABNCNP6) + present(DABNCNP5) + present(DABNCNP4)
		 + present(DABNCNP3) + present(DABNCNP2) + present(DABNCNP1)) 
		* LIG0 * LIG2 ;

LIGDIDAB = positif_ou_nul(DIDABNCNP) * positif(LIGDAB) * LIG0 * LIG2 ;

LIGDEFBNCNPF = positif(DEFBNCNPF) ;
LIGDEFBANIF  = positif (DEFBANIF) ;
LIGDEFBICNPF = positif (DEFBICNPF) ;
LIGDEFRFNONI = positif (DEFRFNONI) ;

regle 901130:
application :  iliad ;

LIG910 = positif(present(RCMABD) + present(RCMTNC) + present(RCMAV) + present(RCMHAD) + present(RCMHAB) 
                 + present(REGPRIV) + present(COD2TT)+ present(COD2VV)+present(COD2WW)+present(COD2YY)+present(COD2ZZ) + ((1 - present(BRCMQ)) * present(RCMFR))) * LIG0 * LIG2 ;

regle 901140: 
application : iliad  ;

LIG1130 = positif(present(REPSOF)) * LIG0 * LIG2 ;

regle 901150:
application : iliad  ;

LIG1950 = INDREV1A8 *  positif_ou_nul(REVKIRE) 
                    * (1 - positif_ou_nul(IND_TDR)) 
                    * (1 - positif(ANNUL2042 + 0)) ;

regle 901160:
application :  iliad ;

LIG29 = positif(present(RFORDI) + present(RFDHIS) + present(RFDANT) +
                present(RFDORD)) 
                * (1 - positif(LIG30)) * LIG1 * LIG2 * IND_REV ;

regle 901170:
application : iliad  ;

LIG30 = positif(RFMIC) * LIG1 * LIG2 ;
LIGREVRF = positif(present(FONCI) + present(REAMOR)) * LIG1 * LIG2 ;

regle 901180:
application :  iliad ;

LIG49 =  INDREV1A8 * positif_ou_nul(DRBG) * LIG2 ;

regle 901190:
application : iliad  ;

LIG52 = positif(present(CHENF1) + present(CHENF2) + present(CHENF3) + present(CHENF4) 
                 + present(NCHENF1) + present(NCHENF2) + present(NCHENF3) + present(NCHENF4)) 
	     * LIG1 * LIG2 ;

regle 901200:
application : iliad  ;

LIG58 = (present(PAAV) + present(PAAP)) * positif(LIG52) * LIG1 * LIG2 ;

regle 901210:
application : iliad  ;

LIG585 = (present(PAAP) + present(PAAV)) * (1 - positif(LIG58)) * LIG1 * LIG2 ;
LIG65 = positif(LIG52 + LIG58 + LIG585 
                + present(CHRFAC) + present(CHNFAC) + present(CHRDED)
		+ present(DPERPV) + present(DPERPC) + present(DPERPP)
                + LIGREPAR)  
       * LIG1 * LIG2 ;

regle 901220:
application : iliad  ;

LIGDPREC = present(CHRFAC) * (1 - positif(ANNUL2042)) * LIG1 ;

LIGDFACC = (positif(20-V_NOTRAIT+0) * positif(DFACC)
           + (1 - positif(20-V_NOTRAIT+0)) * present(DFACC)) * (1 - positif(ANNUL2042)) * LIG1 ;

regle 901230:
application :  iliad ;

LIG1390 = positif(positif(ABMAR) + (1 - positif(RI1)) * positif(V_0DN)) * LIG1 * LIG2 ;

regle 901240:
application :  iliad ;

LIG68 = INDREV1A8 * (1 - positif(abs(RNIDF))) * LIG2 ;

regle 901250:
application : iliad  ;

LIGTTPVQ = positif(
                   positif(CARTSV) + positif(CARTSC) + positif(CARTSP1) + positif(CARTSP2)+ positif(CARTSP3)+ positif(CARTSP4)
                   + positif(REMPLAV) + positif(REMPLAC) + positif(REMPLAP1) + positif(REMPLAP2)+ positif(REMPLAP3)+ positif(REMPLAP4)
                   + positif(PEBFV) + positif(PEBFC) + positif(PEBF1) + positif(PEBF2)+ positif(PEBF3)+ positif(PEBF4)
                   + positif(CARPEV) + positif(CARPEC) + positif(CARPEP1) + positif(CARPEP2)+ positif(CARPEP3)+ positif(CARPEP4)
                   + positif(CODRAZ) + positif(CODRBZ) + positif(CODRCZ) + positif(CODRDZ) + positif(CODREZ) + positif(CODRFZ) 
                   + positif(PENSALV) + positif(PENSALC) + positif(PENSALP1) + positif(PENSALP2)+ positif(PENSALP3)+ positif(PENSALP4)
                   + positif(RENTAX) + positif(RENTAX5) + positif(RENTAX6) + positif(RENTAX7)
                   + positif(REVACT) + positif(REVPEA) + positif(PROVIE) + positif(DISQUO) + positif(RESTUC) + positif(INTERE)
                   + positif(FONCI) + positif(REAMOR)
                   + positif(4BACREV) + positif(4BACREC) + positif(4BACREP) + positif(4BAHREV) + positif(4BAHREC) + positif(4BAHREP)
                   + positif(CODDAJ) + positif(CODEAJ) + positif(CODDBJ)+ positif(CODEBJ) + positif(CODRVG)
		   + positif(CODRAF) + positif(CODRAG) + positif(CODRBF) + positif(CODRBG) + positif(CODRCF) + positif(CODRCG)
		   + positif(CODRDF) + positif(CODRDG) + positif(CODREF) + positif(CODREG) + positif(CODRFF) + positif(CODRFG)
		   + positif(CODRAL) + positif(CODRAM) + positif(CODRBL) + positif(CODRBM) + positif(CODRCL) + positif(CODRCM)
		   + positif(CODRDL) + positif(CODRDM) + positif(CODREL) + positif(CODREM) + positif(CODRFL) + positif(CODRFM)
		   + positif(CODRAR) + positif(CODRBR) + positif(CODRCR) + positif(CODRDR)
		   + positif(CODCJG) + positif(CODCKC) + positif(CODCKI) + positif(CODCLC) + positif(CODCLI) + positif(CODCMC)
		   + positif(CODCMI) + positif(CODCNC) + positif(CODCNI) + positif(CODCNS) + positif(CODCOC) + positif(CODCOI)
		   + positif(CODCOS) + positif(CODCPC) + positif(CODCPI) + positif(CODCQC) + positif(CODCQI) + positif(CODCRC)
		   + positif(CODCRF) + positif(CODCRI) + positif(CODCSC) + positif(CODCSF) + positif(CODCSI) + positif(CODCSN)
                  ) * LIG1 * LIG2 ;

regle 901260:
application :  iliad ;

LIG1430 = positif(BPTP3) * LIG0 * LIG2 ;

LIG1431 = positif(BPTP18) * LIG0 * LIG2 ;

LIG1432 = positif(BPT19) * LIG0 * LIG2 ;

regle 901270:
application :  iliad ;

LIG815 = V_EAD * positif(BPTPD) * LIG0 * LIG2 ;
LIG816 = V_EAG * positif(BPTPG) * LIG0 * LIG2 ;
LIGTXF225 = positif(PEA+0) * LIG0 * LIG2 ;
LIGTXF24 = positif(BPT24) * LIG0 * LIG2 ;
LIGTXF30 = positif_ou_nul(BPCOPTV + BPVSK) * LIG0  * LIG2 ;
LIGTXF40 = positif(BPV40V + 0) * LIG0 * LIG2 ;




regle 901290:
application :  iliad ;
 
LIG81 = positif(present(RDDOUP) + present(DONAUTRE) + present(REPDON03) + present(REPDON04) 
                + present(REPDON05) + present(REPDON06) + present(REPDON07) + present(COD7UH)
                + positif(EXCEDANTA))
        * LIG1 * LIG2 ;

LIGCRDIE = positif(REGCI) * LIG1 * LIG2 ;

regle 901300:
application : iliad  ;

LIG1500 = positif((positif(IPMOND) * positif(present(IPTEFP)+positif(VARIPTEFP)*present(DEFZU))) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	      * (1 - positif(DEFRIMOND)) * CNRLIG12 ;

LIG1510 = positif((positif(IPMOND) * present(IPTEFN)) + positif(INDTEFF) * (1 - positif(TEFFREVTOT))) 
	      * (1 - positif(DEFRIMOND)) * CNRLIG12 ;

LIG1500YT = positif((positif(IPMOND) * positif(present(IPTEFP)+positif(VARIPTEFP)*present(DEFZU))) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	     * positif(positif(max(0,IPTEFP+DEFZU-IPTEFN))+positif(max(0,RMOND+DEFZU-DMOND))) * positif(DEFRIMOND) * CNRLIG12 ;

LIG1510YT =  positif(null(max(0,RMOND+DEFZU-DMOND))+null(max(0,IPTEFP+DEFZU-IPTEFN))) * positif(DEFRIMOND) * CNRLIG12 ;

regle 901310:
application : iliad  ;

LIG1522 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * (1 - INDTXMOY) * V_CNR * LIG2 ;

regle 901320:
application :  iliad ;

LIG1523 = (1 - present(IND_TDR)) * LIG2 ;

regle 901330:
application : iliad  ;

LIG75 = (1 - INDTXMIN) * (1 - INDTXMOY) * (1 - (LIG1500+ LIG1500YT)) * (1 - (LIG1510+ LIG1510YT)) * INDREV1A8 * LIG2 ;

LIG1545 = (1 - present(IND_TDR)) * INDTXMIN * positif(IND_REV) * LIG2 ;

LIG1760 = (1 - present(IND_TDR)) * INDTXMOY * LIG2 ;

LIG1546 = positif(PRODOM + PROGUY) * (1 - positif(V_EAD + V_EAG)) * LIG2 ;

LIG1550 = (1 - present(IND_TDR)) * INDTXMOY * LIG2 ;

LIG74 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * positif(LIG1500 + LIG1510 + LIG1500YT + LIG1510YT) * LIG2 ;

LIGBAMARG = positif(BATMARGTOT) * (1 - present(IND_TDR)) * LIG138 * LIG2 ;

regle 901340:
application :  iliad ;

LIG80 = positif(present(RDREP) + present(DONETRAN)) * LIG1 * LIG2 ;

regle 901350:
application : iliad  ;

LIGRSOCREPR = positif(present(RSOCREPRISE)) * LIG1 * LIG2 ;

regle 901360:
application :  iliad ;

LIG1740 = positif(RECOMP) * LIG2 ;

regle 901370:
application :  iliad ;

LIG1780 = positif(RDCOM + NBACT) * LIG1 * LIG2 ;

regle 901380:
application :  iliad ;

LIG98B = positif(LIG80 + LIGFIPC + LIGFIPDOM 
                 + LIGDUFTOT + LIGPINTOT + LIG7CY + LIG7DY + LIG7EY + LIG7FY
                 + LIGREDAGRI + LIGFORET + LIGRESTIMO  
	         + LIGCINE + LIGPRESSE + LIGRSOCREPR + LIGCOTFOR 
	         + present(PRESCOMP2000) + present(RDPRESREPORT) + present(FCPI) 
		 + present(DSOUFIP) + LIGRIRENOV + present(DFOREST) 
		 + present(DHEBE) + present(DSURV)
	         + LIGLOGDOM + LIGREPTOUR + LIGREPHA + LIGREHAB
		 + LIG1780 + LIG2040 + LIG81 + LIGCRDIE
                 + LIGLOGSOC + LIGDOMSOC1 
                 + somme (i=E,M,N,G,D,S,T,H,F,Z,X,I,J : LIGCELLi) + LIGCELMG + LIGCELMH
                 + somme (i=S,R,U,T,Z,X,W,V,F,D,A : LIGCELHi)
                 + somme (i=U,S,L,J : LIGCELGi )
                 + somme (i=H,L,F,K,D,J,B,P,S,O,R,N,Q,M : LIGCELYi)
		 + LIGCELSOM1 + LIGCELSOM2 + LIGCELSOM3 + LIGCELSOM4
		 + LIGCELSOM5 + LIGCELSOM6 + LIGCELSOM7
                 + LIGILMNP1 + LIGILMNP2 + LIGILMNP3 + LIGILMNP4
                 + LIGILMPA + LIGILMPB + LIGILMPC + LIGILMPD + LIGILMPE
                 + LIGILMPF + LIGILMPK + LIGILMPG + LIGILMPL + LIGILMPH 
		 + LIGILMPM + LIGILMPI + LIGILMPN + LIGILMPJ + LIGILMPO
                 + LIGILMOA + LIGILMOB + LIGILMOC + LIGILMOD + LIGILMOE
                 + LIGILMOJ + LIGILMOI + LIGILMOH + LIGILMOG + LIGILMOF
		 + LIGILMOO + LIGILMON + LIGILMOM + LIGILMOL + LIGILMOK
		 + LIGILMIX + LIGILMIY + LIGINVRED
                 + LIGILMIH  + LIGILMJC + LIGILMIZ + LIGILMJI + LIGILMJS
                 + LIGMEUBLE + LIGPROREP + LIGREPNPRO + LIGMEUREP + LIGILMIC
                 + LIGILMIB + LIGILMIA + LIGILMJY + LIGILMJX + LIGILMJW
                 + LIGILMJV + LIGRESINEUV + LIGRESIVIEU + LIGLOCIDEFG
		 + LIGCODJTJU + LIGCODOU + LIGCODOV + LIGCODOW
		 + present(DNOUV) + LIGLOCENT + LIGCOLENT + LIGRIDOMPRO
		 + LIGPATNAT2 + LIGPATNAT3 + LIGPATNAT4) 
           * LIG1 * LIG2 ;

LIGRED = LIG98B * (1 - positif(RIDEFRI)) * LIG1 * LIG2 ;

LIGREDYT = LIG98B * positif(RIDEFRI) * LIG1 * LIG2 ;

regle 901390:
application :  iliad ;

LIG1820 = positif(ABADO + ABAGU + RECOMP) * LIG2 ;

LIGIDRS = positif(IDRS4) * positif(LIGRED + LIGREDYT) ;

regle 901400:
application : iliad  ;

LIG106 = positif(RETIR) ;
LIGINRTAX = positif(RETTAXA) ;
LIG10622 = positif(RETIR22) ;
LIGINRTAX22 = positif(RETTAXA22) ;
ZIG_INT22 = positif(RETCS22 + RETPS22 + RETRD22) ;

LIGINRPCAP = positif(RETPCAP) ;
LIGINRPCAP2 = positif(RETPCAP22) ;
LIGINRLOY = positif(RETLOY) ;
LIGINRLOY2 = positif(RETLOY22) ;

LIGINRHAUT = positif(RETHAUTREV) ;
LIGINRHAUT2 = positif(RETCHR22) ;

regle 901410:
application : iliad  ;

LIG_172810 = TYPE2 * positif(NMAJ1) ;

LIGTAXA17281 = TYPE2 * positif(NMAJTAXA1) ;

LIGPCAP17281 = TYPE2 * positif(NMAJPCAP1) ;

LIGCHR17281 = TYPE2 * positif(NMAJCHR1) ;

LIG_NMAJ1 = TYPE2 * positif(NMAJ1) ;
LIG_NMAJ3 = TYPE2 * positif(NMAJ3) ;
LIG_NMAJ4 = TYPE2 * positif(NMAJ4) ;

LIGNMAJTAXA1 = TYPE2 * positif(NMAJTAXA1) ;
LIGNMAJTAXA3 = TYPE2 * positif(NMAJTAXA3) ;
LIGNMAJTAXA4 = TYPE2 * positif(NMAJTAXA4) ;

LIGNMAJPCAP1 = TYPE2 * positif(NMAJPCAP1) ;
LIGNMAJPCAP3 = TYPE2 * positif(NMAJPCAP3) ;
LIGNMAJPCAP4 = TYPE2 * positif(NMAJPCAP4) ;
LIGNMAJLOY1 = TYPE2 * positif(NMAJLOY1) ;
LIGNMAJLOY4 = TYPE2 * positif(NMAJLOY4) ;

LIGNMAJCHR1 = TYPE2 * positif(NMAJCHR1) ;
LIGNMAJCHR3 = TYPE2 * positif(NMAJCHR3) ;
LIGNMAJCHR4 = TYPE2 * positif(NMAJCHR4) ;

regle 901420:
application :  iliad ;

LIG109 = positif(IPSOUR + IPAE + LIGPVETR + LIGCI8XV + LIGCIGLO + LIGCICAP + LIGREGCI + LIGCULTURE + LIGMECENAT 
		  + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA + LIGCIGE + LIGDEVDUR 
                  + LIGCITEC + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN
		  + LIGCREFAM + LIGCREAPP + LIGCREBIO + LIGCREPROSP + LIGPRESINT + LIGCREFORM + LIGINTER
		  + LIGCONGA + LIGMETART + LIGRESTAU + LIGVERSLIB) 
               * LIG1 * LIG2 ;

LIGCRED1 = positif(LIGPVETR + LIGCICAP + LIGREGCI + LIGCI8XV + LIGCIGLO + 0) 
	    * (1 - positif(IPSOUR + IPAE + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA + LIGCIGE + LIGDEVDUR 
		           + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		           + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGRESTAU + LIGCONGA + LIGMETART
		           + LIGCREFORM + LIGVERSLIB + LIGCITEC + 0))
	    ;

LIGCRED2 = (1 - positif(LIGPVETR + LIGCICAP + LIGREGCI + LIGCI8XV + LIGCIGLO + 0)) 
	    * positif(IPSOUR + IPAE + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA + LIGCIGE + LIGDEVDUR 
		      + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		      + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGRESTAU + LIGCONGA + LIGMETART
		      + LIGCREFORM + LIGVERSLIB + LIGCITEC + 0)
	    ;

LIGCRED3 = positif(LIGPVETR + LIGCICAP + LIGREGCI + LIGCI8XV + LIGCIGLO + 0) 
	    * positif(IPSOUR + IPAE + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA + LIGCIGE + LIGDEVDUR
		      + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		      + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGRESTAU + LIGCONGA + LIGMETART
		      + LIGCREFORM + LIGVERSLIB + LIGCITEC + 0)
           ;

regle 901430:
application :  iliad ;

LIGPVETR = positif(present(CIIMPPRO) + present(CIIMPPRO2)) * LIG1 * LIG2 ;
LIGCICAP = present(PRELIBXT) * LIG1 * LIG2 ;
LIGREGCI = positif(present(REGCI) + present(COD8XY)) * positif(CICHR) * LIG1 * LIG2 ;
LIGCI8XV = present(COD8XV) * LIG1 * LIG2 ;
LIGCIGLO = positif(present(COD8XF) + present(COD8XG) + present(COD8XH)) * LIG1 * LIG2 ;

LIGCULTURE = present(CIAQCUL) * LIG1 * LIG2 ;
LIGMECENAT = present(RDMECENAT) * LIG1 * LIG2 ;
LIGCORSE = positif(present(CIINVCORSE) + present(IPREPCORSE) + present(CICORSENOW)) * LIG1 * LIG2 ;
LIG2305 = positif(DIAVF2) * LIG1 * LIG2 ;
LIGEMPLOI = positif(COD8UW + COD8TL) * LIG1 * LIG2 ;
LIGCI2CK = positif(COD2CK) * LIG1 * LIG2 ;
LIGBPLIB = present(RCMLIB) * LIG0 * LIG2 ;
LIGCIHJA = positif(CODHJA) * LIG1 * LIG2 ;
LIGCIGE = positif(RDTECH + RDEQPAHA) * LIG1 * LIG2 ;
LIGDEVDUR = positif(DDEVDUR) * LIG1 * LIG2 ;
LIGCICA = positif(BAILOC98) * LIG1 * LIG2 ;
LIGCIGARD = positif(DGARD) * LIG1 * LIG2 ;
LIG82 = positif(present(RDSYVO) + present(RDSYCJ) + present(RDSYPP) ) * LIG1 * LIG2 ;
LIGPRETUD = positif(PRETUD+PRETUDANT) * LIG1 * LIG2 ;
LIGSALDOM = present(CREAIDE) * LIG1 * LIG2 ;
LIGCIFORET = positif(BDCIFORET) * LIG1 * LIG2 ;
LIGHABPRIN = positif(present(PREHABT) + present(PREHABT1) + present(PREHABT2) + present(PREHABTN) 
                     + present(PREHABTN1) + present(PREHABTN2) + present(PREHABTVT)
                    ) * LIG1 * LIG2 ;
LIGCREFAM = positif(CREFAM) * LIG1 * LIG2 ;
LIGCREAPP = positif(CREAPP) * LIG1 * LIG2 ;
LIGCREBIO = positif(CREAGRIBIO) * LIG1 * LIG2 ;
LIGPRESINT = positif(PRESINTER) * LIG1 * LIG2 ;
LIGCREPROSP = positif(CREPROSP) * LIG1 * LIG2 ;
LIGINTER = positif(CREINTERESSE) * LIG1 * LIG2 ;
LIGRESTAU = positif(CRERESTAU) * LIG1 * LIG2 ;
LIGCONGA = positif(CRECONGAGRI) * LIG1 * LIG2 ;
LIGMETART = positif(CREARTS) * LIG1 * LIG2 ;
LIGCREFORM = positif(CREFORMCHENT) * LIG1 * LIG2 ;
LIGVERSLIB = positif(AUTOVERSLIB) * LIG1 * LIG2 ;
LIGCITEC = positif(DTEC) * LIG1 * LIG2 ;

regle 901440:
application :  iliad ;

LIGNRBASE = positif(present(NRINET) + present(NRBASE)) * LIG1 * LIG2 ;
LIGBASRET = positif(present(IMPRET) + present(BASRET)) * LIG1 * LIG2 ;

regle 901450:
application : iliad  ;

LIGAVFISC = positif(AVFISCOPTER) * LIG1 * LIG2 ; 

regle 901460:
application :  iliad ;

LIG2040 = positif(DNBE + RNBE + RRETU) * LIG1 * LIG2 ;

regle 901470:
application : iliad  ;

LIGRDCSG = positif(positif(V_BTCSGDED) + present(DCSG) + present(RCMSOC)) * LIG1 * LIG2 ;

regle 901480:
application :  iliad ;

LIGTAXANET = positif((present(CESSASSV) + present(CESSASSC)) * INDREV1A8IR + TAXANTAFF) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;

LIGPCAPNET = positif((present(PCAPTAXV) + present(PCAPTAXC)) * INDREV1A8IR + PCAPANTAFF) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;

LIGLOYNET = (present(LOYELEV) * INDREV1A8IR + TAXLOYANTAFF) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;

LIGHAUTNET = positif(BHAUTREV * INDREV1A8IR + HAUTREVANTAF) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;

LIG_IRNET = positif(LIGTAXANET + LIGPCAPNET + LIGLOYNET + LIGHAUTNET) * (1 - positif(ANNUL2042 + 0)) ;

LIGIRNET = positif(IRNET * LIG_IRNET + LIGTAXANET + LIGPCAPNET + LIGLOYNET + LIGHAUTNET) * (1 - positif(ANNUL2042 + 0)) ;

regle 901490:
application :  iliad ;

LIGANNUL = positif(ANNUL2042) ;

regle 901500:
application :  iliad ;

LIG2053 = positif(V_NOTRAIT - 20) * positif(IDEGR) * positif(IREST - SEUIL_8) * TYPE2 ;

regle 901510:
application :  iliad ;


LIG2051 = (1 - positif(20 - V_NOTRAIT)) 

          * positif (RECUMBIS) ;

LIGBLOC = positif(V_NOTRAIT - 20) ;

LIGSUP = positif(null(V_NOTRAIT - 26) + null(V_NOTRAIT - 36) + null(V_NOTRAIT - 46) + null(V_NOTRAIT - 56) + null(V_NOTRAIT - 66)) ;

LIGDEG = positif_ou_nul(TOTIRPSANT) * positif(SEUIL_8 - RECUM) 
         * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGRES = (1 - positif(TOTIRPSANT + 0)) * positif_ou_nul(RECUM - SEUIL_8)
         * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGDEGRES = positif(TOTIRPSANT + 0) * positif_ou_nul(RECUM - SEUIL_8) 
            * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGNEMP = positif((1 - null(NAPTEMP)) + null(NAPTEMP) * null(NAPTIR) * null(NAPCRP)) * positif(V_NOTRAIT - 20) ;

LIGEMP = (1 - LIGNEMP) * positif(V_NOTRAIT - 20) ;

LIG2052 = (1 - positif(V_ANTREIR + 0)) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGTAXANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_TAXANT + LIGTAXANET * positif(TAXANET))
            * (1 - positif(LIG2051)) * TYPE2 * (1 - APPLI_OCEANS) ;

LIGPCAPANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_PCAPANT + LIGPCAPNET * positif(PCAPNET))
             * (1 - positif(LIG2051)) * TYPE2 * (1 - APPLI_OCEANS) ;

LIGLOYANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_TAXLOYANT + LIGLOYNET * positif(TAXLOYNET))
             * (1 - positif(LIG2051)) * TYPE2 * (1 - APPLI_OCEANS) ;

LIGHAUTANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_CHRANT + LIGHAUTNET * positif(HAUTREVNET))
             * (1 - positif(LIG2051)) * TYPE2 * (1 - APPLI_OCEANS) ;

LIGANTREIR = positif(V_ANTREIR + 0) * (1 - positif(V_ANTCR)) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGNANTREIR = positif(V_ANTREIR + 0) * positif(V_ANTCR + 0) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGNONREC = positif(V_NONMERANT + 0) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGNONREST = positif(V_NONRESTANT + 0) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGIINET = LIGSUP * (positif(NAPT + 0) + null(IINETCALC)) * positif(V_NOTRAIT - 20) ;

LIGIINETC = LIGSUP * null(NAPT) * positif(IINETCALC + 0) * positif(V_NOTRAIT - 20) ;

LIGIDEGR = positif(LIGDEG + LIGDEGRES) * (positif_ou_nul(IDEGR - SEUIL_8) + null(IDEGR)) * positif(V_NOTRAIT - 20) ;

LIGIDEGRC = positif(LIGDEG + LIGDEGRES) * positif(SEUIL_8 - IDEGR) * positif(IDEGR + 0) * positif(V_NOTRAIT - 20) ;

LIGIREST = positif(LIGRES + LIGDEGRES) * (positif_ou_nul(IREST - SEUIL_8) + null(IREST)) * positif(V_NOTRAIT - 20) ;

LIGIRESTC = positif(LIGRES + LIGDEGRES) * positif(SEUIL_8 - IREST) * positif(IREST + 0) * positif(V_NOTRAIT - 20) ;

LIGNMRR = LIGIINETC * positif(V_ANTRE - V_NONRESTANT + 0) * positif(V_NOTRAIT - 20) ;

LIGNMRS = LIGIINETC * (1 - positif(V_ANTRE - V_NONRESTANT)) * positif(V_NOTRAIT - 20) ;

LIGRESINF = positif(LIGIDEGRC + LIGIRESTC) * positif(V_NOTRAIT - 20) ;

regle 901520:
application :  iliad ;


LIG2080 = positif(NATIMP - 71) * LIG2 ;

regle 901530:
application :  iliad ;


LIGTAXADEG = positif(NATIMP - 71) * positif(TAXADEG) * LIG2 ;

LIGPCAPDEG = positif(NATIMP - 71) * positif(PCAPDEG) * LIG2 ;

LIGLOYDEG = positif(NATIMP - 71) * positif(TAXLOYDEG) * LIG2 ;

LIGHAUTDEG = positif(NATIMP - 71) * positif(HAUTREVDEG) * LIG2 ;

regle 901540:
application : iliad  ;

INDCTX = si (  (V_NOTRAIT+0 = 23)  
            ou (V_NOTRAIT+0 = 33)   
            ou (V_NOTRAIT+0 = 43)   
            ou (V_NOTRAIT+0 = 53)   
            ou (V_NOTRAIT+0 = 63)  
            )
         alors (1)
         sinon (0)
         finsi;

INDIS = si (  (V_NOTRAIT+0 = 14)
            ou (V_NOTRAIT+0 = 16)
	    ou (V_NOTRAIT+0 = 26)
	    ou (V_NOTRAIT+0 = 36)
	    ou (V_NOTRAIT+0 = 46)
	    ou (V_NOTRAIT+0 = 56)
	    ou (V_NOTRAIT+0 = 66)
           )
        alors (1)
        sinon (0)
	finsi;


LIG2140 = si (
                ( ( (V_CNR + 0 = 0) et NATIMP = 1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		    ou ((V_CNR + 0 = 1) et (NATIMP = 1 ou  NATIMP = 0))
                    ou ((V_REGCO + 0 = 3) et ((NRINET > 0) et (NRINET < 12) et (CSTOTSSPENA < 61)))
                ) 
		et LIG2141 + 0 = 0
		)
          alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(20 - V_NOTRAIT))
          finsi;

LIG21401 = si (( ((V_CNR+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CNR+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(20 - V_NOTRAIT))
           finsi ;

LIG21402 = si (( ((V_CNR+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CNR+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(V_NOTRAIT - 20))
           finsi ;


regle 901550:
application :  iliad ;

LIG2141 = null(IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - IRANT) 
                  * positif(IRANT)
                  * (1 - positif(LIG2501))
		  * null(V_IND_TRAIT - 4)
		  * (1 - positif(NRINET + 0)) ;

regle 901560:
application :  iliad ;

LIGNETAREC = positif (IINET) * (1 - LIGPS) * positif(ANNUL2042) * TYPE2 ;

LIGNETARECS = positif (IINET) * LIGPS * positif(ANNUL2042) * TYPE2 ;

regle 901570:
application : iliad  ;

LIG2150 = (1 - INDCTX) 
	 * positif(IREST)
         * (1 - positif(LIG2140))
         * (1 - positif(IND_REST50))
	 * positif(20 - V_NOTRAIT)
         * LIG2 ;

regle 901580:
application :  iliad ;

LIG2161 =  INDCTX 
	  * positif(IREST) 
          * positif_ou_nul(IREST - SEUIL_8) 
	  * (1 - positif(IND_REST50)) ;

LIG2368 = INDCTX 
	 * positif(IREST)
         * positif ( positif(IND_REST50)
                     + positif(IDEGR) ) ;

regle 901590:
application :  iliad ;

LIG2171 = (1 - INDCTX) 
	 * positif(IREST)
	 * (1 - positif(LIG2140))
         * positif(IND_REST50)  
	 * positif(20 - V_NOTRAIT)
	 * LIG2 ;

regle 901600:
application :  iliad ;

LIGTROP = positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_12))
               * null(IDRS2 - IDEC + IREP)
	       * (1 - LIGPS)
               * (1 - INDCTX);

LIGTROPS = positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_12))
               * null(IDRS2 - IDEC + IREP)
	       * LIGPS
               * (1 - INDCTX);

LIGTROPREST =  positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
               - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
               - IRANT - SEUIL_12))
		 * (1 - positif(LIGTROP))
	         * (1 - LIGPS)
                 * (1 - INDCTX);

LIGTROPRESTS =  positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
                - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
                - IRANT - SEUIL_12))
		 * (1 - positif(LIGTROP))
	         * LIGPS
                 * (1 - INDCTX);

regle 901610:
application :  iliad ;

LIGRESINF50 = positif(positif(IND_REST50) * positif(IREST) 
                      + positif(RECUM) * (1 - positif_ou_nul(RECUM - SEUIL_8)))  
	      * positif(SEUIL_8 - IRESTIT) * null(LIGRESINF) ;

regle 901620:
application :  iliad ;

LIG2200 = (positif(IDEGR) * positif_ou_nul(IDEGR - SEUIL_8) * (1 - LIGPS) * TYPE2) ;

LIG2200S = (positif(IDEGR) * positif_ou_nul(IDEGR - SEUIL_8) * LIGPS * TYPE2) ;

regle 901630:
application :  iliad ;

LIG2205 = positif(IDEGR) * (1 - positif_ou_nul(IDEGR - SEUIL_8)) * (1 - LIGPS) * LIG2 ;

LIG2205S = positif(IDEGR) * (1 - positif_ou_nul(IDEGR - SEUIL_8)) * LIGPS * LIG2 ;

regle 901640:
application :  iliad ;


IND_NIRED = si ((CODINI=3 ou CODINI=5 ou CODINI=13)
               et ((IAVIM +NAPCRPAVIM)- TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES) = 0 
                   et  V_CNR = 0)
          alors (1 - INDCTX) 
          finsi ;

regle 901650:
application :  iliad ;


IND_IRNMR = si (CODINI=8 et NATIMP=0 et V_CNR = 0)
          alors (1 - INDCTX)  
          finsi;

regle 901660:
application :  iliad ;

IND_IRINF80 = si ( ((CODINI+0=9 et NATIMP+0=0) ou (CODINI +0= 99))
                  et V_CNR=0 
                  et  (IRNET +TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES + NAPCR < SEUIL_12)
                  et  ((IAVIM+NAPCRPAVIM) >= SEUIL_61))
              alors ((1 - positif(INDCTX)) * (1 - positif(IREST))) 
              finsi;

regle 901670:
application :  iliad ;


LIGNIIR = null(IDRS3 - IDEC)
           * null(NRINET+0)
           * null(NATIMP)
           * null(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES + NAPCRP)
           * (1 - positif(IREP))
           * (1 - positif(IPROP))
           * (1 - positif(IRESTIT))
           * (1 - positif(IDEGR))
           * (1 - positif(LIGAUCUN))
           * (1 - positif(LIG2141))
           * (1 - positif(LIG2501))
           * (1 - positif(LIG8FV))
           * (1 - positif(LIGNIDB))
           * (1 - V_CNR)
           * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * (1 - positif(NRINET))
	   * positif(20 - V_NOTRAIT)
           * LIG2 ;

LIGNIIRDEG = null(IDRS3 - IDEC)
	       * null(IAMD2)
	       * (1 - positif(IRE))
               * null(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES + NAPCRP)
               * (1 - V_CNR)
               * (1 - positif(LIG2501))
	       * (1 - positif(LIGTROP))
	       * (1 - positif(LIGTROPREST))
	       * (1 - positif(IMPRET - SEUIL_12))
	       * (1 - positif(NRINET - SEUIL_12))
	       * positif(1 + null(3 - INDIRPS))
	       * positif(V_NOTRAIT - 20)
               * LIG2 ;

regle 901680:
application :  iliad ;


LIGCBAIL = null(IDOM11 - DEC11 - RMENAGE)
            * (1 - positif(IAMD2))
	    * positif_ou_nul(TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES + NAPCRP - SEUIL_61)
	    * (positif_ou_nul(NAPTIR - SEUIL_12) + positif_ou_nul(NAPCRP - SEUIL_61))
	    * positif_ou_nul(NAPTIR + NAPCRP - SEUIL_12)
	    * (1 - positif(LIGNIDB))
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * (1 - positif(IMPRET))
	    * (1 - positif(NRINET))
            * (1 - V_CNR)
            * LIG2 ;

LIGNITSUP = positif_ou_nul(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - SEUIL_61)
             * null(IDRS2-IDEC+IREP)
             * positif_ou_nul(TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET - SEUIL_12)
	     * (1 - positif(LIG0TSUP))
             * (1 - V_CNR)
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
	     * positif(V_NOTRAIT - 20)
	     * (1 - positif(INDCTX))
             * LIG2 ;
                       
LIGNITDEG = positif(TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET)
             * positif_ou_nul(IRB2 - SEUIL_61)
             * positif_ou_nul(TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET - SEUIL_12)
             * null(INDNIRI) * (1 - positif(IAMD2))
             * positif(1 - V_CNR) * INDREV1A8
             * (1 - V_CNR)
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
	     * (1 - positif(IMPRET))
	     * positif(INDCTX)
             * LIG2 ;
                       
regle 901690:
application :  iliad ;

LIGNIDB = null(IDOM11 - DEC11-RMENAGE)
           * positif(SEUIL_61 - TAXASSUR - IPCAPTAXTOT - TAXLOY - CHRAPRES)
           * positif(SEUIL_61 - NAPCRP)
	   * positif(TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES + NAPCRP)
           * null(IRNETBIS)
	   * (1 - positif(IRESTIT))
           * (1 - positif(IREP))
           * (1 - positif(IPROP))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(NRINET))
	   * (1 - positif(IMPRET))
           * (1 - V_CNR)
           * LIG2 ;  

LIGREVSUP = INDREV1A8
	     * positif(REVFONC)
             * (1 - V_CNR)
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
	     * (1 - positif(IMPRET))
	     * positif(V_NOTRAIT - 20)
	     * (1 - positif(INDCTX))
             * LIG2 ;  

LIGREVDEG = INDREV1A8
	     * positif(REVFONC)
             * (1 - V_CNR)
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
	     * (1 - positif(IMPRET))
	     * positif(INDCTX)
             * LIG2 ;  

regle 901700:
application :  iliad ;

LIG0TSUP = INDNIRI
            * null(IRNETBIS)
            * positif_ou_nul(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - SEUIL_61)
            * (1 - positif(IREP))
            * (1 - positif(IPROP))
            * (1 - V_CNR)
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * positif(V_NOTRAIT - 20)
	    * (1 - positif(INDCTX))
            * LIG2 ;

LIG0TDEG = INDNIRI
            * null(IRNETBIS)
            * positif_ou_nul(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - SEUIL_61)
            * (1 - positif(IREP))
            * (1 - positif(IPROP))
            * (1 - V_CNR)
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * positif(INDCTX)
            * LIG2 ;

regle 901710:
application :  iliad ;


LIGPSNIR = positif(IAVIM) 
           * positif(SEUIL_61 - IAVIM) 
           * positif(SEUIL_61 - (NAPTIR + V_ANTREIR))
           * positif_ou_nul(NAPCRP - SEUIL_61)
           * (positif(IINET) * positif(20 - V_NOTRAIT) + positif(V_NOTRAIT - 20)) 
           * (1 - V_CNR)
           * (1 - positif(LIGNIDB))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * (1 - positif(NRINET))
           * LIG2 ;

LIGIRNPS = positif((positif_ou_nul(IAVIM - SEUIL_61) * positif_ou_nul(NAPTIR - SEUIL_12)) * positif(IAMD2)
                   + positif(IRESTIT + 0))
           * positif(SEUIL_61 - NAPCRP)
           * positif(NAPCRP)
           * (1 - V_CNR)
           * (1 - positif(LIGNIDB))
	   * (1 - positif(LIGTROP))
	   * (1 - positif(LIGTROPREST))
	   * (1 - positif(IMPRET))
	   * (1 - positif(NRINET))
           * LIG2 ;

LIG400DEG = positif(IAVIM + NAPCRPAVIM)
  	     * positif (SEUIL_61 - (IAVIM + NAPCRPAVIM))
	     * null(ITRED)
	     * positif (IRNET)
	     * (1 - positif(IRNET + V_ANTREIR - SEUIL_61))
             * (1 - V_CNR)
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
	     * (1 - positif(IMPRET - SEUIL_12))
	     * (1 - positif(NRINET - SEUIL_12))
	     * positif(V_NOTRAIT - 20)
             * LIG2 ;

regle 901720:
application :  iliad ;


LIG61DEG = positif(ITRED)
	    * positif(IAVIM)  
            * positif(SEUIL_61 - IAVIM)
            * (1 - positif(INDNMR2))
            * (1 - V_CNR)
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * (1 - positif(IMPRET))
            * positif(INDCTX)
            * LIG2 ;

regle 901730:
application :  iliad ;
	

LIGAUCUN = positif((positif(SEUIL_61 - IAVIM) * positif(IAVIM) * (1 - positif(NAPCRP))
                    + positif_ou_nul(IAVIM - SEUIL_61) * positif(NAPTIR) * positif(SEUIL_12 - NAPTIR) * (1 - positif(NAPCRP))
                    + positif(SEUIL_61 - NAPCRP) * positif(NAPCRP) * positif(SEUIL_61 - IAVIM)
                    + (positif_ou_nul(IAVIM - SEUIL_61) + positif_ou_nul(NAPCRP - SEUIL_61)) * positif(NAPCRP) * positif(SEUIL_12 - IRPSCUM))
	           * (1 - positif(IREST))
                   * (1 - LIGNIDB)
	           * (1 - positif(LIGTROP))
	           * (1 - positif(LIGTROPREST))
	           * (1 - positif(IMPRET))
	           * (1 - positif(NRINET))
                   * (1 - V_CNR)
	           * positif(20 - V_NOTRAIT) 
	           * LIG2
                  ) ;

regle 901740:
application :  iliad ;


LIG12ANT = positif (IRANT)
            * positif (SEUIL_12 - TOTNET )
	    * positif( TOTNET)
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * (1 - positif(V_CNR + (1 - V_CNR) * positif(NRINET-SEUIL_12)))
	    * (1 - positif(IMPRET - SEUIL_12)) 
	    * (1 - positif(NRINET - SEUIL_12))
	    * positif(20 - V_NOTRAIT)
            * LIG2 ; 

regle 901750:
application :  iliad ;

LIG12NMR = positif(IRPSCUM)
            * positif(SEUIL_12 - IRPSCUM)
	    * positif(V_NOTRAIT - 20)
            * (1 - V_CNR)
	    * (1 - positif(IMPRET - SEUIL_12)) 
	    * (1 - positif(NRINET - SEUIL_12)) ;

regle 901760:
application :  iliad ;


LIGNIIRAF = null(IAD11)
             * positif(IRESTIT)
             * (1 - positif(INDNIRI))
             * (1 - positif(IREP))
             * (1 - positif(IPROP))
             * (1 - positif_ou_nul(NAPTIR))
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
	     * positif(1 + null(3 - INDIRPS))
             * LIG2 ;

regle 901770:
application :  iliad ;


LIGNIDEG = null(IDRS3-IDEC)
	    * null(IAMD2)
	    * positif(SEUIL_61 - TAXASSUR)
	    * positif(SEUIL_61 - IPCAPTAXT)
	    * positif(SEUIL_61 - TAXLOY)
	    * positif(SEUIL_61 - CHRAPRES)
            * positif(SEUIL_12 - IRNET)
            * (1 - V_CNR)
	    * (1 - positif(LIGDEG61))
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * positif(INDCTX)
            * LIG2 ;

regle 901780:
application :  iliad ;

LIGDEG61 = positif (IRNETBIS)
            * positif (SEUIL_61 - IAMD1) 
            * positif (SEUIL_61 - NRINET) 
	    * positif (IAMD2)
	    * (1 - positif(LIG61DEG))
            * (1 - V_CNR)
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
	    * (1 - positif(IMPRET))
            * positif (INDCTX)
            * LIG2 ;

regle 901790:
application :  iliad ;

LIGDEG12 = positif (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET)
            * positif (SEUIL_12 - IRNET - TAXANET - PCAPNET - TAXLOYNET - HAUTREVNET)
            * (1 - V_CNR)
            * (1 - positif(LIGNIDEG))
            * (1 - positif(LIGDEG61))
            * (1 - positif(LIG61DEG))
	    * (1 - positif(LIGTROP))
	    * (1 - positif(LIGTROPREST))
            * (1 - positif(IMPRET))
	    * positif(INDCTX)
            * LIG2 ;

regle 901800:
application :  iliad ;

LIGDIPLOI = positif(INDREV1A8)
             * positif(null(NATIMP - 1) + positif(NAPTEMP))
             * positif(REVFONC) * (1 - V_CNR)
	     * (1 - positif(LIGTROP))
	     * (1 - positif(LIGTROPREST))
             * LIG1 ;

regle 901810:
application :  iliad ;

LIGDIPLONI = positif(INDREV1A8)
              * positif(null(NATIMP) + positif(IREST) + (1 - positif(NAPTEMP)))
              * positif(REVFONC) * (1 - V_CNR)
	      * (1 - positif(LIGTROP))
	      * (1 - positif(LIGTROPREST))
	      * (1 - LIGDIPLOI)
              * LIG1 ;

regle 901820:
application :  iliad ;

LIG2355 = positif (
		   IND_NI * (1 - positif(V_ANTRE)) + INDNMR1 + INDNMR2
                   + positif(NAT1BIS) *  null(NAPT) * (positif (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET))
		   + positif(SEUIL_12 - (IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - IRANT))
				 * positif_ou_nul(IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - IRANT) 
                  )
          * positif(INDREV1A8)
          * (1 - null(NATIMP - 1) + null(NATIMP - 1) * positif(IRANT))  
	  * (1 - LIGPS)
	  * LIG2 ;

regle 901830:
application :  iliad ;

LIG7CY = positif(COD7CY) * LIG1 * LIG2 ;
LIG7DY = positif(COD7DY) * LIG1 * LIG2 ;
LIG7EY = positif(COD7EY) * LIG1 * LIG2 ;
LIG7FY = positif(COD7FY) * LIG1 * LIG2 ;
LIGRVG = positif(CODRVG) * LIG1 * LIG2 ;

regle 901840:
application :  iliad ;

LIG2380 = si (NATIMP=0 ou NATIMP=21 ou NATIMP=70 ou NATIMP=91)
          alors (IND_SPR * positif_ou_nul(V_8ZT + CODZRE + CODZRF - RBG1) * positif(V_8ZT + CODZRE + CODZRF)
                * (1 - present(BRAS)) * (1 - present(IPSOUR))
                * V_CNR * LIG2)
          finsi ;

regle 901850:
application :  iliad ;

LIG2383 = si ((IAVIM+NAPCRPAVIM)<=(IPSOUR * LIG1 ))
          alors ( positif(RBG1 - V_8ZT + CODZRE + CODZRF) * present(IPSOUR) 
                * V_CNR * LIG2)
          finsi ;

regle 901860:
application : iliad  ;

LIG2501 = (1 - positif(IND_REV)) * (1 - V_CNR) * LIG2 ;

LIG25012 = (1 - positif(IND_REV)) * V_CNR * LIG2 ;

LIG8FV = positif(REVFONC) * (1 - IND_REV8FV) ;

regle 901870:
application :  iliad ;

LIG2503 = (1 - positif(IND_REV))
          * (1 - positif_ou_nul(IND_TDR))
          * LIG2
          * (1 - V_CNR) ;

regle 901890:
application :  iliad ;

LIG3700 = positif(LIG4271 + LIG3710 + LIG3720 + LIG3730) * LIG1 * TYPE4 ;

regle 901900:
application :  iliad ;

LIG4271 = positif(positif(V_0AB) * LIG1) * (1 - positif(ANNUL2042 + 0)) ;

LIG3710 = positif(20 - V_NOTRAIT) * positif(BOOL_0AZ) * LIG1 ;

regle 901910:
application :  iliad ;

LIG3720 = (1 - positif(20 - V_NOTRAIT)) * (1 - positif(LIG3730)) * LIG1 * LIG2 ;

regle 901920:
application :  iliad ;

LIG3730 = (1 - positif(20 - V_NOTRAIT))
          * positif(BOOL_0AZ)
          * LIG1 ;

regle 901930:
application :  iliad ;

LIG3740 = positif(INDTXMIN) * LIG1 * positif(IND_REV) * (1 - positif(ANNUL2042)) ;

regle 901940:
application :  iliad ;

LIG3750 = present(V_ZDC) * null(abs(V_ZDC - 1)) * positif(IREST) * LIG1 ;

regle 901950:
application : iliad  ;


LIGPRR2 = positif(PRR2V + PRR2C + PRR2P + PRR2ZV + PRR2ZC + PRR2Z1 + PRR2Z2 + PRR2Z3 + PRR2Z4 
                  + PRR2RAL + PRR2RAM + PRR2RBL + PRR2RBM + PRR2RCL + PRR2RCM + PRR2RDL + PRR2RDM 
		  + PRR2REL + PRR2REM + PRR2RFL + PRR2RFM + PENALIMV + PENALIMC + PENALIMP + 0) ;

regle 901990:
application :  iliad ;

LIG062V = CARPEV + CARPENBAV + PENSALV + PENSALNBV + CODRAZ + CODRAL + CODRAM ;
LIG062C = CARPEC + CARPENBAC + PENSALC + PENSALNBC + CODRBZ + CODRBL + CODRBM ;
LIG062P = somme(i=1..4: CARPEPi + CARPENBAPi) + somme(i=1..4: PENSALPi + PENSALNBPi) + CODRCZ + CODRDZ + CODREZ + CODRFZ 
          + CODRCL + CODRCM  + CODRDL + CODRDM  + CODREL + CODREM  + CODRFL + CODRFM ;

regle 902000:
application :  iliad ;

LIG066 = somme(i=1..4:PEBFi);

regle 902020:
application :  iliad ;

TSDECV = TSHALLOV + COD1PM + COD1TP + COD1NX + COD1AF + COD1AG ;
TSDECC = TSHALLOC + COD1QM + COD1UP + COD1OX + COD1BF + COD1BG ;
TSDECP = TSHALLO1 + TSHALLO2 + TSHALLO3 + TSHALLO4 + COD1CF + COD1CG
         + COD1DF + COD1DG + COD1EF + COD1EG + COD1FF + COD1FG ;

LIG_SAL = positif_ou_nul(TSDECV + TSDECC + TSDECP) * positif_ou_nul(ALLOV + ALLOC + ALLOP + COD1GB + COD1HB + REVACPAC) * LIG0  * LIG2 ;

LIG_REVASS = positif_ou_nul(ALLOV + ALLOC + ALLOP) * positif_ou_nul(TSDECV + TSDECC + TSDECP + COD1GB + COD1HB + REVACPAC) * LIG0  * LIG2 ;

LIGREVAC = positif_ou_nul(COD1GB + COD1HB + REVACPAC) * positif_ou_nul(TSDECV + TSDECC + TSDECP + ALLOV + ALLOC + ALLOP) * LIG0  * LIG2 ;

LIG_SALASS = positif(TSBNV + TSBNC + TSBNP + F10AV + F10AC + F10AP
                     + null(ALLOV + ALLOC + ALLOP) * null(TSDECV + TSDECC + TSDECP) * null(COD1GB + COD1HB + REVACPAC))
               * LIG0 * LIG2 ;

LIG_GATASA = positif_ou_nul(BPCOSAV + BPCOSAC + GLDGRATV + GLDGRATC) * LIG0 * LIG2 ;

LIGF10V = positif(F10AV + F10BV) * LIG0 * LIG2 ;

LIGF10C = positif(F10AC + F10BC) * LIG0 * LIG2 ;

LIGF10P = positif(F10AP + F10BP) * LIG0 * LIG2 ;

PENSDECV = PRBRV + COD1AL + COD1AM ;
PENSDECC = PRBRC + COD1BL + COD1BM ;
PENSDECP = PRBR1 + PRBR2 + PRBR3 + PRBR4 + COD1CL + COD1CM
           + COD1DL + COD1DM + COD1EL + COD1EM + COD1FL + COD1FM ;

LIGPENS = positif(PENSDECV + PENSDECC + PENSDECP) * LIG0 * LIG2 ;

regle 902030:
application :  iliad ;


LIGRCMABT = positif(present(RCMABD) + present(RCMTNC)
                    + present(RCMHAD) + present(RCMHAB) + present(RCMAV) + present(REGPRIV)
                    + present(RCMFR) + present(DEFRCM) + present(DEFRCM2) + present(DEFRCM3)
                    + present(DEFRCM4) + present(DEFRCM5) + present(DEFRCM6)+ present(COD2TT)+present(COD2VV)+present                      (COD2WW)+present(COD2YY)+present(COD2ZZ))
             * LIG1 * LIG2 * positif(INDREV1A8IR) ;

LIG2RCMABT = positif(present(REVACT) + present(REVPEA) + present(PROVIE) + present(DISQUO) + present(RESTUC) + present(INTERE))
                * LIG1 * LIG2 * positif(INDREV1A8IR) ;

LIGPV3VG = positif(PVBAR3VG) * LIG1 * LIG2 * positif(INDREV1A8IR) ;

LIGPV3WB = positif(PVBAR3WB) * LIG1 * LIG2 * positif(INDREV1A8IR) ;

LIGPV3VE = positif(PVBAR3VE) * LIG1 * LIG2 * positif(INDREV1A8IR) ;


LIGPVMTS = positif(PVMTS) * LIG1 * LIG2 ;

regle 902040:
application :  iliad ;


LIG_REPORT = positif(LIGRNIDF + LIGDFRCM + LIGDRFRP + LIGDEFBA
                     + LIGDLMRN + LIGDEFPLOC + LIGBNCDF + LIG2TUV + LIGDRCVM
		     + LIGBAMVV + LIGBAMVC + LIGBAMVP
                     + somme(i=V,C,P:LIGMIBDREPi + LIGMBDREPNPi + LIGSPEDREPi + LIGSPDREPNPi)
                     + LIGREPREPAR

	             + LIGRCELFABC + LIGRCELFD + LIGRCEL2012 + LIGRCELJOQR 
                     + LIGRCEL + LIGRCELCOM + LIGRCELHJK + LIGRCELZAB + LIGRCELZEF + LIGRCELZIJ
	             + LIGRRCEL1 + LIGRRCEL2 + LIGRRCEL3 + LIGRRCEL4 + LIGRRCEL5 + LIGRRCEL6 + LIGRRCELH

                     + LIGRCODOX + LIGRCODOW + LIGRCODOV + LIGRCODOU + LIGRCODJT 
                     + LIGRLOCIDFG + LIGNEUV + LIGVIEU
                     + LIGREPLOC15 + LIGREPLOC12 + LIGREPLOC11 + LIGREPLOC10 + LIGREPLOC9                      
	             + LIGRDUFLOTOT + LIGRPINELTOT
                     + LIGCOMP01

	             + LIGREPQKG + LIGREPQUS + LIGREPQWB + LIGREPRXC + LIGREPXIO
	             + LIGREPPEK + LIGREPPFL + LIGREPPHO + LIGREPPIZ + LIGREPCCS 
                     + LIGREPPJA + LIGREPCD + LIGREPPLB
	             + LIGREPRUP + LIGREPRVQ + LIGREPRWR + LIGREPRYT 
                     + LIGREPSAA + LIGREPSAB + LIGREPSAC + LIGREPSAE + LIGREPSAF
                     + LIGREPSAG + LIGREPSAH + LIGREPSAJ + LIGREPSAM 
                     + LIGREPTBE + LIGREPSAU + LIGREPSAV + LIGREPSAW + LIGREPSAY
                     + LIGREPBX + LIGREPBY + LIGREPBZ + LIGREPCB 

                     + LIGREPCORSE + LIGPME + LIGRSN + LIGPLAFRSN + LIGREPDON
                     + LIGNFOREST + LIGRCIF + LIGRCIFAD + LIGRCIFSIN + LIGRCIFADSN
                     + LIGPATNATR + LIGREPRECH + LIGREPCICE
	             ) * LIG2 ;

regle 902050:
application : iliad  ;

LIGRNIDF = positif(abs(RNIDF)) * LIG1 * LIG2 ;
LIGRNIDF0 = positif(abs(RNIDF0)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG1 * LIG2 ;
LIGRNIDF1 = positif(abs(RNIDF1)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG1 * LIG2 ;
LIGRNIDF2 = positif(abs(RNIDF2)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG1 * LIG2 ;
LIGRNIDF3 = positif(abs(RNIDF3)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG1 * LIG2 ;
LIGRNIDF4 = positif(abs(RNIDF4)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG1 * LIG2 ;
LIGRNIDF5 = positif(abs(RNIDF5)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG1 * LIG2 ;

regle 902060:
application : iliad  ;

LIGDUFREP = positif(DDUFREP) * LIG1 * LIG2 ;

LIGDUFLO = positif(DDUFLO) * LIG1 * LIG2 ;

LIGPIREP = positif(DPIREP) * LIG1 * LIG2 ;

LIGPINEL = positif(DPINEL) * LIG1 * LIG2 ;

LIGDUFTOT = LIGDUFREP + LIGDUFLO ; 

LIGPINTOT = LIGPIREP + LIGPINEL ;

regle 902070:
application : iliad  ;

LIGRDUEKL = positif(RIVDUEKL) * LIG1 * LIG2 ;
LIGRDUEKL1 = LIGRDUEKL * null(RIVDUEKL - RIVDUEKL8) ;
LIGRDUEKL2 = LIGRDUEKL * (1 - null(RIVDUEKL - RIVDUEKL8)) ;

LIGRPIQB = positif(RIVPIQB) * LIG1 * LIG2 ;
LIGRPIQB1 = LIGRPIQB * null(RIVPIQB - RIVPIQB8) ;
LIGRPIQB2 = LIGRPIQB * (1 - null(RIVPIQB - RIVPIQB8)) ;

LIGRPIQD = positif(RIVPIQD) * LIG1 * LIG2 ;
LIGRPIQD1 = LIGRPIQD * null(RIVPIQD - RIVPIQD8) ;
LIGRPIQD2 = LIGRPIQD * (1 - null(RIVPIQD - RIVPIQD8)) ;

LIGRPIQA = positif(RIVPIQA) * LIG1 * LIG2 ;
LIGRPIQA1 = LIGRPIQA * null(RIVPIQA - RIVPIQA5) ;
LIGRPIQA2 = LIGRPIQA * (1 - null(RIVPIQA - RIVPIQA5)) ;

LIGRPIQC = positif(RIVPIQC) * LIG1 * LIG2 ;
LIGRPIQC1 = LIGRPIQC * null(RIVPIQC - RIVPIQC5) ;
LIGRPIQC2 = LIGRPIQC * (1 - null(RIVPIQC - RIVPIQC5)) ;

LIGRPIQN = positif(RIVPIQN) * LIG1 * LIG2 ;
LIGRPIQN1 = LIGRPIQN * null(RIVPIQN - RIVPIQN8) ;
LIGRPIQN2 = LIGRPIQN * (1 - null(RIVPIQN - RIVPIQN8)) ;

LIGRPIQO = positif(RIVPIQO) * LIG1 * LIG2 ;
LIGRPIQO1 = LIGRPIQO * null(RIVPIQO - RIVPIQO5) ;
LIGRPIQO2 = LIGRPIQO * (1 - null(RIVPIQO - RIVPIQO5)) ;

LIGRPIQM = positif(RIVPIQM) * LIG1 * LIG2 ;
LIGRPIQM1 = LIGRPIQM * null(RIVPIQM - RIVPIQM5) ;
LIGRPIQM2 = LIGRPIQM * (1 - null(RIVPIQM - RIVPIQM5)) ;

LIGRPIQP = positif(RIVPIQP) * LIG1 * LIG2 ;
LIGRPIQP1 = LIGRPIQP * null(RIVPIQP - RIVPIQP8) ;
LIGRPIQP2 = LIGRPIQP * (1 - null(RIVPIQP - RIVPIQP8)) ;

LIGRPIQL = positif(RIVPIQL) * LIG1 * LIG2 ;
LIGRPIQL1 = LIGRPIQL * null(RIVPIQL - RIVPIQL8) ;
LIGRPIQL2 = LIGRPIQL * (1 - null(RIVPIQL - RIVPIQL8)) ;

LIGRPIQJ = positif(RIVPIQJ) * LIG1 * LIG2 ;
LIGRPIQJ1 = LIGRPIQJ * null(RIVPIQJ - RIVPIQJ8) ;
LIGRPIQJ2 = LIGRPIQJ * (1 - null(RIVPIQJ - RIVPIQJ8)) ;

LIGRPIQK = positif(RIVPIQK) * LIG1 * LIG2 ;
LIGRPIQK1 = LIGRPIQK * null(RIVPIQK - RIVPIQK5) ;
LIGRPIQK2 = LIGRPIQK * (1 - null(RIVPIQK - RIVPIQK5)) ;

LIGRPIQI = positif(RIVPIQI) * LIG1 * LIG2 ;
LIGRPIQI1 = LIGRPIQI * null(RIVPIQI - RIVPIQI5) ;
LIGRPIQI2 = LIGRPIQI * (1 - null(RIVPIQI - RIVPIQI5)) ;

LIGRPIQH = positif(RIVPIQH) * LIG1 * LIG2 ;
LIGRPIQH1 = LIGRPIQH * null(RIVPIQH - RIVPIQH8) ;
LIGRPIQH2 = LIGRPIQH * (1 - null(RIVPIQH - RIVPIQH8)) ;

LIGRPIQF = positif(RIVPIQF) * LIG1 * LIG2 ;
LIGRPIQF1 = LIGRPIQF * null(RIVPIQF - RIVPIQF8) ;
LIGRPIQF2 = LIGRPIQF * (1 - null(RIVPIQF - RIVPIQF8)) ;

LIGRPIQG = positif(RIVPIQG) * LIG1 * LIG2 ;
LIGRPIQG1 = LIGRPIQG * null(RIVPIQG - RIVPIQG5) ;
LIGRPIQG2 = LIGRPIQG * (1 - null(RIVPIQG - RIVPIQG5)) ;

LIGRPIQE = positif(RIVPIQE) * LIG1 * LIG2 ;
LIGRPIQE1 = LIGRPIQE * null(RIVPIQE - RIVPIQE5) ;
LIGRPIQE2 = LIGRPIQE * (1 - null(RIVPIQE - RIVPIQE5)) ;

LIGRDUGIH = positif(RIVDUGIH) * LIG1 * LIG2 ;
LIGRDUGIH1 = LIGRDUGIH * null(RIVDUGIH - RIVDUGIH8) ;
LIGRDUGIH2 = LIGRDUGIH * (1 - null(RIVDUGIH - RIVDUGIH8)) ;

LIGRDUFLOTOT = LIGRDUEKL + LIGRDUGIH ;
LIGRPINELTOT = LIGRPIQA + LIGRPIQB + LIGRPIQC + LIGRPIQD + LIGRPIQE + LIGRPIQF + LIGRPIQG + LIGRPIQH 
               + LIGRPIQI + LIGRPIQJ + LIGRPIQK + LIGRPIQL + LIGRPIQM + LIGRPIQN + LIGRPIQO + LIGRPIQP ;

regle 902080:
application : iliad  ;


LIGCELSOM1 = positif(DCELSOM1) * LIG1 * LIG2 ;

LIGCELSOM2 = positif(DCELSOM2) * LIG1 * LIG2 ;

LIGCELSOM3 = positif(DCELSOM3) * LIG1 * LIG2 ;

LIGCELSOM4 = positif(DCELSOM4) * LIG1 * LIG2 ;

LIGCELSOM5 = positif(DCELSOM5) * LIG1 * LIG2 ;

LIGCELSOM6 = positif(DCELSOM6) * LIG1 * LIG2 ;

LIGCELSOM7 = positif(DCELSOM7) * LIG1 * LIG2 ;

regle 902090:
application : iliad  ;


LIGRCEL =  positif(RIVCEL1) * CNRLIG12 ;
LIG1CEL = LIGRCEL * null(RIVCEL1 - RIVCEL8) ;
LIG2CEL = LIGRCEL * (1 - null(RIVCEL1 - RIVCEL8)) ;

LIGRCELCOM =  positif(RIVCELCOM1) * CNRLIG12 ;
LIGCOM1 = LIGRCELCOM * null(RIVCELCOM1 - RIVCELCOM4) ;
LIGCOM2 = LIGRCELCOM * (1 - null(RIVCELCOM1 - RIVCELCOM4)) ;

LIGRCELHJK =  positif(RIVCELHJK1) * CNRLIG12 ;
LIGHJK1 = LIGRCELHJK * null(RIVCELHJK1 - RIVCELHJK8) ;
LIGHJK2 = LIGRCELHJK * (1 - null(RIVCELHJK1 - RIVCELHJK8)) ;

LIGRCELZAB = positif(RIVCELZAB1) * CNRLIG12 ;
LIGZAB1 = LIGRCELZAB * null(RIVCELZAB1 - RIVCELZAB3) ;
LIGZAB2 = LIGRCELZAB * (1 - null(RIVCELZAB1 - RIVCELZAB3)) ;

LIGRCELZEF = positif(RIVCELZEF1) * CNRLIG12 ;
LIGZEF1 = LIGRCELZEF * null(RIVCELZEF1 - RIVCELZEF3) ;
LIGZEF2 = LIGRCELZEF * (1 - null(RIVCELZEF1 - RIVCELZEF3)) ;

LIGRCELZIJ = positif(RIVCELZIJ1) * CNRLIG12 ;
LIGZIJ1 = LIGRCELZIJ * null(RIVCELZIJ1 - RIVCELZIJ3) ;
LIGZIJ2 = LIGRCELZIJ * (1 - null(RIVCELZIJ1 - RIVCELZIJ3)) ;


LIGRCELJOQR =  positif(RIVCELJOQR1) * CNRLIG12 ;
LIGJOQR1 = LIGRCELJOQR * null(RIVCELJOQR1 - RIVCELJOQR4) ;
LIGJOQR2 = LIGRCELJOQR * (1 - null(RIVCELJOQR1 - RIVCELJOQR4)) ;

LIGRCEL2012 = positif(RIV2012CEL1) * CNRLIG12 ;
LIG20121 = LIGRCEL2012 * null(RIV2012CEL1 - RIV2012CEL8) ;
LIG20122 = LIGRCEL2012 * (1 - null(RIV2012CEL1 - RIV2012CEL8)) ;

LIGRCELFABC =  positif(RIVCELFABC1) * CNRLIG12 ;
LIGFABC1 = LIGRCELFABC * null(RIVCELFABC1 - RIVCELFABC8) ;
LIGFABC2 = LIGRCELFABC * (1 - null(RIVCELFABC1 - RIVCELFABC8)) ;

LIGRCELFD =  positif(RIVCELFD1) * CNRLIG12 ;
LIGFD1 = LIGRCELFD * null(RIVCELFD1 - RIVCELFD4) ;
LIGFD2 = LIGRCELFD * (1 - null(RIVCELFD1 - RIVCELFD4)) ;


LIGRRCEL1 = positif(RRCELLJ + RRCELMG + RRCELMH + RRCELLP + RRCELLV + RRCEL2012) * CNRLIG12 ;
LIGRRCEL11 = positif(RRCELMG) * CNRLIG12 ;
LIGRRCEL12 = positif(RRCELMH) * CNRLIG12 ;
LIGRRCEL13 = positif(RRCELLJ) * CNRLIG12 ;
LIGRRCEL14 = positif(RRCELLP) * CNRLIG12 ;
LIGRRCEL15 = positif(RRCELLV) * CNRLIG12 ;
LIGRRCEL16 = positif(RRCEL2012) * CNRLIG12 ;

LIGRRCEL2 = positif(RRCELLF + RRCELLZ + RRCELLX + RRCELLI + RRCELLO + RRCELLU + RRCEL2011) * CNRLIG12 ;
LIGRRCEL21 = positif(RRCELLF) * CNRLIG12 ;
LIGRRCEL22 = positif(RRCELLZ) * CNRLIG12 ;
LIGRRCEL23 = positif(RRCELLX) * CNRLIG12 ;
LIGRRCEL24 = positif(RRCELLI) * CNRLIG12 ;
LIGRRCEL25 = positif(RRCELLO) * CNRLIG12 ;
LIGRRCEL26 = positif(RRCELLU) * CNRLIG12 ;
LIGRRCEL27 = positif(RRCEL2011) * CNRLIG12 ;

LIGRRCEL3 = positif(RRCELLD + RRCELLS + RRCELLT + RRCELLH + RRCELLL + RRCELLR + RRCEL2010) * CNRLIG12 ;
LIGRRCEL32 = positif(RRCELLD) * CNRLIG12 ;
LIGRRCEL33 = positif(RRCELLS) * CNRLIG12 ;
LIGRRCEL34 = positif(RRCELLT) * CNRLIG12 ;
LIGRRCEL35 = positif(RRCELLH) * CNRLIG12 ;
LIGRRCEL36 = positif(RRCELLL) * CNRLIG12 ;
LIGRRCEL37 = positif(RRCELLR) * CNRLIG12 ;
LIGRRCEL38 = positif(RRCEL2010) * CNRLIG12 ;

LIGRRCEL4 = positif(RRCELLE + RRCELLM + RRCELLN + RRCELLG + RRCELLK + RRCELLQ + RRCEL2009) * CNRLIG12 ;
LIGRRCEL43 = positif(RRCELLE) * CNRLIG12 ;
LIGRRCEL44 = positif(RRCELLM) * CNRLIG12 ;
LIGRRCEL45 = positif(RRCELLN) * CNRLIG12 ;
LIGRRCEL46 = positif(RRCELLG) * CNRLIG12 ;
LIGRRCEL47 = positif(RRCELLK) * CNRLIG12 ;
LIGRRCEL48 = positif(RRCELLQ) * CNRLIG12 ;
LIGRRCEL49 = positif(RRCEL2009) * CNRLIG12 ;

LIGRRCEL5 = positif(RRCELZP + RRCELE) * CNRLIG12 ;
LIGRCELZP = positif(RRCELZP) * CNRLIG12 ;
LIGRRCELE = positif(RRCELE) * CNRLIG12 ;

LIGRRCEL6 = positif(RRCELZO + RRCELF) * CNRLIG12 ;
LIGRCELZO = positif(RRCELZO) * CNRLIG12 ;
LIGRRCELF = positif(RRCELF) * CNRLIG12 ;

LIGRRCELH = positif(RRCELH) * CNRLIG12 ;

regle 902100:
application : iliad  ;


LIGPATNAT2 = LIG1 * LIG2 * (positif(PATNAT2) + null(PATNAT2) * positif(V_NOTRAIT - 20)) ;
LIGPATNAT3 = LIG1 * LIG2 * (positif(PATNAT3) + null(PATNAT3) * positif(V_NOTRAIT - 20)) ;
LIGPATNAT4 = LIG1 * LIG2 * (positif(PATNAT4) + null(PATNAT4) * positif(V_NOTRAIT - 20)) ;

LIGPATNATR = positif(REPNATR + REPNATR1 + REPNATR2 + REPNATR3) * LIG1 ; 
LIGNATR1 = positif(REPNATR1) * LIG1 ; 
LIGNATR2 = positif(REPNATR2) * LIG1 ; 
LIGNATR3 = positif(REPNATR3) * LIG1 ; 
LIGNATR = positif(REPNATR) * LIG1 ; 

regle 902110:
application : iliad  ;


LIGREPRD = positif(REPRD) * CNRLIG1 ;
LIGREPXE = positif(REPXE) * CNRLIG1 ;
LIGREPXK = positif(REPXK) * CNRLIG1 ;
LIGREPXP = positif(REPXP) * CNRLIG1 ;
LIGREPXU = positif(REPXU) * CNRLIG1 ;
LIGREPQKG = positif(REPRD + REPXE + REPXK + REPXP + REPXU) * CNRLIG1 ;

LIGREPRA = positif(REPRA) * CNRLIG1 ;
LIGREPXA = positif(REPXA) * CNRLIG1 ;
LIGREPXF = positif(REPXF) * CNRLIG1 ;
LIGREPXL = positif(REPXL) * CNRLIG1 ;
LIGREPXQ = positif(REPXQ) * CNRLIG1 ;
LIGREPQUS = positif(REPRA + REPXA + REPXF + REPXL + REPXQ) * CNRLIG1 ;

LIGREPRB = positif(REPRB) * CNRLIG1 ;
LIGREPXB = positif(REPXB) * CNRLIG1 ;
LIGREPXG = positif(REPXG) * CNRLIG1 ;
LIGREPXM = positif(REPXM) * CNRLIG1 ;
LIGREPXR = positif(REPXR) * CNRLIG1 ;
LIGREPQWB = positif(REPRB + REPXB + REPXG + REPXM + REPXR) * CNRLIG1 ;

LIGREPRC = positif(REPRC) * CNRLIG1 ;
LIGREPXC = positif(REPXC) * CNRLIG1 ;
LIGREPXH = positif(REPXH) * CNRLIG1 ;
LIGREPXN = positif(REPXN) * CNRLIG1 ;
LIGREPXS = positif(REPXS) * CNRLIG1 ;
LIGREPRXC = positif(REPRC + REPXC + REPXH + REPXN + REPXS) * CNRLIG1 ;

LIGREPXI = positif(REPXI) * CNRLIG1 ;
LIGREPXO = positif(REPXO) * CNRLIG1 ;
LIGREPXT = positif(REPXT) * CNRLIG1 ;
LIGREPXIO = positif(REPXI + REPXO + REPXT) * CNRLIG1 ;

LIGREPSK = positif(REPSK) * CNRLIG1 ;
LIGREPAK = positif(REPAK) * CNRLIG1 ;
LIGREPBI = positif(REPBI) * CNRLIG1 ;
LIGREPPEK = positif(REPSK + REPAK + REPBI) * CNRLIG1 ;

LIGREPSL = positif(REPSL) * CNRLIG1 ;
LIGREPAL = positif(REPAL) * CNRLIG1 ;
LIGREPBJ = positif(REPBJ) * CNRLIG1 ;
LIGREPPFL = positif(REPSL + REPAL + REPBJ) * CNRLIG1 ;

LIGREPSO = positif(REPSO) * CNRLIG1 ;
LIGREPAO = positif(REPAO) * CNRLIG1 ;
LIGREPBM = positif(REPBM) * CNRLIG1 ;
LIGREPPHO = positif(REPSO + REPAO + REPBM) * CNRLIG1 ;

LIGREPSZ = positif(REPSZ) * CNRLIG1 ;
LIGREPBA = positif(REPBA) * CNRLIG1 ;
LIGREPPIZ = positif(REPSZ + REPBA) * CNRLIG1 ;

LIGREPCC = positif(REPCC) * CNRLIG1 ;
LIGREPCS = positif(REPCS) * CNRLIG1 ;
LIGREPDS = positif(REPDS) * CNRLIG1 ;
LIGREPCCS = positif(REPCC + REPCS + REPDS) * CNRLIG1 ;

LIGREPTA = positif(REPTA) * CNRLIG1 ;
LIGREPBB = positif(REPBB) * CNRLIG1 ;
LIGREPPJA = positif(REPTA + REPBB) * CNRLIG1 ;

LIGREPCD = positif(REPCD) * CNRLIG1 ;
LIGREPCT = positif(REPHCT) * CNRLIG1 ;
LIGREPDT = positif(REPDT) * CNRLIG1 ;
LIGREPCDT = positif(REPCD + REPHCT + REPDT) * CNRLIG1 ;

LIGREPTD = positif(REPTD) * CNRLIG1 ;
LIGREPBG = positif(REPBG) * CNRLIG1 ;
LIGREPCG = positif(REPCG) * CNRLIG1 ;
LIGREPCW = positif(REPCW) * CNRLIG1 ;
LIGREPDW = positif(REPDW) * CNRLIG1 ;
LIGREPPLB = positif(REPTD + REPBG + REPCG + REPCW + REPDW) * CNRLIG1 ;

LIGREPSP = positif(REPSP) * CNRLIG1 ;
LIGREPAP = positif(REPAP) * CNRLIG1 ;
LIGREPBN = positif(REPBN) * CNRLIG1 ;
LIGREPRUP = positif(REPSP + REPAP + REPBN) * CNRLIG1 ;

LIGREPSQ = positif(REPSQ) * CNRLIG1 ;
LIGREPAQ = positif(REPAQ) * CNRLIG1 ;
LIGREPBO = positif(REPBO) * CNRLIG1 ;
LIGREPRVQ = positif(REPSQ + REPAQ + REPBO) * CNRLIG1 ;

LIGREPSR = positif(REPSR) * CNRLIG1 ;
LIGREPHAR = positif(REPHAR) * CNRLIG1 ;
LIGREPBP = positif(REPBP) * CNRLIG1 ;
LIGREPRWR = positif(REPSR + REPHAR + REPBP) * CNRLIG1 ;

LIGREPST = positif(REPST) * CNRLIG1 ;
LIGREPAT = positif(REPAT) * CNRLIG1 ;
LIGREPBR = positif(REPBR) * CNRLIG1 ;
LIGREPRYT = positif(REPST + REPAT + REPBR) * CNRLIG1 ;

LIGREPSA = positif(REPSA) * CNRLIG1 ;
LIGREPAA = positif(REPAA) * CNRLIG1 ;
LIGREPSAA = positif(REPSA + REPAA) * CNRLIG1 ;

LIGREPSB = positif(REPSB) * CNRLIG1 ;
LIGREPAB = positif(REPAB) * CNRLIG1 ;
LIGREPSAB = positif(REPSB + REPAB) * CNRLIG1 ;

LIGREPSC = positif(REPSC) * CNRLIG1 ;
LIGREPAC = positif(REPAC) * CNRLIG1 ;
LIGREPSAC = positif(REPSC + REPAC) * CNRLIG1 ;

LIGREPSE = positif(REPSE) * CNRLIG1 ;
LIGREPAE = positif(REPAE) * CNRLIG1 ;
LIGREPSAE = positif(REPSE + REPAE) * CNRLIG1 ;

LIGREPSF = positif(REPSF) * CNRLIG1 ;
LIGREPAF = positif(REPAF) * CNRLIG1 ;
LIGREPSAF = positif(REPSF + REPAF) * CNRLIG1 ;

LIGREPSG = positif(REPSG) * CNRLIG1 ;
LIGREPAG = positif(REPAG) * CNRLIG1 ;
LIGREPSAG = positif(REPSG + REPAG) * CNRLIG1 ;

LIGREPSH = positif(REPSH) * CNRLIG1 ;
LIGREPAH = positif(REPAH) * CNRLIG1 ;
LIGREPSAH = positif(REPSH + REPAH) * CNRLIG1 ;

LIGREPSJ = positif(REPSJ) * CNRLIG1 ;
LIGREPAJ = positif(REPAJ) * CNRLIG1 ;
LIGREPSAJ = positif(REPSJ + REPAJ) * CNRLIG1 ;

LIGREPSM = positif(REPSM) * CNRLIG1 ;
LIGREPAM = positif(REPAM) * CNRLIG1 ;
LIGREPBK = positif(REPBK) * CNRLIG1 ;
LIGREPSAM = positif(REPSM + REPAM + REPBK) * CNRLIG1 ;

LIGREPTB = positif(REPTB) * CNRLIG1 ;
LIGREPBE = positif(REPBE) * CNRLIG1 ;
LIGREPCE = positif(REPCE) * CNRLIG1 ;
LIGREPCU = positif(REPCU) * CNRLIG1 ;
LIGREPDU = positif(REPDU) * CNRLIG1 ;
LIGREPTBE = positif(REPTB + REPBE + REPCE + REPCU + REPDU) * CNRLIG1 ;

LIGREPSU = positif(REPSU) * CNRLIG1 ;
LIGREPAU = positif(REPAU) * CNRLIG1 ;
LIGREPBS = positif(REPBS) * CNRLIG1 ;
LIGREPCI = positif(REPCI) * CNRLIG1 ;
LIGREPDI = positif(REPDI) * CNRLIG1 ;
LIGREPSAU = positif(REPSU + REPAU + REPBS + REPCI + REPDI) * CNRLIG1 ;

LIGREPSV = positif(REPSV) * CNRLIG1 ;
LIGREPAV = positif(REPAV) * CNRLIG1 ;
LIGREPBT = positif(REPBT) * CNRLIG1 ;
LIGREPCJ = positif(REPCJ) * CNRLIG1 ;
LIGREPDJ = positif(REPDJ) * CNRLIG1 ;
LIGREPSAV = positif(REPSV + REPAV + REPBT + REPCJ + REPDJ) * CNRLIG1 ;

LIGREPSW = positif(REPSW) * CNRLIG1 ;
LIGREPAW = positif(REPAW) * CNRLIG1 ;
LIGREPBU = positif(REPBU) * CNRLIG1 ;
LIGREPCK = positif(REPCK) * CNRLIG1 ;
LIGREPDK = positif(REPDK) * CNRLIG1 ;
LIGREPSAW = positif(REPSW + REPAW + REPBU + REPCK + REPDK) * CNRLIG1 ;

LIGREPSY = positif(REPSY) * CNRLIG1 ;
LIGREPAY = positif(REPAY) * CNRLIG1 ;
LIGREPBW = positif(REPBW) * CNRLIG1 ;
LIGREPCM = positif(REPCM) * CNRLIG1 ;
LIGREPDM = positif(REPDM) * CNRLIG1 ;
LIGREPSAY = positif(REPSY + REPAY + REPBW + REPCM + REPDM) * CNRLIG1 ;

LIGREPBX = positif(REPBX) * CNRLIG1 ;
LIGREPCN = positif(REPCN) * CNRLIG1 ;
LIGREPDN = positif(REPDN) * CNRLIG1 ;
LIGREPBXN = positif(REPBX + REPCN + REPDN) * CNRLIG1 ;

LIGREPBY = positif(REPBY) * CNRLIG1 ;
LIGREPCO = positif(REPCO) * CNRLIG1 ;
LIGREPDO = positif(REPDO) * CNRLIG1 ;
LIGREPBYO = positif(REPBY + REPCO + REPDO) * CNRLIG1 ;

LIGREPBZ = positif(REPBZ) * CNRLIG1 ;
LIGREPCP = positif(REPCP) * CNRLIG1 ;
LIGREPDP = positif(REPDP) * CNRLIG1 ;
LIGREPBZP = positif(REPBZ + REPCP + REPDP) * CNRLIG1 ;

LIGREPCB = positif(REPCB) * CNRLIG1 ;
LIGREPCR = positif(REPCR) * CNRLIG1 ;
LIGREPDR = positif(REPDR) * CNRLIG1 ;
LIGREPCBR = positif(REPCB + REPCR + REPDR) * CNRLIG1 ;


LIGREPDON = positif(REPDONR + REPDONR1 + REPDONR2 + REPDONR3 + REPDONR4) * CNRLIG1 ;
LIGREPDONR1 = positif(REPDONR1) * CNRLIG1 ;
LIGREPDONR2 = positif(REPDONR2) * CNRLIG1 ;
LIGREPDONR3 = positif(REPDONR3) * CNRLIG1 ;
LIGREPDONR4 = positif(REPDONR4) * CNRLIG1 ;
LIGREPDONR = positif(REPDONR) * CNRLIG1 ;
LIGRIDOMPRO = positif(RIDOMPRO) * LIG1 ;

LIGPME1 = positif(REPINVPME1) * CNRLIG1 ;
LIGPME2 = positif(REPINVPME2) * CNRLIG1 ;
LIGPME3 = positif(REPINVPME3) * CNRLIG1 ;

LIGRSN = positif(RINVPECR + RINVPECV + RINVPECX + RINVPECF) * CNRLIG1 ;
LIGRSN3 = positif(RINVPECR) * CNRLIG1 ;
LIGRSN2 = positif(RINVPECV) * CNRLIG1 ;
LIGRSN1 = positif(RINVPECX) * CNRLIG1 ;
LIGRSN0 = positif(RINVPECF) * CNRLIG1 ;

LIGPLAFRSN = positif(RPLAFPME17 + RPLAFPME16 + RPLAFPME15 + RPLAFPME14 + RPLAFPME13) * CNRLIG1 ;
LIGPLAFRSN7 = positif(RPLAFPME17) * CNRLIG1 ;
LIGPLAFRSN6 = positif(RPLAFPME16) * CNRLIG1 ;
LIGPLAFRSN5 = positif(RPLAFPME15) * CNRLIG1 ;
LIGPLAFRSN4 = positif(RPLAFPME14) * CNRLIG1 ;
LIGPLAFRSN3 = positif(RPLAFPME13) * CNRLIG1 ;

LIGREPFOR3 = positif(REPFOREST3) * CNRLIG12 ;

regle 902120:
application :  iliad ;

EXOVOUS = present ( TSASSUV ) 
          + positif ( XETRANV )
          + positif ( EXOCETV ) 
	  + positif ( COD5XA )
          + positif ( MIBEXV ) 
          + positif ( MIBNPEXV ) 
          + positif ( BNCPROEXV ) 
          + positif ( XSPENPV ) 
          + positif ( XBAV ) 
          + positif ( XBIPV ) 
          + positif ( XBINPV ) 
          + positif ( XBNV ) 
          + positif ( XBNNPV ) 
          + positif ( ABICPDECV ) * ( 1 - V_CNR )
          + positif ( ABNCPDECV ) * ( 1 - V_CNR )
          + positif ( HONODECV ) * ( 1 - V_CNR )
          + positif ( AGRIV )
          + positif ( BNCCREAV ) 
          ;

EXOCJT = present ( TSASSUC ) 
         + positif ( XETRANC )
         + positif ( EXOCETC ) 
	 + positif ( COD5YA )
         + positif ( MIBEXC ) 
         + positif ( MIBNPEXC ) 
         + positif ( BNCPROEXC ) 
         + positif ( XSPENPC ) 
         + positif ( XBAC ) 
         + positif ( XBIPC ) 
         + positif ( XBINPC ) 
         + positif ( XBNC ) 
         + positif ( XBNNPC ) 
         + positif ( ABICPDECC ) * ( 1 - V_CNR )
         + positif ( ABNCPDECC ) * ( 1 - V_CNR )
         + positif ( HONODECC ) * ( 1 - V_CNR )
         + positif ( AGRIC )
         + positif ( BNCCREAC ) 
         ;
 
EXOPAC = positif ( COD5ZA ) 
         + positif ( MIBEXP ) 
         + positif ( MIBNPEXP ) 
         + positif ( BNCPROEXP ) 
         + positif ( XSPENPP ) 
         + positif ( XBAP ) 
         + positif ( XBIPP ) 
         + positif ( XBINPP ) 
         + positif ( XBNP ) 
         + positif ( XBNNPP ) 
         + positif ( ABICPDECP ) * ( 1 - V_CNR )
         + positif ( ABNCPDECP ) * ( 1 - V_CNR )
         + positif ( HONODECP ) * ( 1 - V_CNR )
         + positif ( AGRIP )
         + positif ( BNCCREAP ) 
         ;

regle 902130:
application :  iliad ;

LIGTITREXVCP = positif(EXOVOUS)
               * positif(EXOCJT)
               * positif(EXOPAC)
	       * (1 - positif(LIG2501))
               * LIG1 * LIG2 ;

LIGTITREXV = positif(EXOVOUS)
             * (1 - positif(EXOCJT))
             * (1 - positif(EXOPAC))
	     * (1 - positif(LIG2501))
             * LIG1 * LIG2 ;

LIGTITREXC =  (1 - positif(EXOVOUS))
               * positif(EXOCJT)
               * (1 - positif(EXOPAC))
	       * (1 - positif(LIG2501))
               * LIG1 * LIG2 ;

LIGTITREXP =  (1 - positif(EXOVOUS))
               * (1 - positif(EXOCJT))
               * positif(EXOPAC)
	       * (1 - positif(LIG2501))
               * LIG1 * LIG2 ;

LIGTITREXVC =  positif(EXOVOUS)
               * positif(EXOCJT)
               * (1 - positif(EXOPAC))
	       * (1 - positif(LIG2501))
               * LIG1 * LIG2 ;

LIGTITREXVP =  positif(EXOVOUS)
               * (1 - positif(EXOCJT))
               * positif(EXOPAC)
	       * (1 - positif(LIG2501))
               * LIG1 * LIG2 ;

LIGTITREXCP =  (1 - positif(EXOVOUS))
               * positif(EXOCJT) 
               * positif(EXOPAC)
	       * (1 - positif(LIG2501))
               * LIG1 * LIG2 ;

regle 902140:
application :  iliad ;

EXOCET = EXOCETC + EXOCETV ;
LIGEXOCET = positif(EXOCET) * LIG1 * LIG2 ;

LIGEXBA = positif(COD5XA + COD5YA + COD5ZA) * LIG1 * LIG2 ;
LIGMXBIP =  positif(MIBEXV + MIBEXC + MIBEXP) * LIG1 * LIG2 ;
LIGMXBINP =  positif(MIBNPEXV + MIBNPEXC + MIBNPEXP) * LIG1 * LIG2 ;
LIGSXBN =  positif(BNCPROEXV + BNCPROEXC + BNCPROEXP) * LIG1 * LIG2 ;
LIGXSPEN =  positif(XSPENPV + XSPENPC + XSPENPP) * LIG1 * LIG2 ;
LIGXBIP =  positif(XBIPV + XBIPC + XBIPP) * LIG1 * LIG2 ;
LIGXBINP =  positif(XBINPV + XBINPC + XBINPP) * LIG1 * LIG2 ;
LIGXBP =  positif(XBNV + XBNC + XBNP) * LIG1 * LIG2 ;
LIGXBN =  positif(XBNNPV + XBNNPC + XBNNPP) * LIG1 * LIG2 ;

LIGXTSA =  positif(present(TSASSUV) + present(TSASSUC)) * LIG1 * LIG2 ;
LIGXIMPA =  positif(XETRANV + XETRANC) * LIG1 * LIG2 ;
LIGXBA =  positif(XBAV + XBAC + XBAP) * LIG1 * LIG2 ;

LIGBICAP = positif(ABICPDECV + ABICPDECC + ABICPDECP) * CNRLIG12 ;
LIGBNCAP = positif(ABNCPDECV + ABNCPDECC + ABNCPDECP) * CNRLIG12 ;
LIGHONO = positif(HONODECV + HONODECC + HONODECP) * CNRLIG12 ;

LIGBAPERP =  positif(BAPERPV + BAPERPC + BAPERPP + BANOCGAV + BANOCGAC + BANOCGAP) * LIG1 * LIG2 ;
LIGBNCCREA =  positif(BNCCREAV + BNCCREAC + BNCCREAP) * LIG1 * LIG2 ;

regle 902150:
application :  iliad ;


LIGPERP = (1 - positif(PERPIMPATRIE+0))
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * (1 - positif(PERP_COND1+PERP_COND2))
                  * (1 - positif(LIG8FV))
                  * (1 - positif(LIG2501))
                  * CNRLIG12 
                  +0
                  ;
LIGPERPI = positif(PERPIMPATRIE+0)
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * (1 - positif(PERP_COND1+PERP_COND2))
                  * (1 - positif(LIG8FV))
                  * (1 - positif(LIG2501))
                  * CNRLIG12
                  +0
                  ;
LIGPERPM = (1 - positif(PERPIMPATRIE+0))
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * positif(PERP_MUT)
                  * positif(PERP_COND1+PERP_COND2)
                  * (1 - positif(LIG8FV))
                  * (1 - positif(LIG2501))
                  * CNRLIG12
                  +0
                  ;
LIGPERPMI = positif(PERPIMPATRIE+0)
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * positif(PERP_MUT)
                  * positif(PERP_COND1+PERP_COND2)
                  * (1 - positif(LIG8FV))
                  * (1 - positif(LIG2501))
                  * CNRLIG12
                  +0
                  ;

LIGPERPFAM = positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV)
              * positif(PERPINDC + PERPINDCC)* positif(PERPINDAFFC)
              * positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP)
              * CNRLIG12
              * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
              ;

LIGPERPV = positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV) * CNRLIG12 * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI) ;

LIGPERPC = positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC) * CNRLIG12 * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI) ;

LIGPERPP = positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP) * CNRLIG12 * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI) ;

LIGPERPCP = positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP)
           * positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC)
           * (1 - positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV))
           * CNRLIG12
           * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
           ;

LIGPERPVP = positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP)
           * positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV)
           * (1 - positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC))
           * CNRLIG12
           * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
           ;

LIGPERPMAR = positif(PERPINDV + PERPINDCV)  * positif(PERPINDAFFV)
             * positif(PERPINDC + PERPINDCC)  * positif(PERPINDAFFC)
             * (1 - positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP))
             * CNRLIG12
             * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
             ;

regle 902160:
application :  iliad ;


ZIG_TITRECRP = positif(BCSG + V_CSANT + CODZRU + CODZRV )
             * positif(BRDS + V_RDANT + CODZRU + CODZRV ) 
             * positif(BPRS + V_PSANT + CODZRU + CODZRV ) * (1 - positif(BCVNSAL + V_CVNANT))
                                       * (1 - (V_CNR * (1 - positif(ZIG_RF + max(0, NPLOCNETSF))))) * LIG2 ;

ZIGTITRECRPS = positif(BCSG + V_CSANT) * positif(BRDS + V_RDANT) * positif(BPRS + V_PSANT) 
                                       * (1 - (V_CNR * (1 - positif(ZIG_RF + max(0, NPLOCNETSF))))) * LIG2 ;

ZIGTITRECRS = positif(BCSG + V_CSANT) * positif(BRDS + V_RDANT) * positif(BCVNSAL + V_CVNANT) * (1 - positif(BPRS + V_PSANT))
                * (1 - V_CNR) * LIG2 ;

ZIGTITRERS = (1 - positif(BCSG + V_CSANT)) * positif(BRDS + V_RDANT) * (1 - positif(BPRS + V_PSANT)) * positif(BCVNSAL + V_CVNANT)
              * (1 - V_CNR) * LIG2 ;

ZIG_TITRECR = positif(BCSG + V_CSANT) * positif(BRDS + V_RDANT) * (1 - positif(BPRS + V_PSANT)) 
              * (1 - V_CNR) * LIG2 ;

ZIG_TITRECP = positif(BCSG + V_CSANT) * (1 - positif(BRDS + V_RDANT)) * positif(BPRS + V_PSANT) * (1 - positif(BCVNSAL + V_CVNANT))
               * (1 - V_CNR) * LIG2 ;

ZIG_TITRERP = (1 - positif(BCSG + V_CSANT)) * positif(BRDS + V_RDANT) * positif(BPRS + V_PSANT) * (1 - positif(BCVNSAL + V_CVNANT))
               * (1 - V_CNR) * LIG2 ;

ZIG_TITREC = positif(BCSG + V_CSANT) * (1 - positif(BRDS + V_RDANT)) * (1 - positif(BPRS + V_PSANT)) * (1 - positif(BCVNSAL + V_CVNANT)) 
              * (1 - V_CNR) * LIG2 ;

ZIG_TITRER = positif(BRDS + V_RDANT) * (1 - positif(BCSG + V_CSANT)) * (1 - positif(BPRS + V_PSANT)) 
              * (1 - V_CNR) * LIG2 ;

ZIGTITRES = positif(BCVNSAL + V_CVNANT) * (1 - positif(BRDS + V_RDANT)) * (1 - positif(BCSG + V_CSANT)) * (1 - positif(BPRS + V_PSANT))
             * LIG2 ;

ZIG_TITREPN = positif(BPRS + V_PSANT) * (1 - V_CNR) * LIG2 ;

regle 902170:
application :  iliad ;

ZIGTITRE = positif((positif(BCSG + V_CSANT + BRDS + V_RDANT + BPRS + V_PSANT) * (1 - (V_CNR * (1 - positif(ZIG_RF+max(0,NPLOCNETSF))))) 
		       + positif (BCVNSAL + V_CVNANT + BCDIS + V_CDISANT)+ positif(CODZRU + CODZRV)
                   ) * TYPE4
		  ) * (1 - positif(ANNUL2042)) ;

ZIGBASECS1 = positif(BCSG + V_CSANT) * positif(INDCTX) ;
ZIGBASERD1 = positif(BRDS + V_RDANT) * positif(INDCTX) ;
ZIGBASEPS1 = positif(BPRS + V_PSANT) * positif(INDCTX) ;
ZIGBASESAL1 = positif(BCVNSAL + V_CVNANT) * positif(INDCTX) ;

regle 902180:
application :  iliad ;

CS_RVT = RDRV ;
RD_RVT = CS_RVT;
PS_RVT = CS_RVT;
IND_ZIGRVT =  0;

ZIG_RVTO = positif (CS_RVT + RD_RVT + PS_RVT) * (1 - positif(ANNUL2042)) * null(3 - INDIRPS) * CNRLIG12 ;

regle 902190:
application :  iliad ;

CS_RCM =  RDRCM ;
RD_RCM = CS_RCM ;
PS_RCM = CS_RCM ;

IND_ZIGRCM = positif(present(RCMABD) + present(RCMAV) + present(RCMHAD) + present(RCMHAB)  
                     + present(RCMTNC) + present(RCMAVFT) + present(REGPRIV)) 
	      * positif(V_NOTRAIT - 20) ;

ZIG_RCM = positif(CS_RCM + RD_RCM + PS_RCM + IND_ZIGRCM)
                   * (1 - positif(ANNUL2042)) * null(3 - INDIRPS) * CNRLIG12 ;

regle 902200:
application :  iliad ;

CS_REVCS = RDNP ;
RD_REVCS = CS_REVCS ;
PS_REVCS = CS_REVCS ;

IND_ZIGPROF = positif(V_NOTRAIT - 20) * positif( present(RCSV)
                     +present(RCSC)
                     +present(RCSP));
ZIG_PROF = positif(CS_REVCS+RD_REVCS+PS_REVCS+IND_ZIGPROF)
           * (1 - positif(ANNUL2042)) * LIG1 * null(3 - INDIRPS) ;


regle 902210:
application :  iliad ;


CS_RFG = RDRFPS ;
RD_RFG = CS_RFG ;
PS_RFG = CS_RFG ;

IND_ZIGRFG = positif(V_NOTRAIT - 20) * positif( present(RFORDI)
                     +present(RFDORD)
                     +present(RFDHIS)
                     +present(RFMIC) );

ZIG_RF = positif(CS_RFG + RD_RFG + PS_RFG + IND_ZIGRFG) * (1 - positif(ANNUL2042)) * LIG1 * LIG2 * null(3 - INDIRPS) ;

regle 902220:
application :  iliad ;


CS_RTF = RDPTP + RDNCP ;
RD_RTF = CS_RTF ;
PS_RTF = CS_RTF ;

IND_ZIGRTF=  positif(V_NOTRAIT - 20) * positif (present (PEA) + present( BPCOPTV ) + present( BPVRCM )) ;

ZIG_RTF = positif (CS_RTF + RD_RTF + PS_RTF + IND_ZIGRTF)
                   * (1 - positif(ANNUL2042)) * null(3 - INDIRPS) * CNRLIG12 ;

ZIGGAINLEV = positif(CVNSALC) * positif(CVNSALAV) * LIG1 * LIG2 ;

regle 902230:
application :  iliad ;


CS_REVETRANG = 0 ;
RD_REVETRANG = SALECS + SALECSG + ALLECS + INDECS + PENECS 
             + COD8SA + COD8SB + COD8SC + COD8SW + COD8SX + COD8PH;
PS_REVETRANG = 0 ;


ZIG_REVETR = positif(SALECS + SALECSG + ALLECS + INDECS + PENECS 
                     + COD8SA + COD8SB + COD8SC + COD8SW + COD8SX + COD8PH)
                   * CNRLIG12 ;

regle 902240:
application :  iliad ;


CS_RVORIGND =   ESFP;
RD_RVORIGND =   ESFP;
PS_RVORIGND =   ESFP;

IND_ZIGREVORIGIND = present(ESFP) ;

ZIG_RVORIGND = positif (CS_RVORIGND + RD_RVORIGND + PS_RVORIGND
                         + IND_ZIGREVORIGIND)
                   * CNRLIG12 ;

regle 902250:
application :  iliad ;

CS_RE168 = RE168 ;
RD_RE168 = RE168 ;
PS_RE168 = RE168 ;

CS_TAX1649 = TAX1649 ;
RD_TAX1649 = TAX1649 ;
PS_TAX1649 = TAX1649 ;

CS_R1649 = R1649 ;
RD_R1649 = R1649 ;
PS_R1649 = R1649 ;

CS_PREREV = PREREV ;
RD_PREREV = PREREV ;
PS_PREREV = PREREV ;

ZIGRE168 = positif(RE168) * (1 - V_CNR) * LIG2 ;
ZIGTAX1649 = positif(TAX1649) * (1 - V_CNR) * LIG2 ;

ZIGR1649 = positif(R1649) * CNRLIG12 ;
ZIGPREREV = positif(PREREV) * CNRLIG12 ;

regle 902255:
application :  iliad ;

REVNONASSU = CODZRU + CODZRV ;
ZIGNONASSU = positif(CODZRU + CODZRV + 0) * LIG2 * LIG1 ;

regle 902260:
application :  iliad ;
 

LIGPS = positif(BCSG + BRDS + BPRS + BCVNSAL + BCDIS 
                + BGLOA + BRSE1 + BRSE2 + BRSE3 + BRSE4 + BRSE5 + BRSE6 + BRSE7 + 0
                + (CODZRU + CODZRV) * LIG1
               ) 
	 * (1 - positif(ANNUL2042)) ;

LIGPSP = 1 - (null(LIGPS) * null(V_ANTCR)) ;

NONLIGPS = positif(positif(1 - LIGPS) + positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63))) ;

INDIRPS =  (1 * (1 - LIGPS) * positif(3 - ANTINDIRPS))
	 + (3 * (1- positif((1 - LIGPS) * positif(3 - ANTINDIRPS)))) ;

regle 902270:
application :  iliad ;


CS_BASE = BCSG ;
RD_BASE = BRDS ;
PS_BASE = BPRS ;

ZIGBASECS = positif(BCSG + V_CSANT + CODZRU + CODZRV) ;
ZIGBASERD = positif(BRDS + V_RDANT + CODZRU + CODZRV) ;
ZIGBASEPS = positif(BPRS + V_PSANT + CODZRU + CODZRV) ;
ZIGBASECVN = positif(BCVNSAL + V_CVNANT) ;
ZIG_BASE = positif(BCSG + BRDS + BPRS + BPSOL + V_CSANT + V_RDANT + V_PSANT + CODZRU + CODZRV) * LIG2 ;
ZIGCDIS = positif(BCDIS + V_CDISANT) * LIG2 ;
ZIGPVTER = positif(PVTERPS) * LIG2 ;
ZIGGLOA = positif(BGLOA) * (1 - V_CNR) * LIG2 ;
ZIGGLOANR = positif(BGLOACNR) * LIG2 ;
ZIGGLOALL = positif(ZIGGLOA + ZIGGLOANR) * LIG2 ;
ZIGRSE1 = positif(BRSE1) * LIG2 ; 
ZIGRSE2 = positif(BRSE2) * LIG2 ;
ZIGRSE3 = positif(BRSE3) * LIG2 ;
ZIGRSE4 = positif(BRSE4) * LIG2 ;
ZIGRSE5 = positif(BRSE5) * LIG2 ;
ZIGRSE6 = positif(BRSE6) * LIG2 ;
ZIGRSE7 = positif(BRSE7) * LIG2 ;
ZIGCSG820 = positif(BCSG820) * LIG2 ;


ZIGRFRET = positif(COD8YK) * LIG2 ;
ZIGRFDEP = positif(COD8XK) * (1 - positif(CODZRA)) * LIG2 ;
ZIGRFZRA = positif(COD8XK) * positif(CODZRA) * LIG2 ;

regle 902280:
application :  iliad ;

ZIG_TAUXCRP  = ZIG_TITRECRP ;
ZIG_TAUXCR   = ZIG_TITRECR ;
ZIG_TAUXCP   = ZIG_TITRECP ;
ZIG_TAUXRP   = ZIG_TITRERP ;
ZIG_TAUXR    = ZIG_TITRER ;
ZIGTAUX1 = ZIGTITRECRPS ;
ZIGTAUX3 = ZIGTITRECRS ;
ZIGTAUX4 = ZIG_TITRECR ;

regle 902290:
application :  iliad ;

ZIGMONTS = positif(BCVNSAL + V_CVNANT) ;
ZIGMONTCS = positif(BCSG + V_CSANT + CODZRU + CODZRV) ;
ZIGMONTRD = positif(BRDS + V_RDANT + CODZRU + CODZRV) ;
ZIGMONTPS = positif(BPRS + V_PSANT + CODZRU + CODZRV) ;
ZIG_MONTANT = positif (BCSG + BRDS + BPRS + BPSOL + V_CSANT + V_RDANT + V_PSANT + CODZRU + CODZRV) * (1 - positif(ANNUL2042)) ;

regle 902300:
application :  iliad ;


ZIG_INT =  positif (RETCS + RETRD + RETPS + RETPSOL) * LIG2 ;

ZIGCVN = positif(RETCVN) * LIG2 ;

ZIGINT = positif(RETCDIS) * LIG2 ;

ZIGLOA = positif(RETGLOA) * LIG2 ;

ZIGINT1 = positif(RETRSE1) * LIG2 ;
ZIGINT2 = positif(RETRSE2) * LIG2 ;
ZIGINT3 = positif(RETRSE3) * LIG2 ;
ZIGINT4 = positif(RETRSE4) * LIG2 ;
ZIGINT5 = positif(RETRSE5) * LIG2 ;
ZIGINT6 = positif(RETRSE6) * LIG2 ;
ZIGINT7 = positif(RETRSE7) * LIG2 ;

ZIGINT820 = positif(RETCSG820) * LIG2 ;

ZIGCVN22 = positif(RETCVN22) ;
ZIGINT22 = positif(RETCDIS22) ;
ZIGLOA22 = positif(RETGLOA22) ;

ZIGINT122 = positif(RETRSE122) * LIG2 ;
ZIGINT222 = positif(RETRSE222) * LIG2 ;
ZIGINT322 = positif(RETRSE322) * LIG2 ;
ZIGINT422 = positif(RETRSE422) * LIG2 ;
ZIGINT522 = positif(RETRSE522) * LIG2 ;
ZIGINT622 = positif(RETRSE622) * LIG2 ;
ZIGINT722 = positif(RETRSE722) * LIG2 ;

regle 902310:
application :  iliad ;

ZIG_PEN17281 = ZIG_PENAMONT * positif(NMAJC1 + NMAJR1 + NMAJP1) * LIG2 ;

ZIG_PENATX4 = ZIG_PENAMONT * positif(NMAJC4 + NMAJR4 + NMAJP4) * LIG2 ;
ZIG_PENATAUX = ZIG_PENAMONT * positif(NMAJC1 + NMAJR1 + NMAJP1) * LIG2 ;

ZIGNONR30 = positif(ZIG_PENATX4) * positif(1 - positif(R1649 + PREREV)) * LIG2 ;
ZIGR30 = positif(ZIG_PENATX4) * positif(R1649 + PREREV) * LIG2 ;

ZIGPENACVN = positif(PCVN) * positif(NMAJCVN1) * LIG2 ;
ZIGPENACDIS = positif(PCDIS) * positif(NMAJCDIS1) * LIG2 ;

ZIGPENAGLO1 = positif(PGLOA) * positif(NMAJGLO1) * LIG2 ;

ZIGPENARSE1 = positif(PRSE1) * positif(NMAJRSE11) * LIG2 ;
ZIGPENARSE2 = positif(PRSE2) * positif(NMAJRSE21) * LIG2 ;
ZIGPENARSE3 = positif(PRSE3) * positif(NMAJRSE31) * LIG2 ;
ZIGPENARSE4 = positif(PRSE4) * positif(NMAJRSE41) * LIG2 ;
ZIGPENARSE5 = positif(PRSE5) * positif(NMAJRSE51) * LIG2 ;
ZIGPENARSE6 = positif(PRSE6) * positif(NMAJRSE61) * LIG2 ;
ZIGPENARSE7 = positif(PRSE7) * positif(NMAJRSE71) * LIG2 ;

ZIGPENA8201 = positif(PCSG820) * positif(NMAJC8201) * LIG2 ;

ZIGPENACVN4 = positif(PCVN) * positif(NMAJCVN4) * LIG2 ;
ZIGPENACDIS4 = positif(PCDIS) * positif(NMAJCDIS4) * LIG2 ;

ZIGPENAGLO4 = positif(PGLOA) * positif(NMAJGLO4) * LIG2 ;

ZIGPENARSE14 = positif(PRSE1) * positif(NMAJRSE14) * LIG2 ;
ZIGPENARSE24 = positif(PRSE2) * positif(NMAJRSE24) * LIG2 ;
ZIGPENARSE34 = positif(PRSE3) * positif(NMAJRSE34) * LIG2 ;
ZIGPENARSE44 = positif(PRSE4) * positif(NMAJRSE44) * LIG2 ;
ZIGPENARSE54 = positif(PRSE5) * positif(NMAJRSE54) * LIG2 ;
ZIGPENARSE64 = positif(PRSE6) * positif(NMAJRSE64) * LIG2 ;
ZIGPENARSE74 = positif(PRSE7) * positif(NMAJRSE74) * LIG2 ;

ZIGPENA8204 = positif(PCSG820) * positif(NMAJC8204) * LIG2 ;

regle 902320:
application :  iliad ;

ZIG_PENAMONT = positif(PCSG + PRDS + PPRS + PPSOL) * LIG2 ;

regle 902330:
application :  iliad ;

ZIG_CRDETR = positif(CICSG + CIRDS + CIPRS + CIPSOL) * LIG2 ;

regle 902340 :
application :  iliad ;

ZIGCIGLOA = positif(CIGLOA) * TYPE2 ;

ZIGCOD8YL = positif(COD8YL) * TYPE2;
CGLOAPROV = COD8YL * (1-V_CNR) ;
ZIGCOD8YT = positif(COD8YT) * TYPE2;
ZIGCDISPROV = positif(CDISPROV) * TYPE2 ;

ZIGREVXA = positif(REVCSXA) * TYPE2 ;

ZIGREVXB = positif(REVCSXB) * TYPE2 ;
ZIGREVXC = positif(REVCSXC + COD8XI) * TYPE2 ;

ZIGREVXD = positif(REVCSXD) * TYPE2 ;
ZIGREVXE = positif(REVCSXE + COD8XJ) * TYPE2 ;

ZIGPROVYD = positif(CSPROVYD) * TYPE2 ;

ZIGPROVYE = positif(CSPROVYE) * TYPE2 ;

ZIGPROVYF = positif(CSPROVYF + CSPROVYN) * TYPE2 ;
CSPROVRSE2 = CSPROVYF + CSPROVYN ;

ZIGPROVYG = positif(CSPROVYG) * TYPE2 ;

ZIGPROVYH = positif(CSPROVYH + CSPROVYP) * TYPE2 ;
CSPROVRSE4 = CSPROVYH + CSPROVYP ;

ZIGCIRSE6 = positif(REVCSXB+REVCSXC+COD8XI) * TYPE2 ;

ZIGPROVYQ = positif ( COD8YQ ) * TYPE2;

regle 902350 :
application :  iliad ;

ZIG_CTRIANT = positif(V_ANTCR) * TYPE2 ;

ZIGCSANT = positif(V_CSANT) * TYPE2* (1 - APPLI_OCEANS) ;

ZIGRDANT = positif(V_RDANT) * TYPE2 * (1 - APPLI_OCEANS);

ZIGPSANT = positif(V_PSANT) * TYPE2 * (1 - APPLI_OCEANS);

ZIGCVNANT = positif(V_CVNANT) * TYPE2 * (1 - APPLI_OCEANS);

ZIGREGVANT = positif(V_REGVANT) * TYPE2 * (1 - APPLI_OCEANS);

ZIGCDISANT = positif(V_CDISANT) * TYPE2 * (1 - APPLI_OCEANS);

ZIGLOANT = positif(V_GLOANT) * TYPE2 * (1 - APPLI_OCEANS);

ZIGRSE1ANT = positif(V_RSE1ANT) * TYPE2 * (1 - APPLI_OCEANS);
ZIGRSE2ANT = positif(V_RSE2ANT) * TYPE2 * (1 - APPLI_OCEANS);
ZIGRSE3ANT = positif(V_RSE3ANT) * TYPE2 * (1 - APPLI_OCEANS);
ZIGRSE4ANT = positif(V_RSE4ANT) * TYPE2 * (1 - APPLI_OCEANS);
ZIGRSE5ANT = positif(V_RSE5ANT) * TYPE2 * (1 - APPLI_OCEANS);
ZIGRSE6ANT = positif(V_RSE6ANT) * TYPE2 * (1 - APPLI_OCEANS);
ZIGRSE7ANT = positif(V_RSE7ANT) * TYPE2 * (1 - APPLI_OCEANS);

regle 902360:
application :  iliad ;


ZIG_CTRIPROV = positif(CSGIM + CRDSIM + PRSPROV + PROPSOL) * LIG2 ;

regle 902370:
application :  iliad ;


IND_COLC = positif(BCSG) * positif(PCSG + CICSG + CSGIM) ;

IND_COLR = positif(BRDS) * positif(PRDS + CIRDS + CRDSIM) ;

IND_COLP = positif(BPRS) * positif(PPRS + CIPRS + PRSPROV) ;

INDCOLSOL = positif(BPSOL) * positif(PPSOL + CIPSOL + PROPSOL) ;

INDCOLVN = positif(BCVNSAL) * positif(PCVN + CICVN + COD8YT) ;

INDCOL = positif(IND_COLC + IND_COLR + IND_COLP + INDCOLSOL) ;

IND_COLD = positif(BCDIS) * positif(PCDIS + CDISPROV) ;

INDGLOA = positif(BGLOA) * positif(PGLOA + COD8YL + COD8XM) ;

INDCSG820 = positif(BCSG820) * positif(PCSG820) ;

INDRSE1 = positif(BRSE1) * positif(PRSE1 + CIRSE1 + CSPROVYD) ;
INDRSE2 = positif(BRSE2) * positif(PRSE2 + CIRSE2 + CSPROVYF + CSPROVYN) ;
INDRSE3 = positif(BRSE3) * positif(PRSE3 + CIRSE3 + CSPROVYG) ;
INDRSE4 = positif(BRSE4) * positif(PRSE4 + CIRSE4 + CSPROVYH + CSPROVYP) ;
INDRSE5 = positif(BRSE5) * positif(PRSE5 + CIRSE5 + CSPROVYE) ;
INDRSE6 = positif(BRSE6) * positif(PRSE6 + CIRSE6 + COD8YQ) ;
INDRSE7 = positif(BRSE7) * positif(PRSE7) ;

IND_CTXC = positif(CS_DEG) * positif(BCSG) * positif(INDCTX) ;

IND_CTXR = positif(CS_DEG) * positif(BRDS) * positif(INDCTX) ;

IND_CTXP = positif(CS_DEG) * positif(BPRS) * positif(INDCTX) ;

IND_CTXD = positif(CS_DEG) * positif(BCDIS) * positif(INDCTX) ;

INDTRAIT = null(5 - V_IND_TRAIT) ;

INDT = positif(IND_COLC + IND_COLR + IND_COLP + IND_COLS + IND_CTXC + IND_CTXR + IND_CTXP + IND_CTXS) 
	* INDTRAIT ;

INDTD = positif(IND_COLD + IND_CTXD) * INDTRAIT ;

regle 902380:
application :  iliad ;

ZIG_NETAP =  positif (BCSG  + BRDS + BPRS + BCVNSAL + BCDIS 
                      + BGLOA + BRSE1 + BRSE2 + BRSE3 + BRSE4 
                      + BRSE5  + BRSE6 + BRSE7 + CODZRU + CODZRV)
             * LIG2 ;

regle 902390:
application :  iliad ;

ZIG_TOTDEG = positif (CRDEG) * positif(INDCTX) ;

ZIG_TITREP = ZIG_NETAP + ZIG_TOTDEG ;

ZIGANNUL = positif(INDCTX) * positif(ANNUL2042) ;

regle 902400:
application :  iliad ;

ZIG_INF8 = positif (CS_DEG) * positif (SEUIL_8 - CS_DEG) * LIG2 ;

regle 902410:
application :  iliad ;

ZIG_REMPLACE  = positif((1 - positif(20 - V_NOTRAIT)) 
               * positif(ANNEEREV - V_0AX)
               * positif(ANNEEREV - V_0AZ)
               * positif(ANNEEREV - V_0AY) + positif(V_NOTRAIT - 20))
               * LIG2 ;

regle 902420:
application :  iliad ;


ZIG_DEGINF61 = positif(V_CSANT+V_RDANT+V_PSANT+0)  
               * positif(CRDEG+0)
               * positif(SEUIL_61-TOTCRA-CSNET-RDNET-PRSNET-CDISNET+0)
               * (1 - null(CSTOT+0))
               * LIG2 ;

regle 902430:
application :  iliad ;


ZIG_CSGDDO = positif(V_IDANT - DCSGD) * positif(IDCSG) * (1 - null(V_IDANT + DCSGD + 0)) * (1 - null(V_IDANT - DCSGD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIG_CSGDRS = positif(DCSGD - V_IDANT) * positif(IDCSG) * (1 - null(V_IDANT + DCSGD + 0)) * (1 - null(V_IDANT - DCSGD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIG_CSGDC = positif(ZIG_CSGDDO + ZIG_CSGDRS + null(V_IDANT - DCSGD)) * (1 - null(V_IDANT + DCSGD + 0)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIG_CSGDCOR = positif(ZIG_CSGDDO + ZIG_CSGDRS) * (1 - null(V_IDANT + DCSGD + 0)) * (1 - null(V_IDANT - DCSGD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGCSGDCOR1 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGCSGDCOR2 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGCSGDCOR3 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGCSGDCOR4 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGCSGDCOR5 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGCSGDCOR6 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 6)) ;
ZIGCSGDCOR7 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 7)) ;

regle 902440:
application :  iliad ;

ZIGLODD = positif(V_IDGLOANT - DGLOD) * positif(IDGLO) * (1 - null(V_IDGLOANT + DGLOD + 0)) * (1 - null(V_IDGLOANT - DGLOD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLORS = positif(DGLOD - V_IDGLOANT) * positif(IDGLO) * (1 - null(V_IDGLOANT + DGLOD + 0)) * (1 - null(V_IDGLOANT - DGLOD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLOCO = positif(ZIGLODD + ZIGLORS + null(V_IDGLOANT - DGLOD)) * (1 - null(V_IDGLOANT + DGLOD + 0)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLOCOR = positif(ZIGLODD + ZIGLORS) * (1 - null(V_IDGLOANT + DGLOD + 0)) * (1 - null(V_IDGLOANT - DGLOD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLOCOR1 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGLOCOR2 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGLOCOR3 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGLOCOR4 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGLOCOR5 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGLOCOR6 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 6)) ;
ZIGLOCOR7 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 7)) ;

ZIGRSEDD = positif(V_IDRSEANT - DRSED) * positif(IDRSE) * (1 - null(V_IDRSEANT + DRSED + 0)) 
	    * (1 - null(V_IDRSEANT - DRSED)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSERS = positif(DRSED - V_IDRSEANT) * positif(IDRSE) * (1 - null(V_IDRSEANT + DRSED + 0)) 
	    * (1 - null(V_IDRSEANT - DRSED)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSECO = positif(ZIGRSEDD + ZIGRSERS + null(V_IDRSEANT - DRSED)) * (1 - null(V_IDRSEANT + DRSED + 0)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSECOR = positif(ZIGRSEDD + ZIGRSERS) * (1 - null(V_IDRSEANT + DRSED + 0)) * (1 - null(V_IDRSEANT - DRSED)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSECOR1 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGRSECOR2 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGRSECOR3 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGRSECOR4 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGRSECOR5 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGRSECOR6 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 6)) ;
ZIGRSECOR7 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 7)) ;

regle 902450:
application :  iliad ;

                 
ZIG_PRIM = positif(NAPCR) * LIG2 ;

regle 902460:
application :  iliad ;


CS_BPCOS = RDNCP ;
RD_BPCOS = CS_BPCOS ;
PS_BPCOS = CS_BPCOS ;

ZIG_BPCOS = positif(CS_BPCOS + RD_BPCOS + PS_BPCOS) * CNRLIG12 ;

regle 902470:
application :  iliad ;


ANCSDED2 = (V_ANCSDED * (1 - null(933 - V_ROLCSG)) + (V_ANCSDED + 1) * null(933 - V_ROLCSG)) * positif(V_ROLCSG + 0)
           + V_ANCSDED * (1 - positif(V_ROLCSG + 0)) ;

ZIG_CSGDPRIM = (1 - positif(V_CSANT + V_RDANT + V_PSANT + V_IDANT)) * positif(IDCSG) * LIG2 ;

ZIG_CSGD99 = ZIG_CSGDPRIM * (1 - null(V_IDANT + DCSGD + 0)) * positif(DCSGD) * positif(20 - V_NOTRAIT) * LIG2 ;

ZIGDCSGD1 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 1)) * LIG2 ;
ZIGDCSGD2 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 2)) * LIG2 ;
ZIGDCSGD3 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 3)) * LIG2 ;
ZIGDCSGD4 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 4)) * LIG2 ;
ZIGDCSGD5 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 5)) * LIG2 ;
ZIGDCSGD6 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 6)) * LIG2 ;

ZIGIDGLO = positif(IDGLO) * (1 - null(V_IDGLOANT + DGLOD + 0))  * positif(20 - V_NOTRAIT) * LIG2 ;

ZIGIDGLO1 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGIDGLO2 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGIDGLO3 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGIDGLO4 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGIDGLO5 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGIDGLO6 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 6)) ;

ZIGIDRSE = positif(IDRSE) * (1 - null(V_IDRSEANT + DRSED + 0)) * positif(20 - V_NOTRAIT) * LIG2 ;

ZIGDRSED1 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGDRSED2 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGDRSED3 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGDRSED4 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGDRSED5 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGDRSED6 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 6)) ;

regle 902480:
application :  iliad ;

ZIGINFO = positif(ZIG_CSGD99 + ZIGIDGLO + ZIGIDRSE + ZIG_CSGDC + ZIGLOCO + ZIGRSECO) ;

regle 902490:
application :  iliad ;


LIGPVSURSI = positif(PVSURSI + CODRWA) * CNRLIG12 ;

LIGIREXITI = positif(IREXITI) * (1 - positif(IREXITS)) * CNRLIG12 ;

LIGIREXITS = positif(IREXITS) * (1 - positif(IREXITI)) * CNRLIG12 ;

LIGIREXIT = positif(PVIMPOS + CODRWB) * positif(PVSURSI + CODRWA) * CNRLIG12 ;

LIGPV3WBI = positif(PVIMPOS + CODRWB) * CNRLIG12 ;

LIG150BTER = positif(COD3TA + COD3TB) ;

LIGSURIMP = positif(SURIMP) * LIG1 * LIG2 ;

LIG_SURSIS = positif( LIGPVSURSI + LIGPV3WBI + LIG150BTER + LIGIREXITI
                      + LIGIREXITS + LIGIREXIT + LIGSURIMP ) * LIG1 * LIG2 ; 

LIGSUR = LIG_SURSIS * positif(LIGREPPLU + LIGABDET + LIGABDETP + LIGRCMSOC + LIGCOD3SG
                              + LIGCOD3SL + LIGPV3SB + LIGCOD3WH + LIGRCMIMPAT + LIGABIMPPV + LIGABIMPMV + LIGROBOR
                              + LIGPVIMMO + LIGPVTISOC + LIG3TZ + LIGMOBNR + LIGDEPCHO + LIGDEPMOB + LIGZRS) ;

LIGI017 = positif(PVSURSI + PVIMPOS + CODRWA + CODRWB) * V_IND_TRAIT ;

regle 902500:
application :  iliad ;


LIG_CORRECT = positif_ou_nul(IAMD2) * INDREV1A8 * LIG1 * LIG2 ;

regle 902510:
application :  iliad ;


LIG_R8ZT = positif(V_8ZT + 0) * LIG1 * LIG2 ;

regle 902520:
application :  iliad ;

                 
LIGTXMOYPOS = positif(present(RMOND)+positif(VARRMOND)*present(DEFZU)) * (1 - positif(DEFRIMOND)) * LIG1 * LIG2 ;

LIGTXMOYNEG = positif(DMOND) * (1 - positif(DEFRIMOND)) * LIG1 * LIG2 ;

LIGTXPOSYT = positif(VARRMOND) * positif(IPTXMO) * positif(DEFRIMOND) * LIG1 * LIG2 ;

LIGTXNEGYT = (1-positif(LIGTXPOSYT)) * positif(VARDMOND) * positif(IPTXMO) * positif(DEFRIMOND) * LIG1 * LIG2 ;

regle 902530:
application :  iliad ;

                 
LIGAMEETREV = positif(INDREV1A8) * LIG1 * LIG2 ;

regle 902540:
application : iliad  ;

LIGBICTOT = positif(MIBNPRV + MIBNPRC + MIBNPRP + present(MIBNPPVV) + present(MIBNPPVC) 
                    + present(MIBNPPVP) + present(MIBNPDCT) + present(COD5RZ) + present(COD5SZ)) ;
                 
LIGMIBNPPOS = positif_ou_nul(MIBNETNPTOT) * (1 - positif(LIGBICNP + DEFNP)) * LIG2 ;

LIGMIBNPNEG = 1 - LIGMIBNPPOS ;

LIG_MIBP = positif(somme(i=V,C,P:MIBVENi) + somme(i=V,C,P:MIBPRESi) + abs(MIB_NETCT) + 0) ; 

LIGMIBPPOS = positif_ou_nul(MIBNETPTOT) * (1 - LIGBICPRO) * LIG2 ;

LIGMIBPNEG = 1 - LIGMIBPPOS ;

regle 902550:
application : iliad  ;

LIGSPENP = positif(BNCNPV + BNCNPC + BNCNPP + LIGBNCNPPV + LIGBNCNPMV) * LIG2 ;
                 
LIGSPENPPOS = positif_ou_nul(SPENETNPF) * (1 - positif(LIGNOCEP + LIGDAB)) * LIG2 ;

LIGSPENPNEG = 1 - LIGSPENPPOS ;




regle 902570:
application : iliad  ;



LIGREPTOUR = positif(INVLOCXN + COD7UY) * LIG1 ;

LIGLOGDOM = positif(DLOGDOM) * LIG1 * LIG2 ;

LIGLOGSOC = positif(DLOGSOC) * LIG1 * LIG2 ;

LIGDOMSOC1 = positif(DDOMSOC1) * LIG1 * LIG2 ;

LIGLOCENT = positif(DLOCENT) * LIG1 * LIG2 ;

LIGCOLENT = positif(DCOLENT) * LIG1 * LIG2 ;

LIGREPHA  = positif(INVLOCXV + COD7UZ) * LIG1 * LIG2 ;

LIGREHAB = positif(DREHAB) * LIG1 * LIG2 ;

LIGRESTIMO1 = positif(DRESTIMO1) * LIG1 * LIG2 ;

regle 902580:
application : iliad  ;

LIGREDAGRI = positif(DDIFAGRI) * LIG1 * LIG2 ;
LIGFORET = positif(DFORET) * LIG1 * LIG2 ;
LIGCOTFOR = positif(DCOTFOR) * LIG1 * LIG2 ; 

LIGRCIF = positif(REPCIF + REPCIFHSN1 + REPCIFHSN2 + REPCIFHSN3) * LIG1 * LIG2 ;
LIGREP7UX = positif(REPCIFHSN3) * LIG1 * LIG2 ;
LIGREP7VM = positif(REPCIFHSN2) * LIG1 * LIG2 ;
LIGREP7VQ = positif(REPCIFHSN1) * LIG1 * LIG2 ;
LIGREP7UP = positif(REPCIF) * LIG1 * LIG2 ;

LIGRCIFAD = positif(REPCIFAD + REPCIFADHSN1 + REPCIFADHSN2 + REPCIFADHSN3) * LIG1 * LIG2 ;
LIGREP7VP = positif(REPCIFADHSN3) * LIG1 * LIG2 ;
LIGREP7VN = positif(REPCIFADHSN2) * LIG1 * LIG2 ;
LIGREP7VR = positif(REPCIFADHSN1) * LIG1 * LIG2 ;
LIGREP7UA = positif(REPCIFAD) * LIG1 * LIG2 ;

LIGRCIFSIN = positif(REPCIFSIN + REPCIFSN1 + REPCIFSN2 + REPCIFSN3) * LIG1 * LIG2 ;
LIGREP7TJ = positif(REPCIFSN3) * LIG1 * LIG2 ;
LIGREP7TM = positif(REPCIFSN2) * LIG1 * LIG2 ;
LIGREP7TP = positif(REPCIFSN1) * LIG1 * LIG2 ;
LIGREP7UT = positif(REPCIFSIN) * LIG1 * LIG2 ;

LIGRCIFADSN = positif(REPCIFADSIN + REPCIFADSSN1 + REPCIFADSSN2 + REPCIFADSSN3) * LIG1 * LIG2 ;
LIGREP7TK = positif(REPCIFADSSN3) * LIG1 * LIG2 ;
LIGREP7TO = positif(REPCIFADSSN2) * LIG1 * LIG2 ;
LIGREP7TQ = positif(REPCIFADSSN1) * LIG1 * LIG2 ;
LIGREP7UB = positif(REPCIFADSIN) * LIG1 * LIG2 ;

LIGNFOREST = positif(REPSIN + REPFORSIN + REPFORSIN2 + REPFORSIN3 + REPNIS) * LIG1 * LIG2 ;
LIGREPSIN = positif(REPSIN) * LIG1 * LIG2 ;
LIGSINFOR = positif(REPFORSIN) * LIG1 * LIG2 ;
LIGSINFOR2 = positif(REPFORSIN2) * LIG1 * LIG2 ;
LIGSINFOR3 = positif(REPFORSIN3) * LIG1 * LIG2 ;
LIGREPNIS = positif(REPNIS) * LIG1 * LIG2 ;

LIGFIPC = positif(DFIPC) * LIG1 * LIG2 ;
LIGFIPDOM = positif(DFIPDOM) * LIG1 * LIG2 ;
LIGPRESSE = positif(DPRESSE) * LIG1 * LIG2 ;
LIGCINE = positif(DCINE) * LIG1 * LIG2 ;
LIGRIRENOV = positif(DRIRENOV) * LIG1 * LIG2 ;
LIGREPAR = positif(NUPROPT) * LIG1 * LIG2 ;
LIGREPREPAR = positif(REPNUREPART) * CNRLIG12 ;
LIGREPARN = positif(REPAR) * CNRLIG12 ;
LIGREPAR1 = positif(REPAR1) * CNRLIG12 ;
LIGREPAR2 = positif(REPAR2) * CNRLIG12 ;
LIGREPAR3 = positif(REPAR3) * CNRLIG12 ;
LIGREPAR4 = positif(REPAR4) * CNRLIG12 ;
LIGREPAR5 = positif(REPAR5) * CNRLIG12 ;
LIGREPAR6 = positif(REPAR6) * CNRLIG12 ;
LIGREPAR7 = positif(REPAR7) * CNRLIG12 ;
LIGREPAR8 = positif(REPAR8) * CNRLIG12 ;

LIGRESTIMO = (present(COD7SY) + present(COD7SX) + present(COD7NX) + present(COD7NY) ) * LIG1 * LIG2 ;

LIGRCODOU = positif(COD7OU + 0) * CNRLIG12 ;
LIGOU1 = LIGRCODOU * null(RCODOU1 - RCODOU8) ;
LIGOU2 = LIGRCODOU * (1 - null(RCODOU1 - RCODOU8)) ;

LIGRCODOX = positif(COD7OX + 0) * CNRLIG12 ;
LIGOX1 = LIGRCODOX * null(RCODOX1 - RCODOX8) ;
LIGOX2 = LIGRCODOX * (1 - null(RCODOX1 - RCODOX8)) ;

LIGRCODOW = positif(COD7OW + 0) * CNRLIG12 ;
LIGOW1 = LIGRCODOW * null(RCODOW1 - RCODOW8) ;
LIGOW2 = LIGRCODOW * (1 - null(RCODOW1 - RCODOW8)) ;

LIGRCODOV = positif(COD7OV + 0) * CNRLIG12 ;
LIGOV1 = LIGRCODOV * null(RCODOV1 - RCODOV8) ;
LIGOV2 = LIGRCODOV * (1 - null(RCODOV1 - RCODOV8)) ;

LIGRCODJT = positif(LOCMEUBJT + 0) * positif(RCODJT1 + RCODJT8) * CNRLIG12 ;
LIGJT1 = LIGRCODJT * null(RCODJT1 - RCODJT8) ;
LIGJT2 = LIGRCODJT * (1 - null(RCODJT1 - RCODJT8)) ;

LIGRLOCIDFG = positif(LOCMEUBID) * positif(RLOCIDFG1 + RLOCIDFG8) * CNRLIG12 ;
LIGIDFG1 = LIGRLOCIDFG * null(RLOCIDFG1 - RLOCIDFG8) ;
LIGIDFG2 = LIGRLOCIDFG * (1 - null(RLOCIDFG1 - RLOCIDFG8)) ;

LIGNEUV = positif(LOCRESINEUV + INVNPROF1) * positif(RESINEUV1 + RESINEUV8) * CNRLIG12 ;
LIGNEUV1 = LIGNEUV * null(RESINEUV1 - RESINEUV8) ;
LIGNEUV2 = LIGNEUV * (1 - null(RESINEUV1 - RESINEUV8)) ;

LIGVIEU = positif(RESIVIEU) * positif(RESIVIEU1 + RESIVIEU8) * CNRLIG12 ;
LIGIM1 = LIGVIEU * null(RESIVIEU1 - RESIVIEU8) ;
LIGIM2 = LIGVIEU * (1 - null(RESIVIEU1 - RESIVIEU8)) ;

LIGREPLOC15 = positif(REPMEUPE + REPMEUPJ + REPMEUPO + REPMEUPT + REP13MEU) * CNRLIG12 ;
LIGREP7PE = positif(REPMEUPE) * CNRLIG12 ;
LIGREP7PJ = positif(REPMEUPJ) * CNRLIG12 ;
LIGREP7PO = positif(REPMEUPO) * CNRLIG12 ;
LIGREP7PT = positif(REPMEUPT) * CNRLIG12 ;
LIGREP13MEU = positif(REP13MEU) * CNRLIG12 ;

LIGREPLOC12 = positif(REPMEUJS + REPMEUPD + REPMEUPI + REPMEUPN + REPMEUPS + REP12MEU) * CNRLIG12 ;
LIGREP7JS = positif(REPMEUJS) * CNRLIG12 ;
LIGREP7PD = positif(REPMEUPD) * CNRLIG12 ;
LIGREP7PI = positif(REPMEUPI) * CNRLIG12 ;
LIGREP7PN = positif(REPMEUPN) * CNRLIG12 ;
LIGREP7PS = positif(REPMEUPS) * CNRLIG12 ;
LIGREP12MEU = positif(REP12MEU) * CNRLIG12 ;

LIGREPLOC11 = positif(REPMEUIZ + REPMEUJI + REPMEUPC + REPMEUPH + REPMEUPM + REPMEUPR + REP11MEU) * CNRLIG12 ;
LIGREP7IZ = positif(REPMEUIZ) * CNRLIG12 ;
LIGREP7JI = positif(REPMEUJI) * CNRLIG12 ;
LIGREP7PC = positif(REPMEUPC) * CNRLIG12 ;
LIGREP7PH = positif(REPMEUPH) * CNRLIG12 ;
LIGREP7PM = positif(REPMEUPM) * CNRLIG12 ;
LIGREP7PR = positif(REPMEUPR) * CNRLIG12 ;
LIGREP11MEU = positif(REP11MEU) * CNRLIG12 ;

LIGREPLOC10 = positif(REPMEUIH + REPMEUJC + REPMEUPB + REPMEUPG + REPMEUPL + REPMEUPQ + REP10MEU) * CNRLIG12 ;
LIGREP7IH = positif(REPMEUIH) * CNRLIG12 ;
LIGREP7JC = positif(REPMEUJC) * CNRLIG12 ;
LIGREP7PB = positif(REPMEUPB) * CNRLIG12 ;
LIGREP7PG = positif(REPMEUPG) * CNRLIG12 ;
LIGREP7PL = positif(REPMEUPL) * CNRLIG12 ;
LIGREP7PQ = positif(REPMEUPQ) * CNRLIG12 ;
LIGREP10MEU = positif(REP10MEU) * CNRLIG12 ;

LIGREPLOC9 = positif(REPMEUIX + REPMEUIY + REPMEUPA + REPMEUPF + REPMEUPK + REPMEUPP + REP9MEU) * CNRLIG12 ;
LIGREP7IX = positif(REPMEUIX) * CNRLIG12 ;
LIGREP7IY = positif(REPMEUIY) * CNRLIG12 ;
LIGREP7PA = positif(REPMEUPA) * CNRLIG12 ;
LIGREP7PF = positif(REPMEUPF) * CNRLIG12 ;
LIGREP7PK = positif(REPMEUPK) * CNRLIG12 ;
LIGREP7PP = positif(REPMEUPP) * CNRLIG12 ;
LIGREP9MEU = positif(REP9MEU) * CNRLIG12 ;

regle 902590:
application : iliad  ;

LIGILMNP1 = positif(DILMNP1) * LIG1 * LIG2 ;
LIGILMNP2 = positif(DILMNP2) * LIG1 * LIG2 ;
LIGILMNP3 = positif(DILMNP3) * LIG1 * LIG2 ;
LIGILMNP4 = positif(DILMNP4) * LIG1 * LIG2 ;

LIGILMIX = positif(DILMIX) * LIG1 * LIG2 ;
LIGILMIY = positif(DILMIY) * LIG1 * LIG2 ;
LIGILMPA = positif(DILMPA) * LIG1 * LIG2 ;
LIGILMPB = positif(DILMPB) * LIG1 * LIG2 ;
LIGILMPC = positif(DILMPC) * LIG1 * LIG2 ;
LIGILMPD = positif(DILMPD) * LIG1 * LIG2 ;
LIGILMPE = positif(DILMPE) * LIG1 * LIG2 ;
LIGILMPF = positif(DILMPF) * LIG1 * LIG2 ;
LIGILMPK = positif(DILMPK) * LIG1 * LIG2 ;
LIGILMPG = positif(DILMPG) * LIG1 * LIG2 ;
LIGILMPL = positif(DILMPL) * LIG1 * LIG2 ;
LIGILMPH = positif(DILMPH) * LIG1 * LIG2 ;
LIGILMPM = positif(DILMPM) * LIG1 * LIG2 ;
LIGILMPI = positif(DILMPI) * LIG1 * LIG2 ;
LIGILMPN = positif(DILMPN) * LIG1 * LIG2 ;
LIGILMPJ = positif(DILMPJ) * LIG1 * LIG2 ;
LIGILMPO = positif(DILMPO) * LIG1 * LIG2 ;

LIGINVRED = positif(DINVRED) * LIG1 * LIG2 ;
LIGILMIH = positif(DILMIH) * LIG1 * LIG2 ;
LIGILMJC = positif(DILMJC) * LIG1 * LIG2 ;
LIGILMIZ = positif(DILMIZ) * LIG1 * LIG2 ;
LIGILMJI = positif(DILMJI) * LIG1 * LIG2 ;
LIGILMJS = positif(DILMJS) * LIG1 * LIG2 ;
LIGMEUBLE = positif(DMEUBLE) * LIG1 * LIG2 ;
LIGPROREP = positif(DPROREP) * LIG1 * LIG2 ;
LIGREPNPRO = positif(DREPNPRO) * LIG1 * LIG2 ;
LIGMEUREP = positif(DREPMEU) * LIG1 * LIG2 ;
LIGILMIC = positif(DILMIC) * LIG1 * LIG2 ;
LIGILMIB = positif(DILMIB) * LIG1 * LIG2 ;
LIGILMIA = positif(DILMIA) * LIG1 * LIG2 ;
LIGILMJY = positif(DILMJY) * LIG1 * LIG2 ;
LIGILMJX = positif(DILMJX) * LIG1 * LIG2 ;
LIGILMJW = positif(DILMJW) * LIG1 * LIG2 ;
LIGILMJV = positif(DILMJV) * LIG1 * LIG2 ;

LIGILMOE = positif(DILMOE) * LIG1 * LIG2 ;
LIGILMOD = positif(DILMOD) * LIG1 * LIG2 ;
LIGILMOC = positif(DILMOC) * LIG1 * LIG2 ;
LIGILMOB = positif(DILMOB) * LIG1 * LIG2 ;
LIGILMOA = positif(DILMOA) * LIG1 * LIG2 ;
LIGILMOJ = positif(DILMOJ) * LIG1 * LIG2 ;
LIGILMOI = positif(DILMOI) * LIG1 * LIG2 ;
LIGILMOH = positif(DILMOH) * LIG1 * LIG2 ;
LIGILMOG = positif(DILMOG) * LIG1 * LIG2 ;
LIGILMOF = positif(DILMOF) * LIG1 * LIG2 ;
LIGILMOO = positif(DILMOO) * LIG1 * LIG2 ;
LIGILMON = positif(DILMON) * LIG1 * LIG2 ;
LIGILMOM = positif(DILMOM) * LIG1 * LIG2 ;
LIGILMOL = positif(DILMOL) * LIG1 * LIG2 ;
LIGILMOK = positif(DILMOK) * LIG1 * LIG2 ;

LIGRESIVIEU = positif(DRESIVIEU) * LIG1 * LIG2 ;
LIGRESINEUV = positif(DRESINEUV) * LIG1 * LIG2 ;
LIGLOCIDEFG = positif(DLOCIDEFG) * LIG1 * LIG2 ;
LIGCODJTJU = positif(DCODJTJU) * LIG1 * LIG2 ;
LIGCODOU = positif(DCODOU) * LIG1 * LIG2 ;
LIGCODOV = positif(DCODOV) * LIG1 * LIG2 ;
LIGCODOW = positif(DCODOW) * LIG1 * LIG2 ;

regle 902600:
application : iliad  ;

LIGTAXASSUR = positif(present(CESSASSV) + present(CESSASSC)) * (1 - positif(ANNUL2042)) * LIG1 ;
LIGTAXASSURV = present(CESSASSV) * (1 - positif(ANNUL2042)) * LIG1 ;
LIGTAXASSURC = present(CESSASSC) * (1 - positif(ANNUL2042)) * LIG1 ;

LIGIPCAP = positif(present(PCAPTAXV) + present(PCAPTAXC)) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;
LIGIPCAPV = present(PCAPTAXV) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;
LIGIPCAPC = present(PCAPTAXC) * (1 - positif(ANNUL2042 + 0)) * LIG1 ;

LIGITAXLOY = present(LOYELEV) * (1 - positif(ANNUL2042)) * LIG1 ;

LIGIHAUT = positif(CHRAVANT) * (1 - positif(TEFFHRC + COD8YJ)) * (1 - positif(ANNUL2042)) * LIG1 ;

LIGHRTEFF = positif(CHRTEFF) * positif(TEFFHRC + COD8YJ) * (1 - positif(ANNUL2042)) * LIG1 ;

regle 902610:
application : iliad  ;

LIGCOMP01 = positif(BPRESCOMP01) * CNRLIG12 ;

regle 902620:
application : iliad  ;

LIGDEFBA = positif(DEFBA1 + DEFBA2 + DEFBA3 + DEFBA4 + DEFBA5 + DEFBA6) * LIG1 * LIG2 ;
LIGDEFBA1 = positif(DEFBA1) * LIG1 * LIG2 ;
LIGDEFBA2 = positif(DEFBA2) * LIG1 * LIG2 ;
LIGDEFBA3 = positif(DEFBA3) * LIG1 * LIG2 ;
LIGDEFBA4 = positif(DEFBA4) * LIG1 * LIG2 ;
LIGDEFBA5 = positif(DEFBA5) * LIG1 * LIG2 ;
LIGDEFBA6 = positif(DEFBA6) * LIG1 * LIG2 ;

LIGDFRCM = positif(DFRCMN+DFRCM1+DFRCM2+DFRCM3+DFRCM4+DFRCM5) * CNRLIG12 ;
LIGDFRCMN = positif(DFRCMN) * CNRLIG12 ;
LIGDFRCM1 = positif(DFRCM1) * CNRLIG12 ;
LIGDFRCM2 = positif(DFRCM2) * CNRLIG12 ;
LIGDFRCM3 = positif(DFRCM3) * CNRLIG12 ;
LIGDFRCM4 = positif(DFRCM4) * CNRLIG12 ;
LIGDFRCM5 = positif(DFRCM5) * CNRLIG12 ;

LIG2TUV = positif(COD2TU + COD2TV) * LIG1 * LIG2 ;
LIGDRCVM = positif(DPVRCM) * LIG1 * LIG2 ;
LIGDRFRP = positif(DRFRP) * LIG1 * LIG2 ;

BAMVV = (COD5XN - BAF1AV) * positif(COD5XN - BAF1AV) ;
BAMVC = (COD5YN - BAF1AC) * positif(COD5YN - BAF1AC) ;
BAMVP = (COD5ZN - BAF1AP) * positif(COD5ZN - BAF1AP) ;
LIGBAMVV = positif(BAMVV) * LIG1 * LIG2 ;
LIGBAMVC = positif(BAMVC) * LIG1 * LIG2 ;
LIGBAMVP = positif(BAMVP) * LIG1 * LIG2 ;

LIGDLMRN = positif(DLMRN6 + DLMRN5 + DLMRN4 + DLMRN3 + DLMRN2 + DLMRN1) * LIG1 * LIG2 ;
LIGDLMRN6 = positif(DLMRN6) * LIG1 * LIG2 ;
LIGDLMRN5 = positif(DLMRN5) * LIG1 * LIG2 ;
LIGDLMRN4 = positif(DLMRN4) * LIG1 * LIG2 ;
LIGDLMRN3 = positif(DLMRN3) * LIG1 * LIG2 ;
LIGDLMRN2 = positif(DLMRN2) * LIG1 * LIG2 ;
LIGDLMRN1 = positif(DLMRN1) * LIG1 * LIG2 ;

LIGBNCDF = positif(BNCDF1 + BNCDF2 + BNCDF3 + BNCDF4 + BNCDF5 + BNCDF6) * LIG1 * LIG2 ;
LIGBNCDF6 = positif(BNCDF6) * LIG1 * LIG2 ;
LIGBNCDF5 = positif(BNCDF5) * LIG1 * LIG2 ;
LIGBNCDF4 = positif(BNCDF4) * LIG1 * LIG2 ;
LIGBNCDF3 = positif(BNCDF3) * LIG1 * LIG2 ;
LIGBNCDF2 = positif(BNCDF2) * LIG1 * LIG2 ;
LIGBNCDF1 = positif(BNCDF1) * LIG1 * LIG2 ;

LIGMBDREPNPV = positif(MIBDREPNPV) * LIG1 * LIG2 ;
LIGMBDREPNPC = positif(MIBDREPNPC) * LIG1 * LIG2 ;
LIGMBDREPNPP = positif(MIBDREPNPP) * LIG1 * LIG2 ;

LIGMIBDREPV = positif(MIBDREPV) * LIG1 * LIG2 ;
LIGMIBDREPC = positif(MIBDREPC) * LIG1 * LIG2 ;
LIGMIBDREPP = positif(MIBDREPP) * LIG1 * LIG2 ;

LIGSPDREPNPV = positif(SPEDREPNPV) * LIG1 * LIG2 ;
LIGSPDREPNPC = positif(SPEDREPNPC) * LIG1 * LIG2 ;
LIGSPDREPNPP = positif(SPEDREPNPP) * LIG1 * LIG2 ;

LIGSPEDREPV = positif(SPEDREPV) * LIG1 * LIG2 ;
LIGSPEDREPC = positif(SPEDREPC) * LIG1 * LIG2 ;
LIGSPEDREPP = positif(SPEDREPP) * LIG1 * LIG2 ;

regle 902630:
application :  iliad ;


LIG_MEMO = positif(LIGPRELIB + LIG_SURSIS + LIGREPPLU + LIGABDET + LIGABDETP 
                     + LIGROBOR + LIGPVIMMO + LIGPVTISOC + LIG3TZ + LIGMOBNR 
                     + LIGDEPCHO + LIGDEPMOB + LIGZRS + LIGCOD3SG + LIGCOD3SL)
             + positif(LIG3525 + LIGRCMSOC + LIGRCMIMPAT + LIGABIMPPV + LIGABIMPMV + LIGPV3SB) * CNRLIG12 ;

regle 902640:
application :  iliad ;

LIGPRELIB = positif(present(PPLIB) + present(RCMLIB)) * LIG0 * LIG2 ;

LIG3525 = positif(RTNC) * CNRLIG12 ;

LIGREPPLU = positif(REPPLU) * LIG1 * LIG2 ;
LIGPVIMPOS = positif(PVIMPOS) * LIG1 * LIG2 ;

LIGABDET = positif(GAINABDET) * LIG1 * LIG2 ;
ABDEPRET = ABDETPLUS ;
LIGABDETP = positif(ABDEPRET) * LIG1 * LIG2 ;

LIGCOD3SG = positif(COD3SG) * LIG1 * LIG2 ;
LIGCOD3SL = positif(COD3SL) * LIG1 * LIG2 ;
LIGPV3SB = positif(PVBAR3SB) * LIG1 * LIG2 ;
LIGCOD3WH = positif(PVREPORT) * LIG1 * LIG2 ;
LIGHR3WT = positif(present(COD3WT)) * LIG1 * LIG2 ;

LIGRCMSOC = positif(RCMSOC) * CNRLIG12 ;
LIGRCMIMPAT = positif(RCMIMPAT) * CNRLIG12 ;
LIGABIMPPV = positif(ABIMPPV) * CNRLIG12 ;
LIGABIMPMV = positif(ABIMPMV) * CNRLIG12 ;

LIGCVNSAL = positif(CVNSALC) * LIG1 * LIG2 ;
LIGCDIS = positif(GSALV + GSALC) * CNRLIG12 ;
LIGROBOR = positif(RFROBOR) * LIG1 * LIG2 ;
LIGPVIMMO = positif(PVIMMO) * LIG1 * LIG2 ;
LIGPVTISOC = positif(PVTITRESOC) * LIG1 * LIG2 ;
LIG3TZ = positif(COD3TZ) * LIG1 * LIG2 ;
LIGMOBNR = positif(PVMOBNR) * LIG1 * LIG2 ;

DEPMOB = (RDEQPAHA + RDTECH) * positif(V_NOTRAIT - 10) ;

LIGDEPMOB = positif(DIFF7WZ + DIFF7WD) * positif(DEPMOB) * CNRLIG12 ;

DEPCHO = (COD7XD + COD7XE + COD7XF + COD7XG) * positif(V_NOTRAIT - 10) ;
LIGDEPCHO = positif(DEPCHO) * CNRLIG12 ;

LIGZRS = positif(CODZRS) * LIG1 * LIG2 ;

LIGDEFPLOC = positif(DEFLOC1 + DEFLOC2 + DEFLOC3 + DEFLOC4 + DEFLOC5 + DEFLOC6 + DEFLOC7 + DEFLOC8 + DEFLOC9 + DEFLOC10) * LIG1 * LIG2 ;
LIGPLOC1 = positif(DEFLOC1) * LIG1 * LIG2 ;
LIGPLOC2 = positif(DEFLOC2) * LIG1 * LIG2 ;
LIGPLOC3 = positif(DEFLOC3) * LIG1 * LIG2 ;
LIGPLOC4 = positif(DEFLOC4) * LIG1 * LIG2 ;
LIGPLOC5 = positif(DEFLOC5) * LIG1 * LIG2 ;
LIGPLOC6 = positif(DEFLOC6) * LIG1 * LIG2 ;
LIGPLOC7 = positif(DEFLOC7) * LIG1 * LIG2 ;
LIGPLOC8 = positif(DEFLOC8) * LIG1 * LIG2 ;
LIGPLOC9 = positif(DEFLOC9) * LIG1 * LIG2 ;
LIGPLOC10 = positif(DEFLOC10) * LIG1 * LIG2 ;

regle 902650:
application :  iliad ;


LIGDCSGD = positif(DCSGD) * null(5 - V_IND_TRAIT) * INDCTX * LIG1 * LIG2 ;

regle 902660:
application :  iliad ;


LIGREPCORSE = positif(REPCORSE) * LIG1 * LIG2 ;

LIGREPRECH = positif(REPRECH) * LIG1 * LIG2 ;

LIGREPCICE = positif(REPCICE) * LIG1 * LIG2 ; 

LIGPME = positif(REPINVPME3 + REPINVPME2 + REPINVPME1) * CNRLIG12 ;

regle 902670:
application :  iliad ;

LIGIBAEX = positif(REVQTOT) * LIG1 * LIG2 
	     * (1 - INDTXMIN) * (1 - INDTXMOY) 
	     * (1 - V_CNR * (1 - LIG1522)) ;

regle 902680:
application :  iliad  ;

LIGANNUL2042 = LIG2 * LIG0 ;

LIG121 = positif(DEFRITS) * LIGANNUL2042 ;
LIG931 = positif(DEFRIRCM)*positif(RCMFR) * LIGANNUL2042 ;
LIG936 = positif(DEFRIRCM)*positif(REPRCM) * LIGANNUL2042 ;
LIG1111 = positif(DFANTIMPUBAR) * LIGANNUL2042 ;
LIG1112 = positif(DFANTIMPU) * positif(DEFRF4BC) * positif(RDRFPS) * LIGANNUL2042 ;
LIGDFANT = positif(DFANTIMPUQUO) * LIGANNUL2042 ;

regle 902690:
application :  iliad ;

LIGTRCT = positif(V_TRCT) ;

regle 902700:
application :  iliad ;

LIGVERSUP = positif(AUTOVERSSUP) ;

regle 902710:
application : iliad  ;


INDRESTIT = positif(IREST + 0) ;

INDIMPOS = positif(null(1 - NATIMP) + 0) ;

regle isf 902740:
application : iliad  ;

LIG_AVISIFI = (positif(LIM_IFIINF)) * present(IFIPAT);



LIGIFI9AA = positif(COD9AA);
LIGIFI9AB = positif(COD9AB);
LIGIFI9AC = positif(COD9AC);
LIGIFI9AD = positif(COD9AD);
LIGIFI9BA = positif(COD9BA);
LIGIFI9BB = positif(COD9BB);
LIGIFI9CA = positif(COD9CA);
LIGIFI9CB = positif(COD9CB);
LIGIFIACT = positif(IFIACT);

LIGIFI9GF = positif(COD9GF); 
LIGIFI9GH = positif(COD9GH);
LIGIFIPAS = positif(IFIPAS);

LIGIFIBASE =  LIGIFI * (1 - positif(ANNUL2042));

LIGIFI =positif(IFIPAT-LIM_IFIINF);


LIGIFIDIRECT = positif(LIGIFI9AA + LIGIFI9AB + LIGIFI9AC + LIGIFI9AD + LIGIFI9BA + LIGIFI9BB) * LIGIFI;


LIGIFINDIR =positif( LIGIFIDIRECT) * positif(LIGIFI9CA + LIGIFI9CB) * LIGIFI;

LIGIFINDIR1 = positif(LIGIFI9CA + LIGIFI9CB) * LIGIFI * (1-positif(LIGIFINDIR))*(1-positif(LIGIFIDIRECT));

LIGIFIDEC = positif(IFI1) * positif(DECIFI) * LIGIFI * (1 - positif(ANNUL2042)) ;  
            


LIGIFIBRUT = (positif(IFI2) * (1 - positif(ANNUL2042)) * LIGIFI * (1-null(5-V_IND_TRAIT))
             * positif(RDONIFI1+RDONIFI2+RIFIPMED+RIFIPMEI+RIFIFIP+RIFIFCPI)
             * positif(COD9NC + COD9NG + COD9NE + COD9NF + COD9MX + COD9NA)

           + positif(IFI2) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT)
             * positif(present(COD9NC) + present(COD9NG)
                       + present(COD9NE) + present(COD9NF) + present(COD9MX) + present(COD9NA)))*(positif(LIGIFIDEC));

LIGIFIRAN = positif(COD9NC) * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI +
            present(COD9NC) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ;

LIGIFICEE = positif(COD9NG) * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI +
            present(COD9NG) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ;

LIGIFIDON = positif(LIGIFIRAN + LIGIFICEE) * LIGIFI ;

LIGIFIPMED = positif(COD9NE) * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI +
             present(COD9NE) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ;

LIGIFIPMEI = positif(COD9NF) * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI +
             present(COD9NF) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) * LIGIFI ;


LIGIFIIP = positif(COD9MX) * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI +
           present(COD9MX) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ;

LIGIFICPI = positif(COD9NA) * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI +
            present(COD9NA) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ;


LIGIFIINV1 = positif(LIGIFIPMED + LIGIFIPMEI + LIGIFIIP + LIGIFICPI) * LIGIFI * (1-positif(LIGIFIDON));

LIGIFIINV = positif (LIGIFIDON)* positif(LIGIFIPMED + LIGIFIPMEI + LIGIFIIP + LIGIFICPI) * LIGIFI;


LIGIFIRED = positif(LIGIFIDON + LIGIFIINV+ LIGIFIINV1) * LIGIFI
             * (1 - positif(null((CODE_2042 + CMAJ_ISF)- 8) + null(CMAJ_ISF - 34)+null((CODE_2042 + CMAJ_ISF)- 11)+null(CODE_2042- 3)+null(CODE_2042- 5)+null(CODE_2042- 55)) * (1 - COD9ZA));





LIGIFIREDPEN8 = positif(LIGIFIDON + LIGIFIINV + LIGIFIINV1) * LIGIFI
             * positif(null ((CODE_2042 + CMAJ_ISF)- 8) + null(CMAJ_ISF - 34)+null((CODE_2042 + CMAJ_ISF)- 11)+null(CODE_2042- 3)+null(CODE_2042- 5)+null(CODE_2042- 55))
             * (1 - COD9ZA); 
	     






LIGIFIIMPU = positif(DIFIBASE) * positif(IFIETR+IFIPLAF)
            * (1 - positif (COD9NC + COD9NG + COD9NE + COD9NF + COD9MX + COD9NA))
            * LIGIFI * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT))
            + positif(DIFIBASE) * positif(IFIETR + IFIPLAF)
            * (1 - positif (COD9NC + COD9NG + COD9NE + COD9NF + COD9MX + COD9NA))
           * LIGIFI * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT)
           * (1-positif(LIGIFIRED + LIGIFIREDPEN8));


LIGIPLAF = positif( PLAFIFI ) * (1-null(5-V_IND_TRAIT))
            * LIGIFI * (1 - positif(ANNUL2042))
	    + positif(PLAFIFI)  * positif(DIFIBASE)* (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT); 


LIGIFIE = positif(DIFIBASE) * positif(COD9RS)
         * (1 - positif(ANNUL2042)) * (1-null(5-V_IND_TRAIT)) * LIGIFI
          + positif(DIFIBASE) * present(COD9RS)
         * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ;



LIGIFICOR1 = present(IFI4BIS)*positif(DIFIBASE) * positif(PIFI)
               * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFI4BIS)))
              * (1 - positif(ANNUL2042)) * LIGIFI
              * (1-positif(V_NOTRAIT-20))
+ positif(V_NOTRAIT-20) * LIGIFI * (1 - positif(ANNUL2042));

LIGIFINOPEN = present(IFITOT)*positif(DIFIBASE)* (1 - positif(PIFI))
                * (1 - positif(LIGIFICOR1))
		                * LIGIFI * (1 - positif(ANNUL2042)) ;
				
LIGIFIRET = positif(RETIFI) * (1 - positif(ANNUL2042)) * LIGIFI
            * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT))); 

LIGIFIRET22 = positif(LIGIFIRET)*positif(RETIFIRED);
 

LIGIFI9749 = positif(LIGNMAJIFI1) * (1 - positif(LIGIFIRET)) ;

LIGNMAJIFI1 = positif(NMAJIFI1) * (1 - positif(ANNUL2042)) * LIGIFI
            * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)));

LIGIFI17281 = positif(NMAJIFI1) * (1 - positif(ANNUL2042)) * 
LIGIFI
               * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)))
               * (1 - positif(V_FLAGR34 + null(CMAJ_ISF - 34))) ;

LIGIFI17285 = positif(NMAJIFI1) * (1 - positif(ANNUL2042)) * LIGIFI
               * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)))
               * positif(V_FLAGR34 + null(CMAJ_ISF - 34)) ;

LIGNMAJIFI4 = positif(NMAJIFI4) * (1 - positif(ANNUL2042)) * LIGIFI
            * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)));


LIGIFI1729 = positif(NMAJIFI4) * (1 - positif(ANNUL2042)) * LIGIFI
            * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)));

LIGIFIANT = positif(V_ANTIFI) * positif(V_NOTRAIT-20) ;

INDCTX23 = si (  (V_NOTRAIT+0 = 23)
                 ou (V_NOTRAIT+0 = 33)
                ou (V_NOTRAIT+0 = 43)
                ou (V_NOTRAIT+0 = 53)
                ou (V_NOTRAIT+0 = 63)
             )
         alors (1)
         sinon (0)
          finsi;
LIGIFINET = (positif(PIFI)*positif(DIFIBASE) * (1-null(5-V_IND_TRAIT))
               * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)))
              * (1 - positif(ANNUL2042)) * LIGIFI
            + (null(5-V_IND_TRAIT)) * positif(LIGIFIRET + LIGNMAJIFI1)
              * positif(IFINAP) * (1 - positif( SEUIL_12 - IFINAP)))
           * (1 - positif(INDCTX23)) ;


INDIS26 = si (  (V_NOTRAIT+0 = 26)
            ou (V_NOTRAIT+0 = 36)
             ou (V_NOTRAIT+0 = 46)
             ou (V_NOTRAIT+0 = 56)
             ou (V_NOTRAIT+0 = 66)
             )
        alors (1)
        sinon (0)
        finsi;

LIGIFI9269 = (1 - positif(LIGIFIRET + LIGNMAJIFI1)) * (1 - positif( SEUIL_12 - IFINAP)) * INDIS26 ;

LIGIFIRECOU = present(IFIRECOUVR);

LIGIFIANNUL = present(IFIPAT) * positif(ANNUL2042) ;


IND9HI0 = INDCTX23 * null( 5-V_IND_TRAIT ) * present(IFIPAT);


LIGIFIDEG = (1 - positif(LIGIFIDEGR)) * IND9HI0 * positif(IFIDEG) ;

LIGIFIDEGR = (null(2- (positif(SEUIL_8 - IFIDEGR) + positif_ou_nul(IFITOT-SEUIL_12)))
              + null(V_ANTISF))
             * INDCTX23 * null(5-V_IND_TRAIT) * (1 - positif(ANNUL2042)) ;


LIGIFIZERO = null(IFITOT) * (1 - positif(ANNUL2042)) * positif(20-V_NOTRAIT) * LIGIFI ;	 

LIGIFINMR = positif( SEUIL_12 - IFITOT) * (1 - null(IFITOT))
           * (1 - positif(INDCTX23)) * positif(20 - V_NOTRAIT)
           * LIGIFI * (1 - positif(ANNUL2042)) ;


LIGIFINMRIS = positif(SEUIL_12 - IFINAP)
            * INDIS26 * positif(V_NOTRAIT - 20) * (1 - positif(ANNUL2042)) ;

LIGIFI0DEG = IND9HI0 * null(IFI4BIS) * (1 - positif(ANNUL2042)) ;

LIGIFINMRDEG = (1 - positif(LIGIFIDEGR)) * (1 - null(IFITOT))
               * positif(SEUIL_12 - IFI4BIS) * positif(DIFIBASE)
              * INDCTX23 * null(5-V_IND_TRAIT) * (1 - positif(ANNUL2042)) ;

LIGIFIINF8 = IND9HI0 * positif(LIGIFIDEGR) * (1 - positif(ANNUL2042)) ;

LIGIFINEW = present(IFIPAT) * (1 - positif(20-V_NOTRAIT)) * null(5 - V_IND_TRAIT) * (1 - positif(ANNUL2042)) ;

LIGIFISUI =null(5-V_IND_TRAIT)*(positif(IFITOT-SEUIL_12)); 
regle 902745:
application :  iliad ;

LIGIFITRCT = present(IFIPAT) * positif(V_TRCT) ;
