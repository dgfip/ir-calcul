#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2017]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2017 
#au titre des revenus per�us en 2016. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle 521000:
application : bareme , iliad , batch ;
 

TAUX1 = (TX_BAR1 - TX_BAR0 ) ;
TAUX2 = (TX_BAR2 - TX_BAR1 ) ;
TAUX3 = (TX_BAR3 - TX_BAR2 ) ;
TAUX4 = (TX_BAR4 - TX_BAR3 ) ;
TAUX5 = (TX_BAR5 - TX_BAR4 ) ;

regle 521010:
application : bareme , iliad , batch ;
DS011 = max( QF011 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF011 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF011 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF011 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF011 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS021 = max( QF021 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF021 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF021 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF021 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF021 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS511 = max( QF511 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF511 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF511 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF511 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF511 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS521 = max( QF521 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF521 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF521 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF521 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF521 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS012 = max( QF012 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF012 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF012 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF012 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF012 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS022 = max( QF022 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF022 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF022 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF022 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF022 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS512 = max( QF512 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF512 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF512 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF512 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF512 - LIM_BAR5 , 0 ) * (TAUX5   / 100);
DS522 = max( QF522 - LIM_BAR1 , 0 ) * (TAUX1   / 100)
      + max( QF522 - LIM_BAR2 , 0 ) * (TAUX2   / 100)
      + max( QF522 - LIM_BAR3 , 0 ) * (TAUX3   / 100)
      + max( QF522 - LIM_BAR4 , 0 ) * (TAUX4   / 100)
      + max( QF522 - LIM_BAR5 , 0 ) * (TAUX5   / 100);

regle 521020:
application : iliad , batch ;

WTXMARJ = (RB51) / ( NBPT * null(PLAFQF) + (1 + BOOL_0AM + BOOL_0AZ * V_0AV) *null(1-PLAFQF)) ;
TXMARJ = max ( positif (WTXMARJ - LIM_BAR1) * TX_BAR1 , 
                max ( positif (WTXMARJ - LIM_BAR2) * TX_BAR2 , 
                      max ( positif (WTXMARJ - LIM_BAR3) * TX_BAR3 , 
                             max ( positif (WTXMARJ - LIM_BAR4) * TX_BAR4 ,
                                   max ( positif (WTXMARJ - LIM_BAR5) * TX_BAR5 , 0
				       )
                                 )
                          )
                     )
              )

          * ( 1 - positif ( 
                              present ( NRBASE ) 
                            + present ( NRINET ) 
                            + present ( IPTEFP ) 
                            + present ( IPTEFN ) 
                            + present ( PRODOM ) 
                            + present ( PROGUY ) 
                          )              
             )
          * (1- V_CNR)
  * positif(IN01+IPQ1001);
TXMARJBA = max ( positif (WTXMARJ - LIM_BAR1) * TX_BAR1 , 
                max ( positif (WTXMARJ - LIM_BAR2) * TX_BAR2 , 
                      max ( positif (WTXMARJ - LIM_BAR3) * TX_BAR3 , 
                             max ( positif (WTXMARJ - LIM_BAR4) * TX_BAR4 ,
                                   max ( positif (WTXMARJ - LIM_BAR5) * TX_BAR5 , 0
				       )
                                 )
                          )
                     )
              )
  * positif(IN01+IPQ1001);

regle 521030:
application : bareme , iliad , batch ;
 

DS014 = max( QF014 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF014 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF014 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF014 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF014 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS024 = max( QF024 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF024 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF024 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF024 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF024 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS015 = max( QF015 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF015 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF015 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF015 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF015 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS515 = max( QF515 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF515 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF515 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF515 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF515 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS025 = max( QF025 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF025 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF025 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF025 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF025 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS525 = max( QF525 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF525 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF525 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF525 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF525 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS016 = max( QF016 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF016 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF016 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF016 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF016 - LIM_BAR5 , 0 ) * (TAUX5 /100);
DS026 = max( QF026 - LIM_BAR1 , 0 ) * (TAUX1 /100)
      + max( QF026 - LIM_BAR2 , 0 ) * (TAUX2 /100)
      + max( QF026 - LIM_BAR3 , 0 ) * (TAUX3 /100)
      + max( QF026 - LIM_BAR4 , 0 ) * (TAUX4 /100)
      + max( QF026 - LIM_BAR5 , 0 ) * (TAUX5 /100);

regle 521040:
application : bareme , iliad , batch ;

NBYV1 = NBPT;
NBYV2 = 1 + BOOL_0AM + BOOL_0AZ * V_0AV;

regle 521050:
application : bareme , iliad , batch ;
QF011 = arr(RB01) / NBYV1;
QF021 = arr(RB01) / NBYV2;
QF012 = arr(RB02) / NBYV1;
QF022 = arr(RB02) / NBYV2;
QF511 = arr(RB51) / NBYV1;
QF521 = arr(RB51) / NBYV2;
QF512 = arr(RB52) / NBYV1;
QF522 = arr(RB52) / NBYV2;
QF014 = arr(RB04) / NBYV1;
QF024 = arr(RB04) / NBYV2;
QF015 = arr(RB05) / NBYV1;
QF515 = arr(RB55) / NBYV1;
QF025 = arr(RB05) / NBYV2;
QF525 = arr(RB55) / NBYV2;
QF016 = arr(RB06) / NBYV1;
QF026 = arr(RB06) / NBYV2;

regle corrective 521070:
application : iliad , batch ;
CFRIAHP = ARESTIMO + ALOGDOM + ADUFREPFI + ADUFREPFK + ADUFREPFR + ADUFLOEKL + ADUFLOGIH + APIREPAI
         + APIREPBI + APIREPCI + APIREPDI + APIREPBZ + APIREPCZ + APIREPDZ + APIREPEZ 
	 +APIQKL + APIQIJ +  APIQGH + APIQEF + APIQCD + APIQAB + ATOURREP
         + ATOUREPA 
	 + ACELRREDLB + ACELRREDLE + ACELRREDLM + ACELRREDLN + ACELRREDLG + ACELRREDLK + ACELRREDLC
         + ACELRREDLD + ACELRREDLS + ACELRREDLT + ACELRREDLH + ACELRREDLL + ACELRREDLF + ACELRREDLZ
	 + ACELRREDLX + ACELRREDLI + ACELRREDLO + ACELRREDMG + ACELRREDMH + ACELRREDLJ + ACELRREDLP
         + ACELREPHS + ACELREPHR + ACELREPHU + ACELREPHT + ACELREPHZ + ACELREPHX + ACELREPHW
         + ACELREPHV + ACELREPHF + ACELREPHD + ACELREPHA + ACELREPGU + ACELREPGX + ACELREPGS
         + ACELREPGW + ACELREPGL + ACELREPGV + ACELREPGJ + ACELREPYH + ACELREPYL + ACELREPYF 
         + ACELREPYK + ACELREPYD + ACELREPYJ + ACELREPYB + ACELREPYP + ACELREPYS + ACELREPYO
         + ACELREPYR + ACELREPYN + ACELREPYQ + ACELREPYM + ACELREPYW + ACELREPYZ + ACELREPYV
         + ACELREPYY + ACELREPYU + ACELREPYX + ACELREPYT + ACELHNO + ACELHJK + ACELNQ
	 + ACELNBGL + ACELCOM + ACEL + ACELJP + ACELJBGL + ACELJOQR + ACEL2012
         + ACELFD + ACELFABC + ACELZA + ACELZB + ACELZC + ACELZD
	 + AREDREP + AILMIX + AILMIY + AILMPA + AILMPF + AILMPK + AINVRED + AILMIH + AILMJC + AILMPB
         + AILMPG + AILMPL + AILMIZ + AILMJI + AILMPC + AILMPH + AILMPM + AILMJS + AILMPD + AILMPI
         + AILMPN + AILMPE + AILMPJ + AILMPO + AMEUBLE + APROREP + AREPNPRO + AREPMEU + AILMIC + AILMIB 
	 + AILMIA + AILMJY + AILMJX + AILMJW + AILMJV + AILMOE + AILMOD + AILMOC + AILMOB + AILMOA
         + AILMOJ + AILMOI + AILMOH + AILMOG + AILMOF + AILMOO + AILMON + AILMOM + AILMOL + AILMOK
         + ARESIVIEU + ARESINEUV + ALOCIDEFG + ACODJTJU + ACODOU + ACODOV + ACODOW ;

CFRIRHP =  RRESTIMO + RLOGDOM + RDUFREPFI + RDUFREPFK + RDUFREPFR + RDUFLOEKL + RDUFLOGIH + RPIREPAI + RPIREPBI
          + RPIREPCI + RPIREPDI + RPIREPBZ + RPIREPCZ + RPIREPDZ + RPIREPEZ
	  + RPIQKL + RPIQIJ + RPIQGH + RPIQEF + RPIQCD + RPIQAB + RTOURREP + RTOUREPA
          + RCELRREDLB + RCELRREDLE + RCELRREDLM + RCELRREDLN + RCELRREDLG + RCELRREDLK + RCELRREDLC
          + RCELRREDLD + RCELRREDLS + RCELRREDLT + RCELRREDLH + RCELRREDLL + RCELRREDLF + RCELRREDLZ
	  + RCELRREDLX + RCELRREDLI + RCELRREDLO + RCELRREDMG + RCELRREDMH + RCELRREDLJ + RCELRREDLP
          + RCELREPHS + RCELREPHR + RCELREPHU + RCELREPHT + RCELREPHZ + RCELREPHX + RCELREPHW
          + RCELREPHV + RCELREPHF + RCELREPHD + RCELREPHA + RCELREPGU + RCELREPGX + RCELREPGS
          + RCELREPGW + RCELREPGL + RCELREPGV + RCELREPGJ + RCELREPYH + RCELREPYL + RCELREPYF
          + RCELREPYK + RCELREPYD + RCELREPYJ + RCELREPYB + RCELREPYP + RCELREPYS + RCELREPYO
          + RCELREPYR + RCELREPYN + RCELREPYQ + RCELREPYM + RCELREPYW + RCELREPYZ + RCELREPYV 
	  + RCELREPYY + RCELREPYU + RCELREPYX + RCELREPYT + RCELHNO + RCELHJK + RCELNQ
          + RCELNBGL + RCELCOM + RCEL + RCELJP + RCELJBGL + RCELJOQR + RCEL2012 
          + RCELFD + RCELFABC + RCELZA + RCELZB + RCELZC + RCELZD
	  + RREDREP + RILMIX + RILMIY + RILMPA + RILMPF + RILMPK + RINVRED + RILMIH + RILMJC + RILMPB
          + RILMPG + RILMPL + RILMIZ + RILMJI + RILMPC + RILMPH + RILMPM + RILMJS + RILMPD + RILMPI 
	  + RILMPN + RILMPE + RILMPJ + RILMPO + RMEUBLE + RPROREP + RREPNPRO + RREPMEU + RILMIC + RILMIB
          + RILMIA + RILMJY + RILMJX + RILMJW + RILMJV + RILMOE + RILMOD + RILMOC + RILMOB + RILMOA
          + RILMOJ + RILMOI + RILMOH + RILMOG + RILMOF + RILMOO + RILMON + RILMOM + RILMOL + RILMOK
          + RRESIVIEU + RRESINEUV + RLOCIDEFG + RCODJTJU + RCODOU + RCODOV + RCODOW ;
CFRIADON = AREPA + ADONS;
CFRIRDON = RREPA + RDONS;
CFRIAENF = APRESCOMP;
CFRIRENF = RPRESCOMP + RRETU;
CFRIADEP = AHEBE + AAIDE;
CFRIRDEP = RHEBE + RAIDE;
CFRIAFOR = BFCPI + ACOMP + AFOREST + AFORET + ANOUV + ALOCENT + ALOGSOC + ACOLENT + ACOTFOR + ADOMSOC1 + AFIPDOM;
CFRIRFOR = RINNO + RCOMP + RFOREST + RFORET + RNOUV + RLOCENT + RLOGSOC + RCOLENT + RCOTFOR + RDOMSOC1 + RFIPDOM;
CFRIAVIE = ASURV;
CFRIRVIE = RSURV;
CFRIAAUTRE = AFIPC + ADIFAGRI + ASOCREPR+ASOUFIP+ARIRENOV+APATNAT+APATNAT1+APATNAT2+APATNAT3;
CFRIRAUTRE = RFIPC + RDIFAGRI + RSOCREPR+RSOUFIP+RRIRENOV+RPATNAT+RPATNAT1+RPATNAT2+RPATNAT3;
CFCIDIV = CIGARD + CISYND + CIADCRE + CIFORET;

TOTDEFRCM = DFRCM1 + DFRCM2+DFRCM3 +DFRCM4 +DFRCM5 +DFRCMN;
TOTDEFLOC = DEFLOC1 + DEFLOC2 +DEFLOC3 +DEFLOC4 +DEFLOC5 +DEFLOC6 +DEFLOC7 +DEFLOC8 +DEFLOC9 +DEFLOC10;
TOTMIBV = MIBRNETV + MIBNPRNETV + MIBNPPVV + MIBPVV - BICPMVCTV - MIBNPDCT;
TOTMIBC = MIBRNETC + MIBNPRNETC + MIBNPPVC + MIBPVC - BICPMVCTC -COD5RZ;
TOTMIBP = MIBRNETP + MIBNPRNETP + MIBNPPVP + MIBPVP - BICPMVCTP - COD5SZ;
TOTBNCV = SPENETPV + SPENETNPV + BNCPROPVV - BNCPMVCTV + BNCNPPVV - BNCNPDCT;
TOTBNCC = SPENETPC + SPENETNPC + BNCPROPVC - BNCPMVCTC + BNCNPPVC;
TOTBNCP = SPENETPP + SPENETNPP + BNCPROPVP - BNCPMVCTP + BNCNPPVP;
TOTSPEREP = SPEDREPV + SPEDREPC +SPEDREPP;
TOTSPEREPNP = SPEDREPNPV + SPEDREPNPC +SPEDREPNPP;
IRNINCFIR = IAN+AVFISCOPTER-IRE;

