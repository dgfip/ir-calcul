#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2017]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2017 
#au titre des revenus per�us en 2016. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************


regle 871000:
application : iliad , batch ;

XBAMIC = COD5XA + COD5YA + COD5ZA; 

regle 871020:
application : iliad , batch ;

XBAV = BAHEXV + BAEXV ;
XBAC = BAHEXC + BAEXC ;
XBAP = BAHEXP + BAEXP ;
XBIPV = BIHEXV + BICEXV;
XBIPC = BIHEXC + BICEXC;
XBIPP = BIHEXP + BICEXP;
XBINPV = BICNPHEXV + BICNPEXV;
XBINPC = BICNPHEXC + BICNPEXC;
XBINPP = BICNPHEXP + BICNPEXP;
XBNV = BNHEXV + BNCEXV ;
XBNC = BNHEXC + BNCEXC ;
XBNP = BNHEXP + BNCEXP ;
XBNNPV = BNCNPREXV+BNCNPREXAAV ;
XBNNPC = BNCNPREXC+BNCNPREXAAC ;
XBNNPP = BNCNPREXP+BNCNPREXAAP ;

regle 871030:
application : iliad , batch ;

XBICHDV = (BICEXV + BICNOV)  ;
XBICHDC = (BICEXC + BICNOC)  ;
XBICHDP = (BICEXP + BICNOP)  ;
XBICNETV = XBICHDV - BICDNV;
XBICNETC = XBICHDC - BICDNC;
XBICNETP = XBICHDP - BICDNP;
XBICSV =  XBICNETV + BA1AV ;
XBICSC =  XBICNETC + BA1AC ;
XBICSP =  XBICNETP + BA1AP ;
XBICNPHDV = BICNPEXV + BICREV ;
XBICNPHDC = BICNPEXC + BICREC ;
XBICNPHDP = BICNPEXP + BICREP ;
XBICNPNETV = XBICNPHDV - BICDEV;
XBICNPNETC = XBICNPHDC - BICDEC;
XBICNPNETP = XBICNPHDP - BICDEP;
XBICNPSV =  XBICNPNETV + BI2AV ;
XBICNPSC =  XBICNPNETC + BI2AC ;
XBICNPSP =  XBICNPNETP + BI2AP ;
XBITV = max (0 , XBICNETV + max (0,XBICNPNETV )); 
XBITC = max (0 , XBICNETC + max (0,XBICNPNETC )); 
XBITP = max (0 , XBICNETP + max (0,XBICNPNETP )); 
XBISV = positif(max(0,XBICNETV + max(0,XBICNPNETV)))
        * ( BI2AV  + BI1AV  );
XBISC = positif(max(0,XBICNETC + max(0,XBICNPNETC)))
        * ( BI2AC  + BI1AC  );
XBISP = positif(max(0,XBICNETP + max(0,XBICNPNETP)))
        * ( BI2AP  + BI1AP  );

XBICIMPV =  XBICHDV + XBICNPHDV ;
XBICIMPC =  XBICHDC + XBICNPHDC ;
XBICIMPP =  XBICHDP + XBICNPHDP ;

regle 871040:
application : iliad , batch ;
 

XTSBV = GLD3V + TSBNV + BPCOSAV + TSASSUV + XETRANV + EXOCETV + GLDGRATV;
XTSBC = GLD3C + TSBNC + BPCOSAC + TSASSUC + XETRANC + EXOCETC + GLDGRATC;

XTSB1 =  TSBN1 ;
XTSB2 =  TSBN2 ;
XTSB3 =  TSBN3 ;
XTSB4 =  TSBN4 ;
XEXTSV = XTSBV + CARTSV + REMPLAV ;
XEXTSC = XTSBC + CARTSC + REMPLAC ;
XEXTS1 = XTSB1 + CARTSP1 + REMPLAP1 ;
XEXTS2 = XTSB2 + CARTSP2 + REMPLAP2 ;
XEXTS3 = XTSB3 + CARTSP3 + REMPLAP3 ;
XEXTS4 = XTSB4 + CARTSP4 + REMPLAP4 ;

regle 871050:
application : iliad , batch ;
 
XTPS10V = arr (XEXTSV * TX_DEDFORFTS /100) ;
XTPS10C = arr (XEXTSC * TX_DEDFORFTS /100) ;
XTPS101 = arr (XEXTS1 * TX_DEDFORFTS /100) ;
XTPS102 = arr (XEXTS2 * TX_DEDFORFTS /100) ;
XTPS103 = arr (XEXTS3 * TX_DEDFORFTS /100) ;
XTPS104 = arr (XEXTS4 * TX_DEDFORFTS /100) ;
XDFNV =  min( PLAF_DEDFORFTS , XTPS10V ) ;
XDFNC =  min( PLAF_DEDFORFTS , XTPS10C ) ;
XDFN1 =  min( PLAF_DEDFORFTS , XTPS101 ) ;
XDFN2 =  min( PLAF_DEDFORFTS , XTPS102 ) ;
XDFN3 =  min( PLAF_DEDFORFTS , XTPS103 ) ;
XDFN4 =  min( PLAF_DEDFORFTS , XTPS104 ) ;
 
regle 871060:
application : iliad , batch ;
 
X10MINSV = max( min(XTSBV,DEDMINV) , XDFNV );
X10MINSC = max( min(XTSBC,DEDMINC) , XDFNC );
X10MINS1 = max( min(XTSB1,DEDMIN1) , XDFN1 );
X10MINS2 = max( min(XTSB2,DEDMIN2) , XDFN2 );
X10MINS3 = max( min(XTSB3,DEDMIN3) , XDFN3 );
X10MINS4 = max( min(XTSB4,DEDMIN4) , XDFN4 );
XIND_10V = positif_ou_nul(X10MINSV-FRNV);
XIND_10C = positif_ou_nul(X10MINSC-FRNC);
XIND_101 = positif_ou_nul(X10MINS1-FRN1);
XIND_102 = positif_ou_nul(X10MINS2-FRN2);
XIND_103 = positif_ou_nul(X10MINS3-FRN3);
XIND_104 = positif_ou_nul(X10MINS4-FRN4);
XDFV = X10MINSV  ;
XDFC = X10MINSC  ;
XDF1 = X10MINS1  ;
XDF2 = X10MINS2  ;
XDF3 = X10MINS3  ;
XDF4 = X10MINS4  ;
XFPTV = XDFV * XIND_10V + FRDV * (1 - XIND_10V);
XFPTC = XDFC * XIND_10C + FRDC * (1 - XIND_10C);
XFPT1 = XDF1 * XIND_101 + FRD1 * (1 - XIND_101);
XFPT2 = XDF2 * XIND_102 + FRD2 * (1 - XIND_102);
XFPT3 = XDF3 * XIND_103 + FRD3 * (1 - XIND_103);
XFPT4 = XDF4 * XIND_104 + FRD4 * (1 - XIND_104);
XTSNTV =  XEXTSV - XFPTV ;
XTSNTC =  XEXTSC - XFPTC ;
XTSNT1 =  XEXTS1 - XFPT1 ;
XTSNT2 =  XEXTS2 - XFPT2 ;
XTSNT3 =  XEXTS3 - XFPT3 ;
XTSNT4 =  XEXTS4 - XFPT4 ;
 
regle 871070:
application : iliad , batch ;
 
XTSNV = positif (-XTSNTV) * min (0 , XTSNTV)
        + positif_ou_nul (XTSNTV) * XTSNTV ;
XTSNC = positif (-XTSNTC) * min (0 , XTSNTC)
        + positif_ou_nul (XTSNTC) * XTSNTC ;
XTSN1 = positif (-XTSNT1) * min (0 , XTSNT1)
        + positif_ou_nul (XTSNT1) * XTSNT1 ;
XTSN2 = positif (-XTSNT2) * min (0 , XTSNT2)
        + positif_ou_nul (XTSNT2) * XTSNT2 ;
XTSN3 = positif (-XTSNT3) * min (0 , XTSNT3)
        + positif_ou_nul (XTSNT3) * XTSNT3 ;
XTSN4 = positif (-XTSNT4) * min (0 , XTSNT4)
        + positif_ou_nul (XTSNT4) * XTSNT4 ;
 
regle 871080:
application : iliad , batch ;
 
XTSV = XTSNV -  max(0,GLD3V - ABGL3V);
XTSC = XTSNC -  max(0,GLD3C - ABGL3C);
XTSNNV = arr( positif(XTSNV) * 
         XTSNV  
         * (TSASSUV/XEXTSV)) * XIND_10V 
	 + (1-XIND_10V) * TSASSUV;
XTSNNC = arr( positif(XTSNC) * 
         XTSNC  
         * (TSASSUC/XEXTSC)) * XIND_10C 
	 + (1-XIND_10C) * TSASSUC;
XETSNNV = arr( positif(XTSNV) * 
         XTSNV  
         * (XETRANV/XEXTSV)) * XIND_10V
	 + (1-XIND_10V) * XETRANV;
XETSNNC = arr( positif(XTSNC) * 
         XTSNC  
         * (XETRANC/XEXTSC)) * XIND_10C
	 + (1-XIND_10C) * XETRANC;
XEXOCETV = arr( positif(XTSNV) * 
         XTSNV  
         * (EXOCETV/XEXTSV)) * XIND_10V 
	 + (1-XIND_10V) * EXOCETV;
XEXOCETC = arr( positif(XTSNC) * 
         XTSNC  
         * (EXOCETC/XEXTSC)) * XIND_10C 
	 + (1-XIND_10C) * EXOCETC;
XEXOCET = somme(i=V,C:XEXOCETi);

regle 871090:
application : iliad , batch ;
 

XELU = ELURASC + ELURASV ;

regle 871100:
application : iliad , batch ;
 

PVTAUX = (BPVSJ + BPVSK + BPV18V + BPCOPTV + BPV40V + PEA + GAINPEA + COD3WI + COD3WJ) ;

regle 871110:
application : iliad , batch ;
 
GLN3NET = arr(GLN3 * GL3 / REV3);
QUOKIRE =   TEGL3 + RPQ4
            + somme (x=V,C,1..4 : TERPQPRRx+TERPQPRRZx+ TEGLFx+ TERPQTSx+ TERPQTSREMPx+TERPQPALIMx)
            + TERPQRF1 + TEGLRF2 + TERPQRCMDC + TERPQRCMFU + TERPQRCMCH
            + TERPQRCMTS + TERPQRCMGO + TERPQRCMTR + TERPQRVO + TERPQRVO5 + TERPQRVO6 + TERPQRVO7;

regle 871120:
application : iliad , batch ;


VARREVKIRE =  BPCAPTAXV + BPCAPTAXC
              + XBAMIC
	      + somme( i=V,C,P: XBAi+XBIPi+XBINPi+XBNi+XBNNPi)
              + somme (i=V,C,P: MIBEXi + MIBNPEXi + BNCPROEXi + XSPENPi)
              + somme (i=V,C,P: BNCCRi)
              + somme (i=V,C,P: BNCCRFi)
              + somme (i=V,C: XETSNNi)
              + somme (i=V,C: XEXOCETi)
              + somme (i=V,C: XTSNNi)
              + RCMLIB + PPLIB 
              + GAINABDET
              + RCMEXCREF
              + RCM2FA
              + XELU 
              + RCMIMPAT
              + PVIMMO
              + PVMOBNR
	      + COD3TZ
	      + COD3WN + COD3WO
              + PVTITRESOC
              + BATMARGTOT
              + max(0,BTP3A)
              + (1-positif(present(TAX1649)+present(RE168))) * (
                PVTAUX                                                                      )
              + COD1UZ + COD1VZ
	      ;
PVTXEFF2 = max(0,(BPVRCM + COD3SG + COD3SL + ABDETPLUS + COD3VB + COD3VO + COD3VP + COD3VY + ABIMPPV+arr(CODRVG/CODNVG) - ABIMPMV)) ;
PVTXEFF = (PVTAXSB + BPVRCM - PVTXEFF2) * positif(present(IPTEFN) + present(IPTEFP)) ;
REVKIRE = (1-null(IND_TDR)) *
 arr (
       max ( 0, ( 
                  (1-positif(max(0,VARIPTEFP-PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF)) * (RI1RFR)  
                  + positif(max(0,VARIPTEFP-PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF) 
					 * max(0,VARIPTEFP-PVTXEFF)
                  + positif(max(0,VARIPTEFP-PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF) 
                  * positif(- TEFFN - DRBG - RNIDF + (APERPV + APERPC + APERPP)* (1 - V_CNR) + QUOKIRE)*
		   ( (APERPV + APERPC + APERPP)* (1 - V_CNR)
		   + QUOKIRE
                   ) 
                  + max(0,TEFFREVTOTRFR*INDTEFF) * (1-positif(max(0,VARIPTEFP- PVTXEFF)))
                )
                * (1-present(IND_TDR))
                + 
                   IND_TDR
		   + (1-positif(max(0,VARIPTEFP - PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF)) * 
		     (QUOKIRE + (APERPV + APERPC + APERPP)* (1 - V_CNR) )
		   +  VARREVKIRE
             - TAX1649 - RE168)
       ) 
       ;

BNCCREAV = BNCCRV + BNCCRFV ;
BNCCREAC = BNCCRC + BNCCRFC ;
BNCCREAP = BNCCRP + BNCCRFP ;
QUOKIREHR =   TGL1 + TGL2 + TGL3 + TGL4
             + somme (x=V,C,1..4 : TGLPRRx+TGLPRRZx+ TGLFx+ TGLTSx+ TGLTSREMPx+TGLPALIMx)
             + TGLRF1 + TGLRF2 + TGLRCMDC + TGLRCMFU + TGLRCMCH
             + TGLRCMTS + TGLRCMGO + TGLRCMTR + TGLRVO + TGLRVO5 + TGLRVO6 + TGLRVO7;
REVKIREHR =  ((1-null(IND_TDR)) *
 arr (
       max ( 0, ( 
                  (1-positif(max(0,VARIPTEFP-PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF)) * (RI1RFRHR)  
                  + positif(max(0,VARIPTEFP-PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF) 
				* max(0,VARIPTEFP-PVTXEFF)
                  + positif(max(0,VARIPTEFP-PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF) 
                   * positif(- TEFFN - DRBG - RNIDF + (APERPV + APERPC + APERPP)* (1 - V_CNR) + QUOKIREHR)*
		   ( (APERPV + APERPC + APERPP)* (1 - V_CNR) 
		   + QUOKIREHR 
                   ) 
                  + max(0,TEFFREVTOTRFRHR*INDTEFF) * (1-positif(max(0,VARIPTEFP- PVTXEFF)))
                )
                * (1-present(IND_TDR))
                + 
                   IND_TDR
		   + (1-positif(max(0,VARIPTEFP- PVTXEFF) +(VARIPTEFN+PVTXEFF*present(IPTEFN))+INDTEFF)) * 
		     (QUOKIREHR + (APERPV + APERPC + APERPP)* (1 - V_CNR)) 
		   +  VARREVKIRE + COD1TZ - COD3WN - COD3WO
             - TAX1649 - RE168)
       ) 
              ) * (1-present(COD8YM)) + COD8YM ;

regle 871130 :
application : batch , iliad ;

PPE_DATE_DEB = positif(V_0AV+0) * positif(V_0AZ+0) * (V_0AZ+0)
              + positif(DATRETETR+0) * (DATRETETR+0) * null(V_0AZ+0) ;

PPE_DATE_FIN = positif(BOOL_0AM) * positif(V_0AZ+0) * (V_0AZ+0)
               + positif(DATDEPETR+0) * (DATDEPETR+0) * null(V_0AZ+0) ;

PPE_DEBJJMMMM =  PPE_DATE_DEB + (01010000+V_ANREV) * null(PPE_DATE_DEB+0);
PPE_DEBJJMM = arr( (PPE_DEBJJMMMM - V_ANREV)/10000);
PPE_DEBJJ =  inf(PPE_DEBJJMM/100);
PPE_DEBMM =  PPE_DEBJJMM -  (PPE_DEBJJ*100);
PPE_DEBUT = PPE_DEBJJ + (PPE_DEBMM - 1 ) * 30 ;

PPE_FINJJMMMM =  PPE_DATE_FIN + (30120000+V_ANREV) * null(PPE_DATE_FIN+0);
PPE_FINJJMM = arr( (PPE_FINJJMMMM - V_ANREV)/10000);
PPE_FINJJ =  inf(PPE_FINJJMM/100);
PPE_FINMM =  PPE_FINJJMM -  (PPE_FINJJ*100);
PPE_FIN = PPE_FINJJ + (PPE_FINMM - 1 ) * 30 - positif (PPE_DATE_FIN) ;

CDEVDUR_NBJ = max(1, arr(min(360 , PPE_FIN - PPE_DEBUT + 1))) ;
CKIREDUR = arr(REVKIRE * 360/CDEVDUR_NBJ);
REVKIREDUR2 = CKIREDUR ;

regle 871140 :
application : bareme ;

REVKIRE =  V_9ZZ;
