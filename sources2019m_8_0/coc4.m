#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2020]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2020 
#au titre des revenus perçus en 2019. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
verif 1700:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   RDCOM > 0
   et
   SOMMEA700 = 0

alors erreur A700 ;
verif 1702:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
    ((V_REGCO +0) dans (1,3,5,6)
    ou
    (VALREGCO+0)  non dans (2))
   et
   INTDIFAGRI * positif(INTDIFAGRI) + 0 > RCMHAB * positif(RCMHAB) + 0

alors erreur A702 ;
verif 1703:
application :  iliad ;

si
 (V_MODUL+0) < 1
   et
 (
  ( (positif(PRETUD+0) = 1 ou positif(PRETUDANT+0) = 1)
   et
    V_0DA < 1979
   et
    positif(BOOL_0AM+0) = 0 )
  ou
  ( (positif(PRETUD+0) = 1 ou positif(PRETUDANT+0) = 1)
   et
   positif(BOOL_0AM+0) = 1
   et
   V_0DA < 1979
   et
   V_0DB < 1979 )
  )
alors erreur A703 ;
verif 1704:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   (positif(CASEPRETUD + 0) = 1 et positif(PRETUDANT + 0) = 0)
   ou
   (positif(CASEPRETUD + 0) = 0 et positif(PRETUDANT + 0) = 1)

alors erreur A704 ;
verif 17071:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDENS + RDENL + RDENU > V_0CF + V_0DJ + V_0DN + 0

alors erreur A70701 ;
verif 17072:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDENSQAR + RDENLQAR + RDENUQAR > V_0CH + V_0DP + 0

alors erreur A70702 ;
verif 1708:
application : iliad ;


si
  (V_MODUL + 0) < 1
  et
  V_IND_TRAIT > 0
  et
  (
   COD7UY + 0 > LIMLOC2
   ou
   COD7UZ + 0 > LIMLOC2
  )

alors erreur A708 ;
verif 1709:
application :  iliad ;


si
  (V_MODUL + 0) < 1
  et
  positif(COD7UY) + positif(COD7UZ) + 0 > 1

alors erreur A709 ;
verif 17111:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   INAIDE > 0
   et
   positif( CREAIDE + 0) = 0

alors erreur A71101 ;
verif 17112:
application :  iliad ;
si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   ASCAPA >0
   et 
   positif (CREAIDE + 0) = 0

alors erreur A71102 ;
verif 17113:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
  et
   PREMAIDE > 0
   et
   positif(CREAIDE + 0) = 0

alors erreur A71103 ;
verif 17121:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   PRESCOMP2000 + 0 > PRESCOMPJUGE
   et
   positif(PRESCOMPJUGE) = 1

alors erreur A712 ;
verif non_auto_cc 1713:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   (PRESCOMPJUGE + 0 > 0 et PRESCOMP2000 + 0 = 0)
   ou
   (PRESCOMPJUGE + 0 = 0 et PRESCOMP2000 + 0 > 0)

alors erreur A713 ;
verif 1714:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDPRESREPORT + 0 > 0
   et
   PRESCOMPJUGE + PRESCOMP2000 + 0 > 0

alors erreur A714 ;
verif 1715:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   RDPRESREPORT + 0 > LIM_REPCOMPENS

alors erreur A715 ;
verif 1716:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   ((SUBSTITRENTE < PRESCOMP2000 + 0)
    ou
    (SUBSTITRENTE > 0 et present(PRESCOMP2000) = 0))

alors erreur A716 ;
verif 1718:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   CIAQCUL > 0
   et
   SOMMEA718 = 0

alors erreur A718 ;
verif 1719:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDMECENAT > 0
   et
   SOMMEA719 = 0

alors erreur A719 ;
verif 1731:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   CASEPRETUD + 0 > 5

alors erreur A731 ;
verif 17363:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif(PINELQI) + positif(PINELQJ) + positif(PINELQK) + positif(PINELQL) + 0 > 2

alors erreur A73603 ;
verif 17364:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif(PINELQM) + positif(PINELQN) + positif(PINELQO) + positif(PINELQP) + 0 > 2

alors erreur A73604 ;
verif 17365:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
  positif(COD7QR) + positif(COD7QS) + positif(COD7QT) + positif(COD7QU) + 0 > 2

alors erreur A73605 ;  
verif 17366:
application :  iliad ;

si
   (V_MODUL+0) < 1
    et
   V_IND_TRAIT > 0
   et
   positif(COD7QW) + positif(COD7QX) + positif(COD7QY) + positif(COD7QQ) + positif(COD7NA) + positif(COD7NB) + positif(COD7NC) + positif(COD7ND) + 0 > 2

alors erreur A73606 ;
verif 17401:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   (CODHCM + CODHCR + CODHCW  + CODHAO + CODHAT + CODHAY + CODHBG + CODHBR + CODHBM 
    + CODHBW + CODHCB + CODHCG + CODHDM + CODHDR + CODHDW + CODHER + CODHEW + CODHFR + CODHFW + 0) > PLAF_INVDOM6 

alors erreur A74001 ;
verif 17402:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   (CODHCM + CODHCR + CODHCW + CODHAO 
    + CODHAT + CODHAY + CODHBG + CODHBR + CODHBM + CODHBW + CODHCB + CODHCG 
    + CODHAE + CODHAJ + CODHDM + CODHDR + CODHDW 
    + CODHER + CODHEW + CODHFR + CODHFW + 0) > PLAF_INVDOM5

alors erreur A74002 ;
verif 1744:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 present(COD7WK) + present(COD7WQ) + present(COD7WH) + present(COD7WS) > 0
 et
 present(COD7WK) + present(COD7WQ) + present(COD7WH) + present(COD7WS) < 4

alors erreur A744;
verif 1745:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif_ou_nul(COD7ZW + COD7ZX + COD7ZY + COD7ZZ) = 1
   et
   positif_ou_nul(COD7ZW) + positif_ou_nul(COD7ZX) + positif_ou_nul(COD7ZY) + positif_ou_nul(COD7ZZ) < 4

alors erreur A745 ;
verif 17461:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 V_IND_TRAIT > 0
 et
  CODHFR * positif(CODHFR + 0) > CODHFQ * positif(CODHFQ + 0) + 0

alors erreur A74601 ;
verif 17462:
application :  iliad ;

si
 (V_MODUL+0) < 1
   et
 V_IND_TRAIT > 0
 et
  CODHFW * positif(CODHFW + 0) > CODHFV * positif(CODHFV + 0) + 0

alors erreur A74602 ;
verif 1747:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   FIPDOMCOM + 0 > 0
   et
    (V_REGCO  = 2
   ou
  VALREGCO = 2) 

alors erreur A747 ;
verif 1752:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif_ou_nul(COD7XD + COD7XE + COD7XF + COD7XG) = 1
   et
   positif_ou_nul(COD7XD) + positif_ou_nul(COD7XE) + positif_ou_nul(COD7XF) + positif_ou_nul(COD7XG) < 4

alors erreur A752 ;
verif 1761: 
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   APPLI_OCEANS = 0
   et
  (
  (CIGARD > 0
  et
  1 - V_CNR > 0
  et
  positif(RDGARD1) + positif(RDGARD2) + positif(RDGARD3) + positif(RDGARD4) > EM7 + 0)
  ou
 (CIGARD > 0
  et
  1 - V_CNR > 0
  et
  positif(RDGARD1QAR) + positif(RDGARD2QAR) + positif(RDGARD3QAR) + positif(RDGARD4QAR) > EM7QAR + 0)
  )

alors erreur A761 ;
