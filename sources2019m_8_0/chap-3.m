#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2020]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2020 
#au titre des revenus perçus en 2019. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
                                                                         #####
  ####   #    #    ##    #####      #     #####  #####   ######         #     #
 #    #  #    #   #  #   #    #     #       #    #    #  #                    #
 #       ######  #    #  #    #     #       #    #    #  #####           #####
 #       #    #  ######  #####      #       #    #####   #                    #
 #    #  #    #  #    #  #          #       #    #   #   #              #     #
  ####   #    #  #    #  #          #       #    #    #  ###### #######  #####
 #
 #
 #
 #
 #
 #
 #
 #                       CALCUL DE L'IMPOT NET
 #
 #
 #
 #
 #
 #
regle 301000:
application : bareme , iliad ;

IRN = min(0 , IAN + AVFISCOPTER - IRE) + max(0 , IAN + AVFISCOPTER - IRE) * positif( IAMD1 + 1 - SEUIL_61) ;


regle 301005:
application : bareme , iliad ;
IRNAF = min( 0, IAN - IRE) + max( 0, IAN - IRE) * positif( IAMD1AF + 1 - SEUIL_61) ;

regle 301010:
application : bareme , iliad ;


IAR = min( 0, IAN + AVFISCOPTER - IRE) + max( 0, IAN + AVFISCOPTER - IRE) ;

regle 301015:
application : bareme , iliad ;

IARAF = min(0 , IANAF - IREAF) + max(0 , IANAF - IREAF) + NEGIANAF ;

regle 301020:
application : iliad ;

CREREVET = min(arr((BPTP3 + BPTPD + BPTPG) * TX128/100),arr(CIIMPPRO * TX128/100 ))
	   + min(arr(BPTP19 * TX19/100),arr(CIIMPPRO2 * TX19/100 ))
	   + min (arr(RCMIMPTR * TX075/100),arr(COD8XX * TX075/100)) 
	   + min (arr(BPTP10 * TX10/100),arr(COD8XV * TX10/100)) 
	   ;

CIIMPPROTOT = CIIMPPRO + CIIMPPRO2 + COD8XX + COD8XX + COD8XV;

regle 301030:
application : iliad ;

ICI8XFH = min(arr(BPTP18 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));
ICIGLO = min(arr(BPTP18 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));

CIGLOTOT = COD8XF + COD8XG + COD8XH; 
regle 301032:
application : iliad ;

CI8XFH = max(0 , min(IRB + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET , ICI8XFH)) ;

CIGLO = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET , ICIGLO)) ;

regle 301035:
application : iliad ;

CI8XFHAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IRETSAF - ICREREVETAF , ICI8XFH)) ;

CIGLOAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IRETSAF - ICREREVETAF , ICIGLO)) ;

regle 301040:
application : iliad ;


ICREREVET = max(0 , min(IAD11 + CIMREP + ITP - CIRCMAVFT - IRETS , min(ITP , CREREVET))) ;

regle 301045:
application : iliad ;

ICREREVETAF = max(0 , min(IAD11 + CIMREP + ITP - CIRCMAVFTAF - IRETSAF , min(ITP , CREREVET))) ;

regle 301050:
application : iliad , bareme ;

INE = (CIRCMAVFT + IRETS + ICREREVET + CIGLO + CICULTUR + CIDONENTR + CICORSE + CIRECH + CICOMPEMPL) * (1 - positif(RE168 + TAX1649)) ;

IAN = max(0 , (IRB + CIMREP - AVFISCOPTER - INE
               + min(TAXASSUR + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER)) 
               + min(IPCAPTAXTOT + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER - min(TAXASSUR + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER))))
               + min(TAXLOY + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER - min(IPCAPTAXTOT + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER 
	                                                                    - min(TAXASSUR + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER)))) 
	                                                                    - min(TAXASSUR + 0 , max(0 , INE - IRB - CIMREP + AVFISCOPTER))))
	      )
         ) ;

regle 301055:
application : iliad , bareme ;

INEAF = (CIRCMAVFTAF + IRETSAF + ICREREVETAF + CIGLOAF + CICULTURAF + CIDONENTRAF + CICORSEAF + CIRECHAF + CICOMPEMPLAF)
            * (1-positif(RE168+TAX1649));
regle 301057:
application : iliad , bareme ;

IANAF = max( 0, (IRBAF  + ((- CIRCMAVFTAF
				     - IRETSAF
                                     - ICREREVETAF
                                     - CIGLOAF
                                     - CICULTURAF
                                     - CIDONENTRAF
                                     - CICORSEAF
				     - CIRECHAF
                                     - CICOMPEMPLAF)
                                   * (1 - positif(RE168 + TAX1649)))
                  + min(TAXASSUR+0 , max(0,INEAF-IRBAF)) 
                  + min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))
                  + min(TAXLOY+0 ,max(0,INEAF-IRBAF- min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))
										  - min(TAXASSUR+0 , max(0,INEAF-IRBAF))))
	      )
         )
 ;
NEGIANAF =   -1 * (min(TAXASSUR+0 , max(0,INEAF-IRBAF))
                  + min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))
                  + min(TAXLOY+0 ,max(0,INEAF-IRBAF- min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF)))) - min(TAXASSUR+0 , max(0,INEAF-IRBAF)))));
regle 301060:
application : iliad ;


IRE = (EPAV + CRICH + CICORSENOW + CIGE + CIDEVDUR + CITEC + CICA + CIGARD + CISYND 
       + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM + COD8TZ + CREAGRIBIO + CRESINTER 
       + CREFORMCHENT + CREARTS + CICONGAGRI + CRERESTAU + AUTOVERSLIB
       + CI2CK + CIFORET + CIHJA + COD8TL * (1 - positif(RE168 + TAX1649))
       + CIMRCO * (1 - V_INDTEO)) * (1 - positif(RE168 + TAX1649 + 0)) ;
IREAF = (EPAV + CRICH + CICORSENOW + CIGE + CIDEVDUR + CITEC + CICA + CIGARD + CISYND 
       + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM + COD8TZ + CREAGRIBIO + CRESINTER 
       + CREFORMCHENT + CREARTS + CICONGAGRI + CRERESTAU + AUTOVERSLIB
       + CI2CK + CIFORET + CIHJA + COD8TL * (1 - positif(RE168 + TAX1649))
        ) * (1 - positif(RE168 + TAX1649 + 0)) ;

IRE2 = IRE ; 

regle 301065:
application : iliad ;

CIHJA = CODHJA * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;

regle 301070:
application : iliad ;

CRICH =  IPRECH * (1 - positif(RE168+TAX1649));

regle 301080:
application : iliad ;


CIRCMAVFT = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER , RCMAVFT * (1 - V_CNR)));

regle 301085:
application : iliad ;

CIRCMAVFTAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY , RCMAVFT * (1 - V_CNR)));

regle 301100:
application : iliad;
CI2CK = COD2CK * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);

regle 301110:
application : iliad;


CICA =  arr(BAILOC98 * TX_BAIL / 100) * (1 - positif(RE168 + TAX1649)) ;

regle 301130:
application : iliad ;


IPAE = COD8VL + COD8VM + COD8WM + COD8UM ;

RASIPSOUR =  IPSOUR * positif( null(V_REGCO-2) + null(V_REGCO-3) ) * ( 1 - positif(RE168+TAX1649) );

RASIPAE = COD8VM + COD8WM + COD8UM ;

regle 301133:
application : iliad ;

IRETS1 = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT , RASIPSOUR)) ;

IRETS21 = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IRETS1 , min(COD8PB , COD8VL) * present(COD8PB) + COD8VL * (1 - present(COD8PB)))) 
          * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) ;

IRETS2 = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IRETS1 - IRETS21 , min(COD8PA , RASIPAE) * present(COD8PA) + RASIPAE * (1 - present(COD8PA)))) 
         * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) + IRETS21 ;
	
IRETS = IRETS1 + IRETS2 ;

regle 301135:
application : iliad ;

IRETS1AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF , RASIPSOUR)) ;

IRETS21AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IRETS1AF , min(COD8PB , COD8VL) * present(COD8PB) + COD8VL * (1 - present(COD8PB))))
            * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) ;

IRETS2AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IRETS1AF - IRETS21AF , min(COD8PA , RASIPAE) * present(COD8PA) + RASIPAE * (1 - present(COD8PA))))
           * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) + IRETS21AF ;
	
IRETSAF = IRETS1AF + IRETS2AF ;

regle 301150:
application : iliad ;

BCIAQCUL = arr(CIAQCUL * TX_CIAQCUL / 100);

regle 301152:
application : iliad ;

CICULTUR = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - REI - IRETS - ICREREVET - CIGLO , min(IAD11+ITP+TAXASSUR+TAXLOY+IPCAPTAXT , BCIAQCUL))) ;

regle 301155:
application : iliad ;

CICULTURAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - REI - IRETSAF - ICREREVETAF - CIGLOAF , min(IAD11+ITP+TAXASSUR+TAXLOY+IPCAPTAXT , BCIAQCUL))) ;

regle 301170:
application : iliad ;

BCIDONENTR = RDMECENAT * (1-V_CNR) ;

regle 301172:
application : iliad ;

CIDONENTR = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - REI - IRETS - ICREREVET - CIGLO - CICULTUR , BCIDONENTR)) ;

regle 301175:
application : iliad ;

CIDONENTRAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - REI - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF , BCIDONENTR)) ;

regle 301180:
application : iliad ;

CICORSE = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IPPRICORSE - IRETS - ICREREVET - CIGLO - CICULTUR - CIDONENTR , CIINVCORSE + IPREPCORSE)) ;

CICORSEAVIS = CICORSE + CICORSENOW ;

TOTCORSE = CIINVCORSE + IPREPCORSE + CICORSENOW ;

regle 301185:
application : iliad ;

CICORSEAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IPPRICORSE - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF - CIDONENTRAF , CIINVCORSE + IPREPCORSE)) ;

CICORSEAVISAF = CICORSEAF + CICORSENOW ;

regle 301190:
application : iliad ;

CIRECH = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET - CIGLO - CICULTUR - CIDONENTR - CICORSE , IPCHER)) ;

regle 301195:
application : iliad ;

CIRECHAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF - CIDONENTRAF - CICORSEAF , IPCHER)) ;
regle 301200:
application : iliad ;

CICOMPEMPL = max(0 , min(IRB + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET - CIGLO - CICULTUR - CIDONENTR - CICORSE - CIRECH , COD8UW)) ;

DIEMPLOI = (COD8UW + COD8TL) * (1 - positif(RE168+TAX1649)) ;

CIEMPLOI = (CICOMPEMPL + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECR = abs(min(0 ,IRB+TAXASSUR+IPCAPTAXT+TAXLOY -AVFISCOPTER-CIRCMAVFT-IRETS-ICREREVET-CIGLO-CICULTUR-CIDONENTR-CICORSE-CIRECH-CICOMPEMPL));

regle 301205:
application : iliad ;

CICOMPEMPLAF = max(0 , min(IRBAF + CIMREP + TAXASSUR + IPCAPTAXT + TAXLOY - CIRCMAVFTAF - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF - CIDONENTRAF - CICORSEAF - CIRECHAF , COD8UW)) ;

CIEMPLOIAF = (CICOMPEMPLAF + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECRAF = abs(min(0 ,IRBAF+TAXASSUR+IPCAPTAXT+TAXLOY -CIRCMAVFTAF-IRETSAF-ICREREVETAF-CIGLOAF-CICULTURAF-CIDONENTRAF-CICORSEAF-CIRECHAF-CICOMPEMPLAF));

regle 301210:
application : iliad ;
  
REPCORSE = abs(CIINVCORSE+IPREPCORSE-CICORSE) ;
REPRECH = abs(IPCHER - CIRECH) ;
REPCICE = abs(COD8UW - CICOMPEMPL) ;

regle 301220:
application : iliad ;

CICONGAGRI = CRECONGAGRI * (1-V_CNR) ;

regle 301230:
application : iliad ;

BCICAP = arr(PRELIBXT * TX90/100 * T_PCAPTAX/100) ;

regle 301233:
application : iliad ;

BCICAPAVIS = arr(PRELIBXT * TX90/100) ;

CICAP = max(0 , min(IPCAPTAXTOT , BCICAP)) ;

regle 301235:
application : iliad ;

CICAPAF = CICAP ;

regle 301240:
application : iliad ;

BCICHR = arr(CHRAPRES * (REGCI*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR - TEFFHRC+COD8YJ));
regle 301242:
application : iliad ;

CICHR = max(0,min(IRB + TAXASSUR + IPCAPTAXT +TAXLOY +CHRAPRES - AVFISCOPTER ,min(CHRAPRES,BCICHR)));
regle 301245:
application : iliad ;

CICHRAF = max(0,min(IRBAF + TAXASSUR + IPCAPTAXT +TAXLOY +CHRAPRES ,min(CHRAPRES,BCICHR)));
regle 301247:
application : iliad ;

BCICHR3WH = arr(CHRAPRES3WH * (REGCI*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR+PVREPORT - TEFFHRC+COD8YJ));

regle 301249:
application : iliad ;

CICHR3WH = max(0,min(IRB + TAXASSUR + IPCAPTAXT +TAXLOY +CHRAPRES3WH - AVFISCOPTER ,min(CHRAPRES3WH,BCICHR3WH)));

regle 301252:
application : iliad ;

CICHR3WHAF = max(0,min(IRBAF + TAXASSUR + IPCAPTAXTOT +TAXLOY +CHRAPRES3WH -CICAPAF ,min(CHRAPRES3WH,BCICHR3WH)));

regle 301257:
application : iliad ;



DSYND = RDSYVO + RDSYCJ + RDSYPP ;


SOMBCOSV = TSHALLOV + COD1AA + CARTSV + ALLOV + REMPLAV + COD1GB + COD1GF + COD1GG + COD1AF  
           + CODRAF + COD1AG + CODRAG + PRBRV + CARPEV + PALIV + PENSALV + CODDAJ + CODEAJ 
	   + PENINV + CODRAZ + COD1AL + CODRAL + COD1AM + CODRAM + COD1TP + GLDGRATV 
	   + COD1TZ + COD1NX ;

SOMBCOSC = TSHALLOC + COD1BA + CARTSC + ALLOC + REMPLAC + COD1HB + COD1HF + COD1HG + COD1BF 
           + CODRBF + COD1BG + CODRBG + PRBRC + CARPEC + PALIC + PENSALC + CODDBJ + CODEBJ 
	   + PENINC + CODRBZ + COD1BL + CODRBL + COD1BM + CODRBM + COD1UP + GLDGRATC + COD1OX ;

SOMBCOSP = TSHALLO1 + TSHALLO2 + TSHALLO3 + TSHALLO4 + COD1CA + COD1DA + COD1EA + COD1FA   
           + CARTSP1 + CARTSP2 + CARTSP3 + CARTSP4 + ALLO1 + ALLO2 + ALLO3 + ALLO4    
           + REMPLAP1 + REMPLAP2 + REMPLAP3 + REMPLAP4 + COD1IB + COD1IF + COD1JB   
           + COD1JF + COD1KF + COD1LF + COD1CF + COD1DF + COD1EF + COD1FF   
           + CODRCF + CODRDF + CODREF + CODRFF + COD1CG + COD1DG + COD1EG + COD1FG    
           + CODRCG + CODRDG + CODRGG + CODRFG + PRBR1 + PRBR2 + PRBR3 + PRBR4     
           + CARPEP1 + CARPEP2 + CARPEP3 + CARPEP4 + PALI1 + PALI2 + PALI3 + PALI4     
           + PENSALP1 + PENSALP2 + PENSALP3 + PENSALP4 + PENIN1 + PENIN2 + PENIN3 + PENIN4    
           + CODRCZ + CODRDZ + CODREZ + CODRFZ + COD1CL + COD1DL + COD1EL + COD1FL    
           + CODRCL + CODRDL + CODREL + CODRFL + COD1CM + COD1DM + COD1EM + COD1FM    
           + CODRCM + CODRDM + CODREM + CODRFM + COD1IG + COD1JG + COD1KG + COD1LG ;


BCOS = min(RDSYVO+0,arr(TX_BASECOTSYN/100*SOMBCOSV*IND_10V))
      +min(RDSYCJ+0,arr(TX_BASECOTSYN/100*SOMBCOSC*IND_10C))                             
      +min(RDSYPP+0,arr(TX_BASECOTSYN/100*SOMBCOSP*IND_10P));

ASYND = BCOS * (1-V_CNR) ;


CISYND = arr(TX_REDCOTSYN/100 * BCOS) * (1 - V_CNR) ;

regle 301260:
application : iliad ;


IAVF = IRE - EPAV + CICORSE + CICULTUR + CIRCMAVFT ;


DIAVF2 = (IPRECH + IPCHER + RCMAVFT ) * (1 - positif(RE168+TAX1649)) + CIRCMAVFT * positif(RE168+TAX1649);


IAVF2 = (IPRECH + CIRECH + CIRCMAVFT + 0) * (1 - positif(RE168 + TAX1649))
        + CIRCMAVFT * positif(RE168 + TAX1649) ;

IAVFGP = IAVF2 + CREFAM + COD8TZ ;

regle 301270:
application : iliad ;


I2DH = EPAV ;

regle 301280:
application : iliad ;


BTANTGECUM   = (V_BTGECUM * (1 - positif(present(COD7ZZ)+present(COD7ZY)+present(COD7ZX)+present(COD7ZW))) + COD7ZZ+COD7ZY+COD7ZX+COD7ZW);

BTANTGECUMWL = V_BTPRT4 * (1 - present(COD7WK)) + COD7WK 
               + V_BTPRT3 * (1 - present(COD7WQ)) + COD7WQ 
	       + V_BTPRT2 * (1- present (COD7WH)) + COD7WH
	       + V_BTPRT1 * (1- present (COD7WS)) + COD7WS
	       ;

P2GE = max( (   PLAF_GE2 * (1 + BOOL_0AM)
             + PLAF_GE2_PACQAR * (V_0CH + V_0DP)
             + PLAF_GE2_PAC * (V_0CR + V_0CF + V_0DJ + V_0DN)  
              ) - BTANTGECUM
             , 0
             ) ;
BGEPAHA = min(RDEQPAHA +COD7WI, P2GE) * (1 - V_CNR);

P2GEWL = max(0,PLAF20000 - BTANTGECUMWL);
BGTECH = min(RDTECH , P2GEWL) * (1 - V_CNR) ;

BGEDECL = RDTECH + RDEQPAHA + COD7WI;
TOTBGE = BGTECH + BGEPAHA ;

RGEPAHA =  (BGEPAHA * TX25 / 100 );
RGTECH = (BGTECH * TX40 / 100); 
CIGE = arr (RGTECH + RGEPAHA ) * (1 - positif(RE168 + TAX1649));


DEPENDPDC = BGEPAHA ;


DEPENPPRT = BGTECH ;

GECUM = min(P2GE,BGEPAHA)+(V_BTGECUM * (1 - positif(present(COD7ZY)+present(COD7ZX)+present(COD7ZW))) + COD7ZW +COD7ZX + COD7ZY);
GECUMWL = max(0,BGTECH + BTANTGECUMWL) ;

BADCRE = min(CREAIDE , min((LIM_AIDOMI * (1 - positif(PREMAIDE)) + LIM_PREMAIDE * positif(PREMAIDE)
                            + MAJSALDOM * (positif_ou_nul(ANNEEREV-V_0DA-65) + positif_ou_nul(ANNEEREV-V_0DB-65) * BOOL_0AM
                                           + V_0CF + V_0DJ + V_0DN + (V_0CH + V_0DP)/2+ASCAPA)
                           ) , LIM_AIDOMI3 * (1 - positif(PREMAIDE)) + LIM_PREMAIDE2 * positif(PREMAIDE) ) * (1-positif(INAIDE + 0))
                               +  LIM_AIDOMI2 * positif(INAIDE + 0)) ;

DAIDC = CREAIDE ;
AAIDC = BADCRE * (1-V_CNR) ;
CIADCRE = arr (BADCRE * TX_AIDOMI /100) * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;

regle 301300:
application : iliad ;


DDEVDUR = COD7CB + COD7AA + COD7AB + COD7AD + COD7AF + COD7AH + COD7AP + COD7AR + COD7AV + COD7AX + COD7AS 
	  + COD7AY + COD7AZ + COD7BB + COD7BC + COD7BM + COD7BN + COD7BD + COD7BE + COD7BF + COD7BH + COD7BK + COD7BL + COD7BQ;
RFRDEVDUR = min(V_BTRFRN2*(1-present(RFRN2))+RFRN2,V_BTRFRN1*(1-present(RFRN1))+RFRN1) * positif(positif(present(V_BTRFRN2)+present(RFRN2))*positif(present(V_BTRFRN1)+present(RFRN1)))
          + (V_BTRFRN2*(1-present(RFRN2))+RFRN2) * positif(positif(present(V_BTRFRN2)+present(RFRN2))*(1-positif(present(V_BTRFRN1)+present(RFRN1))));
	  
VARIDF = V_INDIDF * positif(14 - V_NOTRAIT) + (V_INDIDF * (1 - CODIDF) + (1 - V_INDIDF) * CODIDF) * (1 - positif(14 - V_NOTRAIT)) ;

INDRES =   positif(positif(V_BTRFRN2+RFRN2)*positif(V_BTRFRN1+RFRN1) + positif(V_BTRFRN2 + RFRN2)) *(
                   VARIDF * (positif_ou_nul(LIM_IDF1 - RFRDEVDUR) * null(1-NBPERS)
	                   + positif_ou_nul(LIM_IDF2 - RFRDEVDUR) * null(2-NBPERS) 
	                   + positif_ou_nul(LIM_IDF3 - RFRDEVDUR) * null(3-NBPERS) 
	                   + positif_ou_nul(LIM_IDF4 - RFRDEVDUR) * null(4-NBPERS) 
	                   + positif_ou_nul(LIM_IDF5 - RFRDEVDUR) * null(5-NBPERS) 
	                   + positif_ou_nul(LIM_IDF5 + LIM_IDFSUP * (NBPERS-5) - RFRDEVDUR) * positif(NBPERS-5))
	     + (1-VARIDF) * (positif_ou_nul(LIM_NONIDF1 - RFRDEVDUR) * null(1-NBPERS) 
	                   + positif_ou_nul(LIM_NONIDF2 - RFRDEVDUR) * null(2-NBPERS) 
	                   + positif_ou_nul(LIM_NONIDF3 - RFRDEVDUR) * null(3-NBPERS) 
	                   + positif_ou_nul(LIM_NONIDF4 - RFRDEVDUR) * null(4-NBPERS) 
	                   + positif_ou_nul(LIM_NONIDF5 - RFRDEVDUR ) * null(5-NBPERS) 
	                   + positif_ou_nul(LIM_NONIDF5 + LIM_NONIDFSUP * (NBPERS-5) - RFRDEVDUR) * positif(NBPERS-5))) 
           + (1-positif(V_BTRFRN2 + RFRN2)) * 1
	                                 + 0;
PDEVDUR = max( (   PLAF_DEVDUR * (1 + BOOL_0AM)
                  + PLAF_GE2_PACQAR * (V_0CH+V_0DP)
                  + PLAF_GE2_PAC * (V_0CR+V_0CF+V_0DJ+V_0DN) 
                ) - (V_BTDEVDUR*(1-positif(present(COD7XD)+present(COD7XE)+present(COD7XF)+present(COD7XG)))+COD7XD+COD7XE+COD7XF+COD7XG) , 0 );


BDEV50 = min(PDEVDUR , COD7BQ) * INDRES + 0;
BDEV30 = min(max(0,PDEVDUR - BDEV50), COD7CB + min(LIM3350,COD7AA) +  min(LIM3350,COD7AB) + COD7AD + COD7AF + COD7AH  + COD7AR + COD7AV + min(COD7AX,LIM3000)
                       + min(LIM4000,COD7AS)*INDRES+min(LIM3000,COD7AS)*(1-INDRES) + COD7AZ + COD7BB  + COD7BM  + COD7BN * INDRES+ COD7BC + COD7BD 
		       + COD7BE + COD7BF + COD7BH + COD7BK + COD7BL+COD7AY)+ 0 ;
BDEV15 = min(max(0,PDEVDUR - BDEV50-BDEV30),min(LIM670*COD7AT,COD7AP)) + 0;

ADEVDUR = BDEV30 + BDEV15 + BDEV50; 

CIDEVDUR = arr(BDEV30 * TX30/100 + BDEV15 * TX15/100 + BDEV50 * TX50/100) * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ; 

DEVDURCUM = ADEVDUR + ((V_BTDEVDUR*(1-positif(present(COD7XD)+present(COD7XE)+present(COD7XF)+present(COD7XG))))+COD7XD+COD7XE+COD7XF+COD7XG) ;

regle 301310:
application : iliad ;

DTEC = RISKTEC;
ATEC = positif(DTEC) * DTEC;
CITEC = arr (ATEC * TX40/100);

regle 301320:
application : iliad ;

DPRETUD = PRETUD + PRETUDANT ;
APRETUD = max(min(PRETUD,LIM_PRETUD) + min(PRETUDANT,LIM_PRETUD*CASEPRETUD),0) * (1-V_CNR) ;

CIPRETUD = arr(APRETUD*TX_PRETUD/100) * (1 - positif(RE168 + TAX1649)) * (1-V_CNR) ;

regle 301330:
application : iliad ;


EM7AVRICI = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - ANNEEREV+1)))
         + somme (j=0..5: min (1 , max(0 , V_0Nj + AG_LIMFG - ANNEEREV+1)))
      + (1 - positif(somme(i=0..7:V_0Fi) + somme(i=0..5: V_0Ni) + 0)) * (V_0CF + V_0DN) ;

EM7QARAVRICI = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - ANNEEREV+1)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - ANNEEREV+1)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

EM7 = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - ANNEEREV)))
         + somme (j=0..5: min (1 , max(0 , V_0Nj + AG_LIMFG - ANNEEREV)))
      + (1 - positif(somme(i=0..7:V_0Fi) + somme(i=0..5: V_0Ni) + 0)) * (V_0CF + V_0DN) ;

EM7QAR = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - ANNEEREV)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - ANNEEREV)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

BRFG = min(RDGARD1,PLAF_REDGARD) + min(RDGARD2,PLAF_REDGARD)
       + min(RDGARD3,PLAF_REDGARD) + min(RDGARD4,PLAF_REDGARD)
       + min(RDGARD1QAR,PLAF_REDGARDQAR) + min(RDGARD2QAR,PLAF_REDGARDQAR)
       + min(RDGARD3QAR,PLAF_REDGARDQAR) + min(RDGARD4QAR,PLAF_REDGARDQAR)
       ;
RFG1 = arr ( (BRFG) * TX_REDGARD /100 ) * (1 -V_CNR);
DGARD = somme(i=1..4:RDGARDi)+somme(i=1..4:RDGARDiQAR);
AGARD = (BRFG) * (1-V_CNR) ;
CIGARD = RFG1 * (1 - positif(RE168 + TAX1649)) ;

regle 301340:
application : iliad ;


PREHAB = PREHABT + PREHABT2 + PREHABTN2 + PREHABTVT ;

BCIHP = max(( PLAFHABPRIN * (1 + BOOL_0AM) * (1+positif(V_0AP+V_0AF+V_0CG+V_0CI+V_0CR))
                 + (PLAFHABPRINENF/2) * (V_0CH + V_0DP)
                 + PLAFHABPRINENF * (V_0CR + V_0CF + V_0DJ + V_0DN)
                  )
             ,0);

BCIHABPRIN1 = min(BCIHP , PREHABT) * (1 - V_CNR) ;
BCIHABPRIN5 = min(max(0,BCIHP-BCIHABPRIN1),PREHABT2) * (1 - V_CNR);
BCIHABPRIN6 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN5),PREHABTN2) * (1 - V_CNR);
BCIHABPRIN7 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN5-BCIHABPRIN6),PREHABTVT) * (1 - V_CNR);

BCIHABPRIN = BCIHABPRIN1 + BCIHABPRIN5 + BCIHABPRIN6 + BCIHABPRIN7 ;

CIHABPRIN = arr((BCIHABPRIN1 * TX40/100)
                + (BCIHABPRIN5 * TX20/100)
                + (BCIHABPRIN6 * TX15/100)
                + (BCIHABPRIN7 * TX10/100))
                * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);

regle 301350:
application : iliad ;


BDCIFORET = COD7VQ + COD7VR + COD7TP + COD7TQ + COD7VM + COD7TM + COD7VN + COD7TO
 	    + REPSINFOR5 + COD7TK +RDFORESTRA + SINISFORET
	    + COD7UA + COD7UB + RDFORESTGES + COD7UI + COD7VS + COD7TR + COD7VL + COD7TS
	    + COD7VJ + COD7TT + COD7VK + COD7TU ;
BCIFORETTK = max(0 , min(COD7TK , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)))) * (1-V_CNR) ;
VARTMP1 = BCIFORETTK ;

BCIFORETTJ = max(0 , min(REPSINFOR5 , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTJ ;

BCIFORETVN = max(0 , min(COD7VN , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVN ;

BCIFORETTO = max(0 , min(COD7TO , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTO ;

BCIFORETVM = max(0 , min(COD7VM , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVM ;

BCIFORETTM = max(0 , min(COD7TM , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTM ;

BCIFORETVR = max(0 , min(COD7VR , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVR ;

BCIFORETTQ = max(0 , min(COD7TQ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTQ ;

BCIFORETVQ = max(0 , min(COD7VQ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVQ ;

BCIFORETTP = max(0 , min(COD7TP , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTP ;

BCIFORETVL = max(0 , min(COD7VL , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVL ;

BCIFORETTS = max(0 , min(COD7TS , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTS ;

BCIFORETVS = max(0 , min(COD7VS , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVS ;

BCIFORETTR = max(0 , min(COD7TR , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTR ;

BCIFORETVK = max(0 , min(COD7VK , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVK ;

BCIFORETTU = max(0 , min(COD7TU , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTU ;

BCIFORETVJ = max(0 , min(COD7VJ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVJ ;

BCIFORETTT = max(0 , min(COD7TT , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTT ;

BCIFORETUA = max(0 , min(COD7UA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUA ;

BCIFORETUB = max(0 , min(COD7UB , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUB ;

BCIFORETUP = max(0 , min(RDFORESTRA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUP ;

BCIFORETUT = max(0 , min(SINISFORET , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = 0 ;

BCIFORETUI = max(0 , min(COD7UI , max(0 , PLAF_FOREST2 * (1 + BOOL_0AM)))) * (1-V_CNR) ;

BCIFORETUQ = max(0 , min(RDFORESTGES , max(0 , PLAF_FOREST2 * (1 + BOOL_0AM)-BCIFORETUI))) * (1-V_CNR) ;

BCIFORET = BCIFORETTK + BCIFORETTJ + BCIFORETVN + BCIFORETTO + BCIFORETVM + BCIFORETTM + BCIFORETVR + BCIFORETTQ + BCIFORETVQ + BCIFORETTP 
           + BCIFORETVL + BCIFORETTS + BCIFORETVS + BCIFORETTR + BCIFORETVK + BCIFORETTU + BCIFORETVJ + BCIFORETTT + BCIFORETUA + BCIFORETUB 
	   + BCIFORETUP + BCIFORETUT + BCIFORETUI + BCIFORETUQ ;
	 

CIFORET = arr((BCIFORETTJ + BCIFORETVM + BCIFORETTM + BCIFORETVQ + BCIFORETTP + BCIFORETVS + BCIFORETTR + BCIFORETVJ + BCIFORETTT + BCIFORETUP + BCIFORETUT + BCIFORETUQ) * TX18/100 
              + (BCIFORETTK + BCIFORETVN + BCIFORETTO + BCIFORETVR + BCIFORETTQ + BCIFORETVL + BCIFORETTS + BCIFORETVK + BCIFORETTU + BCIFORETUA + BCIFORETUB + BCIFORETUI) * TX25/100) ;

regle 301360:
application : iliad ;



REPCIFADSSN5 = max(0 , COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM)) * (1 - V_CNR) ;

REPCIFSN5 = (positif_ou_nul(COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM)) * REPSINFOR5
             + (1 - positif_ou_nul(COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , REPSINFOR5 - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK))) * (1 - V_CNR) ;


REPCIFADSSN4 = (positif_ou_nul(COD7TK + REPSINFOR5 + COD7VN - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TO
                + (1 - positif_ou_nul(COD7TK + REPSINFOR5 + COD7VN - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TO - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK - REPSINFOR5 - COD7VN))) * (1 - V_CNR) ;

REPCIFSN4 = (positif_ou_nul(COD7TK + REPSINFOR5 + COD7VN + COD7TO + COD7VM - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TM
             + (1 - positif_ou_nul(COD7TK + REPSINFOR5 + COD7VN + COD7TO + COD7VM - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TM - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK - REPSINFOR5 + COD7VN + COD7TO + COD7VM))) * (1 - V_CNR) ;

VARTMP1 = COD7TK + REPSINFOR5 + COD7VN + COD7TO + COD7VM + COD7TM ;

REPCIFADHSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VR
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VR - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VR ;
							   
REPCIFADSSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TQ
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TQ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TQ ;
							   
REPCIFHSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VQ
              + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VQ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VQ ;
							   
REPCIFSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TP
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TP - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TP ;
							   

REPCIFADHSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VL
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VL - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VL ;

REPCIFADSSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TS
	        + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TS - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TS ;

REPCIFHSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VS
	      + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VS - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VS ;

REPCIFSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TR
	     + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TR - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TR ;
			     

REPCIFADHSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VK
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VK - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VK ;

REPCIFADSSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TU
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TU - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TU ;

REPCIFHSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VJ
              + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VJ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VJ ;

REPCIFSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TT
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TT - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TT ;


REPCIFAD = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7UA
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7UA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7UA ;
							   
REPCIFADSIN = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7UB
               + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7UB - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7UB ;
							   
REPCIF = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * RDFORESTRA
          + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , RDFORESTRA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + RDFORESTRA ;
							   
REPCIFSIN = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * SINISFORET
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , SINISFORET - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = 0 ;


regle 301365:
application : iliad ;

REP7UP = REPCIF + REPCIFHSN1 + REPCIFHSN2 + REPCIFHSN3 ;
REP7UA = REPCIFAD + REPCIFADHSN1 + REPCIFADHSN2 + REPCIFADHSN3 ;
REP7UT = REPCIFSIN + REPCIFSN1 + REPCIFSN2 + REPCIFSN3 + REPCIFSN4 + REPCIFSN5 ;
REP7UB = REPCIFADSIN + REPCIFADSSN1 + REPCIFADSSN2 + REPCIFADSSN3 + REPCIFADSSN4 + REPCIFADSSN5 ; 

regle 301370:
application : iliad ;


CICSG = min(CSGC , arr(IPPNCS * T_CSGCRDS/100)) ;

CIRDS = min(RDSC , arr((min(REVCSXA , SALECS) + min(REVCSXB , SALECSG + COD8SC)
                        + min(REVCSXC , ALLECS) + min(REVCSXD , INDECS + COD8SW)
                        + min(REVCSXE , PENECS + COD8SX) + min(COD8XI , COD8SA)
                        + min(COD8XJ , COD8SB) + min (COD8XM , GLDGRATV + GLDGRATC)
			+ min(COD8XO , COD8TH) + min (COD8XN , COD8SD)
                       ) * T_RDS/100)) ;

CIPSOL = min(MPSOL , arr(IPPNCS * TXPSOL/100)) ;

CICVN = min( CVNSALC , arr( min(BCVNSAL,COD8XL) * TX10/100 )) ;

CIGLOA = min( CGLOA , arr ( min(GLDGRATV+GLDGRATC,COD8XM) * T_CSG/100));


CIRSE1 = min( RSE1 , arr( min(SALECS,REVCSXA) * TXTQ/100 ));

RSE8TV = arr(BRSE8TV * TXTV/100) * (1 - positif(ANNUL2042));
RSE8SA = arr(BRSE8SA * TXTV/100) * (1 - positif(ANNUL2042));
CIRSE8TV = min( RSE8TV , arr( min(ALLECS,REVCSXC) * TXTV/100 )) ;
CIRSE8SA = min( RSE8SA , arr(min(COD8SA,COD8XI) * TXTV/100 )) ;
CIRSE2 = min(RSE2, arr(min(ALLECS,REVCSXC)* TXTV/100 + min(COD8SA,COD8XI) * TXTV/100));

CIRSE3 = min( RSE3 , arr( min(COD8SW+INDECS,REVCSXD * TXTW/100 )));

RSE8TX = arr(BRSE8TX * TXTX/100) * (1 - positif(ANNUL2042));
RSE8SB = arr(BRSE8SB * TXTX/100) * (1 - positif(ANNUL2042));
CIRSE8TX = min( RSE8TX , arr( REVCSXE * TXTX/100 )) ;
CIRSE8SB = min( RSE8SB , arr( COD8XJ * TXTX/100 ));
CIRSE4 =  min(RSE4, arr(min(PENECS+COD8SX,REVCSXE)* TXTX/100 + min(COD8XJ,COD8SB) * TXTX/100));

CIRSE5 = min( RSE5 , arr( min(SALECSG+COD8SC,REVCSXB) * TXTR/100 ));

CIRSE6 = min( RSE6 , arr(( min( REVCSXB , SALECSG+COD8SC ) +
                           min( REVCSXC , ALLECS ) +
                           min( COD8XI , COD8SA ) +
			   min( COD8XN, COD8SD )  +
                           min( COD8XO, COD8TH ) 
                         ) * TXCASA/100 ));
			 
CIRSE8 =  arr((min(COD8XN ,COD8SD) + min(COD8XO , COD8TH)) * TX066/100) ;

CIRSETOT = CIRSE1 + CIRSE2 + CIRSE3 + CIRSE4 + CIRSE5 + CIRSE8 ;

regle 301380:
application : iliad ;

CRESINTER = PRESINTER ;

regle 301390:
application : iliad ;
CIMR3 = CIMR2 * null(V_IND_TRAIT - 4)
        + (CIMR2 * positif(FLAG_RETARD * positif(FLAG_RETARD07) + (1 - FLAG_DEFAUT)) + (CIMR2 - CIMR24TLDEF) * (1 - positif(FLAG_RETARD * positif(FLAG_RETARD07) + (1 - FLAG_DEFAUT)))) * null(V_IND_TRAIT - 5) ;
TOTIRE = max(0,IRE-CIMR2 + CIMR3);
REST = positif(IRE) * positif(IRESTITIR) ;

CIMRREST = positif(REST) * max(0 , min(max(0 , CIMRCO) , IRESTITIR)) ;
CIMRIMP = positif_ou_nul(CIMRREST) * (max(0 , CIMRCO) - CIMRREST) ;
VARTMP1 = max(0 , CIMRCO) ;

LIBREST = positif(REST) * max(0 , min(AUTOVERSLIB , IRESTITIR - VARTMP1)) ;
LIBIMP = positif_ou_nul(LIBREST) * (AUTOVERSLIB - LIBREST) ;
VARTMP1 = VARTMP1 + AUTOVERSLIB ;

TAUREST = positif(REST) * max(0 , min(CRERESTAU , IRESTITIR - VARTMP1)) ;
TAUIMP = positif_ou_nul(TAUREST) * (CRERESTAU - TAUREST) ;
VARTMP1 = VARTMP1 + CRERESTAU ;

AGRREST = positif(REST) * max(0 , min(CICONGAGRI , IRESTITIR - VARTMP1)) ;
AGRIMP = positif_ou_nul(AGRREST) * (CICONGAGRI - AGRREST) ;
VARTMP1 = VARTMP1 + CICONGAGRI ;

ARTREST = positif(REST) * max(0 , min(CREARTS , IRESTITIR - VARTMP1)) ;
ARTIMP = positif_ou_nul(ARTREST) * (CREARTS - ARTREST) ;
VARTMP1 = VARTMP1 + CREARTS ;

FORREST = positif(REST) * max(0 , min(CREFORMCHENT , IRESTITIR - VARTMP1)) ;
FORIMP = positif_ou_nul(FORREST) * (CREFORMCHENT - FORREST) ;
VARTMP1 = VARTMP1 + CREFORMCHENT ;

PSIREST = positif(REST) * max(0 , min(CRESINTER , IRESTITIR - VARTMP1)) ;
PSIIMP = positif_ou_nul(PSIREST) * (CRESINTER - PSIREST) ;
VARTMP1 = VARTMP1 + CRESINTER ;

BIOREST = positif(REST) * max(0 , min(CREAGRIBIO , IRESTITIR - VARTMP1)) ;
BIOIMP = positif_ou_nul(BIOREST) * (CREAGRIBIO - BIOREST) ;
VARTMP1 = VARTMP1 + CREAGRIBIO ;

APPREST = positif(REST) * max(0 , min(COD8TZ , IRESTITIR - VARTMP1)) ;
APPIMP = positif_ou_nul(APPREST) * (COD8TZ - APPREST) ;
VARTMP1 = VARTMP1 + COD8TZ ;

FAMREST = positif(REST) * max(0 , min(CREFAM , IRESTITIR - VARTMP1)) ;
FAMIMP = positif_ou_nul(FAMREST) * (CREFAM - FAMREST) ;
VARTMP1 = VARTMP1 + CREFAM ;

HABREST = positif(REST) * max(0 , min(CIHABPRIN , IRESTITIR - VARTMP1)) ;
HABIMP = positif_ou_nul(HABREST) * (CIHABPRIN - HABREST) ;
VARTMP1 = VARTMP1 + CIHABPRIN ;

ROFREST = positif(REST) * max(0 , min(CIFORET , IRESTITIR - VARTMP1)) ;
ROFIMP = positif_ou_nul(ROFREST) * (CIFORET - ROFREST) ;
VARTMP1 = VARTMP1 + CIFORET ;

SALREST = positif(REST) * max(0 , min(CIADCRE , IRESTITIR - VARTMP1)) ;
SALIMP = positif_ou_nul(SALREST) * (CIADCRE - SALREST) ;
VARTMP1 = VARTMP1 + CIADCRE ;

PREREST = positif(REST) * max(0 , min(CIPRETUD , IRESTITIR - VARTMP1)) ;
PREIMP = positif_ou_nul(PREREST) * (CIPRETUD - PREREST) ;
VARTMP1 = VARTMP1 + CIPRETUD ;

SYNREST = positif(REST) * max(0 , min(CISYND , IRESTITIR - VARTMP1)) ;
SYNIMP = positif_ou_nul(SYNREST) * (CISYND - SYNREST) ;
VARTMP1 = VARTMP1 + CISYND ;

GARREST = positif(REST) * max(0 , min(CIGARD , IRESTITIR - VARTMP1)) ;
GARIMP = positif_ou_nul(GARREST) * (CIGARD - GARREST) ;
VARTMP1 = VARTMP1 + CIGARD ;

BAIREST = positif(REST) * max(0 , min(CICA , IRESTITIR - VARTMP1)) ;
BAIIMP = positif_ou_nul(BAIREST) * (CICA - BAIREST) ;
VARTMP1 = VARTMP1 + CICA ;

TECREST = positif(REST) * max(0 , min(CITEC , IRESTITIR - VARTMP1)) ;
TECIMP = positif_ou_nul(TECREST) * (CITEC - TECREST) ;
VARTMP1 = VARTMP1 + CITEC ;

DEPREST = positif(REST) * max(0 , min(CIDEVDUR , IRESTITIR - VARTMP1)) ;
DEPIMP = positif_ou_nul(DEPREST) * (CIDEVDUR - DEPREST) ;
VARTMP1 = VARTMP1 + CIDEVDUR ;

AIDREST = positif(REST) * max(0 , min(CIGE , IRESTITIR - VARTMP1)) ;
AIDIMP = positif_ou_nul(AIDREST) * (CIGE - AIDREST) ;
VARTMP1 = VARTMP1 + CIGE ;

HJAREST = positif(REST) * max(0 , min(CIHJA , IRESTITIR - VARTMP1)) ;
HJAIMP = positif_ou_nul(HJAREST) * (CIHJA - HJAREST) ;
VARTMP1 = VARTMP1 + CIHJA ;

ASSREST = positif(REST) * max(0 , min(I2DH , IRESTITIR - VARTMP1)) ;
ASSIMP = positif_ou_nul(ASSREST) * (I2DH - ASSREST) ;
VARTMP1 = VARTMP1 + I2DH ;

2CKREST = positif(REST) * max(0 , min(CI2CK , IRESTITIR - VARTMP1)) ;
2CKIMP = positif_ou_nul(2CKREST) * (CI2CK - 2CKREST) ;
VARTMP1 = VARTMP1 + CI2CK ;

EMPREST = positif(REST) * max(0 , min(COD8TL , IRESTITIR - VARTMP1)) ;
EMPIMP = positif_ou_nul(EMPREST) * (COD8TL - EMPREST) ;
VARTMP1 = VARTMP1 + COD8TL ;

RECREST = positif(REST) * max(0 , min(IPRECH , IRESTITIR - VARTMP1)) ;
RECIMP = positif_ou_nul(RECREST) * (IPRECH - RECREST) ;
VARTMP1 = VARTMP1 + IPRECH ;

CORREST = positif(REST) * max(0 , min(CICORSENOW , IRESTITIR - VARTMP1)) ;
CORIMP = positif_ou_nul(CORREST) * (CICORSENOW - CORREST) ;
VARTMP1 = 0 ;

regle 301400:
application : iliad ,bareme;

RFRMENAGE = REVKIRE + COD3SA + (COD3WM+ABPVNOSURSIS) * positif(null(1-FLAG_EXIT)+null(2-FLAG_EXIT));
RFRMENAGE3WG = REVKIRE + COD3SA + PVREPORT + (COD3WM+ABPVNOSURSIS) * positif(null(1-FLAG_EXIT)+null(2-FLAG_EXIT));

PMENAGEMIN = max( (  (PLAF_MENAGE_MIN) * (1 + BOOL_0AM)
                 + (PLAF_MENAGE_QAR * (NBQAR * 4)
                 + PLAF_MENAGE_PAC * (((NSM -1) * (1-BOOL_0AM) + NPA+NIN+NSP) * 2)) * (1-present(V_9VV))
		 + (PLAF_MENAGE_PAC * (V_9VV - (1+BOOL_0AM))*2) * present(V_9VV)
                ), 0 );
PMENAGE = max( (   PLAF_MENAGE * (1 + BOOL_0AM)
                 + (PLAF_MENAGE_QAR * (NBQAR * 4)
                 + PLAF_MENAGE_PAC * (((NSM -1) * (1-BOOL_0AM) + NPA+NIN+NSP) * 2)) * (1-present(V_9VV))
		 + (PLAF_MENAGE_PAC * (V_9VV - (1+BOOL_0AM))*2) * present(V_9VV)
                ), 0 );

INDMENAGEMIN = positif_ou_nul(PMENAGEMIN - RFRMENAGE);
INDMENAGEMIN3WG = positif_ou_nul(PMENAGEMIN - RFRMENAGE3WG);

INDMENAGESUP = positif_ou_nul(PMENAGE - RFRMENAGE) * positif(RFRMENAGE - PMENAGEMIN+1);
INDMENAGESUP3WG = positif_ou_nul(PMENAGE - RFRMENAGE3WG) * positif(RFRMENAGE3WG - PMENAGEMIN+1);

INDMENAGE = positif_ou_nul(PMENAGE - RFRMENAGE)* (1-V_CNR) * (1 - positif(present(REVFONC)+present(IND_TDR)));

RMENAGE = INDMENAGE * (INDMENAGEMIN * (1-positif(INDMENAGESUP)) * arr(( IDOM11 - DEC11)*TX20/100)
                       + positif(INDMENAGESUP) * arr(( IDOM11 - DEC11)*TX20/100 * ((PMENAGE - RFRMENAGE)/(LIMRMENAGE*(1+BOOL_0AM)))))
		    * (1 - FLAG_BAREM) ;
INDMENAGE3WG = positif_ou_nul(PMENAGE - RFRMENAGE3WG)* (1-V_CNR) * (1 - positif(present(REVFONC)+present(IND_TDR)));
RMENAGE3WG = INDMENAGE3WG * ( INDMENAGEMIN3WG * (1-positif(INDMENAGESUP3WG)) * arr(( IDOM13 - DEC13)*TX20/100)
                      + positif(INDMENAGESUP3WG) * arr(( IDOM13 - DEC13)*TX20/100 * ((PMENAGE - RFRMENAGE3WG)/(LIMRMENAGE*(1+BOOL_0AM)))))
		    * (1 - FLAG_BAREM) ;

regle 201905:
application : iliad ;

CIMRPOTE = null(NBPOTE - (V_BTNBPOTE * (1 - present(CODXFA)) + CODXFA)) ;

SALCIMRV = TSN1AJ + TSN1AA + TSN1GF + TSN1GB + TSN1AP + TSN1AG + TSN1GG ;
SALCIMRC = TSN1BJ + TSN1BA + TSN1HF + TSN1HB + TSN1BP + TSN1BG + TSN1HG ;
SALCIMRP = TSN1CJ + TSN1CA + TSN1IF + TSN1IB + TSN1CP + TSN1CG + TSN1IG ;

BACIMRV = max(0 , BAMICV + BAFORESTV + PASBACV + PASBAHV - arr(DEFANTBAV * ((IBAMICV + BAFORESTV + R15HC + R2MAJ5HI) / BAHQNODEFV))) + BATMARGV ;
BACIMRC = max(0 , BAMICC + BAFORESTC + PASBACC + PASBAHC - arr(DEFANTBAC * ((IBAMICC + BAFORESTC + R15IC + R2MAJ5II) / BAHQNODEFC))) + BATMARGC ;
BACIMRP = max(0 , BAMICP + BAFORESTP + PASBACP + PASBAHP - arr(DEFANTBAP * ((IBAMICP + BAFORESTP + R15JC + R2MAJ5JI) / BAHQNODEFP))) ;

BENBAV = BACIMRV + COD5XA + BAEXV + BAHEXV + BAPERPV + BANOCGAV ;
BENBAC = BACIMRC + COD5YA + BAEXC + BAHEXC + BAPERPC + BANOCGAC ;
BENBAP = BACIMRP + COD5ZA + BAEXP + BAHEXP + BAPERPP + BANOCGAP ;

BICIMRV = max(0 , MIB_NETVV + MIB_NETPV + PASBICPCV + PASBICPNCV
                  + max(0 , MIB_NETNPVV + MIB_NETNPPV + PASBICNPCV + PASBICNPNCV
                            - arr(DEFANTBICNPV * (R15NC + R2MAJ5NI + MIB_NETNPVV + MIB_NETNPPV + MIBNPPVV - MIBNPDCT) / BICNPONCV))
                  + max(0 , RLOCFNPIV + SNPLOCPASV + RNPLOCPASV
                            - arr(DEFANTLOCV * (RLOCFNPIV + SNPLOCPASV + RNPLOCPASV) / RNPILOCV))) * (1 - positif(V_BT5DR * (1 - present(CODXAZ)) + CODXAZ)) ;
BICIMRC = max(0 , MIB_NETVC + MIB_NETPC + PASBICPCC + PASBICPNCC
                  + max(0 , MIB_NETNPVC + MIB_NETNPPC + PASBICNPCC + PASBICNPNCC
                            - arr(DEFANTBICNPC * (R15OC + R2MAJ5OI + MIB_NETNPVC + MIB_NETNPPC + MIBNPPVC - COD5RZ) / BICNPONCC))
                  + max(0 , RLOCFNPIC + SNPLOCPASC + RNPLOCPASC
                            - arr(DEFANTLOCC * (RLOCFNPIC + SNPLOCPASC + RNPLOCPASC) / RNPILOCC))) * (1 - positif(V_BT5ER * (1 - present(CODXCS)) + CODXCS)) ;
BICIMRP = max(0 , MIB_NETVP + MIB_NETPP + PASBICPCP + PASBICPNCP
                  + max(0 , MIB_NETNPVP + MIB_NETNPPP + PASBICNPCP + PASBICNPNCP
                            - arr(DEFANTBICNPP * (R15PC + R2MAJ5PI + MIB_NETNPVP + MIB_NETNPPP + MIBNPPVP - COD5SZ) / BICNPONCP))
                  + max(0 , RLOCFNPIP + SNPLOCPASP + RNPLOCPASP
                            - arr(DEFANTLOCP * (RLOCFNPIP + SNPLOCPASP + RNPLOCPASP) / RNPILOCP))) * (1 - positif(V_BT5FR * (1 - present(CODXEL)) + CODXEL)) ;

BENBICV = BICIMRV + MIBEXV + BICEXV + BIHEXV + MIBNPEXV + BICNPEXV + BICNPHEXV ;
BENBICC = BICIMRC + MIBEXC + BICEXC + BIHEXC + MIBNPEXC + BICNPEXC + BICNPHEXC ;
BENBICP = BICIMRP + MIBEXP + BICEXP + BIHEXP + MIBNPEXP + BICNPEXP + BICNPHEXP ;

BNCIMRV = max(0 , SPENETPV + PASBNNAV + PASBNNSV
                  + max(0 , SPENETNPV + PASBNNAAV + PASNOCEPIMPV
                            - arr(DABNCNPV * (SPENETNPV + BNCNPPVV - BNCNPDCT + R15JG + R2MAJ5SN) / BNCNPHQNCV))) * (1 - positif(V_BT5AE * (1 - present(CODXBC)) + CODXBC)) ;
BNCIMRC = max(0 , SPENETPC + PASBNNAC + PASBNNSC
                  + max(0 , SPENETNPC + PASBNNAAC + PASNOCEPIMPC
                            - arr(DABNCNPC * (SPENETNPC + BNCNPPVC - COD5LD + R15RF + R2MAJ5NS) / BNCNPHQNCC))) * (1 - positif(V_BT5BE * (1 - present(CODXCV)) + CODXCV)) ;
BNCIMRP = max(0 , SPENETPP + PASBNNAP + PASBNNSP
                  + max(0 , SPENETNPP + PASBNNAAP + PASNOCEPIMPP
                            - arr(DABNCNPP * (SPENETNPP + BNCNPPVP - COD5MD + R15SF + R2MAJ5OS) / BNCNPHQNCP))) * (1 - positif(V_BT5CE * (1 - present(CODXEO)) + CODXEO)) ;

BENBNCV = BNCIMRV + BNCPROEXV + BNCEXV + BNHEXV + BNCCRV + XSPENPV + BNCNPREXAAV + BNCNPREXV + BNCCRFV ;
BENBNCC = BNCIMRC + BNCPROEXC + BNCEXC + BNHEXC + BNCCRC + XSPENPC + BNCNPREXAAC + BNCNPREXC + BNCCRFC ;
BENBNCP = BNCIMRP + BNCPROEXP + BNCEXP + BNHEXP + BNCCRP + XSPENPP + BNCNPREXAAP + BNCNPREXP + BNCCRFP ;


DIFF1V = (V_BTMRSOM1 * (1 - present(CODXAU)) + CODXAU) - (SALCIMRV + arr(BENBAV * (V_BTBAMRV * (1 - present(CODXAM)) + CODXAM) / (V_BTBENBAV * (1 - present(CODXAL)) + CODXAL)) 
                                                                   + arr(BENBICV * (V_BTBICMRV * (1 - present(CODXAO)) + CODXAO) / (V_BTBENBICV * (1 - present(CODXAN)) + CODXAN)) 
							           + arr(BENBNCV * (V_BTBNCMRV * (1 - present(CODXAQ)) + CODXAQ) / (V_BTBENBNCV * (1 - present(CODXAP)) + CODXAP))) ; 
DIFF1C = (V_BTMRSOM2 * (1 - present(CODXCN)) + CODXCN) - (SALCIMRC + arr(BENBAC * (V_BTBAMRC * (1 - present(CODXCF)) + CODXCF) / (V_BTBENBAC * (1 - present(CODXCE)) + CODXCE)) 
                                                                   + arr(BENBICC * (V_BTBICMRC * (1 - present(CODXCH)) + CODXCH) / (V_BTBENBICC * (1 - present(CODXCG)) + CODXCG)) 
								   + arr(BENBNCC * (V_BTBNCMRC * (1 - present(CODXCJ)) + CODXCJ) / (V_BTBENBNCC * (1 - present(CODXCI)) + CODXCI))) ;
DIFF1P = (V_BTMRSOM3 * (1 - present(CODXEG)) + CODXEG) - (SALCIMRP + arr(BENBAP * (V_BTBAMRP * (1 - present(CODXDY)) + CODXDY) / (V_BTBENBAP * (1 - present(CODXDX)) + CODXDX)) 
                                                                   + arr(BENBICP * (V_BTBICMRP * (1 - present(CODXEA)) + CODXEA) / (V_BTBENBICP * (1 - present(CODXDZ)) + CODXDZ)) 
								   + arr(BENBNCP * (V_BTBNCMRP * (1 - present(CODXEC)) + CODXEC) / (V_BTBENBNCP * (1 - present(CODXEB)) + CODXEB))) ;

DIFFBAV = (V_BTBAMRV * (1 - present(CODXAM)) + CODXAM) - arr(BENBAV * (V_BTBAMRV * (1 - present(CODXAM)) + CODXAM) / (V_BTBENBAV * (1 - present(CODXAL)) + CODXAL)) ;
DIFFBAC = (V_BTBAMRC * (1 - present(CODXCF)) + CODXCF) - arr(BENBAC * (V_BTBAMRC * (1 - present(CODXCF)) + CODXCF) / (V_BTBENBAC * (1 - present(CODXCE)) + CODXCE)) ;
DIFFBAP = (V_BTBAMRP * (1 - present(CODXDY)) + CODXDY) - arr(BENBAP * (V_BTBAMRP * (1 - present(CODXDY)) + CODXDY) / (V_BTBENBAP * (1 - present(CODXDX)) + CODXDX)) ;

DIFFBICV = (V_BTBICMRV * (1 - present(CODXAO)) + CODXAO) - arr(BENBICV * (V_BTBICMRV * (1 - present(CODXAO)) + CODXAO) / (V_BTBENBICV * (1 - present(CODXAN)) + CODXAN)) ;
DIFFBICC = (V_BTBICMRC * (1 - present(CODXCH)) + CODXCH) - arr(BENBICC * (V_BTBICMRC * (1 - present(CODXCH)) + CODXCH) / (V_BTBENBICC * (1 - present(CODXCG)) + CODXCG)) ;
DIFFBICP = (V_BTBICMRP * (1 - present(CODXEA)) + CODXEA) - arr(BENBICP * (V_BTBICMRP * (1 - present(CODXEA)) + CODXEA) / (V_BTBENBICP * (1 - present(CODXDZ)) + CODXDZ)) ;

DIFFBNCV = (V_BTBNCMRV * (1 - present(CODXAQ)) + CODXAQ) - arr(BENBNCV * (V_BTBNCMRV * (1 - present(CODXAQ)) + CODXAQ) / (V_BTBENBNCV * (1 - present(CODXAP)) + CODXAP)) ;
DIFFBNCC = (V_BTBNCMRC * (1 - present(CODXCJ)) + CODXCJ) - arr(BENBNCC * (V_BTBNCMRC * (1 - present(CODXCJ)) + CODXCJ) / (V_BTBENBNCC * (1 - present(CODXCI)) + CODXCI)) ;
DIFFBNCP = (V_BTBNCMRP * (1 - present(CODXEC)) + CODXEC) - arr(BENBNCP * (V_BTBNCMRP * (1 - present(CODXEC)) + CODXEC) / (V_BTBENBNCP * (1 - present(CODXEB)) + CODXEB)) ;

DIFFGEV = (V_BT1AN * (1 - present(CODXBS)) + CODXBS) * positif(V_BT1AV * (1 - present(CODXBD)) + CODXBD) + (V_BT1GN * (1 - present(CODXBT)) + CODXBT) * positif(V_BT1GV * (1 - present(CODXBE)) + CODXBE) 
          + (V_BT1KN * (1 - present(CODXBU)) + CODXBU) * positif(V_BT1KV * (1 - present(CODXBJ)) + CODXBJ) + (V_BT1LN * (1 - present(CODXBV)) + CODXBV) * positif(V_BT1LV * (1 - present(CODXBK)) + CODXBK)
          + (V_BT1MN * (1 - present(CODXBW)) + CODXBW) * positif(V_BT1MV * (1 - present(CODXBL)) + CODXBL) + (V_BT1PN * (1 - present(CODXBX)) + CODXBX) * positif(V_BT1PV * (1 - present(CODXBM)) + CODXBM) 
	  - (COD1AN + COD1GN + COD1KN + COD1LN + COD1MN + COD1PN) ;

DIFFGEC = (V_BT1BN * (1 - present(CODXDL)) + CODXDL) * positif(V_BT1BV * (1 - present(CODXCW)) + CODXCW) + (V_BT1HN * (1 - present(CODXDM)) + CODXDM) * positif(V_BT1HV * (1 - present(CODXCX)) + CODXCX) 
          + (V_BT1QN * (1 - present(CODXDN)) + CODXDN) * positif(V_BT1QV * (1 - present(CODXDC)) + CODXDC) + (V_BT1RN * (1 - present(CODXDO)) + CODXDO) * positif(V_BT1RV * (1 - present(CODXDD)) + CODXDD)
          + (V_BT1SN * (1 - present(CODXDP)) + CODXDP) * positif(V_BT1SV * (1 - present(CODXDE)) + CODXDE) + (V_BT1TN * (1 - present(CODXDQ)) + CODXDQ) * positif(V_BT1TV * (1 - present(CODXDF)) + CODXDF) 
	  - (COD1BN + COD1HN + COD1QN + COD1RN + COD1SN + COD1TN) ;

DIFFGEP = (V_BT1CN * (1 - present(CODXFI)) + CODXFI) * positif(V_BT1CV * (1 - present(CODXEP)) + CODXEP) + (V_BT1IN * (1 - present(CODXFJ)) + CODXFJ) * positif(V_BT1IV * (1 - present(CODXEQ)) + CODXEQ) 
          - (COD1CN + COD1IN) ;

CIMRBA1 = (positif_ou_nul(BENBAV - (V_BTBENBAV * (1 - present(CODXAL)) + CODXAL)) * max(0 , (V_BTBAMRV * (1 - present(CODXAM)) + CODXAM) - (V_BTMRBA12 * (1 - present(CODXAI)) + CODXAI)) 
           + positif((V_BTBENBAV * (1 - present(CODXAL)) + CODXAL) - BENBAV) * positif(BENBAV - (V_BTBENBA1 * (1 - present(CODXAR)) + CODXAR)) 
	     * max(0 , arr(BENBAV * (V_BTBAMRV * (1 - present(CODXAM)) + CODXAM) / (V_BTBENBAV * (1 - present(CODXAL)) + CODXAL)) - (V_BTMRBA12 * (1 - present(CODXAI)) + CODXAI))) 
	  * positif(present(V_BTMRBA12) + present(CODXAI)) * (1 - positif(V_BTBAA * (1 - present(CODXAV)) + CODXAV + V_BT5AC * (1 - present(CODXAW)) + CODXAW + V_BTMRBA11 * (1 - present(CODXAF)) + CODXAF)) 
	  - min(DIFF1V , DIFFBAV) * positif(DIFF1V) * positif(DIFFBAV) * positif(V_BT5AC * (1 - present(CODXAW)) + CODXAW) ; 

CIMRBA2 = (positif_ou_nul(BENBAC - (V_BTBENBAC * (1 - present(CODXCE)) + CODXCE)) * max(0 , (V_BTBAMRC * (1 - present(CODXCF)) + CODXCF) - (V_BTMRBA22 * (1 - present(CODXCB)) + CODXCB)) 
           + positif((V_BTBENBAC * (1 - present(CODXCE)) + CODXCE) - BENBAC) * positif(BENBAC - (V_BTBENBA2 * (1 - present(CODXCK)) + CODXCK)) 
	     * max(0 , arr(BENBAC * (V_BTBAMRC * (1 - present(CODXCF)) + CODXCF) / (V_BTBENBAC * (1 - present(CODXCE)) + CODXCE)) - (V_BTMRBA22 * (1 - present(CODXCB)) + CODXCB))) 
	  * positif(present(V_BTMRBA22) + present(CODXCB)) * (1 - positif(V_BTBAB * (1 - present(CODXCO)) + CODXCO + V_BT5BC * (1 - present(CODXCP)) + CODXCP + V_BTMRBA21 * (1 - present(CODXBY)) + CODXBY)) 
	  - min(DIFF1C , DIFFBAC) * positif(DIFF1C) * positif(DIFFBAC) * positif(V_BT5BC * (1 - present(CODXCP)) + CODXCP) ;

CIMRBA3 = (positif_ou_nul(BENBAP - (V_BTBENBAP * (1 - present(CODXDX)) + CODXDX)) * max(0 , (V_BTBAMRP * (1 - present(CODXDY)) + CODXDY) - (V_BTMRBA32 * (1 - present(CODXDU)) + CODXDU)) 
           + positif((V_BTBENBAP * (1 - present(CODXDX)) + CODXDX) - BENBAP) * positif(BENBAP - (V_BTBENBA3 * (1 - present(CODXED)) + CODXED)) 
	     * max(0 , arr(BENBAP * (V_BTBAMRP * (1 - present(CODXDY)) + CODXDY) / (V_BTBENBAP * (1 - present(CODXDX)) + CODXDX)) - (V_BTMRBA32 * (1 - present(CODXDU)) + CODXDU))) 
	  * positif(present(V_BTMRBA32) + present(CODXDU)) * (1 - positif(V_BTBAC * (1 - present(CODXEH)) + CODXEH + V_BT5CC * (1 - present(CODXEI)) + CODXEI + V_BTMRBA31 * (1 - present(CODXDR)) + CODXDR)) 
	  - min(DIFF1P , DIFFBAP) * positif(DIFF1P) * positif(DIFFBAP) * positif(V_BT5CC * (1 - present(CODXEI)) + CODXEI) ;

CIMRBIC1 = (positif_ou_nul(BENBICV - (V_BTBENBICV * (1 - present(CODXAN)) + CODXAN)) * max(0 , (V_BTBICMRV * (1 - present(CODXAO)) + CODXAO) - (V_BTMRBIC12 * (1 - present(CODXAJ)) + CODXAJ)) 
            + positif((V_BTBENBICV * (1 - present(CODXAN)) + CODXAN) - BENBICV) * positif(BENBICV - (V_BTBENBIC1 * (1 - present(CODXAS)) + CODXAS)) 
	      * max(0 , arr(BENBICV * (V_BTBICMRV * (1 - present(CODXAO)) + CODXAO) / (V_BTBENBICV * (1 - present(CODXAN)) + CODXAN)) - (V_BTMRBIC12 * (1 - present(CODXAJ)) + CODXAJ))) 
	   * positif(present(V_BTMRBIC12) + present(CODXAJ)) * (1 - positif(V_BTBIA * (1 - present(CODXAX)) + CODXAX + V_BT5HN * (1 - present(CODXAY)) + CODXAY + V_BT5DR * (1 - present(CODXAZ)) + CODXAZ + V_BTMRBIC11 * (1 - present(CODXAG)) + CODXAG)) 
	   - min(DIFF1V , DIFFBICV) * positif(DIFF1V) * positif(DIFFBICV) * positif(V_BT5HN * (1 - present(CODXAY)) + CODXAY) ;

CIMRBIC2 = (positif_ou_nul(BENBICC - (V_BTBENBICC * (1 - present(CODXCG)) + CODXCG)) * max(0 , (V_BTBICMRC * (1 - present(CODXCH)) + CODXCH) - (V_BTMRBIC22 * (1 - present(CODXCC)) + CODXCC)) 
            + positif((V_BTBENBICC * (1 - present(CODXCG)) + CODXCG) - BENBICC) * positif(BENBICC - (V_BTBENBIC2 * (1 - present(CODXCL)) + CODXCL)) 
	      * max(0 , arr(BENBICC * (V_BTBICMRC * (1 - present(CODXCH)) + CODXCH) / (V_BTBENBICC * (1 - present(CODXCG)) + CODXCG)) - (V_BTMRBIC22 * (1 - present(CODXCC)) + CODXCC))) 
	   * positif(present(V_BTMRBIC22) + present(CODXCC)) * (1 - positif(V_BTBIB * (1 - present(CODXCQ)) + CODXCQ + V_BT5IN * (1 - present(CODXCR)) + CODXCR + V_BT5ER * (1 - present(CODXCS)) + CODXCS + V_BTMRBIC21 * (1 - present(CODXBZ)) + CODXBZ))
	   - min(DIFF1C , DIFFBICC) * positif(DIFF1C) * positif(DIFFBICC) * positif(V_BT5IN * (1 - present(CODXCR)) + CODXCR) ;

CIMRBIC3 = (positif_ou_nul(BENBICP - (V_BTBENBICP * (1 - present(CODXDZ)) + CODXDZ)) * max(0 , (V_BTBICMRP * (1 - present(CODXEA)) + CODXEA) - (V_BTMRBIC32 * (1 - present(CODXDV)) + CODXDV)) 
            + positif((V_BTBENBICP * (1 - present(CODXDZ)) + CODXDZ) - BENBICP) * positif(BENBICP - (V_BTBENBIC3 * (1 - present(CODXEE)) + CODXEE)) 
	      * max(0 , arr(BENBICP * (V_BTBICMRP * (1 - present(CODXEA)) + CODXEA) / (V_BTBENBICP * (1 - present(CODXDZ)) + CODXDZ)) - (V_BTMRBIC32 * (1 - present(CODXDV)) + CODXDV))) 
	   * positif(present(V_BTMRBIC32) + present(CODXDV)) * (1 - positif(V_BTBIC * (1 - present(CODXEJ)) + CODXEJ + V_BT5JN * (1 - present(CODXEK)) + CODXEK + V_BT5FR * (1 - present(CODXEL)) + CODXEL + V_BTMRBIC31 * (1 - present(CODXDS)) + CODXDS)) 
	   - min(DIFF1P , DIFFBICP) * positif(DIFF1P) * positif(DIFFBICP) * positif(V_BT5JN * (1 - present(CODXEK)) + CODXEK) ;

CIMRBNC1 = (positif_ou_nul(BENBNCV - (V_BTBENBNCV * (1 - present(CODXAP)) + CODXAP)) * max(0 , (V_BTBNCMRV * (1 - present(CODXAQ)) + CODXAQ) - (V_BTMRBNC12 * (1 - present(CODXAK)) + CODXAK)) 
            + positif((V_BTBENBNCV * (1 - present(CODXAP)) + CODXAP) - BENBNCV) * positif(BENBNCV - (V_BTBENBNC1 * (1 - present(CODXAT)) + CODXAT)) 
	      * max(0 , arr(BENBNCV * (V_BTBNCMRV * (1 - present(CODXAQ)) + CODXAQ) / (V_BTBENBNCV * (1 - present(CODXAP)) + CODXAP)) - (V_BTMRBNC12 * (1 - present(CODXAK)) + CODXAK))) 
	   * positif(present(V_BTMRBNC12) + present(CODXAK)) * (1 - positif(V_BTBNA * (1 - present(CODXBA)) + CODXBA + V_BT5KG * (1 - present(CODXBB)) + CODXBB + V_BT5AE * (1 - present(CODXBC)) + CODXBC + V_BTMRBNC11 * (1 - present(CODXAH)) + CODXAH)) 
	   - min(DIFF1V , DIFFBNCV) * positif(DIFF1V) * positif(DIFFBNCV) * positif(V_BT5KG * (1 - present(CODXBB)) + CODXBB) ;

CIMRBNC2 = (positif_ou_nul(BENBNCC - (V_BTBENBNCC * (1 - present(CODXCI)) + CODXCI)) * max(0 , (V_BTBNCMRC * (1 - present(CODXCJ)) + CODXCJ) - (V_BTMRBNC22 * (1 - present(CODXCD)) + CODXCD)) 
            + positif((V_BTBENBNCC * (1 - present(CODXCI)) + CODXCI) - BENBNCC) * positif(BENBNCC - (V_BTBENBNC2 * (1 - present(CODXCM)) + CODXCM)) 
	      * max(0 , arr(BENBNCC * (V_BTBNCMRC * (1 - present(CODXCJ)) + CODXCJ) / (V_BTBENBNCC * (1 - present(CODXCI)) + CODXCI)) - (V_BTMRBNC22 * (1 - present(CODXCD)) + CODXCD))) 
	   * positif(present(V_BTMRBNC22) + present(CODXCD)) * (1 - positif(V_BTBNB * (1 - present(CODXCT)) + CODXCT + V_BT5LG * (1 - present(CODXCU)) + CODXCU + V_BT5BE * (1 - present(CODXCV)) + CODXCV + V_BTMRBNC21 * (1 - present(CODXCA)) + CODXCA))
	   - min(DIFF1C , DIFFBNCC) * positif(DIFF1C) * positif(DIFFBNCC) * positif(V_BT5LG * (1 - present(CODXCU)) + CODXCU) ;

CIMRBNC3 = (positif_ou_nul(BENBNCP - (V_BTBENBNCP * (1 - present(CODXEB)) + CODXEB)) * max(0 , (V_BTBNCMRP * (1 - present(CODXEC)) + CODXEC) - (V_BTMRBNC32 * (1 - present(CODXDW)) + CODXDW)) 
            + positif((V_BTBENBNCP * (1 - present(CODXEB)) + CODXEB) - BENBNCP) * positif(BENBNCP - (V_BTBENBNC3 * (1 - present(CODXEF)) + CODXEF)) 
	      * max(0 , arr(BENBNCP * (V_BTBNCMRP * (1 - present(CODXEC)) + CODXEC) / (V_BTBENBNCP * (1 - present(CODXEB)) + CODXEB)) - (V_BTMRBNC32 * (1 - present(CODXDW)) + CODXDW))) 
	   * positif(present(V_BTMRBNC32) + present(CODXDW)) * (1 - positif(V_BTBNC * (1 - present(CODXEM)) + CODXEM + V_BT5MG * (1 - present(CODXEN)) + CODXEN + V_BT5CE * (1 - present(CODXEO)) + CODXEO + V_BTMRBNC31 * (1 - present(CODXDT)) + CODXDT)) 
	   - min(DIFF1P , DIFFBNCP) * positif(DIFF1P) * positif(DIFFBNCP) * positif(V_BT5MG * (1 - present(CODXEN)) + CODXEN) ;

CIMRB = CIMRBA1 + CIMRBIC1 + CIMRBNC1 + (CIMRBA2 + CIMRBIC2 + CIMRBNC2) * BOOL_0AM + (CIMRBA3 + CIMRBIC3 + CIMRBNC3) * CIMRPOTE  
        - min(DIFF1V , DIFFGEV) * positif_ou_nul(DIFF1V) * positif_ou_nul(DIFFGEV) - min(DIFF1C , DIFFGEC) * positif_ou_nul(DIFF1C) * positif_ou_nul(DIFFGEC) * BOOL_0AM - min(DIFF1P , DIFFGEP) * positif_ou_nul(DIFF1P) * positif_ou_nul(DIFFGEP) * CIMRPOTE ;

CIMRB1 = CIMRBA1 + CIMRBIC1 + CIMRBNC1 - min(DIFF1V , DIFFGEV) * positif_ou_nul(DIFF1V) * positif_ou_nul(DIFFGEV) ;
CIMRB2 = (CIMRBA2 + CIMRBIC2 + CIMRBNC2 - min(DIFF1C , DIFFGEC) * positif_ou_nul(DIFF1C) * positif_ou_nul(DIFFGEC)) * BOOL_0AM ;

CIMR2BIS = (max(0 , arr((V_BTCIMRA * (1 - present(CODXAB)) + CODXAB) * min(1 , (CIMRB + (V_BTCIMRB * (1 - present(CODXAC)) + CODXAC) * (1 - positif(V_BTCIMR * (1 - present(CODXAA)) + CODXAA))) / (V_BTCIMRC * (1 - present(CODXAD)) + CODXAD))) 
                    - (V_BTCIMRCI * (1 - present(CODXAE)) + CODXAE) * (1 - positif(V_BTCIMR * (1 - present(CODXAA)) + CODXAA))) * positif(CIMRB)
            - min(V_BTCIMR * (1 - present(CODXAA)) + CODXAA , arr((V_BTCIMRA * (1 - present(CODXAB)) + CODXAB) * min(1 , (abs(CIMRB) / (V_BTCIMRC * (1 - present(CODXAD)) + CODXAD)))))
	      * positif(V_BTCIMR * (1 - present(CODXAA)) + CODXAA) * (1 - positif(CIMRB))
	   ) * (1 - positif(V_BTCIMRNUM * (1 - present(CODXFB)) + CODXFB)) * (1 - (positif(V_0AX) * (1 - positif(V_0AB)))) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF))) ;

CIMR2BIS1 = (max(0 , arr((V_BTCIMRA * (1 - present(CODXAB)) + CODXAB) * min(1 , (CIMRB1 + (V_BTCIMRB * (1 - present(CODXAC)) + CODXAC) * (1 - positif(V_BTCIMR * (1 - present(CODXAA)) + CODXAA))) / (V_BTCIMRC * (1 - present(CODXAD)) + CODXAD)))
                     - (V_BTCIMRCI * (1 - present(CODXAE)) + CODXAE) * (1 - positif(V_BTCIMR * (1 - present(CODXAA)) + CODXAA))) * positif(CIMRB1)
             - min(V_BTCIMR * (1 - present(CODXAA)) + CODXAA , arr((V_BTCIMRA * (1 - present(CODXAB)) + CODXAB) * min(1 , (abs(CIMRB1) / (V_BTCIMRC * (1 - present(CODXAD)) + CODXAD))))) 
	       * positif(V_BTCIMR * (1 - present(CODXAA)) + CODXAA) * (1 - positif(CIMRB1))
	    ) * (1 - positif(V_BTCIMRNUM * (1 - present(CODXFB)) + CODXFB)) * positif(V_0AX) * (1 - positif(V_0AB)) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF))) ;

CIMR2BIS2 = (max(0 , arr((V_BTCIMRA2 * (1 - present(CODXFQ)) + CODXFQ) * min(1 , (CIMRB2 + (V_BTCIMRB2 * (1 - present(CODXFS)) + CODXFS) * (1 - positif(V_BTCIMR2 * (1 - present(CODXFO)) + CODXFO))) / (V_BTCIMRC2 * (1 - present(CODXFU)) + CODXFU))) 
                    - (V_BTCIMRCI2 * (1 - present(CODXFW)) + CODXFW) * (1 - positif(V_BTCIMR2 * (1 - present(CODXFO)) + CODXFO))) * positif(CIMRB2)
             - min(V_BTCIMR2 * (1 - present(CODXFO)) + CODXFO , arr((V_BTCIMRA2 * (1 - present(CODXFQ)) + CODXFQ) * min(1 , (abs(CIMRB2) / (V_BTCIMRC2 * (1 - present(CODXFU)) + CODXFU)))))
	       * positif(V_BTCIMR2 * (1 - present(CODXFO)) + CODXFO) * (1 - positif(CIMRB2))
            ) * (1 - positif(V_BTCIMRNUM2 * (1 - present(CODXFY)) + CODXFY)) * positif(V_0AX) * (1 - positif(V_0AB)) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF))) ;

CIMRCOMP = min(CIMR2BIS , (V_BTCIMRA * (1 - present(CODXAB)) + CODXAB) - (V_BTCIMR * (1 - present(CODXAA)) + CODXAA)) * positif(CIMR2BIS) * (1 - present(CODCCI)) + CODCCI ;

REPCIMR = min(abs(CIMR2BIS) , (V_BTCIMRA * (1 - present(CODXAB)) + CODXAB)) * (1 - positif(CIMR2BIS)) * (1 - present(CODRCI)) + CODRCI ;

CIMRCOMP1 = min(CIMR2BIS1 , (V_BTCIMRA * (1 - present(CODXAB)) + CODXAB) - (V_BTCIMR * (1 - present(CODXAA)) + CODXAA)) * positif(CIMR2BIS1) * (1 - present(CODCCI)) ;

REPCIMR1 = min(abs(CIMR2BIS1) , (V_BTCIMRA * (1 - present(CODXAB)) + CODXAB)) * (1 - positif(CIMR2BIS1)) * (1 - present(CODRCI)) ;

CIMRCOMP2 = min(CIMR2BIS2 , (V_BTCIMRA2 * (1 - present(CODXFQ)) + CODXFQ) - (V_BTCIMR2 * (1 - present(CODXFO)) + CODXFO)) * positif(CIMR2BIS2) * (1 - present(CODCCI)) ;

REPCIMR2 = min(abs(CIMR2BIS2) , (V_BTCIMRA2 * (1 - present(CODXFQ)) + CODXFQ)) * (1 - positif(CIMR2BIS2)) * (1 - present(CODRCI)) ;

CIMRCO = CIMRCOMP + CIMRCOMP1 + CIMRCOMP2 ;
CIMREP = REPCIMR + REPCIMR1 + REPCIMR2 ;

CIMR2 = CIMRCO - CIMREP ;

CIMRCOND = null(V_BTCIMRA * (1 - present(CODXAB)) + CODXAB - (V_BTCIMR * (1 - present(CODXAA)) + CODXAA) - CIMRCOMP) ;
CIMRCOND1 = null(V_BTCIMRA * (1 - present(CODXAB)) + CODXAB - (V_BTCIMR * (1 - present(CODXAA)) + CODXAA) - CIMRCOMP1) ;
CIMRCOND2 = null(V_BTCIMRA2 * (1 - present(CODXFQ)) + CODXFQ - (V_BTCIMR2 * (1 - present(CODXFO)) + CODXFO) - CIMRCOMP2) ;

regle 201908:
application : iliad ;

CIMR =  CIMR2 * null(V_IND_TRAIT - 4) +
        null(V_IND_TRAIT - 5) * ((1-positif(FLAG_DEFAUT+FLAG_RETARD)) * CIMR24TLDEF
                                 + positif(FLAG_DEFAUT) * 0
                                  + (1-positif(FLAG_DEFAUT)) * (
                                              positif(FLAG_RETARD) * (
                                                       positif(FLAG_RETARD07) * ((1-positif(FLAG_RETARD99)) * (positif(V_FLAGR24) * CIMR2 + (1-positif(V_FLAGR24)) * (CIMR07TLDEF)
                                                                                                                 * positif(positif(PIR-PIRBR2)+null(PIR-PIRBR2)))
                                                                                  + positif(FLAG_RETARD99) * (positif(null(PIR)+positif(V_FLAGR24)) * CIMR2
                                                                                                              + positif(positif(PIR-PIRBR2)+null(PIR-PIRBR2)) * (1-positif(V_FLAGR24))
                                                                                                                       * positif(PIR) * CIMR07TLDEF))
                                                +  (1-positif(FLAG_RETARD07)) * (positif(V_FLAGR24) * max(0,CIMR2-CIMR07TLDEF)
                                                                                     + (1-positif(V_FLAGR24)) * (0*(1-null(PIR-PIRBR2))+max(0,CIMR2-CIMRIRBR2)* null(PIR-PIRBR2)*(1-positif(V_FLAGR22))
                                                                                                        +0* null(PIR-PIRBR2)* positif(V_FLAGR22)))
                                                                   )
                                                           )
                                    );

regle 201906:
application : iliad ;

LOCNPCIPS1 = max(0 , R35NA + R35NK + MIBNETNPVLSV + MIBNPLOCPSV - arr(DEFANTLOCSV * (R35NA + R35NK + MIBNETNPVLSV + MIBNPLOCPSV) / NPLOCNETTSV)) ;
LOCNPCIPS2 = max(0 , R35OA + R35OK + MIBNETNPVLSC + MIBNPLOCPSC - arr(DEFANTLOCSC * (R35OA + R35OK + MIBNETNPVLSC + MIBNPLOCPSC) / NPLOCNETTSC)) ;
LOCNPCIPS3 = max(0 , R35PA + R35PK + MIBNETNPVLSP + MIBNPLOCPSP - arr(DEFANTLOCSP * (R35PA + R35PK + MIBNETNPVLSP + MIBNPLOCPSP) / NPLOCNETTSP)) ;

CIPSLNP1 = max(0 , (positif_ou_nul(LOCNPCIPS1 - (V_BTMRLNP1 * (1 - present(CODXBN)) + CODXBN)) * ((V_BTMRLNP1 * (1 - present(CODXBN)) + CODXBN) - (V_BTMRLNP12 * (1 - present(CODXBO)) + CODXBO)) 
            + positif((V_BTMRLNP1 * (1 - present(CODXBN)) + CODXBN) - LOCNPCIPS1) * positif(LOCNPCIPS1 - (V_BTMAXLNP1 * (1 - present(CODXBP)) + CODXBP)) 
	                                                                          * (LOCNPCIPS1 - (V_BTMRLNP12 * (1 - present(CODXBO)) + CODXBO)))) 
	   * positif(present(V_BTMRLNP12) + present(CODXBO)) * (1 - positif(V_BTLMA * (1 - present(CODXBF)) + CODXBF + V_BT5DH * (1 - present(CODXBG)) + CODXBG + V_BTMRLNP11 * (1 - present(CODXFC)) + CODXFC)) 
	   - ((V_BTMRLNP1 * (1 - present(CODXBN)) + CODXBN) - LOCNPCIPS1) * positif((V_BTMRLNP1 * (1 - present(CODXBN)) + CODXBN) - LOCNPCIPS1) * positif(V_BT5DH * (1 - present(CODXBG)) + CODXBG) ;

CIPSLNP2 = (max(0 , (positif_ou_nul(LOCNPCIPS2 - (V_BTMRLNP2 * (1 - present(CODXDG)) + CODXDG)) * ((V_BTMRLNP2 * (1 - present(CODXDG)) + CODXDG) - (V_BTMRLNP22 * (1 - present(CODXDH)) + CODXDH)) 
             + positif((V_BTMRLNP2 * (1 - present(CODXDG)) + CODXDG) - LOCNPCIPS2) * positif(LOCNPCIPS2 - (V_BTMAXLNP2 * (1 - present(CODXDI)) + CODXDI)) 
	                                                                           * (LOCNPCIPS2 - (V_BTMRLNP22 * (1 - present(CODXDH)) + CODXDH)))) 
	    * positif(present(V_BTMRLNP22) + present(CODXDH)) * (1 - positif(V_BTLMB * (1 - present(CODXCY)) + CODXCY + V_BT5EH * (1 - present(CODXCZ)) + CODXCZ + V_BTMRLNP21 * (1 - present(CODXFF)) + CODXFF)) 
	    - ((V_BTMRLNP2 * (1 - present(CODXDG)) + CODXDG) - LOCNPCIPS2) * positif((V_BTMRLNP2 * (1 - present(CODXDG)) + CODXDG) - LOCNPCIPS2) * positif(V_BT5EH * (1 - present(CODXCZ)) + CODXCZ)) * BOOL_0AM ;

CIPSLNP3 = (max(0 , (positif_ou_nul(LOCNPCIPS3 - (V_BTMRLNP3 * (1 - present(CODXEV)) + CODXEV)) * ((V_BTMRLNP3 * (1 - present(CODXEV)) + CODXEV) - (V_BTMRLNP32 * (1 - present(CODXEW)) + CODXEW)) 
             + positif((V_BTMRLNP3 * (1 - present(CODXEV)) + CODXEV) - LOCNPCIPS3) * positif(LOCNPCIPS3 - (V_BTMAXLNP3 * (1 - present(CODXEX)) + CODXEX)) 
	                                                                           * (LOCNPCIPS3 - (V_BTMRLNP32 * (1 - present(CODXEW)) + CODXEW)))) 
	    * positif(present(V_BTMRLNP32) + present(CODXEW)) * (1 - positif(V_BTLMC * (1 - present(CODXER)) + CODXER + V_BT5FH * (1 - present(CODXES)) + CODXES + V_BTMRLNP31 * (1 - present(CODXFK)) + CODXFK)) 
	    - ((V_BTMRLNP3 * (1 - present(CODXEV)) + CODXEV) - LOCNPCIPS3) * positif((V_BTMRLNP3 * (1 - present(CODXEV)) + CODXEV) - LOCNPCIPS3) * positif(V_BT5FH * (1 - present(CODXES)) + CODXES)) * CIMRPOTE ;

CIPS5HY = max(0 , (positif_ou_nul(RCSV - (V_BT5HY * (1 - present(CODXFD)) + CODXFD)) * ((V_BT5HY * (1 - present(CODXFD)) + CODXFD) - (V_BT5HY12 * (1 - present(CODXBQ)) + CODXBQ)) 
           + positif((V_BT5HY * (1 - present(CODXFD)) + CODXFD) - RCSV) * positif(RCSV - (V_BTMAXHY * (1 - present(CODXBR)) + CODXBR)) 
	                                                                * (RCSV - (V_BT5HY12 * (1 - present(CODXBQ)) + CODXBQ)))) 
	  * positif(present(V_BT5HY12) + present(CODXBQ)) * (1 - positif(V_BTPSA * (1 - present(CODXBH)) + CODXBH + V_BT5EW * (1 - present(CODXBI)) + CODXBI + V_BT5HY11 * (1 - present(CODXFE)) + CODXFE)) 
	  - ((V_BT5HY * (1 - present(CODXFD)) + CODXFD) - RCSV) * positif((V_BT5HY * (1 - present(CODXFD)) + CODXFD) - RCSV) * positif(V_BT5EW * (1 - present(CODXBI)) + CODXBI) ;

CIPS5IY = (max(0 , (positif_ou_nul(RCSC - (V_BT5IY * (1 - present(CODXFG)) + CODXFG)) * ((V_BT5IY * (1 - present(CODXFG)) + CODXFG) - (V_BT5IY22 * (1 - present(CODXDJ)) + CODXDJ)) 
            + positif((V_BT5IY * (1 - present(CODXFG)) + CODXFG) - RCSC) * positif(RCSC - (V_BTMAXIY * (1 - present(CODXDK)) + CODXDK)) 
	                                                                 * (RCSC - (V_BT5IY22 * (1 - present(CODXDJ)) + CODXDJ)))) 
	   * positif(present(V_BT5IY22) + present(CODXDJ)) * (1 - positif(V_BTPSB * (1 - present(CODXDA)) + CODXDA + V_BT5FW * (1 - present(CODXDB)) + CODXDB + V_BT5IY21 * (1 - present(CODXFH)) + CODXFH)) 
	   - ((V_BT5IY * (1 - present(CODXFG)) + CODXFG) - RCSC) * positif((V_BT5IY * (1 - present(CODXFG)) + CODXFG) - RCSC) * positif(V_BT5FW * (1 - present(CODXDB)) + CODXDB)) * BOOL_0AM ;

CIPS5JY = (max(0 , (positif_ou_nul(RCSP - (V_BT5JY * (1 - present(CODXFL)) + CODXFL)) * ((V_BT5JY * (1 - present(CODXFL)) + CODXFL) - (V_BT5JY32 * (1 - present(CODXEY)) + CODXEY)) 
            + positif((V_BT5JY * (1 - present(CODXFL)) + CODXFL) - RCSP) * positif(RCSP - (V_BTMAXJY * (1 - present(CODXEZ)) + CODXEZ)) 
	                                                                 * (RCSP - (V_BT5JY32 * (1 - present(CODXEY)) + CODXEY)))) 
	   * positif(present(V_BT5JY32) + present(CODXEY)) * (1 - positif(V_BTPSC * (1 - present(CODXET)) + CODXET + V_BT5GW * (1 - present(CODXEU)) + CODXEU + V_BT5JY31 * (1 - present(CODXFM)) + CODXFM)) 
	   - ((V_BT5JY * (1 - present(CODXFL)) + CODXFL) - RCSP) * positif((V_BT5JY * (1 - present(CODXFL)) + CODXFL) - RCSP) * positif(V_BT5GW * (1 - present(CODXEU)) + CODXEU)) * CIMRPOTE ;


CIPSBASE = arr(CIPSLNP1 + CIPS5HY + CIPSLNP2 + CIPS5IY + CIPSLNP3 + CIPS5JY) ;

regle 201910:
application : iliad ;


ICIMR8SHMO = positif(COD8SH) * (1 - positif(COD8SI)) * BOOL_0AM ;

ICIMR8SIMO = positif(COD8SI) * (1 - positif(COD8SH)) * BOOL_0AM ;


CIPSBA8SHMO = CIPSLNP2 + CIPSLNP3 + CIPS5IY + CIPS5JY ;

CIPSBA8SIMO = CIPSLNP1 + CIPSLNP3 + CIPS5HY + CIPS5JY ;


CIMRCSGP = (positif(ICIMR8SHMO) * positif(positif(CIPSBA8SHMO + 0) + present(CODCDI)) * ((1 - present(CODCDI)) * arr(CIPSBA8SHMO * T_CSGCRDS/100) + present(CODCDI) * (arr((CODCDI/TX0172) * T_CSGCRDS/100)))
            + positif(ICIMR8SIMO) * positif(positif(CIPSBA8SIMO + 0) + present(CODCDI)) * ((1 - present(CODCDI)) * arr(CIPSBA8SIMO * T_CSGCRDS/100) + present(CODCDI) * (arr((CODCDI/TX0172) * T_CSGCRDS/100)))
            + (1 - positif(COD8SH + COD8SI)) * positif(positif(CIPSBASE + 0) + present(CODCDI)) * ((1 - present(CODCDI)) * arr(CIPSBASE * T_CSGCRDS/100) + present(CODCDI) * (arr((CODCDI/TX0172) * T_CSGCRDS/100))))
           * (1 - positif(V_BTCIMRNUM * (1 - present(CODXFB)) + CODXFB)) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF)))  ;

CIMRPSOLP = (positif(ICIMR8SHMO) * positif(positif(CIPSBA8SHMO + 0) + present(CODCDI)) * ((1 - present(CODCDI)) * arr(CIPSBA8SHMO * TXPSOL/100) + present(CODCDI) * (arr((CODCDI/TX0172) * TXPSOL/100)))
             + positif(ICIMR8SIMO) * positif(positif(CIPSBA8SIMO + 0) + present(CODCDI)) * ((1 - present(CODCDI)) * arr(CIPSBA8SIMO * TXPSOL/100) + present(CODCDI) * (arr((CODCDI/TX0172) * TXPSOL/100)))
             + (1 - positif(COD8SH + COD8SI)) * positif(positif(CIPSBASE + 0) + present(CODCDI)) * ((1 - present(CODCDI)) * arr(CIPSBASE * TXPSOL/100) + present(CODCDI) * (arr((CODCDI/TX0172) * TXPSOL/100))))
            * (1 - positif(V_BTCIMRNUM * (1 - present(CODXFB)) + CODXFB)) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF))) ;

RCIMRCSG = (positif(ICIMR8SHMO) * positif((1 - positif(CIPSBA8SHMO + 0)) + present(CODRDI)) * ((1 - present(CODRDI)) * arr(abs(CIPSBA8SHMO) * T_CSGCRDS/100) + present(CODRDI) * (arr((CODRDI/TX0172) * T_CSGCRDS/100)))
            + positif(ICIMR8SIMO) * positif((1 - positif(CIPSBA8SIMO + 0))+ present(CODRDI)) * ((1 - present(CODRDI)) * arr(abs(CIPSBA8SIMO) * T_CSGCRDS/100) + present(CODRDI) * (arr((CODRDI/TX0172) * T_CSGCRDS/100)))
            + (1 - positif(COD8SH + COD8SI)) * positif((1 - positif(CIPSBASE + 0)) + present(CODRDI)) * ((1 - present(CODRDI)) * arr(abs(CIPSBASE) * T_CSGCRDS/100) + present(CODRDI) * (arr((CODRDI/TX0172) * T_CSGCRDS/100))))
           * (1 - positif(V_BTCIMRNUM * (1 - present(CODXFB)) + CODXFB)) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF)))  ;

RCIMRPSOL = (positif(ICIMR8SHMO) * positif((1 - positif(CIPSBA8SHMO + 0)) + present(CODRDI)) * ((1 - present(CODRDI)) * arr(abs(CIPSBA8SHMO) * TXPSOL/100) + present(CODRDI) * (arr((CODRDI/TX0172) * TXPSOL/100)))
             + positif(ICIMR8SIMO) * positif((1 - positif(CIPSBA8SIMO + 0)) + present(CODRDI)) * ((1 - present(CODRDI)) * arr(abs(CIPSBA8SIMO) * TXPSOL/100) + present(CODRDI) * (arr((CODRDI/TX0172) * TXPSOL/100)))
             + (1 - positif(COD8SH + COD8SI)) * positif((1 - positif(CIPSBASE + 0)) + present(CODRDI)) * ((1 - present(CODRDI)) * arr(abs(CIPSBASE) * TXPSOL/100) + present(CODRDI) * (arr((CODRDI/TX0172) * TXPSOL/100))))
            * (1 - positif(V_BTCIMRNUM * (1 - present(CODXFB)) + CODXFB)) * (1 - positif(V_0AZ)) * positif(positif(null(7 - CMAJ) + null(7 - CMAJ_ISF)) * positif(CMAJ + CMAJ_ISF) + (1 - positif(CMAJ + CMAJ_ISF))) ;

regle 201915:
application : iliad ;

CIMRCSG = (CIMRCSGP - RCIMRCSG) * null(V_IND_TRAIT - 4)
          + null(V_IND_TRAIT - 5) * ((1 - positif(FLAG_DEFAUT + FLAG_RETARD)) * max(0 , CSGC + PCSG - CICSG - CSGIM - CSNET)
                                     + positif(FLAG_DEFAUT) * 0
                                     + (1 - positif(FLAG_DEFAUT)) * positif(FLAG_RETARD) * max(0 , CSGC + PCSG - CICSG - CSGIM - CSNET)) ;

CIMRPSOL = (CIMRPSOLP - RCIMRPSOL) * null(V_IND_TRAIT - 4)
           + null(V_IND_TRAIT - 5) * ((1 - positif(FLAG_DEFAUT + FLAG_RETARD)) * max(0 , MPSOL + PPSOL - CIPSOL- PRSPROV - PSOLNET)
                                      + positif(FLAG_DEFAUT) * 0
                                      + (1 - positif(FLAG_DEFAUT)) * positif(FLAG_RETARD) * max(0 , MPSOL + PPSOL - CIPSOL- PRSPROV - PSOLNET)) ;
 
regle 201920:
application : iliad ;


CIPS = CIMRCSG + CIMRPSOL ;

TOTCIMRPS = CIMR + CIPS ;

IRHCIMRPS = IDRS4 + NAPCRP + CIPS ;


TOTPSC = IRHCIMRPS - TOTCIMRPS ;


ICIMRPS =   0 * (1 - positif(TOTCIMRPS))
          + 1 * ((positif(TOTCIMRPS) * positif((1-null(NATIMP)))) + positif(TOTCIMRPS)*null(NATIMP)*(1-null(TOTPSC)))
          + 2 * (positif(TOTCIMRPS) * null(NATIMP) * null(TOTPSC)) ;

IPREHC = positif(IRHCIMRPS - TOTCIMRPS) ;

regle 201930:
application : iliad ;

CIMRCOR = null(4-V_IND_TRAIT) * max(0 , arr(V_BTCIMRA * min(1 , CIMRB/V_BTCIMRC))
               - (COD8VM + COD8WM + COD8UM) * (1 - positif_ou_nul(COD8PA)) 
	       - min(COD8VM + COD8WM + COD8UM , COD8PA) * positif_ou_nul(COD8PA))
	     + null(5-V_IND_TRAIT) * (CIMR99 + CIMR07NTL + CIMR24NTL + CIMR07TL + CIMR24TL)  ;

