#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2012 
#au titre des revenus percus en 2011. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
 #
 #
 #
 # #####   ######   ####    #####     #     #####  
 # #    #  #       #          #       #       #   
 # #    #  #####    ####      #       #       #  
 # #####   #            #     #       #       # 
 # #   #   #       #    #     #       #       # 
 # #    #  ######   ####      #       #       # 
 #
 #      #####   #####   #####   #
 #          #   #   #   #   #   #
 #      #####   #   #   #   #   #
 #      #       #   #   #   #   #
 #      #####   #####   #####   #
 #
 #
 #
 #
 #                     RES-SER2.m
 #                    =============
 #
 #
 #                      zones restituees par l'application
 #
 #
 #
 #
 #
regle 9071 :
application : pro , oceans , iliad , batch ;
IDRS = INDTXMIN*IMI + 
       INDTXMOY*IMO + 
       (1-INDTXMIN) * (1-INDTXMOY) * max(0,IPHQ2 - ADO1) ;
regle 907100 :
application : pro , oceans , iliad , batch, bareme ;
RECOMP = max(0 ,( IPHQANT2 - IPHQ2 )*(1-INDTXMIN) * (1-INDTXMOY)) 
         * (1 - positif(IPMOND+INDTEFF));
regle 907101 :
application : pro , oceans , iliad , batch ;
IDRSANT = INDTXMIN*IMI + INDTXMOY*IMO 
         + (1-INDTXMIN) * (1-INDTXMOY) * max(0,IPHQANT2 - ADO1) ;
IDRS2 = (1 - positif(IPMOND+INDTEFF))  * 
        ( 
         IDRSANT + ( positif(ABADO)*ABADO + positif(ABAGU)*ABAGU )
                  * positif(IDRSANT)
         + IPHQANT2 * (1 - positif(IDRSANT))
         + positif(RE168+TAX1649) * IAMD2
        )
   + positif(IPMOND+INDTEFF) 
         * ( IDRS*(1-positif(IPHQ2)) + IPHQ2 * positif(IPHQ2) );

IDRS3 = IDRT ;
regle 90710 :
application : pro , oceans , iliad , batch ;
PLAFQF = positif(IS521 - PLANT - IS511) * (1-positif(V_CR2+IPVLOC))
           * ( positif(abs(TEFF)) * positif(IDRS) + (1 - positif(abs(TEFF))) );
regle 907105 :
application : pro ,oceans , iliad , batch ;
ABADO = arr(min(ID11 * (TX_RABDOM / 100)
             * ((PRODOM * max(0,1 - V_EAD - V_EAG) / RG ) + V_EAD),PLAF_RABDOM)
	    );
ABAGU = arr(min(ID11 * (TX_RABGUY / 100)
	     * ((PROGUY * max(0,1 - V_EAD - V_EAG) / RG ) + V_EAG),PLAF_RABGUY)
	    );
regle 90711 :
application : pro , oceans , iliad , batch ;

RGPAR =   positif(PRODOM) * 1 
       +  positif(PROGUY) * 2
       +  positif(PROGUY)*positif(PRODOM) 
       ;

regle 9074 :
application : pro , oceans , iliad , batch ;
IBAEX = (IPQT2) * (1 - INDTXMIN) * (1 - INDTXMOY);
regle 9080 :
application : pro , oceans , iliad , batch ;

PRELIB = PPLIB + RCMLIB ;

regle 9091 :
application : pro , oceans , iliad , batch ;
IDEC = DEC11 * (1 - positif(V_CR2 + V_CNR + IPVLOC));
regle 9092 :
application : pro , oceans , iliad , batch ;
IPROP = ITP ;
regle 9093 :
application : pro , oceans , iliad , batch ;

IREP = REI ;

regle 90981 :
application : pro, batch, oceans, iliad ;
RETIR = RETIR2 + arr(BTOINR * TXINT/100) ;
RETCS = RETCS2 + arr((CSG-CSGIM) * TXINT/100) ;
RETRD = RETRD2 + arr((RDSN-CRDSIM) * TXINT/100) ;
RETPS = RETPS2 + arr((PRS-PRSPROV) * TXINT/100) ;
RETGAIN = RETGAIN2 + arr((CGAINSAL - GAINPROV) * TXINT/100) ;
RETCSAL = RETCSAL2 + arr((CSAL - PROVCSAL) * TXINT/100) ;
RETCDIS = RETCDIS2 + arr((CDIS - CDISPROV) * TXINT/100) ;
RETRSE1 = RETRSE12 + arr(RSE1 * TXINT/100) ;
RETRSE2 = RETRSE22 + arr(RSE2 * TXINT/100) ;
RETRSE3 = RETRSE32 + arr(RSE3 * TXINT/100) ;
RETRSE4 = RETRSE42 + arr(RSE4 * TXINT/100) ;
RETTAXA = RETTAXA2 + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT)) * TXINT/100) ;
RETPCAP = RETPCAP2+arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR)) * TXINT/100) ;
RETHAUTREV = RETCHR2 + arr(max(0,IHAUTREVT- min(IHAUTREVT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT)) * TXINT/100) ;

regle 90984 :
application : batch, pro, oceans, iliad ;
MAJOIRTARDIF_A1 = MAJOIRTARDIF_A - MAJOIR17_2TARDIF_A;
MAJOTAXATARDIF_A1 = MAJOTAXATARDIF_A - MAJOTA17_2TARDIF_A;
MAJOCAPTARDIF_A1 = MAJOCAPTARDIF_A - MAJOCP17_2TARDIF_A;
MAJOHRTARDIF_A1 = MAJOHRTARDIF_A - MAJOHR17_2TARDIF_A;
MAJOIRTARDIF_D1 = MAJOIRTARDIF_D - MAJOIR17_2TARDIF_D;
MAJOTAXATARDIF_D1 = MAJOTAXATARDIF_D - MAJOTA17_2TARDIF_D;
MAJOCAPTARDIF_D1 = MAJOCAPTARDIF_D - MAJOCP17_2TARDIF_D;
MAJOHRTARDIF_D1 = MAJOHRTARDIF_D - MAJOHR17_2TARDIF_D;
MAJOIRTARDIF_P1 = MAJOIRTARDIF_P - MAJOIR17_2TARDIF_P;
MAJOIRTARDIF_R1 = MAJOIRTARDIF_R - MAJOIR17_2TARDIF_R;
MAJOTAXATARDIF_R1 = MAJOTAXATARDIF_R - MAJOTA17_2TARDIF_R;
MAJOCAPTARDIF_R1 = MAJOCAPTARDIF_R - MAJOCP17_2TARDIF_R;
MAJOHRTARDIF_R1 = MAJOHRTARDIF_R - MAJOHR17_2TARDIF_R;
NMAJ1 = max(0,MAJO1728IR + arr(BTO * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOIRTARDIF_D1
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOIRTARDIF_P1
		  + (1 - positif(PROPIR_A) ) * MAJOIRTARDIF_D1)
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOIRTARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOIRTARDIF_A1)
		);
NMAJTAXA1 = max(0,MAJO1728TAXA + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN-IRANT)) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOTAXATARDIF_D1
		+ FLAG_TRTARDIF_F * MAJOTAXATARDIF_D1
	- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOTAXATARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOTAXATARDIF_A1)
		);
NMAJPCAP1 = max(0,MAJO1728PCAP + arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN-IRANT+TAXASSUR)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOCAPTARDIF_D1
                + FLAG_TRTARDIF_F * MAJOCAPTARDIF_D1
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCAPTARDIF_R1
                + (1 - positif(FLAG_RECTIF)) * MAJOCAPTARDIF_A1)
                );
NMAJCHR1 = max(0,MAJO1728CHR + arr(max(0,IHAUTREVT- min(IHAUTREVT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOHRTARDIF_D1
                + FLAG_TRTARDIF_F * MAJOHRTARDIF_D1
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOHRTARDIF_R1
                + (1 - positif(FLAG_RECTIF)) * MAJOHRTARDIF_A1)
                );
NMAJC1 = max(0,MAJO1728CS + arr((CSG - CSGIM) * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJOCSTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPCS_A) * MAJOCSTARDIF_P 
		  + (1 - positif(PROPCS_A) ) * MAJOCSTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPCS_A))
				    * ( positif(FLAG_RECTIF) * MAJOCSTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCSTARDIF_A)
		);
NMAJR1 = max(0,MAJO1728RD + arr((RDSN - CRDSIM) * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJORDTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPRD_A) * MAJORDTARDIF_P 
		  + (1 - positif(PROPRD_A) ) * MAJORDTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPCS_A))
				    * ( positif(FLAG_RECTIF) * MAJORDTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORDTARDIF_A)
		);
NMAJP1 = max(0,MAJO1728PS + arr((PRS - PRSPROV) * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJOPSTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPPS_A) * MAJOPSTARDIF_P 
		  + (1 - positif(PROPPS_A) ) * MAJOPSTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPPS_A))
				    * ( positif(FLAG_RECTIF) * MAJOPSTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOPSTARDIF_A)
		);
NMAJGAIN1 = max(0,MAJO1728GAIN + arr((CGAINSAL - GAINPROV) * COPETO/100)
		+ FLAG_TRTARDIF * MAJOGAINTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOGAINTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOGAINTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOGAINTARDIF_A)
		);
NMAJCSAL1 = max(0,MAJO1728CSAL + arr((CSAL - PROVCSAL) * COPETO/100)
		+ FLAG_TRTARDIF * MAJOCSALTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCSALTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCSALTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCSALTARDIF_A)
		);
NMAJCDIS1 = max(0,MAJO1728CDIS + arr((CDIS - CDISPROV) * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJOCDISTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCDISTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCDISTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCDISTARDIF_A)
		);
NMAJRSE11 = max(0,MAJO1728RSE1 + arr(RSE1 * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJORSE1TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE1TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE1TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE1TARDIF_A)
		);
NMAJRSE21 = max(0,MAJO1728RSE2 + arr(RSE2 * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJORSE2TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE2TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE2TARDIF_A)
		);
NMAJRSE31 = max(0,MAJO1728RSE3 + arr(RSE3 * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJORSE3TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE3TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE3TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE3TARDIF_A)
		);
NMAJRSE41 = max(0,MAJO1728RSE4 + arr(RSE4 * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJORSE4TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE4TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE4TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE4TARDIF_A)
		);
NMAJ3 = max(0,MAJO1758AIR + arr(BTO * COPETO/100) * positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOIR17_2TARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOIR17_2TARDIF_P 
		  + (1 - positif(PROPIR_A) ) * MAJOIR17_2TARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOIR17_2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOIR17_2TARDIF_A)
		);
NMAJTAXA3 = max(0,MAJO1758ATAXA + arr(max(0,TAXASSUR+min(0,IRN-IRANT)) * COPETO/100)
					* positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOTA17_2TARDIF_D
		);
NMAJPCAP3 = max(0,MAJO1758APCAP + arr(max(0,IPCAPTAXT+min(0,IRN-IRANT+TAXASSUR)) * COPETO/100)
                * positif(null(CMAJ-10)+null(CMAJ-17))
                + FLAG_TRTARDIF * MAJOCP17_2TARDIF_D
		);
NMAJCHR3 = max(0,MAJO1758ACHR + arr(max(0,IHAUTREVT+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT)) * COPETO/100)
                * positif(null(CMAJ-10)+null(CMAJ-17))
                + FLAG_TRTARDIF * MAJOHR17_2TARDIF_D
		);
NMAJ4    =      somme (i=03..06,30,32,55: MAJOIRi);
NMAJTAXA4  =    somme (i=03..06,30,55: MAJOTAXAi);
NMAJC4 =  somme(i=03..06,30,32,55:MAJOCSi);
NMAJR4 =  somme(i=03..06,30,32,55:MAJORDi);
NMAJP4 =  somme(i=03..06,30,55:MAJOPSi);
NMAJCSAL4 =  somme(i=03..06,30,55:MAJOCSALi);
NMAJCDIS4 =  somme(i=03..06,30,55:MAJOCDISi);
NMAJPCAP4 =  somme(i=03..06,30,55:MAJOCAPi);
NMAJCHR4 =  somme(i=03..06,30,32,55:MAJOHRi);
NMAJRSE14 =  somme(i=03..06,55:MAJORSE1i);
NMAJRSE24 =  somme(i=03..06,55:MAJORSE2i);
NMAJRSE34 =  somme(i=03..06,55:MAJORSE3i);
NMAJRSE44 =  somme(i=03..06,55:MAJORSE4i);
NMAJGAIN4 =  somme(i=03..06,55:MAJOGAINi);
regle isf 9094 :
application : batch, pro, oceans, iliad ;
MAJOISFTARDIF_A1 = MAJOISFTARDIF_A - MAJOISF17TARDIF_A;
MAJOISFTARDIF_D1 = MAJOISFTARDIF_D - MAJOISF17TARDIF_D;
MAJOISFTARDIF_R1 = MAJOISFTARDIF_R - MAJOISF17TARDIF_R;
NMAJISF1BIS = max(0,MAJO1728ISF + arr(ISF4BASE * COPETO/100)
                   + FLAG_TRTARDIF * MAJOISFTARDIF_D
                   + FLAG_TRTARDIF_F * MAJOISFTARDIF_D
                   - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOISFTARDIF_R
					 + (1 - positif(FLAG_RECTIF)) * MAJOISFTARDIF_A)
                 );
regle 90101 :
application : pro , oceans , iliad , batch ;

IAVIM = IRB + PTOT + TAXASSUR + PTAXA + IPCAPTAXT + PPCAP + IHAUTREVT + PHAUTREV ;

IAVIM2 = IRB + PTOT ;

regle 90113 :
application : pro , oceans , iliad , batch ;
CDBA = positif_ou_nul(SEUIL_IMPDEFBA-SHBA-(REVTP-BA1)
      -GLN1-REV2-REV3-REV4-REVRF);
AGRBG = SHBA + (REVTP-BA1) + GLN1 + REV2 + REV3 + REV4 + REVRF ;

regle 901130 :
application : pro , oceans , iliad , batch ;

DBAIP =  abs(min(BAHQT + BAQT , DAGRI6 + DAGRI5 + DAGRI4 + DAGRI3 + DAGRI2 + DAGRI1)
	     * positif(DAGRI6 + DAGRI5 + DAGRI4 + DAGRI3 + DAGRI2 + DAGRI1) * positif(BAHQT + BAQT)) ;

regle 901131 :
application : pro , oceans , iliad , batch ;

RBAT = max (0 , BANOR) ;

regle 901132 :
application : pro , oceans , iliad , batch ;
DEFIBA = (min(max(1+SEUIL_IMPDEFBA-SHBA-(REVTP-BA1)
      -GLN1-REV2-REV3-REV4-REVRF,0),1)) * min( 0 , BANOR ) ;
regle 901133 :
application :  iliad, batch, pro, oceans ;
NAPALEG = abs(NAPT) ;

INDNAP = 1 - positif_ou_nul(NAPT) ;

GAINDBLELIQ = max(0,V_ANC_NAP*(1-2*V_IND_NAP) - NAPT) * (1-positif(V_0AN)) * (1 - V_CNR2) 
	       * (1 - null(V_REGCO - 2)) * (1 - null(V_REGCO - 4)) * (1 - positif(IPTEFP+IPTEFN+IRANT));

GAINPOURCLIQ = (1 - null(V_ANC_NAP*(1-2*V_IND_NAP))) * (V_ANC_NAP*(1-2*V_IND_NAP) - NAPT)/ V_ANC_NAP*(1-2*V_IND_NAP)  * (1 - V_CNR2);

ANCNAP = V_ANC_NAP * (1-2*V_IND_NAP) ;


INDPPEMENS = positif( ( positif(IRESTIT - 180) 
		       + positif((-1)*ANCNAP - 180) 
                       + positif(IRESTIT - IRNET - 180) * null(V_IND_TRAIT-5)
		      ) * positif(PPETOT - PPERSA - 180) )
	           * (1 - V_CNR) ;

BASPPEMENS = INDPPEMENS * min(max(IREST,(-1)*ANCNAP*positif((-1)*ANCNAP)),PPETOT-PPERSA) * null(V_IND_TRAIT-4) 
            + INDPPEMENS * max(0,min(IRESTIT-IRNET,PPETOT-PPERSA)) * null(V_IND_TRAIT-5) ;

regle 90114 :
application : pro , oceans , iliad , batch ;

IINET = max(0 , NAPT) ;

regle 901140 :
application : bareme  ;

IINET = IRNET * positif ( IRNET - SEUIL_PERCEP ) ;

regle 9011410 :
application : oceans , bareme , pro , iliad , batch ;

IRNET2 =  (IAR + PIR - IRANT) * (1 - INDTXMIN)  * (1 - INDTXMOY)
         + min(0, IAR + PIR - IRANT) * (INDTXMIN + INDTXMOY)
         + max(0, IAR + PIR - IRANT) *
                                   (INDTXMIN * positif(IAVIMBIS - SEUIL_TXMIN)
                                  + INDTXMOY * positif(IAVIMO - SEUIL_TXMIN))
         ;

regle 901141 :
application : oceans , pro , iliad , batch ;

IRNETTER = max ( 0 ,   IRNET2
                       + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER))
		         - max(0,TAXASSUR + PTAXA  - min(TAXASSUR + PTAXA + 0,max(0,INE-IRB+AVFISCOPTER))+ min(0,IRNET2)))
                       + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA))
		         - max(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)))
                       + (IHAUTREVT + PHAUTREV -min(IHAUTREVT + PHAUTREV,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-PTAXA-IPCAPTAXT-PPCAP))
                         - max(0,IHAUTREVT+PHAUTREV - min(IHAUTREVT+PHAUTREV,max(0,INE-IRB+AVFISCOPTER - TAXASSUR - PTAXA - IPCAPTAXT - PPCAP))
												   + min(0,PCAPNEG)))
                )
           ;

IRNETBIS = max(0 , IRNETTER - PIR * positif(SEUIL_REC_CP - IRNETTER + PIR) 
				  * positif(SEUIL_REC_CP - PIR) 
				  * positif_ou_nul(IRNETTER - SEUIL_REC_CP)) ;

regle 901143 :
application : oceans , pro , iliad , batch ;

IRNET =  null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * IRNETBIS * positif_ou_nul(IRB - INE)
          + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
                    *
                    (
                    ((positif(IRE) + positif_ou_nul(IAVIM - SEUIL_PERCEP) * (1 - positif(IRE)))
                    *
                    max(0, CHRNEG + NRINET + IMPRET + (RASAR * V_CR2) + (IRNETBIS * positif(positif_ou_nul(IAVIM - SEUIL_PERCEP)) * positif_ou_nul(IRB - INE))
                    ) * (1 - positif(IREST)))
                    + ((1 - positif_ou_nul(IAVIM - SEUIL_PERCEP)) * (1 - positif(IRE)) * max(0, CHRNEG + NRINET + IMPRET + (RASAR * V_CR2)))
             ) ;
regle 901144 :
application : oceans , pro , iliad , batch ;

TOTNET = max ( 0 , (IAR + PIR) * (1 - (positif(NRINET + IMPRET) * positif(SEUIL_PERCEP - IBM23))) + CHRNEG + NRINET + IMPRET + RASAR - IRANT
                + (TAXASSUR + PTAXA - min(TAXASSUR + PTAXA + 0 , max(0 , INE-IRB+AVFISCOPTER)))
                         * (1 - (positif(NRINET + IMPRET) * positif(SEUIL_PERCEP - IBM23 - TAXASSUR)))
		+ (IPCAPTAXT+PPCAP  - min(IPCAPTAXT+PPCAP,max(0 , INE-IRB+AVFISCOPTER-TAXASSUR - PTAXA )))
                         * (1 - (positif(NRINET + IMPRET) * positif(SEUIL_PERCEP - IBM23 - TAXASSUR-IPCAPTAXT)))
        	+ (IHAUTREVT + PHAUTREV - min(IHAUTREVT+PHAUTREV,max(0,INE-IRB+AVFISCOPTER-TAXASSUR - PTAXA - IPCAPTAXT - PPCAP)))
                         * (1 - (positif(NRINET + IMPRET) * positif(SEUIL_PERCEP - IBM23 - TAXASSUR-IPCAPTAXT-IHAUTREVT)))
             );


regle 9011411 :
application : oceans , pro , iliad , batch ;

TAXANEG = min(0 , TAXASSUR + PTAXA - min(TAXASSUR + PTAXA + 0 , max(0,INE-IRB+AVFISCOPTER)) + min(0 , IRNET2)) ;

TAXNET = positif(TAXASSUR)
	  * max(0 , TAXASSUR + PTAXA  - min(TAXASSUR + PTAXA + 0,max(0,INE-IRB+AVFISCOPTER)) + min(0 , IRNET2)) ;

TAXANET = null(NRINET + IMPRET + 0) * TAXNET
           + positif(NRINET + IMPRET + 0)
             * (positif_ou_nul(IAMD1 - SEUIL_PERCEP) * TAXNET + (1 - positif_ou_nul(IAMD1 - SEUIL_PERCEP)) * 0) ;

regle 90114111 :
application : oceans , pro , iliad , batch ;

PCAPNEG =  min(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)) ;

PCAPTAXNET = positif(IPCAPTAXT)
                * max(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)) ;

PCAPNET = null(NRINET + IMPRET + 0) * PCAPTAXNET
           + positif(NRINET + IMPRET + 0)
             * ( positif_ou_nul(IAMD1 - SEUIL_PERCEP) * PCAPTAXNET + (1 - positif_ou_nul(IAMD1 - SEUIL_PERCEP)) * 0 ) ;

regle 901141111 :
application : oceans , pro , iliad , batch ;


CHRNEG = min(0 , IHAUTREVT + PHAUTREV - min(IHAUTREVT + PHAUTREV , max(0 , INE-IRB+AVFISCOPTER-TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)) + min(0 , PCAPNEG)) ;

CHRNET = positif(IHAUTREVT)
                * max(0,IHAUTREVT+PHAUTREV - min(IHAUTREVT+PHAUTREV,max(0,INE-IRB+AVFISCOPTER - TAXASSUR - PTAXA - IPCAPTAXT - PPCAP))
													  + min(0,PCAPNEG))
                ;

HAUTREVNET = (null(NRINET + IMPRET + 0) * CHRNET
                +
                positif(NRINET + IMPRET + 0)
                * ( positif_ou_nul(IAMD1 - SEUIL_PERCEP) * CHRNET
                + (1 - positif_ou_nul(IAMD1 - SEUIL_PERCEP)) * 0 )
             ) * (1-null(1-FLAG_ACO))
                 ;

regle 9011412 :
application : bareme ;

IRNET = max(0 , IRNET2 + RECOMP) ;

regle 9011413 :
application : oceans , pro , iliad , batch ;

IRPROV = min (IRANT , IAR + PIR) * positif(IRANT) ;

regle 9012401 :
application : pro , batch , oceans , iliad ;

NAPPS = (PRS + PPRS - PRSPROV) * positif(TOTCR) ;
NAPCS = (CSG + PCSG - CSGIM) * positif(TOTCR) ;
NAPRD = (RDSN + PRDS - CRDSIM) * positif(TOTCR) ;
NAPGAIN = (CGAINSAL + PGAIN - GAINPROV) * positif(TOTCR) ;
NAPCSAL = (CSAL + PCSAL - PROVCSAL) * positif(TOTCR) ;
NAPCDIS = (CDIS + PCDIS - CDISPROV) * positif(TOTCR) ;
NAPRSE1 = (RSE1 + PRSE1) * positif(TOTCR) ;
NAPRSE2 = (RSE2 + PRSE2) * positif(TOTCR) ;
NAPRSE3 = (RSE3 + PRSE3) * positif(TOTCR) ;
NAPRSE4 = (RSE4 + PRSE4) * positif(TOTCR) ;

NAPCRP = max(0 , NAPPS + NAPCS + NAPRD + NAPGAIN + NAPCSAL + NAPCDIS + NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4)
         * positif_ou_nul(NAPPS + NAPCS + NAPRD + NAPGAIN + NAPCSAL + NAPCDIS  + NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4 - SEUIL_REC_CP2) ;

regle 9011402 :
application : pro , oceans , iliad , batch ;
IKIRN = KIR ;

IMPTHNET = max(0 , (IRB * positif_ou_nul(IRB-SEUIL_PERCEP)-INE-IRE)
		       * positif_ou_nul((IRB*positif_ou_nul(IRB-SEUIL_PERCEP)-INE-IRE)-SEUIL_REC_CP)) 
	     * (1 - V_CNR) ;

regle 90115 :
application : pro , oceans, iliad , batch ;

IRESTIT = abs(min(0 , IRN + PIR + NRINET + IMPRET + RASAR
                    + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER)))
                    + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA)))
                    + ((IHAUTREVT + PHAUTREV) * (1-null(1-FLAG_ACO))                                                   
		      -min((IHAUTREVT + PHAUTREV)* (1-null(1-FLAG_ACO)),max(0,INE-IRB+AVFISCOPTER-TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)))
                 )
	     ) ;

regle 901150 :
application : oceans ;

IRESTITA = abs(min(0 , PIR_A)) ;

IREST = max(0 , IRESTIT - (IRESTITA * positif_ou_nul(IRESTITA-SEUIL_REMBCP))) ;

regle 901151 :
application : pro , iliad , batch ;

IREST = max(0 , IRESTIT - RECUMBIS) ;

regle corrective 901160 :
application : pro , batch , oceans , iliad ;

TOTREC = positif_ou_nul(IRN + TAXANET + PIR + PCAPNET + HAUTREVNET - SEUIL_REC_CP) ;

regle 90116011 :
application : pro , batch , oceans , iliad ;

CSREC = positif(NAPCR) ;

CSRECINR = positif(NAPCRINR) ;

regle 90116 :
application : pro , batch , iliad , oceans ;

RSEREC = positif(max(0 , NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4)
                 * positif_ou_nul(NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4 - SEUIL_REC_CP2)) ;

regle 9011603 :
application : pro , batch , oceans , iliad ;

CSRECA = positif_ou_nul(PRS_A + PPRS_A + CSG_A + RDS_A + PCSG_A + PRDS_A 
		       + CSAL_A + CDIS_A + GAINBASE_A + RSE1BASE_A + RSE2BASE_A + RSE3BASE_A + RSE4BASE_A - SEUIL_REC_CP2) ;

regle corrective 9011600 :
application : oceans;

IDEGR = positif(-NAPT) 
	 * (TOTREC * abs(NAPT) + (1 - TOTREC) * max(0 , PIR_A)) ;

regle isf 90110 :
application : iliad ;
ISFDEGR = max(0,(ANTISFAFF  - ISF4 * positif_ou_nul (ISF4 - SEUIL_REC_CP)) 
	   * (1-positif_ou_nul (ISF4 - SEUIL_REC_CP))
          + (ANTISFAFF  - ISFNET * positif_ou_nul (ISFNET - SEUIL_REC_CP))
	   * positif_ou_nul(ISF4 - SEUIL_REC_CP)) ;


ISFDEG = ISFDEGR * positif_ou_nul(ISFDEGR - SEUIL_REMBCP) ;

regle corrective 9011602 :
application : iliad ;
IDEGR = (ANTIRAFF + TAXANTAFF + PCAPANTAFF + HAUTREVANTAF - ((IRNET * positif_ou_nul(IRNET - SEUIL_REC_CP)
                                                             + TAXANET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP)
                                                             + PCAPNET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP)
                                                             + HAUTREVNET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP))
                                                                * positif(NRINET + IMPRET + RASAR * (V_CR2))
                                                             + (IRNET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP)
                                                             + TAXANET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP)
                                                             + PCAPNET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP)
                                                             + HAUTREVNET * positif_ou_nul(IBM23 + TAXASSUR + IPCAPTAXT + IHAUTREVT - SEUIL_PERCEP))
                                                                * (1 - positif(NRINET + IMPRET + RASAR * (V_CR2))))
                                                            * positif_ou_nul(IRNET + TAXANET + PCAPNET + HAUTREVNET - SEUIL_REC_CP))
          * positif(ANTIRAFF + TAXANTAFF + PCAPANTAFF + HAUTREVANTAF - (IRNET + TAXANET + PCAPNET + HAUTREVNET)) ;

IRDEG = positif(NAPTOTA - IRNET) * positif(NAPTOTA) * max(0 , V_ANTIR - max(0,IRNET))
	* positif_ou_nul(IDEGR - SEUIL_REMBCP) ;                   

TAXDEG = positif(NAPTOTA - TAXANET) * positif(NAPTOTA) * max(0 , V_TAXANT - max(0,TAXANET)) ;                    

TAXADEG = positif(TAXDEG) * positif(V_TAXANT) * max(0 , V_TAXANT - max(0,TOTAXAGA))
          * positif_ou_nul(IDEGR - SEUIL_REMBCP) ;

PCAPTAXDEG = positif(NAPTOTA - PCAPNET) * positif(NAPTOTA) * max(0 , V_PCAPANT- max(0,PCAPNET)) ;


PCAPDEG = positif(PCAPTAXDEG) * positif (V_PCAPANT) * max(0 , V_PCAPANT - max(0,PCAPTOT)) 
          * positif_ou_nul(IDEGR - SEUIL_REMBCP) ;

HAUTREVTAXDEG =  positif(NAPTOTA - HAUTREVNET) * positif(NAPTOTA) * max(0 , V_CHRANT - max(0,HAUTREVNET)) ;

HAUTREVDEG = positif(HAUTREVTAXDEG) * positif(V_CHRANT) * max(0 , V_CHRANT - max(0,HAUTREVTOT)) 
             * positif_ou_nul(IDEGR - SEUIL_REMBCP) ;
regle 90504:
application : pro ,  batch , oceans , iliad ;
ABSRE = ABMAR + ABVIE;
regle 90509:
application : pro , oceans , iliad , batch ;

REVTP = PTP ;

regle 90522:
application : pro , iliad , oceans ;
RPEN = PTOTD ;
regle isf 905270:
application : iliad  ;
ANTISFAFF = V_ANTISF ;    

regle 90527:
application : oceans , iliad  ;
ANTIRAFF = V_ANTIR  * APPLI_ILIAD   
           +
            (  PIR_A * ( positif_ou_nul(PIR_A-SEUIL_REC_CP)))
            * APPLI_OCEANS 
	    + 0 ;

TAXANTAFF = V_TAXANT * APPLI_ILIAD
            + TAXANET_A * APPLI_OCEANS
	    + 0 ;

PCAPANTAFF = V_PCAPANT * APPLI_ILIAD
            + PCAPNET_A * APPLI_OCEANS
	    + 0 ;

HAUTREVANTAF = V_CHRANT * APPLI_ILIAD
            + HAUTREVNET_A * APPLI_OCEANS
	    + 0 ;
regle 90514:
application : pro , oceans , iliad , batch ;
IDRT = IDOM11;
regle 90525:
application : pro , iliad , batch , oceans ;
IAVT = IRE - EPAV - CICA + 
          min( IRB , IPSOUR + CRCFA ) +
          min( max(0,IAN - IRE) , (BCIGA * (1 - positif(RE168+TAX1649))));
IAVT2 = IAVT + CICA;
regle 907001  :
application : pro, oceans, iliad, batch ;
INDTXMOY = positif(TX_MIN_MET - TMOY) * positif( (present(RMOND) 
                             + present(DMOND)) ) * V_CR2 ;
INDTXMIN = positif_ou_nul( IMI - IPQ1 ) 
           * positif(1 - INDTXMOY) * V_CR2;
regle 907002  :
application : pro, batch,  iliad, oceans ;
IND_REST = positif(IREST) ;
regle 907003  :
application : oceans, iliad, pro, batch ;
IND_NI =  null(NAPT) * (null (IRNET));
regle 9070030  :
application :  oceans, iliad, pro, batch ;
IND_IMP = positif(NAPT);

INDNMR =  null(NAPT) * null(NAT1BIS) * (positif (IRNET + TAXANET + PCAPNET + HAUTREVNET));

INDNMRI = INDNMR * positif ( RED ) ;

INDNIRI =   positif(IDOM11-DEC11) * null(IAD11);
regle 907004  :
application : batch , pro , iliad ;

IND_REST50 = positif(SEUIL_REMBCP - IREST) * positif(IREST) ;

regle 9070041  :
application : oceans, iliad, pro, batch;
INDMAJREV = positif(
 positif(BIHNOV)
+ positif(BIHNOC)
+ positif(BIHNOP)
+ positif(BICHREV)
+ positif(BICHREC)
+ positif(BICHREP)
+ positif(BNHREV)
+ positif(BNHREC)
+ positif(BNHREP)
+ positif(ANOCEP)
+ positif(ANOVEP)
+ positif(ANOPEP)
+ positif(BAFV)
+ positif(BAFC)
+ positif(BAFP)
+ positif(BAHREV)
+ positif(BAHREC)
+ positif(BAHREP)
+ positif(4BAHREV)
+ positif(4BAHREC)
+ positif(4BAHREP)
+ positif(REGPRIV)
);
regle 907005  :
application : oceans , iliad , pro , batch ;
INDNMR1 = (1 - positif(IAMD1 +1 -SEUIL_PERCEP)) *
           (1 - min (1 , abs (NAPT))) * positif(IRB2);
INDNMR2 = positif(INDNMR) * (1 - positif(INDNMR1));
regle 907006  :
application : batch,iliad , pro , oceans ;


INDV = positif_ou_nul ( 
  positif( ALLOV ) 
 + positif( REMPLAV ) + positif( REMPLANBV )
 + positif( BACDEV ) + positif( BACREV )
 + positif( 4BACREV ) + positif( 4BAHREV )
 + positif( BAFPVV )
 + positif( BAFV ) + positif( BAHDEV ) + positif( BAHREV )
 + positif( BICDEV ) + positif( BICDNV )
 + positif( BICHDEV )
 + positif( BICHREV ) + positif( BICNOV )
 + positif( BICREV ) 
 + positif( BIHDNV ) + positif( BIHNOV )
 + positif( BNCAADV ) + positif( BNCAABV ) + positif( BNCDEV ) + positif( BNCNPPVV )
 + positif( BNCNPV ) + positif( BNCPROPVV ) + positif( BNCPROV )
 + positif( BNCREV ) + positif( BNHDEV ) + positif( BNHREV )
 + positif( BPCOSAV ) + positif( CARPENBAV ) + positif( CARPEV )
 + positif( CARTSNBAV ) + positif( CARTSV ) + positif( COTFV )
 + positif( DETSV ) + positif( FRNV ) + positif( GLD1V )
 + positif( GLD2V ) + positif( GLD3V ) + positif( ANOCEP )
 + positif( MIBNPPRESV ) + positif( MIBNPPVV ) + positif( MIBNPVENV )
 + positif( MIBPRESV ) + positif( MIBPVV ) + positif( MIBVENV )
 + positif( PALIV ) + positif( PENSALV ) + positif( PENSALNBV ) 
 + positif( PEBFV ) + positif( PRBRV )
 + positif( TSHALLOV ) + positif( DNOCEP ) + positif(BAFORESTV)
 + positif( LOCPROCGAV ) + positif( LOCPROV ) + positif( LOCNPCGAV )
 + positif( LOCNPV ) + positif( LOCDEFNPCGAV ) + positif( LOCDEFNPV )
 + positif( MIBMEUV ) + positif( MIBGITEV )
);
INDC = positif_ou_nul ( 
  positif( ALLOC ) 
 + positif( REMPLAC ) + positif( REMPLANBC )
 + positif( BACDEC ) + positif( BACREC )
 + positif( 4BACREC ) + positif( 4BAHREC )
 + positif( BAFC ) + positif( ANOVEP ) + positif( DNOCEPC )
 + positif( BAFPVC ) + positif( BAHDEC ) + positif( BAHREC )
 + positif( BICDEC ) + positif( BICDNC )
 + positif( BICHDEC ) 
 + positif( BICHREC ) + positif( BICNOC )
 + positif( BICREC )  
 + positif( BIHDNC ) + positif( BIHNOC )
 + positif( BNCAADC ) + positif( BNCAABC ) + positif( BNCDEC ) + positif( BNCNPC )
 + positif( BNCNPPVC ) + positif( BNCPROC ) + positif( BNCPROPVC )
 + positif( BNCREC ) + positif( BNHDEC ) + positif( BNHREC )
 + positif( BPCOSAC ) + positif( CARPEC ) + positif( CARPENBAC )
 + positif( CARTSC ) + positif( CARTSNBAC ) + positif( COTFC )
 + positif( DETSC ) + positif( FRNC ) + positif( GLD1C )
 + positif( GLD2C ) + positif( GLD3C )
 + positif( MIBNPPRESC ) + positif( MIBNPPVC ) + positif( MIBNPVENC )
 + positif( MIBPRESC ) + positif( MIBPVC ) + positif( MIBVENC )
 + positif( PALIC ) + positif( PENSALC ) + positif( PENSALNBC )
 + positif( PEBFC ) 
 + positif( PRBRC ) + positif( TSHALLOC ) + positif(BAFORESTC)
 + positif( LOCPROCGAC ) + positif( LOCPROC ) + positif( LOCNPCGAC )
 + positif( LOCNPC ) + positif( LOCDEFNPCGAC ) + positif( LOCDEFNPC )
 + positif( MIBMEUC ) + positif( MIBGITEC )
 );
INDP = positif_ou_nul (
  positif( ALLO1 ) + positif( ALLO2 ) + positif( ALLO3 ) + positif( ALLO4 ) 
 + positif( CARTSP1 ) + positif( CARTSP2 ) + positif( CARTSP3 ) + positif( CARTSP4 )
 + positif( CARTSNBAP1 ) + positif( CARTSNBAP2 ) + positif( CARTSNBAP3 ) + positif( CARTSNBAP4 )
 + positif( REMPLAP1 ) + positif( REMPLAP2 ) + positif( REMPLAP3 ) + positif( REMPLAP4 )
 + positif( REMPLANBP1 ) + positif( REMPLANBP2 ) + positif( REMPLANBP3 ) + positif( REMPLANBP4 )
 + positif( BACDEP ) + positif( BACREP )
 + positif( 4BACREP ) + positif( 4BAHREP )
 + positif( BAFP ) + positif( ANOPEP ) + positif( DNOCEPP )
 + positif( BAFPVP ) + positif( BAHDEP ) + positif( BAHREP )
 + positif( BICDEP ) + positif( BICDNP )
 + positif( BICHDEP ) 
 + positif( BICHREP ) + positif( BICNOP )
 + positif( BICREP )  
 + positif( BIHDNP ) + positif( BIHNOP )
 + positif( BNCAADP ) + positif( BNCAABP ) + positif( BNCDEP ) + positif( BNCNPP )
 + positif( BNCNPPVP ) + positif( BNCPROP ) + positif( BNCPROPVP )
 + positif( BNCREP ) + positif( BNHDEP ) + positif( BNHREP )
 + positif( COTF1 ) + positif( COTF2 ) + positif( COTF3 ) + positif( COTF4 ) 
 + positif( DETS1 ) + positif( DETS2 ) + positif( DETS3 ) + positif( DETS4 ) 
 + positif( FRN1 ) + positif( FRN2 ) + positif( FRN3 ) + positif( FRN4 )
 + positif( MIBNPPRESP ) + positif( MIBNPPVP ) + positif( MIBNPVENP )
 + positif( MIBPRESP ) + positif( MIBPVP ) + positif( MIBVENP )
 + positif( PALI1 ) + positif( PALI2 ) + positif( PALI3 ) + positif( PALI4 ) 
 + positif( PENSALP1 ) + positif( PENSALP2 ) + positif( PENSALP3 ) + positif( PENSALP4 )
 + positif( PENSALNBP1 ) + positif( PENSALNBP2 ) + positif( PENSALNBP3 ) + positif( PENSALNBP4 )
 + positif( PEBF1 ) + positif( PEBF2 ) + positif( PEBF3 ) + positif( PEBF4 ) 
 + positif( PRBR1 ) + positif( PRBR2 ) + positif( PRBR3 ) + positif( PRBR4 ) 
 + positif( CARPEP1 ) + positif( CARPEP2 ) + positif( CARPEP3 ) + positif( CARPEP4 )
 + positif( CARPENBAP1 ) + positif( CARPENBAP2 ) + positif( CARPENBAP3 ) + positif( CARPENBAP4 )
 + positif( TSHALLO1 ) + positif( TSHALLO2 ) + positif( TSHALLO3 ) + positif( TSHALLO4 ) 
 + positif( BAFORESTP )
 + positif( LOCPROCGAP ) + positif( LOCPROP ) + positif( LOCNPCGAPAC )
 + positif( LOCNPPAC ) + positif( LOCDEFNPCGAPAC ) + positif( LOCDEFNPPAC )
