#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2022]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2022 
#au titre des revenus perçus en 2021. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
                                                                         #####
  ####   #    #    ##    #####      #     #####  #####   ######         #     #
 #    #  #    #   #  #   #    #     #       #    #    #  #                    #
 #       ######  #    #  #    #     #       #    #    #  #####           #####
 #       #    #  ######  #####      #       #    #####   #                    #
 #    #  #    #  #    #  #          #       #    #   #   #              #     #
  ####   #    #  #    #  #          #       #    #    #  ###### #######  #####
 #
 #
 #
 #
 #
 #
 #
 #                       CALCUL DE L'IMPOT NET
 #
 #
 #
 #
 #
 #
regle 301000:
application : bareme , iliad ;

IRN = min(0 , IAN + AVFISCOPTER - IRE) + max(0 , IAN + AVFISCOPTER - IRE) * positif( IAMD1 + 1 - SEUIL_61) ;


regle 301005:
application : bareme , iliad ;
IRNAF = min( 0, IAN - IRE) + max( 0, IAN - IRE) * positif( IAMD1AF + 1 - SEUIL_61) ;

regle 301010:
application : bareme , iliad ;


IAR = min( 0, IAN + AVFISCOPTER - IRE) + max( 0, IAN + AVFISCOPTER - IRE) ;

regle 301015:
application : bareme , iliad ;

IARAF = min(0 , IANAF - IREAF) + max(0 , IANAF - IREAF) + NEGIANAF ;

regle 301027:
application : iliad ;
BLOY7LS = COD7LS * (1 - V_CNR) ;
LOY7LS = arr(COD7LS * TX50/100) * (1-V_CNR);

LOY8LA = COD8LA * (1 - V_CNR) ;

regle 301020:
application : iliad ;

CREREVET = min(arr((BPTP3 + BPTPD + BPTPG) * TX128/100),arr(CIIMPPRO * TX128/100 ))
	   + min(arr(BPTP19 * TX19/100),arr(CIIMPPRO2 * TX19/100 ))
	   + min (arr(RCMIMPTR * TX075/100),arr(COD8XX * TX075/100)) 
	   + min (arr(BPTP10 * TX10/100),arr(COD8XV * TX10/100)) 
	   ;

CIIMPPROTOT = CIIMPPRO + CIIMPPRO2 + COD8XX + COD8XX + COD8XV;

regle 301030:
application : iliad ;

ICI8XFH = min(arr(BPTP18 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));
ICIGLO = min(arr(BPTP18 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));

CIGLOTOT = COD8XF + COD8XG + COD8XH; 
regle 301032:
application : iliad ;

CI8XFH = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET , ICI8XFH)) ;

CIGLO = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET , ICIGLO)) ;

regle 301035:
application : iliad ;

CI8XFHAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF , ICI8XFH)) ;

CIGLOAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF , ICIGLO)) ;

regle 301040:
application : iliad ;


ICREREVET = max(0 , min(IAD11 + ITP - CIRCMAVFT - IRETS , min(ITP , CREREVET))) ;

regle 301045:
application : iliad ;

ICREREVETAF = max(0 , min(IAD11 + ITP - CIRCMAVFTAF - IRETSAF , min(ITP , CREREVET))) ;

regle 301050:
application : iliad , bareme ;

INE = (CIRCMAVFT + IRETS + ICREREVET + CIGLO + CIDONENTR + CICORSE + CIRECH + CICOMPEMPL) * (1 - positif(RE168 + TAX1649)) ;

IAN = max(0 , (IRB - AVFISCOPTER - INE
               + min(TAXASSUR + 0 , max(0 , INE - IRB + AVFISCOPTER)) 
               + min(IPCAPTAXTOT + 0 , max(0 , INE - IRB + AVFISCOPTER - min(TAXASSUR + 0 , max(0 , INE - IRB + AVFISCOPTER))))
	      )
         ) ;
IANINR = max(0 , (IRBINR - AVFISCOPTER - INE
               + min(TAXASSUR + 0 , max(0 , INE - IRBINR + AVFISCOPTER)) 
               + min(IPCAPTAXTOT + 0 , max(0 , INE - IRBINR + AVFISCOPTER - min(TAXASSUR + 0 , max(0 , INE - IRBINR + AVFISCOPTER))))
	      )
         ) ;

regle 301055:
application : iliad , bareme ;

INEAF = (CIRCMAVFTAF + IRETSAF + ICREREVETAF + CIGLOAF + CIDONENTRAF + CICORSEAF + CIRECHAF + CICOMPEMPLAF)
            * (1-positif(RE168+TAX1649));
regle 301057:
application : iliad , bareme ;

IANAF = max( 0, (IRBAF  + ((- CIRCMAVFTAF
				     - IRETSAF
                                     - ICREREVETAF
                                     - CIGLOAF
                                     - CIDONENTRAF
                                     - CICORSEAF
				     - CIRECHAF
                                     - CICOMPEMPLAF)
                                   * (1 - positif(RE168 + TAX1649)))
                  + min(TAXASSUR+0 , max(0,INEAF-IRBAF)) 
                  + min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))
	      )
         ) ;

NEGIANAF = -1 * (min(TAXASSUR+0 , max(0,INEAF-IRBAF))
                 + min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))) ;

regle 301060:
application : iliad ;


IRE = (EPAV + CRICH + CICORSENOW + CIGE + CIDEVDUR + CITEC + CICA + CIGARD + CISYND 
       + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM + CREAGRIBIO + CRESINTER 
       + CREFORMCHENT + CREARTS + CICONGAGRI + AUTOVERSLIB + CIPAP
       + CI2CK + CIFORET + CIHJA + LOY8LA + COD8TE + LOY7LS + CIVHELEC+ CREAGRIHVE+CREAGRIGLY
       + COD8TL) * (1 - positif(RE168 + TAX1649 + 0)) ;

IREAF = (EPAV + CRICH + CICORSENOW + CIGE + CIDEVDUR + CITEC + CICA + CIGARD + CISYND 
       + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM + CREAGRIBIO + CRESINTER 
       + CREFORMCHENT + CREARTS + CICONGAGRI + AUTOVERSLIB + CIPAP
       + CI2CK + CIFORET + CIHJA + LOY8LA + COD8TE + LOY7LS + CIVHELEC+ CREAGRIHVE+CREAGRIGLY
       + COD8TL) * (1 - positif(RE168 + TAX1649 + 0)) ;

IRE2 = IRE ; 

regle 301065:
application : iliad ;

CIHJA = CODHJA * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;

regle 301070:
application : iliad ;

CRICH =  IPRECH * (1 - positif(RE168+TAX1649));

regle 301080:
application : iliad ;


CIRCMAVFT = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER , RCMAVFT * (1 - V_CNR)));

regle 301085:
application : iliad ;

CIRCMAVFTAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT , RCMAVFT * (1 - V_CNR)));

regle 301100:
application : iliad;
CI2CK = COD2CK * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);

regle 301110:
application : iliad;


CICA =  arr(BAILOC98 * TX_BAIL / 100) * (1 - positif(RE168 + TAX1649)) ;

regle 301130:
application : iliad ;


IPAE = COD8VL + COD8VM + COD8WM + COD8UM ;

RASIPSOUR =  IPSOUR * positif( null(V_REGCO-2) + null(V_REGCO-3) ) * ( 1 - positif(RE168+TAX1649) );

RASIPAE = COD8VM + COD8WM + COD8UM ;

regle 301133:
application : iliad ;

IRETS1 = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT , RASIPSOUR)) ;

IRETS21 = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS1 , min(COD8PB , COD8VL) * present(COD8PB) + COD8VL * (1 - present(COD8PB)))) 
          * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) ;

IRETS2 = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS1 - IRETS21 , min(COD8PA , RASIPAE) * present(COD8PA) + RASIPAE * (1 - present(COD8PA)))) 
         * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) + IRETS21 ;
	
IRETS = IRETS1 + IRETS2 ;

regle 301135:
application : iliad ;

IRETS1AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF , RASIPSOUR)) ;

IRETS21AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT + - CIRCMAVFTAF - IRETS1AF , min(COD8PB , COD8VL) * present(COD8PB) + COD8VL * (1 - present(COD8PB))))
            * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) ;

IRETS2AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETS1AF - IRETS21AF , min(COD8PA , RASIPAE) * present(COD8PA) + RASIPAE * (1 - present(COD8PA))))
           * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) + IRETS21AF ;
	
IRETSAF = IRETS1AF + IRETS2AF ;

regle 301170:
application : iliad ;

BCIDONENTR = RDMECENAT * (1-V_CNR) ;

regle 301172:
application : iliad ;

CIDONENTR = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - REI - IRETS - ICREREVET - CIGLO , BCIDONENTR)) ;

regle 301175:
application : iliad ;

CIDONENTRAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - REI - IRETSAF - ICREREVETAF - CIGLOAF , BCIDONENTR)) ;

regle 301180:
application : iliad ;

CICORSE = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IPPRICORSE - IRETS - ICREREVET - CIGLO - CIDONENTR , CIINVCORSE + IPREPCORSE)) ;

CICORSEAVIS = CICORSE + CICORSENOW ;

TOTCORSE = CIINVCORSE + IPREPCORSE + CICORSENOW ;

regle 301185:
application : iliad ;

CICORSEAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IPPRICORSE - IRETSAF - ICREREVETAF - CIGLOAF - CIDONENTRAF , CIINVCORSE + IPREPCORSE)) ;

CICORSEAVISAF = CICORSEAF + CICORSENOW ;

regle 301190:
application : iliad ;

CIRECH = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET - CIGLO - CIDONENTR - CICORSE , IPCHER)) ;

regle 301195:
application : iliad ;

CIRECHAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF - CIGLOAF - CIDONENTRAF - CICORSEAF , IPCHER)) ;
regle 301200:
application : iliad ;

CICOMPEMPL = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET - CIGLO - CIDONENTR - CICORSE - CIRECH , COD8UW)) ;

DIEMPLOI = (COD8UW + COD8TL) * (1 - positif(RE168+TAX1649)) ;

CIEMPLOI = (CICOMPEMPL + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECR = abs(min(0 ,IRB+TAXASSUR+IPCAPTAXT -AVFISCOPTER-CIRCMAVFT-IRETS-ICREREVET-CIGLO-CIDONENTR-CICORSE-CIRECH-CICOMPEMPL));

regle 301205:
application : iliad ;

CICOMPEMPLAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF - CIGLOAF - CIDONENTRAF - CICORSEAF - CIRECHAF , COD8UW)) ;

CIEMPLOIAF = (CICOMPEMPLAF + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECRAF = abs(min(0 ,IRBAF+TAXASSUR+IPCAPTAXT -CIRCMAVFTAF-IRETSAF-ICREREVETAF-CIGLOAF-CIDONENTRAF-CICORSEAF-CIRECHAF-CICOMPEMPLAF));

regle 301210:
application : iliad ;
  
REPCORSE = abs(CIINVCORSE+IPREPCORSE-CICORSE) ;
REPRECH = abs(IPCHER - CIRECH) ;
REPCICE = abs(COD8UW - CICOMPEMPL) ;

regle 301220:
application : iliad ;

CICONGAGRI = CRECONGAGRI * (1-V_CNR) ;

regle 301230:
application : iliad ;

BCICAP = arr(PRELIBXT * TX90/100 * T_PCAPTAX/100) ;

regle 301233:
application : iliad ;

BCICAPAVIS = arr(PRELIBXT * TX90/100) ;

CICAP = max(0 , min(IPCAPTAXTOT , BCICAP)) ;

regle 301235:
application : iliad ;

CICAPAF = CICAP ;

regle 301240:
application : iliad ;

BCICHR = arr(CHRAPRES * ((REGCIAUTO+CIIMPPRO)*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR - TEFFHRC+COD8YJ));
regle 301242:
application : iliad ;

CICHR = max(0,min(IRB + TAXASSUR + IPCAPTAXT +CHRAPRES - AVFISCOPTER ,min(CHRAPRES,BCICHR)));
regle 301245:
application : iliad ;

CICHRAF = max(0,min(IRBAF + TAXASSUR + IPCAPTAXT +CHRAPRES ,min(CHRAPRES,BCICHR)));
regle 301247:
application : iliad ;

BCICHR3WH = arr(CHRAPRES3WH * (REGCIAUTO*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR+PVREPORT - TEFFHRC+COD8YJ));

regle 301249:
application : iliad ;

CICHR3WH = max(0,min(IRB + TAXASSUR + IPCAPTAXT +CHRAPRES3WH - AVFISCOPTER ,min(CHRAPRES3WH,BCICHR3WH)));

regle 301252:
application : iliad ;

CICHR3WHAF = max(0,min(IRBAF + TAXASSUR + IPCAPTAXTOT +CHRAPRES3WH -CICAPAF ,min(CHRAPRES3WH,BCICHR3WH)));

regle 301257:
application : iliad ;



DSYND = RDSYVO + RDSYCJ + RDSYPP ;


SOMBCOSV = TSHALLOV + COD1AA + CARTSV + ALLOV + REMPLAV + COD1GB + COD1GF + COD1GG + COD1AF  
           + CODRAF + COD1AG + CODRAG + PRBRV + CARPEV + PALIV + PENSALV + CODDAJ + CODEAJ 
	   + PENINV + CODRAZ + COD1AL + CODRAL + COD1AM + CODRAM + COD1TP + GLDGRATV 
	   + COD1TZ + COD1NX + max(0,COD1GH - LIM5000);

SOMBCOSC = TSHALLOC + COD1BA + CARTSC + ALLOC + REMPLAC + COD1HB + COD1HF + COD1HG + COD1BF 
           + CODRBF + COD1BG + CODRBG + PRBRC + CARPEC + PALIC + PENSALC + CODDBJ + CODEBJ 
	   + PENINC + CODRBZ + COD1BL + CODRBL + COD1BM + CODRBM + COD1UP + GLDGRATC + COD1OX + max(0,COD1HH - LIM5000);

SOMBCOSP = TSHALLO1 + TSHALLO2 + TSHALLO3 + TSHALLO4 + COD1CA + COD1DA + COD1EA + COD1FA   
           + CARTSP1 + CARTSP2 + CARTSP3 + CARTSP4 + ALLO1 + ALLO2 + ALLO3 + ALLO4    
           + REMPLAP1 + REMPLAP2 + REMPLAP3 + REMPLAP4 + COD1IB + COD1IF + COD1JB   
           + COD1JF + COD1KF + COD1LF + COD1CF + COD1DF + COD1EF + COD1FF   
           + CODRCF + CODRDF + CODREF + CODRFF + COD1CG + COD1DG + COD1EG + COD1FG    
           + CODRCG + CODRDG + CODRGG + CODRFG + PRBR1 + PRBR2 + PRBR3 + PRBR4     
           + CARPEP1 + CARPEP2 + CARPEP3 + CARPEP4 + PALI1 + PALI2 + PALI3 + PALI4     
           + PENSALP1 + PENSALP2 + PENSALP3 + PENSALP4 + PENIN1 + PENIN2 + PENIN3 + PENIN4    
           + CODRCZ + CODRDZ + CODREZ + CODRFZ + COD1CL + COD1DL + COD1EL + COD1FL    
           + CODRCL + CODRDL + CODREL + CODRFL + COD1CM + COD1DM + COD1EM + COD1FM    
           + CODRCM + CODRDM + CODREM + CODRFM + COD1IG + COD1JG + COD1KG + COD1LG 
	   + max(0,COD1IH - LIM5000)+ max(0,COD1JH - LIM5000)+ max(0,COD1KH - LIM5000)+ max(0,COD1LH - LIM5000);


BCOS = min(RDSYVO+0,arr(TX_BASECOTSYN/100*SOMBCOSV*IND_10V))
      +min(RDSYCJ+0,arr(TX_BASECOTSYN/100*SOMBCOSC*IND_10C))                             
      +min(RDSYPP+0,arr(TX_BASECOTSYN/100*SOMBCOSP*IND_10P));

ASYND = BCOS * (1-V_CNR) ;


CISYND = arr(TX_REDCOTSYN/100 * BCOS) * (1 - V_CNR) ;

regle 301260:
application : iliad ;


IAVF = IRE - EPAV + CICORSE + CIRCMAVFT ;


DIAVF2 = (IPRECH + IPCHER + RCMAVFT ) * (1 - positif(RE168+TAX1649)) + CIRCMAVFT * positif(RE168+TAX1649);


IAVF2 = (IPRECH + CIRECH + CIRCMAVFT + 0) * (1 - positif(RE168 + TAX1649))
        + CIRCMAVFT * positif(RE168 + TAX1649) ;

IAVFGP = IAVF2 + CREFAM ;

regle 301270:
application : iliad ;


I2DH = EPAV ;

regle 301280:
application : iliad ;


BTANTGECUM   = (V_BTGECUM * (1 - positif(present(COD7ZZ)+present(COD7ZY)+present(COD7ZX)+present(COD7ZW))) + COD7ZZ+COD7ZY+COD7ZX+COD7ZW);

BTANTGECUMWL =   V_BTPRT6 * (1 - present(COD7WK)) + COD7WK 
               + V_BTPRT5 * (1 - present(COD7WQ)) + COD7WQ 
	       + V_BTPRT4 * (1- present (COD7WH)) + COD7WH
	       + V_BTPRT3 * (1- present (COD7WS)) + COD7WS
	       + V_BTPRT2 * (1- present (COD7XZ)) + COD7XZ
	       + V_BTPRT1 * (1- present (COD7XR)) + COD7XR
	       ;

P2GE = max( (   PLAF_GE2 * (1 + BOOL_0AM)
             + PLAF_GE2_PACQAR * (V_0CH + V_0DP)
             + PLAF_GE2_PAC * (V_0CR + V_0CF + V_0DJ + V_0DN)  
              ) - BTANTGECUM
             , 0
             ) ;
BGEPAHA = min(RDEQPAHA +COD7WI, P2GE) * (1 - V_CNR);

P2GEWL = max(0,PLAF20000 - BTANTGECUMWL);
BGTECH = min(RDTECH , P2GEWL) * (1 - V_CNR) ;

BGEDECL = RDTECH + RDEQPAHA + COD7WI;
TOTBGE = BGTECH + BGEPAHA ;

RGEPAHA =  (BGEPAHA * TX25 / 100 );
RGTECH = (BGTECH * TX40 / 100); 
CIGE = arr (RGTECH + RGEPAHA ) * (1 - positif(RE168 + TAX1649));


DEPENDPDC = BGEPAHA ;


DEPENPPRT = BGTECH ;

GECUM = min(P2GE,BGEPAHA)+(V_BTGECUM * (1 - positif(present(COD7ZY)+present(COD7ZX)+present(COD7ZW) + present(COD7ZZ))) + COD7ZZ + COD7ZW +COD7ZX + COD7ZY);
GECUMWL = max(0,BGTECH + BTANTGECUMWL) ;

BADCRE = min(max(0,CREAIDE-COD7DR) , min((LIM_AIDOMI * (1 - positif(PREMAIDE)) + LIM_PREMAIDE * positif(PREMAIDE)
                            + MAJSALDOM * (positif_ou_nul(ANNEEREV-V_0DA-65) + positif_ou_nul(ANNEEREV-V_0DB-65) * BOOL_0AM
                                           + V_0CF + V_0DJ + V_0DN + (V_0CH + V_0DP)/2+ASCAPA)
                           ) , LIM_AIDOMI3 * (1 - positif(PREMAIDE)) + LIM_PREMAIDE2 * positif(PREMAIDE) ) * (1-positif(INAIDE + 0))
                               +  LIM_AIDOMI2 * positif(INAIDE + 0)) ;

DAIDC = max(0 , CREAIDE - COD7DR) ;
AAIDC = BADCRE * (1 - V_CNR) ;
CIADCRE = max(0 , arr(BADCRE * TX_AIDOMI /100)) * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;
CIADCREB3 = COD7HB ;

regle 301302:
application : iliad ;




DDEVDURN = COD7AL + COD7AN + COD7CK + COD7AQ + COD7DA + COD7BR + COD7DD + COD7BV + COD7DF + COD7BX + COD7BY + COD7EH + COD7EM + COD7EQ + COD7DH + COD7AW              + COD7ER + COD7DK + COD7EU + COD7DM + COD7EV + COD7DN + COD7EW + COD7DU + COD7EX + COD7DV + COD7FB + COD7GK + COD7FC + COD7GP + COD7FD + COD7GT              + COD7FE + COD7GV + COD7GH ; 


ICITE4 = arr(V_BTDEV5 *(1-present(COD7XG)) + COD7XG) *  TX30/100 ;


ICITE3 = arr(V_BTDEV4 * (1-present(COD7XF)) + COD7XF) * TX30/100 ;


ICITECUM = ICITE4 * (1-present(COD7VD)) + ICITE3 * (1-present(COD7VE)) + V_BTCITE3 * (1-present(COD7VF)) + V_BTCITE2 * (1-present(COD7VG))                            + V_BTCITE1 * (1-present(COD7VU)) + V_BTCITDEP * (1-present(COD7VP)) ;


RFRDEVDURN = max(V_BTRFRN2 * (1-present(RFRN2)) * (1-present(COD8XZ)) * (1-present(V_BTCITRFR)) + RFRN2 * (1-positif(COD8XZ + V_BTCITRFR + 0))                          + V_BTCITRFR * (1-positif(COD8XZ)) + COD8XZ * positif(COD8XZ + RFRN2 + V_BTCITRFR) ,
             V_BTRFRN1 * (1-present(RFRN1)) * (1-present(COD8YZ)) + RFRN1 * (1-positif(COD8YZ + 0)) + COD8YZ * positif(COD8YZ + RFRN1)) ;

VARIDF = V_INDIDF * positif(14 - V_NOTRAIT) + (V_INDIDF * (1 - CODIDF) + (1 - V_INDIDF) * CODIDF) * (1 - positif(14 - V_NOTRAIT)) ;

RFRMIN = VARIDF * (null(1-NBPERS) * LIM_IDF1
       + null(2-NBPERS) * LIM_IDF2
       + null(3-NBPERS) * LIM_IDF3
       + null(4-NBPERS) * LIM_IDF4
       + null(5-NBPERS) * LIM_IDF5 
       + positif(NBPERS-5) * (LIM_IDF5 + (LIM_IDFSUP * (NBPERS-5))))
       + (1-VARIDF) * (null(1-NBPERS) * LIM_NONIDF1
       + null(2-NBPERS) * LIM_NONIDF2
       + null(3-NBPERS) * LIM_NONIDF3
       + null(4-NBPERS) * LIM_NONIDF4
       + null(5-NBPERS) * LIM_NONIDF5
       + positif(NBPERS-5) * (LIM_NONIDF5 + (LIM_NONIDFSUPN * (NBPERS-5)))) ;


RFRMAX = null(1-NBPT) * LIM_RFRMAX1
       + null(1.25-NBPT) * LIM_RFRMAX125
       + null(1.5-NBPT) * LIM_RFRMAX15
       + null(1.75-NBPT) * LIM_RFRMAX175
       + null(2-NBPT) * LIM_RFRMAX2
       + null(2.25-NBPT) * LIM_RFRMAX225
       + null(2.5-NBPT) * LIM_RFRMAX25
       + null(2.75-NBPT) * LIM_RFRMAX275
       + null(3-NBPT) * LIM_RFRMAX3
       + positif(NBPT-3) *(LIM_RFRMAX3 + LIM_DEVSUP1 * ((NBPT-3)*4)) ; 
       

CONDRFR2 = V_BTRFRN2 * (1-present(RFRN2)) * (1-present(COD8XZ)) * (1-present(V_BTCITRFR)) + RFRN2 * (1-present(COD8XZ)) * (1-present(V_BTCITRFR))                     + V_BTCITRFR * (1-positif(COD8XZ)) + COD8XZ * positif(present(COD8XZ) + present(RFRN2) + present(V_BTCITRFR)) ;

CONDRFR1 = V_BTRFRN1 * (1-present(RFRN1)) * (1-present(COD8YZ)) + RFRN1 * (1-present(COD8YZ )) + COD8YZ * positif(present(COD8YZ) + present(RFRN1)) ;

CONDINDR = (positif(positif(RFRMIN - CONDRFR2) * positif(RFRMIN - CONDRFR1) * positif(CONDRFR2 * CONDRFR1) + positif(RFRMIN - CONDRFR2)* (1-positif(CONDRFR1)         ) + positif(RFRMIN - CONDRFR1)* (1-positif(CONDRFR2)))) 
         + (positif(positif_ou_nul(CONDRFR2-RFRMIN) * positif(RFRMAX-CONDRFR2)  + positif_ou_nul(CONDRFR1 -RFRMIN) * positif(RFRMAX-CONDRFR1))) ;


INDRESN = positif(positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ) + present(V_BTCITRFR)) * positif(present(V_BTRFRN1)                                  + present(RFRN1) + present(COD8YZ))
        + positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ) + present(V_BTCITRFR)) + positif(present(V_BTRFRN1) + present(RFRN1)                         + present(COD8YZ))) * 
	(
        positif_ou_nul(RFRMAX - RFRMIN) * (
        positif(positif(RFRMIN - CONDRFR2) * positif(RFRMIN - CONDRFR1) * positif(CONDRFR2 * CONDRFR1) + positif(RFRMIN - CONDRFR2) * (1-positif(CONDRFR1))        + positif(RFRMIN - CONDRFR1) * (1-positif(CONDRFR2))) * 1
      + positif(positif_ou_nul(CONDRFR2 - RFRMIN) * positif(RFRMAX - CONDRFR2)  + positif_ou_nul(CONDRFR1 - RFRMIN) * positif(RFRMAX - CONDRFR1)) * 2  
      + positif(positif_ou_nul(CONDRFR2 - RFRMAX) + positif_ou_nul(CONDRFR1 - RFRMAX)) * positif(COD7AN + COD7CK + COD7AQ + COD7DA)* (1-positif(CONDINDR))* 3
                                          )
     + positif(RFRMIN - RFRMAX) * (
       positif(positif(RFRMAX - CONDRFR2)* positif(RFRMAX - CONDRFR1)) * 1
     + positif(positif(CONDRFR2 - RFRMAX) + positif(CONDRFR1 - RFRMAX)) * positif(COD7AN + COD7CK + COD7AQ + COD7DA) * 3
                                  )
       ) ;

regle 301303:
application : iliad ;



CI7AL = null(2-INDRESN) * arr(min(COD7AK * NOMBRE40 , COD7AL * NOMBRE75/100)) ;

CI7AN = null(2-INDRESN) * arr(min(COD7AM * NOMBRE15 , COD7AN * NOMBRE75/100)) 
      + null(3-INDRESN) * arr(min(COD7AM * NOMBRE10 , COD7AN * NOMBRE75/100)) ;

CI7CK = null(2-INDRESN) * arr(min(COD7CC * NOMBRE15 * min(1,(COD7CG/100)) , COD7CK * NOMBRE75/100))
      + null(3-INDRESN) * arr(min(COD7CC * NOMBRE10 * min(1,(COD7CG/100)) , COD7CK * NOMBRE75/100)) ;

CI7AQ = null(2-INDRESN) * arr(min(COD7AO * NOMBRE50 , COD7AQ * NOMBRE75/100)) 
      + null(3-INDRESN) * arr(min(COD7AO * NOMBRE25 , COD7AQ * NOMBRE75/100)) ;

CI7DA = null(2-INDRESN) * arr(min(COD7CN * NOMBRE50 * min(1,(COD7CU/100)) , COD7DA * NOMBRE75/100))
      + null(3-INDRESN) * arr(min(COD7CN * NOMBRE25 * min(1,(COD7CU/100)) , COD7DA * NOMBRE75/100)) ;

CI7BR = null(2-INDRESN) * arr(min(COD7BR * NOMBRE75/100 , LIM4000)) ; 

CI7DD = null(2-INDRESN) * arr(min(COD7DD * NOMBRE75/100 , LIM1000)) ; 

CI7BV = null(2-INDRESN) * arr(min(COD7BV * NOMBRE75/100 , LIM3000)) ; 

CI7DF = null(2-INDRESN) * arr(min(COD7DF * NOMBRE75/100 , LIM1000)) ; 

CI7BX = null(2-INDRESN) * arr(min(COD7BX * NOMBRE75/100 , LIM3000)) ; 
      
CI7BY = null(2-INDRESN) * arr(min(COD7BY * NOMBRE75/100 , LIM2000)) ; 

CI7EH = null(2-INDRESN) * arr(min(COD7EH * NOMBRE75/100 , LIM1500)) ; 

CI7EM = null(2-INDRESN) * arr(min(COD7EM * NOMBRE75/100 , LIM1000)) ; 

CI7EQ = null(2-INDRESN) * arr(min(COD7EQ * NOMBRE75/100 , LIM1000)) ; 

CI7DH = null(2-INDRESN) * arr(min(COD7DH * NOMBRE75/100 , LIM350)) ; 

CI7AW = null(2-INDRESN) * arr(min(COD7AW * NOMBRE75/100 , LIM600)) ; 

CI7ER = null(2-INDRESN) * arr(min(COD7ER * NOMBRE75/100 , LIM4000)) ; 

CI7DK = null(2-INDRESN) * arr(min(COD7DK * NOMBRE75/100 , LIM1000)) ; 

CI7EU = null(2-INDRESN) * arr(min(COD7EU * NOMBRE75/100 , LIM2000)) ; 

CI7DM = null(2-INDRESN) * arr(min(COD7DM * NOMBRE75/100 , LIM1000)) ; 
     
CI7EV = null(2-INDRESN) * arr(min(COD7EV * NOMBRE75/100 , LIM400)) ; 

CI7DN = null(2-INDRESN) * arr(min(COD7DN * NOMBRE75/100 , LIM150)) ; 

CI7EW = null(2-INDRESN) * arr(min(COD7EW * NOMBRE75/100 , LIM400)) ; 

CI7DU = null(2-INDRESN) * arr(min(COD7DU * NOMBRE75/100 , LIM150)) ; 

CI7EX = arr(min(COD7EX * NOMBRE75/100 , LIM300)) ;      

CI7DV = arr(min(COD7DV * NOMBRE75/100 , LIM300)) ;

CI7FB = null(2-INDRESN) * arr(min(COD7FA * NOMBRE15 , COD7FB * NOMBRE75/100)) ; 

CI7GK = null(2-INDRESN) * arr(min(COD7DX * NOMBRE15 * min(1,(COD7GI/100)), COD7GK * NOMBRE75/100)) ; 

CI7FC = null(2-INDRESN) * arr(min(COD7FC * NOMBRE75/100 , LIM300)) ; 

CI7GP = null(2-INDRESN) * arr(min(COD7GP * NOMBRE75/100 , LIM150)) ; 

CI7FD = null(2-INDRESN) * arr(min(COD7FD * NOMBRE75/100 , LIM400)) ; 

CI7GT = null(2-INDRESN) * arr(min(COD7GT * NOMBRE75/100 , LIM150)) ; 

CI7FE = null(2-INDRESN) * arr(min(COD7FE * NOMBRE75/100 , LIM2000)) ; 

CI7GV = null(2-INDRESN) * arr(min(COD7GV * NOMBRE75/100 , LIM1000)) ; 

CI7GH = null(2-INDRESN) * arr(min(COD7FZ * LIM150 , COD7GH * NOMBRE75/100)) ; 


PDEVDURN = max((( PLAF_DEVDURN * (1 + BOOL_0AM)
         + PLAF_GE2_PACQARN * (V_0CH+V_0DP)
         + PLAF_GE2_PACN * (V_0CR+V_0CF+V_0DJ+V_0DN))
         - (ICITECUM + COD7VD + COD7VE + COD7VF + COD7VG + COD7VU + COD7VP)), 0) ;
	

CIDEVDURN = arr(min( CI7AL + CI7AN + CI7CK + CI7AQ + CI7DA + CI7BR + CI7DD + CI7BV + CI7DF + CI7BX + CI7BY + CI7EH + CI7EM + CI7EQ + CI7DH + CI7AW + CI7ER             + CI7DK + CI7EU + CI7DM + CI7EV + CI7DN + CI7EW + CI7DU + CI7EX + CI7DV + CI7FB + CI7GK + CI7FC + CI7GP + CI7FD + CI7GT + CI7FE + CI7GV + CI7GH ,              PDEVDURN )) * (1-V_CNR) ;   

regle 301304:
application : iliad ;






CIDEVRET21 = COD7VP + COD7VU + COD7VG + COD7VF + COD7VE + COD7VD ;

 
DDEVDUR = DDEVDURN ; 



CIDEVDUR =  arr(CIDEVDURN * (1-present(CODCIT)) + CODCIT) * (1-V_CNR) ;
regle 301310:
application : iliad ;

DTEC = RISKTEC;
ATEC = positif(DTEC) * DTEC;
CITEC = arr (ATEC * TX40/100);

regle 301320:
application : iliad ;

DPRETUD = PRETUD + PRETUDANT ;
APRETUD = max(min(PRETUD,LIM_PRETUD) + min(PRETUDANT,LIM_PRETUD*CASEPRETUD),0) * (1-V_CNR) ;

CIPRETUD = arr(APRETUD*TX_PRETUD/100) * (1 - positif(RE168 + TAX1649)) * (1-V_CNR) ;

regle 301325:
application : iliad ;


DPAP = COD7PA ;
APAP = DPAP * (1 - V_CNR) ;

CIPAP = arr(DPAP * TX30/100) * (1 - V_CNR) ;

regle 301330:
application : iliad ;


EM7AVRICI = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - ANNEEREV+1)))
         + somme (j=0..5: min (1 , max(0 , V_0Nj + AG_LIMFG - ANNEEREV+1)))
      + (1 - positif(somme(i=0..7:V_0Fi) + somme(i=0..5: V_0Ni) + 0)) * (V_0CF + V_0DN) ;

EM7QARAVRICI = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - ANNEEREV+1)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - ANNEEREV+1)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

EM7 = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - ANNEEREV)))
         + somme (j=0..5: min (1 , max(0 , V_0Nj + AG_LIMFG - ANNEEREV)))
      + (1 - positif(somme(i=0..7:V_0Fi) + somme(i=0..5: V_0Ni) + 0)) * (V_0CF + V_0DN) ;

EM7QAR = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - ANNEEREV)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - ANNEEREV)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

BRFG = min(RDGARD1,PLAF_REDGARD) + min(RDGARD2,PLAF_REDGARD)
       + min(RDGARD3,PLAF_REDGARD) + min(RDGARD4,PLAF_REDGARD)
       + min(RDGARD1QAR,PLAF_REDGARDQAR) + min(RDGARD2QAR,PLAF_REDGARDQAR)
       + min(RDGARD3QAR,PLAF_REDGARDQAR) + min(RDGARD4QAR,PLAF_REDGARDQAR)
       ;
RFG1 = arr ( (BRFG) * TX_REDGARD /100 ) * (1 -V_CNR);
DGARD = somme(i=1..4:RDGARDi)+somme(i=1..4:RDGARDiQAR);
AGARD = (BRFG) * (1-V_CNR) ;
CIGARD = RFG1 * (1 - positif(RE168 + TAX1649)) ;

regle 301340:
application : iliad ;


PREHAB = PREHABT + PREHABT2 + PREHABTN2 + PREHABTVT ;

BCIHP = max(( PLAFHABPRIN * (1 + BOOL_0AM) * (1+positif(V_0AP+V_0AF+V_0CG+V_0CI+V_0CR))
                 + (PLAFHABPRINENF/2) * (V_0CH + V_0DP)
                 + PLAFHABPRINENF * (V_0CR + V_0CF + V_0DJ + V_0DN)
                  )
             ,0);

BCIHABPRIN1 = min(BCIHP , PREHABT) * (1 - V_CNR) ;
BCIHABPRIN5 = min(max(0,BCIHP-BCIHABPRIN1),PREHABT2) * (1 - V_CNR);
BCIHABPRIN6 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN5),PREHABTN2) * (1 - V_CNR);
BCIHABPRIN7 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN5-BCIHABPRIN6),PREHABTVT) * (1 - V_CNR);

BCIHABPRIN = BCIHABPRIN1 + BCIHABPRIN5 + BCIHABPRIN6 + BCIHABPRIN7 ;

CIHABPRIN = arr((BCIHABPRIN1 * TX40/100)
                + (BCIHABPRIN5 * TX20/100)
                + (BCIHABPRIN6 * TX15/100)
                + (BCIHABPRIN7 * TX10/100))
                * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);

regle 301350:
application : iliad ;


BDCIFORET = COD7VM + COD7TA + COD7VN + COD7TB + COD7VH + COD7TV + COD7VI + COD7TW + COD7VJ + COD7TT + COD7VK + COD7TU + COD7VS + COD7TR + COD7VL + COD7TS + COD7TP + COD7TQ + COD7TM + COD7TO
            + REPSINFOR5 + COD7TK +RDFORESTRA + SINISFORET + COD7UA + COD7UB + RDFORESTGES + COD7UI ;

BCIFORETTK = max(0 , min(COD7TK , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)))) * (1-V_CNR) ;
VARTMP1 = BCIFORETTK ;

BCIFORETTJ = max(0 , min(REPSINFOR5 , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTJ ;

BCIFORETTO = max(0 , min(COD7TO , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTO ;

BCIFORETTM = max(0 , min(COD7TM , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTM ;

BCIFORETTQ = max(0 , min(COD7TQ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTQ ;

BCIFORETTP = max(0 , min(COD7TP , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTP ;

BCIFORETVL = max(0 , min(COD7VL , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVL ;

BCIFORETTS = max(0 , min(COD7TS , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTS ;

BCIFORETVS = max(0 , min(COD7VS , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVS ;

BCIFORETTR = max(0 , min(COD7TR , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTR ;

BCIFORETVK = max(0 , min(COD7VK , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVK ;

BCIFORETTU = max(0 , min(COD7TU , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTU ;

BCIFORETVJ = max(0 , min(COD7VJ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVJ ;

BCIFORETTT = max(0 , min(COD7TT , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTT ;

BCIFORETVI = max(0 , min(COD7VI , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVI ;

BCIFORETTW = max(0 , min(COD7TW , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTW ;

BCIFORETVH = max(0 , min(COD7VH , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVH ;

BCIFORETTV = max(0 , min(COD7TV , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTV ;

BCIFORETVN = max(0 , min(COD7VN , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVN ;

BCIFORETTB = max(0 , min(COD7TB , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTB ;

BCIFORETVM = max(0 , min(COD7VM , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVM ;

BCIFORETTA = max(0 , min(COD7TA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTA ;

BCIFORETUA = max(0 , min(COD7UA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUA ;

BCIFORETUB = max(0 , min(COD7UB , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUB ;

BCIFORETUP = max(0 , min(RDFORESTRA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUP ;

BCIFORETUT = max(0 , min(SINISFORET , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = 0 ;

BCIFORETUI = max(0 , min(COD7UI , max(0 , PLAF_FOREST2 * (1 + BOOL_0AM)))) * (1-V_CNR) ;
BCIFORETUQ = max(0 , min(RDFORESTGES , max(0 , PLAF_FOREST2 * (1 + BOOL_0AM)-BCIFORETUI))) * (1-V_CNR) ;

BCIFORET = BCIFORETTK + BCIFORETTJ + BCIFORETTO + BCIFORETTM + BCIFORETTQ + BCIFORETTP + BCIFORETVL + BCIFORETTS + BCIFORETVS + BCIFORETTR + BCIFORETVK + BCIFORETTU + BCIFORETVJ + BCIFORETTT + BCIFORETVI
         + BCIFORETTW + BCIFORETVH + BCIFORETTV + BCIFORETVN + BCIFORETTB + BCIFORETVM + BCIFORETTA + BCIFORETUA + BCIFORETUB + BCIFORETUP + BCIFORETUT + BCIFORETUI + BCIFORETUQ ;

CIFORET = arr((BCIFORETTJ + BCIFORETTM + BCIFORETTP + BCIFORETVS + BCIFORETTR + BCIFORETVJ + BCIFORETTT + BCIFORETVH + BCIFORETTV + BCIFORETVM + BCIFORETTA + BCIFORETUP + BCIFORETUT + BCIFORETUQ
                                                                  ) * TX18/100
        + (BCIFORETTK + BCIFORETTO + BCIFORETTQ + BCIFORETVL + BCIFORETTS + BCIFORETVK + BCIFORETTU + BCIFORETVI + BCIFORETTW + BCIFORETVN + BCIFORETTB + BCIFORETUA + BCIFORETUB + BCIFORETUI
                                                                  ) * TX25/100) ;

regle 301360:
application : iliad ;



REPCIFADSSN7 = max(0 , COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM)) * (1 - V_CNR) ;

REPCIFSN7 = (positif_ou_nul(COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM)) * REPSINFOR5
             + (1 - positif_ou_nul(COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , REPSINFOR5 - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK))) * (1 - V_CNR) ;


REPCIFADSSN6 = (positif_ou_nul(COD7TK + REPSINFOR5 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TO
                + (1 - positif_ou_nul(COD7TK + REPSINFOR5 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TO - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK - REPSINFOR5))) * (1 - V_CNR) ;

REPCIFSN6 = (positif_ou_nul(COD7TK + REPSINFOR5 + COD7TO - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TM
             + (1 - positif_ou_nul(COD7TK + REPSINFOR5 + COD7TO - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TM - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK - REPSINFOR5 + COD7TO))) * (1 - V_CNR) ;

VARTMP1 = COD7TK + REPSINFOR5 + COD7TO + COD7TM ;

REPCIFADSSN5 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TQ
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TQ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TQ ;

REPCIFSN5 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TP
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TP - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TP + COD7VL ;
							   

REPCIFADSSN4 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TS
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TS - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TS +  COD7VS ;

REPCIFSN4 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TR
          + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TR - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TR ;
			     

REPCIFADHSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VK
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VK - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VK ;

REPSN3 = COD7TK + REPSINFOR5 + COD7TO + COD7TM + COD7TQ + COD7TP + COD7VL + COD7TS + COD7VS + COD7TR + COD7UA;

REPCIFADSSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TU
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TU - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TU ;

REPCIFHSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VJ
              + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VJ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VJ ;

REPCIFSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TT
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TT - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TT ;


REPCIFADHSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VI
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VI - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VI ;
							   
REPCIFADSSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TW
               + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TW - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TW ;
							   
REPCIFHSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VH
          + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VH - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VH ;
							   
REPCIFSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TV
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TV - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TV ;



REPCIFADHSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VN
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VN - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VN ;

REPCIFADSSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TB
               + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TW - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TB ;

REPCIFHSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VM
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VM - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VM ;

REPCIFSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TA
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TA ;



REPCIFAD = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7UA
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7UA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7UA ;
							   
REPCIFADSIN = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7UB
               + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7UB - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7UB ;
							   
REPCIF = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * RDFORESTRA
          + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , RDFORESTRA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + RDFORESTRA ;
							   
REPCIFSIN = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * SINISFORET
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , SINISFORET - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = 0 ;


regle 301365:
application : iliad ;

REP7UP = REPCIF + REPCIFHSN1 + REPCIFHSN2 + REPCIFHSN3 + REPCIFHSN4;
REP7UA = REPCIFAD + REPCIFADHSN1 + REPCIFADHSN2 + REPCIFADHSN3 + REPCIFADHSN4;
REP7UT = REPCIFSIN + REPCIFSN1 + REPCIFSN2 + REPCIFSN3 + REPCIFSN4 + REPCIFSN5 + REPCIFSN6 + REPCIFSN7;
REP7UB = REPCIFADSIN + REPCIFADSSN1 + REPCIFADSSN2 + REPCIFADSSN3 + REPCIFADSSN4 + REPCIFADSSN5 +REPCIFADSSN6 + REPCIFADSSN7; 

regle 301370:
application : iliad ;


CICSG = min(CSGC , arr(IPPNCS * T_CSGCRDS/100)) ;

CIRDS = min(RDSC , arr((min(REVCSXA , SALECS) + min(REVCSXB , SALECSG + COD8SC)
                        + min(REVCSXC , ALLECS) + min(REVCSXD , INDECS + COD8SW)
                        + min(REVCSXE , PENECS + COD8SX) + min(COD8XI , COD8SA)
                        + min(COD8XJ , COD8SB) + min (COD8XM , GLDGRATV + GLDGRATC)
			+ min(COD8XO , COD8TH) + min (COD8XN , COD8SD)
                       ) * T_RDS/100)) ;

CIPSOL = min(MPSOL , arr(IPPNCS * TXPSOL/100)) ;

CICVN = min( CVNSALC , arr( min(BCVNSAL,COD8XL) * TX10/100 )) ;

CIGLOA = min( CGLOA , arr ( min(GLDGRATV+GLDGRATC,COD8XM) * T_CSG/100));


CIRSE1 = min( RSE1 , arr( min(SALECS,REVCSXA) * TXTQ/100 ));

RSE8TV = arr(BRSE8TV * TXTV/100) * (1 - positif(ANNUL2042));
RSE8SA = arr(BRSE8SA * TXTV/100) * (1 - positif(ANNUL2042));
CIRSE8TV = min( RSE8TV , arr( min(ALLECS,REVCSXC) * TXTV/100 )) ;
CIRSE8SA = min( RSE8SA , arr(min(COD8SA,COD8XI) * TXTV/100 )) ;
CIRSE2 = min(RSE2, arr(min(ALLECS,REVCSXC)* TXTV/100 + min(COD8SA,COD8XI) * TXTV/100));

CIRSE3 = min( RSE3 , arr( min(COD8SW+INDECS,REVCSXD * TXTW/100 )));

RSE8TX = arr(BRSE8TX * TXTX/100) * (1 - positif(ANNUL2042));
RSE8SB = arr(BRSE8SB * TXTX/100) * (1 - positif(ANNUL2042));
CIRSE8TX = min( RSE8TX , arr( REVCSXE * TXTX/100 )) ;
CIRSE8SB = min( RSE8SB , arr( COD8XJ * TXTX/100 ));
CIRSE4 =  min(RSE4, arr(min(PENECS+COD8SX,REVCSXE)* TXTX/100 + min(COD8XJ,COD8SB) * TXTX/100));

CIRSE5 = min( RSE5 , arr( min(SALECSG+COD8SC,REVCSXB) * TXTR/100 ));

CIRSE6 = min( RSE6 , arr(( min( REVCSXB , SALECSG+COD8SC ) +
                           min( REVCSXC , ALLECS ) +
                           min( COD8XI , COD8SA ) +
			   min( COD8XN, COD8SD )  +
                           min( COD8XO, COD8TH ) 
                         ) * TXCASA/100 ));
			 
CIRSE8 =  arr((min(COD8XN ,COD8SD) + min(COD8XO , COD8TH)) * TX066/100) ;

CIRSETOT = CIRSE1 + CIRSE2 + CIRSE3 + CIRSE4 + CIRSE5 + CIRSE8 ;

regle 301380:
application : iliad ;

CRESINTER = PRESINTER ;

regle 301385:
application : iliad ;



BDCIVHELEC = COD7ZQ + COD7ZR + COD7ZS + COD7ZT + COD7ZU + COD7ZV ; 


BCIVHELEC = BDCIVHELEC * (1-V_CNR) ; 



CI7ZQ = min((COD7ZQ * NOMBRE75/100),LIM300) ;
CI7ZR = min((COD7ZR * NOMBRE75/100),LIM300) ;
CI7ZS = min((COD7ZS * NOMBRE75/100),LIM300) ;
CI7ZT = min((COD7ZT * NOMBRE75/100),LIM300) ;
CI7ZU = min((COD7ZU * NOMBRE75/100),LIM300) ;
CI7ZV = min((COD7ZV * NOMBRE75/100),LIM300) ;


CIVHELEC = arr(CI7ZQ + CI7ZR + CI7ZS + CI7ZT + CI7ZU + CI7ZV)  * (1-V_CNR);

regle 301390:
application : iliad ;
REST = positif(IRE) * positif(CRESTACID) ;
VARTMP1 = 0 ;

8LAREST = positif(REST) * max(0 , min(LOY8LA , CRESTACID - VARTMP1)) ;
8LAIMP = positif_ou_nul(8LAREST) * (LOY8LA - 8LAREST) ;
VARTMP1 = VARTMP1 + LOY8LA ;

7LSREST = positif(REST) * max(0 , min(LOY7LS , CRESTACID - VARTMP1)) ;
7LSIMP = positif_ou_nul(7LSREST) * (LOY7LS - 7LSREST) ;
VARTMP1 = VARTMP1 + LOY7LS ;

LIBREST = positif(REST) * max(0 , min(AUTOVERSLIB , CRESTACID - VARTMP1)) ;
LIBIMP = positif_ou_nul(LIBREST) * (AUTOVERSLIB - LIBREST) ;
VARTMP1 = VARTMP1 + AUTOVERSLIB ;

8TEREST = positif(REST) * max(0 , min(COD8TE , CRESTACID - VARTMP1)) ;
8TEIMP = positif_ou_nul(8TEREST) * (COD8TE - 8TEREST) ;
VARTMP1 = VARTMP1 + COD8TE ;

AGRREST = positif(REST) * max(0 , min(CICONGAGRI , CRESTACID - VARTMP1)) ;
AGRIMP = positif_ou_nul(AGRREST) * (CICONGAGRI - AGRREST) ;
VARTMP1 = VARTMP1 + CICONGAGRI ;

ARTREST = positif(REST) * max(0 , min(CREARTS , CRESTACID - VARTMP1)) ;
ARTIMP = positif_ou_nul(ARTREST) * (CREARTS - ARTREST) ;
VARTMP1 = VARTMP1 + CREARTS ;

FORREST = positif(REST) * max(0 , min(CREFORMCHENT , CRESTACID - VARTMP1)) ;
FORIMP = positif_ou_nul(FORREST) * (CREFORMCHENT - FORREST) ;
VARTMP1 = VARTMP1 + CREFORMCHENT ;

PSIREST = positif(REST) * max(0 , min(CRESINTER , CRESTACID - VARTMP1)) ;
PSIIMP = positif_ou_nul(PSIREST) * (CRESINTER - PSIREST) ;
VARTMP1 = VARTMP1 + CRESINTER ;

HVEREST = positif(REST) * max(0 , min(CREAGRIHVE , CRESTACID - VARTMP1)) ;
HVEIMP = positif_ou_nul(HVEREST) * (CREAGRIHVE - HVEREST) ;
VARTMP1 = VARTMP1 + CREAGRIHVE ;

GLYREST = positif(REST) * max(0 , min(CREAGRIGLY , CRESTACID - VARTMP1)) ;
GLYIMP = positif_ou_nul(GLYREST) * (CREAGRIGLY - GLYREST) ;
VARTMP1 = VARTMP1 + CREAGRIGLY ;

BIOREST = positif(REST) * max(0 , min(CREAGRIBIO , CRESTACID - VARTMP1)) ;
BIOIMP = positif_ou_nul(BIOREST) * (CREAGRIBIO - BIOREST) ;
VARTMP1 = VARTMP1 + CREAGRIBIO ;

FAMREST = positif(REST) * max(0 , min(CREFAM , CRESTACID - VARTMP1)) ;
FAMIMP = positif_ou_nul(FAMREST) * (CREFAM - FAMREST) ;
VARTMP1 = VARTMP1 + CREFAM ;

PAPREST = positif(REST) * max(0 , min(CIPAP , CRESTACID - VARTMP1)) ;
PAPIMP = positif_ou_nul(PAPREST) * (CIPAP - PAPREST) ;
VARTMP1 = VARTMP1 + CIPAP ;

HABREST = positif(REST) * max(0 , min(CIHABPRIN , CRESTACID - VARTMP1)) ;
HABIMP = positif_ou_nul(HABREST) * (CIHABPRIN - HABREST) ;
VARTMP1 = VARTMP1 + CIHABPRIN ;

ROFREST = positif(REST) * max(0 , min(CIFORET , CRESTACID - VARTMP1)) ;
ROFIMP = positif_ou_nul(ROFREST) * (CIFORET - ROFREST) ;
VARTMP1 = VARTMP1 + CIFORET ;

SALREST = positif(REST) * max(0 , min(CIADCRE , CRESTACID - VARTMP1)) ;
SALIMP = positif_ou_nul(SALREST) * (CIADCRE - SALREST) ;
VARTMP1 = VARTMP1 + CIADCRE ;

PREREST = positif(REST) * max(0 , min(CIPRETUD , CRESTACID - VARTMP1)) ;
PREIMP = positif_ou_nul(PREREST) * (CIPRETUD - PREREST) ;
VARTMP1 = VARTMP1 + CIPRETUD ;

SYNREST = positif(REST) * max(0 , min(CISYND , CRESTACID - VARTMP1)) ;
SYNIMP = positif_ou_nul(SYNREST) * (CISYND - SYNREST) ;
VARTMP1 = VARTMP1 + CISYND ;

GARREST = positif(REST) * max(0 , min(CIGARD , CRESTACID - VARTMP1)) ;
GARIMP = positif_ou_nul(GARREST) * (CIGARD - GARREST) ;
VARTMP1 = VARTMP1 + CIGARD ;

BAIREST = positif(REST) * max(0 , min(CICA , CRESTACID - VARTMP1)) ;
BAIIMP = positif_ou_nul(BAIREST) * (CICA - BAIREST) ;
VARTMP1 = VARTMP1 + CICA ;

VEHREST = positif(REST) * max(0 , min(CIVHELEC , CRESTACID - VARTMP1)) ;
VEHIMP = positif_ou_nul(CIVHELEC) * (CIVHELEC - VEHREST) ;
VARTMP1 = VARTMP1 + CIVHELEC ;

TECREST = positif(REST) * max(0 , min(CITEC , CRESTACID - VARTMP1)) ;
TECIMP = positif_ou_nul(TECREST) * (CITEC - TECREST) ;
VARTMP1 = VARTMP1 + CITEC ;

DEPREST = positif(REST) * max(0 , min(CIDEVDUR , CRESTACID - VARTMP1)) ;
DEPIMP = positif_ou_nul(DEPREST) * (CIDEVDUR - DEPREST) ;
VARTMP1 = VARTMP1 + CIDEVDUR ;

AIDREST = positif(REST) * max(0 , min(CIGE , CRESTACID - VARTMP1)) ;
AIDIMP = positif_ou_nul(AIDREST) * (CIGE - AIDREST) ;
VARTMP1 = VARTMP1 + CIGE ;

HJAREST = positif(REST) * max(0 , min(CIHJA , CRESTACID - VARTMP1)) ;
HJAIMP = positif_ou_nul(HJAREST) * (CIHJA - HJAREST) ;
VARTMP1 = VARTMP1 + CIHJA ;

CORREST = positif(REST) * max(0 , min(CICORSENOW , CRESTACID - VARTMP1)) ;
CORIMP = positif_ou_nul(CORREST) * (CICORSENOW - CORREST) ;
VARTMP1 = VARTMP1 + CICORSENOW ;

EMPREST = positif(REST) * max(0 , min(COD8TL , CRESTACID - VARTMP1)) ;
EMPIMP = positif_ou_nul(EMPREST) * (COD8TL - EMPREST) ;
VARTMP1 = VARTMP1 + COD8TL ;

RECREST = positif(REST) * max(0 , min(IPRECH , CRESTACID - VARTMP1)) ;
RECIMP = positif_ou_nul(RECREST) * (IPRECH - RECREST) ;
VARTMP1 = VARTMP1 + IPRECH ;

ASSREST = positif(REST) * max(0 , min(I2DH , CRESTACID - VARTMP1)) ;
ASSIMP = positif_ou_nul(ASSREST) * (I2DH - ASSREST) ;
VARTMP1 = VARTMP1 + I2DH ;

2CKREST = positif(REST) * max(0 , min(CI2CK , CRESTACID - VARTMP1)) ;
2CKIMP = positif_ou_nul(2CKREST) * (CI2CK - 2CKREST) ;
VARTMP1 = 0 ;


