#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2019]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2019 
#au titre des revenus perçus en 2018. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

# =================================================================================
# Chapitre 83 : Revenu catégoriels nets : Revenus fonciers
# =================================================================================

regle 831000:
application : iliad ;


RRFI = positif(RFON + DRCF + RFMIC - MICFR -max(0,RFDANT - DEFRFNONI)*(1-positif(RFON))) * (RFON + DRCF + max(0,RFMIC - MICFR -max(0,RFDANT  - DEFRFNONI)*(1-positif(RFON))))
     + (1-positif(RFON + DRCF + RFMIC - MICFR -max(0,RFDANT - DEFRFNONI)*(1-positif(RFON)))) * (positif(RFDANT-DEFRFNONI)*(1-positif(RFON)) * DEFRFNONI+DRCF+RFON) ;
RRFIPS = RRFI ; 

regle 831010:
application : iliad ;


MICFR=arr(RFMIC*TX_MICFON/100);

regle 831020:
application : iliad ;


RMF = RFMIC - MICFR ;


RMFN = max(0 , RMF - RFDANT * (1-positif(DEFRFNONI)) - min(RFDANT , max(0 , RFDANT - DEFRFNONI)) * positif(DEFRFNONI)) ;

PASRFASS = positif(RMF - RFDANT) * (RMFN - (COD4BK - arr(COD4BK * TX_MICFON/100) - arr(COD4BK * RFDANT/RFMIC)) * positif(RMFN)) ;

regle 831030:
application : iliad ;


RFCD = RFORDI + FONCI + REAMOR ;

regle 831040:
application : iliad ;


RFCE = max(0,RFCD- RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));
RFCEAPS = max(0,RFORDI- RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));
RFCEPS = max(0,RFCD-RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

DFCE = min(0,RFCD- RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

RFCF = max(0,RFCE-RFDHIS);
RFCFPS = (RFCEPS-RFDHIS);
RFCFAPS = max(0,RFCEAPS-RFDHIS);

DRCF  = min(0,RFCE-RFDHIS);

RFCG = max(0,RFCF- RFDANT* (1-positif(DEFRFNONI))
                   - min(RFDANT, max(0,RFDANT-DEFRFNONI)) * positif(DEFRFNONI));
DFCG = min(0,RFCF- RFDANT* (1-positif(DEFRFNONI))
                   - min(RFDANT, max(0,RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

regle 831050:
application : iliad ;

RFON = arr(RFCG*RFORDI/RFCD);

2REVF = min( arr ((RFCG)*(FONCI)/RFCD) , RFCG-RFON) ;
3REVF = min( arr (RFCG*(REAMOR)/RFCD) , RFCG-RFON-2REVF) ;
RFQ = FONCI + REAMOR ;
regle 831055:
application : iliad ;

RFDANT4BA = max(0,RFORDI - RFON);
RFDANTRBA = max(0,FONCI - 2REVF);
RFDANTSBA = max(0,REAMOR - 3REVF);

PASRF = (1 - positif(-DFCG)) * max(0 , present(RFORDI) * (RFON  - (COD4BL - arr(COD4BL * RFDANT4BA/RFORDI)))) ;

regle 831060:
application : iliad ;

 
DEF4BB = min(RFDORD,RFORDI + RFMIC * 0.70 + FONCI + REAMOR) ;
DEFRF4BB = min(RFDORD,max(DEF4BB1731,max(DEF4BB_P,DEF4BBP2))) * positif(SOMMERF_2) * (1-PREM8_11) ;

regle 831070:
application : iliad ;
 
 
DEF4BD = min(RFDANT,RFORDI + RFMIC * 0.70 + FONCI + REAMOR-RFDORD - RFDHIS) ;
DEFRF4BD = min(RFDANT,max(DEF4BD1731,max(DEF4BD_P,DEF4BDP2)))* positif(SOMMERF_2) * (1-PREM8_11) ;

regle 831080:
application : iliad ;

DEF4BC = max(0, RFORDI + RFMIC * 0.70 + FONCI + REAMOR - RFDORD) ;
DEFRF4BC = max(0,RFDHIS-max(DEF4BC1731,max(DEF4BC_P,DEF4BCP2))) * positif(DFANTIMPU)*(1-positif(PREM8_11))
          + RFDHIS * positif(DFANTIMPU)*positif(PREM8_11);
regle 834210:
application : iliad ;

RFREVENU = (RFORDI + RFMIC * 0.70 + FONCI + REAMOR);
regle 834215:
application : iliad ;
DEFRFNONIBIS =  min(RFDORD,RFORDI + RFMIC * 0.70 + FONCI + REAMOR) + max(0,min(RFDANT,RFORDI + RFMIC * 0.70 + FONCI + REAMOR-RFDORD - RFDHIS));
regle 831090:
application : iliad ;
DEFRFNONI1 =  RFDORD + RFDANT - DEFRFNONIBIS;
DEFRFNONI2 = positif(SOMMERF_2) * null(PREM8_11) *
                  (max(0,RFDORD + RFDANT - max(DEFRFNONI1731,max(DEFRFNONI_P,DEFRFNONIP2))
                                         - max(0,RFREVENU - RFREVENUP3)))
            + PREM8_11 * positif(RFORDI + RFMIC * 0.70 + FONCI + REAMOR) * DEFRFNONIBIS
            + 0;
DEFRFNONI = positif(null(PREM8_11) * positif(DEFRFNONI2-DEFRFNONI1)) * DEFRFNONI2 + 0
            + PREM8_11 * positif(DEFRFNONI2) * (DEFRFNONI2+DEFRFNONI1) + 0;

regle 831095:
application : iliad ;




4BB4BA =arr(RFDORD*(RFORDI/RFCD));
R14BA = RFORDI-4BB4BA;
4BC4BA =arr( RFDHIS*(R14BA/RFCE));
R24BA = R14BA -4BC4BA;
R2BA = RFCE - RFDHIS ;
4BD4BA =arr( RFDANT * (R24BA/R2BA));
R3BA = max (0, R2BA-RFDANT);
D3BA = min (0,R2BA-RFDANT);

R34BA = R24BA -4BD4BA ; 


4BBRBA = max (0,arr(RFDORD*(FONCI/RFCD)));
R1RBA =max(0, FONCI- 4BBRBA);
4BCRBA =max(0,arr( RFDHIS*(R1RBA/RFCE)));
R2RBA =max(0, R1RBA - 4BCRBA);
4BDRBA =max (0,arr(RFDANT*( R2RBA/R2BA)));

R3RBA = max(0,arr(R2RBA - 4BDRBA)) ; 


4BBSBA =max (0,arr( RFDORD-(4BB4BA+4BBRBA)));
R1SBA =max(0, REAMOR-4BBSBA);
4BCSBA = max(0,RFDHIS-(4BC4BA+4BCRBA));
R2SBA =max(0,R1SBA-4BCSBA);
4BDSBA =max(0, RFDANT-(4BD4BA+4BDRBA));

R3SBA =max (0,R2SBA - 4BDSBA);

R4BL = (positif (D3BA))*0
      + (positif(R3BA))*( arr(COD4BL-(COD4BL*4BD4BA/RFORDI)));


RRBT =(positif(R3BA))*max(0,arr( CODRBT - arr((4BBRBA+4BCRBA+4BDRBA)*CODRBT/FONCI)))
       + positif(D3BA)*(0) ;
       
regle 831096:
application : iliad ;


REVORDIRF = RRFI - R4BL ;


REVQUOTRF = REVRF -RRBT;


RFTXFOYER =(1-positif(COD4BN))* max(0,REVORDIRF + REVQUOTRF)
           +positif(COD4BN)*(0);

