#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2021]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2021 
#au titre des revenus perçus en 2020. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
verif 1700:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   RDCOM > 0
   et
   SOMMEA700 = 0

alors erreur A700 ;
verif 1702:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
    ((V_REGCO +0) dans (1,3,5,6)
    ou
    (VALREGCO+0)  non dans (2))
   et
   INTDIFAGRI * positif(INTDIFAGRI) + 0 > RCMHAB * positif(RCMHAB) + 0

alors erreur A702 ;
verif 1703:
application :  iliad ;

si
 (V_MODUL+0) < 1
   et
 (
  ( (positif(PRETUD+0) = 1 ou positif(PRETUDANT+0) = 1)
   et
    V_0DA < 1985
   et
    positif(BOOL_0AM+0) = 0 )
  ou
  ( (positif(PRETUD+0) = 1 ou positif(PRETUDANT+0) = 1)
   et
   positif(BOOL_0AM+0) = 1
   et
   V_0DA < 1985
   et
   V_0DB < 1985 )
  )
alors erreur A703 ;
verif 1704:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   (positif(CASEPRETUD + 0) = 1 et positif(PRETUDANT + 0) = 0)
   ou
   (positif(CASEPRETUD + 0) = 0 et positif(PRETUDANT + 0) = 1)

alors erreur A704 ;
verif 17071:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDENS + RDENL + RDENU > V_0CF + V_0DJ + V_0DN + 0

alors erreur A70701 ;
verif 17072:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDENSQAR + RDENLQAR + RDENUQAR > V_0CH + V_0DP + 0

alors erreur A70702 ;
verif 1708:
application : iliad ;


si
  (V_MODUL + 0) < 1
  et
  V_IND_TRAIT > 0
  et
  (
   COD7UY + 0 > LIMLOC2
   ou
   COD7UZ + 0 > LIMLOC2
  )

alors erreur A708 ;
verif 1709:
application :  iliad ;


si
  (V_MODUL + 0) < 1
  et
  positif(COD7UY) + positif(COD7UZ) + 0 > 1

alors erreur A709 ;
verif 17111:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   INAIDE > 0
   et
   positif( CREAIDE + 0) = 0

alors erreur A71101 ;
verif 17112:
application :  iliad ;
si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   ASCAPA >0
   et 
   positif (CREAIDE + 0) = 0

alors erreur A71102 ;
verif 17113:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
  et
   PREMAIDE > 0
   et
   positif(CREAIDE + 0) = 0

alors erreur A71103 ;
verif 17114:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
     et
   COD7DR > 0
     et
   positif(CREAIDE + 0) = 0

alors erreur A71104 ;
verif 17115:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
     et
   COD7HB > 0
     et
   positif(CREAIDE + 0) = 0

alors erreur A71105 ;
verif 17121:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   PRESCOMP2000 + 0 > PRESCOMPJUGE
   et
   positif(PRESCOMPJUGE) = 1

alors erreur A712 ;
verif non_auto_cc 1713:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   (PRESCOMPJUGE + 0 > 0 et PRESCOMP2000 + 0 = 0)
   ou
   (PRESCOMPJUGE + 0 = 0 et PRESCOMP2000 + 0 > 0)

alors erreur A713 ;
verif 1714:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDPRESREPORT + 0 > 0
   et
   PRESCOMPJUGE + PRESCOMP2000 + 0 > 0

alors erreur A714 ;
verif 1715:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   RDPRESREPORT + 0 > LIM_REPCOMPENS

alors erreur A715 ;
verif 1716:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   ((SUBSTITRENTE < PRESCOMP2000 + 0)
    ou
    (SUBSTITRENTE > 0 et present(PRESCOMP2000) = 0))

alors erreur A716 ;
verif 1718:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   CIAQCUL > 0
   et
   SOMMEA718 = 0

alors erreur A718 ;
verif 1719:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   RDMECENAT > 0
   et
   SOMMEA719 = 0

alors erreur A719 ;
verif 1731:
application :  iliad ;


si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   CASEPRETUD + 0 > 5

alors erreur A731 ;
verif 17364:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif(PINELQM) + positif(PINELQN) + positif(PINELQO) + positif(PINELQP) + 0 > 2

alors erreur A73604 ;
verif 17365:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
  positif(COD7QR) + positif(COD7QS) + positif(COD7QT) + positif(COD7QU) + 0 > 2

alors erreur A73605 ;  
verif 17366:
application :  iliad ;

si
   (V_MODUL+0) < 1
    et
   V_IND_TRAIT > 0
   et
   positif(COD7QW) + positif(COD7QX) + positif(COD7QY) + positif(COD7QQ) + positif(COD7NA) + positif(COD7NB) + positif(COD7NC) + positif(COD7ND) + 0 > 2

alors erreur A73606 ;
verif 17367:
application :  iliad ;

si
   (V_MODUL+0) < 1
    et
   V_IND_TRAIT > 0
    et
   positif(COD7QA) + positif(COD7QB) + positif(COD7QC) + positif(COD7QD) + positif(COD7NE) + positif(COD7NF) + positif(COD7NG) + positif(COD7NH) + 0 > 2

alors erreur A73607 ;
verif 17371:
application :  iliad ;

si
   (V_MODUL+0) < 1
    et
   V_IND_TRAIT > 0
   et
   (COD7YE + 0) < 1
   et
   positif(COD7ZM) + positif(COD7ZN) + 0 > 1

alors erreur A73701 ;
verif 17372:
application :  iliad ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
    et
   (COD7YE + 0) < 1
   et
   positif(COD7ZA) + positif(COD7ZB) + positif(COD7ZG) + positif(COD7ZH) + 0 > 1

alors erreur A73702 ;
verif 17373:
application :  iliad ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
    et
  (COD7YE + 0) < 1
   et
   positif(COD7ZC) + positif(COD7ZD) + positif(COD7ZE) + positif(COD7ZF) + positif(COD7RI) +  positif(COD7RJ) +  positif(COD7RK) +  positif(COD7RL) +  positif(COD7IR) +  positif(COD7IS) +  positif(COD7IT) +  positif(COD7IU) +  0 > 1

alors erreur A73703 ;
verif 17374:
application :  iliad ;

si
   (V_MODUL+0) < 1
   et
    V_IND_TRAIT > 0
    et
   (COD7YE + 0) < 1
   et
   positif(COD7RM) + positif(COD7RN) + positif(COD7RO) + positif(COD7RP) + positif(COD7IV) +  positif(COD7IW) +  positif(COD7IX) +  positif(COD7IY) +  0 > 1

alors erreur A73704 ;
verif 17375:
application :  iliad ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   (COD7YE + 0) < 1
   et   
   positif(COD7RQ) + positif(COD7IZ) +  0 > 1

alors erreur A73705 ;
verif 17376:
application :  iliad ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
  (COD7YE + 0) < 1
   et
   positif(COD7XH) + positif(COD7XI) + positif(COD7XJ) + positif(COD7XK) + positif(COD7JE) +  positif(COD7JF) +  positif(COD7JG) +  positif(COD7JH) +  0 > 1

alors erreur A73706 ;
verif 17377:
application :  iliad ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   (COD7YE + 0) = 0
   et
   positif(COD7JI) + positif(COD7JJ) + positif(COD7JK) + positif(COD7JL) +  0 > 1

alors erreur A73707 ;
verif 17401:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   (CODHCM + CODHCR + CODHCW + CODHBW + CODHCB + CODHCG + CODHDM + CODHDR + CODHDW + CODHER + CODHEW + CODHFR + CODHFW + CODHGW  + 0) > PLAF_INVDOM6

alors erreur A74001 ;
verif 17402:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
  (CODHCM + CODHCR + CODHCW  + CODHBR + CODHBM + CODHBW + CODHCB + CODHCG + CODHDM + CODHDR + CODHDW + CODHER + CODHEW + CODHFR + CODHFW + CODHGW + 0) > PLAF_INVDOM5

alors erreur A74002 ;
verif 1744:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 present(COD7WK) + present(COD7WQ) + present(COD7WH) + present(COD7WS) + present(COD7XZ) > 0
 et
 present(COD7WK) + present(COD7WQ) + present(COD7WH) + present(COD7WS)+ present(COD7XZ) < 5

alors erreur A744;
verif 1745:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif_ou_nul(COD7ZW + COD7ZX + COD7ZY + COD7ZZ) = 1
   et
   positif_ou_nul(COD7ZW) + positif_ou_nul(COD7ZX) + positif_ou_nul(COD7ZY) + positif_ou_nul(COD7ZZ) < 4

alors erreur A745 ;
verif 1752:
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   V_IND_TRAIT > 0
   et
   positif(DDEVDURN1) = 1
   et
   positif_ou_nul(COD7XD + COD7XE + COD7XF + COD7XG + COD7XV) = 1
   et
   positif_ou_nul(COD7XD) + positif_ou_nul(COD7XE) + positif_ou_nul(COD7XF) + positif_ou_nul(COD7XG) + positif_ou_nul(COD7XV) < 5

alors erreur A752 ;
verif 17541:
application : iliad  ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(COD7AL) = 1
   et
   present(COD7AK) = 0

alors erreur A75401 ;
verif 17542:
application : iliad  ;

si
   (V_MODUL+0) < 1
    et
   V_IND_TRAIT > 0
    et
   positif(COD7AN) = 1
    et
   present(COD7AM) =  0

alors erreur A75402 ;
verif 17543:
application : iliad  ;

si
   (V_MODUL+0) < 1
    et
   V_IND_TRAIT > 0
    et
   positif(COD7CK) = 1
    et
   present(COD7CC) + present(COD7CG) < 2

alors erreur A75403 ;
verif 17544:
application : iliad  ;

si
   (V_MODUL+0) < 1
    et
    V_IND_TRAIT > 0
    et
    positif(COD7AQ) = 1
    et
    present(COD7AO) =  0

alors erreur A75404 ;
verif 17545:
application : iliad  ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(COD7DA) = 1
   et
  present(COD7CN) + present(COD7CU) < 2

alors erreur A75405 ;
verif 17546:
application : iliad  ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(COD7FB) = 1
   et
   present(COD7FA) =  0

alors erreur A75406 ;
verif 17547:
application : iliad  ;

si
   (V_MODUL+0) < 1
   et
    V_IND_TRAIT > 0
   et
   positif(COD7GK) = 1
   et
   present(COD7DX) + present(COD7GI) < 2

alors erreur A75407 ;
verif 17548:
application : iliad  ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(COD7GH) = 1
   et
   present(COD7FZ) =  0

alors erreur A75408 ;
verif 17549:
application : iliad  ;

si
   (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(COD7AP) = 1
   et
   present(COD7AT) =  0

alors erreur A75409 ;
verif 175501:
application : iliad  ;

si
  (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
  positif(SOMMEA755) = 1
   et
  positif(V_0AM * V_0AX) = 1
   et 
  present(V_0AB) + present(V_0AU) < 1
   et 
  (present(COD8XZ) + present(COD8YZ)) < 2

alors erreur A75501 ;
verif 175502:
application : iliad  ;

si
  (V_MODUL+0) < 1
  et
  V_IND_TRAIT > 0
  et
  positif(SOMMEA755) = 1
  et
  positif(V_0AO * V_0AX) = 1
  et
  present(V_0AB) = 0
  et
  (present(COD8XZ) + present(COD8YZ)) < 2
  
alors erreur A75502;
verif 175503:
application : iliad  ;

si
  (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(SOMMEA755) = 1
   et
   positif(V_0AD * V_0AY) = 1
   et
  (present(COD8XZ) + present(COD8YZ)) < 2

alors erreur A75503;
verif 175504:
application : iliad  ;

si
  (V_MODUL+0) < 1
   et
   V_IND_TRAIT > 0
   et
   positif(SOMMEA755) = 1
   et
   positif(V_0AV * V_0AZ) = 1
   et
   (present(COD8XZ) + present(COD8YZ)) < 2
   et
  (present(V_BTRFRN1)+ present(V_BTRFRN2) + present(RFRN1) + present(RFRN2)) < 2
  
alors erreur A75504;
verif 1761: 
application : iliad  ;

si
   (V_MODUL+0) < 1
     et
   APPLI_OCEANS = 0
   et
  (
  (CIGARD > 0
  et
  1 - V_CNR > 0
  et
  positif(RDGARD1) + positif(RDGARD2) + positif(RDGARD3) + positif(RDGARD4) > EM7 + 0)
  ou
 (CIGARD > 0
  et
  1 - V_CNR > 0
  et
  positif(RDGARD1QAR) + positif(RDGARD2QAR) + positif(RDGARD3QAR) + positif(RDGARD4QAR) > EM7QAR + 0)
  )

alors erreur A761 ;
