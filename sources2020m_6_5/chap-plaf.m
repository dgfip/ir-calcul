#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2021]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2021 
#au titre des revenus perçus en 2020. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************


regle 8200:
application : iliad  ;


LIMIT12 = 18000 + max(0, arr( max(0, RI1 + TONEQUO1) * (4/100))) 
		     * (1 - positif(VARRMOND))
	        + max(0, 
		      arr( max(0, 
				VARRMOND 
				 + TONEQUOM1
			      )* (4/100))
		      ) 
		      * positif(VARRMOND);



LIMIT11 = 18000 + max(0, arr( max(0, RI1 + TONEQUO1) * (6/100))) 
		     * (1 - positif(VARRMOND))
	        + max(0, 
		      arr( max(0, 
			        VARRMOND
				  + TONEQUOM1
			      ) * (6/100))
		      ) 
		      * positif(VARRMOND);



LIMIT10 = 20000 + max(0, arr( max(0, RI1 + TONEQUO1) * (8/100))) 
		     * (1 - positif(VARRMOND))
	        + max(0, 
		      arr( max(0,
				VARRMOND
				  + TONEQUOM1
			      ) * (8/100))
		     ) 
		     * positif(VARRMOND);



LIMIT9 = 25000 + max(0, arr( max(0, RI1 + TONEQUO1) * (10/100))) 
		    * (1 - positif(VARRMOND))
               + max(0, 
		     arr( max(0,
			       VARRMOND
				 + TONEQUOM1
			     ) * (10/100))
		    ) 
		    * positif(VARRMOND);
		     
regle 82021:
application : iliad  ;


IRBTEO = max(0,V_IAD11TEO - min(RRI1,RLOGDOMTEO)-min(RRISUP,RLOGSOCTEO+RCOLENTTEO) + VERSLIB + ITP + PVMTS + REI + AUTOVERSSUP);
IANTEO = max( 0, (IRBTEO - V_INETEO
                 + min(TAXASSUR+0 , max(0,V_INETEO-IRBTEO))
                 + min(IPCAPTAXTOT+0 , max(0,V_INETEO-IRBTEO - min(TAXASSUR+0,max(0,V_INETEO-IRBTEO))))
              )
         )
       ;
NEGIANTEO =  -1 * (min(TAXASSUR+0 , max(0,V_INETEO-IRBTEO))
		 + min(IPCAPTAXTOT+0 , max(0,V_INETEO-IRBTEO - min(TAXASSUR+0,max(0,V_INETEO-IRBTEO))))) ;

IARTEO = min( 0, IANTEO - V_IRETEO ) + max( 0, IANTEO - V_IRETEO ) + NEGIANTEO;
regle 820211:
application : iliad  ;
AVFISCO = (IARTEO - IARAF)* positif_ou_nul(V_INDTEO);

regle 82025:
application : iliad  ;
DIFFTEOREEL = AVFISCO * (1 - V_INDTEO)* positif_ou_nul(V_INDTEO);

regle 8202:
application : iliad  ;
AVFISCOPTER = (AVPLAF9 + AVPLAF10 + AVPLAF11 + AVPLAF12 + AVPLAF13) * positif_ou_nul(V_INDTEO);
regle 82463:
application : iliad  ;


A13RSOC = max(0 , RSOC49 + RSOC54 + RSOC45 + RSOC50 + RSOC39 + RSOC44 + RSOC35 + RSOC40 + RSOC29 + RSOC34 
                  + RSOC25 + RSOC30 + RSOC20 + RSOC24 + RSOCHYB + RSOCHYBR + RSOCHYA + RSOCHYAR
		  + RSOCHYD + RSOCHYDR + RSOCHYC + RSOCHYCR + RSOCHYE + RSOCHYER
                  - arr((((INVRETXT + INVRETXTR) * (1 - INDPLAF) + (INVRETXTA + INVRETXTRA) * INDPLAF)
		       + ((INVRETXO + INVRETXOR) * (1 - INDPLAF) + (INVRETXOA + INVRETXORA) * INDPLAF)
		       + ((INVRETXI + INVRETXIR) * (1 - INDPLAF) + (INVRETXIA + INVRETXIRA) * INDPLAF)
		       + ((INVRETYA + INVRETYAR) * (1 - INDPLAF) + (INVRETYAA + INVRETYARA) * INDPLAF)
		       + ((INVRETYC + INVRETYCR) * (1 - INDPLAF) + (INVRETYCA + INVRETYCRA) * INDPLAF)) * TX65/100)
                  - arr((((INVRETXU + INVRETXUR) * (1 - INDPLAF) + (INVRETXUA + INVRETXURA) * INDPLAF)
		       + ((INVRETYB + INVRETYBR) * (1 - INDPLAF) + (INVRETYBA + INVRETYBRA) * INDPLAF)
		       + ((INVRETYD + INVRETYDR) * (1 - INDPLAF) + (INVRETYDA + INVRETYDRA) * INDPLAF)
		       + ((INVRETYE + INVRETYER) * (1 - INDPLAF) + (INVRETYEA + INVRETYERA) * INDPLAF)
		       + ((INVRETXP + INVRETXPR) * (1 - INDPLAF) + (INVRETXPA + INVRETXPRA) * INDPLAF)
		       + ((INVRETXK + INVRETXKR) * (1 - INDPLAF) + (INVRETXKA + INVRETXKRA) * INDPLAF)) * TX70/100)
             ) * (1 - V_CNR) ;

regle 82462:
application : iliad  ;


A12RSOC = max(0 , RSOC48 + RSOC53 + RSOC38 + RSOC43 + RSOC28 + RSOC33 + RSOC19 + RSOC23 
                  - arr((((INVRETXS + INVRETXSR) * (1 - INDPLAF) + (INVRETXSA + INVRETXSRA) * INDPLAF)
		       + ((INVRETXN + INVRETXNR) * (1 - INDPLAF) + (INVRETXNA + INVRETXNRA) * INDPLAF)
		       + ((INVRETXH + INVRETXHR) * (1 - INDPLAF) + (INVRETXHA + INVRETXHRA) * INDPLAF)) * TX65/100)
             ) * (1 - V_CNR) ; 

regle 82461:
application : iliad  ;


A11RSOC = max(0 , RSOC47 + RSOC52 + RSOC37 + RSOC42 + RSOC27 + RSOC32 + RSOC18 + RSOC22 
                  - arr((((INVRETXR + INVRETXRR) * (1 - INDPLAF) + (INVRETXRA + INVRETXRRA) * INDPLAF)
		       + ((INVRETXM + INVRETXMR) * (1 - INDPLAF) + (INVRETXMA + INVRETXMRA) * INDPLAF)
		       + ((INVRETXG + INVRETXGR) * (1 - INDPLAF) + (INVRETXGA + INVRETXGRA) * INDPLAF)) * TX65/100)
             ) * (1 - V_CNR) ;

regle 8246:
application : iliad  ;


A10RSOC = max(0 , RSOC46 + RSOC51 + RSOC36 + RSOC41 + RSOC26 + RSOC31 + RSOC17 + RSOC21 
                  - arr((((INVRETXQ + INVRETXQR) * (1 - INDPLAF) + (INVRETXQA + INVRETXQRA) * INDPLAF)
		       + ((INVRETXL + INVRETXLR) * (1 - INDPLAF) + (INVRETXLA + INVRETXLRA) * INDPLAF)
		       + ((INVRETXF + INVRETXFR) * (1 - INDPLAF) + (INVRETXFA + INVRETXFRA) * INDPLAF)) * TX65/100)
             ) * (1 - V_CNR) ;

regle 82473:
application : iliad  ;


A13RENT = (RLOCHFP + RLOCHFR + RLOCHFU + RLOCHGU + RLOCHGW + RLOCHFW + RLOCHEP + RLOCHEU + RLOCHER + RLOCHEW + RLOC142 + RLOC145 + RLOC143 + RLOC146 + RLOC68 + RLOC74 
           + RLOC100 + RLOC105 + RLOC101 + RLOC106 + RLOC124 + RLOC127  + RLOC125 + RLOC128
           + max (0 , RLOCHFN + RLOCHGT + RLOCHGS + RLOCHFO + RLOCHFS + RLOCHFT + RLOCHFNR + RLOCHGTR + RLOCHGSR + RLOCHFOR + RLOCHFSR + RLOCHFTR 
	              + RLOCHET + RLOCHEO + RLOCHES + RLOCHEN + RLOCHETR + RLOCHEOR + RLOCHESR + RLOCHENR 
	              + RLOC140 + RLOC152 + RLOC137 + RLOC149 + RLOC138 + RLOC150 + RLOC135 + RLOC147 + RLOC62 + RLOC86 + RLOC56 + RLOC80 
		      + RLOC96 + RLOC115 + RLOC91 + RLOC110 + RLOC92 + RLOC116 + RLOC87 + RLOC111 + RLOC122 + RLOC134 + RLOC119 + RLOC131 
		      + RLOC120 + RLOC132 + RLOC117 + RLOC129
                    - (
		         arr(((INVRETFN + INVRETFNR) * (1 - INDPLAF) + (INVRETFNA + INVRETFNRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETFO + INVRETFOR) * (1 - INDPLAF) + (INVRETFOA + INVRETFORA) * INDPLAF) * TX625/100)
		       + arr(((INVRETFS + INVRETFSR) * (1 - INDPLAF) + (INVRETFSA + INVRETFSRA) * INDPLAF) * TX56/100)
		       + arr(((INVRETFT + INVRETFTR) * (1 - INDPLAF) + (INVRETFTA + INVRETFTRA) * INDPLAF) * TX66/100)
		       + arr(((INVRETEN + INVRETENR) * (1 - INDPLAF) + (INVRETENA + INVRETENRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETEO + INVRETEOR) * (1 - INDPLAF) + (INVRETEOA + INVRETEORA) * INDPLAF) * TX625/100)
		       + arr(((INVRETES + INVRETESR) * (1 - INDPLAF) + (INVRETESA + INVRETESRA) * INDPLAF) * TX56/100)
		       + arr(((INVRETET + INVRETETR) * (1 - INDPLAF) + (INVRETETA + INVRETETRA) * INDPLAF) * TX66/100)
		       + arr(((INVRETDN + INVRETDNR) * (1 - INDPLAF) + (INVRETDNA + INVRETDNRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETDO + INVRETDOR) * (1 - INDPLAF) + (INVRETDOA + INVRETDORA) * INDPLAF) * TX625/100)
		       + arr(((INVRETDS + INVRETDSR) * (1 - INDPLAF) + (INVRETDSA + INVRETDSRA) * INDPLAF) * TX56/100)
		       + arr(((INVRETDT + INVRETDTR) * (1 - INDPLAF) + (INVRETDTA + INVRETDTRA) * INDPLAF) * TX66/100)
		       + arr(((INVRETCN + INVRETCNR) * (1 - INDPLAF) + (INVRETCNA + INVRETCNRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETCO + INVRETCOR) * (1 - INDPLAF) + (INVRETCOA + INVRETCORA) * INDPLAF) * TX625/100)
		       + arr(((INVRETCS + INVRETCSR) * (1 - INDPLAF) + (INVRETCSA + INVRETCSRA) * INDPLAF) * TX56/100)
		       + arr(((INVRETCT + INVRETCTR) * (1 - INDPLAF) + (INVRETCTA + INVRETCTRA) * INDPLAF) * TX66/100)
		       + arr(((INVRETBX + INVRETBXR) * (1 - INDPLAF) + (INVRETBXA + INVRETBXRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETBY + INVRETBYR) * (1 - INDPLAF) + (INVRETBYA + INVRETBYRA) * INDPLAF) * TX625/100)
		       + arr(((INVRETCC + INVRETCCR) * (1 - INDPLAF) + (INVRETCCA + INVRETCCRA) * INDPLAF) * TX56/100)
		       + arr(((INVRETCD + INVRETCDR) * (1 - INDPLAF) + (INVRETCDA + INVRETCDRA) * INDPLAF) * TX66/100)
                       + arr(((INVRETGS + INVRETGSR) * (1 - INDPLAF) + (INVRETGSA + INVRETGSRA) * INDPLAF) * TX56/100)
                       + arr(((INVRETGT + INVRETGTR) * (1 - INDPLAF) + (INVRETGTA + INVRETGTRA) * INDPLAF) * TX66/100)
		       )
                  )
             ) * (1 - V_CNR) ;

regle 82472:
application : iliad  ;


A12RENT = (RLOC73 + RLOC67 + RLOC99 + RLOC104 + RLOC123 + RLOC126 + RLOC141 + RLOC144
           + max (0 , RLOC139 + RLOC151 + RLOC136 + RLOC148 + RLOC121 + RLOC133 + RLOC118 + RLOC130 + RLOC95 + RLOC114 
	              + RLOC90 + RLOC109 + RLOC61 + RLOC85 + RLOC55 + RLOC79  
                    - (arr(((INVRETDI + INVRETDIR) * (1 - INDPLAF) + (INVRETDIA + INVRETDIRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETDJ + INVRETDJR) * (1 - INDPLAF) + (INVRETDJA + INVRETDJRA) * INDPLAF) * TX625/100)
		       + arr(((INVRETCI + INVRETCIR) * (1 - INDPLAF) + (INVRETCIA + INVRETCIRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETCJ + INVRETCJR) * (1 - INDPLAF) + (INVRETCJA + INVRETCJRA) * INDPLAF) * TX625/100)
		       + arr(((INVRETBS + INVRETBSR) * (1 - INDPLAF) + (INVRETBSA + INVRETBSRA) * INDPLAF) * TX5263/100)
		       + arr(((INVRETBT + INVRETBTR) * (1 - INDPLAF) + (INVRETBTA + INVRETBTRA) * INDPLAF) * TX625/100))
                 )
            ) * (1 - V_CNR);

regle 82471:
application : iliad  ;


A11RENT = (RLOC64 + RLOC70 + RLOC66 + RLOC72 + RLOC98 + RLOC103    
           + max (0 , RLOC58 + RLOC82 
	              + RLOC52 + RLOC76 + RLOC60 + RLOC84 + RLOC54 + RLOC78 + RLOC94 + RLOC113 + RLOC89 + RLOC108
                      - (arr(((INVRETBN + INVRETBNR) * (1 - INDPLAF) + (INVRETBNA + INVRETBNRA) * INDPLAF) * TX5263/100)
		         + arr(((INVRETBO + INVRETBOR) * (1 - INDPLAF) + (INVRETBOA + INVRETBORA) * INDPLAF) * TX625/100))
                 )
            ) * (1 - V_CNR) ;

regle 8247:
application : iliad  ;


A10RENT = (RLOC63 + RLOC69 + RLOC65 + RLOC71 + RLOC97 + RLOC102 
           + max (0 , RLOC57 + RLOC81 
	              + RLOC51 + RLOC75 + RLOC59 + RLOC83 + RLOC53 + RLOC77 + RLOC93 + RLOC112  + RLOC88 + RLOC107
                      - (arr(((INVRETBI + INVRETBIR) * (1 - INDPLAF) + (INVRETBIA + INVRETBIRA) * INDPLAF) * TX5263/100)
		         + arr(((INVRETBJ + INVRETBJR) * (1 - INDPLAF) + (INVRETBJA + INVRETBJRA) * INDPLAF) * TX625/100))
                 )
            ) * (1 - V_CNR) ;

regle 82492:
application : iliad  ;
PLAFRED_FORTRA = max( 0, PLAF_FOREST1 * (1 + BOOL_0AM) - ACOTFOR_R);
BASE7UWI = max(0, min(REPSINFOR3 + REPSINFOR4 , PLAFRED_FORTRA)
			   - (REPSINFOR3)) * (1 - V_CNR) ;
BASE7UN = (min (RDFOREST, PLAF_FOREST * (1 + BOOL_0AM))) * (1 - V_CNR) ;

regle 82493:
application : iliad  ;
A13RFOR_1 = max(0 , min(arr((BASE7UWI + BASE7UN)* TX18/100) , RRI1-RLOGDOMTOT-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT
                                                                -RNOUV-RPLAFREPME4-RPENTDY-RPENTEY-A10RFOR-A11RFOR-A12RFOR)) ;

A13RFOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (A13RFOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(A13RFOR_1,A13RFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
regle 8249101:
application : iliad  ;
BASE7UWH = max(0, min(REPSINFOR3 , PLAFRED_FORTRA)) * (1 - V_CNR) ;
BA12RFOR  = arr(BASE7UWH * TX18 / 100 ) ;
A12RFOR_1 = max(0 , min(BA12RFOR , RRI1-RLOGDOMTOT-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT
                                     -RNOUV-RPLAFREPME4-RPENTDY-RPENTEY-A10RFOR-A11RFOR)) ;

A12RFOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (A12RFOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(A12RFOR_1,A12RFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
regle 8249:
application : iliad  ;
BA10RFOR  = arr(BASE7UTF * TX25 / 100 ) ;
A10RFOR_1 = max(0 , min(BA10RFOR , RRI1-RLOGDOMTOT-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPLAFREPME4-RPENTDY-RPENTEY)) ;

A10RFOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (A10RFOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(A10RFOR_1,A10RFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
regle 8250:
application : iliad  ;
A13REELA = RCOTFOR + RFIPDOM + RFIPC  + RPRESSE + RINNO + RSOUFIP + RRIRENOV + RDUFREP 
           + RPIQW + RPIQX + RPIQM + RPIQN + RPIQR + RPIQS + RPIREPBI + RPIREPBZ 
	   + RPIREPCZ + RPIREPQZ + RPIREPRZ + RPIREPRA + RPIREPRB + RPIREPRE + RPIREPRF
	   + RPIREPJM + RPIREPKM + RPIQA + RPIQB + RNORMJA + RNORMJB 
           + RPI7RR
           + RNONA + RNONB + RNONE + RNONF+ A13RFOR 
           + arr(RSNCF + RSNCL + RSNCQ + RSNCR + RSNCN + RSNCV + RSNCC + RSNCX + RSNCH)
	   + RPENTOT + RRSOFON
           + CIDEVDUR + CIGARD + CIADCRE + CIHABPRIN + CIFORET
	   + RCODJT + RILMOA + RILMPJ + RILMOF + RILMPO + RILMPT + RILMPY + RILMHS 
	   + RILMHX + RILMSA + RILMSN + RILMSP + RILMOK + RILMOP 
	   + RCODOU + RCODOV + RCODOW + RCODOX + RCODOY + RCODPZ + RCODMZ + RPATNAT + RREHAB ; 
A13REELB = RCINE 
           + RPIQQ + RPIQY + RPIQO + RPIQP + RPIQT + RPIQU + RPIREPDI + RPIREPDZ + RPIREPEZ 
	   + RPIREPSZ + RPIREPTZ + RPIREPRC + RPIREPRD + RPIREPRG + RPIREPRH
	   + RPI7RS + RPIREPLM + RPIREPMM + RPIQC + RPIQD 
	   + RNORMJC + RNORMJD + RNONC + RNOND + RNONG + RNONH
	   + RLOG32 + RLOG39 + RLOG46 + RLOG53 + RLOG60 + RLOGHVH + RLOGHVI + RLOGHVJ
           + A13RSOC + A13RENT ;

regle 8254:
application : iliad  ;
AUBAINE13A = max(0, min(A13REELA, DIFFTEOREEL)) ;
AUBAINE13B = max(0, min(A13REELB, DIFFTEOREEL - AUBAINE13A)) ;

regle 8255:
application : iliad  ;


A12REEL = A12RFOR + RTOURREP * positif(COD7UY) + RTOUREPA * positif(COD7UZ)
          + RCELRREDLJ + RCELRREDLP + RCELRREDLV
          + RCELLY + RCELMV + RCELMR + RCELREPGJ
          + RCELREPYB + RCELREPYM  
	  + RCELREPYT + RCELREPWX + RCELREPWT 
	  + RCELRV + RCELRT + RCELXQ + RCELYL + RCELZL + RCELWD + RCELWG
	  + RCELNT + RCELNW + RCELJI + RCELJK + RCELJL + RCELIZ
	  + RCELRM + RCELRO + RCELRP + RCELRQ + RCELIX + RCELIY + RCELIV
          + RCODID
          + RILMJV + RILMOB + RILMPI + RILMOG + RILMPN + RILMOL 
	  + RILMPS + RILMPX + RILMHR + RILMHW + RILMOQ + RILMSB + RILMSO
          + RLOG25 + RLOG31 + RLOG38 + RLOG45 + RLOG52 + RLOG59
          + A12RSOC + A12RENT ;

regle 8256:
application : iliad  ;
AUBAINE12 = max( 0, min( A12REEL , DIFFTEOREEL - AUBAINE13A - AUBAINE13B ))   ;

regle 8260:
application : iliad  ;


A11REEL = RLOG16 + RLOG21 + RLOG24 + RLOG28 + RLOG30 
          + RLOG35 + RLOG37 + RLOG42 + RLOG44 + RLOG49 
          + RLOG51 + RLOG56 + RLOG58
          + A11RSOC + A11RENT
          + RCELRREDLI
	  + RCELRREDLO + RCELRREDLU + RCELLC + RCELMU + RCELMQ
          + RCELREPGL + RCELREPYD 
	  + RCELREPYN + RCELREPYU + RCELREPWY + RCELREPWU
	  + RCELRW + RCELRU + RCELZO + RCELXO + RCELYK + RCELZK + RCELKC
	  + RCELWC + RCELWF + RCELNS + RCELNV + RCELKA + RCELJJ
	  + RCELRI + RCELRK + RCELRL + RCELZC + RCELZE + RCELZF
	  + RCELIR + RCELIT + RCELIU + RCELIW
          + RCELRN + RCELXH + RCELXJ + RCELXK + RCELJE + RCELJG + RCELJH
	  + RCODIN + RCODIJ
          + RILMIZ + RILMJW + RILMOC + RILMSC
	  + RILMPH + RILMOH + RILMPM + RILMOM + RILMPR + RILMPW + RILMHQ + RILMHV + RILMOR


          + A11RFOR ;

regle 8261:
application : iliad  ;
AUBAINE11 = max( 0, min( A11REEL , DIFFTEOREEL - AUBAINE13A-AUBAINE13B-AUBAINE12 ));
regle 8262:
application : iliad  ;

A10REEL = RLOG11 + RLOG13 + RLOG15 + RLOG18 + RLOG20 + RLOG23 + RLOG26 + RLOG27 
          + RLOG29 + RLOG33 + RLOG34 + RLOG36 + RLOG40 + RLOG41 + RLOG43 
          + RLOG47 + RLOG48 + RLOG50 + RLOG54 + RLOG55 + RLOG57
          + A10RSOC  + A10RENT 
          + RCELRREDLH + RCELRREDLL + RCELRREDLR + RCELLB + RCELMT + RCELMP
          + RCELREPGS + RCELREPYF + RCELREPYO + RCELREPYV + RCELREPWZ + RCELREPWV
	  + RCELZP + RCELXP + RCELYJ + RCELZJ + RCELKD + RCELWB + RCELWE + RCELNP 
	  + RCELNR + RCELNU + RCELKB + RCELRJ + RCELZD + RCELIS + RCELXI + RCELJF + RCELZA + RCELZG
          + RINVRED + RCODIM 
	  + RILMIH + RILMJX  
	  + RILMPG + RILMOD + RILMOI + RILMPL + RILMON 
	  + RILMPQ + RILMPV + RILMHP + RILMHU + RILMOS + RTOURREP + RTOUREPA
          + A10RFOR ;
regle 8263:
application : iliad  ;
AUBAINE10 = max( 0, min( A10REEL , DIFFTEOREEL - AUBAINE13A-AUBAINE13B-AUBAINE12-AUBAINE11 ));
regle 8280:
application : iliad  ;

AUBAINE9 = max(0, DIFFTEOREEL - AUBAINE13A - AUBAINE13B - AUBAINE12 - AUBAINE11 - AUBAINE10);
regle 8290:
application : iliad  ;

AVPLAF13A = max(0, AUBAINE13A - LIM10000 ) * positif(DIFFTEOREEL) ;
AVPLAF13B = max(0, min(AUBAINE13A , LIM10000) + AUBAINE13B - LIM18000 ) * positif(DIFFTEOREEL) ;
AVPLAF13 = AVPLAF13A + AVPLAF13B ;
AVPLAF12 = max(0, AUBAINE13A + AUBAINE13B + AUBAINE12 
                  - AVPLAF13 - LIMIT12) * positif(DIFFTEOREEL);
AVPLAF11 = max(0, AUBAINE13A + AUBAINE13B + AUBAINE12 + AUBAINE11 
                  - AVPLAF13 - AVPLAF12 - LIMIT11) * positif(DIFFTEOREEL);
AVPLAF10 = max(0, AUBAINE13A + AUBAINE13B + AUBAINE12 + AUBAINE11 + AUBAINE10 
                  - AVPLAF13 - AVPLAF12 - AVPLAF11 - LIMIT10) * positif(DIFFTEOREEL);
AVPLAF9  = max(0, AUBAINE13A + AUBAINE13B + AUBAINE12 + AUBAINE11 + AUBAINE10 + AUBAINE9 
                  - AVPLAF13 - AVPLAF12 - AVPLAF11 - AVPLAF10 - LIMIT9) * positif(DIFFTEOREEL) ;
regle 8321:
application : iliad  ;
RFTEO = RFORDI + RFROBOR ; 
regle 8331:
application : iliad  ;

RFNTEO = (RFORDI + RFROBOR - min(
                                     min(RFDORD,RFDORD1731+0) * positif(ART1731BIS) + RFDORD * (1 - ART1731BIS)
			           + min(RFDANT,RFDANT1731+0) * positif(ART1731BIS) + RFDANT * (1 - ART1731BIS) ,
                                    RFORDI + RFROBOR
                                ) 
                           - RFDHIS * (1 - ART1731BIS)      
         ) * present(RFROBOR) + RRFI * (1-present(RFROBOR));

regle 8341:
application : iliad  ;
RRFTEO = RFNTEO ;

regle 8400 :
application : iliad  ;

RLOG01_1 = max(min(INVLOG2008 , RRI1) , 0) * (1 - V_CNR) ;
RLOG01 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG01_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG01_1,RLOG011731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = RLOG01 ;

RLOG02_1 = max(min(INVLGDEB2009 , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG02 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG02_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG02_1,RLOG021731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG02 ;

RLOG03_1 = max(min(INVLGDEB , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG03 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG03_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG03_1,RLOG031731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG03 ;

RLOG04_1 = max(min(INVOMLOGOA , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG04 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG04_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG04_1,RLOG041731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG04 ;

RLOG05_1 = max(min(INVOMLOGOH , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG05 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG05_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG05_1,RLOG051731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG05 ;

RLOG06_1 = max(min(INVOMLOGOL , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG06 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG06_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG06_1,RLOG061731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG06 ;

RLOG07_1 = max(min(INVOMLOGOO , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG07 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG07_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG07_1,RLOG071731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG07 ;

RLOG08_1 = max(min(INVOMLOGOS , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
RLOG08 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG08_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG08_1,RLOG081731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG08 ;

RLOG09_1 = max(min((INVRETQL * (1 - INDPLAF) + INVRETQLA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG09 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG09_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG09_1,RLOG091731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG09 ;

RLOG10_1 = max(min((INVRETQM * (1 - INDPLAF) + INVRETQMA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG10 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG10_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG10_1,RLOG101731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG10 ;

RLOG11_1 = max(min((INVRETQD * (1 - INDPLAF) + INVRETQDA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG11 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG11_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG11_1,RLOG111731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG11 ;

RLOG12_1 = max(min((INVRETOB * (1 - INDPLAF) + INVRETOBA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG12 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG12_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG12_1,RLOG121731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG12 ;

RLOG13_1 = max(min((INVRETOC * (1 - INDPLAF) + INVRETOCA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG13 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG13_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG13_1,RLOG131731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG13 ;

RLOG14_1 = max(min((INVRETOI * (1 - INDPLAF) + INVRETOIA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG14 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG14_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG14_1,RLOG141731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG14 ;

RLOG15_1 = max(min((INVRETOJ * (1 - INDPLAF) + INVRETOJA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG15 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG15_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG15_1,RLOG151731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG15 ;

RLOG16_1 = max(min((INVRETOK * (1 - INDPLAF) + INVRETOKA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG16 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG16_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG16_1,RLOG161731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG16 ;

RLOG17_1 = max(min((INVRETOM * (1 - INDPLAF) + INVRETOMA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG17 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG17_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG17_1,RLOG171731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG17 ;

RLOG18_1 = max(min((INVRETON * (1 - INDPLAF) + INVRETONA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG18 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG18_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG18_1,RLOG181731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG18 ;

RLOG19_1 = max(min((INVRETOP * (1 - INDPLAF) + INVRETOPA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG19 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG19_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG19_1,RLOG191731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG19 ;

RLOG20_1 = max(min((INVRETOQ * (1 - INDPLAF) + INVRETOQA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG20 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG20_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG20_1,RLOG201731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG20 ;

RLOG21_1 = max(min((INVRETOR * (1 - INDPLAF) + INVRETORA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG21 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG21_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG21_1,RLOG211731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG21 ;

RLOG22_1 = max(min((INVRETOT * (1 - INDPLAF) + INVRETOTA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG22 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG22_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG22_1,RLOG221731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG22 ;

RLOG23_1 = max(min((INVRETOU * (1 - INDPLAF) + INVRETOUA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG23 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG23_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG23_1,RLOG231731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG23 ;

RLOG24_1 = max(min((INVRETOV * (1 - INDPLAF) + INVRETOVA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG24 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG24_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG24_1,RLOG241731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG24 ;

RLOG25_1 = max(min((INVRETOW * (1 - INDPLAF) + INVRETOWA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG25 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG25_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG25_1,RLOG251731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG25 ;

RLOG26_1 = max(min((INVRETOD * (1 - INDPLAF) + INVRETODA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG26 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG26_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG26_1,RLOG261731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG26 ;

RLOG27_1 = max(min((INVRETOE * (1 - INDPLAF) + INVRETOEA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG27 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG27_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG27_1,RLOG271731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG27 ;

RLOG28_1 = max(min((INVRETOF * (1 - INDPLAF) + INVRETOFA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG28 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG28_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG28_1,RLOG281731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG28 ;

RLOG29_1 = max(min((INVRETOG * (1 - INDPLAF) + INVRETOGA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG29 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG29_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG29_1,RLOG291731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG29 ;

RLOG30_1 = max(min((INVRETOX * (1 - INDPLAF) + INVRETOXA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG30 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG30_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG30_1,RLOG301731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG30 ;

RLOG31_1 = max(min((INVRETOY * (1 - INDPLAF) + INVRETOYA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG31 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG31_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG31_1,RLOG311731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG31 ;

RLOG32_1 = max(min((INVRETOZ * (1 - INDPLAF) + INVRETOZA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG32 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG32_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG32_1,RLOG321731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG32 ;

RLOG33_1 = max(min((INVRETUA * (1 - INDPLAF) + INVRETUAA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG33 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG33_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG33_1,RLOG331731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG33 ;

RLOG34_1 = max(min((INVRETUB * (1 - INDPLAF) + INVRETUBA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG34 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG34_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG34_1,RLOG341731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG34 ;

RLOG35_1 = max(min((INVRETUC * (1 - INDPLAF) + INVRETUCA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG35 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG35_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG35_1,RLOG351731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG35 ;

RLOG36_1 = max(min((INVRETUD * (1 - INDPLAF) + INVRETUDA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG36 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG36_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG36_1,RLOG361731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG36 ;

RLOG37_1 = max(min((INVRETUE * (1 - INDPLAF) + INVRETUEA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG37 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG37_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG37_1,RLOG371731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG37 ;

RLOG38_1 = max(min((INVRETUF * (1 - INDPLAF) + INVRETUFA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG38 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG38_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG38_1,RLOG381731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG38 ;

RLOG39_1 = max(min((INVRETUG * (1 - INDPLAF) + INVRETUGA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG39 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG39_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG39_1,RLOG391731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG39 ;

RLOG40_1 = max(min((INVRETUH * (1 - INDPLAF) + INVRETUHA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG40 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG40_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG40_1,RLOG401731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG40 ;

RLOG41_1 = max(min((INVRETUI * (1 - INDPLAF) + INVRETUIA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG41 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG41_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG41_1,RLOG411731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG41 ;

RLOG42_1 = max(min((INVRETUJ * (1 - INDPLAF) + INVRETUJA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG42 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG42_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG42_1,RLOG421731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG42 ;

RLOG43_1 = max(min((INVRETUK * (1 - INDPLAF) + INVRETUKA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG43 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG43_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG43_1,RLOG431731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG43 ;

RLOG44_1 = max(min((INVRETUL * (1 - INDPLAF) + INVRETULA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG44 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG44_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG44_1,RLOG441731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG44 ;

RLOG45_1 = max(min((INVRETUM * (1 - INDPLAF) + INVRETUMA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG45 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG45_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG45_1,RLOG451731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG45 ;

RLOG46_1 = max(min((INVRETUN * (1 - INDPLAF) + INVRETUNA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG46 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG46_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG46_1,RLOG461731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG46 ;

RLOG47_1 = max(min((INVRETUO * (1 - INDPLAF) + INVRETUOA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG47 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG47_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG47_1,RLOG471731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG47 ;

RLOG48_1 = max(min((INVRETUP * (1 - INDPLAF) + INVRETUPA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG48 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG48_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG48_1,RLOG481731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG48 ;

RLOG49_1 = max(min((INVRETUQ * (1 - INDPLAF) + INVRETUQA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG49 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG49_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG49_1,RLOG491731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG49 ;

RLOG50_1 = max(min((INVRETUR * (1 - INDPLAF) + INVRETURA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG50 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG50_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG50_1,RLOG501731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG50 ;

RLOG51_1 = max(min((INVRETUS * (1 - INDPLAF) + INVRETUSA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG51 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG51_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG51_1,RLOG511731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG51 ;

RLOG52_1 = max(min((INVRETUT * (1 - INDPLAF) + INVRETUTA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG52 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG52_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG52_1,RLOG521731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG52 ;

RLOG53_1 = max(min((INVRETUU * (1 - INDPLAF) + INVRETUUA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG53 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG53_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG53_1,RLOG531731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG53 ;

RLOG54_1 = max(min((INVRETVA * (1 - INDPLAF) + INVRETVAA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG54 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG54_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG54_1,RLOG541731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG54 ;

RLOG55_1 = max(min((INVRETVB * (1 - INDPLAF) + INVRETVBA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG55 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG55_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG55_1,RLOG551731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG55 ;

RLOG56_1 = max(min((INVRETVC * (1 - INDPLAF) + INVRETVCA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG56 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG56_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG56_1,RLOG561731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG56 ;

RLOG57_1 = max(min((INVRETVD * (1 - INDPLAF) + INVRETVDA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG57 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG57_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG57_1,RLOG571731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG57 ;

RLOG58_1 = max(min((INVRETVE * (1 - INDPLAF) + INVRETVEA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG58 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG58_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG58_1,RLOG581731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG58 ;

RLOG59_1 = max(min((INVRETVF * (1 - INDPLAF) + INVRETVFA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG59 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG59_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG59_1,RLOG591731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG59 ;

RLOG60_1 = max(min((INVRETVG * (1 - INDPLAF) + INVRETVGA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOG60 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOG60_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOG60_1,RLOG601731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOG60 ;

RLOGHVH_1 = max(min((INVRETVH * (1 - INDPLAF) + INVRETVHA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOGHVH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOGHVH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOGHVH_1,RLOGHVH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOGHVH ;

RLOGHVI_1 = max(min((INVRETVI * (1 - INDPLAF) + INVRETVIA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOGHVI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOGHVI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOGHVI_1,RLOGHVI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOGHVI ;

RLOGHVJ_1 = max(min((INVRETVJ * (1 - INDPLAF) + INVRETVJA * INDPLAF) , RRI1 - VARTMP1) , 0) * (1 - V_CNR) ;
RLOGHVJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOGHVJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOGHVJ_1,RLOGHVJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

RLOGDOMTOT = (1 - V_INDTEO) * (somme(i=1..60: RLOGi) + RLOGHVH + RLOGHVI+RLOGHVJ) ;
RLOGDOMTOT_1 = (1 - V_INDTEO) * (somme(i=1..60: RLOGi_1) + RLOGHVH_1 + RLOGHVI_1+RLOGHVJ_1) ;

RLOGDOMTEO = (RLOG01 + RLOG02 + RLOG03 + RLOG04 + RLOG05 + RLOG06 + RLOG07 + RLOG08) ;

regle 8401 :
application : iliad  ;


VARTMP1 = 0 ;

RSOC25_1 = arr(max(min((INVRETXK * (1 - INDPLAF) + INVRETXKA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC25 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC25_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC25_1,RSOC251731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC25 ;

RSOC26_1 = arr(max(min((INVRETXF * (1 - INDPLAF) + INVRETXFA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC26 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC26_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC26_1,RSOC261731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC26 ;

RSOC27_1 = arr(max(min((INVRETXG * (1 - INDPLAF) + INVRETXGA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC27 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC27_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC27_1,RSOC271731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC27 ;

RSOC28_1 = arr(max(min((INVRETXH * (1 - INDPLAF) + INVRETXHA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC28 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC28_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC28_1,RSOC281731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC28 ;

RSOC29_1 = arr(max(min((INVRETXI * (1 - INDPLAF) + INVRETXIA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC29 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC29_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC29_1,RSOC291731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC29 ;

RSOC30_1 = arr(max(min((INVRETXKR * (1 - INDPLAF) + INVRETXKRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC30 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC30_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC30_1,RSOC301731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC30 ;

RSOC31_1 = arr(max(min((INVRETXFR * (1 - INDPLAF) + INVRETXFRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC31 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC31_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC31_1,RSOC311731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC31 ;

RSOC32_1 = arr(max(min((INVRETXGR * (1 - INDPLAF) + INVRETXGRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC32 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC32_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC32_1,RSOC321731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC32 ;

RSOC33_1 = arr(max(min((INVRETXHR * (1 - INDPLAF) + INVRETXHRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC33 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC33_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC33_1,RSOC331731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC33 ;

RSOC34_1 = arr(max(min((INVRETXIR * (1 - INDPLAF) + INVRETXIRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC34 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC34_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC34_1,RSOC341731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC34 ;

RSOC35_1 = arr(max(min((INVRETXP * (1 - INDPLAF) + INVRETXPA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC35 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC35_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC35_1,RSOC351731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC35 ;

RSOC36_1 = arr(max(min((INVRETXL * (1 - INDPLAF) + INVRETXLA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC36 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC36_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC36_1,RSOC361731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC36 ;

RSOC37_1 = arr(max(min((INVRETXM * (1 - INDPLAF) + INVRETXMA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC37 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC37_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC37_1,RSOC371731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC37 ;

RSOC38_1 = arr(max(min((INVRETXN * (1 - INDPLAF) + INVRETXNA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC38 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC38_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC38_1,RSOC381731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC38 ;

RSOC39_1 = arr(max(min((INVRETXO * (1 - INDPLAF) + INVRETXOA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC39 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC39_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC39_1,RSOC391731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC39 ;

RSOC40_1 = arr(max(min((INVRETXPR * (1 - INDPLAF) + INVRETXPRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC40 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC40_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC40_1,RSOC401731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC40 ;

RSOC41_1 = arr(max(min((INVRETXLR * (1 - INDPLAF) + INVRETXLRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC41 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC41_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC41_1,RSOC411731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC41 ;

RSOC42_1 = arr(max(min((INVRETXMR * (1 - INDPLAF) + INVRETXMRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC42 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC42_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC42_1,RSOC421731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC42 ;

RSOC43_1 = arr(max(min((INVRETXNR * (1 - INDPLAF) + INVRETXNRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC43 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC43_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC43_1,RSOC431731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC43 ;

RSOC44_1 = arr(max(min((INVRETXOR * (1 - INDPLAF) + INVRETXORA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC44 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC44_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC44_1,RSOC441731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC44 ;

RSOC45_1 = arr(max(min((INVRETXU * (1 - INDPLAF) + INVRETXUA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC45 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC45_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC45_1,RSOC451731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC45 ;

RSOC46_1 = arr(max(min((INVRETXQ * (1 - INDPLAF) + INVRETXQA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC46 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC46_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC46_1,RSOC461731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC46 ;

RSOC47_1 = arr(max(min((INVRETXR * (1 - INDPLAF) + INVRETXRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC47 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC47_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC47_1,RSOC471731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC47 ;

RSOC48_1 = arr(max(min((INVRETXS * (1 - INDPLAF) + INVRETXSA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC48 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC48_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC48_1,RSOC481731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC48 ;

RSOC49_1 = arr(max(min((INVRETXT * (1 - INDPLAF) + INVRETXTA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC49 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC49_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC49_1,RSOC491731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC49 ;

RSOC50_1 = arr(max(min((INVRETXUR * (1 - INDPLAF) + INVRETXURA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC50 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC50_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC50_1,RSOC501731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC50 ;

RSOC51_1 = arr(max(min((INVRETXQR * (1 - INDPLAF) + INVRETXQRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC51 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC51_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC51_1,RSOC511731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC51 ;

RSOC52_1 = arr(max(min((INVRETXRR * (1 - INDPLAF) + INVRETXRRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC52 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC52_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC52_1,RSOC521731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC52 ;

RSOC53_1 = arr(max(min((INVRETXSR * (1 - INDPLAF) + INVRETXSRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC53 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC53_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC53_1,RSOC531731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC53 ;

RSOC54_1 = arr(max(min((INVRETXTR * (1 - INDPLAF) + INVRETXTRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOC54 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOC54_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOC54_1,RSOC541731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOC54 ;

RSOCHYB_1 = arr(max(min((INVRETYB * (1 - INDPLAF) + INVRETYBA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYB_1,RSOCHYB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYB ;

RSOCHYA_1 = arr(max(min((INVRETYA * (1 - INDPLAF) + INVRETYAA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYA_1,RSOCHYA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYA ;

RSOCHYBR_1 = arr(max(min((INVRETYBR * (1 - INDPLAF) + INVRETYBRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYBR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYBR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYBR_1,RSOCHYBR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYBR ;

RSOCHYAR_1 = arr(max(min((INVRETYAR * (1 - INDPLAF) + INVRETYARA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYAR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYAR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYAR_1,RSOCHYAR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYAR ;

RSOCHYD_1 = arr(max(min((INVRETYD * (1 - INDPLAF) + INVRETYDA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYD_1,RSOCHYD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYD ;

RSOCHYC_1 = arr(max(min((INVRETYC * (1 - INDPLAF) + INVRETYCA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYC_1,RSOCHYC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYC ;

RSOCHYDR_1 = arr(max(min((INVRETYDR * (1 - INDPLAF) + INVRETYDRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYDR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYDR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYDR_1,RSOCHYDR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYDR ;

RSOCHYCR_1 = arr(max(min((INVRETYCR * (1 - INDPLAF) + INVRETYCRA * INDPLAF) , RRISUP - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYCR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYCR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYCR_1,RSOCHYCR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

regle 8402 :
application : iliad  ;


VARTMP1 = 0 ;

RSOCHYE_1 = arr(max(min((INVRETYE * (1 - INDPLAF) + INVRETYEA * INDPLAF) , RRISUP - RDOMSOC1 - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYE_1,RSOCHYE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSOCHYE ;

RSOCHYER_1 = arr(max(min((INVRETYER * (1 - INDPLAF) + INVRETYERA * INDPLAF) , RRISUP - RDOMSOC1 - VARTMP1) , 0)) * (1 - V_CNR) ;
RSOCHYER =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCHYER_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCHYER_1,RSOCHYER1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

regle 8403 :
application : iliad  ;


VARTMP1 = 0 ;
RRILOC = RRISUP - RDOMSOC1 - RLOGSOC ;

RLOC87_1 = max(min((INVRETCD * (1 - INDPLAF) + INVRETCDA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC87 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC87_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC87_1,RLOC871731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC87 ;

RLOC88_1 = max(min((INVRETBJ * (1 - INDPLAF) + INVRETBJA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC88 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC88_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC88_1,RLOC881731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC88 ;

RLOC89_1 = max(min((INVRETBO * (1 - INDPLAF) + INVRETBOA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC89 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC89_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC89_1,RLOC891731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC89 ;

RLOC90_1 = max(min((INVRETBT * (1 - INDPLAF) + INVRETBTA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC90 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC90_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC90_1,RLOC901731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC90 ;

RLOC91_1 = max(min((INVRETBY * (1 - INDPLAF) + INVRETBYA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC91 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC91_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC91_1,RLOC911731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC91 ;

RLOC92_1 = max(min((INVRETCC * (1 - INDPLAF) + INVRETCCA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC92 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC92_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC92_1,RLOC921731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC92 ;

RLOC93_1 = max(min((INVRETBI * (1 - INDPLAF) + INVRETBIA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC93 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC93_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC93_1,RLOC931731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC93 ;

RLOC94_1 = max(min((INVRETBN * (1 - INDPLAF) + INVRETBNA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC94 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC94_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC94_1,RLOC941731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC94 ;

RLOC95_1 = max(min((INVRETBS * (1 - INDPLAF) + INVRETBSA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC95 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC95_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC95_1,RLOC951731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC95 ;

RLOC96_1 = max(min((INVRETBX * (1 - INDPLAF) + INVRETBXA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC96 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC96_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC96_1,RLOC961731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC96 ;

RLOC97_1 = max(min((INVRETBK * (1 - INDPLAF) + INVRETBKA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC97 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC97_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC97_1,RLOC971731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC97 ;

RLOC98_1 = max(min((INVRETBP * (1 - INDPLAF) + INVRETBPA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC98 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC98_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC98_1,RLOC981731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC98 ;

RLOC99_1 = max(min((INVRETBU * (1 - INDPLAF) + INVRETBUA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC99 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC99_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC99_1,RLOC991731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC99 ;

RLOC100_1 = max(min((INVRETBZ * (1 - INDPLAF) + INVRETBZA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC100 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC100_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC100_1,RLOC1001731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC100 ;

RLOC101_1 = max(min((INVRETCE * (1 - INDPLAF) + INVRETCEA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC101 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC101_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC101_1,RLOC1011731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC101 ;

RLOC102_1 = max(min((INVRETBM * (1 - INDPLAF) + INVRETBMA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC102 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC102_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC102_1,RLOC1021731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC102 ;

RLOC103_1 = max(min((INVRETBR * (1 - INDPLAF) + INVRETBRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC103 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC103_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC103_1,RLOC1031731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC103 ;

RLOC104_1 = max(min((INVRETBW * (1 - INDPLAF) + INVRETBWA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC104 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC104_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC104_1,RLOC1041731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC104 ;

RLOC105_1 = max(min((INVRETCB * (1 - INDPLAF) + INVRETCBA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC105 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC105_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC105_1,RLOC1051731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC105 ;

RLOC106_1 = max(min((INVRETCG * (1 - INDPLAF) + INVRETCGA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC106 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC106_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC106_1,RLOC1061731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC106 ;

RLOC107_1 = max(min((INVRETBJR * (1 - INDPLAF) + INVRETBJRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC107 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC107_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC107_1,RLOC1071731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC107 ;

RLOC108_1 = max(min((INVRETBOR * (1 - INDPLAF) + INVRETBORA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC108 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC108_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC108_1,RLOC1081731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC108 ;

RLOC109_1 = max(min((INVRETBTR * (1 - INDPLAF) + INVRETBTRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC109 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC109_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC109_1,RLOC1091731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC109 ;

RLOC110_1 = max(min((INVRETBYR * (1 - INDPLAF) + INVRETBYRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC110 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC110_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC110_1,RLOC1101731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC110 ;

RLOC111_1 = max(min((INVRETCDR * (1 - INDPLAF) + INVRETCDRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC111 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC111_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC111_1,RLOC1111731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC111 ;

RLOC112_1 = max(min((INVRETBIR * (1 - INDPLAF) + INVRETBIRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC112 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC112_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC112_1,RLOC1121731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC112 ;

RLOC113_1 = max(min((INVRETBNR * (1 - INDPLAF) + INVRETBNRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC113 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC113_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC113_1,RLOC1131731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC113 ;

RLOC114_1 = max(min((INVRETBSR * (1 - INDPLAF) + INVRETBSRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC114 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC114_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC114_1,RLOC1141731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC114 ;

RLOC115_1 = max(min((INVRETBXR * (1 - INDPLAF) + INVRETBXRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC115 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC115_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC115_1,RLOC1151731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC115 ;

RLOC116_1 = max(min((INVRETCCR * (1 - INDPLAF) + INVRETCCRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC116 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC116_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC116_1,RLOC1161731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC116 ;

RLOC117_1 = max(min((INVRETCT * (1 - INDPLAF) + INVRETCTA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC117 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC117_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC117_1,RLOC1171731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC117 ;

RLOC118_1 = max(min((INVRETCJ * (1 - INDPLAF) + INVRETCJA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC118 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC118_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC118_1,RLOC1181731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC118 ;

RLOC119_1 = max(min((INVRETCO * (1 - INDPLAF) + INVRETCOA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC119 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC119_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC119_1,RLOC1191731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC119 ;

RLOC120_1 = max(min((INVRETCS * (1 - INDPLAF) + INVRETCSA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC120 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC120_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC120_1,RLOC1201731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC120 ;

RLOC121_1 = max(min((INVRETCI * (1 - INDPLAF) + INVRETCIA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC121 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC121_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC121_1,RLOC1211731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC121 ;

RLOC122_1 = max(min((INVRETCN * (1 - INDPLAF) + INVRETCNA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC122 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC122_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC122_1,RLOC1221731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC122 ;

RLOC123_1 = max(min((INVRETCK * (1 - INDPLAF) + INVRETCKA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC123 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC123_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC123_1,RLOC1231731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC123 ;

RLOC124_1 = max(min((INVRETCP * (1 - INDPLAF) + INVRETCPA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC124 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC124_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC124_1,RLOC1241731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC124 ;

RLOC125_1 = max(min((INVRETCU * (1 - INDPLAF) + INVRETCUA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC125 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC125_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC125_1,RLOC1251731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC125 ;

RLOC126_1 = max(min((INVRETCM * (1 - INDPLAF) + INVRETCMA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC126 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC126_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC126_1,RLOC1261731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC126 ;

RLOC127_1 = max(min((INVRETCR * (1 - INDPLAF) + INVRETCRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC127 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC127_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC127_1,RLOC1271731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC127 ;

RLOC128_1 = max(min((INVRETCW * (1 - INDPLAF) + INVRETCWA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC128 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC128_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC128_1,RLOC1281731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC128 ;

RLOC129_1 = max(min((INVRETCTR * (1 - INDPLAF) + INVRETCTRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC129 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC129_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC129_1,RLOC1291731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC129 ;

RLOC130_1 = max(min((INVRETCJR * (1 - INDPLAF) + INVRETCJRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC130 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC130_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC130_1,RLOC1301731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC130 ;

RLOC131_1 = max(min((INVRETCOR * (1 - INDPLAF) + INVRETCORA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC131 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC131_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC131_1,RLOC1311731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC131 ;

RLOC132_1 = max(min((INVRETCSR * (1 - INDPLAF) + INVRETCSRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC132 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC132_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC132_1,RLOC1321731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC132 ;

RLOC133_1 = max(min((INVRETCIR * (1 - INDPLAF) + INVRETCIRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC133 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC133_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC133_1,RLOC1331731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC133 ;

RLOC134_1 = max(min((INVRETCNR * (1 - INDPLAF) + INVRETCNRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC134 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC134_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC134_1,RLOC1341731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC134 ;

RLOC135_1 = max(min((INVRETDT * (1 - INDPLAF) + INVRETDTA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC135 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC135_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC135_1,RLOC1351731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC135 ;

RLOC136_1 = max(min((INVRETDJ * (1 - INDPLAF) + INVRETDJA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC136 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC136_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC136_1,RLOC1361731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC136 ;

RLOC137_1 = max(min((INVRETDO * (1 - INDPLAF) + INVRETDOA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC137 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC137_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC137_1,RLOC1371731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC137 ;

RLOC138_1 = max(min((INVRETDS * (1 - INDPLAF) + INVRETDSA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC138 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC138_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC138_1,RLOC1381731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC138 ;

RLOC139_1 = max(min((INVRETDI * (1 - INDPLAF) + INVRETDIA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC139 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC139_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC139_1,RLOC1391731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC139 ;

RLOC140_1 = max(min((INVRETDN * (1 - INDPLAF) + INVRETDNA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC140 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC140_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC140_1,RLOC1401731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC140 ;

RLOC141_1 = max(min((INVRETDK * (1 - INDPLAF) + INVRETDKA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC141 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC141_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC141_1,RLOC1411731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC141 ;

RLOC142_1 = max(min((INVRETDP * (1 - INDPLAF) + INVRETDPA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC142 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC142_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC142_1,RLOC1421731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC142 ;

RLOC143_1 = max(min((INVRETDU * (1 - INDPLAF) + INVRETDUA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC143 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC143_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC143_1,RLOC1431731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC143 ;

RLOC144_1 = max(min((INVRETDM * (1 - INDPLAF) + INVRETDMA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC144 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC144_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC144_1,RLOC1441731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC144 ;

RLOC145_1 = max(min((INVRETDR * (1 - INDPLAF) + INVRETDRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC145 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC145_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC145_1,RLOC1451731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC145 ;

RLOC146_1 = max(min((INVRETDW * (1 - INDPLAF) + INVRETDWA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC146 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC146_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC146_1,RLOC1461731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC146 ;

RLOC147_1 = max(min((INVRETDTR * (1 - INDPLAF) + INVRETDTRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC147 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC147_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC147_1,RLOC1471731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC147 ;

RLOC148_1 = max(min((INVRETDJR * (1 - INDPLAF) + INVRETDJRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC148 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC148_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC148_1,RLOC1481731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC148 ;

RLOC149_1 = max(min((INVRETDOR * (1 - INDPLAF) + INVRETDORA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC149 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC149_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC149_1,RLOC1491731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC149 ;

RLOC150_1 = max(min((INVRETDSR * (1 - INDPLAF) + INVRETDSRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC150 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC150_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC150_1,RLOC1501731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC150 ;

RLOC151_1 = max(min((INVRETDIR * (1 - INDPLAF) + INVRETDIRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC151 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC151_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC151_1,RLOC1511731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC151 ;

RLOC152_1 = max(min((INVRETDNR * (1 - INDPLAF) + INVRETDNRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOC152 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOC152_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOC152_1,RLOC1521731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOC152 ;

RLOCHET_1 = max(min((INVRETET * (1 - INDPLAF) + INVRETETA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHET =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHET_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHET_1,RLOCHET1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHET ;

RLOCHEO_1 = max(min((INVRETEO * (1 - INDPLAF) + INVRETEOA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHEO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHEO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHEO_1,RLOCHEO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHEO ;

RLOCHES_1 = max(min((INVRETES * (1 - INDPLAF) + INVRETESA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHES =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHES_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHES_1,RLOCHES1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHES ;

RLOCHEN_1 = max(min((INVRETEN * (1 - INDPLAF) + INVRETENA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHEN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHEN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHEN_1,RLOCHEN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHEN ;

RLOCHEP_1 = max(min((INVRETEP * (1 - INDPLAF) + INVRETEPA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHEP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHEP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHEP_1,RLOCHEP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHEP ;

RLOCHEU_1 = max(min((INVRETEU * (1 - INDPLAF) + INVRETEUA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHEU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHEU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHEU_1,RLOCHEU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHEU ;

RLOCHER_1 = max(min((INVRETER * (1 - INDPLAF) + INVRETERA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHER =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHER_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHER_1,RLOCHER1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHER ;

RLOCHEW_1 = max(min((INVRETEW * (1 - INDPLAF) + INVRETEWA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHEW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHEW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHEW_1,RLOCHEW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHEW ;

RLOCHFT_1 = max(min((INVRETFT * (1 - INDPLAF) + INVRETFTA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFT_1,RLOCHFT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFT ;

RLOCHFO_1 = max(min((INVRETFO * (1 - INDPLAF) + INVRETFOA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFO_1,RLOCHFO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFO ;

RLOCHFS_1 = max(min((INVRETFS * (1 - INDPLAF) + INVRETFSA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFS_1,RLOCHFS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFS ;

RLOCHFN_1 = max(min((INVRETFN * (1 - INDPLAF) + INVRETFNA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFN_1,RLOCHFN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFN ;

RLOCHFP_1 = max(min((INVRETFP * (1 - INDPLAF) + INVRETFPA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFP_1,RLOCHFP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFP ;

RLOCHFU_1 = max(min((INVRETFU * (1 - INDPLAF) + INVRETFUA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFU_1,RLOCHFU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFU ;

RLOCHFR_1 = max(min((INVRETFR * (1 - INDPLAF) + INVRETFRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFR_1,RLOCHFR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFR ;

RLOCHFW_1 = max(min((INVRETFW * (1 - INDPLAF) + INVRETFWA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFW_1,RLOCHFW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFW ;

RLOCHETR_1 = max(min((INVRETETR * (1 - INDPLAF) + INVRETETRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHETR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHETR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHETR_1,RLOCHETR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHETR ;

RLOCHEOR_1 = max(min((INVRETEOR * (1 - INDPLAF) + INVRETEORA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHEOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHEOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHEOR_1,RLOCHEOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHEOR ;

RLOCHESR_1 = max(min((INVRETESR * (1 - INDPLAF) + INVRETESRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHESR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHESR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHESR_1,RLOCHESR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHESR ;

RLOCHENR_1 = max(min((INVRETENR * (1 - INDPLAF) + INVRETENRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHENR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHENR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHENR_1,RLOCHENR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHENR ;

RLOCHFTR_1 = max(min((INVRETFTR * (1 - INDPLAF) + INVRETFTRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFTR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFTR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFTR_1,RLOCHFTR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFTR ;

RLOCHFOR_1 = max(min((INVRETFOR * (1 - INDPLAF) + INVRETFORA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFOR_1,RLOCHFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFOR ;

RLOCHFSR_1 = max(min((INVRETFSR * (1 - INDPLAF) + INVRETFSRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFSR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFSR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFSR_1,RLOCHFSR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHFSR ;

RLOCHFNR_1 = max(min((INVRETFNR * (1 - INDPLAF) + INVRETFNRA * INDPLAF) , RRILOC - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHFNR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHFNR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHFNR_1,RLOCHFNR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

regle 8404 :
application : iliad  ;

VARTMP1 = 0 ;
RRIRENT = RRISUP - RDOMSOC1 - RLOGSOC - RCOLENT ;

RLOCHGT_1 = max(min((INVRETGT * (1 - INDPLAF) + INVRETGTA * INDPLAF) , RRIRENT - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHGT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHGT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHGT_1,RLOCHGT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHGT ;

RLOCHGS_1 = max(min((INVRETGS * (1 - INDPLAF) + INVRETGSA * INDPLAF) , RRIRENT - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHGS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHGS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHGS_1,RLOCHGS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHGS ;

RLOCHGU_1 = max(min((INVRETGU * (1 - INDPLAF) + INVRETGUA * INDPLAF) , RRIRENT - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHGU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHGU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHGU_1,RLOCHGU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHGU ;

RLOCHGW_1 = max(min((INVRETGW * (1 - INDPLAF) + INVRETGWA * INDPLAF) , RRIRENT - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHGW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHGW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHGW_1,RLOCHGW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHGW ;

RLOCHGTR_1 = max(min((INVRETGTR * (1 - INDPLAF) + INVRETGTRA * INDPLAF) , RRIRENT - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHGTR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHGTR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHGTR_1,RLOCHGTR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RLOCHGTR ;

RLOCHGSR_1 = max(min((INVRETGSR * (1 - INDPLAF) + INVRETGSRA * INDPLAF) , RRIRENT - VARTMP1) , 0) * (1 - V_CNR) ;
RLOCHGSR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RLOCHGSR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RLOCHGSR_1,RLOCHGSR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0;

