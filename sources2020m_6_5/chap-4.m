#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2021]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2021 
#au titre des revenus perçus en 2020. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

  ####   #    #    ##    #####      #     #####  #####   ######      #    
 #    #  #    #   #  #   #    #     #       #    #    #  #           #    #
 #       ######  #    #  #    #     #       #    #    #  #####       #    #
 #       #    #  ######  #####      #       #    #####   #           ######
 #    #  #    #  #    #  #          #       #    #   #   #                #
  ####   #    #  #    #  #          #       #    #    #  ######           #
regle 401000:
application : bareme , iliad ;


IRB = IAMD2 ; 
IRBINR = IAMD2INR ; 
IRB2 = IAMD2 + TAXASSUR + IPCAPTAXTOT + CHRAPRES ;

regle 401020:
application : bareme , iliad ;


IAMD1 = IAD11 + IBATMARG + ITP + PVMTS + REI + AVFISCOPTER + VERSLIB + AUTOVERSSUP + IMPETAL19 + TAXASSUR + IPCAPTAXTOT + CHRAPRES + BRAS + NRINET + IMPRET + CODZRA ;

regle 401023:
application : bareme , iliad ;

IRBAF = IAD11  + VERSLIB + ITP + PVMTS + REI + AUTOVERSSUP;

regle 401025:
application : bareme , iliad ;

IAMD2 = IAD11 + IBATMARG + ITP + PVMTS + REI + AVFISCOPTER + VERSLIB + AUTOVERSSUP + IMPETAL19 ;
IAMD2INR = IAD11INR + IBATMARG + ITP + PVMTS + REI + AVFISCOPTER + VERSLIB + AUTOVERSSUP + IMPETAL19 ;
IAMD2TH = positif_ou_nul(IAMD2 - SEUIL_61) * IAMD2 ;


regle 401030:
application : bareme , iliad ;
IAVIM2 = IAMD1 + PTOT + PTAXA + PPCAP + PHAUTREV ;

regle 401060:
application : iliad ;

DOMITPD = arr(BN1 + SPEPV + BI12F + BA1) * (TX896/100) * positif(V_EAD);
DOMITPG = arr(BN1 + SPEPV + BI12F + BA1) * (TX768/100) * positif(V_EAG);
DOMAVTD = arr((BN1 + SPEPV + BI12F + BA1) * (TX128 - TX896)/100) * positif(V_EAD);
DOMAVTG = arr((BN1 + SPEPV + BI12F + BA1) * (TX128 - TX768)/100) * positif(V_EAG);
DOMAVTO = DOMAVTD + DOMAVTG;
DOMABDB = max(PLAF_RABDOM - ABADO , 0) * positif(V_EAD)
          + max(PLAF_RABGUY - ABAGU , 0) * positif(V_EAG);
DOMDOM = max(DOMAVTO - DOMABDB , 0) * positif(V_EAD + V_EAG);
ITP = 
        arr((BPTP4 * TX30/100) 
       +  DOMITPD + DOMITPG
       +(BPTP3 * TX128/100)
       + (BPTP10 * TX10/100)
       + (BPTP40 * TX41/100)
       + DOMDOM * positif(V_EAD + V_EAG)
       + (BPTP18 * TX18/100)
       + (BPTP5 * TX128/100) * positif(FLAG_EXIT)
       + (BPTPSJ * TX19/100)
       + (BPTPWI * TX24/100)
       + (BPTPWJ * TX19/100)
       + (BPTPPI * TX50/100)
       + IMPOT75
       + (BPTP24 * TX24/100)
	  )
       * (1 - positif(present(TAX1649)+present(RE168))) ; 

regle 401070:
application : iliad ;


REVTP = BPTP4 + BPTP3 + BPTP10 + BPTP40 + BPTP18 + (BPTP5 * positif(FLAG_EXIT)) + BPTPSJ + BPTPWI + BPTPWJ + BPTPPI + RCMIMPTR + BPTP24;

regle 401080:
application : iliad ;


BPTP3 =(BTP3A*(1 - positif(V_EAD + V_EAG)) + (1-positif(COD2OP))*(BTPM3VG+BTPM3UA+BPTPSB+BTPM3TJ+COD3SZ+RCMIMPTN+BPTPVT)+COD3AN)*(1-positif(present(TAX1649)+present(RE168)));


BPTP10 = PVINDUSPBIC + PVINDUSNPBIC  + PVINDUSPBNC + PVINDUSBA ;
 
regle 401085:
application : iliad ;

BTP3A =(BN1 + SPEPV + BI12F + BA1 );
BPTPD = BTP3A * positif(V_EAD)*(1-positif(present(TAX1649)+present(RE168)));
BPTPG = BTP3A * positif(V_EAG)*(1-positif(present(TAX1649)+present(RE168)));
BTP3G = BPVRCM;


BTPM3VG =(1-positif(COD2OP))*BPVRCM * (1-positif(present(TAX1649)+present(RE168)))  
                      + positif (COD2OP)* (max(0,(BPVRCM-COD3SG))) * (1-positif(present(TAX1649)+present(RE168))); 

BTPM3UA =(1-positif(COD2OP))*(max(0,(COD3UA-ABDETPLUS)))*(1-positif(present(TAX1649)+present(RE168)))
         +(positif(COD2OP)) * ((max(0,(COD3UA-ABDETPLUS-COD3SL)))* (1-positif(present(TAX1649)+present(RE168))));

BTPM3TJ =(1-positif(COD2OP))*(max(0,(COD3TJ-COD3TK)))*(1-positif(present(TAX1649)+present(RE168)))
         +(positif(COD2OP)) * ((max(0,(COD3TJ-COD3TK)))* (1-positif(present(TAX1649)+present(RE168))));

BPTPWI = COD3WI * (1-positif(present(TAX1649)+present(RE168))) ;

BPTPWJ = COD3WJ * (1-positif(present(TAX1649)+present(RE168))) ;

BPTPVT = GAINPEA * (1-positif(COD2OP)) *(1-positif(present(TAX1649)+present(RE168)));

BPTP18 = BPV18V * (1-positif(present(TAX1649)+present(RE168))) ;

BPTP4 = (BPCOPTV + BPVSK) * (1 - positif(present(TAX1649) + present(RE168))) ;
BPTP4I = BPCOPTV * (1 - positif(present(TAX1649) + present(RE168))) ;

BPTP40 = BPV40V * (1-positif(present(TAX1649)+present(RE168))) ;

BPTP5 = (PVIMPOS * (1-positif(present(TAX1649)+present(RE168))) + PVSURSI) * (1-present(COD2OP));

BPTPSJ = BPVSJ * (1-positif(present(TAX1649)+present(RE168))) ;
BPTPSK = BPVSK * (1-positif(present(TAX1649)+present(RE168)));



BPTPSB = PVTAXSB * (1-positif(present(TAX1649)+present(RE168))) ;

BTPM3SB  = BPTPSB *(1-positif(present(TAX1649)+present(RE168))) ;

BTPM3SZ = COD3SZ * (1-positif(present(TAX1649)+present(RE168)));

BPTPPI = COD3PI  * (1-positif(present(TAX1649)+present(RE168))) ;
BPTP19 = BPVSJ * (1 - positif(present(TAX1649) + present(RE168))) ;

BPTP24 = RCM2FA * (1 - positif(present(TAX1649) + present(RE168))) * (1 - V_CNR) ;
ITPRCM =( arr(BPTP24 * TX24/100));

BPT19 = BPTP19 + BPTPWJ ;

BPT24 = BPTP24 + BPTPWI ;

regle 401090:
application : iliad ;


REI = IPREP + IPPRICORSE ;

regle 401100:
application : bareme , iliad ;

IAD11 = ( max(0,IDOM11-DEC11-RED) *(1-positif(V_CNR))
        + positif(V_CNR) *max(0 , IDOM11 - RED) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * (IDOM16 - DEC6); 
IAD11INR = ( max(0,IDOM11-DEC11-RED_1) *(1-positif(V_CNR))
        + positif(V_CNR) *max(0 , IDOM11 - RED_1) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * (IDOM16 - DEC6); 
IAD13 = ( max(0,IDOM13-DEC13) *(1-positif(V_CNR))
        + positif(V_CNR) *max(0 , IDOM13 - RED3WG) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * IDOM16 ;

regle 401105:
application : bareme , iliad ;

3WBHORBAR = arr(PVIMPOS * positif(1-COD2OP) * TX128/100) * (1 - V_CNR);
3WAHORBAR = arr(PVSURSI * positif(1-COD2OP) * TX128/100) * (1 - V_CNR);
regle 401112:
application : bareme , iliad ;

IREXITI = present(FLAG_EXIT) * abs(ID11 - V_ID113WB) * positif(positif(PVIMPOS)+positif(CODRWB)) * (1 - V_CNR) * positif(COD2OP) + 3WBHORBAR;

IREXITS = (
           abs(V_ID113WA-V_ID113WB) * positif(positif(PVIMPOS)+positif(CODRWB))
         + abs(V_ID113WA-ID11) * (1-positif(positif(PVIMPOS)+positif(CODRWB)))
          ) 
          * present(FLAG_EXIT) * positif(positif(PVSURSI)+positif(CODRWA))
          * (1 - V_CNR) * positif(COD2OP) + 3WAHORBAR;


regle 401113:
application : bareme , iliad ;
EXITTAX3 = (V_ID113WB * positif(positif(PVIMPOS)+positif(CODRWB)) + NAPTIR * positif(positif(PVSURSI)+positif(CODRWA)) * (1-positif(positif(PVIMPOS)+positif(CODRWB)))) * (1 - V_CNR) ;


PVCREA = PVSURSI + CODRWA ;

PVCREB = PVIMPOS + CODRWB ;
regle 401115:
application : bareme , iliad ;



PVMTS =( COD3WR) ;

regle 401120:
application : bareme , iliad ;

IREXIT = IREXITI + IREXITS;
regle 401140:
application : bareme , iliad ;


DEC11 = min(max(arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - (IDOM11 * 45.25/100)) , 0) , IDOM11) * (1 - V_CNR) ;

DEC12 = min(max(arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - (IDOM12 * 45.25/100)) , 0) , IDOM12) * (1 - V_CNR) ;

DEC13 = min(max(arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - (IDOM13 * 45.25/100)) , 0) , IDOM13) * (1 - V_CNR) ;

DEC6 = min(max(arr((SEUIL_DECOTE1 * (1 - BOOL_0AM)) + (SEUIL_DECOTE2 * BOOL_0AM) - (IDOM16 * 45.25/100)) , 0) , IDOM16) * (1 - V_CNR) ;

regle 401150:
application : iliad ;

ART1731BIS = positif(positif(SOMMERI_2+SOMMEBIC_2+SOMMEBA_2+SOMMEBNC_2+SOMMELOC_2+SOMMERF_2+SOMMERCM_2+SOMMEMOND_2+SOMMEGLOBAL_2) + PREM8_11) ;

regle 401160:
application : iliad ;

      
RED = RCOTFOR + RREPA + RDIFAGRI + RPRESSE + RFORET + RFIPDOM 
      + RFIPC + RCINE + RRESTIMO 
      + RSOCREPR + RRPRESCOMP + RHEBE + RSURV + RINNO + RSOUFIP
      + RRIRENOV + RLOGDOM + RCOMP + RRETU + RDONS + CRDIE
      + RDUFREP + RPINELTOT + RNORMTOT + RNOUV + RPENTOT + RRSOFON 
      + RFOR + RTOURREP + RTOUREPA + RREHAB + RRESTREP + RRESTIMO1
      + RCELTOT + RLOCNPRO 
      + RDOMSOC1 + RLOGSOC + RCOLENT + RLOCENT;
RED_1 = RCOTFOR_1 + RREPA_1 + RDIFAGRI_1 + RPRESSE_1 + RFORET_1 + RFIPDOM_1 
      + RFIPC_1 + RCINE_1 + RRESTIMO_1 
      + RSOCREPR_1 + RRPRESCOMP_1 + RHEBE_1 + RSURV_1 + RINNO_1 + RSOUFIP_1
      + RRIRENOV_1 + RLOGDOM_1 + RCOMP_1 + RRETU_1 + RDONS_1 + CRDIE_1
      + ADUFREPFI_1 + ADUFREPFK_1 + ADUFREPFR_1 + ADUFREPFV_1 + ADUFREPFW_1 + ADUFREPFX_1 + ADUFREPFU_1
      + RPINELTOT_1 + RNORMTOT_1 + RNOUV_1 + RPENTOT_1 + RRSOFON_1
      + RFOR_1 + RTOURREP_1 + RTOUREPA_1 + RREHAB_1 + RRESTREP_1 + RRESTIMO1_1
      + RCELTOT_1 + RLOCNPRO_1 
      + RDOMSOC1_1 + RLOGSOC_1 + RCOLENT_1 + RLOCENT_1
       ;

REDTL = ASURV + ACOMP ;

CIMPTL = ATEC + TOTBGE ;


regle 401170:
application : bareme ;

RED = V_9UY ;

regle 401180:
application : iliad ;

DPRESSE = COD7MY + COD7MX ;

APRESSE_1 = (min(COD7MY , LIM5000 * (1 + BOOL_0AM)) + min(COD7MX , max(0 , LIM5000 * (1 + BOOL_0AM) - COD7MY))) * (1 - V_CNR) ;
APRESSE = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APRESSE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(APRESSE_1,APRESSE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RAPRESSE = arr(min(COD7MY , LIM5000 * (1 + BOOL_0AM)) * TX50/100 + min(COD7MX , max(0 , LIM5000 * (1 + BOOL_0AM) - COD7MY)) * TX30/100) * (1 - V_CNR) ;

RPRESSE_1 = max(min(RAPRESSE , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI) , 0) ;
RPRESSE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPRESSE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPRESSE_1,RPRESSE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401185:
application : iliad ;

DFORET = FORET ;

AFORET_1 = min(DFORET , LIM_FORET) * (1 - V_CNR) ;
AFORET = positif(null(V_IND_TRAIT-4)+COD9ZA) * (AFORET_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
         + (max(0,min(AFORET_1,AFORET1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RAFORET = arr(AFORET * TX_FORET/100) * (1 - V_CNR) ;

RFORET_1 = max(min(RAFORET , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE) , 0) ;
RFORET =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RFORET_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RFORET_1,RFORET1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401190:
application : iliad ;

DFIPDOM = FIPDOMCOM + COD7HL ;

AFIPDOM_1 = (min(FIPDOMCOM , LIMFIPDOM * (1 + BOOL_0AM)) + min(COD7HL , max(0 , LIMFIPDOM * (1 + BOOL_0AM) - FIPDOMCOM))) * (1 - V_CNR) ;
AFIPDOM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AFIPDOM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AFIPDOM_1,AFIPDOM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RFIPDOMCOM = arr(min(FIPDOMCOM , LIMFIPDOM * (1 + BOOL_0AM)) * TXFIPDOM/100 + min(COD7HL , max(0 , LIMFIPDOM * (1 + BOOL_0AM) - FIPDOMCOM)) * TX30/100) * (1 - V_CNR) ;

RFIPDOM_1 = max(min(RFIPDOMCOM , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RCINE) , 0) ;
RFIPDOM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RFIPDOM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RFIPDOM_1,RFIPDOM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401200:
application : iliad ;

DFIPC = FIPCORSE + COD7HM ;

AFIPC_1 = (min(FIPCORSE , LIM_FIPCORSE * (1 + BOOL_0AM)) + min(COD7HM , max(0 , LIM_FIPCORSE * (1 + BOOL_0AM) - FIPCORSE))) * (1 - V_CNR) ;
AFIPC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AFIPC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AFIPC_1,AFIPC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RFIPCORSE = arr(min(FIPCORSE , LIM_FIPCORSE * (1 + BOOL_0AM)) * TX_FIPCORSE/100 + min(COD7HM , max(0 , LIM_FIPCORSE * (1 + BOOL_0AM) - FIPCORSE)) * TX30/100) * (1 - V_CNR) ;

RFIPC_1 = max(min(RFIPCORSE , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RCINE-RFIPDOM) , 0) ;
RFIPC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RFIPC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RFIPC_1,RFIPC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401210:
application : iliad ;

BSURV = min(RDRESU , PLAF_RSURV + PLAF_COMPSURV * (EAC + V_0DN) + PLAF_COMPSURVQAR * (V_0CH + V_0DP)) * (1 - V_CNR) ;

RRS = arr(BSURV * TX_REDSURV / 100) * (1 - V_CNR) ;

DSURV = RDRESU ;

ASURV = positif(null(V_IND_TRAIT-4)+COD9ZA) * (BSURV) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(BSURV,BSURV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RSURV_1 = max(min(RRS , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC
			            -RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE ) , 0) ;
RSURV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSURV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSURV_1,RSURV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


regle 401220:
application : iliad ;


DCINE = COD7EN + CINE1 + CINE2 ;

ACINE_1 = max(0 , min(DCINE , min(arr(max(SOFIRNG,RNG) * TX_CINE3/100) , PLAF_CINE))) * (1 - V_CNR) ;
ACINE = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACINE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(ACINE_1,ACINE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RRCN1 = min(COD7EN , min(max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE)) ;
RRCN2 = min(CINE1 , max(min(max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE) - RRCN1 , 0)) ;
RRCN3 = min(CINE2 , max(min(max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE) - RRCN1 - RRCN2 , 0)) ;

RRCN = arr((RRCN1 * TX48/100) + (RRCN2 * TX_CINE1/100) + (RRCN3 * TX_CINE2/100)) * (1 - V_CNR) ;

RCINE_1 = max(min(RRCN , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI- RPRESSE - RFORET) , 0) ;
RCINE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCINE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCINE_1,RCINE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401230:
application : iliad ;


BSOUFIPFT = min(COD7FT , LIM_SOUFIP * (1 + BOOL_0AM)) ;
BSOUFIP = min(FFIP , max(0,LIM_SOUFIP * (1 + BOOL_0AM)-COD7FT)) ;

RFIP = arr(BSOUFIP * TX_REDFIP / 100+BSOUFIPFT * TX25 / 100) * (1 - V_CNR) ;

DSOUFIP = FFIP + COD7FT;

ASOUFIP_1 = (BSOUFIP + BSOUFIPFT) * (1 - V_CNR) ;
ASOUFIP = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ASOUFIP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ASOUFIP_1,ASOUFIP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RSOUFIP_1 = max(min(RFIP , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC
			   -RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO) , 0 ) ;
RSOUFIP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOUFIP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOUFIP_1,RSOUFIP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


regle 401240:
application : iliad ;


BRENOV = min(RIRENOV , PLAF_RENOV) * (1 - V_CNR) ;

RENOV = arr(BRENOV * TX_RENOV / 100) * (1 - V_CNR) ;

DRIRENOV = RIRENOV ;

ARIRENOV = positif(null(V_IND_TRAIT-4)+COD9ZA) * (BRENOV) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(BRENOV,BRENOV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RRIRENOV_1 = max(min(RENOV , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC-RCINE
			     -RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP) , 0 ) ;
RRIRENOV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRIRENOV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRIRENOV_1,RRIRENOV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


regle 401250:
application : iliad ;


NCOMP = max(1 , NBACT) * present(RDCOM) ;

DCOMP = RDCOM ;
ACOMP_1 = min(RDCOM , PLAF_FRCOMPTA * max(1 , NBACT)) * present(RDCOM) ;
ACOMP = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACOMP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(ACOMP_1,ACOMP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


regle 401260:
application : iliad ;

RCOMP_1 = max(min(ACOMP , RRI1 - RLOGDOM) , 0) ;
RCOMP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCOMP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCOMP_1,RCOMP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401265:
application : iliad ;

CONDPINEL = 1 - (null(2 - V_REGCO) + null(3 - V_REGCO) * V_INDVB31) * (1 - positif(COD7QH)) ;
CONDNORMD = 1 - (null(2 - V_REGCO) + null(3 - V_REGCO) * V_INDVB31) * (1 - positif(COD7QF)) ;

regle 401270:
application : iliad ;


ADUFREPFI_1 = (min(DUFLOFI , LIMREPDUF) * (1 - COD7QV) + DUFLOFI * COD7QV) * (1 - V_CNR) ;
ADUFREPFI = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFI_1,ADUFREPFI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADUFREPFK_1 = (min(DUFLOFK , LIMREPDUF) * (1 - COD7QV) + DUFLOFK * COD7QV) * (1 - V_CNR) ;
ADUFREPFK = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFK_1,ADUFREPFK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADUFREPFR_1 = (min(DUFLOFR , LIMREPDUF) * (1 - COD7QV) + DUFLOFR * COD7QV) * (1 - V_CNR) ;
ADUFREPFR = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFR_1,ADUFREPFR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADUFREPFV_1 = (min(DUFLOFV , LIMREPDUF) * (1 - COD7QV) + DUFLOFV * COD7QV) * (1 - V_CNR) ;
ADUFREPFV = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFV_1,ADUFREPFV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADUFREPFW_1 = (min(COD7FW , LIMREPDUF) * (1 - COD7QV) + COD7FW * COD7QV) * (1 - V_CNR) ;
ADUFREPFW = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFW_1,ADUFREPFW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADUFREPFX_1 = (min(COD7FX , LIMREPDUF) * (1 - COD7QV) + COD7FX * COD7QV) * (1 - V_CNR) ;
ADUFREPFX = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFX_1,ADUFREPFX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADUFREPFU_1 = (min(COD7FU , LIMREPDUF) * (1 - COD7QV) + COD7FU * COD7QV) * (1 - V_CNR) ;
ADUFREPFU = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADUFREPFU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ADUFREPFU_1,ADUFREPFU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DDUFREP = DUFLOFI + DUFLOFK + DUFLOFR + DUFLOFV + COD7FW + COD7FX + COD7FU ;
ADUFREP = ADUFREPFI + ADUFREPFK + ADUFREPFR + ADUFREPFV + ADUFREPFW + ADUFREPFX + ADUFREPFU ;

regle 401272:
application : iliad ;


APIREPBI_1 = (min(PINELBI , LIMREPPIN1) * (1 - COD7QV) + PINELBI * COD7QV) * (1 - V_CNR) ;
APIREPBI = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPBI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPBI_1,APIREPBI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPDI_1 = (min(PINELDI , LIMREPPIN2) * (1 - COD7QV) + PINELDI * COD7QV) * (1 - V_CNR) ;
APIREPDI = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPDI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPDI_1,APIREPDI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPBZ_1 = (min(PINELBZ , LIMREPPIN4) * (1 - COD7QV) + PINELBZ * COD7QV) * (1 - V_CNR) ;
APIREPBZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPBZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPBZ_1,APIREPBZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPCZ_1 = (min(PINELCZ , LIMREPPIN4) * (1 - COD7QV) + PINELCZ * COD7QV) * (1 - V_CNR) ;
APIREPCZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPCZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPCZ_1,APIREPCZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPDZ_1 = (min(PINELDZ , LIMREPPIN5) * (1 - COD7QV) + PINELDZ * COD7QV) * (1 - V_CNR) ;
APIREPDZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPDZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPDZ_1,APIREPDZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPEZ_1 = (min(PINELEZ , LIMREPPIN5) * (1 - COD7QV) + PINELEZ * COD7QV) * (1 - V_CNR) ;
APIREPEZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPEZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPEZ_1,APIREPEZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPQZ_1 = (min(PINELQZ , LIMREPPIN4) * (1 - COD7QV) + PINELQZ * COD7QV) * (1 - V_CNR) ;
APIREPQZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPQZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPQZ_1,APIREPQZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRZ_1 = (min(PINELRZ , LIMREPPIN4) * (1 - COD7QV) + PINELRZ * COD7QV) * (1 - V_CNR) ;
APIREPRZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRZ_1,APIREPRZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPSZ_1 = (min(PINELSZ , LIMREPPIN5) * (1 - COD7QV) + PINELSZ * COD7QV) * (1 - V_CNR) ;
APIREPSZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPSZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPSZ_1,APIREPSZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPTZ_1 = (min(PINELTZ , LIMREPPIN5) * (1 - COD7QV) + PINELTZ * COD7QV) * (1 - V_CNR) ;
APIREPTZ = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPTZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPTZ_1,APIREPTZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRA_1 = (min(COD7RA , LIMREPPIN4) * (1 - COD7QV) + COD7RA * COD7QV) * (1 - V_CNR) ;
APIREPRA = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRA_1,APIREPRA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRB_1 = (min(COD7RB , LIMREPPIN4) * (1 - COD7QV) + COD7RB * COD7QV) * (1 - V_CNR) ;
APIREPRB = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRB_1,APIREPRB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRC_1 = (min(COD7RC , LIMREPPIN5) * (1 - COD7QV) + COD7RC * COD7QV) * (1 - V_CNR) ;
APIREPRC = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRC_1,APIREPRC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRD_1 = (min(COD7RD , LIMREPPIN5) * (1 - COD7QV) + COD7RD * COD7QV) * (1 - V_CNR) ;
APIREPRD = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRD_1,APIREPRD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRE_1 = (min(COD7RE , LIMREPPIN4) * (1 - COD7QV) + COD7RE * COD7QV) * (1 - V_CNR) ;
APIREPRE = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRE_1,APIREPRE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRF_1 = (min(COD7RF , LIMREPPIN4) * (1 - COD7QV) + COD7RF * COD7QV) * (1 - V_CNR) ;
APIREPRF = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRF_1,APIREPRF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRG_1 = (min(COD7RG , LIMREPPIN5) * (1 - COD7QV) + COD7RG * COD7QV) * (1 - V_CNR) ;
APIREPRG = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRG_1,APIREPRG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPRH_1 = (min(COD7RH , LIMREPPIN5) * (1 - COD7QV) + COD7RH * COD7QV) * (1 - V_CNR) ;
APIREPRH = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPRH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPRH_1,APIREPRH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPJM_1 = (min(COD7JM , LIMREPPIN4) * (1 - COD7QV) + COD7JM * COD7QV) * CONDPINEL ;
APIREPJM = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPJM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPJM_1,APIREPJM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPKM_1 = (min(COD7KM , LIMREPPIN4) * (1 - COD7QV) + COD7KM * COD7QV) * CONDPINEL ;
APIREPKM = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPKM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPKM_1,APIREPKM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPLM_1 = (min(COD7LM , LIMREPPIN5) * (1 - COD7QV) + COD7LM * COD7QV) * CONDPINEL ;
APIREPLM = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPLM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPLM_1,APIREPLM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

APIREPMM_1 = (min(COD7MM , LIMREPPIN5) * (1 - COD7QV) + COD7MM * COD7QV) * CONDPINEL ;
APIREPMM = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APIREPMM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(APIREPMM_1,APIREPMM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DPIREP = PINELBI + PINELDI + PINELBZ + PINELCZ + PINELDZ + PINELEZ + PINELQZ + PINELRZ + PINELSZ + PINELTZ 
         + COD7RA + COD7RB + COD7RC + COD7RD + COD7RE + COD7RF + COD7RG + COD7RH + COD7JM + COD7KM + COD7LM + COD7MM ;
APIREP = APIREPBI + APIREPDI + APIREPBZ + APIREPCZ + APIREPDZ + APIREPEZ + APIREPQZ + APIREPRZ + APIREPSZ + APIREPTZ 
         + APIREPRA + APIREPRB + APIREPRC + APIREPRD + APIREPRE + APIREPRF + APIREPRG + APIREPRH + APIREPJM + APIREPKM + APIREPLM + APIREPMM ;

regle 401274:
application : iliad ;


ANORMJA_1 = min(COD7JA , LIMREPPIN4) * CONDNORMD ;
ANORMJA = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ANORMJA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ANORMJA_1,ANORMJA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ANORMJB_1 = min(COD7JB , LIMREPPIN4) * CONDNORMD ;
ANORMJB = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ANORMJB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ANORMJB_1,ANORMJB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ANORMJC_1 = min(COD7JC , LIMREPPIN5) * CONDNORMD ;
ANORMJC = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ANORMJC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ANORMJC_1,ANORMJC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ANORMJD_1 = min(COD7JD , LIMREPPIN5) * CONDNORMD ;
ANORMJD = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ANORMJD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ANORMJD_1,ANORMJD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DNORMREP = COD7JA + COD7JB + COD7JC + COD7JD ;
ANORMREP = ANORMJA + ANORMJB + ANORMJC + ANORMJD ;

regle 401276:
application : iliad ;


BAS7QP = min(PINELQP , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QP ;

BAS7QU = min(COD7QU , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QU ;

BAS7QQ = min(COD7QQ , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QQ ;

BAS7ND = min(COD7ND , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7ND ;

BAS7QD = min(COD7QD , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QD ;

BAS7NH = min(COD7NH , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7NH ;

BAS7QO = min(PINELQO , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QO ;

BAS7QT = min(COD7QT , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QT ;

BAS7QY = min(COD7QY , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QY ;

BAS7NC = min(COD7NC , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7NC ;

BAS7QC = min(COD7QC , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QC ;

BAS7NG = min(COD7NG , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7NG ;

BAS7QN = min(PINELQN , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QN ;

BAS7QS = min(COD7QS , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QS ;

BAS7QX = min(COD7QX , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QX ;

BAS7NB = min(COD7NB , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7NB ;

BAS7QB = min(COD7QB , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QB ;

BAS7NF = min(COD7NF , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7NF ;

BAS7QM = min(PINELQM , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QM ;

BAS7QR = min(COD7QR , LIMDUFLO - VARTMP1) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + BAS7QR ;

BAS7QW = min(COD7QW , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QW ;

BAS7NA = min(COD7NA , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = VARTMP1 + BAS7NA ;

BAS7QA = min(COD7QA , LIMDUFLO - VARTMP1) * CONDPINEL ;
VARTMP1 = VARTMP1 + BAS7QA ;

BAS7NE = min(COD7NE , LIMDUFLO - VARTMP1) * CONDNORMD ;
VARTMP1 = 0 ;

BAS7RR = min(COD7RR , LIMDUFLO) * (1 - V_CNR) ;
BAS7RS = min(COD7RS , LIMDUFLO - BAS7RR) * (1 - V_CNR) ;

DNORMAN = COD7NA + COD7NB + COD7NC + COD7ND + COD7NE + COD7NF + COD7NG + COD7NH ;

ANORMAN = arr(BAS7NA/6) + arr(BAS7NB/9) + arr(BAS7NC/6) + arr(BAS7ND/9) + arr(BAS7NE/6) + arr(BAS7NF/9) + arr(BAS7NG/6) + arr(BAS7NH/9) ;

RNORABCD = arr(arr(BAS7NA/6) * TX12/100) + arr(arr(BAS7NB/9) * TX18/100) + arr(arr(BAS7NC/6) * TX23/100) + arr(arr(BAS7ND/9) * TX29/100)
           + arr(arr(BAS7NE/6) * TX12/100) + arr(arr(BAS7NF/9) * TX18/100) + arr(arr(BAS7NG/6) * TX23/100) + arr(arr(BAS7NH/9) * TX29/100) ;

APIQAB = arr(BAS7QB/9) + arr(BAS7QA/6) ;

APIQCD = arr(BAS7QD/9) + arr(BAS7QC/6) ;

APIQQY = arr(BAS7QQ/9) + arr(BAS7QY/6) ;

APIQWX = arr(BAS7QX/9) + arr(BAS7QW/6) ;

APIQTU = arr(BAS7QU/9) + arr(BAS7QT/6) ;

APIQRS = arr(BAS7QS/9) + arr(BAS7QR/6) ;

APIQOP = arr(BAS7QP/9) + arr(BAS7QO/6) ;

APIQMN = arr(BAS7QN/9) + arr(BAS7QM/6) ;

DPINEL = COD7QA + COD7QB + COD7QC + COD7QD + COD7QQ + COD7QW + COD7QX + COD7QY + COD7QT + COD7QU + COD7QR + COD7QS + PINELQO + PINELQP + PINELQM + PINELQN ;
APINEL = APIQAB + APIQCD + APIQQY + APIQWX + APIQTU + APIQRS + APIQOP + APIQMN ;



RPINABCD = arr(arr(BAS7QD/9) * TX29/100) + arr(arr(BAS7QC/6) * TX23/100) + arr(arr(BAS7QB/9) * TX18/100) + arr(arr(BAS7QA/6) * TX12/100) + arr(arr(BAS7QQ/9) * TX29/100) + arr(arr(BAS7QY/6) * TX23/100)
           + arr(arr(BAS7QX/9) * TX18/100) + arr(arr(BAS7QW/6) * TX12/100) + arr(arr(BAS7QU/9) * TX29/100) + arr(arr(BAS7QT/6) * TX23/100) + arr(arr(BAS7QS/9) * TX18/100) + arr(arr(BAS7QR/6) * TX12/100)
	   + arr(arr(BAS7QP/9) * TX29/100) + arr(arr(BAS7QO/6) * TX23/100) + arr(arr(BAS7QN/9) * TX18/100) + arr(arr(BAS7QM/6) * TX12/100) ;

DPIRRS = COD7RR + COD7RS ;

APIRRS = arr(BAS7RR/3) + arr(BAS7RS/3) ;

RPINRRS = arr(arr(BAS7RR/3) * TX06/100) + arr(arr(BAS7RS/3) * TX06/100) ;

regle 401280:
application : iliad ;


RRI1DUPI = RRI1 - RLOGDOM - RCOMP - RRETU - RDONS - CRDIE ;
VARTMP1 = 0 ;

regle 401282:
application : iliad ;

RDUFREP_1 = max(0 , min(ADUFREP , RRI1DUPI)) ;
RDUFREP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RDUFREP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RDUFREP_1,RDUFREP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RDUFREP ;

RPIREPBI_1 = max(0 , min(APIREPBI , RRI1DUPI - VARTMP1)) ;
RPIREPBI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPBI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPBI_1,RPIREPBI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPBI ;

RPIREPDI_1 = max(0 , min(APIREPDI , RRI1DUPI - VARTMP1)) ;
RPIREPDI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPDI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPDI_1,RPIREPDI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPDI ;

RPIREPBZ_1 = max(0 , min(APIREPBZ , RRI1DUPI - VARTMP1)) ;
RPIREPBZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPBZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPBZ_1,RPIREPBZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPBZ ;

RPIREPCZ_1 = max(0 , min(APIREPCZ , RRI1DUPI - VARTMP1)) ;
RPIREPCZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPCZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPCZ_1,RPIREPCZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPCZ ;

RPIREPDZ_1 = max(0 , min( APIREPDZ , RRI1DUPI - VARTMP1)) ;
RPIREPDZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPDZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPDZ_1,RPIREPDZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPDZ ;

RPIREPEZ_1 = max(0 , min(APIREPEZ , RRI1DUPI - VARTMP1)) ;
RPIREPEZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPEZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPEZ_1,RPIREPEZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPEZ ;

RPIREPQZ_1 = max(0 , min(APIREPQZ , RRI1DUPI - VARTMP1)) ;
RPIREPQZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPQZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPQZ_1,RPIREPQZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPQZ ;

RPIREPRZ_1 = max(0 , min(APIREPRZ , RRI1DUPI - VARTMP1)) ;
RPIREPRZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRZ_1,RPIREPRZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRZ ;

RPIREPSZ_1 = max(0 , min(APIREPSZ , RRI1DUPI - VARTMP1)) ;
RPIREPSZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPSZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPSZ_1,RPIREPSZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPSZ ;

RPIREPTZ_1 = max(0 , min(APIREPTZ , RRI1DUPI - VARTMP1)) ;
RPIREPTZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPTZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPTZ_1,RPIREPTZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPTZ ;

RPIREPRA_1 = max(0 , min(APIREPRA , RRI1DUPI - VARTMP1)) ;
RPIREPRA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRA_1,RPIREPRA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRA ;

RPIREPRB_1 = max(0 , min(APIREPRB , RRI1DUPI - VARTMP1)) ;
RPIREPRB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRB_1,RPIREPRB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRB ;

RPIREPRC_1 = max(0 , min(APIREPRC , RRI1DUPI - VARTMP1)) ;
RPIREPRC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRC_1,RPIREPRC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRC ;

RPIREPRD_1 = max(0 , min(APIREPRD , RRI1DUPI - VARTMP1)) ;
RPIREPRD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRD_1,RPIREPRD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRD ;

RPIREPRE_1 = max(0 , min(APIREPRE , RRI1DUPI - VARTMP1)) ;
RPIREPRE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRE_1,RPIREPRE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRE ;

RPIREPRF_1 = max(0 , min(APIREPRF , RRI1DUPI - VARTMP1)) ;
RPIREPRF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRF_1,RPIREPRF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRF ;

RPIREPRG_1 = max(0 , min(APIREPRG , RRI1DUPI - VARTMP1)) ;
RPIREPRG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRG_1,RPIREPRG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRG ;

RPIREPRH_1 = max(0 , min(APIREPRH , RRI1DUPI - VARTMP1)) ;
RPIREPRH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPRH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPRH_1,RPIREPRH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPRH ;

RPIREPJM_1 = max(0 , min(APIREPJM , RRI1DUPI - VARTMP1)) ;
RPIREPJM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPJM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPJM_1,RPIREPJM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPJM ;

RNORMJA_1 = max(0 , min(ANORMJA , RRI1DUPI - VARTMP1)) ;
RNORMJA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RNORMJA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RNORMJA_1,RNORMJA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RNORMJA ;

RPIREPKM_1 = max(0 , min(APIREPKM , RRI1DUPI - VARTMP1)) ;
RPIREPKM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPKM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPKM_1,RPIREPKM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPKM ;

RNORMJB_1 = max(0 , min(ANORMJB , RRI1DUPI - VARTMP1)) ;
RNORMJB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RNORMJB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RNORMJB_1,RNORMJB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RNORMJB ;

RPIREPLM_1 = max(0 , min(APIREPLM , RRI1DUPI - VARTMP1)) ;
RPIREPLM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPLM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPLM_1,RPIREPLM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPLM ;

RNORMJC_1 = max(0 , min(ANORMJC , RRI1DUPI - VARTMP1)) ;
RNORMJC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RNORMJC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RNORMJC_1,RNORMJC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RNORMJC ;

RPIREPMM_1 = max(0 , min(APIREPMM , RRI1DUPI - VARTMP1)) ;
RPIREPMM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPIREPMM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPIREPMM_1,RPIREPMM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPIREPMM ;

RNORMJD_1 = max(0 , min(ANORMJD , RRI1DUPI - VARTMP1)) ;
RNORMJD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RNORMJD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RNORMJD_1,RNORMJD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RNORMJD ;

RPIQP_1 = max(0 , min(arr(arr(BAS7QP/9) * TX29/100) , RRI1DUPI - VARTMP1)) ;
RPIQP = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQP_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQP_1 , RPIQP1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQP ;

RPIQU_1 = max(0 , min(arr(arr(BAS7QU/9) * TX29/100) , RRI1DUPI - VARTMP1)) ;
RPIQU = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQU_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQU_1 , RPIQU1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQU ;

RPIQQ_1 = max(0 , min(arr(arr(BAS7QQ/9) * TX29/100) , RRI1DUPI - VARTMP1)) ;
RPIQQ = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQQ_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQQ_1 , RPIQQ1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQQ ;

RNOND_1 = max(0 , min(arr(arr(BAS7ND/9) * TX29/100) , RRI1DUPI - VARTMP1)) ;
RNOND = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNOND_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNOND_1 , RNOND1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNOND ;

RPIQD_1 = max(0 , min(arr(arr(BAS7QD/9) * TX29/100) , RRI1DUPI - VARTMP1)) ;
RPIQD = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQD_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQD_1 , RPIQD1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQD ;

RNONH_1 = max(0 , min(arr(arr(BAS7NH/9) * TX29/100) , RRI1DUPI - VARTMP1)) ;
RNONH = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONH_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONH_1 , RNONH1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONH ;

RPIQO_1 = max(0 , min(arr(arr(BAS7QO/6) * TX23/100) , RRI1DUPI - VARTMP1)) ;
RPIQO = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQO_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQO_1 , RPIQO1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQO ;

RPIQT_1 = max(0 , min(arr(arr(BAS7QT/6) * TX23/100) , RRI1DUPI - VARTMP1)) ;
RPIQT = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQT_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQT_1 , RPIQT1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQT ;

RPIQY_1 = max(0 , min(arr(arr(BAS7QY/6) * TX23/100) , RRI1DUPI - VARTMP1)) ;
RPIQY = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQY_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQY_1 , RPIQY1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQY ;

RNONC_1 = max(0 , min(arr(arr(BAS7NC/6) * TX23/100) , RRI1DUPI - VARTMP1)) ;
RNONC = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONC_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONC_1 , RNONC1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONC ;

RPIQC_1 = max(0 , min(arr(arr(BAS7QC/6) * TX23/100) , RRI1DUPI - VARTMP1)) ;
RPIQC = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQC_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQC_1 , RPIQC1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQC ;

RNONG_1 = max(0 , min(arr(arr(BAS7NG/6) * TX23/100) , RRI1DUPI - VARTMP1)) ;
RNONG = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONG_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONG_1 , RNONG1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONG ;

RPIQN_1 = max(0 , min(arr(arr(BAS7QN/9) * TX18/100) , RRI1DUPI - VARTMP1)) ;
RPIQN = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQN_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQN_1 , RPIQN1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQN ;

RPIQS_1 = max(0 , min(arr(arr(BAS7QS/9) * TX18/100) , RRI1DUPI - VARTMP1)) ;
RPIQS = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQS_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQS_1 , RPIQS1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQS ;

RPIQX_1 = max(0 , min(arr(arr(BAS7QX/9) * TX18/100) , RRI1DUPI - VARTMP1)) ;
RPIQX = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQX_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQX_1 , RPIQX1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQX ;

RNONB_1 = max(0 , min(arr(arr(BAS7NB/9) * TX18/100) , RRI1DUPI - VARTMP1)) ;
RNONB = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONB_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONB_1 , RNONB1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONB ;

RPIQB_1 = max(0 , min(arr(arr(BAS7QB/9) * TX18/100) , RRI1DUPI - VARTMP1)) ;
RPIQB = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQB_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQB_1 , RPIQB1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQB ;

RNONF_1 = max(0 , min(arr(arr(BAS7NF/9) * TX18/100) , RRI1DUPI - VARTMP1)) ;
RNONF = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONF_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONF_1 , RNONF1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONF ;

RPIQM_1 = max(0 , min(arr(arr(BAS7QM/6) * TX12/100) , RRI1DUPI - VARTMP1)) ;
RPIQM = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQM_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQM_1 , RPIQM1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQM ;

RPIQR_1 = max(0 , min(arr(arr(BAS7QR/6) * TX12/100) , RRI1DUPI - VARTMP1)) ;
RPIQR = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQR_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQR_1 , RPIQR1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQR ;

RPIQW_1 = max(0 , min(arr(arr(BAS7QW/6) * TX12/100) , RRI1DUPI - VARTMP1)) ;
RPIQW = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQW_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQW_1 , RPIQW1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQW ;

RNONA_1 = max(0 , min(arr(arr(BAS7NA/6) * TX12/100) , RRI1DUPI - VARTMP1)) ;
RNONA = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONA_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONA_1 , RNONA1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONA ;

RPIQA_1 = max(0 , min(arr(arr(BAS7QA/6) * TX12/100) , RRI1DUPI - VARTMP1)) ;
RPIQA = positif(null(V_IND_TRAIT-4) + COD9ZA) * RPIQA_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RPIQA_1 , RPIQA1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RPIQA ;

RNONE_1 = max(0 , min(arr(arr(BAS7NE/6) * TX12/100) , RRI1DUPI - VARTMP1)) ;
RNONE = positif(null(V_IND_TRAIT-4) + COD9ZA) * RNONE_1 * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
        + (max(0,min(RNONE_1 , RNONE1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0 ;
VARTMP1 = VARTMP1 + RNONE ;


RPI7RR_1 = max(0 , min(arr(arr(BAS7RR/3) * TX06/100) , RRI1DUPI - VARTMP1)) ;
RPI7RR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPI7RR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPI7RR_1,RPI7RR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RPI7RR ;

RPI7RS_1 = max(0 , min(arr(arr(BAS7RS/3) * TX06/100) , RRI1DUPI - VARTMP1)) ;
RPI7RS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPI7RS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPI7RS_1,RPI7RS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

RNORMREP = RNORMJA + RNORMJB + RNORMJC + RNORMJD ;

RNORMAN = RNONC + RNOND + RNONA + RNONB + RNONG + RNONH + RNONE + RNONF ;

RNORMTOT = RNORMREP + RNORMAN ;
RNORMTOT_1 = ANORMJA_1 + ANORMJB_1 + ANORMJC_1 + ANORMJD_1 + RNONC_1 + RNOND_1 + RNONA_1 + RNONB_1 + RNONG_1 + RNONH_1 + RNONE_1 + RNONF_1 ;

RPIREP = RPIREPBI + RPIREPDI + RPIREPBZ + RPIREPCZ + RPIREPDZ + RPIREPEZ + RPIREPQZ + RPIREPRZ + RPIREPSZ + RPIREPTZ 
         + RPIREPRA + RPIREPRB + RPIREPRC + RPIREPRD + RPIREPRE + RPIREPRF + RPIREPRG + RPIREPRH + RPIREPJM + RPIREPKM + RPIREPLM + RPIREPMM ;

RPINEL = RPIQA + RPIQB + RPIQC + RPIQD + RPIQQ + RPIQY + RPIQW + RPIQX + RPIQT + RPIQU + RPIQR + RPIQS + RPIQO + RPIQP + RPIQM + RPIQN ;

RPROPINEL = RPI7RR + RPI7RS ;

RPINELTOT = RPIREP + RPINEL + RPROPINEL ;
RPINELTOT_1 = max(0,min(APIREPBI_1 + APIREPDI_1 + APIREPBZ_1 + APIREPCZ_1 + APIREPDZ_1 + APIREPEZ_1 + APIREPQZ_1 + APIREPRZ_1
            + APIREPSZ_1 + APIREPTZ_1 + APIREPRA_1 + APIREPRB_1 + APIREPRC_1 + APIREPRD_1 + APIREPRE_1 + APIREPRF_1
            + APIREPRG_1 + APIREPRH_1 + APIREPJM_1 + APIREPKM_1 + APIREPLM_1 + APIREPMM_1 , RRI1DUPI)) 
          + RPIQA_1 + RPIQB_1 + RPIQC_1 + RPIQD_1 + RPIQQ_1 + RPIQY_1 + RPIQW_1 + RPIQX_1 + RPIQT_1 + RPIQU_1 + RPIQR_1 + RPIQS_1 + RPIQO_1 + RPIQP_1 + RPIQM_1 + RPIQN_1
          + RPI7RR_1 + RPI7RS_1 ;

regle 401290:
application : iliad ;


RIVPIQD = arr(arr(BAS7QD/9) * TX29/100) ;
RIVPIQD8 = max(0 , arr(BAS7QD * TX29/100) - 8 * RIVPIQD) ;

RIVPIQB = arr(arr(BAS7QB/9) * TX18/100) ;
RIVPIQB8 = max(0 , arr(BAS7QB * TX18/100) - 8 * RIVPIQB) ;

RIVPIQC = arr(arr(BAS7QC/6) * TX23/100) ;
RIVPIQC5 = max(0 , arr(BAS7QC * TX23/100) - 5 * RIVPIQC) ;

RIVPIQA = arr(arr(BAS7QA/6) * TX12/100) ;
RIVPIQA5 = max(0 , arr(BAS7QA * TX12/100) - 5 * RIVPIQA) ;


RIVPIQQ = arr(arr(BAS7QQ/9) * TX29/100) ;
RIVPIQQ8 = max(0 , arr(BAS7QQ * TX29/100) - 8 * RIVPIQQ) ;

RIVPIQX = arr(arr(BAS7QX/9) * TX18/100) ;
RIVPIQX8 = max(0 , arr(BAS7QX * TX18/100) - 8 * RIVPIQX) ;

RIVPIQY = arr(arr(BAS7QY/6) * TX23/100) ;
RIVPIQY5 = max(0 , arr(BAS7QY * TX23/100) - 5 * RIVPIQY) ;

RIVPIQW = arr(arr(BAS7QW/6) * TX12/100) ;
RIVPIQW5 = max(0 , arr(BAS7QW * TX12/100) - 5 * RIVPIQW) ;


RIVNONH = arr(arr(BAS7NH/9) * TX29/100) ;
RIVNONH8 = max(0 , arr(BAS7NH * TX29/100) - 8 * RIVNONH) ;

RIVNONF = arr(arr(BAS7NF/9) * TX18/100) ;
RIVNONF8 = max(0 , arr(BAS7NF * TX18/100) - 8 * RIVNONF) ;

RIVNONG = arr(arr(BAS7NG/6) * TX23/100) ;
RIVNONG5 = max(0 , arr(BAS7NG * TX23/100) - 5 * RIVNONG) ;

RIVNONE = arr(arr(BAS7NE/6) * TX12/100) ;
RIVNONE5 = max(0 , arr(BAS7NE * TX12/100) - 5 * RIVNONE) ;


RIVNOND = arr(arr(BAS7ND/9) * TX29/100) * (1 - V_CNR) ;
RIVNOND8 = max(0 , arr(BAS7ND * TX29/100) - 8 * RIVNOND) * (1 - V_CNR) ;

RIVNONB = arr(arr(BAS7NB/9) * TX18/100) * (1 - V_CNR) ;
RIVNONB8 = max(0 , arr(BAS7NB * TX18/100) - 8 * RIVNONB) * (1 - V_CNR) ;

RIVNONC = arr(arr(BAS7NC/6) * TX23/100) * (1 - V_CNR) ;
RIVNONC5 = max(0 , arr(BAS7NC * TX23/100) - 5 * RIVNONC) * (1 - V_CNR) ;

RIVNONA = arr(arr(BAS7NA/6) * TX12/100) * (1 - V_CNR) ;
RIVNONA5 = max(0 , arr(BAS7NA * TX12/100) - 5 * RIVNONA) * (1 - V_CNR) ;


RIVPIQU = arr(arr(BAS7QU/9) * (TX29/100)) * (1 - V_CNR) ;
RIVPIQU8 = max(0 , arr(BAS7QU * (TX29/100)) - 8 * RIVPIQU) * (1 - V_CNR) ; 

RIVPIQS = arr(arr(BAS7QS/9) * (TX18/100)) * (1 - V_CNR) ;
RIVPIQS8 = max(0 , arr(BAS7QS * (TX18/100)) - 8 * RIVPIQS) * (1 - V_CNR) ;

RIVPIQT = arr(arr(BAS7QT/6) * (TX23/100)) * (1 - V_CNR) ;
RIVPIQT5 = max(0 , arr(BAS7QT * (TX23/100)) - 5 * RIVPIQT) * (1 - V_CNR) ;

RIVPIQR = arr(arr(BAS7QR/6) * (TX12/100)) * (1 - V_CNR) ;
RIVPIQR5 = max(0 , arr(BAS7QR * (TX12/100)) - 5 * RIVPIQR) * (1 - V_CNR) ;


RIVPIQP = arr(arr(BAS7QP/9) * (TX29/100)) * (1 - V_CNR) ;
RIVPIQP8 = max(0 , arr(BAS7QP * (TX29/100)) - 8 * RIVPIQP) * (1 - V_CNR) ; 

RIVPIQN = arr(arr(BAS7QN/9) * (TX18/100)) * (1 - V_CNR) ;
RIVPIQN8 = max(0 , arr(BAS7QN * (TX18/100)) - 8 * RIVPIQN) * (1 - V_CNR) ;

RIVPIQO = arr(arr(BAS7QO/6) * (TX23/100)) * (1 - V_CNR) ;
RIVPIQO5 = max(0 , arr(BAS7QO * (TX23/100)) - 5 * RIVPIQO) * (1 - V_CNR) ;

RIVPIQM = arr(arr(BAS7QM/6) * (TX12/100)) * (1 - V_CNR) ;
RIVPIQM5 = max(0 , arr(BAS7QM * (TX12/100)) - 5 * RIVPIQM) * (1 - V_CNR) ;


RIVPIRR = arr(arr(BAS7RR/3) * (TX06/100)) * (1 - V_CNR) ;
RIVPIRR2 = max(0 , arr(BAS7RR * (TX06/100)) - 2 * RIVPIRR) * (1 - V_CNR) ;

RIVPIRS = arr(arr(BAS7RS/3) * (TX06/100)) * (1 - V_CNR) ;
RIVPIRS2 = max(0 , arr(BAS7RS * (TX06/100)) - 2 * RIVPIRS) * (1 - V_CNR) ;

REPIQD = RIVPIQD * 7 + RIVPIQD8 ;
REPIQB = RIVPIQB * 7 + RIVPIQB8 ;
REPIQC = RIVPIQC * 4 + RIVPIQC5 ;
REPIQA = RIVPIQA * 4 + RIVPIQA5 ;
REPIQQ = RIVPIQQ * 7 + RIVPIQQ8 ;
REPIQX = RIVPIQX * 7 + RIVPIQX8 ;
REPIQY = RIVPIQY * 4 + RIVPIQY5 ;
REPIQW = RIVPIQW * 4 + RIVPIQW5 ;
RENONH = RIVNONH * 7 + RIVNONH8 ;
RENONF = RIVNONF * 7 + RIVNONF8 ;
RENONG = RIVNONG * 4 + RIVNONG5 ;
RENONE = RIVNONE * 4 + RIVNONE5 ;
RENOND = RIVNOND * 7 + RIVNOND8 ;
RENONB = RIVNONB * 7 + RIVNONB8 ;
RENONC = RIVNONC * 4 + RIVNONC5 ;
RENONA = RIVNONA * 4 + RIVNONA5 ;
REPIQU = RIVPIQU * 7 + RIVPIQU8 ;
REPIQS = RIVPIQS * 7 + RIVPIQS8 ;
REPIQT = RIVPIQT * 4 + RIVPIQT5 ;
REPIQR = RIVPIQR * 4 + RIVPIQR5 ;
REPIQP = RIVPIQP * 7 + RIVPIQP8 ;
REPIQN = RIVPIQN * 7 + RIVPIQN8 ;
REPIQO = RIVPIQO * 4 + RIVPIQO5 ;
REPIQM = RIVPIQM * 4 + RIVPIQM5 ;
REPIRR = RIVPIRR + RIVPIRR2 ;
REPIRS = RIVPIRS + RIVPIRS2 ;

regle 401300:
application : iliad ;

REDUCAVTCEL = RCOTFOR + RREPA + RDIFAGRI + RPRESSE + RFORET + RFIPDOM + RFIPC + RCINE + RRESTIMO + RSOCREPR + RRPRESCOMP 
              + RHEBE + RSURV + RINNO + RSOUFIP + RRIRENOV + RLOGDOM + RCOMP + RRETU + RDONS + CRDIE + RDUFREP + RPINELTOT 
	      + RNORMTOT + RNOUV + RPENTOT + RRSOFON + RFOR + RTOURREP + RTOUREPA + RREHAB + RRESTREP + RRESTIMO1 ;

VARTMP1 = DEC11 + REDUCAVTCEL ;

RCELRREDLG_1 = max(min(CELRREDLG, IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLG_1,RCELRREDLG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLG ;

RCELRREDLK_1 = max(min(CELRREDLK , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLK_1,RCELRREDLK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLK ;

RCELRREDLQ_1 = max(min(CELRREDLQ , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLQ_1,RCELRREDLQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLQ ;

RCELLA_1 = max(min(COD7LA , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELLA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELLA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELLA_1,RCELLA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELLA ;

RCELMS_1 = max(min(COD7MS , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMS_1,RCELMS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMS ;

RCELMO_1 = max(min(COD7MO , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMO_1,RCELMO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMO ;

RCELRREDLH_1 = max(min(CELRREDLH , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLH_1,RCELRREDLH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLH ;

RCELRREDLL_1 = max(min(CELRREDLL , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLL_1,RCELRREDLL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLL ;

RCELRREDLR_1 = max(min(CELRREDLR , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLR_1,RCELRREDLR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLR ;

RCELLB_1 = max(min(COD7LB , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELLB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELLB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELLB_1,RCELLB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELLB ;

RCELMT_1 = max(min(COD7MT , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMT_1,RCELMT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMT ;

RCELMP_1 = max(min(COD7MP , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMP_1,RCELMP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMP ;

RCELRREDLI_1 = max(min(CELRREDLI , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLI_1,RCELRREDLI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLI ;

RCELRREDLO_1 = max(min(CELRREDLO , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLO_1,RCELRREDLO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLO ;

RCELRREDLU_1 = max(min(CELRREDLU , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLU_1,RCELRREDLU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLU ;

RCELLC_1 = max(min(COD7LC , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELLC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELLC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELLC_1,RCELLC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELLC ;

RCELMU_1 = max(min(COD7MU , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMU_1,RCELMU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMU ;

RCELMQ_1 = max(min(COD7MQ , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMQ_1,RCELMQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMQ ;

RCELRREDLJ_1 = max(min(CELRREDLJ , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLJ_1,RCELRREDLJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLJ ;

RCELRREDLP_1 = max(min(CELRREDLP , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLP_1,RCELRREDLP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLP ;

RCELRREDLV_1 = max(min(CELRREDLV , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELRREDLV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRREDLV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRREDLV_1,RCELRREDLV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRREDLV ;

RCELLY_1 = max(min(COD7LY , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELLY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELLY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELLY_1,RCELLY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELLY ;

RCELMV_1 = max(min(COD7MV , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMV_1,RCELMV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELMV ;

RCELMR_1 = max(min(COD7MR , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELMR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELMR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELMR_1,RCELMR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

DCELSOM1 = somme (i=G,K,Q,H,L,R,I,O,U,J,P,V : CELRREDLi) + somme (i=A,B,C,Y : COD7Li) + COD7MS + COD7MT + COD7MU + COD7MV + COD7MO + COD7MP + COD7MQ + COD7MR ;

ACELSOM1 = DCELSOM1 * ( 1 - V_CNR ) ;

RCELSOM1 = somme (i=G,K,Q,H,L,R,I,O,U,J,P,V : RCELRREDLi) + somme (i=A,B,C,Y : RCELLi) + RCELMS + RCELMT + RCELMU + RCELMV + RCELMO + RCELMP + RCELMQ + RCELMR ;
RCELSOM1_1 = somme (i=G,K,Q,H,L,R,I,O,U,J,P,V : RCELRREDLi_1) + somme (i=A,B,C,Y : RCELLi_1) + RCELMS_1 + RCELMT_1 + RCELMU_1
                                                  + RCELMV_1 + RCELMO_1 + RCELMP_1 + RCELMQ_1 + RCELMR_1 ;

regle 401302:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 ;



RCELZP_1 = max(min(COD7ZP , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELZP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZP_1,RCELZP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZP ;

RCELZO_1 = max(min(COD7ZO , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELZO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZO_1,RCELZO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZO ;

RCELXP_1 = max(min(COD7XP , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELXP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXP_1,RCELXP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXP ;

RCELXO_1 = max(min(COD7XO , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELXO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXO_1,RCELXO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXO ;

RCELXQ_1 = max(min(COD7XQ , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELXQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXQ_1,RCELXQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXQ ;

RCELYI_1 = max(min(COD7YI , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELYI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELYI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELYI_1,RCELYI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELYI ;

RCELYJ_1 = max(min(COD7YJ , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELYJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELYJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELYJ_1,RCELYJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELYJ ;

RCELYK_1 = max(min(COD7YK , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELYK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELYK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELYK_1,RCELYK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELYK ;

RCELYL_1 = max(min(COD7YL , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELYL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELYL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELYL_1,RCELYL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELYL ;

RCELZI_1 = max(min(COD7ZI , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELZI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZI_1,RCELZI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZI ;

RCELZJ_1 = max(min(COD7ZJ , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELZJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZJ_1,RCELZJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZJ ;

RCELZK_1 = max(min(COD7ZK , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELZK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZK_1,RCELZK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZK ;

RCELZL_1 = max(min(COD7ZL , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELZL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZL_1,RCELZL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

DCELSOM2 = COD7YI + COD7ZI + COD7ZP + COD7XP + COD7YJ + COD7ZJ + COD7ZO + COD7XO + COD7YK + COD7ZK + COD7XQ + COD7YL + COD7ZL ;

ACELSOM2 = DCELSOM2 * (1 - V_CNR) ;

RCELSOM2 = RCELYI + RCELZI + RCELZP + RCELXP + RCELYJ + RCELZJ + RCELZO + RCELXO + RCELYK + RCELZK + RCELXQ + RCELYL + RCELZL ;
RCELSOM2_1 = RCELYI_1 + RCELZI_1 + RCELZP_1 + RCELXP_1 + RCELYJ_1 + RCELZJ_1 + RCELZO_1 + RCELXO_1 + RCELYK_1 + RCELZK_1 + RCELXQ_1 + RCELYL_1 + RCELZL_1 ;

regle 401304:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 ;

RCELKC_1 = max(min(COD7KC , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELKC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELKC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELKC_1,RCELKC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELKC ;

RCELKD_1 = max(min(COD7KD , IDOM11 - VARTMP1) , 0 ) * ( 1 - V_CNR ) ;
RCELKD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELKD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELKD_1,RCELKD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

DCELSOM5 = COD7KC + COD7KD ;

ACELSOM5 = DCELSOM5 * (1 - V_CNR) ;

RCELSOM5 = RCELKC + RCELKD ;
RCELSOM5_1 = RCELKC_1 + RCELKD_1 ;

regle 401306:
application : iliad ;

ACELREPGU_1 = (min(LIMREPSC7 , CELREPGU) * (1 - COD7YE) + CELREPGU * COD7YE) * (1 - V_CNR) ;
ACELREPGU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPGU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPGU_1,ACELREPGU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPGS_1 = (min(LIMREPSC7 , CELREPGS) * (1 - COD7YE) + CELREPGS * COD7YE) * (1 - V_CNR) ;
ACELREPGS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPGS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPGS_1,ACELREPGS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPGL_1 = (min(LIMREPSC6 , CELREPGL) * (1 - COD7YE) + CELREPGL * COD7YE) * (1 - V_CNR) ;
ACELREPGL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPGL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPGL_1,ACELREPGL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPGJ_1 = (min(LIMREPSC2 , CELREPGJ) * (1 - COD7YE) + CELREPGJ * COD7YE) * (1 - V_CNR) ;
ACELREPGJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPGJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPGJ_1,ACELREPGJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYH_1 = (min(LIMREPSC7 , CELREPYH) * (1 - COD7YE) + CELREPYH * COD7YE) * (1 - V_CNR) ;
ACELREPYH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYH_1,ACELREPYH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYF_1 = (min(LIMREPSC7 , CELREPYF) * (1 - COD7YE) + CELREPYF * COD7YE) * (1 - V_CNR) ;
ACELREPYF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYF_1,ACELREPYF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYD_1 = (min(LIMREPSC6 , CELREPYD) * (1 - COD7YE) + CELREPYD * COD7YE) * (1 - V_CNR) ;
ACELREPYD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYD_1,ACELREPYD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYB_1 = (min(LIMREPSC4 , CELREPYB) * (1 - COD7YE) + CELREPYB * COD7YE) * (1 - V_CNR) ;
ACELREPYB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYB_1,ACELREPYB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYP_1 = (min(LIMREPSC7 , CELREPYP) * (1 - COD7YE) + CELREPYP * COD7YE) * (1 - V_CNR) ;
ACELREPYP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYP_1,ACELREPYP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYO_1 = (min(LIMREPSC7 , CELREPYO) * (1 - COD7YE) + CELREPYO * COD7YE) * (1 - V_CNR) ;
ACELREPYO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYO_1,ACELREPYO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYN_1 = (min(LIMREPSC6 , CELREPYN) * (1 - COD7YE) + CELREPYN * COD7YE) * (1 - V_CNR) ;
ACELREPYN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYN_1,ACELREPYN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYM_1 = (min(LIMREPSC4 , CELREPYM) * (1 - COD7YE) + CELREPYM * COD7YE) * (1 - V_CNR) ;
ACELREPYM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYM_1,ACELREPYM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYW_1 = (min(LIMREPSC7 , CELREPYW) * (1 - COD7YE) + CELREPYW * COD7YE) * (1 - V_CNR) ;
ACELREPYW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYW_1,ACELREPYW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYV_1 = (min(LIMREPSC7 , CELREPYV) * (1 - COD7YE) + CELREPYV * COD7YE) * (1 - V_CNR) ;
ACELREPYV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYV_1,ACELREPYV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYU_1 = (min(LIMREPSC6 , CELREPYU) * (1 - COD7YE) + CELREPYU * COD7YE) * (1 - V_CNR) ;
ACELREPYU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYU_1,ACELREPYU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPYT_1 = (min(LIMREPSC4 , CELREPYT) * (1 - COD7YE) + CELREPYT * COD7YE) * (1 - V_CNR) ;
ACELREPYT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPYT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPYT_1,ACELREPYT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWW_1 = (min(LIMREPSC7 , CELREPWW) * (1 - COD7YE) + CELREPWW * COD7YE) * (1 - V_CNR) ;
ACELREPWW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWW_1,ACELREPWW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWZ_1 = (min(LIMREPSC6 , CELREPWZ) * (1 - COD7YE) + CELREPWZ * COD7YE) * (1 - V_CNR) ;
ACELREPWZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWZ_1,ACELREPWZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWV_1 = (min(LIMREPSC7 , CELREPWV) * (1 - COD7YE) + CELREPWV * COD7YE) * (1 - V_CNR) ;
ACELREPWV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWV_1,ACELREPWV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWY_1 = (min(LIMREPSC9 , CELREPWY) * (1 - COD7YE) + CELREPWY * COD7YE) * (1 - V_CNR) ;
ACELREPWY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWY_1,ACELREPWY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWU_1 = (min(LIMREPSC6 , CELREPWU) * (1 - COD7YE) + CELREPWU * COD7YE) * (1 - V_CNR) ;
ACELREPWU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWU_1,ACELREPWU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWX_1 = (min(LIMREPSC8 , CELREPWX) * (1 - COD7YE) + CELREPWX * COD7YE) * (1 - V_CNR) ;
ACELREPWX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWX_1,ACELREPWX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELREPWT_1 = (min(LIMREPSC4 , CELREPWT) * (1 - COD7YE) + CELREPWT * COD7YE) * (1 - V_CNR) ;
ACELREPWT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELREPWT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELREPWT_1,ACELREPWT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRW_1 = (min(LIMREPSC5 , COD7RW) * (1 - COD7YE) + COD7RW * COD7YE) * (1 - V_CNR) ;
ACELRW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRW_1,ACELRW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRV_1 = (min(LIMREPSC8 , COD7RV) * (1 - COD7YE) + COD7RV * COD7YE) * (1 - V_CNR) ;
ACELRV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRV_1,ACELRV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRU_1 = (min(LIMREPSC3 , COD7RU) * (1 - COD7YE) + COD7RU * COD7YE) * (1 - V_CNR) ;
ACELRU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRU_1,ACELRU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRT_1 = (min(LIMREPSC4 , COD7RT) * (1 - COD7YE) + COD7RT * COD7YE) * (1 - V_CNR) ;
ACELRT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRT_1,ACELRT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCELSOM4 = CELREPGU + CELREPGS + CELREPGL + CELREPGJ + CELREPYH + CELREPYF + CELREPYD + CELREPYB 
           + CELREPYP + CELREPYO + CELREPYN + CELREPYM + CELREPYW + CELREPYV + CELREPYU + CELREPYT 
	   + CELREPWW + CELREPWZ + CELREPWV + CELREPWY + CELREPWU + CELREPWX + CELREPWT + COD7RW 
	   + COD7RV + COD7RU + COD7RT ;

ACELSOM4 = ACELREPGU + ACELREPGS + ACELREPGL + ACELREPGJ + ACELREPYH + ACELREPYF + ACELREPYD + ACELREPYB 
           + ACELREPYP + ACELREPYO + ACELREPYN + ACELREPYM + ACELREPYW + ACELREPYV + ACELREPYU + ACELREPYT 
	   + ACELREPWW + ACELREPWZ + ACELREPWV + ACELREPWY + ACELREPWU + ACELREPWX + ACELREPWT + ACELRW 
	   + ACELRV + ACELRU + ACELRT ;

regle 401308:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 + RCELSOM5 ;

RCELREPGU_1 = max(min(ACELREPGU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPGU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPGU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPGU_1,RCELREPGU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPGU ;

RCELREPGS_1 = max(min(ACELREPGS , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPGS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPGS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPGS_1,RCELREPGS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPGS ;

RCELREPGL_1 = max(min(ACELREPGL , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPGL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPGL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPGL_1,RCELREPGL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPGL ;

RCELREPGJ_1 = max(min(ACELREPGJ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPGJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPGJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPGJ_1,RCELREPGJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPGJ ;

RCELREPYH_1 = max(min(ACELREPYH , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYH_1,RCELREPYH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYH ;

RCELREPYF_1 = max(min(ACELREPYF , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYF_1,RCELREPYF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYF ;

RCELREPYD_1 = max(min(ACELREPYD , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYD_1,RCELREPYD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYD ;

RCELREPYB_1 = max(min(ACELREPYB , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYB_1,RCELREPYB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYB ;

RCELREPYP_1 = max(min(ACELREPYP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYP_1,RCELREPYP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYP ;

RCELREPYO_1 = max(min(ACELREPYO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYO_1,RCELREPYO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYO ;

RCELREPYN_1 = max(min(ACELREPYN , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYN_1,RCELREPYN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYN ;

RCELREPYM_1 = max(min(ACELREPYM , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYM_1,RCELREPYM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYM ;

RCELREPYW_1 = max(min(ACELREPYW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYW_1,RCELREPYW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYW ;

RCELREPYV_1 = max(min(ACELREPYV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYV_1,RCELREPYV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYV ;

RCELREPYU_1 = max(min(ACELREPYU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYU_1,RCELREPYU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYU ;

RCELREPYT_1 = max(min(ACELREPYT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPYT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPYT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPYT_1,RCELREPYT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPYT ;

RCELREPWW_1 = max(min(ACELREPWW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWW_1,RCELREPWW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWW ;

RCELREPWZ_1 = max(min(ACELREPWZ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWZ_1,RCELREPWZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWZ ;

RCELREPWV_1 = max(min(ACELREPWV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWV_1,RCELREPWV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWV ;

RCELREPWY_1 = max(min(ACELREPWY , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWY_1,RCELREPWY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWY ;

RCELREPWU_1 = max(min(ACELREPWU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWU_1,RCELREPWU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWU ;

RCELREPWX_1 = max(min(ACELREPWX , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWX_1,RCELREPWX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWX ;

RCELREPWT_1 = max(min(ACELREPWT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELREPWT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELREPWT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELREPWT_1,RCELREPWT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELREPWT ;

RCELRW_1 = max(min(ACELRW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRW_1,RCELRW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRW ;

RCELRU_1 = max(min(ACELRU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRU_1,RCELRU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRU ;

RCELRV_1 = max(min(ACELRV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRV_1,RCELRV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRV ;

RCELRT_1 = max(min(ACELRT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRT_1,RCELRT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

RCELSOM4 = RCELREPGU + RCELREPGS + RCELREPGL + RCELREPGJ + RCELREPYH + RCELREPYF + RCELREPYD + RCELREPYB 
           + RCELREPYP + RCELREPYO + RCELREPYN + RCELREPYM + RCELREPYW + RCELREPYV + RCELREPYU + RCELREPYT 
	   + RCELREPWW + RCELREPWZ + RCELREPWV + RCELREPWY + RCELREPWU + RCELREPWX + RCELREPWT + RCELRW 
	   + RCELRV + RCELRU + RCELRT ;
RCELSOM4_1 = min(ACELREPGU_1 + ACELREPGS_1 + ACELREPGL_1 + ACELREPGJ_1 + ACELREPYH_1 + ACELREPYF_1 + ACELREPYD_1 + ACELREPYB_1
           + ACELREPYP_1 + ACELREPYO_1 + ACELREPYN_1 + ACELREPYM_1 + ACELREPYW_1 + ACELREPYV_1 + ACELREPYU_1 + ACELREPYT_1
	   + ACELREPWW_1 + ACELREPWZ_1 + ACELREPWV_1 + ACELREPWY_1 + ACELREPWU_1 + ACELREPWX_1 + ACELREPWT_1 + ACELRW_1
	   + ACELRV_1 + ACELRU_1 + ACELRT_1
	   ,IDOM11-(DEC11 + REDUCAVTCEL + RCELSOM1_1 + RCELSOM2_1 + RCELSOM5_1));

regle 401310:
application : iliad ;

ACELWA_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7WA) + (COD7YE * COD7WA)) * (1 - V_CNR) ;
ACELWA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWA_1,ACELWA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELWB_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7WB) + (COD7YE * COD7WB)) * (1 - V_CNR) ;
ACELWB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWB_1,ACELWB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELWC_1 = (min(LIMREPSC12 * (1 - COD7YE) , COD7WC) + (COD7YE * COD7WC)) * (1 - V_CNR) ;
ACELWC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWC_1,ACELWC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELWD_1 = (min(LIMREPSC10 * (1 - COD7YE) , COD7WD) + (COD7YE * COD7WD)) * (1 - V_CNR) ;
ACELWD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWD_1,ACELWD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELWE_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7WE) + (COD7YE * COD7WE)) * (1 - V_CNR) ;
ACELWE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWE_1,ACELWE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELWF_1 = (min(LIMREPSC12 * (1 - COD7YE) , COD7WF) + (COD7YE * COD7WF)) * (1 - V_CNR) ;
ACELWF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWF_1,ACELWF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELWG_1 = (min(LIMREPSC2 * (1 - COD7YE) , COD7WG) + (COD7YE * COD7WG)) * (1 - V_CNR) ;
ACELWG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELWG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELWG_1,ACELWG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNO_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7NO) + (COD7YE * COD7NO)) * (1 - V_CNR) ;
ACELNO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNO_1,ACELNO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNP_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7NP) + (COD7YE * COD7NP)) * (1 - V_CNR) ;
ACELNP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNP_1,ACELNP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNQ_1 = (min(LIMREPSC3 * (1 - COD7YE) , COD7NQ) + (COD7YE * COD7NQ)) * (1 - V_CNR) ;
ACELNQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNQ_1,ACELNQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNR_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7NR) + (COD7YE * COD7NR)) * (1 - V_CNR) ;
ACELNR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNR_1,ACELNR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNS_1 = (min(LIMREPSC12 * (1 - COD7YE) , COD7NS) + (COD7YE * COD7NS)) * (1 - V_CNR) ;
ACELNS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNS_1,ACELNS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNT_1 = (min(LIMREPSC2 * (1 - COD7YE) , COD7NT) + (COD7YE * COD7NT)) * (1 - V_CNR) ;
ACELNT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNT_1,ACELNT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNU_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7NU) + (COD7YE * COD7NU)) * (1 - V_CNR) ;
ACELNU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNU_1,ACELNU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNV_1 = (min(LIMREPSC12 * (1 - COD7YE) , COD7NV) + (COD7YE * COD7NV)) * (1 - V_CNR) ;
ACELNV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNV_1,ACELNV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELNW_1 = (min(LIMREPSC2 * (1 - COD7YE) , COD7NW) + (COD7YE * COD7NW)) * (1 - V_CNR) ;
ACELNW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELNW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELNW_1,ACELNW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCELSOM6 = COD7WA + COD7WB + COD7WC + COD7WD + COD7WE + COD7WF + COD7WG + COD7NO + COD7NP + COD7NQ + COD7NR + COD7NS + COD7NT + COD7NU + COD7NV + COD7NW ;

ACELSOM6 = ACELWA + ACELWB + ACELWC + ACELWD + ACELWE + ACELWF + ACELWG + ACELNO + ACELNP + ACELNQ + ACELNR + ACELNS + ACELNT + ACELNU + ACELNV + ACELNW ;

regle 401312:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 + RCELSOM5 + RCELSOM4 ;

RCELWA_1 = max(min(ACELWA , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWA_1,RCELWA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWA ;

RCELWB_1 = max(min(ACELWB , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWB_1,RCELWB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWB ;

RCELWC_1 = max(min(ACELWC , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWC_1,RCELWC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWC ;

RCELWD_1 = max(min(ACELWD , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWD_1,RCELWD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWD ;

RCELWE_1 = max(min(ACELWE , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWE_1,RCELWE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWE ;

RCELWF_1 = max(min(ACELWF , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWF_1,RCELWF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWF ;

RCELWG_1 = max(min(ACELWG , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELWG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELWG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELWG_1,RCELWG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELWG ;

RCELNO_1 = max(min(ACELNO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNO_1,RCELNO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNO ;

RCELNP_1 = max(min(ACELNP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNP_1,RCELNP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNP ;

RCELNQ_1 = max(min(ACELNQ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNQ_1,RCELNQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNQ ;

RCELNR_1 = max(min(ACELNR , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNR_1,RCELNR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNR ;

RCELNS_1 = max(min(ACELNS , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNS_1,RCELNS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNS ;

RCELNT_1 = max(min(ACELNT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNT_1,RCELNT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNT ;

RCELNU_1 = max(min(ACELNU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNU_1,RCELNU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNU ;

RCELNV_1 = max(min(ACELNV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNV_1,RCELNV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELNV ;

RCELNW_1 = max(min(ACELNW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELNW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELNW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELNW_1,RCELNW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

RCELSOM6 = RCELWA + RCELWB + RCELWC + RCELWD + RCELWE + RCELWF + RCELWG + RCELNO + RCELNP + RCELNQ + RCELNR + RCELNS + RCELNT + RCELNU + RCELNV + RCELNW ;
RCELSOM6_1 = min(ACELWA_1 + ACELWB_1 + ACELWC_1 + ACELWD_1 + ACELWE_1 + ACELWF_1 + ACELWG_1 + ACELNO_1 + ACELNP_1 + ACELNQ_1 + ACELNR_1 
                                            + ACELNS_1 + ACELNT_1 + ACELNU_1 + ACELNV_1 + ACELNW_1 
              ,IDOM11-(DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 + RCELSOM5 + RCELSOM4));
regle 401314:
application : iliad ;

ACELKA_1 = (min(LIM5000 * (1 - COD7YE) , COD7KA) + (COD7YE * COD7KA)) * (1 - V_CNR) ;
ACELKA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELKA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELKA_1,ACELKA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
ACELKB_1 = (min(LIMREPSC11 * (1 - COD7YE) , COD7KB) + (COD7YE * COD7KB)) * (1 - V_CNR) ;
ACELKB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELKB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELKB_1,ACELKB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 + RCELSOM5 + RCELSOM4 + RCELSOM6 ;

RCELKA_1 = max(min(ACELKA , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELKA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELKA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELKA_1,RCELKA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELKA ;

RCELKB_1 = max(min(ACELKB , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELKB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELKB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELKB_1,RCELKB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

DCELSOM8 = COD7KA + COD7KB ;
ACELSOM8 = ACELKA + ACELKB ;
RCELSOM8 = RCELKA + RCELKB ;
RCELSOM8_1 = ACELKA_1 + ACELKB_1 ;

regle 401316:
application : iliad ;

ACELZM_1 = arr((min(COD7ZM , LIMCELLIER) * (1 - COD7YE) + COD7ZM * COD7YE) /3) * (1 - V_CNR) ;
ACELZM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZM_1,ACELZM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZN_1 = arr((min(COD7ZN , LIMCELLIER) * (1 - COD7YE) + COD7ZN * COD7YE) /3) * (1 - V_CNR) ;
ACELZN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZN_1,ACELZN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZA_1 = arr((min(COD7ZA , LIMCELLIER) * (1 - COD7YE) + COD7ZA * COD7YE) /3) * (1 - V_CNR) ;
ACELZA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZA_1,ACELZA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZB_1 = arr((min(COD7ZB , LIMCELLIER) * (1 - COD7YE) + COD7ZB * COD7YE) /3) * (1 - V_CNR) ;
ACELZB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZB_1,ACELZB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZG_1 = arr((min(COD7ZG , LIMCELLIER) * (1 - COD7YE) + COD7ZG * COD7YE) /3) * (1 - V_CNR) ;
ACELZG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZG_1,ACELZG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZH_1 = arr((min(COD7ZH , LIMCELLIER) * (1 - COD7YE) + COD7ZH * COD7YE) /3) * (1 - V_CNR) ;
ACELZH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZH_1,ACELZH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZC_1 = arr((min(COD7ZC , LIMCELLIER) * (1 - COD7YE) + COD7ZC * COD7YE) /3) * (1 - V_CNR) ;
ACELZC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZC_1,ACELZC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZD_1 = arr((min(COD7ZD , LIMCELLIER) * (1 - COD7YE) + COD7ZD * COD7YE) /3) * (1 - V_CNR) ;
ACELZD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZD_1,ACELZD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZE_1 = arr((min(COD7ZE , LIMCELLIER) * (1 - COD7YE) + COD7ZE * COD7YE) /3) * (1 - V_CNR) ;
ACELZE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZE_1,ACELZE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELZF_1 = arr((min(COD7ZF , LIMCELLIER) * (1 - COD7YE) + COD7ZF * COD7YE) /3) * (1 - V_CNR) ;
ACELZF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELZF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELZF_1,ACELZF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRI_1 = arr((min(COD7RI , LIMCELLIER) * (1 - COD7YE) + COD7RI * COD7YE) /3) * (1 - V_CNR) ;
ACELRI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRI_1,ACELRI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRJ_1 = arr((min(COD7RJ , LIMCELLIER) * (1 - COD7YE) + COD7RJ * COD7YE) /3) * (1 - V_CNR) ;
ACELRJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRJ_1,ACELRJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRK_1 = arr((min(COD7RK , LIMCELLIER) * (1 - COD7YE) + COD7RK * COD7YE) /3) * (1 - V_CNR) ;
ACELRK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRK_1,ACELRK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRL_1 = arr((min(COD7RL , LIMCELLIER) * (1 - COD7YE) + COD7RL * COD7YE) /3) * (1 - V_CNR) ;
ACELRL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRL_1,ACELRL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIR_1 = arr((min(COD7IR , LIMCELLIER) * (1 - COD7YE) + COD7IR * COD7YE) /3) * (1 - V_CNR) ;
ACELIR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIR_1,ACELIR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIS_1 = arr((min(COD7IS , LIMCELLIER) * (1 - COD7YE) + COD7IS * COD7YE) /3) * (1 - V_CNR) ;
ACELIS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIS_1,ACELIS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIT_1 = arr((min(COD7IT , LIMCELLIER) * (1 - COD7YE) + COD7IT * COD7YE) /3) * (1 - V_CNR) ;
ACELIT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIT_1,ACELIT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIU_1 = arr((min(COD7IU , LIMCELLIER) * (1 - COD7YE) + COD7IU * COD7YE) /3) * (1 - V_CNR) ;
ACELIU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIU_1,ACELIU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRM_1 = arr((min(COD7RM , LIMCELLIER) * (1 - COD7YE) + COD7RM * COD7YE) /3) * (1 - V_CNR) ;
ACELRM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRM_1,ACELRM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRN_1 = arr((min(COD7RN , LIMCELLIER) * (1 - COD7YE) + COD7RN * COD7YE) /3) * (1 - V_CNR) ;
ACELRN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRN_1,ACELRN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRO_1 = arr((min(COD7RO , LIMCELLIER) * (1 - COD7YE) + COD7RO * COD7YE) /3) * (1 - V_CNR) ;
ACELRO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRO_1,ACELRO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRP_1 = arr((min(COD7RP , LIMCELLIER) * (1 - COD7YE) + COD7RP * COD7YE) /3) * (1 - V_CNR) ;
ACELRP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRP_1,ACELRP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIV_1 = arr((min(COD7IV , LIMCELLIER) * (1 - COD7YE) + COD7IV * COD7YE) /3) * (1 - V_CNR) ;
ACELIV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIV_1,ACELIV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIW_1 = arr((min(COD7IW , LIMCELLIER) * (1 - COD7YE) + COD7IW * COD7YE) /3) * (1 - V_CNR) ;
ACELIW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIW_1,ACELIW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIX_1 = arr((min(COD7IX , LIMCELLIER) * (1 - COD7YE) + COD7IX * COD7YE) /3) * (1 - V_CNR) ;
ACELIX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIX_1,ACELIX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIY_1 = arr((min(COD7IY , LIMCELLIER) * (1 - COD7YE) + COD7IY * COD7YE) /3) * (1 - V_CNR) ;
ACELIY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIY_1,ACELIY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELRQ_1 = arr((min(COD7RQ , LIMCELLIER) * (1 - COD7YE) + COD7RQ * COD7YE) /3) * (1 - V_CNR) ;
ACELRQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELRQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELRQ_1,ACELRQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELIZ_1 = arr((min(COD7IZ , LIMCELLIER) * (1 - COD7YE) + COD7IZ * COD7YE) /3) * (1 - V_CNR) ;
ACELIZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELIZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELIZ_1,ACELIZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


RCEL7ZM = arr(ACELZM * (TX06/100)) ;
RCEL7ZM_1 = arr(ACELZM_1 * (TX06/100)) ;

RCEL7ZN = arr(ACELZN * (TX06/100)) ;
RCEL7ZN_1 = arr(ACELZN_1 * (TX06/100)) ;

RCEL7ZA = arr(ACELZA * (TX06/100)) ;
RCEL7ZA_1 = arr(ACELZA_1 * (TX06/100)) ;

RCEL7ZB = arr(ACELZB * (TX06/100)) ;
RCEL7ZB_1 = arr(ACELZB_1 * (TX06/100)) ;

RCEL7ZG = arr(ACELZG * (TX06/100)) ;
RCEL7ZG_1 = arr(ACELZG_1 * (TX06/100)) ;

RCEL7ZH = arr(ACELZH * (TX06/100)) ;
RCEL7ZH_1 = arr(ACELZH_1 * (TX06/100)) ;

RCEL7ZC = arr(ACELZC * (TX05/100)) ;
RCEL7ZC_1 = arr(ACELZC_1 * (TX05/100)) ;

RCEL7ZD = arr(ACELZD * (TX06/100)) ;
RCEL7ZD_1 = arr(ACELZD_1 * (TX06/100)) ;

RCEL7ZE = arr(ACELZE * (TX06/100)) ;
RCEL7ZE_1 = arr(ACELZE_1 * (TX06/100)) ;

RCEL7ZF = arr(ACELZF * (TX05/100)) ;
RCEL7ZF_1 = arr(ACELZF_1 * (TX05/100)) ;

RCEL7RI = arr(ACELRI * (TX05/100)) ;
RCEL7RI_1 = arr(ACELRI_1 * (TX05/100)) ;

RCEL7RJ = arr(ACELRJ * (TX06/100)) ;
RCEL7RJ_1 = arr(ACELRJ_1 * (TX06/100)) ;

RCEL7RK = arr(ACELRK * (TX06/100)) ;
RCEL7RK_1 = arr(ACELRK_1 * (TX06/100)) ;

RCEL7RL = arr(ACELRL * (TX05/100)) ;
RCEL7RL_1 = arr(ACELRL_1 * (TX05/100)) ;

RCEL7IR = arr(ACELIR * (TX05/100)) ;
RCEL7IR_1 = arr(ACELIR_1 * (TX05/100)) ;

RCEL7IS = arr(ACELIS * (TX06/100)) ;
RCEL7IS_1 = arr(ACELIS_1 * (TX06/100)) ;

RCEL7IT = arr(ACELIT * (TX06/100)) ;
RCEL7IT_1 = arr(ACELIT_1 * (TX06/100)) ;

RCEL7IU = arr(ACELIU * (TX05/100)) ;
RCEL7IU_1 = arr(ACELIU_1 * (TX05/100)) ;

RCEL7RM = arr(ACELRM * (TX04/100)) ;
RCEL7RM_1 = arr(ACELRM_1 * (TX04/100)) ;

RCEL7RN = arr(ACELRN * (TX05/100)) ;
RCEL7RN_1 = arr(ACELRN_1 * (TX05/100)) ;

RCEL7RO = arr(ACELRO * (TX05/100)) ;
RCEL7RO_1 = arr(ACELRO_1 * (TX05/100)) ;

RCEL7RP = arr(ACELRP * (TX04/100)) ;
RCEL7RP_1 = arr(ACELRP_1 * (TX04/100)) ;

RCEL7IV = arr(ACELIV * (TX04/100)) ;
RCEL7IV_1 = arr(ACELIV_1 * (TX04/100)) ;

RCEL7IW = arr(ACELIW * (TX05/100)) ;
RCEL7IW_1 = arr(ACELIW_1 * (TX05/100)) ;

RCEL7IX = arr(ACELIX * (TX05/100)) ;
RCEL7IX_1 = arr(ACELIX_1 * (TX05/100)) ;

RCEL7IY = arr(ACELIY * (TX04/100)) ;
RCEL7IY_1 = arr(ACELIY_1 * (TX04/100)) ;

RCEL7RQ = arr(ACELRQ * (TX04/100)) ;
RCEL7RQ_1 = arr(ACELRQ_1 * (TX04/100)) ;

RCEL7IZ = arr(ACELIZ * (TX04/100)) ;
RCEL7IZ_1 = arr(ACELIZ_1 * (TX04/100)) ;

regle 401318:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 + RCELSOM5 + RCELSOM4 + RCELSOM6 + RCELSOM8 ;

RCELZM_1 = max(min(RCEL7ZM , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZM_1,RCELZM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZM ;

RCELZN_1 = max(min(RCEL7ZN , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZN_1,RCELZN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZN ;

RCELZA_1 = max(min(RCEL7ZA , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZA_1,RCELZA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZA ;

RCELZB_1 = max(min(RCEL7ZB , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZB_1,RCELZB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZB ;

RCELZG_1 = max(min(RCEL7ZG , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZG_1,RCELZG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZG ;

RCELZH_1 = max(min(RCEL7ZH , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZH_1,RCELZH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZH ;

RCELZC_1 = max(min(RCEL7ZC , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZC_1,RCELZC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZC ;

RCELZD_1 = max(min(RCEL7ZD , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZD_1,RCELZD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZD ;

RCELZE_1 = max(min(RCEL7ZE , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZE_1,RCELZE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZE ;

RCELZF_1 = max(min(RCEL7ZF , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELZF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELZF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELZF_1,RCELZF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELZF ;

RCELRI_1 = max(min(RCEL7RI , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRI_1,RCELRI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRI ;

RCELRJ_1 = max(min(RCEL7RJ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRJ_1,RCELRJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRJ ;

RCELRK_1 = max(min(RCEL7RK , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRK_1,RCELRK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRK ;

RCELRL_1 = max(min(RCEL7RL , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRL_1,RCELRL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRL ;

RCELIR_1 = max(min(RCEL7IR , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIR_1,RCELIR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIR ;

RCELIS_1 = max(min(RCEL7IS , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIS_1,RCELIS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIS ;

RCELIT_1 = max(min(RCEL7IT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIT_1,RCELIT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIT ;

RCELIU_1 = max(min(RCEL7IU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIU_1,RCELIU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIU ;

RCELRM_1 = max(min(RCEL7RM , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRM_1,RCELRM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRM ;

RCELRN_1 = max(min(RCEL7RN , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRN_1,RCELRN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRN ;

RCELRO_1 = max(min(RCEL7RO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRO_1,RCELRO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRO ;

RCELRP_1 = max(min(RCEL7RP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRP_1,RCELRP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRP ;

RCELIV_1 = max(min(RCEL7IV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIV_1,RCELIV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIV ;

RCELIW_1 = max(min(RCEL7IW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIW_1,RCELIW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIW ;

RCELIX_1 = max(min(RCEL7IX , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIX_1,RCELIX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIX ;

RCELIY_1 = max(min(RCEL7IY , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIY_1,RCELIY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELIY ;

RCELRQ_1 = max(min(RCEL7RQ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELRQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELRQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELRQ_1,RCELRQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELRQ ;

RCELIZ_1 = max(min(RCEL7IZ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELIZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELIZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELIZ_1,RCELIZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

DCELSOM7 = COD7ZM + COD7ZN + COD7ZA + COD7ZB + COD7ZG + COD7ZH + COD7ZC + COD7ZD + COD7ZE + COD7ZF + COD7RI + COD7RJ + COD7RK + COD7RL 
           + COD7IR + COD7IS + COD7IT + COD7IU + COD7RM + COD7RN + COD7RO + COD7RP + COD7IV + COD7IW + COD7IX + COD7IY + COD7RQ + COD7IZ ;

ACELSOM7 = ACELZM + ACELZN + ACELZA + ACELZB + ACELZG + ACELZH + ACELZC + ACELZD + ACELZE + ACELZF + ACELRI + ACELRJ + ACELRK + ACELRL 
           + ACELIR + ACELIS + ACELIT + ACELIU + ACELRM + ACELRN + ACELRO + ACELRP + ACELIV + ACELIW + ACELIX + ACELIY + ACELRQ + ACELIZ ;

RCELSOM7 = RCELZM + RCELZN + RCELZA + RCELZB + RCELZG + RCELZH + RCELZC + RCELZD + RCELZE + RCELZF + RCELRI + RCELRJ + RCELRK + RCELRL 
           + RCELIR + RCELIS + RCELIT + RCELIU + RCELRM + RCELRN + RCELRO + RCELRP + RCELIV + RCELIW + RCELIX + RCELIY + RCELRQ + RCELIZ ;
RCELSOM7_1 = min(RCEL7ZM_1+ RCEL7ZN_1+ RCEL7ZA_1+ RCEL7ZB_1+ RCEL7ZG_1+ RCEL7ZH_1+ RCEL7ZC_1+ RCEL7ZD_1+ RCEL7ZE_1+ RCEL7ZF_1+ RCEL7RI_1+ RCEL7RJ_1+ RCEL7RK_1
            + RCEL7RL_1+ RCEL7IR_1+ RCEL7IS_1+ RCEL7IT_1+ RCEL7IU_1+ RCEL7RM_1+ RCEL7RN_1+ RCEL7RO_1+ RCEL7RP_1+ RCEL7IV_1+ RCEL7IW_1+ RCEL7IX_1+ RCEL7IY_1+ RCEL7RQ_1+ RCEL7IZ_1,
	                                                       IDOM11-(DEC11 + REDUCAVTCEL + RCELSOM1_1 + RCELSOM2_1 + RCELSOM5_1 + RCELSOM4_1 + RCELSOM6_1 + RCELSOM8_1));
regle 401320:
application : iliad ;

ACELXH_1 = arr((min(COD7XH , LIMCELLIER) * (1 - COD7YE) + COD7XH * COD7YE) /3) * (1 - V_CNR) ;
ACELXH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELXH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELXH_1,ACELXH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELXI_1 = arr((min(COD7XI , LIMCELLIER) * (1 - COD7YE) + COD7XI * COD7YE) /3) * (1 - V_CNR) ;
ACELXI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELXI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELXI_1,ACELXI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELXJ_1 = arr((min(COD7XJ , LIMCELLIER) * (1 - COD7YE) + COD7XJ * COD7YE) /3) * (1 - V_CNR) ;
ACELXJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELXJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELXJ_1,ACELXJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELXK_1 = arr((min(COD7XK , LIMCELLIER) * (1 - COD7YE) + COD7XK * COD7YE) /3) * (1 - V_CNR) ;
ACELXK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELXK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELXK_1,ACELXK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJE_1 = arr((min(COD7JE , LIMCELLIER) * (1 - COD7YE) + COD7JE * COD7YE) /3) * (1 - V_CNR) ;
ACELJE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJE_1,ACELJE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJF_1 = arr((min(COD7JF , LIMCELLIER) * (1 - COD7YE) + COD7JF * COD7YE) /3) * (1 - V_CNR) ;
ACELJF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJF_1,ACELJF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJG_1 = arr((min(COD7JG , LIMCELLIER) * (1 - COD7YE) + COD7JG * COD7YE) /3) * (1 - V_CNR) ;
ACELJG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJG_1,ACELJG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJH_1 = arr((min(COD7JH , LIMCELLIER) * (1 - COD7YE) + COD7JH * COD7YE) /3) * (1 - V_CNR) ;
ACELJH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJH_1,ACELJH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJI_1 = arr((min(COD7JI , LIMCELLIER) * (1 - COD7YE) + COD7JI * COD7YE) /3) * (1 - V_CNR) ;
ACELJI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJI_1,ACELJI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJJ_1 = arr((min(COD7JJ , LIMCELLIER) * (1 - COD7YE) + COD7JJ * COD7YE) /3) * (1 - V_CNR) ;
ACELJJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJJ_1,ACELJJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJK_1 = arr((min(COD7JK , LIMCELLIER) * (1 - COD7YE) + COD7JK * COD7YE) /3) * (1 - V_CNR) ;
ACELJK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJK_1,ACELJK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACELJL_1 = arr((min(COD7JL , LIMCELLIER) * (1 - COD7YE) + COD7JL * COD7YE) /3) * (1 - V_CNR) ;
ACELJL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACELJL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACELJL_1,ACELJL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


RCEL7XH = arr(ACELXH * (TX05/100)) ;
RCEL7XH_1 = arr(ACELXH_1 * (TX05/100)) ;

RCEL7XI = arr(ACELXI * (TX06/100)) ;
RCEL7XI_1 = arr(ACELXI_1 * (TX06/100)) ;

RCEL7XJ = arr(ACELXJ * (TX06/100)) ;
RCEL7XJ_1 = arr(ACELXJ_1 * (TX06/100)) ;

RCEL7XK = arr(ACELXK * (TX05/100)) ;
RCEL7XK_1 = arr(ACELXK_1 * (TX05/100)) ;

RCEL7JE = arr(ACELJE * (TX05/100)) ;
RCEL7JE_1 = arr(ACELJE_1 * (TX05/100)) ;

RCEL7JF = arr(ACELJF * (TX06/100)) ;
RCEL7JF_1 = arr(ACELJF_1 * (TX06/100)) ;

RCEL7JG = arr(ACELJG * (TX06/100)) ;
RCEL7JG_1 = arr(ACELJG_1 * (TX06/100)) ;

RCEL7JH = arr(ACELJH * (TX05/100)) ;
RCEL7JH_1 = arr(ACELJH_1 * (TX05/100)) ;

RCEL7JI = arr(ACELJI * (TX04/100)) ;
RCEL7JI_1 = arr(ACELJI_1 * (TX04/100)) ;

RCEL7JJ = arr(ACELJJ * (TX05/100)) ;
RCEL7JJ_1 = arr(ACELJJ_1 * (TX05/100)) ;

RCEL7JK = arr(ACELJK * (TX05/100)) ;
RCEL7JK_1 = arr(ACELJK_1 * (TX05/100)) ;

RCEL7JL = arr(ACELJL * (TX04/100)) ;
RCEL7JL_1 = arr(ACELJL_1 * (TX04/100)) ;

regle 401322:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELSOM1 + RCELSOM2 + RCELSOM5 + RCELSOM4 + RCELSOM6 + RCELSOM8 + RCELSOM7 ;

RCELXH_1 = max(min(RCEL7XH , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELXH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXH_1,RCELXH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXH ;

RCELXI_1 = max(min(RCEL7XI , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELXI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXI_1,RCELXI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXI ;

RCELXJ_1 = max(min(RCEL7XJ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELXJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXJ_1,RCELXJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXJ ;

RCELXK_1 = max(min(RCEL7XK , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RCELXK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELXK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELXK_1,RCELXK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELXK ;

RCELJE_1 = max(min(RCEL7JE , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJE_1,RCELJE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJE ;

RCELJF_1 = max(min(RCEL7JF , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJF_1,RCELJF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJF ;

RCELJG_1 = max(min(RCEL7JG , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJG_1,RCELJG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJG ;

RCELJH_1 = max(min(RCEL7JH , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJH_1,RCELJH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJH ;

RCELJI_1 = max(min(RCEL7JI , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJI_1,RCELJI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJI ;

RCELJJ_1 = max(min(RCEL7JJ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJJ_1,RCELJJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJJ ;

RCELJK_1 = max(min(RCEL7JK , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJK_1,RCELJK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCELJK ;

RCELJL_1 = max(min(RCEL7JL , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ; 
RCELJL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCELJL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCELJL_1,RCELJL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

DCELSOM3 = COD7XH + COD7XI + COD7XJ + COD7XK + COD7JE + COD7JF + COD7JG + COD7JH + COD7JI + COD7JJ + COD7JK + COD7JL ;

ACELSOM3 = ACELXH + ACELXI + ACELXJ + ACELXK + ACELJE + ACELJF + ACELJG + ACELJH + ACELJI + ACELJJ + ACELJK + ACELJL ;

RCELSOM3 = RCELXH + RCELXI + RCELXJ + RCELXK + RCELJE + RCELJF + RCELJG + RCELJH + RCELJI + RCELJJ + RCELJK + RCELJL ;
RCELSOM3_1 = min(RCEL7XH_1+ RCEL7XI_1+ RCEL7XJ_1+ RCEL7XK_1+ RCEL7JE_1+ RCEL7JF_1+ RCEL7JG_1+ RCEL7JH_1+ RCEL7JI_1+ RCEL7JJ_1+ RCEL7JK_1+ RCEL7JL_1, 
                                 IDOM11 - ( DEC11 + REDUCAVTCEL + RCELSOM1_1 + RCELSOM2_1 + RCELSOM5_1 + RCELSOM4_1 + RCELSOM6_1 + RCELSOM8_1 + RCELSOM7_1));
RCELTOT = RCELSOM1 + RCELSOM2 + RCELSOM5 + RCELSOM4 + RCELSOM6 + RCELSOM8 + RCELSOM7 + RCELSOM3 ;
RCELTOT_1 = RCELSOM1_1 + RCELSOM2_1 + RCELSOM5_1 + RCELSOM4_1 + RCELSOM6_1 + RCELSOM8_1 + RCELSOM7_1 + RCELSOM3_1 ;

regle 401324:
application : iliad ;


RIVCELZMN1 = arr((ACELZM + ACELZN) * TX06/100) * (1 - V_CNR) ;

RIVCELZMN3 = (arr(min(COD7ZM + COD7ZN , LIMCELLIER) * TX06/100) - (2 * RIVCELZMN1)) * (1 - V_CNR) ;

REPCELZMN = RIVCELZMN1 + RIVCELZMN3 ;

RIVCELZAB1 = arr((ACELZA + ACELZB + ACELZG + ACELZH) * TX06/100) * (1 - V_CNR) ;

RIVCELZAB3 = (arr(min(COD7ZA + COD7ZB + COD7ZG + COD7ZH , LIMCELLIER) * TX06/100) - (2 * RIVCELZAB1)) * (1 - V_CNR) ;

REPCELZAB = RIVCELZAB1 + RIVCELZAB3 ;

RIVCELSIJ1 = arr((ACELZC + ACELZF + ACELRI + ACELRL + ACELIR + ACELIU) * (TX05/100) + (ACELZD + ACELZE + ACELRJ + ACELRK + ACELIS + ACELIT) * (TX06/100)) * (1 - V_CNR) ;

RIVCELSIJ3 = (arr(min(COD7ZC + COD7ZF + COD7RI + COD7RL + COD7IR + COD7IU , LIMCELLIER) * TX05/100
                   + min(COD7ZD + COD7ZE + COD7RJ + COD7RK + COD7IS + COD7IT , LIMCELLIER) * TX06/100) - (2 * RIVCELSIJ1)) * (1 - V_CNR) ;

REPCELSIJKL = RIVCELSIJ1 + RIVCELSIJ3 ;

RIVCELRMN1 = arr((ACELRM + ACELRP + ACELIV + ACELIY) * (TX04/100) + (ACELRN + ACELRO + ACELIW + ACELIX) * (TX05/100)) * (1 - V_CNR) ;

RIVCELRMN3 = (arr(min(COD7RM + COD7RP + COD7IV + COD7IY , LIMCELLIER) * TX04/100
                   + min(COD7RN + COD7RO + COD7IW + COD7IX , LIMCELLIER) * TX05/100) - (2 * RIVCELRMN1)) * (1 - V_CNR) ;

REPCELRMNOP = RIVCELRMN1 + RIVCELRMN3 ;

RIVCELRQ1 = arr((ACELRQ + ACELIZ) * TX04/100) * (1 - V_CNR) ;

RIVCELRQ3 = (arr(min(COD7RQ + COD7IZ , LIMCELLIER) * TX04/100) - (2 * RIVCELRQ1)) * (1 - V_CNR) ;

REPCELRQ = RIVCELRQ1 + RIVCELRQ3 ;

RIVCELXHI1 = arr((ACELXH + ACELXK + ACELJE + ACELJH) * (TX05/100) + (ACELXI + ACELXJ + ACELJF + ACELJG) * (TX06/100)) * (1 - V_CNR) ;

RIVCELXHI3 = (arr(min(COD7XH + COD7XK + COD7JE + COD7JH , LIMCELLIER) * TX05/100
                   + min(COD7XI + COD7XJ + COD7JF + COD7JG , LIMCELLIER) * TX06/100) - (2 * RIVCELXHI1)) * (1 - V_CNR) ;

REPCELXHIJK = RIVCELXHI1 + RIVCELXHI3 ;

RIVCELJIJ1 = arr((ACELJI + ACELJL) * (TX04/100) + (ACELJJ + ACELJK) * (TX05/100)) * (1 - V_CNR) ;

RIVCELJIJ3 = (arr(min(COD7JI + COD7JL , LIMCELLIER) * TX04/100 + min(COD7JJ + COD7JK , LIMCELLIER) * TX05/100) - (2 * RIVCELJIJ1)) * (1 - V_CNR) ;

REPCELJIJ = RIVCELJIJ1 + RIVCELJIJ3 ;

regle 401350:
application : iliad ;


RRCELLJ = max(0 , CELRREDLJ - RCELRREDLJ) * (1 - V_CNR) ; 

RRCELLP = max(0 , CELRREDLP - RCELRREDLP) * (1 - V_CNR) ; 

RRCELLV = max(0 , CELRREDLV - RCELRREDLV) * (1 - V_CNR) ; 

RRCELLY = max(0 , COD7LY - RCELLY) * (1 - V_CNR) ; 

RRCELMV = max(0 , COD7MV - RCELMV) * (1 - V_CNR) ; 

RRCELMR = max(0 , COD7MR - RCELMR) * (1 - V_CNR) ;

RRCELA = max(0 , ACELREPGJ + ACELREPYB + ACELREPYM + ACELREPYT + ACELREPWX + ACELREPWT + ACELRT + ACELRV
                 - RCELREPGJ - RCELREPYB - RCELREPYM - RCELREPYT - RCELREPWX - RCELREPWT - RCELRT - RCELRV) * (1 - V_CNR) ;

RRCELLI = max(0, CELRREDLI - RCELRREDLI) * (1 - V_CNR) ;

RRCELLO = max(0, CELRREDLO - RCELRREDLO) * (1 - V_CNR) ;

RRCELLU = max(0, CELRREDLU - RCELRREDLU) * (1 - V_CNR) ;

RRCELLC = max(0, COD7LC - RCELLC) * (1 - V_CNR) ;

RRCELMU = max(0 , COD7MU - RCELMU) * (1 - V_CNR) ; 

RRCELMQ = max(0 , COD7MQ - RCELMQ) * (1 - V_CNR) ; 

RRCELB = max(0 , ACELREPGL + ACELREPYD + ACELREPYN + ACELREPYU + ACELREPWY + ACELREPWU + ACELRU  + ACELRW
                 - RCELREPGL - RCELREPYD - RCELREPYN - RCELREPYU - RCELREPWY - RCELREPWU - RCELRU - RCELRW) * (1 - V_CNR) ;

RRCELLH = max(0 , CELRREDLH - RCELRREDLH) * (1 - V_CNR) ;

RRCELLL = max(0 , CELRREDLL - RCELRREDLL) * (1 - V_CNR) ;

RRCELLR = max(0 , CELRREDLR - RCELRREDLR) * (1 - V_CNR) ;

RRCELLB = max(0 , COD7LB - RCELLB) * (1 - V_CNR) ;

RRCELMT = max(0 , COD7MT - RCELMT) * (1 - V_CNR) ; 

RRCELMP = max(0 , COD7MP - RCELMP) * (1 - V_CNR) ; 

RRCELC = max(0 , ACELREPGS + ACELREPYF + ACELREPYO + ACELREPYV + ACELREPWZ + ACELREPWV 
                 - RCELREPGS - RCELREPYF - RCELREPYO - RCELREPYV - RCELREPWZ - RCELREPWV) * (1 - V_CNR) ;

RRCELLG = max(0 , CELRREDLG - RCELRREDLG) * (1 - V_CNR) ; 

RRCELLK = max(0 , CELRREDLK - RCELRREDLK) * (1 - V_CNR) ; 

RRCELLQ = max(0 , CELRREDLQ - RCELRREDLQ) * (1 - V_CNR) ; 

RRCELLA = max(0 , COD7LA - RCELLA) * (1 - V_CNR) ; 

RRCELMS = max(0 , COD7MS - RCELMS) * (1 - V_CNR) ; 

RRCELMO = max(0 , COD7MO - RCELMO) * (1 - V_CNR) ; 

RRCELD = max(0 , ACELREPGU + ACELREPYH + ACELREPYP + ACELREPYW + ACELREPWW
                 - RCELREPGU - RCELREPYH - RCELREPYP - RCELREPYW - RCELREPWW) * (1 - V_CNR) ;

RRCELYI = max(0 , COD7YI - RCELYI) * (1 - V_CNR) ;

RRCELZI = max(0 , COD7ZI - RCELZI) * (1 - V_CNR) ;

RRCELE = max(0 , RCEL7ZB + RCEL7ZM + RCEL7ZH + RCEL7ZN + ACELWA + ACELNO + ACELNQ 
                 - RCELZB - RCELZM - RCELZH - RCELZN - RCELWA - RCELNO - RCELNQ) * (1 - V_CNR) ;

RRCELZP = max(0 , COD7ZP - RCELZP) * (1 - V_CNR) ;

RRCELXP = max(0 , COD7XP - RCELXP) * (1 - V_CNR) ;

RRCELYJ = max(0 , COD7YJ - RCELYJ) * (1 - V_CNR) ;

RRCELZJ = max(0 , COD7ZJ - RCELZJ) * (1 - V_CNR) ;

RRCELF = max(0 , RCEL7ZA + RCEL7ZD + RCEL7ZG + RCEL7RJ + RCEL7IS + ACELWB + ACELWE + ACELNP + ACELNR + ACELNU
                 - RCELZA - RCELZD - RCELZG - RCELRJ - RCELIS - RCELWB - RCELWE - RCELNP - RCELNR - RCELNU) * (1 - V_CNR) ;

RRCELZO = max(0 , COD7ZO - RCELZO) * (1 - V_CNR) ;

RRCELXO = max(0 , COD7XO - RCELXO) * (1 - V_CNR) ;

RRCELYK = max(0 , COD7YK - RCELYK) * (1 - V_CNR) ;

RRCELZK = max(0 , COD7ZK - RCELZK) * (1 - V_CNR) ;

RRCELG = max(0 , RCEL7ZC + RCEL7ZE + RCEL7ZF + RCEL7RI + RCEL7RK + RCEL7RL + RCEL7RN + RCEL7IR + RCEL7IT + RCEL7IU + RCEL7IW + ACELWC + ACELWF + ACELNS + ACELNV
                 - RCELZC - RCELZE - RCELZF - RCELRI - RCELRK - RCELRL - RCELRN - RCELIR - RCELIT - RCELIU - RCELIW - RCELWC - RCELWF - RCELNS - RCELNV) * (1 - V_CNR) ;

RRCELXQ = max(0 , COD7XQ - RCELXQ) * (1 - V_CNR) ;

RRCELYL = max(0 , COD7YL - RCELYL) * (1 - V_CNR) ;

RRCELZL = max(0 , COD7ZL - RCELZL) * (1 - V_CNR) ;

RRCELH = max(0 , RCEL7RM + RCEL7RO + RCEL7RP + RCEL7RQ + RCEL7IV + RCEL7IX + RCEL7IY + RCEL7IZ + ACELWD + ACELWG + ACELNT + ACELNW
                 - RCELRM - RCELRO - RCELRP - RCELRQ - RCELIV - RCELIX - RCELIY - RCELIZ - RCELWD - RCELWG - RCELNT - RCELNW) * (1 - V_CNR) ;

RRCELKD = max(0 , COD7KD - RCELKD) * (1 - V_CNR) ;

RRCELI = max(0 , RCEL7XI + RCEL7JF + ACELKB - RCELXI - RCELJF - RCELKB) * (1 - V_CNR) ;

RRCELKC = max(0 , COD7KC - RCELKC) * (1 - V_CNR) ;

RRCELJ = max(0 , RCEL7XH + RCEL7XJ + RCEL7XK + RCEL7JE + RCEL7JG + RCEL7JH + RCEL7JJ + ACELKA
                 - RCELXH - RCELXJ - RCELXK - RCELJE - RCELJG - RCELJH - RCELJJ - RCELKA) * (1 - V_CNR) ;

RRCELK = max(0 , RCEL7JI + RCEL7JK + RCEL7JL - RCELJI - RCELJK - RCELJL) * (1 - V_CNR) ;

regle 401370:
application : iliad ;

RITOUR = arr(COD7UY * TX_REDIL25 / 100) + arr(COD7UZ * TX_REDIL20 / 100) ;


DTOURREP = COD7UY ;

ATOURREP_1 = DTOURREP ;
ATOURREP = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ATOURREP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(ATOURREP_1,ATOURREP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DTOUREPA = COD7UZ ;

ATOUREPA_1 = DTOUREPA ;
ATOUREPA = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ATOUREPA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(ATOUREPA_1,ATOUREPA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401380:
application : iliad ;

RTOURREP_1 = max(min(arr(DTOURREP * TX_REDIL25 /100) , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV
                                                         -RPENTOT-RRSOFON-RFOR) , 0) ;
RTOURREP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RTOURREP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RTOURREP_1,RTOURREP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RTOUREPA_1 = max(min(arr(DTOUREPA * TX_REDIL20 /100) , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV
                                                         -RPENTOT-RRSOFON-RFOR-RTOURREP) , 0) ;
RTOUREPA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RTOUREPA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RTOUREPA_1,RTOUREPA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RTOUR = RTOURREP ;

regle 401390 :
application : iliad ;

RRI1 = IDOM11 - DEC11 - RCOTFOR - RREPA - RDIFAGRI - RPRESSE - RFORET - RFIPDOM - RFIPC - RCINE
              - RRESTIMO - RSOCREPR - RRPRESCOMP - RHEBE - RSURV - RINNO - RSOUFIP - RRIRENOV ;

regle 401400 :
application : iliad ;


BAH = (min (RVCURE,LIM_CURE) + min(RCCURE,LIM_CURE)) * (1 - V_CNR) ;

RAH = arr (BAH * TX_CURE /100) ;

DHEBE = RVCURE + RCCURE ;

AHEBE = positif(null(V_IND_TRAIT-4)+COD9ZA) * BAH * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(BAH,BAH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RHEBE_1 = max( min( RAH , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC
			-RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP) , 0 );
RHEBE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RHEBE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RHEBE_1,RHEBE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401410:
application : iliad ;


DREPA = RDREP + DONETRAN ;

EXCEDANTA = max(0 , RDREP + DONETRAN - PLAF_REDREPAS) ;

BAALIM = min(DREPA , PLAF_REDREPAS) * (1 - V_CNR) ;

RAALIM = arr(BAALIM * TX_REDREPAS/100) * (1 - V_CNR) ;

AREPA = positif(null(V_IND_TRAIT-4)+COD9ZA) * BAALIM * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(BAALIM,BAALIM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RREPA_1 = max(min(RAALIM , IDOM11-DEC11-RCOTFOR) , 0) ;
RREPA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RREPA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RREPA_1,RREPA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401420:
application : iliad ;
 
DNOUV = REPSNO3 + COD7CQ + COD7CR + COD7CV + COD7CX + RDSNO + COD7CH ;

BSN1 = min (REPSNO3 , LIM_SOCNOUV2 * (1 + BOOL_0AM)) ;

BSN2 = min (COD7CQ + COD7CR + COD7CV + COD7CX + RDSNO + COD7CH , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1) ;


BSNCL = min(REPSNO3 , LIM_SOCNOUV2 * (1 + BOOL_0AM)) ;
RSNNCL =  BSNCL * TX18/100 ;

BSNCQ = max(0, min(COD7CQ , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1)) ;
RSNNCQ = BSNCQ * TX18/100 ;

BSNCR = max(0, min(COD7CR , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1 - BSNCQ)) ;
RSNNCR = BSNCR * TX18/100 ;

BSNCV = max(0, min(COD7CV , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1 - BSNCQ - BSNCR)) ;
RSNNCV = BSNCV * TX18/100 ;

BSNCX = max(0, min(COD7CX , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1 - BSNCQ - BSNCR - BSNCV)) ;
RSNNCX = BSNCX * TX18/100 ;

BSNCH = max(0, min(COD7CH , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1 - BSNCQ - BSNCR - BSNCV - BSNCX)) ;
RSNNCH = BSNCH * TX25/100 ;

BSNCF = max(0, min(RDSNO , LIM_TITPRISE * (1 + BOOL_0AM) - BSN1 - BSNCQ - BSNCR - BSNCV - BSNCX - BSNCH)) ;
RSNNCF = BSNCF * TX18/100 ;

RSN = arr(RSNNCL + RSNNCQ + RSNNCR  + RSNNCV + RSNNCX + RSNNCH + RSNNCF) * (1 - V_CNR) ;

ANOUV = (positif(null(V_IND_TRAIT-4) + COD9ZA) * (BSN1 + BSN2) * (1 - positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
         + (max(0,min(BSN1 + BSN2 , BSN11731 + BSN11731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5) + 0) * (1 - V_CNR) ;

regle 401430:
application : iliad ;


VARTMP1 = RLOGDOM + RCOMP + RRETU + RDONS + CRDIE + RDUFREP + RPINELTOT + RNORMTOT ;

RSNCL_1 = max(0 , min(RSNNCL , RRI1 - VARTMP1)) ;
RSNCL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCL_1,RSNCL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCL ;

RSNCC_1 = max(0, min(RSNNCC , RRI1 - VARTMP1)) ;
RSNCC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCC_1,RSNCC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCC ;

RSNCQ_1 = max(0, min(RSNNCQ , RRI1 - VARTMP1)) ;
RSNCQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCQ_1,RSNCQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCQ ;

RSNCR_1 = max(0, min(RSNNCR , RRI1 - VARTMP1)) ;
RSNCR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCR_1,RSNCR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCR ;

RSNCV_1 = max(0, min(RSNNCV , RRI1 - VARTMP1)) ;
RSNCV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCV_1,RSNCV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCV ;

RSNCX_1 = max(0, min(RSNNCX , RRI1 - VARTMP1)) ;
RSNCX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCX_1,RSNCX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCX ;

RSNCH_1 = max(0, min(RSNNCH , RRI1 - VARTMP1)) ;
RSNCH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCH_1,RSNCH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCH ;

RSNCF_1 = max(0, min(RSNNCF , RRI1 - VARTMP1)) ;
RSNCF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSNCF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSNCF_1,RSNCF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RSNCF ;

RNOUV_1 = max(min(RSN , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT) , 0) ;
RNOUV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RNOUV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RNOUV_1,RNOUV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401440:
application : iliad ;


DPENTCY = COD7CY ;
APENTCY_1 = COD7CY * positif(COD7CY) * (1 - V_CNR) ;
APENTCY = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APENTCY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(APENTCY_1,APENTCY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPENTCY_1 = max(min(APENTCY , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV) , 0) ;
RPENTCY = positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPENTCY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPENTCY_1,RPENTCY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DPENTDY = COD7DY ;
APENTDY_1 = COD7DY * positif(COD7DY) * (1 - V_CNR) ;
APENTDY = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APENTDY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(APENTDY_1,APENTDY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPENTDY_1 = max(min(APENTDY , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTCY) , 0) ;
RPENTDY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPENTDY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPENTDY_1,RPENTDY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DPENTEY = COD7EY ;
APENTEY_1 = COD7EY * positif(COD7EY) * (1 - V_CNR) ;
APENTEY = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APENTEY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(APENTEY_1,APENTEY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPENTEY_1 = max(min(APENTEY , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTCY- RPENTDY) , 0) ;
RPENTEY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPENTEY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPENTEY_1,RPENTEY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DPENTFY = COD7FY ;
APENTFY_1 = COD7FY * positif(COD7FY) * (1 - V_CNR) ;
APENTFY = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APENTFY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(APENTFY_1,APENTFY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPENTFY_1 = max(min(APENTFY , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTCY-RPENTDY-RPENTEY) , 0);
RPENTFY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPENTFY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPENTFY_1,RPENTFY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DPENTGY = COD7GY ;
APENTGY_1 = COD7GY * positif(COD7GY) * (1 - V_CNR) ;
APENTGY = positif(null(V_IND_TRAIT-4)+COD9ZA) * (APENTGY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(APENTGY_1,APENTGY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPENTGY_1 =  max(min(APENTGY , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTCY-RPENTDY-RPENTEY-RPENTFY) , 0) ;
RPENTGY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RPENTGY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RPENTGY_1,RPENTGY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPENTOT = RPENTCY + RPENTDY + RPENTEY + RPENTFY + RPENTGY ;
RPENTOT_1 = RPENTCY_1 + RPENTDY_1 + RPENTEY_1 + RPENTFY_1 + RPENTGY_1 ;

regle 401450:
application : iliad ;


RINVPECR = max(0 , COD7CR - max(0 , (LIM_TITPRISE * (1 + BOOL_0AM)) - max(0 , min(BSNCL , LIM_SOCNOUV2 * (1 + BOOL_0AM)) + COD7CQ))) 
           * (1 - V_CNR) ;

RINVPECV = max(0 , COD7CV - max(0 , (LIM_TITPRISE * (1 + BOOL_0AM)) 
			     - max(0 , min(BSNCL , LIM_SOCNOUV2 * (1 + BOOL_0AM)) + COD7CQ + COD7CR)))
	       * (1 - V_CNR) ;

RINVPECX = max(0 , COD7CX - max(0 , (LIM_TITPRISE * (1 + BOOL_0AM)) 
			     - max(0 , min(BSNCL , LIM_SOCNOUV2 * (1 + BOOL_0AM)) + COD7CQ + COD7CR + COD7CV))) 
	       * (1 - V_CNR) ;


RINVPECH = max(0 , COD7CH - max(0 , (LIM_TITPRISE * (1 + BOOL_0AM))
                             - max(0 , min(BSNCL , LIM_SOCNOUV2 * (1 + BOOL_0AM)) + COD7CQ + COD7CR + COD7CV + COD7CX)))
			                    * (1 - V_CNR) ;

RINVPECF = max(0 , RDSNO - max(0 , (LIM_TITPRISE * (1 + BOOL_0AM)) 
			     - max(0 , min(BSNCL , LIM_SOCNOUV2 * (1 + BOOL_0AM)) + COD7CQ + COD7CR + COD7CV + COD7CX + COD7CH))) 
	       * (1 - V_CNR) ;

regle 401455:
application : iliad ;


DSOFON = COD7GW ;

LIMSOFON = max(0, LIM_TITPRISE * (1 + BOOL_0AM) - (BSN1 + BSN2)) ;
ASOFON_1 = min(COD7GW , LIMSOFON) * (1 - V_CNR) ;
ASOFON = (positif(null(V_IND_TRAIT-4)+COD9ZA) * (ASOFON_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ASOFON_1,ASOFON1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0) * (1 - V_CNR) ;

RSOFON = arr(ASOFON * TX25/100) ;

RRSOFON_1 =  max(min(RSOFON , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTCY-RPENTDY-RPENTEY-RPENTFY-RPENTGY) , 0) * (1 - V_CNR);
RRSOFON =(positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRSOFON_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRSOFON_1,RRSOFON1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0)* (1 - V_CNR);
regle 401456:
application : iliad ;


RSOUSGW = max(0 , COD7GW - max(0 , (LIM_TITPRISE * (1 + BOOL_0AM)) - (BSN1 + BSN2))) * (1 - V_CNR);
regle 401460:
application : iliad ;



PLAFREPETOT = arr(max(0 ,RSNCF + RSNCH + RRSOFON + RSNCQ + RSNCR + RSNCV + RSNCX + RPENTCY + RPENTDY + RPENTEY + RPENTFY + RPENTGY - 10000)) * (1 - V_CNR) * positif(AVFISCOPTER) ;

RPLAFPME20 = arr(max(0 , RSNCH + RSNCF - 10000)) * (1 - V_CNR) * positif(AVFISCOPTER) ;
RPLAFSOCFO20 = arr(max(0 , RSNCH + RSNCF + RRSOFON - (10000 + RPLAFPME20))) * (1 - V_CNR) * positif(AVFISCOPTER) ;

RPLAFPME16 = arr(max(0 ,RSNCH + RSNCF + RRSOFON + RSNCQ + RPENTDY - (10000 + RPLAFPME20 + RPLAFSOCFO20))) * positif(AVFISCOPTER) ;

RPLAFPME17 = arr(max(0 ,RSNCH + RSNCF +  RRSOFON +  RSNCQ + RPENTDY + RSNCR + RPENTEY - (10000 + RPLAFPME20 + RPLAFSOCFO20 + RPLAFPME16))) * positif(AVFISCOPTER) ;

RPLAFPME18 = arr(max(0 , RSNCH + RSNCF + RRSOFON + RSNCQ + RPENTDY + RSNCR + RPENTEY + RSNCV + RPENTFY - (10000 +  RPLAFPME20 + RPLAFSOCFO20 + RPLAFPME16 + RPLAFPME17 ))) * positif(AVFISCOPTER) ;

RPLAFPME19 = arr(max(0 , RSNCH + RSNCF + RRSOFON + RSNCQ + RPENTDY + RSNCR + RPENTEY + RSNCV + RPENTFY+RSNCX+RPENTGY - (10000 +  RPLAFPME20 + RPLAFSOCFO20 + RPLAFPME16 + RPLAFPME17 + RPLAFPME18 ))) * positif(AVFISCOPTER) ;

regle 401470:
application : iliad ;


AILMPF_1 = COD7PF * (1 - V_CNR) ;
AILMPF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPF_1,AILMPF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPK_1 = COD7PK * (1 - V_CNR) ;
AILMPK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPK_1,AILMPK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPP_1 = COD7PP * (1 - V_CNR) ;
AILMPP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPP_1,AILMPP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPU_1 = COD7PU * (1 - V_CNR) ;
AILMPU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPU_1,AILMPU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHO_1 = COD7HO * (1 - V_CNR) ;
AILMHO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHO_1,AILMHO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHT_1 = COD7HT * (1 - V_CNR) ;
AILMHT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHT_1,AILMHT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPG_1 = COD7PG * (1 - V_CNR) ;
AILMPG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPG_1,AILMPG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPL_1 = COD7PL * (1 - V_CNR) ;
AILMPL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPL_1,AILMPL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPQ_1 = COD7PQ * (1 - V_CNR) ;
AILMPQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPQ_1,AILMPQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPV_1 = COD7PV * (1 - V_CNR) ;
AILMPV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPV_1,AILMPV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHP_1 = COD7HP * (1 - V_CNR) ;
AILMPF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPF_1,AILMPF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHU_1 = COD7HU * (1 - V_CNR) ;
AILMHU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHU_1,AILMHU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPH_1 = COD7PH * (1 - V_CNR) ;
AILMPH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPH_1,AILMPH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPM_1 = COD7PM * (1 - V_CNR) ;

AILMPM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPM_1,AILMPM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
AILMPR_1 = COD7PR * (1 - V_CNR) ;
AILMPR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPR_1,AILMPR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPW_1 = COD7PW * (1 - V_CNR) ;
AILMPW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPW_1,AILMPW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHQ_1 = COD7HQ * (1 - V_CNR) ;
AILMHQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHQ_1,AILMHQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHV_1 = COD7HV * (1 - V_CNR) ;
AILMHV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHV_1,AILMHV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPI_1 = COD7PI * (1 - V_CNR) ;
AILMPI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPI_1,AILMPI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPN_1 = COD7PN * (1 - V_CNR) ;
AILMPN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPN_1,AILMPN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPS_1 = COD7PS * (1 - V_CNR) ;
AILMPS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPS_1,AILMPS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPX_1 = COD7PX * (1 - V_CNR) ;
AILMPX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPX_1,AILMPX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHR_1 = COD7HR * (1 - V_CNR) ;
AILMHR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHR_1,AILMHR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHW_1 = COD7HW * (1 - V_CNR) ;
AILMHW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHW_1,AILMHW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPJ_1 = COD7PJ * (1 - V_CNR) ;
AILMPJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPJ_1,AILMPJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPO_1 = COD7PO * (1 - V_CNR) ;
AILMPO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPO_1,AILMPO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPT_1 = COD7PT * (1 - V_CNR) ;
AILMPT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPT_1,AILMPT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMPY_1 = COD7PY * (1 - V_CNR) ;
AILMPY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMPY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMPY_1,AILMPY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHS_1 = COD7HS * (1 - V_CNR) ;
AILMHS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHS_1,AILMHS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

AILMHX_1 = COD7HX * (1 - V_CNR) ;
AILMHX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMHX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMHX_1,AILMHX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DILMNP1 = COD7PF + COD7PK + COD7PP + COD7PU + COD7HO + COD7HT + COD7PG + COD7PL + COD7PQ + COD7PV 
          + COD7HP + COD7HU + COD7PH + COD7PM + COD7PR + COD7PW + COD7HQ + COD7HV + COD7PI + COD7PN 
	  + COD7PS + COD7PX + COD7HR + COD7HW + COD7PJ + COD7PO + COD7PT + COD7PY + COD7HS + COD7HX ;

AILMNP1 = AILMPF + AILMPK + AILMPP + AILMPU + AILMHO + AILMHT + AILMPG + AILMPL + AILMPQ + AILMPV 
          + AILMHP + AILMHU + AILMPH + AILMPM + AILMPR + AILMPW + AILMHQ + AILMHV + AILMPI + AILMPN 
	  + AILMPS + AILMPX + AILMHR + AILMHW + AILMPJ + AILMPO + AILMPT + AILMPY + AILMHS + AILMHX ;


BILMJY = min(LIMREPLOC8 , LOCMEUBJY) * (1 - COD7OZ) + LOCMEUBJY * COD7OZ ;
AILMJY_1 = BILMJY * (1 - V_CNR) ;
AILMJY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMJY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMJY_1,AILMJY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMJX = min(LIMREPLOC6 , LOCMEUBJX) * (1 - COD7OZ) + LOCMEUBJX * COD7OZ ;
AILMJX_1 = BILMJX * (1 - V_CNR) ;
AILMJX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMJX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMJX_1,AILMJX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMJW = min(LIMREPLOC5 , LOCMEUBJW) * (1 - COD7OZ) + LOCMEUBJW * COD7OZ ;
AILMJW_1 = BILMJW * (1 - V_CNR) ;
AILMJW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMJW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMJW_1,AILMJW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMJV = min(LIMREPLOC2 , LOCMEUBJV) * (1 - COD7OZ) + LOCMEUBJV * COD7OZ ;
AILMJV_1 = BILMJV * (1 - V_CNR) ;
AILMJV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMJV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMJV_1,AILMJV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOE = min(LIMREPLOC8 , COD7OE) * (1 - COD7OZ) + COD7OE * COD7OZ ;
AILMOE_1 = BILMOE * (1 - V_CNR) ;
AILMOE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOE_1,AILMOE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOD = min(LIMREPLOC8 , COD7OD) * (1 - COD7OZ) + COD7OD * COD7OZ ;
AILMOD_1 = BILMOD * (1 - V_CNR) ;
AILMOD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOD_1,AILMOD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOC = min(LIMREPLOC6 , COD7OC) * (1 - COD7OZ) + COD7OC * COD7OZ ;
AILMOC_1 = BILMOC * (1 - V_CNR) ;
AILMOC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOC_1,AILMOC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOB = min(LIMREPLOC5 , COD7OB) * (1 - COD7OZ) + COD7OB * COD7OZ ;
AILMOB_1 = BILMOB * (1 - V_CNR) ;
AILMOB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOB_1,AILMOB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOA = min(LIMREPLOC1 , COD7OA) * (1 - COD7OZ) + COD7OA * COD7OZ ;
AILMOA_1 = BILMOA * (1 - V_CNR) ;
AILMOA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOA_1,AILMOA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOJ = min(LIMREPLOC8 , COD7OJ) * (1 - COD7OZ) + COD7OJ * COD7OZ ;
AILMOJ_1 = BILMOJ * (1 - V_CNR) ;
AILMOJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOJ_1,AILMOJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOI = min(LIMREPLOC8 , COD7OI) * (1 - COD7OZ) + COD7OI * COD7OZ ;
AILMOI_1 = BILMOI * (1 - V_CNR) ;
AILMOI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOI_1,AILMOI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOH = min(LIMREPLOC6 , COD7OH) * (1 - COD7OZ) + COD7OH * COD7OZ ;
AILMOH_1 = BILMOH * (1 - V_CNR) ;
AILMOH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOH_1,AILMOH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOG = min(LIMREPLOC5 , COD7OG) * (1 - COD7OZ) + COD7OG * COD7OZ ;
AILMOG_1 = BILMOG * (1 - V_CNR) ;
AILMOG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOG_1,AILMOG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOF = min(LIMREPLOC3 , COD7OF) * (1 - COD7OZ) + COD7OF * COD7OZ ;
AILMOF_1 = BILMOF * (1 - V_CNR) ;
AILMOF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOF_1,AILMOF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOO = min(LIMREPLOC8 , COD7OO) * (1 - COD7OZ) + COD7OO * COD7OZ ;
AILMOO_1 = BILMOO * (1 - V_CNR) ;
AILMOO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOO_1,AILMOO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMON = min(LIMREPLOC8 , COD7ON) * (1 - COD7OZ) + COD7ON * COD7OZ ;
AILMON_1 = BILMON * (1 - V_CNR) ;
AILMON =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMON_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMON_1,AILMON1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOM = min(LIMREPLOC6 , COD7OM) * (1 - COD7OZ) + COD7OM * COD7OZ ;
AILMOM_1 = BILMOM * (1 - V_CNR) ;
AILMOM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOM_1,AILMOM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOL = min(LIMREPLOC5 , COD7OL) * (1 - COD7OZ) + COD7OL * COD7OZ ;
AILMOL_1 = (BILMOL) * (1 - V_CNR) ;
AILMOL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOL_1,AILMOL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOK = min(LIMREPLOC11 , COD7OK) * (1 - COD7OZ) + COD7OK * COD7OZ ;
AILMOK_1 = (BILMOK) * (1 - V_CNR) ;
AILMOK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOK_1,AILMOK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOT = min(LIMREPLOC8 , COD7OT) * (1 - COD7OZ) + COD7OT * COD7OZ ;
AILMOT_1 = (BILMOT) * (1 - V_CNR) ;
AILMOT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOT_1,AILMOT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOS = min(LIMREPLOC8 , COD7OS) * (1 - COD7OZ) + COD7OS * COD7OZ ;
AILMOS_1 = (BILMOS) * (1 - V_CNR) ;
AILMOS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOS_1,AILMOS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOR = min(LIMREPLOC6 , COD7OR) * (1 - COD7OZ) + COD7OR * COD7OZ ;
AILMOR_1 = (BILMOR) * (1 - V_CNR) ;
AILMOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOR_1,AILMOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOQ = min(LIMREPLOC5 , COD7OQ) * (1 - COD7OZ) + COD7OQ * COD7OZ ;
AILMOQ_1 = (BILMOQ) * (1 - V_CNR) ;
AILMOQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOQ_1,AILMOQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMOP = min(LIMREPLOC11 , COD7OP) * (1 - COD7OZ) + COD7OP * COD7OZ ;
AILMOP_1 = (BILMOP) * (1 - V_CNR) ;
AILMOP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMOP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMOP_1,AILMOP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMSA = min(LIMREPLOC11 , COD7SA) * (1 - COD7OZ) + COD7SA * COD7OZ ;
AILMSA_1 = (BILMSA) * (1 - V_CNR) ;
AILMSA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMSA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMSA_1,AILMSA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMSB = min(LIMREPLOC2 , COD7SB) * (1 - COD7OZ) + COD7SB * COD7OZ ;
AILMSB_1 = (BILMSB) * (1 - V_CNR) ;
AILMSB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMSB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMSB_1,AILMSB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMSC = min(LIMREPLOC9 , COD7SC) * (1 - COD7OZ) + COD7SC * COD7OZ ;
AILMSC_1 = (BILMSC) * (1 - V_CNR) ;
AILMSC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMSC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMSC_1,AILMSC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMSN = min(LIMREPLOC11 , COD7SN) * (1 - COD7OZ) + COD7SN * COD7OZ ;
AILMSN_1 = (BILMSN) * (1 - V_CNR) ;
AILMSN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMSN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMSN_1,AILMSN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMSO = min(LIMREPLOC2 , COD7SO) * (1 - COD7OZ) + COD7SO * COD7OZ ;
AILMSO_1 = (BILMSO) * (1 - V_CNR) ;
AILMSO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMSO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMSO_1,AILMSO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

BILMSP = min(LIM10000 , COD7SP) * (1 - COD7OZ) + COD7SP * COD7OZ ;
AILMSP_1 = (BILMSP) * (1 - V_CNR) ;
AILMSP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (AILMSP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AILMSP_1,AILMSP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DILMNP3 = LOCMEUBJV + LOCMEUBJW + LOCMEUBJX + LOCMEUBJY + COD7OA + COD7OB + COD7OC + COD7OD + COD7OE + COD7OF 
          + COD7OG + COD7OH + COD7OI + COD7OJ + COD7OK + COD7OL + COD7OM + COD7ON + COD7OO + COD7OP + COD7OQ 
	  + COD7OR + COD7OS + COD7OT + COD7SA + COD7SB + COD7SC + COD7SN + COD7SO + COD7SP ;

AILMNP3 = AILMJV + AILMJW + AILMJX + AILMJY + AILMOA + AILMOB + AILMOC + AILMOD + AILMOE + AILMOF 
          + AILMOG + AILMOH + AILMOI + AILMOJ + AILMOK + AILMOL + AILMOM + AILMON + AILMOO + AILMOP 
	  + AILMOQ + AILMOR + AILMOS + AILMOT + AILMSA + AILMSB + AILMSC + AILMSN + AILMSO + AILMSP ;

regle 401500:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELTOT ;

RILMPF_1 = max(min(COD7PF , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPF_1,RILMPF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPF ;

REPMEUPF = (COD7PF - RILMPF) * (1 - V_CNR) ;

RILMPK_1 = max(min(COD7PK , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPK_1,RILMPK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPK ;

REPMEUPK = (COD7PK - RILMPK) * (1 - V_CNR) ;

RILMPP_1 = max(min(COD7PP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPP_1,RILMPP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPP ;

REPMEUPP = (COD7PP - RILMPP) * (1 - V_CNR) ;

RILMPU_1 = max(min(COD7PU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPU_1,RILMPU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPU ;

REPMEUPU = (COD7PU - RILMPU) * (1 - V_CNR) ;

RILMHO_1 = max(min(COD7HO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHO_1,RILMHO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHO ;

REPMEUHO = (COD7HO - RILMHO) * (1 - V_CNR) ;

RILMHT_1 = max(min(COD7HT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHT_1,RILMHT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHT ;

REPMEUHT = (COD7HT - RILMHT) * (1 - V_CNR) ;

RILMPG_1 = max(min(COD7PG , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPG_1,RILMPG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPG ;

REPMEUPG = (COD7PG - RILMPG) * (1 - V_CNR) ;

RILMPL_1 = max(min(COD7PL , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPL_1,RILMPL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPL ;

REPMEUPL = (COD7PL - RILMPL) * (1 - V_CNR) ;

RILMPQ_1 = max(min(COD7PQ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPQ_1,RILMPQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPQ ;

REPMEUPQ = (COD7PQ - RILMPQ) * (1 - V_CNR) ;

RILMPV_1 = max(min(COD7PV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPV_1,RILMPV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPV ;

REPMEUPV = (COD7PV - RILMPV) * (1 - V_CNR) ;

RILMHP_1 = max(min(COD7HP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHP_1,RILMHP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHP ;

REPMEUHP = (COD7HP - RILMHP) * (1 - V_CNR) ;

RILMHU_1 = max(min(COD7HU , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHU_1,RILMHU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHU ;

REPMEUHU = (COD7HU - RILMHU) * (1 - V_CNR) ;

RILMPH_1 = max(min(COD7PH , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPH_1,RILMPH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPH ;

REPMEUPH = (COD7PH - RILMPH) * (1 - V_CNR) ;

RILMPM_1 = max(min(COD7PM , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPM_1,RILMPM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPM ;

REPMEUPM = (COD7PM - RILMPM) * (1 - V_CNR) ;

RILMPR_1 = max(min(COD7PR , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPR_1,RILMPR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPR ;

REPMEUPR = (COD7PR - RILMPR) * (1 - V_CNR) ;

RILMPW_1 = max(min(COD7PW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPW_1,RILMPW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPW ;

REPMEUPW = (COD7PW - RILMPW) * (1 - V_CNR) ;

RILMHQ_1 = max(min(COD7HQ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHQ_1,RILMHQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHQ ;

REPMEUHQ = (COD7HQ - RILMHQ) * (1 - V_CNR) ;

RILMHV_1 = max(min(COD7HV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHV_1,RILMHV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHV ;

REPMEUHV = (COD7HV - RILMHV) * (1 - V_CNR) ;

RILMPI_1 = max(min(COD7PI , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPI_1,RILMPI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPI ;

REPMEUPI = (COD7PI - RILMPI) * (1 - V_CNR) ;

RILMPN_1 = max(min(COD7PN , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPN_1,RILMPN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPN ;

REPMEUPN = (COD7PN - RILMPN) * (1 - V_CNR) ;

RILMPS_1 = max(min(COD7PS , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPS_1,RILMPS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPS ;

REPMEUPS = (COD7PS - RILMPS) * (1 - V_CNR) ;

RILMPX_1 = max(min(COD7PX , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPX_1,RILMPX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPX ;

REPMEUPX = (COD7PX - RILMPX) * (1 - V_CNR) ;

RILMHR_1 = max(min(COD7HR , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHR_1,RILMHR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHR ;

REPMEUHR = (COD7HR - RILMHR) * (1 - V_CNR) ;

RILMHW_1 = max(min(COD7HW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHW_1,RILMHW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHW ;

REPMEUHW = (COD7HW - RILMHW) * (1 - V_CNR) ;

RILMPJ_1 = max(min(COD7PJ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPJ_1,RILMPJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPJ ;

REPMEUPJ = (COD7PJ - RILMPJ) * (1 - V_CNR) ;

RILMPO_1 = max(min(COD7PO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPO_1,RILMPO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPO ;

REPMEUPO = (COD7PO - RILMPO) * (1 - V_CNR) ;

RILMPT_1 = max(min(COD7PT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPT_1,RILMPT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPT  ;

REPMEUPT = (COD7PT - RILMPT) * (1 - V_CNR) ;

RILMPY_1 = max(min(COD7PY , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMPY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMPY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMPY_1,RILMPY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMPY ;

REPMEUPY = (COD7PY - RILMPY) * (1 - V_CNR) ;

RILMHS_1 = max(min(COD7HS , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHS_1,RILMHS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMHS ;

REPMEUHS = (COD7HS - RILMHS) * (1 - V_CNR) ;

RILMHX_1 = max(min(COD7HX , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMHX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMHX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMHX_1,RILMHX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

REPMEUHX = (COD7HX - RILMHX) * (1 - V_CNR) ;

RILMNP1 = RILMPF + RILMPK + RILMPP + RILMPU + RILMHO + RILMHT + RILMPG + RILMPL + RILMPQ + RILMPV + RILMHP + RILMHU + RILMPH 
          + RILMPM + RILMPR + RILMPW + RILMHQ + RILMHV + RILMPI + RILMPN + RILMPS + RILMPX + RILMHR + RILMHW + RILMPJ 
	  + RILMPO + RILMPT + RILMPY + RILMHS + RILMHX ;
RILMNP1_1 = RILMPF_1 + RILMPK_1 + RILMPP_1 + RILMPU_1 + RILMHO_1 + RILMHT_1 + RILMPG_1 + RILMPL_1 + RILMPQ_1 + RILMPV_1 + RILMHP_1 + RILMHU_1 + RILMPH_1 
          + RILMPM_1 + RILMPR_1 + RILMPW_1 + RILMHQ_1 + RILMHV_1 + RILMPI_1 + RILMPN_1 + RILMPS_1 + RILMPX_1 + RILMHR_1 + RILMHW_1 + RILMPJ_1 
	  + RILMPO_1 + RILMPT_1 + RILMPY_1 + RILMHS_1 + RILMHX_1 ;

regle 401680:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELTOT + RILMNP1 ;

RILMJY_1 = max(min(BILMJY , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMJY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMJY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMJY_1,RILMJY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMJY ;

RILMJX_1 = max(min(BILMJX , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMJX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMJX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMJX_1,RILMJX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMJX ;

RILMJW_1 = max(min(BILMJW , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMJW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMJW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMJW_1,RILMJW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMJW ;

RILMJV_1 = max(min(BILMJV , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMJV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMJV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMJV_1,RILMJV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMJV ;

RILMOE_1 = max(min(BILMOE , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOE_1,RILMOE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOE ;

RILMOD_1 = max(min(BILMOD , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOD =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOD_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOD_1,RILMOD1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOD ;

RILMOC_1 = max(min(BILMOC , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOC_1,RILMOC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOC ;

RILMOB_1 = max(min(BILMOB , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOB_1,RILMOB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOB ;

RILMOA_1 = max(min(BILMOA , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOA_1,RILMOA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOA ;

RILMOJ_1 = max(min(BILMOJ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOJ_1,RILMOJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOJ ;

RILMOI_1 = max(min(BILMOI , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOI_1,RILMOI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOI ;

RILMOH_1 = max(min(BILMOH , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOH =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOH_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOH_1,RILMOH1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOH ;

RILMOG_1 = max(min(BILMOG , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOG_1,RILMOG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOG ;

RILMOF_1 = max(min(BILMOF , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOF =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOF_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOF_1,RILMOF1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOF ;

RILMOO_1 = max(min(BILMOO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOO_1,RILMOO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOO ;

RILMON_1 = max(min(BILMON , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMON =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMON_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMON_1,RILMON1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMON ;

RILMOM_1 = max(min(BILMOM , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOM_1,RILMOM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOM ;

RILMOL_1 = max(min(BILMOL , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOL =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOL_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOL_1,RILMOL1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOL ;

RILMOK_1 = max(min(BILMOK , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOK =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOK_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOK_1,RILMOK1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOK ;

RILMOT_1 = max(min(BILMOT , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOT_1,RILMOT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOT ;

RILMOS_1 = max(min(BILMOS , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOS_1,RILMOS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOS ;

RILMOR_1 = max(min(BILMOR , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOR_1,RILMOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOR ;

RILMOQ_1 = max(min(BILMOQ , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOQ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOQ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOQ_1,RILMOQ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOQ ;

RILMOP_1 = max(min(BILMOP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMOP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMOP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMOP_1,RILMOP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMOP ;

RILMSC_1 = max(min(BILMSC , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMSC =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMSC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMSC_1,RILMSC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMSC ;
RILMSB_1 = max(min(BILMSB , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMSB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMSB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMSB_1,RILMSB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMSB ;
RILMSA_1 = max(min(BILMSA , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMSA =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMSA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMSA_1,RILMSA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMSA ;

RILMSO_1 = max(min(BILMSO , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMSO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMSO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMSO_1,RILMSO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMSO ;

RILMSN_1 = max(min(BILMSN , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMSN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMSN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMSN_1,RILMSN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMSN ;

RILMSP_1 = max(min(BILMSP , IDOM11 - VARTMP1) , 0) * (1 - V_CNR) ;
RILMSP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RILMSP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RILMSP_1,RILMSP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RILMSP ;
VARTMP1 = 0 ;

RILMNP3 = RILMJV + RILMJW + RILMJX + RILMJY + RILMOA + RILMOB + RILMOC + RILMOD + RILMOE + RILMOF + RILMOG + RILMOH 
          + RILMOI + RILMOJ + RILMOK + RILMOL + RILMOM + RILMON + RILMOO + RILMOP + RILMOQ + RILMOR + RILMOS + RILMOT 
	  + RILMSA + RILMSB + RILMSC + RILMSN + RILMSO + RILMSP ;
RILMNP3_1 = RILMJV_1 + RILMJW_1 + RILMJX_1 + RILMJY_1 + RILMOA_1 + RILMOB_1 + RILMOC_1 + RILMOD_1 + RILMOE_1 + RILMOF_1 + RILMOG_1 + RILMOH_1 
          + RILMOI_1 + RILMOJ_1 + RILMOK_1 + RILMOL_1 + RILMOM_1 + RILMON_1 + RILMOO_1 + RILMOP_1 + RILMOQ_1 + RILMOR_1 + RILMOS_1 + RILMOT_1 
	  + RILMSA_1 + RILMSB_1 + RILMSC_1 + RILMSN_1 + RILMSO_1 + RILMSP_1 ;

REPMEUJY = (BILMJY - RILMJY) * (1 - V_CNR) ;
REPMEUJX = (BILMJX - RILMJX) * (1 - V_CNR) ;
REPMEUJW = (BILMJW - RILMJW) * (1 - V_CNR) ;
REPMEUJV = (BILMJV - RILMJV) * (1 - V_CNR) ;
REPMEUOE = (BILMOE - RILMOE) * (1 - V_CNR) ;
REPMEUOD = (BILMOD - RILMOD) * (1 - V_CNR) ;
REPMEUOC = (BILMOC - RILMOC) * (1 - V_CNR) ;
REPMEUOB = (BILMOB - RILMOB) * (1 - V_CNR) ;
REPMEUOA = (BILMOA - RILMOA) * (1 - V_CNR) ;
REPMEUOJ = (BILMOJ - RILMOJ) * (1 - V_CNR) ;
REPMEUOI = (BILMOI - RILMOI) * (1 - V_CNR) ;
REPMEUOH = (BILMOH - RILMOH) * (1 - V_CNR) ;
REPMEUOG = (BILMOG - RILMOG) * (1 - V_CNR) ;
REPMEUOF = (BILMOF - RILMOF) * (1 - V_CNR) ;
REPMEUOO = (BILMOO - RILMOO) * (1 - V_CNR) ;
REPMEUON = (BILMON - RILMON) * (1 - V_CNR) ;
REPMEUOM = (BILMOM - RILMOM) * (1 - V_CNR) ;
REPMEUOL = (BILMOL - RILMOL) * (1 - V_CNR) ;
REPMEUOK = (BILMOK - RILMOK) * (1 - V_CNR) ;
REPMEUOT = (BILMOT - RILMOT) * (1 - V_CNR) ;
REPMEUOS = (BILMOS - RILMOS) * (1 - V_CNR) ;
REPMEUOR = (BILMOR - RILMOR) * (1 - V_CNR) ;
REPMEUOQ = (BILMOQ - RILMOQ) * (1 - V_CNR) ;
REPMEUOP = (BILMOP - RILMOP) * (1 - V_CNR) ;
REPMEUSA = (BILMSA - RILMSA) * (1 - V_CNR) ;
REPMEUSB = (BILMSB - RILMSB) * (1 - V_CNR) ;
REPMEUSC = (BILMSC - RILMSC) * (1 - V_CNR) ;
REPMEUSN = (BILMSN - RILMSN) * (1 - V_CNR) ;
REPMEUSO = (BILMSO - RILMSO) * (1 - V_CNR) ;
REPMEUSP = (BILMSP - RILMSP) * (1 - V_CNR) ;

regle 401750:
application : iliad ;


DCODMZ = COD7MZ ;
ACODMZ_1 = min(DCODMZ , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODMZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODMZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODMZ_1,ACODMZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
DCODPZ = COD7PZ ;
ACODPZ_1 = min(DCODPZ , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODPZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODPZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODPZ_1,ACODPZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCODOY = COD7OY ;
ACODOY_1 = min(DCODOY , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODOY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODOY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODOY_1,ACODOY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCODOX = COD7OX ;
ACODOX_1 =  min(DCODOX , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODOX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODOX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODOX_1,ACODOX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCODOW = COD7OW ;
ACODOW_1 = min(DCODOW , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODOW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODOW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODOW_1,ACODOW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCODOV = COD7OV ;
ACODOV_1 = min(DCODOV , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODOV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODOV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODOV_1,ACODOV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCODOU = COD7OU ;
ACODOU_1 = min(DCODOU , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODOU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODOU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODOU_1,ACODOU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DCODJTJU = LOCMEUBJT ;
ACODJTJU_1 = min(LOCMEUBJT , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODJTJU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACODJTJU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACODJTJU_1,ACODJTJU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DLOCIDEFG = LOCMEUBID ;

ACODID = min(LOCMEUBID , PLAF_RESINEUV)* (1 - V_CNR) ;
ALOCIDEFG_1 = (ACODID) * (1 - V_CNR) ;
ALOCIDEFG =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ALOCIDEFG_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ALOCIDEFG_1,ALOCIDEFG1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


DRESINEUV = LOCRESINEUV + INVNPROF1 ;

ACODIN = min(INVNPROF1 , PLAF_RESINEUV) * (1 - V_CNR) ;
ACODIJ = min(LOCRESINEUV ,( PLAF_RESINEUV - ACODIN)) * (1 - V_CNR) ;

ARESINEUV_1 = ACODIN  + ACODIJ  ; 
ARESINEUV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ARESINEUV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ARESINEUV_1,ARESINEUV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

DRESIVIEU = RESIVIEU ;
ACODIM = min(RESIVIEU , PLAF_RESINEUV) * (1 - V_CNR) ;

ARESIVIEU_1 = min(RESIVIEU , PLAF_RESINEUV) * (1 - V_CNR) ;
ARESIVIEU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (ARESIVIEU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ARESIVIEU_1,ARESIVIEU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;



RETCODMZ = arr((ACODMZ * TX11/100) /9) ;
RETCODMZ_1 = arr((ACODMZ_1 * TX11/100) /9) ;

RETCODPZ = arr((ACODPZ * TX11/100) /9) ;
RETCODPZ_1 = arr((ACODPZ_1 * TX11/100) /9) ;

RETCODOY = arr((ACODOY * TX11/100) /9) ;
RETCODOY_1 = arr((ACODOY_1 * TX11/100) /9) ;

RETCODOX = arr((ACODOX * TX11/100) /9) ;
RETCODOX_1 = arr((ACODOX_1 * TX11/100) /9) ;

RETCODOW = arr((ACODOW* TX11/100)  /9) ;
RETCODOW_1 = arr((ACODOW_1* TX11/100)  /9) ;

RETCODOV = arr((ACODOV* TX11/100) /9) ;
RETCODOV_1 = arr((ACODOV_1* TX11/100) /9) ;

RETCODOU = arr((ACODOU* TX11/100) /9) ;
RETCODOU_1 = arr((ACODOU_1* TX11/100) /9) ;

RETCODJT = arr((ACODJTJU* TX11/100) / 9)  ;
RETCODJT_1 = arr((ACODJTJU_1* TX11/100) / 9)  ;

RETCODID = arr((ACODID* TX11/100) /9)  ;


RETRESINEUV = arr((ACODIN * TX20 / 100) / 9) + arr((ACODIJ  * TX18 / 100) / 9) ;

RETCODIN = arr((ACODIN  * TX20 / 100) / 9) ;

RETCODIJ = arr((ACODIJ  * TX18 / 100) / 9) ;


RETRESIVIEU = arr((ACODIM * TX25 / 100) / 9) ;

RETCODIM = arr((ACODIM * TX25 / 100) / 9)  ;

regle 401770:
application : iliad ;

VARTMP1 = DEC11 + REDUCAVTCEL + RCELTOT + RILMNP1 + RILMNP3 ;

RCODIM_1 = max(min(RETCODIM , IDOM11 - VARTMP1) , 0) ;
RCODIM =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODIM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODIM_1,RCODIM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODIM ;

REPLOCIM = (RETCODIM - RCODIM) * positif(RESIVIEU + 0) * (1 - V_CNR) ;

RRESIVIEU = RCODIM ;

RCODIN_1 = max(min(RETCODIN , IDOM11 - VARTMP1) , 0) ;
RCODIN =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODIN_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODIN_1,RCODIN1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODIN ;

REPLOCIN = (RETCODIN - RCODIN) * positif(INVNPROF1 + 0) * (1 - V_CNR) ;

RCODIJ_1 = max(min(RETCODIJ , IDOM11 - VARTMP1) , 0) ;
RCODIJ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODIJ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODIJ_1,RCODIJ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODIJ ;

REPLOCIJ = (RETCODIJ - RCODIJ) * positif(LOCRESINEUV + 0) * (1 - V_CNR) ;

RRESINEUV = RCODIN + RCODIJ ;

RCODID_1 = max(min(RETCODID , IDOM11 - VARTMP1) , 0) ;
RCODID =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODID_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODID_1,RCODID1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODID ;

REPLOCID = (RETCODID - RCODID) * positif(LOCMEUBID + 0) * (1 - V_CNR) ;

RLOCIDEFG = RCODID ;

RCODJT_1 = max(min(RETCODJT , IDOM11 - VARTMP1) , 0) ;
RCODJT =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODJT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODJT_1,RCODJT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODJT ;

RCODJTJU = RCODJT ;

REPLOCJT = (RETCODJT - RCODJT) * positif(LOCMEUBJT + 0) * (1 - V_CNR) ;

RCODOU_1 = max(min(RETCODOU , IDOM11 - VARTMP1) , 0) ;
RCODOU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODOU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODOU_1,RCODOU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODOU ;

REPMEUOU = (RETCODOU - RCODOU) * positif(COD7OU + 0) * (1 - V_CNR) ;

RCODOV_1 = max(min(RETCODOV , IDOM11 - VARTMP1) , 0) ;
RCODOV =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODOV_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODOV_1,RCODOV1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODOV ;

REPMEUOV = (RETCODOV - RCODOV) * positif(COD7OV + 0) * (1 - V_CNR) ;

RCODOW_1 = max(min(RETCODOW , IDOM11 - VARTMP1) , 0) ;
RCODOW =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODOW_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODOW_1,RCODOW1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODOW ;

REPMEUOW = (RETCODOW - RCODOW) * positif(COD7OW + 0) * (1 - V_CNR) ;

RCODOX_1 = max(min(RETCODOX , IDOM11 - VARTMP1) , 0) ;
RCODOX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODOX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODOX_1,RCODOX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODOX ;

REPMEUOX = (RETCODOX - RCODOX) * positif(COD7OX + 0) * (1 - V_CNR) ;

RCODOY_1 = max(min(RETCODOY , IDOM11 - VARTMP1) , 0) ;
RCODOY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODOY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODOY_1,RCODOY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODOY ;

REPMEUOY = (RETCODOY - RCODOY) * positif(COD7OY + 0) * (1 - V_CNR) ;

RCODPZ_1 = max(min(RETCODPZ , IDOM11 - VARTMP1) , 0) ;
RCODPZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODPZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODPZ_1,RCODPZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = VARTMP1 + RCODPZ ;

REPMEUPZ = (RETCODPZ - RCODPZ) * positif(COD7PZ + 0) * (1 - V_CNR) ;

RCODMZ_1 = max(min(RETCODMZ , IDOM11 - VARTMP1) , 0) ;
RCODMZ =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCODMZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCODMZ_1,RCODMZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;
VARTMP1 = 0 ;

REPMEUMZ = (RETCODMZ - RCODMZ) * positif(COD7MZ + 0) * (1 - V_CNR) ;


DILMNP4 = DRESIVIEU + DRESINEUV + DLOCIDEFG + DCODJTJU + DCODOU + DCODOV + DCODOW + DCODOX + DCODOY + DCODPZ + DCODMZ ;

AILMNP4 = ARESIVIEU + ARESINEUV + ALOCIDEFG + ACODJTJU + ACODOU + ACODOV + ACODOW + ACODOX + ACODOY + ACODPZ + ACODMZ ;

RILMNP4 = RRESIVIEU + RRESINEUV + RLOCIDEFG + RCODJTJU + RCODOU + RCODOV + RCODOW + RCODOX + RCODOY + RCODPZ + RCODMZ ;
RILMNP4_1 =  RETRESIVIEU+RETRESINEUV+RETCODIJ+ RETCODID+RETCODJT_1+RETCODOU_1+RETCODOV_1+RETCODOW_1+RETCODOX_1+RETCODOY_1+RETCODPZ_1+RETCODMZ_1;

RLOCNPRO = RILMNP1 + RILMNP3 + RILMNP4 ;

RLOCNPRO_1 = RILMNP1_1 + RILMNP3_1 + RILMNP4_1 ;

regle 401810:
application : iliad ;


REP13MEU = REPLOCJT + REPMEUOA + REPMEUOU + REPMEUOV + REPMEUOF + REPMEUOW + REPMEUOK + REPMEUOX + REPMEUOY + REPMEUOP + REPMEUSA + REPMEUPZ + REPMEUMZ + REPMEUSN + REPMEUSP ;

REP12MEU = REPMEUJV + REPLOCID + REPMEUOB + REPMEUOG + REPMEUOL + REPMEUOQ + REPMEUSB + REPMEUSO ; 

REP11MEU = REPMEUJW + REPLOCIN + REPLOCIJ + REPMEUOC + REPMEUOH + REPMEUOM + REPMEUOR + REPMEUSC ;

REP10MEU = REPMEUJX + REPLOCIM + REPMEUOD + REPMEUOI + REPMEUON + REPMEUOS ;

REP9MEU = REPMEUJY + REPMEUOE + REPMEUOJ + REPMEUOO + REPMEUOT ; 
           
regle 401820:
application : iliad ;

RCODMZ1 = arr((ACODMZ * TX11/100)/9) ;
RCODMZ8 = (arr(min(PLAF_RESINEUV , COD7MZ) * TX11/100) - 8 * RCODMZ1) * (1 - V_CNR) ;

REPLOCMZ = (RCODMZ8 + RCODMZ1 * 7) ;

RCODPZ1 = arr((ACODPZ * TX11/100)/9) ;
RCODPZ8 = (arr(min(PLAF_RESINEUV , COD7PZ) * TX11/100) - 8 * RCODPZ1) * (1 - V_CNR) ;

REPLOCPZ = (RCODPZ8 + RCODPZ1 * 7) ;

RCODOY1 = arr((ACODOY * TX11/100)/9) ;
RCODOY8 = (arr(min(PLAF_RESINEUV , COD7OY) * TX11/100) - 8 * RCODOY1) * (1 - V_CNR) ;

REPLOCOY = (RCODOY8 + RCODOY1 * 7) ;

RCODOX1 = arr((ACODOX * TX11/100)/9) ;
RCODOX8 = (arr(min(PLAF_RESINEUV , COD7OX) * TX11/100) - 8 * RCODOX1) * (1 -V_CNR) ;

REPLOCOX = (RCODOX8 + RCODOX1 * 7) ;

RCODOW1 = arr((ACODOW * TX11/100)/9) ;
RCODOW8 = (arr(min(PLAF_RESINEUV , COD7OW) * TX11/100) - 8 * RCODOW1) * (1 -V_CNR) ;

REPLOCOW = (RCODOW8 + RCODOW1 * 7) ;

RCODOV1 = arr((ACODOV * TX11/100)/9) ;
RCODOV8 = (arr(min(PLAF_RESINEUV , COD7OV) * TX11/100) - 8 * RCODOV1) * (1 -V_CNR) ;

REPLOCOV = (RCODOV8 + RCODOV1 * 7) ;

RCODOU1 = arr((ACODOU * TX11/100)/9) ;
RCODOU8 = (arr(min(PLAF_RESINEUV , COD7OU) * TX11/100) - 8 * RCODOU1) * (1 -V_CNR) ;

REPLOCOU = (RCODOU8 + RCODOU1 * 7) * (1-null(2-V_REGCO)) * (1-null(4-V_REGCO)) ;

RCODJT1 = arr(arr(ACODJTJU * TX11/100) / 9) ;
RCODJT8 = arr(ACODJTJU * TX11/100) - 8 * RCODJT1 ; 

REPLNPT = (RCODJT1 * 7) + RCODJT8 ;

RLOCIDFG1 = arr(arr(ACODID * TX11/100) / 9) ;
RLOCIDFG8 = arr(ACODID * TX11/100) - (8 * RLOCIDFG1) ;

REPLOCIDFG = (RLOCIDFG1 * 7) + RLOCIDFG8 ;

RESINEUV1 = arr((ACODIN* TX20/100) / 9) + arr((ACODIJ * TX18/100) / 9) ;
RESINEUV8 = arr(ACODIN * TX20/100) + arr(ACODIJ * TX18/100) - (8 * RESINEUV1) ;

REPINVLOCNP = (RESINEUV1 * 7) + RESINEUV8 ;

RESIVIEU1 = arr((ACODIM * TX25/100) / 9) ;
RESIVIEU8 = arr(ACODIM * TX25/100) - (8 * RESIVIEU1) ;

REPINVIEU = (RESIVIEU1 * 7) + RESIVIEU8 ;

regle 401830:
application : iliad ;


BSOCREP = min(RSOCREPRISE , LIM_SOCREPR * ( 1 + BOOL_0AM)) ;

RSOCREP = arr ( TX_SOCREPR/100 * BSOCREP ) * (1 - V_CNR);

DSOCREPR = RSOCREPRISE;

ASOCREPR = (positif(null(V_IND_TRAIT-4) + COD9ZA) * BSOCREP * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
            + (max(0 , min(BSOCREP,BSOCREP1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5) + 0) * (1 - V_CNR) ;

RSOCREPR_1 = max( min( RSOCREP , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE
                                             -RFORET-RFIPDOM-RFIPC-RCINE-RRESTIMO) , 0 );
RSOCREPR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RSOCREPR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RSOCREPR_1,RSOCREPR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401835:
application : iliad ;


RCOD7KZ_1 = max(0 , min(COD7KZ , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC-RCINE-RRESTIMO-RSOCREPR
                                     -RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP-RRIRENOV-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE
				     -RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA-RRREHAP)) * (1 - null(V_REGCO - 2)) ;
RCOD7KZ =(positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCOD7KZ_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCOD7KZ_1,RCOD7KZ1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0)  * (1 - null(V_REGCO - 2));

REPRESTKZ = max(0 , COD7KZ - RCOD7KZ) * (1 - null(V_REGCO - 2)) ;

RCOD7KY_1 = max(0 , min(COD7KY , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC-RCINE-RRESTIMO-RSOCREPR
                                     -RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP-RRIRENOV-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE
				     -RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA-RRREHAP-RCOD7KZ))  * (1 - null(V_REGCO - 2));
RCOD7KY =(positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCOD7KY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCOD7KY_1,RCOD7KY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0)  * (1 - null(V_REGCO - 2));

REPRESTKY = max(0 , COD7KY - RCOD7KY) * (1 - null(V_REGCO - 2)) ;

RCOD7KX_1 = max(0 , min(COD7KX , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC-RCINE-RRESTIMO-RSOCREPR
                                     -RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP-RRIRENOV-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE
				     -RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA-RRREHAP-RCOD7KZ-RCOD7KY))  * (1 - null(V_REGCO - 2));
RCOD7KX =(positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCOD7KX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCOD7KX_1,RCOD7KX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0)  * (1 - null(V_REGCO - 2));

REPRESTKX = max(0 , COD7KX - RCOD7KX) * (1 - null(V_REGCO - 2)) ;

DRESTREP = COD7KZ + COD7KY + COD7KX ;
ARESTREP = DRESTREP ;
RRESTREP = RCOD7KZ + RCOD7KY + RCOD7KX ; 
RRESTREP_1 = RCOD7KZ_1 + RCOD7KY_1 + RCOD7KX_1 ; 

regle 401840:
application : iliad ;


DRESTIMO = COD7NX + COD7NY ;


DRESTIMO1 = COD7TX + COD7TY ;


RESTIMONX = min(COD7NX , LIM_RESTIMO) ;

RRESTIMONX_1 = max(min(RESTIMONX * TX30/100 , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM
                                                  -RDIFAGRI-RPRESSE-RFORET-RFIPC-RCINE) , 0) ;
RRESTIMONX =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRESTIMONX_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRESTIMONX_1,RRESTIMONX1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RESTIMONY = min(COD7NY , max(0,LIM_RESTIMO - RESTIMONX)) ;

RRESTIMONY_1 = max(min(RESTIMONY * TX22/100 , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM
                                                  -RDIFAGRI-RPRESSE-RFORET-RFIPC-RCINE-RRESTIMONX ) , 0) ;
RRESTIMONY =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRESTIMONY_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRESTIMONY_1,RRESTIMONY1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401844:
application : iliad ;


RESTIMOTX = min(COD7TX , max(0,LIM_RESTIMO1 - V_BTDRIMM3*(1-present(COD7ST)) - V_BTDRIMM2 * (1-present(COD7SU))-V_BTDRIMM1*(1-present(COD7SV))- COD7ST-COD7SU-COD7SV)) ;
RRESTIMOTX = max(min(RESTIMOTX * TX30/100 , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RDIFAGRI-RPRESSE-RFORET-RFIPC-RCINE-RRESTIMO-RSOCREPR
                                                  -RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP-RRIRENOV-RCOMP-RRETU-RDONS-CRDIE-RDUFREP
						  -RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA-RLOGDOM-RRESTIMONX-RRESTIMONY) , 0) ;
											                                            

RESTIMOTY = min(COD7TY , max(0,(LIM_RESTIMO1-RESTIMOTX - V_BTDRIMM3*(1-present(COD7ST))- V_BTDRIMM2 * (1-present(COD7SU)) -V_BTDRIMM1*(1-present(COD7SV))- COD7ST-COD7SU-COD7SV))) ;

RRESTIMOTY = max(min(RESTIMOTY * TX22/100 , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RDIFAGRI-RPRESSE-RFORET-RFIPC-RCINE-RRESTIMO-RSOCREPR
                                                  -RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP-RRIRENOV-RCOMP-RRETU-RDONS-CRDIE-RDUFREP
						  -RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA-RLOGDOM-RRESTIMONX-RRESTIMONY-RRESTIMOTX) , 0) ;

regle 401845:
application : iliad ;

ARESTIMO_1 = (RESTIMONX + RESTIMONY) * (1 - V_CNR) ;
ARESTIMO = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ARESTIMO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(ARESTIMO_1,ARESTIMO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RETRESTIMO = arr((RESTIMONX * TX30/100) + (RESTIMONY * TX22/100)) * (1 - V_CNR) ;

RRESTIMO_1 = max (min (RETRESTIMO , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM
                               -RDIFAGRI-RPRESSE-RFORET-RFIPC-RCINE) , 0) ;
RRESTIMO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRESTIMO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRESTIMO_1,RRESTIMO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401847:
application : iliad ;

ARESTIMO1_1 = (RESTIMOTX + RESTIMOTY) * (1 - V_CNR) ;
ARESTIMO1 = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ARESTIMO1_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
            + (max(0,min(ARESTIMO1_1,ARESTIMO11731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RETRESTIMO_2 = arr((RESTIMOTX * TX30/100) + (RESTIMOTY * TX22/100) ) * (1 - V_CNR) ;

RRESTIMO1_1 = max(min(RETRESTIMO_2 , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET-RFIPDOM-RFIPC
                                           -RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV- RINNO-RSOUFIP-RRIRENOV-RLOGDOM-RCOMP- RRETU-RDONS
					   -CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA-RRREHAP-RRESTREP),0);
RRESTIMO1 =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRESTIMO1_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRESTIMO1_1,RRESTIMO11731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


A12RRESTIMO = RRESTIMO * (1 - V_CNR) ;

REPRESTXY = max(0 , RETRESTIMO_2 - RRESTIMO1) * (1 - V_CNR) ;

regle 401850:
application : iliad ;

REVDON = max(0 , RBL1 + TOTALQUOHT - SDDD - SDC1) ;


BDON7UH = min(LIM15000 , COD7UH) ;

BASEDONB = REPDON03 + REPDON04 + REPDON05 + REPDON06 + REPDON07 + RDDOUP + COD7UH + DONAUTRE ;
BASEDONP = RDDOUP + BDON7UH + DONAUTRE + EXCEDANTA + EXCEDANTD ;

BONS = arr(min(REPDON03 + REPDON04 + REPDON05 + REPDON06 + REPDON07 + BASEDONP , REVDON * TX_BASEDUP/100)) ;

regle 401860:
application : iliad ;

BASEDONF = min(REPDON03 , arr(REVDON * TX_BASEDUP/100)) ;
REPDON = max(BASEDONF + REPDON04 + REPDON05 + REPDON06 + REPDON07 + BASEDONP - arr(REVDON * TX_BASEDUP/100) , 0) * (1 - V_CNR) ;

REPDONR4 = (positif_ou_nul(BASEDONF - arr(REVDON * (TX_BASEDUP)/100)) * REPDON04
            + (1 - positif_ou_nul(BASEDONF - arr(REVDON * (TX_BASEDUP)/100)))
	      * max(REPDON04 + (BASEDONF - arr(REVDON * (TX_BASEDUP)/100)),0)
	   )
	   * (1 - V_CNR);

REPDONR3 = (positif_ou_nul(BASEDONF + REPDON04 - arr(REVDON * (TX_BASEDUP)/100)) * REPDON05
	    + (1 - positif_ou_nul(BASEDONF + REPDON04 - arr(REVDON * (TX_BASEDUP)/100)))
	      * max(REPDON05 + (BASEDONF + REPDON04 - arr(REVDON * (TX_BASEDUP)/100)),0)
           ) 
	   * (1 - V_CNR);

REPDONR2 = (positif_ou_nul(BASEDONF + REPDON04 + REPDON05 - arr(REVDON * (TX_BASEDUP)/100)) * REPDON06
            + (1 - positif_ou_nul(BASEDONF + REPDON04 + REPDON05 - arr(REVDON * (TX_BASEDUP)/100)))
	      * max(REPDON06 + (BASEDONF + REPDON04 + REPDON05 - arr(REVDON * (TX_BASEDUP)/100)),0)
	   )
	   * (1 - V_CNR);

REPDONR1 = (positif_ou_nul(BASEDONF + REPDON04 + REPDON05 + REPDON06 - arr(REVDON * (TX_BASEDUP)/100)) * REPDON07
	    + (1 - positif_ou_nul(BASEDONF + REPDON04 + REPDON05 + REPDON06 - arr(REVDON * (TX_BASEDUP)/100)))
	      * max(REPDON07 + (BASEDONF + REPDON04 + REPDON05 + REPDON06 - arr(REVDON * (TX_BASEDUP)/100)),0)
           )
	   * (1 - V_CNR) ;

REPDONR = max(REPDON - REPDONR1 - REPDONR2 - REPDONR3 - REPDONR4 , 0) * (1 - V_CNR) ;

regle 401870:
application : iliad ;


RONS = arr(BONS * TX_REDDON /100) * (1 - V_CNR) ;

DDONS = REPDON03 + REPDON04 + REPDON05 + REPDON06 + REPDON07 + RDDOUP + COD7UH + DONAUTRE ;

ADONS_1 = BONS * (1 - V_CNR) ;
ADONS = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADONS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(ADONS_1,ADONS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401880:
application : iliad ;

RDONS_1 = max(min(RONS , RRI1-RLOGDOM-RCOMP-RRETU) , 0) ;
RDONS =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RDONS_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RDONS_1,RDONS1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401885:
application : iliad ;

CRCFA_1 = arr(IPQ1 * REGCI / (RB01 + TONEQUO)) * (1 - positif(RE168 + TAX1649)) ;
CRCFA = positif(null(V_IND_TRAIT-4)+COD9ZA) * (CRCFA_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
        + (max(0,min(CRCFA_1,CRCFA1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401887:
application : iliad ;

CRDIE_1 = max( min( CRCFA , RRI1-RLOGDOM-RCOMP-RRETU-RDONS) , 0 ) ;
CRDIE =positif(null(V_IND_TRAIT-4)+COD9ZA) * (CRDIE_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(CRDIE_1,CRDIE1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401890:
application : iliad ;


SEUILRED1 = arr((arr(RI1)+REVQUO) / NBPT) ;

regle 401900:
application : iliad ;


RETUD = arr((RDENS * MTRC) + (RDENL * MTRL) + (RDENU * MTRS) + (RDENSQAR * MTRC /2) + (RDENLQAR * MTRL /2) + (RDENUQAR * MTRS /2)) 
        * (1 - V_CNR) ;

DNBE = RDENS + RDENL + RDENU + RDENSQAR + RDENLQAR + RDENUQAR ;

RNBE = DNBE ;

regle 401910:
application : iliad ;

RRETU_1 = max(min(RETUD , RRI1-RLOGDOM-RCOMP) , 0) ;
RRETU =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRETU_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRETU_1,RRETU1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401920:
application : iliad ;


BFCPIGR = min(COD7GR , PLAF_FCPI * (1 + BOOL_0AM)) * (1 - V_CNR) ;
BFCPIGQ = min(FCPI , max(0 , PLAF_FCPI * (1 + BOOL_0AM) - COD7GR)) * (1 - V_CNR) ;

DFCPI = FCPI + COD7GR ;
BFCPI = (BFCPIGR + BFCPIGQ) * (1 - V_CNR) ;

RFCPI = arr(BFCPIGQ * TX_FCPI/100 + BFCPIGR * TX25/100) * (1 - V_CNR) ; 

RINNO_1 = max(0 , min(RFCPI , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RDIFAGRI-RPRESSE-RFORET
                                  -RFIPC-RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV)) ;
RINNO =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RINNO_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RINNO_1,RINNO1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401930:
application : iliad ;


BPRESCOMP =(RDPRESREPORT 
	   + (1-positif(RDPRESREPORT+0)) * 
	   arr(
	         ((1 - present(SUBSTITRENTE)) * 
                  arr (
                 null(PRESCOMP2000 - PRESCOMPJUGE)
                   * min(PLAFPRESCOMP,PRESCOMP2000)
	         + positif(PRESCOMPJUGE - PRESCOMP2000)
                   * (positif_ou_nul(PLAFPRESCOMP -PRESCOMPJUGE))
                   * PRESCOMP2000
	         + positif(PRESCOMPJUGE - PRESCOMP2000)
                    * (1 - positif_ou_nul(PLAFPRESCOMP -PRESCOMPJUGE))
                    * PLAFPRESCOMP * PRESCOMP2000/PRESCOMPJUGE
                       )
	       +
             present(SUBSTITRENTE) *
                  arr (
                   null(PRESCOMP2000 - SUBSTITRENTE)
		   * 
		   (positif_ou_nul(PLAFPRESCOMP - PRESCOMPJUGE)*SUBSTITRENTE
		   + 
		   positif(PRESCOMPJUGE-PLAFPRESCOMP)*arr(PLAFPRESCOMP*SUBSTITRENTE/PRESCOMPJUGE))
                 + 
		   positif(SUBSTITRENTE - PRESCOMP2000)
		   * (positif_ou_nul(PLAFPRESCOMP - PRESCOMPJUGE)*PRESCOMP2000
		   + 
		   positif(PRESCOMPJUGE-PLAFPRESCOMP)*arr(PLAFPRESCOMP*(SUBSTITRENTE/PRESCOMPJUGE)*(PRESCOMP2000/SUBSTITRENTE)))
                       )
	           )
                )
              )
               *(1 - V_CNR);


RPRESCOMP = arr (BPRESCOMP * TX_PRESCOMP / 100) * (1 -V_CNR);
BPRESCOMP01 = max(0,(1 - present(SUBSTITRENTE)) * 
                   (  positif_ou_nul(PLAFPRESCOMP -PRESCOMPJUGE)
                       * (PRESCOMPJUGE - BPRESCOMP)
                     + positif(PRESCOMPJUGE - PLAFPRESCOMP)
                       * (PLAFPRESCOMP - BPRESCOMP)
                   )
	       +
             present(SUBSTITRENTE) *
                   (  positif_ou_nul(PLAFPRESCOMP -PRESCOMPJUGE)
                       * (SUBSTITRENTE - PRESCOMP2000)
                     + positif(PRESCOMPJUGE - PLAFPRESCOMP)
		     *arr(PLAFPRESCOMP*(SUBSTITRENTE/PRESCOMPJUGE)*((SUBSTITRENTE-PRESCOMP2000)/SUBSTITRENTE))
                   )
		* (1 - V_CNR)
		);
DPRESCOMP = PRESCOMP2000 + RDPRESREPORT ;

APRESCOMP = (positif(null(V_IND_TRAIT-4) + COD9ZA) * BPRESCOMP * (1-positif(null(8-CMAJ) + null(11-CMAJ) + null(34-CMAJ)))
             + (max(0 , min(BPRESCOMP,BPRESCOMP1731)) * positif(1-COD9ZA) * (1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0) * (1 - V_CNR) ;

RRPRESCOMP_1 = max( min( RPRESCOMP , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RDIFAGRI-RPRESSE-RFORET
                                                 -RFIPC-RCINE-RRESTIMO-RSOCREPR) , 0) ;
RRPRESCOMP =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RRPRESCOMP_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RRPRESCOMP_1,RRPRESCOMP1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RPRESCOMPREP = max( min( RPRESCOMP , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RDIFAGRI-RPRESSE-RFORET
				      -RFIPC-RCINE-RRESTIMO-RSOCREPR) , 0) * positif(RDPRESREPORT) ;

RPRESCOMPAN = RRPRESCOMP * (1-positif(RDPRESREPORT)) ;

regle 401940:
application : iliad ;
                                     

DCOTFOR = COTFORET ;

ACOTFOR_R = min(DCOTFOR , PLAF_FOREST1 * (1 + BOOL_0AM)) * (1 - V_CNR) ;

ACOTFOR = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACOTFOR_R) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACOTFOR_R,ACOTFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RCOTFOR_1 = max( min( arr(ACOTFOR_R * TX76/100) , IDOM11-DEC11) , 0) ;
RCOTFOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RCOTFOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RCOTFOR_1,RCOTFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401950:
application : iliad ;

FORTRA = REPSINFOR3 + REPSINFOR4 ;

DFOREST = FORTRA + RDFOREST;


AFOREST_1 = (min(FORTRA , max(0 , (PLAF_FOREST1 * (1 + BOOL_0AM)) - ACOTFOR))
             + min(RDFOREST, PLAF_FOREST * (1 + BOOL_0AM))) * (1 - V_CNR) ;
AFOREST = positif(null(V_IND_TRAIT-4)+COD9ZA) * (AFOREST_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(AFOREST_1,AFOREST1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


RFOREST3 = min( REPSINFOR3 +REPSINFOR4, max(0 , (PLAF_FOREST1 * (1 + BOOL_0AM)) - ACOTFOR - RFOREST1 - RFOREST2)) ;

RFOREST = (arr(RFOREST3 * TX18/100) + arr( max(0 , AFOREST - RFOREST3) * TX18/100)) * (1 - V_CNR) ;

regle 401960:
application : iliad ;

RFOR_1 = max(min(RFOREST , IDOM11-DEC11-RCOTFOR-RREPA-RDIFAGRI-RPRESSE-RFORET
                                 -RFIPDOM-RFIPC-RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP-RRIRENOV 
                                 -RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON) , 0) ;
RFOR =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RFOR_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RFOR_1,RFOR1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401970:
application : iliad ;


REPNIS = (positif_ou_nul(REPSINFOR3 - max(0,(PLAF_FOREST1*(1+BOOL_0AM))-ACOTFOR_R)) * REPSINFOR4
          + (1-positif_ou_nul(REPSINFOR3 - max(0,(PLAF_FOREST1*(1+BOOL_0AM))-ACOTFOR_R)))
            * max(0,REPSINFOR4-max(0,(PLAF_FOREST1*(1+BOOL_0AM))-ACOTFOR_R) + REPSINFOR3)) * (1 - V_CNR) ; 

REP7TE = REPNIS ;

regle 401980:
application : iliad ;


BDIFAGRI = min(INTDIFAGRI , LIM_DIFAGRI * (1 + BOOL_0AM)) * (1 - V_CNR) ;

DDIFAGRI = INTDIFAGRI ;

ADIFAGRI = positif(null(V_IND_TRAIT-4)+COD9ZA) * (BDIFAGRI) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(BDIFAGRI,BDIFAGRI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RAGRI = arr (BDIFAGRI  * TX_DIFAGRI / 100 );

RDIFAGRI_1 = max( min(RAGRI , IDOM11-DEC11-RCOTFOR-RREPA),0);
RDIFAGRI =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RDIFAGRI_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RDIFAGRI_1,RDIFAGRI1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 401990:
application : iliad ;


ITRED = min( RED , IDOM11-DEC11) ;

regle 402000:
application : iliad ;


NNRI2 = max(0 , RRI1 - (DLOGDOM + ACOMP + RETUD + RONS + CRCFA + ADUFREP + APIREP + ANORMREP + RPINABCD + RPINRRS + RNORABCD
			+ RSN + COD7CY + COD7DY + COD7EY + COD7FY + COD7GY + RFOREST + arr(ATOURREP * TX_REDIL25 / 100) + arr(ATOUREPA * TX_REDIL20 / 100)
                        + CELRREDLG + CELRREDLK + CELRREDLH + CELRREDLL + CELRREDLI 
			+ CELRREDLO + CELRREDLJ + CELRREDLP + CELRREDLQ + CELRREDLR + CELRREDLU + CELRREDLV 
			+ COD7LA + COD7LB + COD7LC + COD7LY + COD7MS + COD7MT + COD7MU + COD7MQ + COD7MV
			+ ACELSOM4 + ACELSOM6 + COD7KZ + COD7KY
			+ RCEL7ZM + RCEL7ZN + RCEL7ZA + RCEL7ZB + RCEL7ZG + RCEL7ZH + RCEL7ZC + RCEL7ZD + RCEL7ZE + RCEL7ZF + RCEL7RI + RCEL7RJ + RCEL7RK + RCEL7RL 
			+ RCEL7IR + RCEL7IS + RCEL7IT + RCEL7IU + RCEL7RM + RCEL7RN + RCEL7RO + RCEL7RP + RCEL7IV + RCEL7IW + RCEL7IX + RCEL7IY + RCEL7RQ + RCEL7IZ 
			+ RCEL7XH + RCEL7XI + RCEL7XJ + RCEL7XK + RCEL7JE + RCEL7JF + RCEL7JG + RCEL7JH + RCEL7JI + RCEL7JJ + RCEL7JK + RCEL7JL
			+ RETCODID + RETCODIJ + RETCODIM + RETCODIN + RETCODJT + RETCODOU + RETCODOV + RETCODOW + RETCODOX + RETCODOY + RETCODPZ + RETCODMZ
			+ AILMNP1 + AILMNP3 + RRREHAP)) ; 

regle 402010:
application : iliad ;




DLOGDOM = INVLOG2008 + INVLGDEB2009 + INVLGDEB + INVLGAUTRE + INVLGDEB2010 + INVLOG2009 + INVOMLOGOA + INVOMLOGOB 
          + INVOMLOGOC + INVOMLOGOH + INVOMLOGOI + INVOMLOGOJ + INVOMLOGOK + INVOMLOGOL + INVOMLOGOM + INVOMLOGON 
          + INVOMLOGOO + INVOMLOGOP + INVOMLOGOQ + INVOMLOGOR + INVOMLOGOS + INVOMLOGOT + INVOMLOGOU + INVOMLOGOV 
          + INVOMLOGOW 
          + CODHOD + CODHOE + CODHOF + CODHOG + CODHOX + CODHOY + CODHOZ + CODHUA + CODHUB + CODHUC + CODHUD + CODHUE 
          + CODHUF + CODHUG + CODHUH + CODHUI + CODHUJ + CODHUK + CODHUL + CODHUM + CODHUN + CODHUO + CODHUP + CODHUQ
	  + CODHUR + CODHUS + CODHUT + CODHUU + CODHVA + CODHVB + CODHVC + CODHVD + CODHVE + CODHVF + CODHVG + CODHVH 
	  + CODHVI + CODHVJ ;


DDOMSOC1 = CODHXF + CODHXG + CODHXH + CODHXI + CODHXK + CODHXL + CODHXM + CODHXN + CODHXO 
           + CODHXP + CODHXU + CODHXQ + CODHXR + CODHXS + CODHXT + CODHYA + CODHYB + CODHYC + CODHYD ;

DLOGSOC = CODHYE ;


DCOLENT = CODHBI + CODHBJ + CODHBK + CODHBM 
	  + CODHBN + CODHBO + CODHBP + CODHBR + CODHBS + CODHBT + CODHBU + CODHBW + CODHBX + CODHBY + CODHBZ + CODHCB + CODHCC + CODHCD 
	  + CODHCE + CODHCG + CODHCI + CODHCJ + CODHCK + CODHCM + CODHCN + CODHCO + CODHCP + CODHCR + CODHCS + CODHCT + CODHCU + CODHCW 
	  + CODHDI + CODHDJ + CODHDK + CODHDM + CODHDN + CODHDO + CODHDP + CODHDR + CODHDS + CODHDT + CODHDU + CODHDW + CODHEN + CODHEO 
	  + CODHEP + CODHER + CODHES + CODHET + CODHEU + CODHEW + CODHFN + CODHFO + CODHFP + CODHFR + CODHFS + CODHFT + CODHFU + CODHFW ;

DLOCENT = CODHGS + CODHGT + CODHGU + CODHGW ;

regle 402020:
application : iliad ;



TOTALPLAF1 = INVRETXF + INVRETXG 
             + INVRETXH + INVRETXI + INVRETXK + INVRETXL + INVRETXM + INVRETXN + INVRETXO + INVRETXP + INVRETXU + INVRETXQ 
	     + INVRETXR + INVRETXS + INVRETXT + INVRETYB + INVRETYA + INVRETYD + INVRETYC + INVRETYE + INVRETQL + INVRETQM 
	     + INVRETQD + INVRETOB + INVRETOC + INVRETOM + INVRETON + INVRETOD + INVRETUA + INVRETUH + INVRETUO + INVRETVA
	     + INVRETXFR + INVRETXGR + INVRETXHR + INVRETXIR + INVRETXKR + INVRETXLR + INVRETXMR + INVRETXNR + INVRETXUR 
	     + INVRETXQR + INVRETXRR + INVRETXSR + INVRETXTR + INVRETYBR + INVRETYAR + INVRETYDR + INVRETYCR + INVRETYER 
	     + INVRETXOR + INVRETXPR ;

TOTALPLAF2 = INVRETOI + INVRETOJ + INVRETOK + INVRETOP + INVRETOQ + INVRETOR + INVRETOE + INVRETOF + INVRETUB + INVRETUC 
             + INVRETUI + INVRETUJ + INVRETUP + INVRETUQ + INVRETVB + INVRETVC ;

TOTALPLAF3 = INVRETBJ 
	     + INVRETBO + INVRETBT + INVRETBY + INVRETCD + INVRETBI + INVRETBN + INVRETBS + INVRETBX + INVRETCC + INVRETBK + INVRETBP 
	     + INVRETBU + INVRETBZ + INVRETCE + INVRETBM + INVRETBR + INVRETBW + INVRETCB + INVRETCG + INVRETCT + INVRETCJ + INVRETCO 
	     + INVRETCS + INVRETCI + INVRETCN + INVRETCK + INVRETCP + INVRETCU + INVRETCM + INVRETCR + INVRETCW + INVRETDT + INVRETDJ 
	     + INVRETDO + INVRETDS + INVRETDI + INVRETDN + INVRETET + INVRETEO + INVRETES + INVRETEN + INVRETEP + INVRETEU + INVRETER 
	     + INVRETEW + INVRETFT + INVRETFO + INVRETFS + INVRETFN + INVRETFP + INVRETFU + INVRETFR + INVRETFW + INVRETGT + INVRETGS 
	     + INVRETGU + INVRETGW + INVRETDK + INVRETDP + INVRETDU + INVRETDM + INVRETDR + INVRETDW + INVRETOT + INVRETOU + INVRETOV 
	     + INVRETOW + INVRETOG + INVRETOX + INVRETOY + INVRETOZ + INVRETUD + INVRETUE + INVRETUF + INVRETUG + INVRETUK + INVRETUL 
	     + INVRETUM + INVRETUN + INVRETUR + INVRETUS + INVRETUT + INVRETUU + INVRETVD + INVRETVE + INVRETVF + INVRETVG + INVRETVH 
	     + INVRETVI + INVRETVJ + INVRETBJR + INVRETBOR 
	     + INVRETBTR + INVRETBYR + INVRETCDR + INVRETBIR + INVRETBNR + INVRETBSR + INVRETBXR + INVRETCCR + INVRETCTR + INVRETCJR 
	     + INVRETCOR + INVRETCSR + INVRETCIR + INVRETCNR + INVRETDTR + INVRETDJR + INVRETDOR + INVRETDSR + INVRETDIR + INVRETDNR 
	     + INVRETETR + INVRETEOR + INVRETESR + INVRETENR + INVRETFTR + INVRETFOR + INVRETFSR + INVRETFNR + INVRETGTR + INVRETGSR ;

RNIDOM1 = arr((RNG + TTPVQ) * TX15/100) * (1 - V_CNR) ;

RNIDOM2 = arr((RNG + TTPVQ) * TX13/100) * (1 - V_CNR) ;

RNIDOM3 = arr((RNG + TTPVQ) * TX11/100) * (1 - V_CNR) ;

INDPLAF1 = positif(RNIDOM1 - TOTALPLAF1) * (1 - V_CNR) ;

regle 402022:
application : iliad ;


VARTMP1 = 0 ;

INVRETXKA = min(arr(NINVRETXK * TX30 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXKA ;

INVRETXFA = min(arr(NINVRETXF * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXFA ;

INVRETXGA = min(arr(NINVRETXG * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXGA ;

INVRETXHA = min(arr(NINVRETXH * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXHA ;

INVRETXIA = min(arr(NINVRETXI * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXIA ;

INVRETXPA = min(arr(NINVRETXP * TX30 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXPA ;

INVRETXLA = min(arr(NINVRETXL * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXLA ;

INVRETXMA = min(arr(NINVRETXM * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXMA ;

INVRETXNA = min(arr(NINVRETXN * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXNA ;

INVRETXOA = min(arr(NINVRETXO * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXOA ;

INVRETXUA = min(arr(NINVRETXU * TX30 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXUA ;

INVRETXQA = min(arr(NINVRETXQ * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXQA ;

INVRETXRA = min(arr(NINVRETXR * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXRA ;

INVRETXSA = min(arr(NINVRETXS * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXSA ;

INVRETXTA = min(arr(NINVRETXT * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXTA ;

INVRETYBA = min(arr(NINVRETYB * TX30 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYBA ;

INVRETYAA = min(arr(NINVRETYA * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYAA ;

INVRETYDA = min(arr(NINVRETYD * TX30 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYDA ;

INVRETYCA = min(arr(NINVRETYC * TX35 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYCA ;

INVRETYEA = min(arr(NINVRETYE * TX30 / 100) , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYEA ;

INVRETQLA = min(NINVRETQL , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETQLA ;

INVRETQMA = min(NINVRETQM , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETQMA ;

INVRETQDA = min(NINVRETQD , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETQDA ;

INVRETOBA = min(NINVRETOB , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOBA ;

INVRETOCA = min(NINVRETOC , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOCA ;

INVRETOMA = min(NINVRETOM , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOMA ;

INVRETONA = min(NINVRETON , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETONA ;

INVRETODA = min(NINVRETOD , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETODA ;

INVRETUAA = min(NINVRETUA , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUAA ;

INVRETUHA = min(NINVRETUH , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUHA ;

INVRETUOA = min(NINVRETUO , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUOA ;

INVRETVAA = min(NINVRETVA , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVAA ;

INVRETXKRA = min(NINVRETXKR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXKRA ;

INVRETXFRA = min(NINVRETXFR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXFRA ;

INVRETXGRA = min(NINVRETXGR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXGRA ;

INVRETXHRA = min(NINVRETXHR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXHRA ;

INVRETXIRA = min(NINVRETXIR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXIRA ;

INVRETXPRA = min(NINVRETXPR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXPRA ;

INVRETXLRA = min(NINVRETXLR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXLRA ;

INVRETXMRA = min(NINVRETXMR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXMRA ;

INVRETXNRA = min(NINVRETXNR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXNRA ;

INVRETXORA = min(NINVRETXOR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXORA ;

INVRETXURA = min(NINVRETXUR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXURA ;

INVRETXQRA = min(NINVRETXQR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXQRA ;

INVRETXRRA = min(NINVRETXRR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXRRA ;

INVRETXSRA = min(NINVRETXSR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXSRA ;

INVRETXTRA = min(NINVRETXTR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETXTRA ;

INVRETYBRA = min(NINVRETYBR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYBRA ;

INVRETYARA = min(NINVRETYAR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYARA ;

INVRETYDRA = min(NINVRETYDR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYDRA ;

INVRETYCRA = min(NINVRETYCR , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETYCRA ;

INVRETYERA = min(NINVRETYER , max(0 , RNIDOM1 - VARTMP1)) ;
VARTMP1 = 0 ;

TOTALPLAFA = INVRETXFA + INVRETXGA 
             + INVRETXHA + INVRETXIA + INVRETXKA + INVRETXLA + INVRETXMA + INVRETXNA + INVRETXOA + INVRETXPA + INVRETXUA + INVRETXQA 
	     + INVRETXRA + INVRETXSA + INVRETXTA + INVRETYBA + INVRETYAA + INVRETYDA + INVRETYCA + INVRETYEA + INVRETQLA + INVRETQMA 
	     + INVRETQDA + INVRETOBA + INVRETOCA + INVRETOMA + INVRETONA + INVRETODA + INVRETUAA + INVRETUHA + INVRETUOA + INVRETVAA 
             + INVRETXFRA 
	     + INVRETXGRA + INVRETXHRA + INVRETXIRA + INVRETXKRA + INVRETXLRA + INVRETXMRA + INVRETXNRA + INVRETXORA + INVRETXPRA 
	     + INVRETXURA + INVRETXQRA + INVRETXRRA + INVRETXSRA + INVRETXTRA + INVRETYBRA + INVRETYARA + INVRETYDRA + INVRETYCRA 
	     + INVRETYERA ; 

INDPLAF2 = positif(RNIDOM2 - TOTALPLAF2 - TOTALPLAFA) ;

regle 402024:
application : iliad ;


VARTMP1 = 0 ;
MAXDOM2 = max(0,RNIDOM2 - TOTALPLAFA) ;

INVRETOIA = min(NINVRETOI , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOIA ;

INVRETOJA = min(NINVRETOJ , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOJA ;

INVRETOKA = min(NINVRETOK , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOKA ;

INVRETOPA = min(NINVRETOP , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOPA ;

INVRETOQA = min(NINVRETOQ , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOQA ;

INVRETORA = min(NINVRETOR , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETORA ;

INVRETOEA = min(NINVRETOE , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOEA ;

INVRETOFA = min(NINVRETOF , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOFA ;

INVRETUBA = min(NINVRETUB , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUBA ;

INVRETUCA = min(NINVRETUC , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUCA ;

INVRETUIA = min(NINVRETUI , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUIA ;

INVRETUJA = min(NINVRETUJ , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUJA ;

INVRETUPA = min(NINVRETUP , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUPA ;

INVRETUQA = min(NINVRETUQ , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUQA ;

INVRETVBA = min(NINVRETVB , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVBA ;

INVRETVCA = min(NINVRETVC , max(0 , MAXDOM2 - VARTMP1)) ;
VARTMP1 = 0 ;

TOTALPLAFB = INVRETOIA + INVRETOJA 
             + INVRETOKA + INVRETOPA + INVRETOQA + INVRETORA + INVRETOEA + INVRETOFA + INVRETUBA + INVRETUCA + INVRETUIA + INVRETUJA 
	     + INVRETUPA + INVRETUQA + INVRETVBA + INVRETVCA ;
 
INDPLAF3 = positif(RNIDOM3 - TOTALPLAF3 - TOTALPLAFA - TOTALPLAFB) ;

regle 402026:
application : iliad ;


VARTMP1 = 0 ;
MAXDOM3 = max(0,RNIDOM3 -TOTALPLAFA-TOTALPLAFB) ;

INVRETCDA = min(arr(NINVRETCD*TX34/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCDA ;

INVRETBJA = min(arr(NINVRETBJ*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBJA ;

INVRETBOA = min(arr(NINVRETBO*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBOA ;

INVRETBTA = min(arr(NINVRETBT*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBTA ;

INVRETBYA = min(arr(NINVRETBY*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBYA ;

INVRETCCA = min(arr(NINVRETCC*TX44/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCCA ;

INVRETBIA = min(arr(NINVRETBI*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBIA ;

INVRETBNA = min(arr(NINVRETBN*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBNA ;

INVRETBSA = min(arr(NINVRETBS*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBSA ;

INVRETBXA = min(arr(NINVRETBX*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBXA ;

INVRETBKA = min(NINVRETBK , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBKA ;

INVRETBPA = min(NINVRETBP , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBPA ;

INVRETBUA = min(NINVRETBU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBUA ;

INVRETBZA = min(NINVRETBZ , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBZA ;

INVRETCEA = min(NINVRETCE , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCEA ;

INVRETBMA = min(NINVRETBM , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBMA ;

INVRETBRA = min(NINVRETBR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBRA ;

INVRETBWA = min(NINVRETBW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBWA ;

INVRETCBA = min(NINVRETCB , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCBA ;

INVRETCGA = min(NINVRETCG , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCGA ;

INVRETCTA = min(arr(NINVRETCT*TX34/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCTA ;

INVRETCJA = min(arr(NINVRETCJ*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCJA ;

INVRETCOA = min(arr(NINVRETCO*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCOA ;

INVRETCSA = min(arr(NINVRETCS*TX44/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCSA ;

INVRETCIA = min(arr(NINVRETCI*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCIA ;

INVRETCNA = min(arr(NINVRETCN*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCNA ;

INVRETCKA = min(NINVRETCK , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCKA ;

INVRETCPA = min(NINVRETCP , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCPA ;

INVRETCUA = min(NINVRETCU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCUA ;

INVRETCMA = min(NINVRETCM , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCMA ;

INVRETCRA = min(NINVRETCR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCRA ;

INVRETCWA = min(NINVRETCW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCWA ;

INVRETDTA = min(arr(NINVRETDT*TX34/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDTA ;

INVRETDJA = min(arr(NINVRETDJ*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDJA ;

INVRETDOA = min(arr(NINVRETDO*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDOA ;

INVRETDSA = min(arr(NINVRETDS*TX44/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDSA ;

INVRETDIA = min(arr(NINVRETDI*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDIA ;

INVRETDNA = min(arr(NINVRETDN*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDNA ;

INVRETDKA = min(NINVRETDK , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDKA ;

INVRETDPA = min(NINVRETDP , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDPA ;

INVRETDUA = min(NINVRETDU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDUA ;

INVRETDMA = min(NINVRETDM , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDMA ;

INVRETDRA = min(NINVRETDR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDRA ;

INVRETDWA = min(NINVRETDW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDWA ;

INVRETETA = min(arr(NINVRETET*TX34/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETETA ;

INVRETEOA = min(arr(NINVRETEO*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETEOA ;

INVRETESA = min(arr(NINVRETES*TX44/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETESA ;

INVRETENA = min(arr(NINVRETEN*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETENA ;

INVRETEPA = min(NINVRETEP , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETEPA ;

INVRETEUA = min(NINVRETEU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETEUA ;

INVRETERA = min(NINVRETER , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETERA ;

INVRETEWA = min(NINVRETEW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETEWA ;

INVRETFTA = min(arr(NINVRETFT*TX34/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFTA ;

INVRETFOA = min(arr(NINVRETFO*TX375/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFOA ;

INVRETFSA = min(arr(NINVRETFS*TX44/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFSA ;

INVRETFNA = min(arr(NINVRETFN*TX4737/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFNA ;

INVRETGTA = min(arr(NINVRETGT*TX34/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETGTA ;

INVRETGSA = min(arr(NINVRETGS*TX44/100) , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETGSA ;

INVRETFPA = min(NINVRETFP , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFPA ;

INVRETFUA = min(NINVRETFU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFUA ;

INVRETFRA = min(NINVRETFR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFRA ;

INVRETFWA = min(NINVRETFW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFWA ;

INVRETGUA = min(NINVRETGU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETGUA ;

INVRETGWA = min(NINVRETGW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETGWA ;

INVRETOTA = min(NINVRETOT , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOTA ;

INVRETOUA = min(NINVRETOU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOUA ;

INVRETOVA = min(NINVRETOV , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOVA ;

INVRETOWA = min(NINVRETOW , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOWA ;

INVRETOGA = min(NINVRETOG , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOGA ;

INVRETOXA = min(NINVRETOX , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOXA ;

INVRETOYA = min(NINVRETOY , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOYA ;

INVRETOZA = min(NINVRETOZ , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETOZA ;

INVRETUDA = min(NINVRETUD , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUDA ;

INVRETUEA = min(NINVRETUE , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUEA ;

INVRETUFA = min(NINVRETUF , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUFA ;

INVRETUGA = min(NINVRETUG , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUGA ;

INVRETUKA = min(NINVRETUK , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUKA ;

INVRETULA = min(NINVRETUL , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETULA ;

INVRETUMA = min(NINVRETUM , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUMA ;

INVRETUNA = min(NINVRETUN , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUNA ;

INVRETURA = min(NINVRETUR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETURA ;

INVRETUSA = min(NINVRETUS , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUSA ;

INVRETUTA = min(NINVRETUT , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUTA ;

INVRETUUA = min(NINVRETUU , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETUUA ;

INVRETVDA = min(NINVRETVD , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVDA ;

INVRETVEA = min(NINVRETVE , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVEA ;

INVRETVFA = min(NINVRETVF , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVFA ;

INVRETVGA = min(NINVRETVG , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVGA ;

INVRETVHA = min(NINVRETVH , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVHA ;

INVRETVIA = min(NINVRETVI , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVIA ;

INVRETVJA = min(NINVRETVJ , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETVJA ;

INVRETCDRA = min(NINVRETCDR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCDRA ;

INVRETBJRA = min(NINVRETBJR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBJRA ;

INVRETBORA = min(NINVRETBOR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBORA ;

INVRETBTRA = min(NINVRETBTR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBTRA ;

INVRETBYRA = min(NINVRETBYR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBYRA ;

INVRETCCRA = min(NINVRETCCR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCCRA ;

INVRETBIRA = min(NINVRETBIR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBIRA ;

INVRETBNRA = min(NINVRETBNR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBNRA ;

INVRETBSRA = min(NINVRETBSR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBSRA ;

INVRETBXRA = min(NINVRETBXR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETBXRA ;

INVRETCTRA = min(NINVRETCTR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCTRA ;

INVRETCJRA = min(NINVRETCJR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCJRA ;

INVRETCORA = min(NINVRETCOR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCORA ;

INVRETCSRA = min(NINVRETCSR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCSRA ;

INVRETCIRA = min(NINVRETCIR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCIRA ;

INVRETCNRA = min(NINVRETCNR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETCNRA ;

INVRETDTRA = min(NINVRETDTR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDTRA ;

INVRETDJRA = min(NINVRETDJR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDJRA ;

INVRETDORA = min(NINVRETDOR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDORA ;

INVRETDSRA = min(NINVRETDSR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDSRA ;

INVRETDIRA = min(NINVRETDIR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDIRA ;

INVRETDNRA = min(NINVRETDNR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETDNRA ;

INVRETETRA = min(NINVRETETR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETETRA ;

INVRETEORA = min(NINVRETEOR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETEORA ;

INVRETESRA = min(NINVRETESR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETESRA ;

INVRETENRA = min(NINVRETENR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETENRA ;

INVRETFTRA = min(NINVRETFTR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFTRA ;

INVRETFORA = min(NINVRETFOR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFORA ;

INVRETFSRA = min(NINVRETFSR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFSRA ;

INVRETFNRA = min(NINVRETFNR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETFNRA ;

INVRETGTRA = min(NINVRETGTR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = VARTMP1 + INVRETGTRA ;

INVRETGSRA = min(NINVRETGSR , max(0 , MAXDOM3 - VARTMP1)) ;
VARTMP1 = 0 ;

TOTALPLAFC = INVRETBJA + INVRETBOA + INVRETBTA + INVRETBYA + INVRETCDA + INVRETBIA + INVRETBNA 
	     + INVRETBSA + INVRETBXA + INVRETCCA + INVRETBKA + INVRETBPA + INVRETBUA + INVRETBZA + INVRETCEA + INVRETBMA + INVRETBRA + INVRETBWA + INVRETCBA + INVRETCGA 
	     + INVRETCTA + INVRETCJA + INVRETCOA + INVRETCSA + INVRETCIA + INVRETCNA + INVRETCKA + INVRETCPA + INVRETCUA + INVRETCMA + INVRETCRA + INVRETCWA + INVRETDTA 
	     + INVRETDJA + INVRETDOA + INVRETDSA + INVRETDIA + INVRETDNA + INVRETETA + INVRETEOA + INVRETESA + INVRETENA + INVRETFTA + INVRETFOA + INVRETFSA + INVRETFNA
	     + INVRETGTA + INVRETGSA + INVRETDKA + INVRETDPA + INVRETDUA + INVRETDMA + INVRETDRA + INVRETDWA + INVRETEPA + INVRETEUA + INVRETERA + INVRETEWA + INVRETFPA 
	     + INVRETFUA + INVRETFRA + INVRETFWA + INVRETGUA + INVRETGWA + INVRETOTA + INVRETOUA + INVRETOVA + INVRETOWA + INVRETOGA + INVRETOXA + INVRETOYA + INVRETOZA 
	     + INVRETUDA + INVRETUEA + INVRETUFA + INVRETUGA + INVRETUKA + INVRETULA + INVRETUMA + INVRETUNA + INVRETURA + INVRETUSA + INVRETUTA + INVRETUUA + INVRETVDA 
	     + INVRETVEA + INVRETVFA + INVRETVGA + INVRETVHA + INVRETVIA + INVRETVJA
             + INVRETBJRA + INVRETBORA + INVRETBTRA + INVRETBYRA 
	     + INVRETCDRA + INVRETBIRA + INVRETBNRA + INVRETBSRA + INVRETBXRA + INVRETCCRA + INVRETCTRA + INVRETCJRA + INVRETCORA + INVRETCSRA + INVRETCIRA + INVRETCNRA 
	     + INVRETDTRA + INVRETDJRA + INVRETDORA + INVRETDSRA + INVRETDIRA + INVRETDNRA + INVRETETRA + INVRETEORA + INVRETESRA + INVRETENRA + INVRETFTRA + INVRETFORA 
	     + INVRETFSRA + INVRETFNRA + INVRETGTRA + INVRETGSRA ;

INDPLAF = positif(TOTALPLAFA + TOTALPLAFB + TOTALPLAFC - TOTALPLAF1 - TOTALPLAF2 - TOTALPLAF3) * positif(INDPLAF1 + INDPLAF2 + INDPLAF3) * positif(OPTPLAF15) ;


ALOGDOM_1 = (INVLOG2008 + INVLGDEB2009 + INVLGDEB + INVOMLOGOA + INVOMLOGOH + INVOMLOGOL + INVOMLOGOO + INVOMLOGOS
                      + (INVRETQL + INVRETQM + INVRETQD + INVRETOB + INVRETOC + INVRETOM + INVRETON + INVRETOI + INVRETOJ + INVRETOK + INVRETOP 
			 + INVRETOQ + INVRETOR + INVRETOT + INVRETOU + INVRETOV + INVRETOW + INVRETOD + INVRETOE + INVRETOF + INVRETOG
                         + INVRETOX + INVRETOY + INVRETOZ + INVRETUA + INVRETUB + INVRETUC + INVRETUD + INVRETUE + INVRETUF + INVRETUG
                         + INVRETUH + INVRETUI + INVRETUJ + INVRETUK + INVRETUL + INVRETUM + INVRETUN + INVRETUO + INVRETUP + INVRETUQ 
			 + INVRETUR + INVRETUS + INVRETUT + INVRETUU + INVRETVA + INVRETVB + INVRETVC + INVRETVD + INVRETVE + INVRETVF
			 + INVRETVG + INVRETVH + INVRETVI + INVRETVJ) * (1 - INDPLAF)
		      + (INVRETQLA + INVRETQMA + INVRETQDA + INVRETOBA + INVRETOCA + INVRETOMA + INVRETONA + INVRETOIA + INVRETOJA + INVRETOKA 
			 + INVRETOPA + INVRETOQA + INVRETORA + INVRETOTA + INVRETOUA + INVRETOVA + INVRETOWA + INVRETODA + INVRETOEA + INVRETOFA 
			 + INVRETOGA + INVRETOXA + INVRETOYA + INVRETOZA + INVRETUAA + INVRETUBA + INVRETUCA + INVRETUDA + INVRETUEA + INVRETUFA 
			 + INVRETUGA + INVRETUHA + INVRETUIA + INVRETUJA + INVRETUKA + INVRETULA + INVRETUMA + INVRETUNA + INVRETUOA + INVRETUPA 
			 + INVRETUQA + INVRETURA + INVRETUSA + INVRETUTA + INVRETUUA + INVRETVAA + INVRETVBA + INVRETVCA + INVRETVDA + INVRETVEA 
			 + INVRETVFA + INVRETVGA + INVRETVHA + INVRETVIA + INVRETVJA) * INDPLAF)
	     * (1 - V_CNR) ;
ALOGDOM = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ALOGDOM_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ALOGDOM_1,ALOGDOM1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ALOGSOC_1 = ((INVRETYE + INVRETYER) * (1 - INDPLAF) + (INVRETYEA + INVRETYERA) * INDPLAF) * (1 - V_CNR) ;
ALOGSOC = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ALOGSOC_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ALOGSOC_1,ALOGSOC1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ADOMSOC1_1 = ((INVRETXF + INVRETXG + INVRETXH + INVRETXI + INVRETXK + INVRETXL 
               + INVRETXM + INVRETXN + INVRETXO + INVRETXP + INVRETXU + INVRETXQ + INVRETXR + INVRETXS + INVRETXT + INVRETYB + INVRETYA + INVRETYC
	       + INVRETYD
	       + INVRETXFR + INVRETXGR + INVRETXHR 
	       + INVRETXIR + INVRETXKR + INVRETXLR + INVRETXMR + INVRETXNR + INVRETXOR + INVRETXPR + INVRETXUR + INVRETXQR + INVRETXRR + INVRETXSR 
	       + INVRETXTR + INVRETYBR + INVRETYAR + INVRETYCR + INVRETYDR) * (1 - INDPLAF) 
	     + (INVRETXFA + INVRETXGA + INVRETXHA 
	        + INVRETXIA + INVRETXKA + INVRETXLA + INVRETXMA + INVRETXNA + INVRETXOA + INVRETXPA + INVRETXUA + INVRETXQA + INVRETXRA + INVRETXSA 
		+ INVRETXTA + INVRETYBA + INVRETYAA + INVRETYCA + INVRETYDA
                + INVRETXFRA + INVRETXGRA 
		+ INVRETXHRA + INVRETXIRA + INVRETXKRA + INVRETXLRA + INVRETXMRA + INVRETXNRA + INVRETXORA + INVRETXPRA + INVRETXURA + INVRETXQRA 
		+ INVRETXRRA + INVRETXSRA + INVRETXTRA + INVRETYBRA + INVRETYARA + INVRETYCRA + INVRETYDRA) * INDPLAF) 
              * (1 - V_CNR) ;
ADOMSOC1 = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ADOMSOC1_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
           + (max(0,min(ADOMSOC1_1,ADOMSOC11731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ALOCENT_1 = ((INVRETGT + INVRETGS + INVRETGU + INVRETGW
              + INVRETGTR + INVRETGSR) * (1 - INDPLAF)
            + (INVRETGTA + INVRETGSA + INVRETGUA + INVRETGWA
               + INVRETGTRA + INVRETGSRA) * INDPLAF)
            * (1 - V_CNR) ;
ALOCENT = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ALOCENT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ALOCENT_1,ALOCENT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

ACOLENT_1 = ((INVRETBJ + INVRETBO + INVRETBT + INVRETBY 
	      + INVRETCD + INVRETBI + INVRETBN + INVRETBS + INVRETBX + INVRETCC + INVRETBK + INVRETBP + INVRETBU + INVRETBZ + INVRETCE + INVRETBM + INVRETBR 
	      + INVRETBW + INVRETCB + INVRETCG + INVRETCT + INVRETCJ + INVRETCO + INVRETCS + INVRETCI + INVRETCN + INVRETCK + INVRETCP + INVRETCU + INVRETCM 
	      + INVRETCR + INVRETCW + INVRETDT + INVRETDJ + INVRETDO + INVRETDS + INVRETDI + INVRETDN + INVRETDK + INVRETDP + INVRETDU + INVRETDM + INVRETDR 
	      + INVRETDW + INVRETET + INVRETEO + INVRETES + INVRETEN + INVRETEP + INVRETEU + INVRETER + INVRETEW + INVRETFT + INVRETFO + INVRETFS + INVRETFN 
	      + INVRETFP + INVRETFU + INVRETFR + INVRETFW
	      + INVRETBJR + INVRETBOR + INVRETBTR + INVRETBYR + INVRETCDR + INVRETBIR + INVRETBNR + INVRETBSR + INVRETBXR + INVRETCCR + INVRETCTR + INVRETCJR 
	      + INVRETCOR + INVRETCSR + INVRETCIR + INVRETCNR + INVRETDTR + INVRETDJR + INVRETDOR + INVRETDSR + INVRETDIR + INVRETDNR + INVRETETR + INVRETEOR 
	      + INVRETESR + INVRETENR + INVRETFTR + INVRETFOR + INVRETFSR + INVRETFNR) * (1 - INDPLAF) 
	   + (INVRETBJA + INVRETBOA + INVRETBTA + INVRETBYA + INVRETCDA + INVRETBIA + INVRETBNA + INVRETBSA + INVRETBXA + INVRETCCA + INVRETBKA + INVRETBPA 
	      + INVRETBUA + INVRETBZA + INVRETCEA + INVRETBMA + INVRETBRA + INVRETBWA + INVRETCBA + INVRETCGA + INVRETCTA + INVRETCJA + INVRETCOA + INVRETCSA 
	      + INVRETCIA + INVRETCNA + INVRETCKA + INVRETCPA + INVRETCUA + INVRETCMA + INVRETCRA + INVRETCWA + INVRETDTA + INVRETDJA + INVRETDOA + INVRETDSA 
	      + INVRETDIA + INVRETDNA + INVRETDKA + INVRETDPA + INVRETDUA + INVRETDMA + INVRETDRA + INVRETDWA + INVRETETA + INVRETEOA + INVRETESA + INVRETENA 
	      + INVRETEPA + INVRETEUA + INVRETERA + INVRETEWA + INVRETFTA + INVRETFOA + INVRETFSA + INVRETFNA + INVRETFPA + INVRETFUA + INVRETFRA + INVRETFWA
	      + INVRETBJRA + INVRETBORA + INVRETBTRA + INVRETBYRA + INVRETCDRA + INVRETBIRA + INVRETBNRA + INVRETBSRA + INVRETBXRA + INVRETCCRA 
	      + INVRETCTRA + INVRETCJRA + INVRETCORA + INVRETCSRA + INVRETCIRA + INVRETCNRA + INVRETDTRA + INVRETDJRA + INVRETDORA + INVRETDSRA + INVRETDIRA 
	      + INVRETDNRA + INVRETETRA + INVRETEORA + INVRETESRA + INVRETENRA + INVRETFTRA + INVRETFORA + INVRETFSRA + INVRETFNRA) * INDPLAF) 
	     * (1 - V_CNR) ;
ACOLENT = positif(null(V_IND_TRAIT-4)+COD9ZA) * (ACOLENT_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(ACOLENT_1,ACOLENT1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

regle 402030:
application : iliad ;


VARTMP1 = 0 ;

NINVRETQB = max(min(INVLOG2008 , RRI1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETQB ;

NINVRETQC = max(min(INVLGDEB2009 , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETQC ;

NINVRETQT = max(min(INVLGDEB , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETQT ;

NINVRETOA = max(min(INVOMLOGOA , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOA ;

NINVRETOH = max(min(INVOMLOGOH , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOH ;

NINVRETOL = max(min(INVOMLOGOL , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOL ;

NINVRETOO = max(min(INVOMLOGOO , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOO ;

NINVRETOS = max(min(INVOMLOGOS , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOS ;

NINVRETQL = max(min(INVLGAUTRE , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETQL ;

NINVRETQM = max(min(INVLGDEB2010 , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETQM ;

NINVRETQD = max(min(INVLOG2009 , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETQD ;

NINVRETOB = max(min(INVOMLOGOB , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOB ;

NINVRETOC = max(min(INVOMLOGOC , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOC ;

NINVRETOI = max(min(INVOMLOGOI , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOI ;

NINVRETOJ = max(min(INVOMLOGOJ , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOJ ;

NINVRETOK = max(min(INVOMLOGOK , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOK ;

NINVRETOM = max(min(INVOMLOGOM , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOM ;

NINVRETON = max(min(INVOMLOGON , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETON ;

NINVRETOP = max(min(INVOMLOGOP , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOP ;

NINVRETOQ = max(min(INVOMLOGOQ , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOQ ; 

NINVRETOR = max(min(INVOMLOGOR , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOR ;

NINVRETOT = max(min(INVOMLOGOT , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOT ;

NINVRETOU = max(min(INVOMLOGOU , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOU ;

NINVRETOV = max(min(INVOMLOGOV , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOV ;

NINVRETOW = max(min(INVOMLOGOW , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOW ;

NINVRETOD = max(min(CODHOD , RRI1-VARTMP1) , 0) * (1 - V_CNR) ; 
VARTMP1 = VARTMP1 + NINVRETOD ;

NINVRETOE = max(min(CODHOE , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOE ;

NINVRETOF = max(min(CODHOF , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOF ;

NINVRETOG = max(min(CODHOG , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOG ;

NINVRETOX = max(min(CODHOX , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOX ;

NINVRETOY = max(min(CODHOY , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOY ;

NINVRETOZ = max(min(CODHOZ , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETOZ ;

NINVRETUA = max(min(CODHUA , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUA ;

NINVRETUB = max(min(CODHUB , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUB ;

NINVRETUC = max(min(CODHUC , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUC ;

NINVRETUD = max(min(CODHUD , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUD ; 

NINVRETUE = max(min(CODHUE , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUE ;

NINVRETUF = max(min(CODHUF , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUF ;

NINVRETUG = max(min(CODHUG , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUG ;

NINVRETUH = max(min(CODHUH , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUH ;

NINVRETUI = max(min(CODHUI , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUI ;

NINVRETUJ = max(min(CODHUJ , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUJ ;

NINVRETUK = max(min(CODHUK , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUK ;

NINVRETUL = max(min(CODHUL , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUL ;

NINVRETUM = max(min(CODHUM , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUM ;

NINVRETUN = max(min(CODHUN , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUN ;

NINVRETUO = max(min(CODHUO , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUO ;

NINVRETUP = max(min(CODHUP , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUP ;

NINVRETUQ = max(min(CODHUQ , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUQ ;

NINVRETUR = max(min(CODHUR , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUR ;

NINVRETUS = max(min(CODHUS , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUS ;

NINVRETUT = max(min(CODHUT , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUT ;

NINVRETUU = max(min(CODHUU , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETUU ; 

NINVRETVA = max(min(CODHVA , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVA ;

NINVRETVB = max(min(CODHVB , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVB ;

NINVRETVC = max(min(CODHVC , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVC ;

NINVRETVD = max(min(CODHVD , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVD ;

NINVRETVE = max(min(CODHVE , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVE ;

NINVRETVF = max(min(CODHVF , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVF ;

NINVRETVG = max(min(CODHVG , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVG ;

NINVRETVH = max(min(CODHVH , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVH ;

NINVRETVI = max(min(CODHVI , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETVI ;

NINVRETVJ = max(min(CODHVJ , RRI1-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = 0 ; 

regle 402040:
application : iliad ;


VARTMP1 = 0 ;

NINVRETXK = max(min(CODHXK , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXK ;

NINVRETXF = max(min(CODHXF , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXF ;

NINVRETXG = max(min(CODHXG , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXG ;

NINVRETXH = max(min(CODHXH , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXH ;

NINVRETXI = max(min(CODHXI , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXI ;

NINVRETXP = max(min(CODHXP , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXP ;

NINVRETXL = max(min(CODHXL , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXL ; 

NINVRETXM = max(min(CODHXM , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXM ;

NINVRETXN = max(min(CODHXN , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXN ;

NINVRETXO = max(min(CODHXO , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXO ;

NINVRETXU = max(min(CODHXU , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXU ;

NINVRETXQ = max(min(CODHXQ , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXQ ;

NINVRETXR = max(min(CODHXR , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXR ;

NINVRETXS = max(min(CODHXS , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXS ;

NINVRETXT = max(min(CODHXT , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETXT ;

NINVRETYB = max(min(CODHYB , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETYB ;

NINVRETYA = max(min(CODHYA , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETYA ;

NINVRETYD = max(min(CODHYD , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETYD ;

NINVRETYC = max(min(CODHYC , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETYC ;

NINVRETYE = max(min(CODHYE , NNRI2-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = 0 ;

NRDOMSOC1 = NINVRETXK + NINVRETXF + NINVRETXG + NINVRETXH + NINVRETXI + NINVRETXP + NINVRETXL 
            + NINVRETXM + NINVRETXN + NINVRETXO + NINVRETXU + NINVRETXQ + NINVRETXR + NINVRETXS 
	    + NINVRETXT + NINVRETYB + NINVRETYA + NINVRETYD + NINVRETYC ;

NRLOGSOC = NINVRETYE ;


NINVRETXFR = NINVRETXF - arr(NINVRETXF * TX35 / 100) ;

NINVRETXGR = NINVRETXG - arr(NINVRETXG * TX35 / 100) ;

NINVRETXHR = NINVRETXH - arr(NINVRETXH * TX35 / 100) ;

NINVRETXIR = NINVRETXI - arr(NINVRETXI * TX35 / 100) ;

NINVRETXKR = NINVRETXK - arr(NINVRETXK * TX30 / 100) ;

NINVRETXLR = NINVRETXL - arr(NINVRETXL * TX35 / 100) ;

NINVRETXMR = NINVRETXM - arr(NINVRETXM * TX35 / 100) ;

NINVRETXNR = NINVRETXN - arr(NINVRETXN * TX35 / 100) ;

NINVRETXOR = NINVRETXO - arr(NINVRETXO * TX35 / 100) ;

NINVRETXPR = NINVRETXP - arr(NINVRETXP * TX30 / 100) ;

NINVRETXUR = NINVRETXU - arr(NINVRETXU * TX30 / 100) ;

NINVRETXQR = NINVRETXQ - arr(NINVRETXQ * TX35 / 100) ;

NINVRETXRR = NINVRETXR - arr(NINVRETXR * TX35 / 100) ;

NINVRETXSR = NINVRETXS - arr(NINVRETXS * TX35 / 100) ;

NINVRETXTR = NINVRETXT - arr(NINVRETXT * TX35 / 100) ;

NINVRETYBR = NINVRETYB - arr(NINVRETYB * TX30 / 100) ;

NINVRETYAR = NINVRETYA - arr(NINVRETYA * TX35 / 100) ;

NINVRETYDR = NINVRETYD - arr(NINVRETYD * TX30 / 100) ;

NINVRETYCR = NINVRETYC - arr(NINVRETYC * TX35 / 100) ;

NINVRETYER = NINVRETYE - arr(NINVRETYE * TX30 / 100) ;

regle 402050:
application : iliad ;


VARTMP1 = 0 ;

INVRETXK = min(arr(NINVRETXK * TX30 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXK ;

INVRETXF = min(arr(NINVRETXF * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXF ;

INVRETXG = min(arr(NINVRETXG * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXG ;

INVRETXH = min(arr(NINVRETXH * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXH ;

INVRETXI = min(arr(NINVRETXI * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXI ;

INVRETXP = min(arr(NINVRETXP * TX30 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXP ;

INVRETXL = min(arr(NINVRETXL * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXL ;

INVRETXM = min(arr(NINVRETXM * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXM ;

INVRETXN = min(arr(NINVRETXN * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXN ;

INVRETXO = min(arr(NINVRETXO * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXO ;

INVRETXU = min(arr(NINVRETXU * TX30 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXU ;

INVRETXQ = min(arr(NINVRETXQ * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXQ ;

INVRETXR = min(arr(NINVRETXR * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXR ;

INVRETXS = min(arr(NINVRETXS * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXS ;

INVRETXT = min(arr(NINVRETXT * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETXT ;

INVRETYB = min(arr(NINVRETYB * TX30 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETYB ;

INVRETYA = min(arr(NINVRETYA * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETYA ;

INVRETYD = min(arr(NINVRETYD * TX30 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETYD ;

INVRETYC = min(arr(NINVRETYC * TX35 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETYC ;

INVRETYE = min(arr(NINVRETYE * TX30 / 100) , max(0 , PLAF_INVDOM -VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = 0 ;

INVRETSOC = INVRETXF + INVRETXG + INVRETXH + INVRETXI 
            + INVRETXK + INVRETXL + INVRETXM + INVRETXN + INVRETXO + INVRETXP + INVRETXU + INVRETXQ 
	    + INVRETXR + INVRETXS + INVRETXT + INVRETYB + INVRETYA + INVRETYD + INVRETYC + INVRETYE ;


INVRETXKR = min(arr(INVRETXK * 7 / 3) , NINVRETXK - INVRETXK) * (1 - null(1 - abs(arr(INVRETXK * 7 / 3) - (NINVRETXK - INVRETXK))))
                + (NINVRETXK - INVRETXK) * null(1 - abs(arr(INVRETXK * 7 / 3) - (NINVRETXK - INVRETXK))) ;

INVRETXFR = min(arr(INVRETXF * 13 / 7) , NINVRETXF - INVRETXF) * (1 - null(1 - abs(arr(INVRETXF * 13 / 7) - (NINVRETXF - INVRETXF)))) 
                + (NINVRETXF - INVRETXF) * null(1 - abs(arr(INVRETXF * 13 / 7) - (NINVRETXF - INVRETXF))) ;

INVRETXGR = min(arr(INVRETXG * 13 / 7) , NINVRETXG - INVRETXG) * (1 - null(1 - abs(arr(INVRETXG * 13 / 7) - (NINVRETXG - INVRETXG)))) 
                + (NINVRETXG - INVRETXG) * null(1 - abs(arr(INVRETXG * 13 / 7) - (NINVRETXG - INVRETXG))) ;

INVRETXHR = min(arr(INVRETXH * 13 / 7) , NINVRETXH - INVRETXH) * (1 - null(1 - abs(arr(INVRETXH * 13 / 7) - (NINVRETXH - INVRETXH))))
                + (NINVRETXH - INVRETXH) * null(1 - abs(arr(INVRETXH * 13 / 7) - (NINVRETXH - INVRETXH))) ;

INVRETXIR = min(arr(INVRETXI * 13 / 7) , NINVRETXI - INVRETXI) * (1 - null(1 - abs(arr(INVRETXI * 13 / 7) - (NINVRETXI - INVRETXI))))
                + (NINVRETXI - INVRETXI) * null(1 - abs(arr(INVRETXI * 13 / 7) - (NINVRETXI - INVRETXI))) ;

INVRETXPR = min(arr(INVRETXP * 7 / 3) , NINVRETXP - INVRETXP) * (1 - null(1 - abs(arr(INVRETXP * 7 / 3) - (NINVRETXP - INVRETXP))))
                + (NINVRETXP - INVRETXP) * null(1 - abs(arr(INVRETXP * 7 / 3) - (NINVRETXP - INVRETXP))) ;

INVRETXLR = min(arr(INVRETXL * 13 / 7) , NINVRETXL - INVRETXL) * (1 - null(1 - abs(arr(INVRETXL * 13 / 7) - (NINVRETXL - INVRETXL))))
                + (NINVRETXL - INVRETXL) * null(1 - abs(arr(INVRETXL * 13 / 7) - (NINVRETXL - INVRETXL))) ;

INVRETXMR = min(arr(INVRETXM * 13 / 7) , NINVRETXM - INVRETXM) * (1 - null(1 - abs(arr(INVRETXM * 13 / 7) - (NINVRETXM - INVRETXM))))
                + (NINVRETXM - INVRETXM) * null(1 - abs(arr(INVRETXM * 13 / 7) - (NINVRETXM - INVRETXM))) ;

INVRETXNR = min(arr(INVRETXN * 13 / 7) , NINVRETXN - INVRETXN) * (1 - null(1 - abs(arr(INVRETXN * 13 / 7) - (NINVRETXN - INVRETXN))))
                + (NINVRETXN - INVRETXN) * null(1 - abs(arr(INVRETXN * 13 / 7) - (NINVRETXN - INVRETXN))) ;

INVRETXOR = min(arr(INVRETXO * 13 / 7) , NINVRETXO - INVRETXO) * (1 - null(1 - abs(arr(INVRETXO * 13 / 7) - (NINVRETXO - INVRETXO))))
                + (NINVRETXO - INVRETXO) * null(1 - abs(arr(INVRETXO * 13 / 7) - (NINVRETXO - INVRETXO))) ;

INVRETXUR = min(arr(INVRETXU * 7 / 3) , NINVRETXU - INVRETXU) * (1 - null(1 - abs(arr(INVRETXU * 7 / 3) - (NINVRETXU - INVRETXU))))
                + (NINVRETXU - INVRETXU) * null(1 - abs(arr(INVRETXU * 7 / 3) - (NINVRETXU - INVRETXU))) ;

INVRETXQR = min(arr(INVRETXQ * 13 / 7) , NINVRETXQ - INVRETXQ) * (1 - null(1 - abs(arr(INVRETXQ * 13 / 7) - (NINVRETXQ - INVRETXQ))))
                + (NINVRETXQ - INVRETXQ) * null(1 - abs(arr(INVRETXQ * 13 / 7) - (NINVRETXQ - INVRETXQ))) ;

INVRETXRR = min(arr(INVRETXR * 13 / 7) , NINVRETXR - INVRETXR) * (1 - null(1 - abs(arr(INVRETXR * 13 / 7) - (NINVRETXR - INVRETXR))))
                + (NINVRETXR - INVRETXR) * null(1 - abs(arr(INVRETXR * 13 / 7) - (NINVRETXR - INVRETXR))) ;

INVRETXSR = min(arr(INVRETXS * 13 / 7) , NINVRETXS - INVRETXS) * (1 - null(1 - abs(arr(INVRETXS * 13 / 7) - (NINVRETXS - INVRETXS))))
                + (NINVRETXS - INVRETXS) * null(1 - abs(arr(INVRETXS * 13 / 7) - (NINVRETXS - INVRETXS))) ;

INVRETXTR = min(arr(INVRETXT * 13 / 7) , NINVRETXT - INVRETXT) * (1 - null(1 - abs(arr(INVRETXT * 13 / 7) - (NINVRETXT - INVRETXT))))
                + (NINVRETXT - INVRETXT) * null(1 - abs(arr(INVRETXT * 13 / 7) - (NINVRETXT - INVRETXT))) ;

INVRETYBR = min(arr(INVRETYB * 7 / 3) , NINVRETYB - INVRETYB) * (1 - null(1 - abs(arr(INVRETYB * 7 / 3) - (NINVRETYB - INVRETYB))))
                + (NINVRETYB - INVRETYB) * null(1 - abs(arr(INVRETYB * 7 / 3) - (NINVRETYB - INVRETYB))) ;

INVRETYAR = min(arr(INVRETYA * 13 / 7) , NINVRETYA - INVRETYA) * (1 - null(1 - abs(arr(INVRETYA * 13 / 7) - (NINVRETYA - INVRETYA))))
                + (NINVRETYA - INVRETYA) * null(1 - abs(arr(INVRETYA * 13 / 7) - (NINVRETYA - INVRETYA))) ;

INVRETYDR = min(arr(INVRETYD * 7 / 3) , NINVRETYD - INVRETYD) * (1 - null(1 - abs(arr(INVRETYD * 7 / 3) - (NINVRETYD - INVRETYD))))
                + (NINVRETYD - INVRETYD) * null(1 - abs(arr(INVRETYD * 7 / 3) - (NINVRETYD - INVRETYD))) ;

INVRETYCR = min(arr(INVRETYC * 13 / 7) , NINVRETYC - INVRETYC) * (1 - null(1 - abs(arr(INVRETYC * 13 / 7) - (NINVRETYC - INVRETYC))))
                + (NINVRETYC - INVRETYC) * null(1 - abs(arr(INVRETYC * 13 / 7) - (NINVRETYC - INVRETYC))) ;

INVRETYER = min(arr(INVRETYE * 7 / 3) , NINVRETYE - INVRETYE) * (1 - null(1 - abs(arr(INVRETYE * 7 / 3) - (NINVRETYE - INVRETYE))))
                + (NINVRETYE - INVRETYE) * null(1 - abs(arr(INVRETYE * 7 / 3) - (NINVRETYE - INVRETYE))) ;

regle 402070:
application : iliad ;


NINVENT12 = NRLOGSOC + NRDOMSOC1 ;


VARTMP1 = 0 ;

NINVRETCD = max(min(CODHCD , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCD ;

NINVRETBJ = max(min(CODHBJ , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBJ ;

NINVRETBO = max(min(CODHBO , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBO ;

NINVRETBT = max(min(CODHBT , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBT ;

NINVRETBY = max(min(CODHBY , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBY ;

NINVRETCC = max(min(CODHCC , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCC ;

NINVRETBI = max(min(CODHBI , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBI ;

NINVRETBN = max(min(CODHBN , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBN ;

NINVRETBS = max(min(CODHBS , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBS ;

NINVRETBX = max(min(CODHBX , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBX ;

NINVRETBK = max(min(CODHBK , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBK ;

NINVRETBP = max(min(CODHBP , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBP ;

NINVRETBU = max(min(CODHBU , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBU ;

NINVRETBZ = max(min(CODHBZ , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBZ ;

NINVRETCE = max(min(CODHCE , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCE ;

NINVRETBM = max(min(CODHBM , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBM ;

NINVRETBR = max(min(CODHBR , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBR ;

NINVRETBW = max(min(CODHBW , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETBW ;

NINVRETCB = max(min(CODHCB , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCB ;

NINVRETCG = max(min(CODHCG , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCG ;

NINVRETCT = max(min(CODHCT , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCT ;

NINVRETCJ = max(min(CODHCJ , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCJ ;

NINVRETCO = max(min(CODHCO , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCO ;

NINVRETCS = max(min(CODHCS , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCS ;

NINVRETCI = max(min(CODHCI , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCI ;

NINVRETCN = max(min(CODHCN , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCN ;

NINVRETCK = max(min(CODHCK , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCK ;

NINVRETCP = max(min(CODHCP , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCP ;

NINVRETCU = max(min(CODHCU , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCU ;

NINVRETCM = max(min(CODHCM , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCM ;

NINVRETCR = max(min(CODHCR , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCR ;

NINVRETCW = max(min(CODHCW , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETCW ;

NINVRETDT = max(min(CODHDT , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDT ;

NINVRETDJ = max(min(CODHDJ , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDJ ;

NINVRETDO = max(min(CODHDO , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDO ;

NINVRETDS = max(min(CODHDS , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDS ;

NINVRETDI = max(min(CODHDI , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDI ;

NINVRETDN = max(min(CODHDN , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDN ;

NINVRETDK = max(min(CODHDK , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDK ;

NINVRETDP = max(min(CODHDP , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDP ;

NINVRETDU = max(min(CODHDU , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDU ;

NINVRETDM = max(min(CODHDM , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDM ;

NINVRETDR = max(min(CODHDR , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDR ;

NINVRETDW = max(min(CODHDW , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETDW ;

NINVRETET = max(min(CODHET , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETET ;

NINVRETEO = max(min(CODHEO , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETEO ;

NINVRETES = max(min(CODHES , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETES ;

NINVRETEN = max(min(CODHEN , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETEN ;

NINVRETEP = max(min(CODHEP , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETEP ;

NINVRETEU = max(min(CODHEU , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETEU ;

NINVRETER = max(min(CODHER , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETER ;

NINVRETEW = max(min(CODHEW , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETEW ;

NINVRETFT = max(min(CODHFT , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFT ;

NINVRETFO = max(min(CODHFO , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFO ;

NINVRETFS = max(min(CODHFS , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFS ;

NINVRETFN = max(min(CODHFN , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFN ;

NINVRETFP = max(min(CODHFP , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFP ;

NINVRETFU = max(min(CODHFU , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFU ;

NINVRETFR = max(min(CODHFR , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFR ;

NINVRETFW = max(min(CODHFW , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETFW ;

NINVRETGT = max(min(CODHGT , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETGT ;

NINVRETGS = max(min(CODHGS , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETGS ;

NINVRETGU = max(min(CODHGU , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + NINVRETGU ;

NINVRETGW = max(min(CODHGW , NNRI2-NINVENT12-VARTMP1) , 0) * (1 - V_CNR) ;
VARTMP1 = 0 ;


NINVRETBJR = NINVRETBJ - arr(NINVRETBJ * TX375/100) ;

NINVRETBOR = NINVRETBO - arr(NINVRETBO * TX375/100) ;

NINVRETBIR = NINVRETBI - arr(NINVRETBI * TX4737/100) ;

NINVRETBNR = NINVRETBN - arr(NINVRETBN * TX4737/100) ;

NINVRETBTR = NINVRETBT - arr(NINVRETBT * TX375/100) ;

NINVRETBYR = NINVRETBY - arr(NINVRETBY * TX375/100) ;

NINVRETCDR = NINVRETCD - arr(NINVRETCD * TX34/100) ;

NINVRETBSR = NINVRETBS - arr(NINVRETBS * TX4737/100) ;

NINVRETBXR = NINVRETBX - arr(NINVRETBX * TX4737/100) ;

NINVRETCCR = NINVRETCC - arr(NINVRETCC * TX44/100) ;

NINVRETCTR = NINVRETCT - arr(NINVRETCT * TX34/100) ;

NINVRETCJR = NINVRETCJ - arr(NINVRETCJ * TX375/100) ;

NINVRETCOR = NINVRETCO - arr(NINVRETCO * TX375/100) ;

NINVRETCSR = NINVRETCS - arr(NINVRETCS * TX44/100) ;

NINVRETCIR = NINVRETCI - arr(NINVRETCI * TX4737/100) ;

NINVRETCNR = NINVRETCN - arr(NINVRETCN * TX4737/100) ;

NINVRETDTR = NINVRETDT - arr(NINVRETDT * TX34/100) ;

NINVRETDJR = NINVRETDJ - arr(NINVRETDJ * TX375/100) ;

NINVRETDOR = NINVRETDO - arr(NINVRETDO * TX375/100) ;

NINVRETDSR = NINVRETDS - arr(NINVRETDS * TX44/100) ;

NINVRETDIR = NINVRETDI - arr(NINVRETDI * TX4737/100) ;

NINVRETDNR = NINVRETDN - arr(NINVRETDN * TX4737/100) ;

NINVRETETR = NINVRETET - arr(NINVRETET * TX34/100) ;

NINVRETEOR = NINVRETEO - arr(NINVRETEO * TX375/100) ;

NINVRETESR = NINVRETES - arr(NINVRETES * TX44/100) ;

NINVRETENR = NINVRETEN - arr(NINVRETEN * TX4737/100) ;

NINVRETFTR = NINVRETFT - arr(NINVRETFT * TX34/100) ;

NINVRETFOR = NINVRETFO - arr(NINVRETFO * TX375/100) ;

NINVRETFSR = NINVRETFS - arr(NINVRETFS * TX44/100) ;

NINVRETFNR = NINVRETFN - arr(NINVRETFN * TX4737/100) ;

NINVRETGTR = NINVRETGT - arr(NINVRETGT * TX34/100) ;

NINVRETGSR = NINVRETGS - arr(NINVRETGS * TX44/100) ;

regle 402080:
application : iliad ;


VARTMP1 = 0 ;

INVRETCD = min(arr(NINVRETCD * TX34/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCD ;

INVRETBJ = min(arr(NINVRETBJ * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBJ ;

INVRETBO = min(arr(NINVRETBO * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBO ;

INVRETBT = min(arr(NINVRETBT * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBT ;

INVRETBY = min(arr(NINVRETBY * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBY ;

INVRETCC = min(arr(NINVRETCC * TX44/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCC ;

INVRETBI = min(arr(NINVRETBI * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBI ;

INVRETBN = min(arr(NINVRETBN * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBN ;

INVRETBS = min(arr(NINVRETBS * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBS ;

INVRETBX = min(arr(NINVRETBX * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBX ;

INVRETCT = min(arr(NINVRETCT * TX34/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCT ;

INVRETCJ = min(arr(NINVRETCJ * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCJ ;

INVRETCO = min(arr(NINVRETCO * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCO ;

INVRETCS = min(arr(NINVRETCS * TX44/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCS ;

INVRETCI = min(arr(NINVRETCI * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCI ;

INVRETCN = min(arr(NINVRETCN * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCN ;

INVRETDT = min(arr(NINVRETDT * TX34/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDT ;

INVRETDJ = min(arr(NINVRETDJ * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDJ ;

INVRETDO = min(arr(NINVRETDO * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDO ;

INVRETDS = min(arr(NINVRETDS * TX44/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDS ;

INVRETDI = min(arr(NINVRETDI * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDI ;

INVRETDN = min(arr(NINVRETDN * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDN ;

INVRETET = min(arr(NINVRETET * TX34/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETET ;

INVRETEO = min(arr(NINVRETEO * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETEO ;

INVRETES = min(arr(NINVRETES * TX44/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETES ;

INVRETEN = min(arr(NINVRETEN * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETEN ;

INVRETFT = min(arr(NINVRETFT * TX34/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETFT ;

INVRETFO = min(arr(NINVRETFO * TX375/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETFO ;

INVRETFS = min(arr(NINVRETFS * TX44/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETFS ;

INVRETFN = min(arr(NINVRETFN * TX4737/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETFN ;

INVRETGT = min(arr(NINVRETGT * TX34/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETGT ;

INVRETGS = min(arr(NINVRETGS * TX44/100) , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETGS ;

INVRETBK = min(NINVRETBK , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBK ;

INVRETBP = min(NINVRETBP , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBP ;

INVRETBU = min(NINVRETBU , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBU ;

INVRETBZ = min(NINVRETBZ , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETBZ ;

INVRETCE = min(NINVRETCE , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCE ;

INVRETCK = min(NINVRETCK , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCK ;

INVRETCP = min(NINVRETCP , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCP ;

INVRETCU = min(NINVRETCU , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETCU ;

INVRETDK = min(NINVRETDK , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDK ;

INVRETDP = min(NINVRETDP , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDP ;

INVRETDU = min(NINVRETDU , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETDU ;

INVRETEP = min(NINVRETEP , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETEP ;

INVRETEU = min(NINVRETEU , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETEU ;

INVRETFP = min(NINVRETFP , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETFP ;

INVRETFU = min(NINVRETFU , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETFU ;

INVRETGU = min(NINVRETGU , max(0 , PLAF_INVDOM4 -INVRETSOC-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = 0 ;

INVRETENT = INVRETBJ + INVRETBO 
	    + INVRETBT + INVRETBY + INVRETCD + INVRETBI + INVRETBN + INVRETBS + INVRETBX + INVRETCC + INVRETCT + INVRETCJ + INVRETCO + INVRETCS + INVRETCI 
	    + INVRETCN + INVRETDT + INVRETDJ + INVRETDO + INVRETDS + INVRETDI + INVRETDN + INVRETET + INVRETEO + INVRETES + INVRETEN + INVRETFT + INVRETFO 
	    + INVRETFS + INVRETFN + INVRETGT + INVRETGS + INVRETBK + INVRETBP + INVRETBU + INVRETBZ + INVRETCE + INVRETCK + INVRETCP + INVRETCU + INVRETDK 
	    + INVRETDP + INVRETDU + INVRETEP + INVRETEU + INVRETFP + INVRETFU + INVRETGU ;

INVRETBM = NINVRETBM ;

INVRETBR = NINVRETBR ;

INVRETBW = NINVRETBW ;

INVRETCB = NINVRETCB ;

INVRETCG = NINVRETCG ;

INVRETCM = NINVRETCM ;

INVRETCR = NINVRETCR ;

INVRETCW = NINVRETCW ;

INVRETDM = NINVRETDM ;

INVRETDR = NINVRETDR ;

INVRETDW = NINVRETDW ;

INVRETER = NINVRETER ;

INVRETEW = NINVRETEW ;

INVRETFR = NINVRETFR ;

INVRETFW = NINVRETFW ;

INVRETGW = NINVRETGW ;


INVRETBJR = min(arr(INVRETBJ * 5/3) , NINVRETBJ - INVRETBJ) * (1 - null(1 - abs(arr(INVRETBJ * 5/3) - (NINVRETBJ - INVRETBJ))))
             + (NINVRETBJ - INVRETBJ) * null(1 - abs(arr(INVRETBJ * 5/3) - (NINVRETBJ - INVRETBJ))) ;

INVRETBOR = min(arr(INVRETBO * 5/3) , NINVRETBO - INVRETBO) * (1 - null(1 - abs(arr(INVRETBO * 5/3) - (NINVRETBO - INVRETBO))))
             + (NINVRETBO - INVRETBO) * null(1 - abs(arr(INVRETBO * 5/3) - (NINVRETBO - INVRETBO))) ;

INVRETBTR = min(arr(INVRETBT * 5/3) , NINVRETBT - INVRETBT) * (1 - null(1 - abs(arr(INVRETBT * 5/3) - (NINVRETBT - INVRETBT))))
             + (NINVRETBT - INVRETBT) * null(1 - abs(arr(INVRETBT * 5/3) - (NINVRETBT - INVRETBT))) ;

INVRETBYR = min(arr(INVRETBY * 5/3) , NINVRETBY - INVRETBY) * (1 - null(1 - abs(arr(INVRETBY * 5/3) - (NINVRETBY - INVRETBY))))
             + (NINVRETBY - INVRETBY) * null(1 - abs(arr(INVRETBY * 5/3) - (NINVRETBY - INVRETBY))) ;

INVRETCDR = min(arr(INVRETCD * 33/17) , NINVRETCD - INVRETCD) * (1 - null(1 - abs(arr(INVRETCD * 33/17) - (NINVRETCD - INVRETCD))))
             + (NINVRETCD - INVRETCD) * null(1 - abs(arr(INVRETCD * 33/17) - (NINVRETCD - INVRETCD))) ;

INVRETBIR = min(arr(INVRETBI * 10/9) , NINVRETBI - INVRETBI) * (1 - null(1 - abs(arr(INVRETBI * 10/9) - (NINVRETBI - INVRETBI))))
             + (NINVRETBI - INVRETBI) * null(1 - abs(arr(INVRETBI * 10/9) - (NINVRETBI - INVRETBI))) ;

INVRETBNR = min(arr(INVRETBN * 10/9) , NINVRETBN - INVRETBN) * (1 - null(1 - abs(arr(INVRETBN * 10/9) - (NINVRETBN - INVRETBN))))
             + (NINVRETBN - INVRETBN) * null(1 - abs(arr(INVRETBN * 10/9) - (NINVRETBN - INVRETBN))) ;

INVRETBSR = min(arr(INVRETBS * 10/9) , NINVRETBS - INVRETBS) * (1 - null(1 - abs(arr(INVRETBS * 10/9) - (NINVRETBS - INVRETBS))))
             + (NINVRETBS - INVRETBS) * null(1 - abs(arr(INVRETBS * 10/9) - (NINVRETBS - INVRETBS))) ;

INVRETBXR = min(arr(INVRETBX * 10/9) , NINVRETBX - INVRETBX) * (1 - null(1 - abs(arr(INVRETBX * 10/9) - (NINVRETBX - INVRETBX))))
             + (NINVRETBX - INVRETBX) * null(1 - abs(arr(INVRETBX * 10/9) - (NINVRETBX - INVRETBX))) ;

INVRETCCR = min(arr(INVRETCC * 14/11) , NINVRETCC - INVRETCC) * (1 - null(1 - abs(arr(INVRETCC * 14/11) - (NINVRETCC - INVRETCC))))
             + (NINVRETCC - INVRETCC) * null(1 - abs(arr(INVRETCC * 14/11) - (NINVRETCC - INVRETCC))) ;

INVRETCTR = min(arr(INVRETCT * 33/17) , NINVRETCT - INVRETCT) * (1 - null(1 - abs(arr(INVRETCT * 33/17) - (NINVRETCT - INVRETCT))))
             + (NINVRETCT - INVRETCT) * null(1 - abs(arr(INVRETCT * 33/17) - (NINVRETCT - INVRETCT))) ;

INVRETCJR = min(arr(INVRETCJ * 5/3) , NINVRETCJ - INVRETCJ) * (1 - null(1 - abs(arr(INVRETCJ * 5/3) - (NINVRETCJ - INVRETCJ))))
             + (NINVRETCJ - INVRETCJ) * null(1 - abs(arr(INVRETCJ * 5/3) - (NINVRETCJ - INVRETCJ))) ;

INVRETCOR = min(arr(INVRETCO * 5/3) , NINVRETCO - INVRETCO) * (1 - null(1 - abs(arr(INVRETCO * 5/3) - (NINVRETCO - INVRETCO))))
             + (NINVRETCO - INVRETCO) * null(1 - abs(arr(INVRETCO * 5/3) - (NINVRETCO - INVRETCO))) ;

INVRETCSR = min(arr(INVRETCS * 14/11) , NINVRETCS - INVRETCS) * (1 - null(1 - abs(arr(INVRETCS * 14/11) - (NINVRETCS - INVRETCS))))
             + (NINVRETCS - INVRETCS) * null(1 - abs(arr(INVRETCS * 14/11) - (NINVRETCS - INVRETCS))) ;

INVRETCIR = min(arr(INVRETCI * 10/9) , NINVRETCI - INVRETCI) * (1 - null(1 - abs(arr(INVRETCI * 10/9) - (NINVRETCI - INVRETCI))))
             + (NINVRETCI - INVRETCI) * null(1 - abs(arr(INVRETCI * 10/9) - (NINVRETCI - INVRETCI))) ;

INVRETCNR = min(arr(INVRETCN * 10/9) , NINVRETCN - INVRETCN) * (1 - null(1 - abs(arr(INVRETCN * 10/9) - (NINVRETCN - INVRETCN))))
             + (NINVRETCN - INVRETCN) * null(1 - abs(arr(INVRETCN * 10/9) - (NINVRETCN - INVRETCN))) ;

INVRETDTR = min(arr(INVRETDT * 33/17) , NINVRETDT - INVRETDT) * (1 - null(1 - abs(arr(INVRETDT * 33/17) - (NINVRETDT - INVRETDT))))
            + (NINVRETDT - INVRETDT) * null(1 - abs(arr(INVRETDT * 33/17) - (NINVRETDT - INVRETDT))) ;

INVRETDJR = min(arr(INVRETDJ * 5/3) , NINVRETDJ - INVRETDJ) * (1 - null(1 - abs(arr(INVRETDJ * 5/3) - (NINVRETDJ - INVRETDJ))))
            + (NINVRETDJ - INVRETDJ) * null(1 - abs(arr(INVRETDJ * 5/3) - (NINVRETDJ - INVRETDJ))) ;

INVRETDOR = min(arr(INVRETDO * 5/3) , NINVRETDO - INVRETDO) * (1 - null(1 - abs(arr(INVRETDO * 5/3) - (NINVRETDO - INVRETDO))))
            + (NINVRETDO - INVRETDO) * null(1 - abs(arr(INVRETDO * 5/3) - (NINVRETDO - INVRETDO))) ;

INVRETDSR = min(arr(INVRETDS * 14/11) , NINVRETDS - INVRETDS) * (1 - null(1 - abs(arr(INVRETDS * 14/11) - (NINVRETDS - INVRETDS))))
            + (NINVRETDS - INVRETDS) * null(1 - abs(arr(INVRETDS * 14/11) - (NINVRETDS - INVRETDS))) ;

INVRETDIR = min(arr(INVRETDI * 10/9) , NINVRETDI - INVRETDI) * (1 - null(1 - abs(arr(INVRETDI * 10/9) - (NINVRETDI - INVRETDI))))
            + (NINVRETDI - INVRETDI) * null(1 - abs(arr(INVRETDI * 10/9) - (NINVRETDI - INVRETDI))) ;

INVRETDNR = min(arr(INVRETDN * 10/9) , NINVRETDN - INVRETDN) * (1 - null(1 - abs(arr(INVRETDN * 10/9) - (NINVRETDN - INVRETDN))))
            + (NINVRETDN - INVRETDN) * null(1 - abs(arr(INVRETDN * 10/9) - (NINVRETDN - INVRETDN))) ;

INVRETETR = min(arr(INVRETET * 33/17) , NINVRETET - INVRETET) * (1 - null(1 - abs(arr(INVRETET * 33/17) - (NINVRETET - INVRETET))))
            + (NINVRETET - INVRETET) * null(1 - abs(arr(INVRETET * 33/17) - (NINVRETET - INVRETET))) ;

INVRETEOR = min(arr(INVRETEO * 5/3) , NINVRETEO - INVRETEO) * (1 - null(1 - abs(arr(INVRETEO * 5/3) - (NINVRETEO - INVRETEO))))
            + (NINVRETEO - INVRETEO) * null(1 - abs(arr(INVRETEO * 5/3) - (NINVRETEO - INVRETEO))) ;

INVRETESR = min(arr(INVRETES * 14/11) , NINVRETES - INVRETES) * (1 - null(1 - abs(arr(INVRETES * 14/11) - (NINVRETES - INVRETES))))
            + (NINVRETES - INVRETES) * null(1 - abs(arr(INVRETES * 14/11) - (NINVRETES - INVRETES))) ;

INVRETENR = min(arr(INVRETEN * 10/9) , NINVRETEN - INVRETEN) * (1 - null(1 - abs(arr(INVRETEN * 10/9) - (NINVRETEN - INVRETEN))))
            + (NINVRETEN - INVRETEN) * null(1 - abs(arr(INVRETEN * 10/9) - (NINVRETEN - INVRETEN))) ;

INVRETFTR = min(arr(INVRETFT * 33/17) , NINVRETFT - INVRETFT) * (1 - null(1 - abs(arr(INVRETFT * 33/17) - (NINVRETFT - INVRETFT))))
            + (NINVRETFT - INVRETFT) * null(1 - abs(arr(INVRETFT * 33/17) - (NINVRETFT - INVRETFT))) ;

INVRETFOR = min(arr(INVRETFO * 5/3) , NINVRETFO - INVRETFO) * (1 - null(1 - abs(arr(INVRETFO * 5/3) - (NINVRETFO - INVRETFO))))
            + (NINVRETFO - INVRETFO) * null(1 - abs(arr(INVRETFO * 5/3) - (NINVRETFO - INVRETFO))) ;

INVRETFSR = min(arr(INVRETFS * 14/11) , NINVRETFS - INVRETFS) * (1 - null(1 - abs(arr(INVRETFS * 14/11) - (NINVRETFS - INVRETFS))))
            + (NINVRETFS - INVRETFS) * null(1 - abs(arr(INVRETFS * 14/11) - (NINVRETFS - INVRETFS))) ;

INVRETFNR = min(arr(INVRETFN * 10/9) , NINVRETFN - INVRETFN) * (1 - null(1 - abs(arr(INVRETFN * 10/9) - (NINVRETFN - INVRETFN))))
            + (NINVRETFN - INVRETFN) * null(1 - abs(arr(INVRETFN * 10/9) - (NINVRETFN - INVRETFN))) ;

INVRETGTR = min(arr(INVRETGT * 33/17) , NINVRETGT - INVRETGT) * (1 - null(1 - abs(arr(INVRETGT * 33/17) - (NINVRETGT - INVRETGT))))
            + (NINVRETGT - INVRETGT) * null(1 - abs(arr(INVRETGT * 33/17) - (NINVRETGT - INVRETGT))) ;

INVRETGSR = min(arr(INVRETGS * 14/11) , NINVRETGS - INVRETGS) * (1 - null(1 - abs(arr(INVRETGS * 14/11) - (NINVRETGS - INVRETGS))))
            + (NINVRETGS - INVRETGS) * null(1 - abs(arr(INVRETGS * 14/11) - (NINVRETGS - INVRETGS))) ;

regle 402100:
application : iliad ;


VARTMP1 = 0 ;

INVRETQB = NINVRETQB ; 

INVRETQC = NINVRETQC ; 

INVRETQT = NINVRETQT ; 

INVRETQL = min(NINVRETQL , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETQL ;

INVRETQM = min(NINVRETQM , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETQM ;

INVRETQD = min(NINVRETQD , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETQD ;

INVRETOB = min(NINVRETOB , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOB ;

INVRETOC = min(NINVRETOC , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOC ;

INVRETOI = min(NINVRETOI , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOI ;

INVRETOJ = min(NINVRETOJ , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOJ ;

INVRETOK = min(NINVRETOK , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOK ;

INVRETOM = min(NINVRETOM , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOM ;

INVRETON = min(NINVRETON , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETON ;

INVRETOP = min(NINVRETOP , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOP ;

INVRETOQ = min(NINVRETOQ , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOQ ;

INVRETOR = min(NINVRETOR , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOR ;

INVRETOT = min(NINVRETOT , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOT ;

INVRETOU = min(NINVRETOU , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOU ;

INVRETOV = min(NINVRETOV , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOV ;

INVRETOW = min(NINVRETOW , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOW ;

INVRETOD = min(NINVRETOD , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOD ;

INVRETOE = min(NINVRETOE , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOE ;

INVRETOF = min(NINVRETOF , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOF ;

INVRETOG = min(NINVRETOG , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOG ;

INVRETOX = min(NINVRETOX , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOX ;

INVRETOY = min(NINVRETOY , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOY ;

INVRETOZ = min(NINVRETOZ , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETOZ ;

INVRETUA = min(NINVRETUA , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUA ;

INVRETUB = min(NINVRETUB , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUB ;

INVRETUC = min(NINVRETUC , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUC ;

INVRETUD = min(NINVRETUD , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUD ;

INVRETUE = min(NINVRETUE , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUE ;

INVRETUF = min(NINVRETUF , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUF ;

INVRETUG = min(NINVRETUG , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUG ;

INVRETUH = min(NINVRETUH , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUH ;

INVRETUI = min(NINVRETUI , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUI ;

INVRETUJ = min(NINVRETUJ , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUJ ;

INVRETUK = min(NINVRETUK , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUK ;

INVRETUL = min(NINVRETUL , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUL ;

INVRETUM = min(NINVRETUM , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUM ;

INVRETUN = min(NINVRETUN , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUN ;

INVRETUO = min(NINVRETUO , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUO ;

INVRETUP = min(NINVRETUP , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUP ;

INVRETUQ = min(NINVRETUQ , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUQ ;

INVRETUR = min(NINVRETUR , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUR ;

INVRETUS = min(NINVRETUS , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUS ;

INVRETUT = min(NINVRETUT , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUT ;

INVRETUU = min(NINVRETUU , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETUU ;

INVRETVA = min(NINVRETVA , max(0 , PLAF_INVDOM -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVA ;

INVRETVB = min(NINVRETVB , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVB ;

INVRETVC = min(NINVRETVC , max(0 , PLAF_INVDOM3 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVC ;

INVRETVD = min(NINVRETVD , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVD ;

INVRETVE = min(NINVRETVE , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVE ;

INVRETVF = min(NINVRETVF , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVF ;

INVRETVG = min(NINVRETVG , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVG ;

INVRETVH = min(NINVRETVH , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVH ;

INVRETVI = min(NINVRETVI , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + INVRETVI ;

INVRETVJ = min(NINVRETVJ , max(0 , PLAF_INVDOM4 -INVRETSOC-INVRETENT-VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = 0 ;

INVRETLOG = INVRETQL + INVRETQM + INVRETQD + INVRETOB + INVRETOC + INVRETOI + INVRETOJ + INVRETOK + INVRETOM + INVRETON + INVRETOP + INVRETOQ
            + INVRETOR + INVRETOT + INVRETOU + INVRETOV + INVRETOW + INVRETOD + INVRETOE + INVRETOF + INVRETOG + INVRETOX + INVRETOY + INVRETOZ 
            + INVRETUA + INVRETUB + INVRETUC + INVRETUD + INVRETUE + INVRETUF + INVRETUG + INVRETUH + INVRETUI + INVRETUJ + INVRETUK + INVRETUL
            + INVRETUM + INVRETUN + INVRETUO + INVRETUP + INVRETUQ + INVRETUR + INVRETUS + INVRETUT + INVRETUU + INVRETVA + INVRETVB + INVRETVC
	    + INVRETVD + INVRETVE + INVRETVF + INVRETVG + INVRETVH + INVRETVI + INVRETVJ ;

regle 402060:
application : iliad ;


RLOGDOM = min(ALOGDOM , RRI1) * (1 - V_CNR) ;
RLOGDOM_1 = min(ALOGDOM_1 , RRI1) * (1 - V_CNR);

RINVDOMTOMLG = RLOGDOM ;

regle 402110:
application : iliad ;


RRISUP = RRI1 - RLOGDOM - RCOMP - RRETU - RDONS - CRDIE - RLOCNPRO - RDUFREP - RPINELTOT - RNORMTOT - RNOUV 
              - RPENTOT - RRSOFON - RFOR - RTOURREP - RTOUREPA - RREHAB - RRESTREP - RRESTIMO1 - RCELTOT ; 


RDOMSOC1 = min(ADOMSOC1 , RRISUP) * (1 - V_CNR) ;
RDOMSOC1_1 = min(ADOMSOC1_1 , RRISUP) * (1 - V_CNR) ;

RLOGSOC = min(ALOGSOC , max(0 , RRISUP - ADOMSOC1)) * (1 - V_CNR) ;
RLOGSOC_1 = min(ALOGSOC_1 , max(0 , RRISUP - ADOMSOC1)) * (1 - V_CNR) ;

RLOGSOCTEO = (arr((((INVRETXQ + INVRETXQR) * (1 - INDPLAF) + (INVRETXQA + INVRETXQRA) * INDPLAF)
	           + ((INVRETXL + INVRETXLR) * (1 - INDPLAF) + (INVRETXLA + INVRETXLRA) * INDPLAF) 
                   + ((INVRETXF + INVRETXFR) * (1 - INDPLAF) + (INVRETXFA + INVRETXFRA) * INDPLAF)) * TX65/100)
              + arr((((INVRETXR + INVRETXRR) * (1 - INDPLAF) + (INVRETXRA + INVRETXRRA) * INDPLAF)
	           + ((INVRETXM + INVRETXMR) * (1 - INDPLAF) + (INVRETXMA + INVRETXMRA) * INDPLAF) 
                   + ((INVRETXG + INVRETXGR) * (1 - INDPLAF) + (INVRETXGA + INVRETXGRA) * INDPLAF)) * TX65/100)
              + arr((((INVRETXS + INVRETXSR) * (1 - INDPLAF) + (INVRETXSA + INVRETXSRA) * INDPLAF)
	           + ((INVRETXN + INVRETXNR) * (1 - INDPLAF) + (INVRETXNA + INVRETXNRA) * INDPLAF) 
                   + ((INVRETXH + INVRETXHR) * (1 - INDPLAF) + (INVRETXHA + INVRETXHRA) * INDPLAF)) * TX65/100)
              + arr((((INVRETXT + INVRETXTR) * (1 - INDPLAF) + (INVRETXTA + INVRETXTRA) * INDPLAF)
	           + ((INVRETXO + INVRETXOR) * (1 - INDPLAF) + (INVRETXOA + INVRETXORA) * INDPLAF) 
                   + ((INVRETXI + INVRETXIR) * (1 - INDPLAF) + (INVRETXIA + INVRETXIRA) * INDPLAF)) * TX65/100)
              + arr((((INVRETXU + INVRETXUR) * (1 - INDPLAF) + (INVRETXUA + INVRETXURA) * INDPLAF)
	           + ((INVRETXP + INVRETXPR) * (1 - INDPLAF) + (INVRETXPA + INVRETXPRA) * INDPLAF) 
                   + ((INVRETXK + INVRETXKR) * (1 - INDPLAF) + (INVRETXKA + INVRETXKRA) * INDPLAF)) * TX70/100)
              + arr((((INVRETYB + INVRETYBR) * (1 - INDPLAF) + (INVRETYBA + INVRETYBRA) * INDPLAF)) * TX70/100)
              + arr((((INVRETYA + INVRETYAR) * (1 - INDPLAF) + (INVRETYAA + INVRETYARA) * INDPLAF)) * TX65/100)
              + arr((((INVRETYD + INVRETYDR) * (1 - INDPLAF) + (INVRETYDA + INVRETYDRA) * INDPLAF)) * TX70/100)
              + arr((((INVRETYC + INVRETYCR) * (1 - INDPLAF) + (INVRETYCA + INVRETYCRA) * INDPLAF)) * TX65/100)
              + arr((((INVRETYE + INVRETYER) * (1 - INDPLAF) + (INVRETYEA + INVRETYERA) * INDPLAF)) * TX70/100)
             ) * (1 - V_CNR) ; 

regle 402120:
application : iliad ;


RCOLENT = min(ACOLENT , max(0 , RRISUP - ALOGSOC - ADOMSOC1)) * (1 - V_CNR) ;
RCOLENT_1 = min(ACOLENT_1 , max(0 , RRISUP - ALOGSOC_1 - ADOMSOC1_1)) * (1 - V_CNR) ;

RLOCENT = min(ALOCENT , max(0 , RRISUP - ALOGSOC - ADOMSOC1 - ACOLENT)) * (1 - V_CNR) ;
RLOCENT_1 = min(ALOCENT_1 , max(0 , RRISUP - ALOGSOC_1 - ADOMSOC1_1 - ACOLENT_1)) * (1 - V_CNR) ;

RIDOMENT = RLOCENT ;

RCOLENTTEO = (
               arr(((INVRETBI + INVRETBIR) * (1 - INDPLAF) + (INVRETBIA + INVRETBIRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETBJ + INVRETBJR) * (1 - INDPLAF) + (INVRETBJA + INVRETBJRA) * INDPLAF) * TX625/100)

              + arr(((INVRETBN + INVRETBNR) * (1 - INDPLAF) + (INVRETBNA + INVRETBNRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETBO + INVRETBOR) * (1 - INDPLAF) + (INVRETBOA + INVRETBORA) * INDPLAF) * TX625/100)

              + arr(((INVRETDI + INVRETDIR) * (1 - INDPLAF) + (INVRETDIA + INVRETDIRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETDJ + INVRETDJR) * (1 - INDPLAF) + (INVRETDJA + INVRETDJRA) * INDPLAF) * TX625/100)
              + arr(((INVRETCI + INVRETCIR) * (1 - INDPLAF) + (INVRETCIA + INVRETCIRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETCJ + INVRETCJR) * (1 - INDPLAF) + (INVRETCJA + INVRETCJRA) * INDPLAF) * TX625/100)
              + arr(((INVRETBS + INVRETBSR) * (1 - INDPLAF) + (INVRETBSA + INVRETBSRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETBT + INVRETBTR) * (1 - INDPLAF) + (INVRETBTA + INVRETBTRA) * INDPLAF) * TX625/100)

              + arr(((INVRETDN + INVRETDNR) * (1 - INDPLAF) + (INVRETDNA + INVRETDNRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETDO + INVRETDOR) * (1 - INDPLAF) + (INVRETDOA + INVRETDORA) * INDPLAF) * TX625/100)
              + arr(((INVRETDS + INVRETDSR) * (1 - INDPLAF) + (INVRETDSA + INVRETDSRA) * INDPLAF) * TX56/100)
              + arr(((INVRETDT + INVRETDTR) * (1 - INDPLAF) + (INVRETDTA + INVRETDTRA) * INDPLAF) * TX66/100)
              + arr(((INVRETEN + INVRETENR) * (1 - INDPLAF) + (INVRETENA + INVRETENRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETEO + INVRETEOR) * (1 - INDPLAF) + (INVRETEOA + INVRETEORA) * INDPLAF) * TX625/100)
              + arr(((INVRETES + INVRETESR) * (1 - INDPLAF) + (INVRETESA + INVRETESRA) * INDPLAF) * TX56/100)
              + arr(((INVRETET + INVRETETR) * (1 - INDPLAF) + (INVRETETA + INVRETETRA) * INDPLAF) * TX66/100)
              + arr(((INVRETCN + INVRETCNR) * (1 - INDPLAF) + (INVRETCNA + INVRETCNRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETCO + INVRETCOR) * (1 - INDPLAF) + (INVRETCOA + INVRETCORA) * INDPLAF) * TX625/100)
              + arr(((INVRETCS + INVRETCSR) * (1 - INDPLAF) + (INVRETCSA + INVRETCSRA) * INDPLAF) * TX56/100)
              + arr(((INVRETCT + INVRETCTR) * (1 - INDPLAF) + (INVRETCTA + INVRETCTRA) * INDPLAF) * TX66/100)
              + arr(((INVRETBX + INVRETBXR) * (1 - INDPLAF) + (INVRETBXA + INVRETBXRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETBY + INVRETBYR) * (1 - INDPLAF) + (INVRETBYA + INVRETBYRA) * INDPLAF) * TX625/100)
              + arr(((INVRETCC + INVRETCCR) * (1 - INDPLAF) + (INVRETCCA + INVRETCCRA) * INDPLAF) * TX56/100)
              + arr(((INVRETCD + INVRETCDR) * (1 - INDPLAF) + (INVRETCDA + INVRETCDRA) * INDPLAF) * TX66/100)

              + arr(((INVRETFN + INVRETFNR) * (1 - INDPLAF) + (INVRETFNA + INVRETFNRA) * INDPLAF) * TX5263/100)
              + arr(((INVRETFO + INVRETFOR) * (1 - INDPLAF) + (INVRETFOA + INVRETFORA) * INDPLAF) * TX625/100)
              + arr(((INVRETFS + INVRETFSR) * (1 - INDPLAF) + (INVRETFSA + INVRETFSRA) * INDPLAF) * TX56/100)
              + arr(((INVRETFT + INVRETFTR) * (1 - INDPLAF) + (INVRETFTA + INVRETFTRA) * INDPLAF) * TX66/100)

              + arr(((INVRETGS + INVRETGSR) * (1 - INDPLAF) + (INVRETGSA + INVRETGSRA) * INDPLAF) * TX56/100)
              + arr(((INVRETGT + INVRETGTR) * (1 - INDPLAF) + (INVRETGTA + INVRETGTRA) * INDPLAF) * TX66/100)
              ) * (1 - V_CNR) ;

regle 402130:
application : iliad ;


RRIREP = RRI1 - DLOGDOM - RCOMP - RRETU - RDONS - CRDIE - RLOCNPRO - RDUFREP - RPINELTOT - RNORMTOT - RNOUV 
              - RPENTOT - RRSOFON - RFOR - RTOURREP - RTOUREPA - RRREHAP - RRESTREP - RRESTIMO1 - RCELTOT ;

VARTMP1 = VARTMP1 + CODHXK + CODHXF + CODHXG + CODHXH + CODHXI ;

REPXP = max(0 , CODHXP - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXP ;

REPXL = max(0 , CODHXL - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXL ;

REPXM = max(0 , CODHXM - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXM ;

REPXN = max(0 , CODHXN - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXN ;

REPXO = max(0 , CODHXO - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXO ;

REPDOMSOC4 = REPXP + REPXL + REPXM + REPXN + REPXO ;


REPXU = max(0 , CODHXU - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXU ;

REPXQ = max(0 , CODHXQ - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXQ ;

REPXR = max(0 , CODHXR - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXR ;

REPXS = max(0 , CODHXS - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXS ;

REPXT = max(0 , CODHXT - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHXT ;

REPDOMSOC3 = REPXU + REPXQ + REPXR + REPXS + REPXT ;


REPYB = max(0 , CODHYB - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHYB ;

REPYA = max(0 , CODHYA - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHYA ;

REPDOMSOC2 = REPYB + REPYA ;


REPYD = max(0 , CODHYD - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHYD ;

REPYC = max(0 , CODHYC - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHYC ;

REPDOMSOC1 = REPYD + REPYC ;


REPYE = max(0 , CODHYE - max(0 , RRIREP - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHYE ;

REPDOMSOC = REPYE ;

REPSOC = CODHXF + CODHXG + CODHXH + CODHXI + CODHXK + CODHXL + CODHXM + CODHXN + CODHXO + CODHXP 
         + CODHXU + CODHXQ + CODHXR + CODHXS + CODHXT + CODHYB + CODHYA + CODHYD + CODHYC + CODHYE ;


REPENT5 = CODHBJ + CODHBO + CODHBT + CODHBY + CODHCD + CODHBI + CODHBN + CODHBS + CODHBX + CODHCC
          + CODHBK + CODHBP + CODHBU + CODHBZ + CODHCE + CODHBM + CODHBR + CODHBW + CODHCB + CODHCG ;


REPHCT = max(0 , CODHCT - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCT ;

REPCJ = max(0 , CODHCJ - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCJ ;

REPCO = max(0 , CODHCO - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCO ;

REPCS = max(0 , CODHCS - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCS ;

REPCI = max(0 , CODHCI - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCI ;

REPCN = max(0 , CODHCN - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCN ;

REPCK = max(0 , CODHCK - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCK ;

REPCP = max(0 , CODHCP - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCP ;

REPCU = max(0 , CODHCU - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCU ;

REPCM = max(0 , CODHCM - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCM ;

REPCR = max(0 , CODHCR - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCR ;

REPCW = max(0 , CODHCW - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHCW ;

REPDOMENTR4 = REPHCT + REPCJ + REPCO + REPCS + REPCI + REPCN + REPCK + REPCP + REPCU + REPCM + REPCR + REPCW ;


REPDT = max(0 , CODHDT - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDT ; 

REPDJ = max(0 , CODHDJ - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDJ ;

REPDO = max(0 , CODHDO - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDO ;

REPDS = max(0 , CODHDS - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDS ;

REPDI = max(0 , CODHDI - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDI ;

REPDN = max(0 , CODHDN - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDN ;

REPDK = max(0 , CODHDK - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDK ;

REPDP = max(0 , CODHDP - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDP ;

REPDU = max(0 , CODHDU - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDU ;

REPDM = max(0 , CODHDM - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDM ;

REPDR = max(0 , CODHDR - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDR ;

REPDW = max(0 , CODHDW - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHDW ;

REPDOMENTR3 = REPDT + REPDJ + REPDO + REPDS + REPDI + REPDN + REPDK + REPDP + REPDU + REPDM + REPDR + REPDW ;


REPET = max(0 , CODHET - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHET ; 

REPEO = max(0 , CODHEO - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHEO ;

REPES = max(0 , CODHES - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHES ;

REPEN = max(0 , CODHEN - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHEN ;

REPEP = max(0 , CODHEP - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHEP ;

REPEU = max(0 , CODHEU - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHEU ;

REPER = max(0 , CODHER - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHER ;

REPEW = max(0 , CODHEW - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHEW ;

REPDOMENTR2 = REPET + REPEO + REPES + REPEN + REPEP + REPEU + REPER + REPEW ;


REPFT = max(0 , CODHFT - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFT ; 

REPFO = max(0 , CODHFO - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFO ;

REPFS = max(0 , CODHFS - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFS ;

REPFN = max(0 , CODHFN - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFN ;

REPFP = max(0 , CODHFP - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFP ;

REPFU = max(0 , CODHFU - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFU ;

REPFR = max(0 , CODHFR - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFR ;

REPFW = max(0 , CODHFW - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHFW ;

REPDOMENTR1 = REPFT + REPFO + REPFS + REPFN + REPFP + REPFU + REPFR + REPFW ;


REPGT = max(0 , CODHGT - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHGT ; 

REPGS = max(0 , CODHGS - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHGS ; 

REPGU = max(0 , CODHGU - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + CODHGU ; 

REPGW = max(0 , CODHGW - max(0 , RRIREP - REPENT5 - VARTMP1)) * (1 - V_CNR) ;
VARTMP1 = 0 ;

REPDOMENTR = REPGT + REPGS + REPGU + REPGW ;




RIDEFRI = ((1-COD9ZA+0) * positif(positif(RED_1 - RED1731+0)+ positif(RDONIFI_1+RDONIFI2_1 - RDONIFI11731 - RDONIFI21731)) * positif(1-PREM8_11)
                 + positif(positif(RED_1 - RED1731+0)+ positif(RDONIFI_1+RDONIFI2_1 - RDONIFI11731 - RDONIFI21731)) * positif(PREM8_11)) * null(V_IND_TRAIT - 5);
       
regle 4666:
application : iliad ;

TOTRI3WG = RAPRESSE + RAFORET + RFIPDOMCOM + RFIPCORSE + RRS + RRCN + RFIP + RENOV + ACOMP
           + ADUFREP + APIREP + ANORMREP + RPINABCD + RPINRRS + RNORABCD
	   + CELRREDLG + CELRREDLK + CELRREDLH 
	   + CELRREDLL + CELRREDLI + CELRREDLO 
	   + CELRREDLJ + CELRREDLP + CELRREDLQ + CELRREDLR + CELRREDLU 
	   + CELRREDLV + COD7LA + COD7LB + COD7LC + COD7LY
	   + COD7MS + COD7MT + COD7MU + COD7MV
	   + CELREPGU + CELREPGS + CELREPGL + CELREPGJ 
	   + CELREPYH + CELREPYF + CELREPYD + CELREPYB + CELREPYP + CELREPYO + CELREPYN + CELREPYM 
	   + CELREPYW + CELREPYV + CELREPYU + CELREPYT 
	   + CELREPWT + CELREPWU + CELREPWV + CELREPWW + CELREPWX + CELREPWY + CELREPWZ
           + COD7RW + COD7RV + COD7RU + COD7RT
	   + RCEL7RI + RCEL7RJ + RCEL7RK + RCEL7RL + RCEL7RM + RCEL7RN + RCEL7RO + RCEL7RP + RCEL7RQ
	   + RCEL7XH + RCEL7XI + RCEL7XJ + RCEL7XK + RCEL7JE + RCEL7JF + RCEL7JG + RCEL7JH + RCEL7JI + RCEL7JJ + RCEL7JK + RCEL7JL
	   + arr(DTOURREP * TX_REDIL25 / 100) + arr(DTOUREPA * TX_REDIL20 / 100)
	   + RAH + RAALIM + RSNNCL + RSNNCC + RSNNCQ + RSNNCR
	   + RSNNCV + RSNNCX + RSNNCF + RSN + APLAFREPME4 + APENTCY + APENTDY + APENTEY + APENTFY + APENTGY
	   + DILMNP1 + DILMNP3
	   + RETCODIM + RETCODIN + RETCODIJ + RETCODID 
	   + RETCODJT + RETCODOU + RETCODOV + RETCODOW + RETCODOX + RETCODOY + RETCODPZ + RETCODMZ + RSOCREP + RETRESTIMO + RONS 
	   + CRCFA + RETUD + RFCPI + RPRESCOMP + arr(ACOTFOR_R * TX76/100) + RFOREST
	   + RAGRI + TOTINVDOM ;

regle 4700:
application : iliad ;
RED3WG =  max( min(TOTRI3WG , IDOM13-DEC13) , 0 ) ;
regle 4800:
application : iliad ;

TOTINVDOM = (INVRETXF * (1 - INDPLAF) + INVRETXFA * INDPLAF)
           +(INVRETXG * (1 - INDPLAF) + INVRETXGA * INDPLAF)
           +(INVRETXH * (1 - INDPLAF) + INVRETXHA * INDPLAF)
           +(INVRETXI * (1 - INDPLAF) + INVRETXIA * INDPLAF)
           +(INVRETXK * (1 - INDPLAF) + INVRETXKA * INDPLAF)
           +(INVRETXFR * (1 - INDPLAF) + INVRETXFRA * INDPLAF)
           +(INVRETXGR * (1 - INDPLAF) + INVRETXGRA * INDPLAF)
           +(INVRETXHR * (1 - INDPLAF) + INVRETXHRA * INDPLAF)
           +(INVRETXIR * (1 - INDPLAF) + INVRETXIRA * INDPLAF)
           +(INVRETXKR * (1 - INDPLAF) + INVRETXKRA * INDPLAF)
           +(INVRETXL * (1 - INDPLAF) + INVRETXLA * INDPLAF)
           +(INVRETXM * (1 - INDPLAF) + INVRETXMA * INDPLAF)
           +(INVRETXN * (1 - INDPLAF) + INVRETXNA * INDPLAF)
           +(INVRETXO * (1 - INDPLAF) + INVRETXOA * INDPLAF)
           +(INVRETXP * (1 - INDPLAF) + INVRETXPA * INDPLAF)
           +(INVRETXLR * (1 - INDPLAF) + INVRETXLRA * INDPLAF)
           +(INVRETXMR * (1 - INDPLAF) + INVRETXMRA * INDPLAF)
           +(INVRETXNR * (1 - INDPLAF) + INVRETXNRA * INDPLAF)
           +(INVRETXOR * (1 - INDPLAF) + INVRETXORA * INDPLAF)
           +(INVRETXPR * (1 - INDPLAF) + INVRETXPRA * INDPLAF)
           +(INVRETCT * (1 - INDPLAF) + INVRETCTA * INDPLAF)
           +(INVRETCJ * (1 - INDPLAF) + INVRETCJA * INDPLAF)
           +(INVRETCO * (1 - INDPLAF) + INVRETCOA * INDPLAF)
           +(INVRETCS * (1 - INDPLAF) + INVRETCSA * INDPLAF)
           +(INVRETCI * (1 - INDPLAF) + INVRETCIA * INDPLAF)
           +(INVRETCN * (1 - INDPLAF) + INVRETCNA * INDPLAF)
           +(INVRETCK * (1 - INDPLAF) + INVRETCKA * INDPLAF)
           +(INVRETCP * (1 - INDPLAF) + INVRETCPA * INDPLAF)
           +(INVRETCU * (1 - INDPLAF) + INVRETCUA * INDPLAF)
           +(INVRETCM * (1 - INDPLAF) + INVRETCMA * INDPLAF)
           +(INVRETCR * (1 - INDPLAF) + INVRETCRA * INDPLAF)
           +(INVRETCW * (1 - INDPLAF) + INVRETCWA * INDPLAF)
           +(INVRETCTR * (1 - INDPLAF) + INVRETCTRA * INDPLAF)
           +(INVRETCJR * (1 - INDPLAF) + INVRETCJRA * INDPLAF)
           +(INVRETCOR * (1 - INDPLAF) + INVRETCORA * INDPLAF)
           +(INVRETCSR * (1 - INDPLAF) + INVRETCSRA * INDPLAF)
           +(INVRETCIR * (1 - INDPLAF) + INVRETCIRA * INDPLAF)
           +(INVRETCNR * (1 - INDPLAF) + INVRETCNRA * INDPLAF)
           +INVLOG2008 +INVLGDEB2009 +INVLGDEB +INVOMLOGOA
           +INVOMLOGOH +INVOMLOGOL +INVOMLOGOO +INVOMLOGOS
           +(INVRETQL * (1 - INDPLAF) + INVRETQLA * INDPLAF)
           +(INVRETQM * (1 - INDPLAF) + INVRETQMA * INDPLAF)
           +(INVRETQD * (1 - INDPLAF) + INVRETQDA * INDPLAF)
           +(INVRETOB * (1 - INDPLAF) + INVRETOBA * INDPLAF)
           +(INVRETOC * (1 - INDPLAF) + INVRETOCA * INDPLAF)
           +(INVRETOI * (1 - INDPLAF) + INVRETOIA * INDPLAF)
           +(INVRETOJ * (1 - INDPLAF) + INVRETOJA * INDPLAF)
           +(INVRETOK * (1 - INDPLAF) + INVRETOKA * INDPLAF)
           +(INVRETOM * (1 - INDPLAF) + INVRETOMA * INDPLAF)
           +(INVRETON * (1 - INDPLAF) + INVRETONA * INDPLAF)
           +(INVRETOP * (1 - INDPLAF) + INVRETOPA * INDPLAF)
           +(INVRETOQ * (1 - INDPLAF) + INVRETOQA * INDPLAF)
           +(INVRETOR * (1 - INDPLAF) + INVRETORA * INDPLAF)
           +(INVRETOT * (1 - INDPLAF) + INVRETOTA * INDPLAF)
           +(INVRETOU * (1 - INDPLAF) + INVRETOUA * INDPLAF)
           +(INVRETOV * (1 - INDPLAF) + INVRETOVA * INDPLAF)
           +(INVRETOW * (1 - INDPLAF) + INVRETOWA * INDPLAF)
           +(INVRETOD * (1 - INDPLAF) + INVRETODA * INDPLAF)
           +(INVRETOE * (1 - INDPLAF) + INVRETOEA * INDPLAF)
           +(INVRETOF * (1 - INDPLAF) + INVRETOFA * INDPLAF)
           +(INVRETOG * (1 - INDPLAF) + INVRETOGA * INDPLAF)
           +(INVRETOX * (1 - INDPLAF) + INVRETOXA * INDPLAF)
           +(INVRETOY * (1 - INDPLAF) + INVRETOYA * INDPLAF)
           +(INVRETOZ * (1 - INDPLAF) + INVRETOZA * INDPLAF)
           +(INVRETUA * (1 - INDPLAF) + INVRETUAA * INDPLAF)
           +(INVRETUB * (1 - INDPLAF) + INVRETUBA * INDPLAF)
           +(INVRETUC * (1 - INDPLAF) + INVRETUCA * INDPLAF)
           +(INVRETUD * (1 - INDPLAF) + INVRETUDA * INDPLAF)
           +(INVRETUE * (1 - INDPLAF) + INVRETUEA * INDPLAF)
           +(INVRETUF * (1 - INDPLAF) + INVRETUFA * INDPLAF)
           +(INVRETUG * (1 - INDPLAF) + INVRETUGA * INDPLAF)
           +(INVRETUH * (1 - INDPLAF) + INVRETUHA * INDPLAF)
           +(INVRETUI * (1 - INDPLAF) + INVRETUIA * INDPLAF)
           +(INVRETUJ * (1 - INDPLAF) + INVRETUJA * INDPLAF)
           +(INVRETUK * (1 - INDPLAF) + INVRETUKA * INDPLAF)
           +(INVRETUL * (1 - INDPLAF) + INVRETULA * INDPLAF)
           +(INVRETUM * (1 - INDPLAF) + INVRETUMA * INDPLAF)
           +(INVRETUN * (1 - INDPLAF) + INVRETUNA * INDPLAF)
           +(INVRETUO * (1 - INDPLAF) + INVRETUOA * INDPLAF)
           +(INVRETUP * (1 - INDPLAF) + INVRETUPA * INDPLAF)
           +(INVRETUQ * (1 - INDPLAF) + INVRETUQA * INDPLAF)
           +(INVRETUR * (1 - INDPLAF) + INVRETURA * INDPLAF)
           +(INVRETUS * (1 - INDPLAF) + INVRETUSA * INDPLAF)
           +(INVRETUT * (1 - INDPLAF) + INVRETUTA * INDPLAF)
           +(INVRETUU * (1 - INDPLAF) + INVRETUUA * INDPLAF)
           +(INVRETVA * (1 - INDPLAF) + INVRETVAA * INDPLAF)
           +(INVRETVB * (1 - INDPLAF) + INVRETVBA * INDPLAF)
           +(INVRETVC * (1 - INDPLAF) + INVRETVCA * INDPLAF)
           +(INVRETVD * (1 - INDPLAF) + INVRETVDA * INDPLAF)
           +(INVRETVE * (1 - INDPLAF) + INVRETVEA * INDPLAF)
           +(INVRETVF * (1 - INDPLAF) + INVRETVFA * INDPLAF)
           +(INVRETVG * (1 - INDPLAF) + INVRETVGA * INDPLAF)
           +(INVRETVH * (1 - INDPLAF) + INVRETVHA * INDPLAF)
           +(INVRETVI * (1 - INDPLAF) + INVRETVIA * INDPLAF)
           +(INVRETVJ * (1 - INDPLAF) + INVRETVJA * INDPLAF)
           +(INVRETBJ * (1 - INDPLAF) + INVRETBJA * INDPLAF)
           +(INVRETBO * (1 - INDPLAF) + INVRETBOA * INDPLAF)
           +(INVRETBT * (1 - INDPLAF) + INVRETBTA * INDPLAF)
           +(INVRETBY * (1 - INDPLAF) + INVRETBYA * INDPLAF)
           +(INVRETCD * (1 - INDPLAF) + INVRETCDA * INDPLAF)
           +(INVRETBI * (1 - INDPLAF) + INVRETBIA * INDPLAF)
           +(INVRETBN * (1 - INDPLAF) + INVRETBNA * INDPLAF)
           +(INVRETBS * (1 - INDPLAF) + INVRETBSA * INDPLAF)
           +(INVRETBX * (1 - INDPLAF) + INVRETBXA * INDPLAF)
           +(INVRETCC * (1 - INDPLAF) + INVRETCCA * INDPLAF)
           +(INVRETBK * (1 - INDPLAF) + INVRETBKA * INDPLAF)
           +(INVRETBP * (1 - INDPLAF) + INVRETBPA * INDPLAF)
           +(INVRETBU * (1 - INDPLAF) + INVRETBUA * INDPLAF)
           +(INVRETBZ * (1 - INDPLAF) + INVRETBZA * INDPLAF)
           +(INVRETCE * (1 - INDPLAF) + INVRETCEA * INDPLAF)
           +(INVRETBM * (1 - INDPLAF) + INVRETBMA * INDPLAF)
           +(INVRETBR * (1 - INDPLAF) + INVRETBRA * INDPLAF)
           +(INVRETBW * (1 - INDPLAF) + INVRETBWA * INDPLAF)
           +(INVRETCB * (1 - INDPLAF) + INVRETCBA * INDPLAF)
           +(INVRETCG * (1 - INDPLAF) + INVRETCGA * INDPLAF)
           +(INVRETBJR * (1 - INDPLAF) + INVRETBJRA * INDPLAF)
           +(INVRETBOR * (1 - INDPLAF) + INVRETBORA * INDPLAF)
           +(INVRETBTR * (1 - INDPLAF) + INVRETBTRA * INDPLAF)
           +(INVRETBYR * (1 - INDPLAF) + INVRETBYRA * INDPLAF)
           +(INVRETCDR * (1 - INDPLAF) + INVRETCDRA * INDPLAF)
           +(INVRETBIR * (1 - INDPLAF) + INVRETBIRA * INDPLAF)
           +(INVRETBNR * (1 - INDPLAF) + INVRETBNRA * INDPLAF)
           +(INVRETBSR * (1 - INDPLAF) + INVRETBSRA * INDPLAF)
           +(INVRETBXR * (1 - INDPLAF) + INVRETBXRA * INDPLAF)
           +(INVRETCCR * (1 - INDPLAF) + INVRETCCRA * INDPLAF);

regle 402160 :
application : iliad ;

DREHAB = COD7XX ;

AREHAB_1 = DREHAB * (1 - V_CNR) ;
AREHAB = positif(null(V_IND_TRAIT-4)+COD9ZA) * (AREHAB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
         + (max(0,min(AREHAB_1,AREHAB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;

RRREHAP = arr(AREHAB_1 * TX20/100) ;

regle 402161 :
application : iliad ;
RREHAB_1= max(min( RRREHAP , RRI1-RLOGDOM-RCOMP-RRETU-RDONS-CRDIE-RDUFREP-RPINELTOT-RNORMTOT-RNOUV-RPENTOT-RRSOFON-RFOR-RTOURREP-RTOUREPA) , 0) ;
RREHAB =positif(null(V_IND_TRAIT-4)+COD9ZA) * (RREHAB_1) * (1-positif(null(8-CMAJ)+null(11-CMAJ)+null(34-CMAJ)))
          + (max(0,min(RREHAB_1,RREHAB1731))*positif(1-COD9ZA)*(1-positif(PREM8_11))) * null(V_IND_TRAIT-5)+0;


