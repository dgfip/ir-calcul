#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2021]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2021 
#au titre des revenus perçus en 2020. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
                                                                         #####
  ####   #    #    ##    #####      #     #####  #####   ######         #     #
 #    #  #    #   #  #   #    #     #       #    #    #  #                    #
 #       ######  #    #  #    #     #       #    #    #  #####           #####
 #       #    #  ######  #####      #       #    #####   #                    #
 #    #  #    #  #    #  #          #       #    #   #   #              #     #
  ####   #    #  #    #  #          #       #    #    #  ###### #######  #####
 #
 #
 #
 #
 #
 #
 #
 #                       CALCUL DE L'IMPOT NET
 #
 #
 #
 #
 #
 #
regle 301000:
application : bareme , iliad ;

IRN = min(0 , IAN + AVFISCOPTER - IRE) + max(0 , IAN + AVFISCOPTER - IRE) * positif( IAMD1 + 1 - SEUIL_61) ;


regle 301005:
application : bareme , iliad ;
IRNAF = min( 0, IAN - IRE) + max( 0, IAN - IRE) * positif( IAMD1AF + 1 - SEUIL_61) ;

regle 301010:
application : bareme , iliad ;


IAR = min( 0, IAN + AVFISCOPTER - IRE) + max( 0, IAN + AVFISCOPTER - IRE) ;

regle 301015:
application : bareme , iliad ;

IARAF = min(0 , IANAF - IREAF) + max(0 , IANAF - IREAF) + NEGIANAF ;

regle 301027:
application : iliad ;
BLOY7LS = COD7LS * (1 - V_CNR) ;
LOY7LS = arr(COD7LS * TX50/100) * (1-V_CNR);

LOY8LA = COD8LA * (1 - V_CNR) ;

regle 301020:
application : iliad ;

CREREVET = min(arr((BPTP3 + BPTPD + BPTPG) * TX128/100),arr(CIIMPPRO * TX128/100 ))
	   + min(arr(BPTP19 * TX19/100),arr(CIIMPPRO2 * TX19/100 ))
	   + min (arr(RCMIMPTR * TX075/100),arr(COD8XX * TX075/100)) 
	   + min (arr(BPTP10 * TX10/100),arr(COD8XV * TX10/100)) 
	   ;

CIIMPPROTOT = CIIMPPRO + CIIMPPRO2 + COD8XX + COD8XX + COD8XV;

regle 301030:
application : iliad ;

ICI8XFH = min(arr(BPTP18 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));
ICIGLO = min(arr(BPTP18 * TX18/100),arr(COD8XF * TX18/100 ))
      + min(arr(BPTP4I * TX30/100),arr(COD8XG * TX30/100 ))
      + min(arr(BPTP40 * TX41/100),arr(COD8XH * TX41/100 ));

CIGLOTOT = COD8XF + COD8XG + COD8XH; 
regle 301032:
application : iliad ;

CI8XFH = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET , ICI8XFH)) ;

CIGLO = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET , ICIGLO)) ;

regle 301035:
application : iliad ;

CI8XFHAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF , ICI8XFH)) ;

CIGLOAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF , ICIGLO)) ;

regle 301040:
application : iliad ;


ICREREVET = max(0 , min(IAD11 + ITP - CIRCMAVFT - IRETS , min(ITP , CREREVET))) ;

regle 301045:
application : iliad ;

ICREREVETAF = max(0 , min(IAD11 + ITP - CIRCMAVFTAF - IRETSAF , min(ITP , CREREVET))) ;

regle 301050:
application : iliad , bareme ;

INE = (CIRCMAVFT + IRETS + ICREREVET + CIGLO + CICULTUR + CIDONENTR + CICORSE + CIRECH + CICOMPEMPL) * (1 - positif(RE168 + TAX1649)) ;

IAN = max(0 , (IRB - AVFISCOPTER - INE
               + min(TAXASSUR + 0 , max(0 , INE - IRB + AVFISCOPTER)) 
               + min(IPCAPTAXTOT + 0 , max(0 , INE - IRB + AVFISCOPTER - min(TAXASSUR + 0 , max(0 , INE - IRB + AVFISCOPTER))))
	      )
         ) ;
IANINR = max(0 , (IRBINR - AVFISCOPTER - INE
               + min(TAXASSUR + 0 , max(0 , INE - IRBINR + AVFISCOPTER)) 
               + min(IPCAPTAXTOT + 0 , max(0 , INE - IRBINR + AVFISCOPTER - min(TAXASSUR + 0 , max(0 , INE - IRBINR + AVFISCOPTER))))
	      )
         ) ;

regle 301055:
application : iliad , bareme ;

INEAF = (CIRCMAVFTAF + IRETSAF + ICREREVETAF + CIGLOAF + CICULTURAF + CIDONENTRAF + CICORSEAF + CIRECHAF + CICOMPEMPLAF)
            * (1-positif(RE168+TAX1649));
regle 301057:
application : iliad , bareme ;

IANAF = max( 0, (IRBAF  + ((- CIRCMAVFTAF
				     - IRETSAF
                                     - ICREREVETAF
                                     - CIGLOAF
                                     - CICULTURAF
                                     - CIDONENTRAF
                                     - CICORSEAF
				     - CIRECHAF
                                     - CICOMPEMPLAF)
                                   * (1 - positif(RE168 + TAX1649)))
                  + min(TAXASSUR+0 , max(0,INEAF-IRBAF)) 
                  + min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))
	      )
         ) ;

NEGIANAF = -1 * (min(TAXASSUR+0 , max(0,INEAF-IRBAF))
                 + min(IPCAPTAXTOT+0 , max(0,INEAF-IRBAF - min(TAXASSUR+0,max(0,INEAF-IRBAF))))) ;

regle 301060:
application : iliad ;


IRE = (EPAV + CRICH + CICORSENOW + CIGE + CIDEVDUR + CITEC + CICA + CIGARD + CISYND 
       + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM + CREAGRIBIO + CRESINTER 
       + CREFORMCHENT + CREARTS + CICONGAGRI + AUTOVERSLIB
       + CI2CK + CIFORET + CIHJA + LOY8LA + COD8TE + LOY7LS
       + COD8TL * (1 - positif(RE168 + TAX1649))) * (1 - positif(RE168 + TAX1649 + 0)) ;

IREAF = (EPAV + CRICH + CICORSENOW + CIGE + CIDEVDUR + CITEC + CICA + CIGARD + CISYND 
       + CIPRETUD + CIADCRE + CIHABPRIN + CREFAM + CREAGRIBIO + CRESINTER 
       + CREFORMCHENT + CREARTS + CICONGAGRI + AUTOVERSLIB
       + CI2CK + CIFORET + CIHJA + LOY8LA + COD8TE + LOY7LS
       + COD8TL * (1 - positif(RE168 + TAX1649))
        ) * (1 - positif(RE168 + TAX1649 + 0)) ;

IRE2 = IRE ; 

regle 301065:
application : iliad ;

CIHJA = CODHJA * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;

regle 301070:
application : iliad ;

CRICH =  IPRECH * (1 - positif(RE168+TAX1649));

regle 301080:
application : iliad ;


CIRCMAVFT = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER , RCMAVFT * (1 - V_CNR)));

regle 301085:
application : iliad ;

CIRCMAVFTAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT , RCMAVFT * (1 - V_CNR)));

regle 301100:
application : iliad;
CI2CK = COD2CK * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);

regle 301110:
application : iliad;


CICA =  arr(BAILOC98 * TX_BAIL / 100) * (1 - positif(RE168 + TAX1649)) ;

regle 301130:
application : iliad ;


IPAE = COD8VL + COD8VM + COD8WM + COD8UM ;

RASIPSOUR =  IPSOUR * positif( null(V_REGCO-2) + null(V_REGCO-3) ) * ( 1 - positif(RE168+TAX1649) );

RASIPAE = COD8VM + COD8WM + COD8UM ;

regle 301133:
application : iliad ;

IRETS1 = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT , RASIPSOUR)) ;

IRETS21 = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS1 , min(COD8PB , COD8VL) * present(COD8PB) + COD8VL * (1 - present(COD8PB)))) 
          * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) ;

IRETS2 = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS1 - IRETS21 , min(COD8PA , RASIPAE) * present(COD8PA) + RASIPAE * (1 - present(COD8PA)))) 
         * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) + IRETS21 ;
	
IRETS = IRETS1 + IRETS2 ;

regle 301135:
application : iliad ;

IRETS1AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF , RASIPSOUR)) ;

IRETS21AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT + - CIRCMAVFTAF - IRETS1AF , min(COD8PB , COD8VL) * present(COD8PB) + COD8VL * (1 - present(COD8PB))))
            * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) ;

IRETS2AF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETS1AF - IRETS21AF , min(COD8PA , RASIPAE) * present(COD8PA) + RASIPAE * (1 - present(COD8PA))))
           * positif(null(V_REGCO - 1) + null(V_REGCO - 3) + null(V_REGCO - 5) + null(V_REGCO - 6)) + IRETS21AF ;
	
IRETSAF = IRETS1AF + IRETS2AF ;

regle 301150:
application : iliad ;

BCIAQCUL = arr(CIAQCUL * TX_CIAQCUL / 100);

regle 301152:
application : iliad ;

CICULTUR = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - REI - IRETS - ICREREVET - CIGLO , min(IAD11+ITP+TAXASSUR+IPCAPTAXT , BCIAQCUL))) ;

regle 301155:
application : iliad ;

CICULTURAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - REI - IRETSAF - ICREREVETAF - CIGLOAF , min(IAD11+ITP+TAXASSUR+IPCAPTAXT , BCIAQCUL))) ;

regle 301170:
application : iliad ;

BCIDONENTR = RDMECENAT * (1-V_CNR) ;

regle 301172:
application : iliad ;

CIDONENTR = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - REI - IRETS - ICREREVET - CIGLO - CICULTUR , BCIDONENTR)) ;

regle 301175:
application : iliad ;

CIDONENTRAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - REI - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF , BCIDONENTR)) ;

regle 301180:
application : iliad ;

CICORSE = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IPPRICORSE - IRETS - ICREREVET - CIGLO - CICULTUR - CIDONENTR , CIINVCORSE + IPREPCORSE)) ;

CICORSEAVIS = CICORSE + CICORSENOW ;

TOTCORSE = CIINVCORSE + IPREPCORSE + CICORSENOW ;

regle 301185:
application : iliad ;

CICORSEAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IPPRICORSE - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF - CIDONENTRAF , CIINVCORSE + IPREPCORSE)) ;

CICORSEAVISAF = CICORSEAF + CICORSENOW ;

regle 301190:
application : iliad ;

CIRECH = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET - CIGLO - CICULTUR - CIDONENTR - CICORSE , IPCHER)) ;

regle 301195:
application : iliad ;

CIRECHAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF - CIDONENTRAF - CICORSEAF , IPCHER)) ;
regle 301200:
application : iliad ;

CICOMPEMPL = max(0 , min(IRB + TAXASSUR + IPCAPTAXT - AVFISCOPTER - CIRCMAVFT - IRETS - ICREREVET - CIGLO - CICULTUR - CIDONENTR - CICORSE - CIRECH , COD8UW)) ;

DIEMPLOI = (COD8UW + COD8TL) * (1 - positif(RE168+TAX1649)) ;

CIEMPLOI = (CICOMPEMPL + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECR = abs(min(0 ,IRB+TAXASSUR+IPCAPTAXT -AVFISCOPTER-CIRCMAVFT-IRETS-ICREREVET-CIGLO-CICULTUR-CIDONENTR-CICORSE-CIRECH-CICOMPEMPL));

regle 301205:
application : iliad ;

CICOMPEMPLAF = max(0 , min(IRBAF + TAXASSUR + IPCAPTAXT - CIRCMAVFTAF - IRETSAF - ICREREVETAF - CIGLOAF - CICULTURAF - CIDONENTRAF - CICORSEAF - CIRECHAF , COD8UW)) ;

CIEMPLOIAF = (CICOMPEMPLAF + COD8TL) * (1 - positif(RE168+TAX1649)) ;

IRECRAF = abs(min(0 ,IRBAF+TAXASSUR+IPCAPTAXT -CIRCMAVFTAF-IRETSAF-ICREREVETAF-CIGLOAF-CICULTURAF-CIDONENTRAF-CICORSEAF-CIRECHAF-CICOMPEMPLAF));

regle 301210:
application : iliad ;
  
REPCORSE = abs(CIINVCORSE+IPREPCORSE-CICORSE) ;
REPRECH = abs(IPCHER - CIRECH) ;
REPCICE = abs(COD8UW - CICOMPEMPL) ;

regle 301220:
application : iliad ;

CICONGAGRI = CRECONGAGRI * (1-V_CNR) ;

regle 301230:
application : iliad ;

BCICAP = arr(PRELIBXT * TX90/100 * T_PCAPTAX/100) ;

regle 301233:
application : iliad ;

BCICAPAVIS = arr(PRELIBXT * TX90/100) ;

CICAP = max(0 , min(IPCAPTAXTOT , BCICAP)) ;

regle 301235:
application : iliad ;

CICAPAF = CICAP ;

regle 301240:
application : iliad ;

BCICHR = arr(CHRAPRES * (REGCI*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR - TEFFHRC+COD8YJ));
regle 301242:
application : iliad ;

CICHR = max(0,min(IRB + TAXASSUR + IPCAPTAXT +CHRAPRES - AVFISCOPTER ,min(CHRAPRES,BCICHR)));
regle 301245:
application : iliad ;

CICHRAF = max(0,min(IRBAF + TAXASSUR + IPCAPTAXT +CHRAPRES ,min(CHRAPRES,BCICHR)));
regle 301247:
application : iliad ;

BCICHR3WH = arr(CHRAPRES3WH * (REGCI*(1-present(COD8XY))+COD8XY+0) / (REVKIREHR+PVREPORT - TEFFHRC+COD8YJ));

regle 301249:
application : iliad ;

CICHR3WH = max(0,min(IRB + TAXASSUR + IPCAPTAXT +CHRAPRES3WH - AVFISCOPTER ,min(CHRAPRES3WH,BCICHR3WH)));

regle 301252:
application : iliad ;

CICHR3WHAF = max(0,min(IRBAF + TAXASSUR + IPCAPTAXTOT +CHRAPRES3WH -CICAPAF ,min(CHRAPRES3WH,BCICHR3WH)));

regle 301257:
application : iliad ;



DSYND = RDSYVO + RDSYCJ + RDSYPP ;


SOMBCOSV = TSHALLOV + COD1AA + CARTSV + ALLOV + REMPLAV + COD1GB + COD1GF + COD1GG + COD1AF  
           + CODRAF + COD1AG + CODRAG + PRBRV + CARPEV + PALIV + PENSALV + CODDAJ + CODEAJ 
	   + PENINV + CODRAZ + COD1AL + CODRAL + COD1AM + CODRAM + COD1TP + GLDGRATV 
	   + COD1TZ + COD1NX + max(0,COD1GH - LIM7500);

SOMBCOSC = TSHALLOC + COD1BA + CARTSC + ALLOC + REMPLAC + COD1HB + COD1HF + COD1HG + COD1BF 
           + CODRBF + COD1BG + CODRBG + PRBRC + CARPEC + PALIC + PENSALC + CODDBJ + CODEBJ 
	   + PENINC + CODRBZ + COD1BL + CODRBL + COD1BM + CODRBM + COD1UP + GLDGRATC + COD1OX + max(0,COD1HH - LIM7500);

SOMBCOSP = TSHALLO1 + TSHALLO2 + TSHALLO3 + TSHALLO4 + COD1CA + COD1DA + COD1EA + COD1FA   
           + CARTSP1 + CARTSP2 + CARTSP3 + CARTSP4 + ALLO1 + ALLO2 + ALLO3 + ALLO4    
           + REMPLAP1 + REMPLAP2 + REMPLAP3 + REMPLAP4 + COD1IB + COD1IF + COD1JB   
           + COD1JF + COD1KF + COD1LF + COD1CF + COD1DF + COD1EF + COD1FF   
           + CODRCF + CODRDF + CODREF + CODRFF + COD1CG + COD1DG + COD1EG + COD1FG    
           + CODRCG + CODRDG + CODRGG + CODRFG + PRBR1 + PRBR2 + PRBR3 + PRBR4     
           + CARPEP1 + CARPEP2 + CARPEP3 + CARPEP4 + PALI1 + PALI2 + PALI3 + PALI4     
           + PENSALP1 + PENSALP2 + PENSALP3 + PENSALP4 + PENIN1 + PENIN2 + PENIN3 + PENIN4    
           + CODRCZ + CODRDZ + CODREZ + CODRFZ + COD1CL + COD1DL + COD1EL + COD1FL    
           + CODRCL + CODRDL + CODREL + CODRFL + COD1CM + COD1DM + COD1EM + COD1FM    
           + CODRCM + CODRDM + CODREM + CODRFM + COD1IG + COD1JG + COD1KG + COD1LG 
	   + max(0,COD1IH - LIM7500)+ max(0,COD1JH - LIM7500)+ max(0,COD1KH - LIM7500)+ max(0,COD1LH - LIM7500);


BCOS = min(RDSYVO+0,arr(TX_BASECOTSYN/100*SOMBCOSV*IND_10V))
      +min(RDSYCJ+0,arr(TX_BASECOTSYN/100*SOMBCOSC*IND_10C))                             
      +min(RDSYPP+0,arr(TX_BASECOTSYN/100*SOMBCOSP*IND_10P));

ASYND = BCOS * (1-V_CNR) ;


CISYND = arr(TX_REDCOTSYN/100 * BCOS) * (1 - V_CNR) ;

regle 301260:
application : iliad ;


IAVF = IRE - EPAV + CICORSE + CICULTUR + CIRCMAVFT ;


DIAVF2 = (IPRECH + IPCHER + RCMAVFT ) * (1 - positif(RE168+TAX1649)) + CIRCMAVFT * positif(RE168+TAX1649);


IAVF2 = (IPRECH + CIRECH + CIRCMAVFT + 0) * (1 - positif(RE168 + TAX1649))
        + CIRCMAVFT * positif(RE168 + TAX1649) ;

IAVFGP = IAVF2 + CREFAM ;

regle 301270:
application : iliad ;


I2DH = EPAV ;

regle 301280:
application : iliad ;


BTANTGECUM   = (V_BTGECUM * (1 - positif(present(COD7ZZ)+present(COD7ZY)+present(COD7ZX)+present(COD7ZW))) + COD7ZZ+COD7ZY+COD7ZX+COD7ZW);

BTANTGECUMWL = V_BTPRT5 * (1 - present(COD7WK)) + COD7WK 
               + V_BTPRT4 * (1 - present(COD7WQ)) + COD7WQ 
	       + V_BTPRT3 * (1- present (COD7WH)) + COD7WH
	       + V_BTPRT2 * (1- present (COD7WS)) + COD7WS
	       + V_BTPRT1 * (1- present (COD7XZ)) + COD7XZ
	       ;

P2GE = max( (   PLAF_GE2 * (1 + BOOL_0AM)
             + PLAF_GE2_PACQAR * (V_0CH + V_0DP)
             + PLAF_GE2_PAC * (V_0CR + V_0CF + V_0DJ + V_0DN)  
              ) - BTANTGECUM
             , 0
             ) ;
BGEPAHA = min(RDEQPAHA +COD7WI, P2GE) * (1 - V_CNR);

P2GEWL = max(0,PLAF20000 - BTANTGECUMWL);
BGTECH = min(RDTECH , P2GEWL) * (1 - V_CNR) ;

BGEDECL = RDTECH + RDEQPAHA + COD7WI;
TOTBGE = BGTECH + BGEPAHA ;

RGEPAHA =  (BGEPAHA * TX25 / 100 );
RGTECH = (BGTECH * TX40 / 100); 
CIGE = arr (RGTECH + RGEPAHA ) * (1 - positif(RE168 + TAX1649));


DEPENDPDC = BGEPAHA ;


DEPENPPRT = BGTECH ;

GECUM = min(P2GE,BGEPAHA)+(V_BTGECUM * (1 - positif(present(COD7ZY)+present(COD7ZX)+present(COD7ZW))) + COD7ZW +COD7ZX + COD7ZY);
GECUMWL = max(0,BGTECH + BTANTGECUMWL) ;

BADCRE = min(max(0,CREAIDE-COD7DR) , min((LIM_AIDOMI * (1 - positif(PREMAIDE)) + LIM_PREMAIDE * positif(PREMAIDE)
                            + MAJSALDOM * (positif_ou_nul(ANNEEREV-V_0DA-65) + positif_ou_nul(ANNEEREV-V_0DB-65) * BOOL_0AM
                                           + V_0CF + V_0DJ + V_0DN + (V_0CH + V_0DP)/2+ASCAPA)
                           ) , LIM_AIDOMI3 * (1 - positif(PREMAIDE)) + LIM_PREMAIDE2 * positif(PREMAIDE) ) * (1-positif(INAIDE + 0))
                               +  LIM_AIDOMI2 * positif(INAIDE + 0)) ;

DAIDC = max(0 , CREAIDE - COD7DR) ;
AAIDC = BADCRE * (1 - V_CNR) ;
CIADCRE = max(0 , arr(BADCRE * TX_AIDOMI /100)) * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR) ;
CIADCREB3 = COD7HB * (1 - V_CNR) ;

regle 301301:
application : iliad ;





DDEVDURN1 = COD7AA + COD7AB + COD7AF + COD7AH + COD7AP + COD7AR + COD7AV + COD7AS + COD7AY + COD7AZ + COD7BB + COD7BN + COD7BQ + COD7BC + COD7BM + COD7BD + COD7BE + COD7BF + COD7BH + COD7BK + COD7BL ;

RFRDEVDURN1 = min(V_BTRFRN2*(1-present(RFRN2)) * (1-present(COD8XZ))+ RFRN2 * (1-positif(COD8XZ +0)) + COD8XZ * positif(COD8XZ + RFRN2) ,
                  V_BTRFRN1*(1-present(RFRN1)) * (1-present(COD8YZ))+ RFRN1 * (1-positif(COD8YZ + 0)) + COD8YZ * positif(COD8YZ + RFRN1))
            +  (V_BTRFRN2 * (1-present(RFRN2)) * (1-present(COD8XZ)) + RFRN2 * (1-positif(COD8XZ +0)) + COD8XZ * positif(COD8XZ + RFRN2)) 
	    *   positif(positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ)) * (1-positif(present(V_BTRFRN1) + present(RFRN1) + present(COD8YZ))))
	    + (V_BTRFRN1 * (1-present(RFRN1)) * (1-present(COD8YZ)) + RFRN1 * (1-positif(COD8YZ +0)) + COD8YZ * positif(COD8YZ + RFRN1))
	    * positif(positif(present(V_BTRFRN1) + present(RFRN1) + present(COD8YZ)) * (1-positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ))));
	    
INDRESN1 = (positif(positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ)) * positif(present(V_BTRFRN1) + present(RFRN1) + present(COD8YZ)) 
         + positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ))
         + positif(present(V_BTRFRN1) + present(RFRN1) + present(COD8YZ))) *(

          positif(V_INDIDF) * (
           positif_ou_nul(LIM_IDF1 - RFRDEVDURN1) * null(1-NBPERS)
         + positif_ou_nul(LIM_IDF2 - RFRDEVDURN1) * null(2-NBPERS)
         + positif_ou_nul(LIM_IDF3 - RFRDEVDURN1) * null(3-NBPERS)
         + positif_ou_nul(LIM_IDF4 - RFRDEVDURN1) * null(4-NBPERS)
         + positif_ou_nul(LIM_IDF5 - RFRDEVDURN1) * null(5-NBPERS)
         + positif_ou_nul(LIM_IDF5 + LIM_IDFSUP * (NBPERS-5) - RFRDEVDURN1) * positif(NBPERS-5))
 
         + null(V_INDIDF + 0) *(
	   positif_ou_nul(LIM_NONIDF1 - RFRDEVDURN1) * null(1-NBPERS)
         + positif_ou_nul(LIM_NONIDF2 - RFRDEVDURN1) * null(2-NBPERS)
         + positif_ou_nul(LIM_NONIDF3 - RFRDEVDURN1) * null(3-NBPERS)
         + positif_ou_nul(LIM_NONIDF4 - RFRDEVDURN1) * null(4-NBPERS)
         + positif_ou_nul(LIM_NONIDF5 - RFRDEVDURN1 ) * null(5-NBPERS)
         + positif_ou_nul(LIM_NONIDF5 + (LIM_NONIDFSUPN * (NBPERS-5)) - RFRDEVDURN1)* positif(NBPERS-5)))
	 ) * (1-positif(V_INDCITE19))
	 + 1 * positif(V_INDCITE19)
         + 0 ;


IDEVCUMN1 = V_BTDEV5 * (1- present (COD7XV)) 
          + V_BTDEV4 * (1- present (COD7XG))
          + V_BTDEV3 * (1- present (COD7XF))
          + V_BTDEV2 * (1- present (COD7XE))
          + V_BTDEV1 * (1- present (COD7XD)) ;

PDEVDURN1 = max(((PLAF_DEVDUR * (1 + BOOL_0AM)
          + PLAF_GE2_PACQAR * (V_0CH+V_0DP)
          + PLAF_GE2_PAC * (V_0CR+V_0CF+V_0DJ+V_0DN))
          - (IDEVCUMN1 + COD7XD + COD7XE + COD7XF + COD7XG + COD7XV)) , 0);



BDEV50N1 = min(PDEVDURN1 , COD7BQ) * INDRESN1 + 0;

BDEV30N1 = min(max(0,PDEVDURN1 - BDEV50N1), min(LIM3350,COD7AA) +  min(LIM3350,COD7AB) + COD7AF + COD7AH  + COD7AR + COD7AV
         + min(LIM4000,COD7AS)*INDRESN1+min(LIM3000,COD7AS)*(1-INDRESN1) + COD7AY + COD7AZ + COD7BB  + COD7BN * INDRESN1 + COD7BC + COD7BM + COD7BD
         + COD7BE + COD7BF + COD7BH + COD7BK + COD7BL ) ;


BDEV15N1 = min(max(0,PDEVDURN1 - BDEV50N1-BDEV30N1),min(LIM670*COD7AT,COD7AP)) + 0;

BDEVDURN1 = BDEV30N1 + BDEV15N1 + BDEV50N1 ;

CIDEVDURN1 = arr(BDEV30N1 * TX30/100 + BDEV15N1 * TX15/100 + BDEV50N1 * TX50/100) * (1 - positif(RE168 + TAX1649)) * (1-V_CNR);

regle 301302:
application : iliad ;




DDEVDURN = COD7AL + COD7AN + COD7CK + COD7AQ + COD7DA + COD7BR + COD7DD + COD7BV + COD7DF + COD7BX + COD7BY + COD7EH + COD7EM + COD7EQ + COD7DH + COD7AW              + COD7ER + COD7DK + COD7EU + COD7DM + COD7EV + COD7DN + COD7EW + COD7DU + COD7EX + COD7DV + COD7FB + COD7GK + COD7FC + COD7GP + COD7FD + COD7GT              + COD7FE + COD7GV + COD7GH  ;  


ICITE3 = (V_BTDEV3 * (1-present(COD7XF)) + COD7XF) * TX30/100 ;
ICITE4 = (V_BTDEV4 *(1-present(COD7XG)) + COD7XG) *  TX30/100 ;


ICITECUM = ICITE4*(1-present(COD7VD)) + ICITE3*(1-present(COD7VE)) + V_BTCITE2*(1-present(COD7VF)) + V_BTCITE1*(1-present(COD7VG)) ;

RFRDEVDURN = max( V_BTRFRN2 * (1-present(RFRN2)) * (1-present(COD8XZ)) + RFRN2 * (1-positif(COD8XZ + 0)) + COD8XZ * positif(COD8XZ + RFRN2) ,                                  V_BTRFRN1 * (1-present(RFRN1)) * (1-present(COD8YZ)) + RFRN1 * (1-positif(COD8YZ + 0)) + COD8YZ * positif(COD8YZ + RFRN1));

VARIDF = V_INDIDF * positif(14 - V_NOTRAIT) + (V_INDIDF * (1 - CODIDF) + (1 - V_INDIDF) * CODIDF) * (1 - positif(14 - V_NOTRAIT)) ;

RFRMIN = VARIDF * (null(1-NBPERS) * LIM_IDF1
       + null(2-NBPERS) * LIM_IDF2
       + null(3-NBPERS) * LIM_IDF3
       + null(4-NBPERS) * LIM_IDF4
       + null(5-NBPERS) * LIM_IDF5 
       + positif(NBPERS-5) * (LIM_IDF5 + (LIM_IDFSUP * (NBPERS-5))))
       + (1-VARIDF) * (null(1-NBPERS) * LIM_NONIDF1
       + null(2-NBPERS) * LIM_NONIDF2
       + null(3-NBPERS) * LIM_NONIDF3
       + null(4-NBPERS) * LIM_NONIDF4
       + null(5-NBPERS) * LIM_NONIDF5
       + positif(NBPERS-5) * (LIM_NONIDF5 + (LIM_NONIDFSUPN * (NBPERS-5))));


RFRMAX = null(1-NBPT) * LIM_RFRMAX1
       + null(1.25-NBPT) * LIM_RFRMAX125
       + null(1.5-NBPT) * LIM_RFRMAX15
       + null(1.75-NBPT) * LIM_RFRMAX175
       + null(2-NBPT) * LIM_RFRMAX2
       + null(2.25-NBPT) * LIM_RFRMAX225
       + null(2.5-NBPT) * LIM_RFRMAX25
       + null(2.75-NBPT) * LIM_RFRMAX275
       + null(3-NBPT) * LIM_RFRMAX3
       + positif(NBPT-3) *(LIM_RFRMAX3 + LIM_DEVSUP1 *((NBPT-3)*4)); 
       

CONDRFR2 = V_BTRFRN2*(1-present(RFRN2)) * (1-present(COD8XZ))+ RFRN2 * (1-present(COD8XZ)) + COD8XZ * positif(present(COD8XZ) + present(RFRN2)) ;
CONDRFR1 = V_BTRFRN1*(1-present(RFRN1)) * (1-present(COD8YZ))+ RFRN1 * (1-present(COD8YZ )) + COD8YZ * positif(present(COD8YZ) + present(RFRN1)) ;

CONDINDR = (positif(RFRMIN - CONDRFR2) * positif(RFRMIN - CONDRFR1) * positif(CONDRFR2 * CONDRFR1) + positif(RFRMIN - CONDRFR2)* (1-positif(CONDRFR1)) + positif(RFRMIN - CONDRFR1)* (1-positif(CONDRFR2)))
         +  positif (positif_ou_nul(CONDRFR2 - RFRMIN)* (1-positif_ou_nul(CONDRFR2-RFRMAX)) + positif(RFRMAX - CONDRFR1) * (1-positif(RFRMIN - CONDRFR1)));


INDRESN = positif(positif(present(V_BTRFRN2)+present(RFRN2) + present(COD8XZ))*positif(present(V_BTRFRN1)+present(RFRN1)+present(COD8YZ)) + positif(present(V_BTRFRN2) + present(RFRN2) + present(COD8XZ)) + positif(present(V_BTRFRN1) + present(RFRN1)+present(COD8YZ))) * (
       positif_ou_nul(RFRMAX - RFRMIN) * (
       positif(positif(RFRMIN - CONDRFR2) * positif(RFRMIN - CONDRFR1) * positif(CONDRFR2 * CONDRFR1) + positif(RFRMIN - CONDRFR2)* (1-positif(CONDRFR1)) + positif(RFRMIN - CONDRFR1)* (1-positif(CONDRFR2))) * 1
     + positif (positif_ou_nul(CONDRFR2 - RFRMIN)* (1-positif_ou_nul(CONDRFR2-RFRMAX)) + positif(RFRMAX - CONDRFR1) * (1-positif(RFRMIN - CONDRFR1)))* 2
     + positif(positif_ou_nul(CONDRFR2 - RFRMAX) + positif_ou_nul(CONDRFR1 - RFRMAX))* positif(COD7AN + COD7CK + COD7AQ + COD7DA)* (1-positif(CONDINDR)) * 3)
     + positif(RFRMIN - RFRMAX) * (
       positif(positif(RFRMAX - CONDRFR2)* positif(RFRMAX - CONDRFR1)) * 1
     + positif(positif(CONDRFR2 - RFRMAX) + positif(CONDRFR1 - RFRMAX)) * positif(COD7AN + COD7CK + COD7AQ + COD7DA) * 3)) ;

regle 301303:
application : iliad ;




CI7AL = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7AK*NOMBRE40 , COD7AL * NOMBRE75/100));

CI7AN = null(1-INDRESN) * 0
      + null(2-INDRESN) * arr(min(COD7AM * NOMBRE15 , COD7AN * NOMBRE75/100))
      + null(3-INDRESN) * arr(min(COD7AM * NOMBRE10 , COD7AN * NOMBRE75/100));

CI7CK = null(1-INDRESN) * 0
      + null(2-INDRESN) * arr(min(COD7CC * NOMBRE15 * min(1,(COD7CG/100)) , COD7CK * NOMBRE75/100))
      + null(3-INDRESN) * arr(min(COD7CC * NOMBRE10 * min(1,(COD7CG/100)) , COD7CK * NOMBRE75/100));


CI7AQ = null(1-INDRESN) * 0
      + null(2-INDRESN) * arr(min(COD7AO * NOMBRE50 , COD7AQ * NOMBRE75/100))
      + null(3-INDRESN) * arr(min(COD7AO * NOMBRE25 , COD7AQ * NOMBRE75/100));

CI7DA = null(1-INDRESN) * 0
      + null(2-INDRESN) * arr(min(COD7CN * NOMBRE50 * min(1,(COD7CU/100)) , COD7DA * NOMBRE75/100))
      + null(3-INDRESN) * arr(min(COD7CN * NOMBRE25 *min(1,(COD7CU/100)) , COD7DA * NOMBRE75/100));

CI7BR = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7BR*NOMBRE75/100 , LIM4000));

CI7DD = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DD*NOMBRE75/100 , LIM1000));

CI7BV = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7BV*NOMBRE75/100 , LIM3000));

CI7DF = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DF*NOMBRE75/100 , LIM1000));

CI7BX = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7BX*NOMBRE75/100 , LIM3000));
      
CI7BY = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7BY*NOMBRE75/100 , LIM2000));

CI7EH = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7EH*NOMBRE75/100 , LIM1500));

CI7EM = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7EM*NOMBRE75/100 , LIM1000));

CI7EQ = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7EQ*NOMBRE75/100 , LIM1000));

CI7DH = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DH*NOMBRE75/100 , LIM350));

CI7AW = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7AW*NOMBRE75/100 , LIM600));

CI7ER = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7ER*NOMBRE75/100 , LIM4000));

CI7DK = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DK*NOMBRE75/100 , LIM1000));

CI7EU = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7EU*NOMBRE75/100 , LIM2000));

CI7DM = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DM*NOMBRE75/100 , LIM1000));
     
CI7EV = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7EV*NOMBRE75/100 , LIM400));

CI7DN = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DN*NOMBRE75/100 , LIM150));

CI7EW = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7EW*NOMBRE75/100 , LIM400));

CI7DU = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DU*NOMBRE75/100 , LIM150));

CI7EX = arr(min(COD7EX*NOMBRE75/100 , LIM300));      

CI7DV = arr(min(COD7DV*NOMBRE75/100 , LIM300));

CI7FB = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7FA * NOMBRE15 , COD7FB * NOMBRE75/100));

CI7GK = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7DX * NOMBRE15 * min(1,(COD7GI/100)), COD7GK * NOMBRE75/100));

CI7FC = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7FC*NOMBRE75/100 , LIM300));

CI7GP = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7GP*NOMBRE75/100 , LIM150));

CI7FD = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7FD*NOMBRE75/100 , LIM400));

CI7GT = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7GT*NOMBRE75/100 , LIM150));

CI7FE = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7FE*NOMBRE75/100 , LIM2000));

CI7GV = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7GV*NOMBRE75/100 , LIM1000));

CI7GH = positif(null(1-INDRESN) + null(3-INDRESN)) * 0
      + null(2-INDRESN) * arr(min(COD7FZ * LIM150 , COD7GH * NOMBRE75/100)); 


PLAFAVDEV = max((( PLAF_DEVDURN * (1 + BOOL_0AM)
          + PLAF_GE2_PACQARN * (V_0CH+V_0DP)
          + PLAF_GE2_PACN * (V_0CR+V_0CF+V_0DJ+V_0DN))
          - (ICITECUM + COD7VD + COD7VE + COD7VF + COD7VG)), 0);

PDEVDURN =max(0,PLAFAVDEV - CIDEVDURN1) ;
	

CIDEVDURN = arr(min( CI7AL + CI7AN + CI7CK + CI7AQ + CI7DA + CI7BR + CI7DD + CI7BV + CI7DF + CI7BX + CI7BY + CI7EH + CI7EM + CI7EQ + CI7DH + CI7AW + CI7ER + CI7DK + CI7EU + CI7DM + CI7EV + CI7DN + CI7EW + CI7DU + CI7EX + CI7DV + CI7FB + CI7GK + CI7FC + CI7GP + CI7FD + CI7GT + CI7FE + CI7GV + CI7GH , PDEVDURN )) * (1-V_CNR);   

regle 301304:
application : iliad ;




CIDEVRET19 = (COD7XD + COD7XE + COD7XF + COD7XG + COD7XV) * (1-V_CNR) ; 

CIDEVRET20 = (COD7VG + COD7VF + COD7VE + COD7VD) * (1-V_CNR) ;    


 
DDEVDUR =  (DDEVDURN1 + DDEVDURN) ; 



CIDEVDUR =  arr((CIDEVDURN1 + CIDEVDURN) * (1-present(CODCIT)) + CODCIT) * (1-V_CNR) ;
regle 301310:
application : iliad ;

DTEC = RISKTEC;
ATEC = positif(DTEC) * DTEC;
CITEC = arr (ATEC * TX40/100);

regle 301320:
application : iliad ;

DPRETUD = PRETUD + PRETUDANT ;
APRETUD = max(min(PRETUD,LIM_PRETUD) + min(PRETUDANT,LIM_PRETUD*CASEPRETUD),0) * (1-V_CNR) ;

CIPRETUD = arr(APRETUD*TX_PRETUD/100) * (1 - positif(RE168 + TAX1649)) * (1-V_CNR) ;

regle 301330:
application : iliad ;


EM7AVRICI = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - ANNEEREV+1)))
         + somme (j=0..5: min (1 , max(0 , V_0Nj + AG_LIMFG - ANNEEREV+1)))
      + (1 - positif(somme(i=0..7:V_0Fi) + somme(i=0..5: V_0Ni) + 0)) * (V_0CF + V_0DN) ;

EM7QARAVRICI = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - ANNEEREV+1)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - ANNEEREV+1)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

EM7 = somme (i=0..7: min (1 , max(0 , V_0Fi + AG_LIMFG - ANNEEREV)))
         + somme (j=0..5: min (1 , max(0 , V_0Nj + AG_LIMFG - ANNEEREV)))
      + (1 - positif(somme(i=0..7:V_0Fi) + somme(i=0..5: V_0Ni) + 0)) * (V_0CF + V_0DN) ;

EM7QAR = somme (i=0..5: min (1 , max(0 , V_0Hi + AG_LIMFG - ANNEEREV)))
         + somme (j=0..3: min (1 , max(0 , V_0Pj + AG_LIMFG - ANNEEREV)))
         + (1 - positif(somme(i=0..5: V_0Hi) + somme(j=0..3: V_0Pj) + 0)) * (V_0CH + V_0DP) ;

BRFG = min(RDGARD1,PLAF_REDGARD) + min(RDGARD2,PLAF_REDGARD)
       + min(RDGARD3,PLAF_REDGARD) + min(RDGARD4,PLAF_REDGARD)
       + min(RDGARD1QAR,PLAF_REDGARDQAR) + min(RDGARD2QAR,PLAF_REDGARDQAR)
       + min(RDGARD3QAR,PLAF_REDGARDQAR) + min(RDGARD4QAR,PLAF_REDGARDQAR)
       ;
RFG1 = arr ( (BRFG) * TX_REDGARD /100 ) * (1 -V_CNR);
DGARD = somme(i=1..4:RDGARDi)+somme(i=1..4:RDGARDiQAR);
AGARD = (BRFG) * (1-V_CNR) ;
CIGARD = RFG1 * (1 - positif(RE168 + TAX1649)) ;

regle 301340:
application : iliad ;


PREHAB = PREHABT + PREHABT2 + PREHABTN2 + PREHABTVT ;

BCIHP = max(( PLAFHABPRIN * (1 + BOOL_0AM) * (1+positif(V_0AP+V_0AF+V_0CG+V_0CI+V_0CR))
                 + (PLAFHABPRINENF/2) * (V_0CH + V_0DP)
                 + PLAFHABPRINENF * (V_0CR + V_0CF + V_0DJ + V_0DN)
                  )
             ,0);

BCIHABPRIN1 = min(BCIHP , PREHABT) * (1 - V_CNR) ;
BCIHABPRIN5 = min(max(0,BCIHP-BCIHABPRIN1),PREHABT2) * (1 - V_CNR);
BCIHABPRIN6 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN5),PREHABTN2) * (1 - V_CNR);
BCIHABPRIN7 = min(max(0,BCIHP-BCIHABPRIN1-BCIHABPRIN5-BCIHABPRIN6),PREHABTVT) * (1 - V_CNR);

BCIHABPRIN = BCIHABPRIN1 + BCIHABPRIN5 + BCIHABPRIN6 + BCIHABPRIN7 ;

CIHABPRIN = arr((BCIHABPRIN1 * TX40/100)
                + (BCIHABPRIN5 * TX20/100)
                + (BCIHABPRIN6 * TX15/100)
                + (BCIHABPRIN7 * TX10/100))
                * (1 - positif(RE168 + TAX1649)) * (1 - V_CNR);

regle 301350:
application : iliad ;


BDCIFORET = COD7VH + COD7TV + COD7VI + COD7TW + COD7VQ + COD7VR + COD7TP + COD7TQ + COD7TM + COD7TO
 	    + REPSINFOR5 + COD7TK +RDFORESTRA + SINISFORET
	    + COD7UA + COD7UB + RDFORESTGES + COD7UI + COD7VS + COD7TR + COD7VL + COD7TS
	    + COD7VJ + COD7TT + COD7VK + COD7TU ;
BCIFORETTK = max(0 , min(COD7TK , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)))) * (1-V_CNR) ;
VARTMP1 = BCIFORETTK ;

BCIFORETTJ = max(0 , min(REPSINFOR5 , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTJ ;


BCIFORETTO = max(0 , min(COD7TO , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTO ;


BCIFORETTM = max(0 , min(COD7TM , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTM ;

BCIFORETVR = max(0 , min(COD7VR , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVR ;

BCIFORETTQ = max(0 , min(COD7TQ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTQ ;

BCIFORETVQ = max(0 , min(COD7VQ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVQ ;

BCIFORETTP = max(0 , min(COD7TP , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTP ;

BCIFORETVL = max(0 , min(COD7VL , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVL ;

BCIFORETTS = max(0 , min(COD7TS , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTS ;

BCIFORETVS = max(0 , min(COD7VS , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVS ;

BCIFORETTR = max(0 , min(COD7TR , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTR ;

BCIFORETVK = max(0 , min(COD7VK , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVK ;

BCIFORETTU = max(0 , min(COD7TU , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTU ;

BCIFORETVJ = max(0 , min(COD7VJ , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVJ ;

BCIFORETTT = max(0 , min(COD7TT , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTT ;

BCIFORETVI = max(0 , min(COD7VI , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVI ;

BCIFORETTW = max(0 , min(COD7TW , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTW ;

BCIFORETVH = max(0 , min(COD7VH , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETVH ;

BCIFORETTV = max(0 , min(COD7TV , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETTV ;

BCIFORETUA = max(0 , min(COD7UA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUA ;

BCIFORETUB = max(0 , min(COD7UB , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUB ;

BCIFORETUP = max(0 , min(RDFORESTRA , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = VARTMP1 + BCIFORETUP ;

BCIFORETUT = max(0 , min(SINISFORET , max(0 , PLAF_FOREST1 * (1 + BOOL_0AM)-VARTMP1))) * (1-V_CNR) ;
VARTMP1 = 0 ;

BCIFORETUI = max(0 , min(COD7UI , max(0 , PLAF_FOREST2 * (1 + BOOL_0AM)))) * (1-V_CNR) ;

BCIFORETUQ = max(0 , min(RDFORESTGES , max(0 , PLAF_FOREST2 * (1 + BOOL_0AM)-BCIFORETUI))) * (1-V_CNR) ;

BCIFORET = BCIFORETTK + BCIFORETTJ + BCIFORETVN + BCIFORETTO + BCIFORETVM + BCIFORETTM + BCIFORETVR + BCIFORETTQ + BCIFORETVQ + BCIFORETTP 
           + BCIFORETVL + BCIFORETTS + BCIFORETVS + BCIFORETTR + BCIFORETVK + BCIFORETTU + BCIFORETVJ + BCIFORETTT + BCIFORETVI 
	   + BCIFORETTW + BCIFORETVH + BCIFORETTV + BCIFORETUA + BCIFORETUB 
	   + BCIFORETUP + BCIFORETUT + BCIFORETUI + BCIFORETUQ ;
	 

CIFORET = arr((BCIFORETTJ + BCIFORETVM + BCIFORETTM + BCIFORETVQ + BCIFORETTP + BCIFORETVS + BCIFORETTR + BCIFORETVJ + BCIFORETTT + BCIFORETUP + BCIFORETUT + BCIFORETUQ
                                                           + BCIFORETVH + BCIFORETTV ) * TX18/100 
              + (BCIFORETTK + BCIFORETVN + BCIFORETTO + BCIFORETVR + BCIFORETTQ + BCIFORETVL + BCIFORETTS + BCIFORETVK + BCIFORETTU + BCIFORETUA + BCIFORETUB + BCIFORETUI
	                                                   + BCIFORETVI + BCIFORETTW ) * TX25/100) ;

regle 301360:
application : iliad ;



REPCIFADSSN6 = max(0 , COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM)) * (1 - V_CNR) ;

REPCIFSN6 = (positif_ou_nul(COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM)) * REPSINFOR5
             + (1 - positif_ou_nul(COD7TK - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , REPSINFOR5 - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK))) * (1 - V_CNR) ;


REPCIFADSSN5 = (positif_ou_nul(COD7TK + REPSINFOR5 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TO
                + (1 - positif_ou_nul(COD7TK + REPSINFOR5 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TO - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK - REPSINFOR5))) * (1 - V_CNR) ;

REPCIFSN5 = (positif_ou_nul(COD7TK + REPSINFOR5 + COD7TO - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TM
             + (1 - positif_ou_nul(COD7TK + REPSINFOR5 + COD7TO - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TM - (PLAF_FOREST1 * (1 + BOOL_0AM) - COD7TK - REPSINFOR5 + COD7TO))) * (1 - V_CNR) ;

VARTMP1 = COD7TK + REPSINFOR5 + COD7TO + COD7TM ;

REPCIFADHSN4 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VR
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VR - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VR ;
							   
REPCIFADSSN4 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TQ
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TQ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TQ ;
							   
REPCIFHSN4 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VQ
              + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VQ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VQ ;
							   
REPCIFSN4 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TP
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TP - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TP ;
							   

REPCIFADHSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VL
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VL - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VL ;

REPCIFADSSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TS
	        + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TS - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TS ;

REPCIFHSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VS
	      + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VS - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VS ;

REPCIFSN3 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TR
	     + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TR - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TR ;
			     

REPCIFADHSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VK
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VK - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VK ;

REPCIFADSSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TU
                + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TU - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TU ;

REPCIFHSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VJ
              + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VJ - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VJ ;

REPCIFSN2 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TT
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TT - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TT ;


REPCIFADHSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VI
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VI - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VI ;
							   
REPCIFADSSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TW
               + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TW - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TW ;
							   
REPCIFHSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7VH
          + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7VH - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7VH ;
							   
REPCIFSN1 = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7TV
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7TV - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7TV ;


REPCIFAD = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7UA
            + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7UA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7UA ;
							   
REPCIFADSIN = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * COD7UB
               + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , COD7UB - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + COD7UB ;
							   
REPCIF = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * RDFORESTRA
          + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , RDFORESTRA - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = VARTMP1 + RDFORESTRA ;
							   
REPCIFSIN = (positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM)) * SINISFORET
             + (1 - positif_ou_nul(VARTMP1 - PLAF_FOREST1 * (1 + BOOL_0AM))) * max(0 , SINISFORET - (PLAF_FOREST1 * (1 + BOOL_0AM) - VARTMP1))) * (1 - V_CNR) ;
VARTMP1 = 0 ;


regle 301365:
application : iliad ;

REP7UP = REPCIF + REPCIFHSN1 + REPCIFHSN2 + REPCIFHSN3 + REPCIFHSN4;
REP7UA = REPCIFAD + REPCIFADHSN1 + REPCIFADHSN2 + REPCIFADHSN3 + REPCIFADHSN4;
REP7UT = REPCIFSIN + REPCIFSN1 + REPCIFSN2 + REPCIFSN3 + REPCIFSN4 + REPCIFSN5 + REPCIFSN6;
REP7UB = REPCIFADSIN + REPCIFADSSN1 + REPCIFADSSN2 + REPCIFADSSN3 + REPCIFADSSN4 + REPCIFADSSN5 +REPCIFADSSN6; 

regle 301370:
application : iliad ;


CICSG = min(CSGC , arr(IPPNCS * T_CSGCRDS/100)) ;

CIRDS = min(RDSC , arr((min(REVCSXA , SALECS) + min(REVCSXB , SALECSG + COD8SC)
                        + min(REVCSXC , ALLECS) + min(REVCSXD , INDECS + COD8SW)
                        + min(REVCSXE , PENECS + COD8SX) + min(COD8XI , COD8SA)
                        + min(COD8XJ , COD8SB) + min (COD8XM , GLDGRATV + GLDGRATC)
			+ min(COD8XO , COD8TH) + min (COD8XN , COD8SD)
                       ) * T_RDS/100)) ;

CIPSOL = min(MPSOL , arr(IPPNCS * TXPSOL/100)) ;

CICVN = min( CVNSALC , arr( min(BCVNSAL,COD8XL) * TX10/100 )) ;

CIGLOA = min( CGLOA , arr ( min(GLDGRATV+GLDGRATC,COD8XM) * T_CSG/100));


CIRSE1 = min( RSE1 , arr( min(SALECS,REVCSXA) * TXTQ/100 ));

RSE8TV = arr(BRSE8TV * TXTV/100) * (1 - positif(ANNUL2042));
RSE8SA = arr(BRSE8SA * TXTV/100) * (1 - positif(ANNUL2042));
CIRSE8TV = min( RSE8TV , arr( min(ALLECS,REVCSXC) * TXTV/100 )) ;
CIRSE8SA = min( RSE8SA , arr(min(COD8SA,COD8XI) * TXTV/100 )) ;
CIRSE2 = min(RSE2, arr(min(ALLECS,REVCSXC)* TXTV/100 + min(COD8SA,COD8XI) * TXTV/100));

CIRSE3 = min( RSE3 , arr( min(COD8SW+INDECS,REVCSXD * TXTW/100 )));

RSE8TX = arr(BRSE8TX * TXTX/100) * (1 - positif(ANNUL2042));
RSE8SB = arr(BRSE8SB * TXTX/100) * (1 - positif(ANNUL2042));
CIRSE8TX = min( RSE8TX , arr( REVCSXE * TXTX/100 )) ;
CIRSE8SB = min( RSE8SB , arr( COD8XJ * TXTX/100 ));
CIRSE4 =  min(RSE4, arr(min(PENECS+COD8SX,REVCSXE)* TXTX/100 + min(COD8XJ,COD8SB) * TXTX/100));

CIRSE5 = min( RSE5 , arr( min(SALECSG+COD8SC,REVCSXB) * TXTR/100 ));

CIRSE6 = min( RSE6 , arr(( min( REVCSXB , SALECSG+COD8SC ) +
                           min( REVCSXC , ALLECS ) +
                           min( COD8XI , COD8SA ) +
			   min( COD8XN, COD8SD )  +
                           min( COD8XO, COD8TH ) 
                         ) * TXCASA/100 ));
			 
CIRSE8 =  arr((min(COD8XN ,COD8SD) + min(COD8XO , COD8TH)) * TX066/100) ;

CIRSETOT = CIRSE1 + CIRSE2 + CIRSE3 + CIRSE4 + CIRSE5 + CIRSE8 ;

regle 301380:
application : iliad ;

CRESINTER = PRESINTER ;

regle 301390:
application : iliad ;
REST = positif(IRE) * positif(CRESTACID) ;
VARTMP1 = 0 ;

8LAREST = positif(REST) * max(0 , min(LOY8LA , CRESTACID - VARTMP1)) ;
8LAIMP = positif_ou_nul(8LAREST) * (LOY8LA - 8LAREST) ;
VARTMP1 = VARTMP1 + LOY8LA ;

7LSREST = positif(REST) * max(0 , min(LOY7LS , CRESTACID - VARTMP1)) ;
7LSIMP = positif_ou_nul(7LSREST) * (LOY7LS - 7LSREST) ;
VARTMP1 = VARTMP1 + LOY7LS ;

LIBREST = positif(REST) * max(0 , min(AUTOVERSLIB , CRESTACID - VARTMP1)) ;
LIBIMP = positif_ou_nul(LIBREST) * (AUTOVERSLIB - LIBREST) ;
VARTMP1 = VARTMP1 + AUTOVERSLIB ;

8TEREST = positif(REST) * max(0 , min(COD8TE , CRESTACID - VARTMP1)) ;
8TEIMP = positif_ou_nul(8TEREST) * (COD8TE - 8TEREST) ;
VARTMP1 = VARTMP1 + COD8TE ;

AGRREST = positif(REST) * max(0 , min(CICONGAGRI , CRESTACID - VARTMP1)) ;
AGRIMP = positif_ou_nul(AGRREST) * (CICONGAGRI - AGRREST) ;
VARTMP1 = VARTMP1 + CICONGAGRI ;

ARTREST = positif(REST) * max(0 , min(CREARTS , CRESTACID - VARTMP1)) ;
ARTIMP = positif_ou_nul(ARTREST) * (CREARTS - ARTREST) ;
VARTMP1 = VARTMP1 + CREARTS ;

FORREST = positif(REST) * max(0 , min(CREFORMCHENT , CRESTACID - VARTMP1)) ;
FORIMP = positif_ou_nul(FORREST) * (CREFORMCHENT - FORREST) ;
VARTMP1 = VARTMP1 + CREFORMCHENT ;

PSIREST = positif(REST) * max(0 , min(CRESINTER , CRESTACID - VARTMP1)) ;
PSIIMP = positif_ou_nul(PSIREST) * (CRESINTER - PSIREST) ;
VARTMP1 = VARTMP1 + CRESINTER ;

BIOREST = positif(REST) * max(0 , min(CREAGRIBIO , CRESTACID - VARTMP1)) ;
BIOIMP = positif_ou_nul(BIOREST) * (CREAGRIBIO - BIOREST) ;
VARTMP1 = VARTMP1 + CREAGRIBIO ;

FAMREST = positif(REST) * max(0 , min(CREFAM , CRESTACID - VARTMP1)) ;
FAMIMP = positif_ou_nul(FAMREST) * (CREFAM - FAMREST) ;
VARTMP1 = VARTMP1 + CREFAM ;

HABREST = positif(REST) * max(0 , min(CIHABPRIN , CRESTACID - VARTMP1)) ;
HABIMP = positif_ou_nul(HABREST) * (CIHABPRIN - HABREST) ;
VARTMP1 = VARTMP1 + CIHABPRIN ;

ROFREST = positif(REST) * max(0 , min(CIFORET , CRESTACID - VARTMP1)) ;
ROFIMP = positif_ou_nul(ROFREST) * (CIFORET - ROFREST) ;
VARTMP1 = VARTMP1 + CIFORET ;

SALREST = positif(REST) * max(0 , min(CIADCRE , CRESTACID - VARTMP1)) ;
SALIMP = positif_ou_nul(SALREST) * (CIADCRE - SALREST) ;
VARTMP1 = VARTMP1 + CIADCRE ;

PREREST = positif(REST) * max(0 , min(CIPRETUD , CRESTACID - VARTMP1)) ;
PREIMP = positif_ou_nul(PREREST) * (CIPRETUD - PREREST) ;
VARTMP1 = VARTMP1 + CIPRETUD ;

SYNREST = positif(REST) * max(0 , min(CISYND , CRESTACID - VARTMP1)) ;
SYNIMP = positif_ou_nul(SYNREST) * (CISYND - SYNREST) ;
VARTMP1 = VARTMP1 + CISYND ;

GARREST = positif(REST) * max(0 , min(CIGARD , CRESTACID - VARTMP1)) ;
GARIMP = positif_ou_nul(GARREST) * (CIGARD - GARREST) ;
VARTMP1 = VARTMP1 + CIGARD ;

BAIREST = positif(REST) * max(0 , min(CICA , CRESTACID - VARTMP1)) ;
BAIIMP = positif_ou_nul(BAIREST) * (CICA - BAIREST) ;
VARTMP1 = VARTMP1 + CICA ;

TECREST = positif(REST) * max(0 , min(CITEC , CRESTACID - VARTMP1)) ;
TECIMP = positif_ou_nul(TECREST) * (CITEC - TECREST) ;
VARTMP1 = VARTMP1 + CITEC ;

DEPREST = positif(REST) * max(0 , min(CIDEVDUR , CRESTACID - VARTMP1)) ;
DEPIMP = positif_ou_nul(DEPREST) * (CIDEVDUR - DEPREST) ;
VARTMP1 = VARTMP1 + CIDEVDUR ;

AIDREST = positif(REST) * max(0 , min(CIGE , CRESTACID - VARTMP1)) ;
AIDIMP = positif_ou_nul(AIDREST) * (CIGE - AIDREST) ;
VARTMP1 = VARTMP1 + CIGE ;

HJAREST = positif(REST) * max(0 , min(CIHJA , CRESTACID - VARTMP1)) ;
HJAIMP = positif_ou_nul(HJAREST) * (CIHJA - HJAREST) ;
VARTMP1 = VARTMP1 + CIHJA ;

CORREST = positif(REST) * max(0 , min(CICORSENOW , CRESTACID - VARTMP1)) ;
CORIMP = positif_ou_nul(CORREST) * (CICORSENOW - CORREST) ;
VARTMP1 = VARTMP1 + CICORSENOW ;

EMPREST = positif(REST) * max(0 , min(COD8TL , CRESTACID - VARTMP1)) ;
EMPIMP = positif_ou_nul(EMPREST) * (COD8TL - EMPREST) ;
VARTMP1 = VARTMP1 + COD8TL ;

RECREST = positif(REST) * max(0 , min(IPRECH , CRESTACID - VARTMP1)) ;
RECIMP = positif_ou_nul(RECREST) * (IPRECH - RECREST) ;
VARTMP1 = VARTMP1 + IPRECH ;

ASSREST = positif(REST) * max(0 , min(I2DH , CRESTACID - VARTMP1)) ;
ASSIMP = positif_ou_nul(ASSREST) * (I2DH - ASSREST) ;
VARTMP1 = VARTMP1 + I2DH ;

2CKREST = positif(REST) * max(0 , min(CI2CK , CRESTACID - VARTMP1)) ;
2CKIMP = positif_ou_nul(2CKREST) * (CI2CK - 2CKREST) ;
VARTMP1 = 0 ;


