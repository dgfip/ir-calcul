#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2014 
#au titre des revenus perçus en 2013. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
  ####   #    #    ##    #####      #     #####  #####   ######      #    
 #    #  #    #   #  #   #    #     #       #    #    #  #           #    #
 #       ######  #    #  #    #     #       #    #    #  #####       #    #
 #       #    #  ######  #####      #       #    #####   #           ######
 #    #  #    #  #    #  #          #       #    #   #   #                #
  ####   #    #  #    #  #          #       #    #    #  ######           #
regle 401:
application : bareme, iliad , batch  ;
IRB = IAMD2; 
IRB2 = IAMD2 + TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES;
regle 40101:
application : iliad , batch  ;
KIR =   IAMD3 ;
regle 4011:
application : bareme , iliad , batch  ;
IAMD1 = IBM13 ;
IAMD2 = IBM23 ;
IAMD2TH = positif_ou_nul(IBM23 - SEUIL_61)*IBM23;
regle 40110:
application : bareme , iliad , batch  ;
IAMD3 = IBM33 - min(ACP3, IMPIM3);
regle 402112:
application : iliad , batch  ;
ANG3 = IAD32 - IAD31;
regle 40220:
application : iliad , batch  ;
ACP3 = max (0 ,
 somme (a=1..4: min(arr(CHENFa * TX_DPAEAV/100) , SEUIL_AVMAXETU)) - ANG3)
        * (1 - positif(V_CR2 + IPVLOC)) * positif(ANG3) * positif(IMPIM3);
regle 403:
application : bareme ,iliad , batch  ;

IBM13 = IAD11 + ITP + REI + AUTOVERSSUP + TAXASSUR + IPCAPTAXTOT  + TAXLOY + CHRAPRES + AVFISCOPTER ;

IBM23 = IAD11 + ITP + REI + AUTOVERSSUP + AVFISCOPTER ;

regle 404:
application : bareme , iliad , batch  ;
IBM33 = IAD31 + ITP + REI;
regle 4041:
application : iliad , batch  ;
DOMITPD = arr(BN1 + SPEPV + BI12F + BA1) * (TX11/100) * positif(V_EAD);
DOMITPG = arr(BN1 + SPEPV + BI12F + BA1) * (TX09/100) * positif(V_EAG);
DOMAVTD = arr((BN1 + SPEPV + BI12F + BA1) * TX05/100) * positif(V_EAD);
DOMAVTG = arr((BN1 + SPEPV + BI12F + BA1) * TX07/100) * positif(V_EAG);
DOMAVTO = DOMAVTD + DOMAVTG;
DOMABDB = max(PLAF_RABDOM - ABADO , 0) * positif(V_EAD)
          + max(PLAF_RABGUY - ABAGU , 0) * positif(V_EAG);
DOMDOM = max(DOMAVTO - DOMABDB , 0) * positif(V_EAD + V_EAG);
ITP = arr((BPTP2 * TX225/100) 
       + (BPTPVT * TX19/100) 
       + (BPTP4 * TX30/100) 
       +  DOMITPD + DOMITPG
       + (BPTP3 * TX16/100) 
       + (BPTP40 * TX41/100)
       + DOMDOM * positif(V_EAD + V_EAG)
       + (BPTP18 * TX18/100)
       + (BPTPSJ * TX19/100)
       + (BPTPWG * TX19/100)
       + (BPTPWJ * TX19/100)
       + (BPTP24 * TX24/100)
	  )
       * (1-positif(IPVLOC)) * (1 - positif(present(TAX1649)+present(RE168))); 
regle 40412:
application : iliad , batch  ;
REVTP = BPTP2 +BPTPVT+BPTP4+BTP3A+BPTP40+ BPTP24+BPTP18 +BPTPSJ +BPTPWG + BPTPWJ;
regle 40413:
application : iliad , batch  ;
BTP3A = (BN1 + SPEPV + BI12F + BA1) * (1 - positif( IPVLOC ));
BPTPD = BTP3A * positif(V_EAD)*(1-positif(present(TAX1649)+present(RE168)));
BPTPG = BTP3A * positif(V_EAG)*(1-positif(present(TAX1649)+present(RE168)));
BPTP3 = BTP3A * (1 - positif(V_EAD + V_EAG))*(1-positif(present(TAX1649)+present(RE168)));
BTP3N = (BPVKRI) * (1 - positif( IPVLOC ));
BTP3G = (BPVRCM) * (1 - positif( IPVLOC ));
BTP2 = PEA * (1 - positif( IPVLOC ));
BPTP2 = BTP2*(1-positif(present(TAX1649)+present(RE168)));
BTPVT = GAINPEA * (1 - positif( IPVLOC ));
BPTPVT = BTPVT*(1-positif(present(TAX1649)+present(RE168)));

BTP18 = (BPV18V + BPV18C) * (1 - positif( IPVLOC ));
BPTP18 = BTP18 * (1-positif(present(TAX1649)+present(RE168))) ;

BPTP4 = (BPCOPTV + BPCOPTC + BPVSK) * (1 - positif(IPVLOC)) * (1 - positif(present(TAX1649) + present(RE168))) ;
BPTP4I = (BPCOPTV + BPCOPTC) * (1 - positif(IPVLOC)) * (1 - positif(present(TAX1649) + present(RE168))) ;
BTPSK = BPVSK * (1 - positif( IPVLOC ));
BPTPSK = BTPSK * (1-positif(present(TAX1649)+present(RE168))) ;

BTP40 = (BPV40V + BPV40C) * (1 - positif( IPVLOC )) ;
BPTP40 = BTP40 * (1-positif(present(TAX1649)+present(RE168))) ;

BTP5 = PVIMPOS * (1 - positif( IPVLOC ));
BPTP5 = BTP5 * (1-positif(present(TAX1649)+present(RE168))) ;
BPTPWG = PVSURSIWG * (1 - positif( IPVLOC ))* (1-positif(present(TAX1649)+present(RE168)));
BPTPWJ = COD3WJ * (1 - positif( IPVLOC ))* (1-positif(present(TAX1649)+present(RE168)));
BTPSJ = BPVSJ * (1 - positif( IPVLOC ));
BPTPSJ = BTPSJ * (1-positif(present(TAX1649)+present(RE168))) ;
BTPSB = PVTAXSB * (1 - positif( IPVLOC ));
BPTPSB = BTPSB * (1-positif(present(TAX1649)+present(RE168))) ;
BPTP19 = (BPVSJ + GAINPEA + PVSURSIWG + COD3WJ) * (1 - positif( IPVLOC )) * (1 - positif(present(TAX1649) + present(RE168))) ;
BPTP19WGWJ = (PVSURSIWG + COD3WJ) * (1 - positif( IPVLOC )) * (1 - positif(present(TAX1649) + present(RE168))) ;
BPTP24 = RCM2FA *(1-positif(present(TAX1649)+present(RE168))) * (1 - positif(null(2-V_REGCO) + null(4-V_REGCO)));
ITPRCM = arr(BPTP24 * TX24/100);

BPTPDIV = BTP5 * (1-positif(present(TAX1649)+present(RE168))) ;

regle 4042:
application : iliad , batch  ;


REI = IPREP+IPPRICORSE;

regle 40421:
application : iliad , batch  ;


PPERSATOT = RSAFOYER + RSAPAC1 + RSAPAC2 ;

PPERSA = min(PPETOTX , PPERSATOT) * (1 - V_CNR) ;

PPEFINAL = PPETOTX - PPERSA ;

regle 405:
application : bareme , iliad , batch  ;


IAD11 = ( max(0,IDOM11-DEC11-RED) *(1-positif(V_CR2+IPVLOC))
        + positif(V_CR2+IPVLOC) *max(0 , IDOM11 - RED) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * IDOM16;
regle 40510:
application : bareme , iliad , batch  ;
IREXITI = (present(FLAG_EXIT) * ((1-positif(FLAG_3WBNEG)) * abs(NAPTIR - V_NAPTIR3WB) 
           + positif(FLAG_3WBNEG) * abs(NAPTIR + V_NAPTIR3WB)) * positif(present(PVIMPOS) + present(COD3WI)))
          * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

IREXITI19 = ((PVSURSIWG + COD3WJ) * TX19/100) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

IREXITS = ((1-positif(FLAG_3WANEG)) * abs(V_NAPTIR3WA-NAPTIR) 
           + positif(FLAG_3WANEG) * abs(-V_NAPTIR3WA - NAPTIR)) * present(FLAG_EXIT) * present(PVSURSI)
          * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

IREXITS19 = (PVSURSIWF * TX19/100) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

IREXIT = IREXITI + IREXITS ;

EXITTAX3 = ((positif(FLAG_3WBNEG) * (-1) * ( V_NAPTIR3WB) + (1-positif(FLAG_3WBNEG)) * (V_NAPTIR3WB)) * positif(present(PVIMPOS)+present(COD3WI))
            + NAPTIR * present(PVSURSI) * (1-positif(present(PVIMPOS)+present(COD3WI))))
           * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;
regle 406:
application : bareme , iliad , batch  ;
IAD31 = ((IDOM31-DEC31)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM31);
IAD32 = ((IDOM32-DEC32)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM32);

regle 4052:
application : bareme , iliad , batch  ;

IMPIM3 =  IAD31 ;

regle 4061:
application : bareme , iliad , batch  ;
pour z = 1,2:
DEC1z = min (max( arr(SEUIL_DECOTE/2 - (IDOM1z/2)),0),IDOM1z) * (1 - V_CNR);

pour z = 1,2:
DEC3z = min (max( arr(SEUIL_DECOTE/2 - (IDOM3z/2)),0),IDOM3z) * (1 - V_CNR);

DEC6 = min (max( arr(SEUIL_DECOTE/2 - (IDOM16/2)),0),IDOM16) * (1 - V_CNR);

regle 407:
application : iliad   , batch ;
      
RED =  RCOTFOR + RSURV + RCOMP + RHEBE + RREPA + RDIFAGRI + RDONS
       + RDUFLOGIH
       + RCELTOT
       + RRESTIMO * (1-V_INDTEO)  + V_RRESTIMOXY * V_INDTEO
       + RFIPC + RFIPDOM + RAIDE + RNOUV 
       + RTOURREP 
       + RTOUREPA + RTOUHOTR  
       + RLOGDOM + RLOGSOC + RDOMSOC1 + RLOCENT + RCOLENT
       + RRETU + RINNO + RRPRESCOMP + RFOR 
       + RSOUFIP + RRIRENOV + RSOCREPR + RRESIMEUB + RRESINEUV + RRESIVIEU 
       + RLOCIDEFG + RCODJT + RCODJU
       + RREDMEUB + RREDREP + RILMIX + RILMIY + RINVRED + RILMIH + RILMJC
       + RILMIZ + RILMJI + RILMJS + RMEUBLE + RPROREP + RREPNPRO + RREPMEU 
       + RILMIC + RILMIB + RILMIA + RILMJY + RILMJX + RILMJW + RILMJV
       + RIDOMPROE3   
       + RPATNAT1 + RPATNAT2 + RPATNAT3 + RPATNAT
       + RFORET + RCREAT + RCINE  
       + RREVMOD ;

REDTL = ASURV + ACOMP ;

CIMPTL = ATEC + ADEVDUR + ADEPENV + TOTBGE ;

RED_AVT_DS =
ACOTFOR * TX76/100 + RRS + RFC + RAH + RAA + RAGRI +RON +RDUFLO_GIH
+ somme (i=A,B,E,M,C,D,S,F,Z : ACELRREDLi) + ACELRREDMG
+somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : ACELREPHi)
+ somme (i=U,X,T,S,W,P,L,V,K,J : ACELREPGi )
+ RCEL_HM + RCEL_HL + RCEL_HNO + RCEL_HJK + RCEL_NQ + RCEL_NBGL + RCEL_COM
+ RCEL_2011 + RCEL_JP + RCEL_JBGL + RCEL_JOQR + RCEL_2012 + RCEL_FD + RCEL_FABC
+ RETRESTIMO + RFIPCORSE + RFIPDOMCOM + RAD + RSN
+ ATOURREP * TX_REDIL25 / 100 + ATOUREPA * TX_REDIL20 / 100 + RIHOTR
+ somme (i=1..9 : RLOGi) + somme (i=10..32 : RLOGi) + somme (i=1..8 : RSOCi) 
+ RSOC9 + somme (i=10..28 : RSOCi)
+ somme (i=1..9 : RENTi) + somme (i=10..36 : RENTi) 
+ somme (i=1..9 : RLOCi) + somme (i=10..88 : RLOCi)
+ RETUD + RFCPI + RPRESCOMP + RFOREST + RFIP + RENOV + RSOCREP + RETRESIMEUB
+ RETCODIL + RETCODIN + RETCODIV + RETCODIJ
+ RETRESIVIEU
+ RETCODIE + RETCODIF + RETCODIG + RETCODID
+ RETCODJT + RETCODJU + AREDMEUB + AREDREP + AILMIX + AILMIY + AINVRED
+ AILMIH + AILMJC + AILMIZ + AILMJI + AILMJS + MEUBLERET + RETPROREP
+ RETREPNPRO + RETREPMEU + AILMIC + AILMIB + AILMIA + AILMJY + AILMJX
+ AILMJW + AILMJV + REPINVDOMPRO3 + APATNAT1 + APATNAT2 + APATNAT3 + RAPATNAT
+ RAFORET + RRCN + REVMOD
+ RCREATEUR + RCREATEURHANDI
;


RED_1 =  RCOTFOR_1 + RSURV_1 + RCOMP_1 + RHEBE_1 + RREPA_1 + RDIFAGRI_1 + RDONS_1
       + RDUFLOGIH_1
       + RCELTOT_1
       + RRESTIMO_1 * (1-V_INDTEO)
       + RFIPC_1 + RFIPDOM_1 + RAIDE_1 + RNOUV_1
       + RTOURREP_1
       + RTOUREPA_1 + RTOUHOTR_1
       + RLOGDOM_1 + RLOGSOC_1 + RDOMSOC1_1 + RLOCENT_1 + RCOLENT_1
       + RRETU_1 + RINNO_1 + RRPRESCOMP_1 + RFOR_1
       + RSOUFIP_1 + RRIRENOV_1 + RSOCREPR_1 + RRESIMEUB_1 + RRESINEUV_1 + RRESIVIEU_1
       + RLOCIDEFG_1 + RCODJT_1 + RCODJU_1
       + RREDMEUB_1 + RREDREP_1 + RILMIX_1 + RILMIY_1 + RINVRED_1 + RILMIH_1 + RILMJC_1
       + RILMIZ_1 + RILMJI_1 + RILMJS_1 + RMEUBLE_1 + RPROREP_1 + RREPNPRO_1 + RREPMEU_1
       + RILMIC_1 + RILMIB_1 + RILMIA_1 + RILMJY_1 + RILMJX_1 + RILMJW_1 + RILMJV_1
       + RIDOMPROE3_1
       + RPATNAT1_1 + RPATNAT2_1 + RPATNAT3_1 + RPATNAT_1
       + RFORET_1 + RCREAT_1 + RCINE_1
       + RREVMOD_1 ;

regle 4070:
application : bareme ;
RED = V_9UY;
regle 4025:
application : iliad , batch  ;

PLAFDOMPRO1 = max(0 , RRI1_1-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS-RDUFLOGIH-RNOUV-RFOR
                          -RTOURREP-RTOUHOTR-RTOUREPA-RCELTOT-RLOCNPRO-RPATNATOT
                          -RDOMSOC1-RLOGSOC ) ;
                          
RIDOMPROE3_1 = min(REPINVDOMPRO3 , PLAFDOMPRO1) * (1 - V_CNR) ;

RIDOMPROE3 = (RIDOMPROE3_1 * (1-ART1731BIS)
              + min(RIDOMPROE3_1 , max(RIDOMPROE3_P, RIDOMPROE31731+0)) * ART1731BIS) * (1-V_CNR);
                  
REPOMENTR3 = positif(REPINVDOMPRO3 - PLAFDOMPRO1) * (REPINVDOMPRO3 - PLAFDOMPRO1) * (1 - V_CNR) ;


RIDOMPROTOT_1 = RIDOMPROE3_1 ;
RIDOMPROTOT = RIDOMPROE3 ;


RINVEST = RIDOMPROE3 ;

RIDOMPRO = REPINVDOMPRO3 ;

DIDOMPRO = ( RIDOMPRO * (1-ART1731BIS) 
             + min( RIDOMPRO, max(DIDOMPRO_P , DIDOMPRO1731+0 )) * ART1731BIS ) * (1 - V_CNR) ;

regle 40749:
application : iliad , batch  ;

DFORET = FORET ;

AFORET_1 = max(min(DFORET,LIM_FORET),0) * (1-V_CNR) ;

AFORET = max( 0 , AFORET_1  * (1-ART1731BIS) 
                  + min( AFORET_1 , max(AFORET_P , AFORET1731+0)) * ART1731BIS
            ) * (1-V_CNR) ;

RAFORET = arr(AFORET*TX_FORET/100) * (1-V_CNR) ;

RFORET_1 =  max( min( RAFORET , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI) , 0 ) ;
RFORET =  max( 0 , RFORET_1 * (1-ART1731BIS) 
                   + min( RFORET_1 , max( RFORET_P , RFORET1731+0 )) * ART1731BIS ) ;

regle 4075:
application : iliad , batch ;

DFIPC = FIPCORSE ;

AFIPC_1 = max( min(DFIPC , LIM_FIPCORSE * (1 + BOOL_0AM)) , 0) * (1 - V_CNR) ;

AFIPC = max( 0, AFIPC_1 * (1-ART1731BIS)
                + min( AFIPC_1 , max( AFIPC_P , AFIPC1731+0 )) * ART1731BIS
           ) * (1 - V_CNR) ;

RFIPCORSE = arr(AFIPC * TX_FIPCORSE/100) * (1 - V_CNR) ;

RFIPC_1 = max( min( RFIPCORSE , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI-RFORET-RFIPDOM) , 0) ;
RFIPC = max( 0, RFIPC_1 * (1 - ART1731BIS) 
                + min( RFIPC_1 , max(RFIPC_P , RFIPC1731+0 )) * ART1731BIS ) ;

regle 40751:
application : iliad , batch ;

DFIPDOM = FIPDOMCOM ;

AFIPDOM_1 = max( min(DFIPDOM , LIMFIPDOM * (1 + BOOL_0AM)) , 0) * (1 - V_CNR) ;
AFIPDOM = max( 0 , AFIPDOM_1 * (1 - ART1731BIS)
               + min( AFIPDOM_1 , max(AFIPDOM_P , AFIPDOM1731+0)) * ART1731BIS
	     ) * (1 - V_CNR) ;

RFIPDOMCOM = arr(AFIPDOM * TXFIPDOM/100) * (1 - V_CNR) ;

RFIPDOM_1 = max( min( RFIPDOMCOM , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI-RFORET),0);
RFIPDOM = max( 0 , RFIPDOM_1 * (1 - ART1731BIS) 
                   + min( RFIPDOM_1, max(RFIPDOM_P ,  RFIPDOM1731+0 )) * ART1731BIS ) ;

regle 4076:
application : iliad , batch  ;
BSURV = min( RDRESU , PLAF_RSURV + PLAF_COMPSURV * (EAC + V_0DN) + PLAF_COMPSURVQAR * (V_0CH + V_0DP) );
RRS = arr( BSURV * TX_REDSURV / 100 ) * (1 - V_CNR);
DSURV = RDRESU;

ASURV = (BSURV * (1-ART1731BIS)
         + min( BSURV , max( ASURV_P , ASURV1731+0 )) * ART1731BIS)  * (1-V_CNR);

RSURV_1 = max( min( RRS , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI-RFORET-RFIPDOM-RFIPC
			  -RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE ) , 0 ) ;

RSURV = max( 0 , RSURV_1 * (1-ART1731BIS) 
                 + min( RSURV_1, max(RSURV_P , RSURV1731+0 )) * ART1731BIS ) ;

regle 4100:
application : iliad , batch ;

RRCN = arr(  min( CINE1 , min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE )) * TX_CINE1/100
        + min( CINE2 , max( min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE ) - CINE1 , 0)) * TX_CINE2/100 
       ) * (1 - V_CNR) ;

DCINE = CINE1 + CINE2 ;

ACINE_1 = max(0,min( CINE1 + CINE2 , min( arr(SOFIRNG * TX_CINE3/100) , PLAF_CINE ))) * (1 - V_CNR) ;
ACINE = max( 0, ACINE_1 * (1-ART1731BIS) 
                + min( ACINE_1 , max(ACINE_P , ACINE1731+0 )) * ART1731BIS
           ) * (1-V_CNR) ; 

RCINE_1 = max( min( RRCN , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI-RFORET-RFIPDOM-RFIPC) , 0 ) ;
RCINE = max( 0, RCINE_1 * (1-ART1731BIS) 
                + min( RCINE_1 , max(RCINE_P , RCINE1731+0 )) * ART1731BIS ) ;

regle 4176:
application : iliad , batch  ;
BSOUFIP = min( FFIP , LIM_SOUFIP * (1 + BOOL_0AM));
RFIP = arr( BSOUFIP * TX_REDFIP / 100 ) * (1 - V_CNR);
DSOUFIP = FFIP;

ASOUFIP = (BSOUFIP * (1-ART1731BIS) 
           + min( BSOUFIP , max(ASOUFIP_P , ASOUFIP1731+0 ))) * (1-V_CNR) ;

RSOUFIP_1 = max( min( RFIP , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI-RFORET-RFIPDOM-RFIPC
			   -RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO) , 0 ) ;

RSOUFIP = max( 0 , RSOUFIP_1 * (1-ART1731BIS) 
                   + min( RSOUFIP_1 , max(RSOUFIP_P , RSOUFIP1731+0)) * ART1731BIS ) ;

regle 4200:
application : iliad , batch  ;

BRENOV = min(RIRENOV,PLAF_RENOV) ;

RENOV = arr( BRENOV * TX_RENOV / 100 ) * (1 - V_CNR) ;

DRIRENOV = RIRENOV ;

ARIRENOV = (BRENOV * (1-ART1731BIS) 
            + min( BRENOV, max(ARIRENOV_P , ARIRENOV1731+0)) * ART1731BIS ) * (1 - V_CNR) ;

RRIRENOV_1 = max(min(RENOV , IDOM11-DEC11-RCOTFOR-RREPA-RAIDE-RDIFAGRI-RFORET-RFIPDOM-RFIPC-RCINE
			     -RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP) , 0 ) ;

RRIRENOV = max( 0 , RRIRENOV_1 * (1-ART1731BIS) 
                    + min(RRIRENOV_1 , max(RRIRENOV_P , RRIRENOV1731+0)) * ART1731BIS ) ;

regle 40771:
application : iliad , batch  ;
RFC = min(RDCOM,PLAF_FRCOMPTA * max(1,NBACT)) * present(RDCOM)*(1-V_CNR);
NCOMP = ( max(1,NBACT)* present(RDCOM) * (1-ART1731BIS) + min( max(1,NBACT)* present(RDCOM) , NCOMP1731+0) * ART1731BIS ) * (1-V_CNR);
DCOMP = RDCOM;

ACOMP =  RFC * (1-ART1731BIS) 
         + min( RFC , max(ACOMP_P , ACOMP1731+0 )) * ART1731BIS ;

regle 10040771:
application :  iliad , batch  ;
RCOMP_1 = max(min( RFC , RRI1-RLOGDOM-RCREAT) , 0) ;
RCOMP = max( 0 , RCOMP_1 * (1-ART1731BIS) 
                 + min( RCOMP_1 ,max(RCOMP_P , RCOMP1731+0 )) * ART1731BIS ) ;

regle 4077:
application : iliad , batch  ;
DDUFLOGIH = DUFLOGI + DUFLOGH ;

ADUFLOGIH_1 = ( arr( min( DUFLOGI + 0, LIMDUFLO) / 9 ) +
              arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) / 9 )
            ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

ADUFLOGIH =  ADUFLOGIH_1 * (1-ART1731BIS) 
             + min( ADUFLOGIH_1, max(ADUFLOGIH_P , ADUFLOGIH1731 +0 )) * ART1731BIS ;

RDUFLO_GIH = ( arr( arr( min( DUFLOGI + 0, LIMDUFLO) / 9 ) * (TX29/100)) +
              arr( arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) / 9 ) * (TX18/100))
             ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

regle 40772:
application : iliad , batch  ;

RDUFLOGIH_1 = max( 0, min( RDUFLO_GIH , RRI1-RLOGDOM-RCREAT-RCOMP-RRETU-RDONS)) ;

RDUFLOGIH = max( 0, RDUFLOGIH_1 * (1 - ART1731BIS) 
                    + min ( RDUFLOGIH_1 , max(RDUFLOGIH_P , RDUFLOGIH1731+0 )) * ART1731BIS ) ;
regle 40773:
application : iliad , batch  ;

RIVDUFLOGIH = ( arr( arr( min( DUFLOGI + 0, LIMDUFLO) / 9 ) * (TX29/100)) +
                arr( arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) / 9 ) * (TX18/100))
              ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ;

RIVDUFLOGIH8 = max (0 , ( arr( min( DUFLOGI + 0, LIMDUFLO) * (TX29/100)) +
                          arr( min( DUFLOGH + 0, LIMDUFLO - min( DUFLOGI + 0, LIMDUFLO)) * (TX18/100))
                        ) 
                          - 8 * RIVDUFLOGIH  
                   ) * ( 1 - null( 4-V_REGCO )) * ( 1 - null( 2-V_REGCO )) ; 

REPDUFLOT2013 = RIVDUFLOGIH * 7 + RIVDUFLOGIH8 ;


regle 4078:
application : iliad , batch  ;
BCEL_FABC = arr ( min( CELLIERFA + CELLIERFB + CELLIERFC , LIMCELLIER ) /9 );

BCEL_FD = arr ( min( CELLIERFD , LIMCELLIER ) /5 );

BCEL_2012 = arr( min(( CELLIERJA + CELLIERJD + CELLIERJE + CELLIERJF + CELLIERJH + CELLIERJJ 
		     + CELLIERJK + CELLIERJM + CELLIERJN + 0 ), LIMCELLIER ) /9 );

BCEL_JOQR = arr( min(( CELLIERJO + CELLIERJQ + CELLIERJR + 0 ), LIMCELLIER ) /5 );

BCEL_2011 = arr( min(( CELLIERNA + CELLIERNC + CELLIERND + CELLIERNE + CELLIERNF + CELLIERNH
		     + CELLIERNI + CELLIERNJ + CELLIERNK + CELLIERNM + CELLIERNN + CELLIERNO  + 0 ), LIMCELLIER ) /9 );

BCELCOM2011 = arr( min(( CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT + 0 ), LIMCELLIER ) /5 );

BCEL_NBGL = arr( min(( CELLIERNB + CELLIERNG + CELLIERNL + 0), LIMCELLIER ) /9 );

BCEL_NQ = arr( min(( CELLIERNQ + 0), LIMCELLIER ) /5 );

BCEL_JBGL = arr( min(( CELLIERJB + CELLIERJG + CELLIERJL + 0), LIMCELLIER ) /9 );

BCEL_JP = arr( min(( CELLIERJP + 0), LIMCELLIER ) /5 );


BCEL_HNO = arr ( min ((CELLIERHN + CELLIERHO + 0 ), LIMCELLIER ) /9 );
BCEL_HJK = arr ( min ((CELLIERHJ + CELLIERHK + 0 ), LIMCELLIER ) /9 );

BCEL_HL = arr ( min ((CELLIERHL + 0 ), LIMCELLIER ) /9 );
BCEL_HM = arr ( min ((CELLIERHM + 0 ), LIMCELLIER ) /9 );


DCELRREDLA = CELRREDLA;
ACELRREDLA_R = CELRREDLA * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELRREDLA = (CELRREDLA * (1-ART1731BIS) 
              + min (CELRREDLA, max(ACELRREDLA_P , ACELRREDLA1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLB = CELRREDLB;
ACELRREDLB_R = CELRREDLB * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELRREDLB = (CELRREDLB * (1-ART1731BIS) 
              + min (CELRREDLB, max(ACELRREDLB_P , ACELRREDLB1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLE = CELRREDLE;
ACELRREDLE_R = CELRREDLE * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELRREDLE = (CELRREDLE * (1-ART1731BIS)
              + min (CELRREDLE , max(ACELRREDLE_P , ACELRREDLE1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLM = CELRREDLM;


ACELRREDLM = (CELRREDLM * (1-ART1731BIS) 
              + min (CELRREDLM, max(ACELRREDLM_P , ACELRREDLM1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLC = CELRREDLC;
ACELRREDLC_R = CELRREDLC * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELRREDLC = (CELRREDLC * (1-ART1731BIS) 
              + min (CELRREDLC, max(ACELRREDLC_P , ACELRREDLC1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLD = CELRREDLD;
ACELRREDLD_R = CELRREDLD * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELRREDLD = (CELRREDLD * (1-ART1731BIS) 
              + min (CELRREDLD, max(ACELRREDLD_P , ACELRREDLD1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLS = CELRREDLS;


ACELRREDLS = (CELRREDLS * (1-ART1731BIS) 
              + min (CELRREDLS, max(ACELRREDLS_P , ACELRREDLS1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLF = CELRREDLF;

ACELRREDLF_R = CELRREDLF * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELRREDLF = (CELRREDLF * (1-ART1731BIS) 
              + min (CELRREDLF, max(ACELRREDLF_P , ACELRREDLF1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDLZ = CELRREDLZ;


ACELRREDLZ = (CELRREDLZ * (1-ART1731BIS) 
              + min (CELRREDLZ, max(ACELRREDLZ_P , ACELRREDLZ1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELRREDMG = CELRREDMG;



ACELRREDMG = (CELRREDMG * (1-ART1731BIS) 
              + min (CELRREDMG, max(ACELRREDMG_P , ACELRREDMG1731 +0)) * ART1731BIS)
	      * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHS = CELREPHS; 
ACELREPHS_R = DCELREPHS * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHS = (DCELREPHS * (1 - ART1731BIS) 
             + min( DCELREPHS , max(ACELREPHS_P , ACELREPHS1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHR = CELREPHR ;    
ACELREPHR_R = DCELREPHR * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHR = (DCELREPHR * (1 - ART1731BIS) 
             + min( DCELREPHR , max(ACELREPHR_P , ACELREPHR1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHU = CELREPHU; 
ACELREPHU_R = DCELREPHU * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHU = (DCELREPHU * (1 - ART1731BIS) 
             + min( DCELREPHU , max(ACELREPHU_P , ACELREPHU1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHT = CELREPHT; 
ACELREPHT_R = DCELREPHT * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHT = (DCELREPHT * (1 - ART1731BIS) 
             + min( DCELREPHT , max(ACELREPHT_P , ACELREPHT1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHZ = CELREPHZ; 
ACELREPHZ_R = DCELREPHZ * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHZ = (DCELREPHZ * (1 - ART1731BIS) 
             + min( DCELREPHZ , max(ACELREPHZ_P , ACELREPHZ1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHX = CELREPHX; 
ACELREPHX_R = DCELREPHX * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHX = (DCELREPHX * (1 - ART1731BIS) 
             + min( DCELREPHX , max(ACELREPHX_P , ACELREPHX1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHW = CELREPHW; 
ACELREPHW_R = DCELREPHW * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHW = (DCELREPHW * (1 - ART1731BIS) 
             + min( DCELREPHW , max(ACELREPHW_P , ACELREPHW1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHV = CELREPHV; 
ACELREPHV_R = DCELREPHV * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHV = (DCELREPHV * (1 - ART1731BIS) 
             + min( DCELREPHV , max(ACELREPHV_P , ACELREPHV1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHF = CELREPHF; 
ACELREPHF_R = DCELREPHF * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHF = (DCELREPHF * (1 - ART1731BIS) 
             + min( DCELREPHF , max(ACELREPHF_P , ACELREPHF1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHE = CELREPHE ;    
ACELREPHE_R = DCELREPHE * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHE = (DCELREPHE * (1 - ART1731BIS) 
             + min( DCELREPHE , max(ACELREPHE_P , ACELREPHE1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHD = CELREPHD; 
ACELREPHD_R = DCELREPHD * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHD = (DCELREPHD * (1 - ART1731BIS) 
             + min( DCELREPHD , max(ACELREPHD_P , ACELREPHD1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHH = CELREPHH; 
ACELREPHH_R = DCELREPHH * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHH = (DCELREPHH * (1 - ART1731BIS) 
             + min( DCELREPHH , max(ACELREPHH_P , ACELREPHH1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHG = CELREPHG; 
ACELREPHG_R = DCELREPHG * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHG = (DCELREPHG * (1 - ART1731BIS) 
             + min( DCELREPHG , max(ACELREPHG_P , ACELREPHG1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHB = CELREPHB; 
ACELREPHB_R = DCELREPHB * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHB = (DCELREPHB * (1 - ART1731BIS) 
             + min( DCELREPHB , max(ACELREPHB_P , ACELREPHB1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPHA = CELREPHA; 
ACELREPHA_R = DCELREPHA * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELREPHA = (DCELREPHA * (1 - ART1731BIS) 
             + min( DCELREPHA , max(ACELREPHA_P , ACELREPHA1731 + 0 )) * ART1731BIS)
	        * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGU = CELREPGU; 


ACELREPGU = (DCELREPGU * (1 - ART1731BIS) 
             + min( DCELREPGU , max (ACELREPGU_P , ACELREPGU1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGX = CELREPGX; 


ACELREPGX = (DCELREPGX * (1 - ART1731BIS) 
             + min( DCELREPGX , max (ACELREPGX_P , ACELREPGX1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGT = CELREPGT; 


ACELREPGT = (DCELREPGT * (1 - ART1731BIS) 
             + min( DCELREPGT , max (ACELREPGT_P , ACELREPGT1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGS = CELREPGS; 


ACELREPGS = (DCELREPGS * (1 - ART1731BIS) 
             + min( DCELREPGS , max (ACELREPGS_P , ACELREPGS1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGW = CELREPGW; 


ACELREPGW = (DCELREPGW * (1 - ART1731BIS) 
             + min( DCELREPGW , max (ACELREPGW_P , ACELREPGW1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGP = CELREPGP; 


ACELREPGP = (DCELREPGP * (1 - ART1731BIS) 
             + min( DCELREPGP , max (ACELREPGP_P , ACELREPGP1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGL = CELREPGL; 


ACELREPGL = (DCELREPGL * (1 - ART1731BIS) 
             + min( DCELREPGL , max (ACELREPGL_P , ACELREPGL1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGV = CELREPGV; 


ACELREPGV = (DCELREPGV * (1 - ART1731BIS) 
             + min( DCELREPGV , max (ACELREPGV_P , ACELREPGV1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGK = CELREPGK; 


ACELREPGK = (DCELREPGK * (1 - ART1731BIS) 
             + min( DCELREPGK , max (ACELREPGK_P , ACELREPGK1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELREPGJ = CELREPGJ; 


ACELREPGJ = (DCELREPGJ * (1 - ART1731BIS) 
             + min( DCELREPGJ , max (ACELREPGJ_P , ACELREPGJ1731 + 0 )) * ART1731BIS)
		* (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELHM = CELLIERHM ; 

ACELHM_R = positif_ou_nul( CELLIERHM) * BCEL_HM * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELHM = ( BCEL_HM * (1-ART1731BIS) 
          + min(BCEL_HM , max(ACELHM_P , ACELHM1731+0))* ART1731BIS )  
         * (positif_ou_nul(CELLIERHM)) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELHL = CELLIERHL ;    

ACELHL_R = positif_ou_nul( CELLIERHL) * BCEL_HL * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELHL = ( BCEL_HL * (1-ART1731BIS) 
          + min(BCEL_HL , max(ACELHL_P , ACELHL1731+0))* ART1731BIS )  
         * (positif_ou_nul(CELLIERHL)) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


DCELHNO = CELLIERHN + CELLIERHO ;

ACELHNO_R = positif_ou_nul( CELLIERHN + CELLIERHO ) * BCEL_HNO * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELHNO = ( BCEL_HNO * (1-ART1731BIS) 
          + min(BCEL_HNO , max(ACELHNO_P , ACELHNO1731+0))* ART1731BIS )  
         * (positif_ou_nul(CELLIERHN + CELLIERHO)) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELHJK = CELLIERHJ + CELLIERHK ;

ACELHJK_R = positif_ou_nul( CELLIERHJ + CELLIERHK ) * BCEL_HJK * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELHJK = ( BCEL_HJK * (1-ART1731BIS) 
          + min(BCEL_HJK , max(ACELHJK_P , ACELHJK1731+0))* ART1731BIS )  
         * (positif_ou_nul(CELLIERHJ + CELLIERHK)) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


DCELNQ = CELLIERNQ;
ACELNQ_R = (positif_ou_nul( CELLIERNQ) * BCEL_NQ) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELNQ = ( BCEL_NQ * (1-ART1731BIS) 
          + min(BCEL_NQ , max(ACELNQ_P , ACELNQ1731+0))* ART1731BIS )  
         * (positif_ou_nul(CELLIERNQ)) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELNBGL =   CELLIERNB + CELLIERNG + CELLIERNL;

ACELNBGL_R = positif_ou_nul( CELLIERNB + CELLIERNG + CELLIERNL ) * BCEL_NBGL * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELNBGL = ( BCEL_NBGL * (1-ART1731BIS) 
             + min(BCEL_NBGL , max(ACELNBGL_P , ACELNBGL1731+0))* ART1731BIS )  
           * positif_ou_nul(CELLIERNB + CELLIERNG + CELLIERNL) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELCOM =   CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT;

ACELCOM_R = positif_ou_nul(CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT) * BCELCOM2011 * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELCOM = ( BCELCOM2011 * (1-ART1731BIS) 
          + min(BCELCOM2011 , max(ACELCOM_P , ACELCOM1731+0))* ART1731BIS )  
          * positif_ou_nul(CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

CELSOMN = CELLIERNA+CELLIERNC+CELLIERND+CELLIERNE+CELLIERNF+CELLIERNH
	 +CELLIERNI+CELLIERNJ+CELLIERNK+CELLIERNM+CELLIERNN+CELLIERNO;  

DCEL = CELSOMN ; 


ACEL_R = positif_ou_nul( CELSOMN ) * BCEL_2011 * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACEL = (BCEL_2011 * (1 - ART1731BIS) 
        + min(BCEL_2011 , max(ACEL_P , ACEL1731+0)) * ART1731BIS)  
          * positif_ou_nul(CELSOMN) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELJP = CELLIERJP;

ACELJP_R = positif_ou_nul( CELLIERJP) * BCEL_JP * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;



ACELJP = (BCEL_JP * (1 - ART1731BIS) 
          + min(BCEL_JP , max(ACELJP_P , ACELJP1731+0)) * ART1731BIS)  
          * positif_ou_nul(CELLIERJP) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;

DCELJBGL =   CELLIERJB + CELLIERJG + CELLIERJL;

ACELJBGL_R = positif_ou_nul( CELLIERJB + CELLIERJG + CELLIERJL ) * BCEL_JBGL * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;


ACELJBGL = (BCEL_JBGL * (1 - ART1731BIS) 
          + min(BCEL_JBGL , max(ACELJBGL_P , ACELJBGL1731+0)) * ART1731BIS)  
          * positif_ou_nul(CELLIERJB+CELLIERJG+CELLIERJL) * (1 - null(2 - V_REGCO)) * (1 - null(4 - V_REGCO)) ;



DCELJOQR =   CELLIE